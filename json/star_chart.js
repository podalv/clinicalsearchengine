var StarChart = class { 

	constructor (canvas) {
		this.populationStarColor = '#9b9b9b';
		this.cohortStarColor = '#ccdd1f';
		this.lineColor = '#292929';
		this.height = 463;
		this.margin = 30;
		if ($query.hasResponse()) {
			this.draw(canvas);
		}
	}
	
	getCoordinates(x0, y0, r, noOfDividingPoints) {
		var x = [];
		var y = [];

        var angle = 0;

        for(var i = 0 ; i < noOfDividingPoints  ;i++)
        {
            angle = i * (360/noOfDividingPoints);
            
            x.push(x0 + r * Math.cos(angle * Math.PI / 180));
            y.push(y0 + r * Math.sin(angle * Math.PI / 180));

        }

        return [x, y];
    }
	
	getMaxPercentage() {
		var maxPercent = 0;
		for (x = 0; x < $query.getPatientStatistics.length; x++) {
			maxPercent = Math.max(maxPercent, Math.max($query.getPatientStatistics[x].generalPercentage, $query.getPatientStatistics[x].cohortPercentage));
		}
		return maxPercent;
	}
	
	drawGeneralPopulationStar(ctx, min, max) {
		ctx.beginPath();
		ctx.strokeStyle=this.populationStarColor;
		for (x = 0; x < $query.getPatientStatistics().length; x++) {
			var coordinates = this.getCoordinates((max / 2), (max / 2), ((max / 2) - 20) * $query.getPatientStatistics()[x].generalPercentage, $query.getPatientStatistics().length);		
			if (x == 0) {
				ctx.moveTo(coordinates[0][x], coordinates[1][x]);
			} else {
				ctx.lineTo(coordinates[0][x], coordinates[1][x]);				
			}
		}
		var coordinates = this.getCoordinates((max / 2), (max / 2), ((max / 2) - 20) * $query.getPatientStatistics()[0].generalPercentage, $query.getPatientStatistics().length);
		ctx.lineTo(coordinates[0][0], coordinates[1][0]);
		ctx.save();
		ctx.globalAlpha = 0.2;
	    ctx.fillStyle = this.populationStarColor;
	    ctx.fill();
		ctx.restore();

		ctx.stroke();
	}
	
	drawCohortStar(ctx, min, max) {
		ctx.strokeStyle=this.cohortStarColor;
		ctx.beginPath();
		for (x = 0; x < $query.getPatientStatistics().length; x++) {
			var coordinates = this.getCoordinates((max / 2), (max / 2), ((max / 2) - 20) * $query.getPatientStatistics()[x].cohortPercentage, $query.getPatientStatistics().length);
			if (x == 0) {
				ctx.moveTo(coordinates[0][x], coordinates[1][x]);
			} else {
				ctx.lineTo(coordinates[0][x], coordinates[1][x]);				
			}
		}
		var coordinates = this.getCoordinates((max / 2), (max / 2), ((max / 2) - 20) * $query.getPatientStatistics()[0].cohortPercentage, $query.getPatientStatistics().length);
		ctx.lineTo(coordinates[0][0], coordinates[1][0]);
		ctx.stroke();
		ctx.save();
		ctx.globalAlpha = 0.4;
		ctx.fillStyle=this.cohortStarColor;
	    ctx.fill();
		ctx.restore();
	}
	
	drawText(ctx, min, max) {
		var coordinates = this.getCoordinates((max / 2), (max / 2), ((max / 2) - 20), $query.getPatientStatistics().length);
		ctx.beginPath();
		ctx.strokeStyle=this.lineColor;
		ctx.fillStyle = this.lineColor;
		for (x = 0; x < $query.getPatientStatistics().length; x++) {
			ctx.moveTo(max / 2, max / 2);
			ctx.lineTo(coordinates[0][x], coordinates[1][x]);
			if (x == 0) {
				ctx.fillText($query.getPatientStatistics()[x].label, coordinates[0][x] + 5, coordinates[1][x] + 3);
			} else if (x == 1) {
				ctx.fillText($query.getPatientStatistics()[x].label, coordinates[0][x] - (ctx.measureText($query.getPatientStatistics()[x].label).width / 2) + 4, coordinates[1][x] + 15);
			} else if (x == 2) {
				ctx.fillText($query.getPatientStatistics()[x].label, coordinates[0][x] - (ctx.measureText($query.getPatientStatistics()[x].label).width) + 5, coordinates[1][x] + 12);
			} else if (x == 3) {
				ctx.fillText($query.getPatientStatistics()[x].label, coordinates[0][x] - (ctx.measureText($query.getPatientStatistics()[x].label).width / 2), coordinates[1][x] - 5);
			} else {
				ctx.fillText($query.getPatientStatistics()[x].label, coordinates[0][x] - (ctx.measureText($query.getPatientStatistics()[x].label).width / 2) + 2, coordinates[1][x] - 4);
			}
		}
		ctx.stroke();
	}
		
	draw(canvas) {
		var maxPercent = this.getMaxPercentage();
		var ctx = canvas.getContext("2d");
		ctx.canvas.width = this.height;			
		ctx.canvas.height = this.height;
		
		ctx.fillStyle = "white";
		ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		this.drawGeneralPopulationStar(ctx, this.margin, this.height - this.margin);
		this.drawCohortStar(ctx, this.margin, this.height - this.margin);
		this.drawText(ctx, this.margin, this.height - this.margin);
		
	}
	
}