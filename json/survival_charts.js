	function createSurvivalChart() {
			var survivalChart = document.getElementById("star_chart");
			if (survivalChart === undefined || survivalChart == null) {				
				survivalChart = document.createElement("canvas");
				survivalChart.id = "star_chart";
			}
		
			return survivalChart;
		}

	function drawSurvivalChart(object, cohortData, globalData, title) {
			var timeLabel;
			if (globalData != null) {
				timeLabel = calculateTimeUnit(globalData); 
			} else {
				timeLabel = calculateTimeUnit(cohortData);
			}
			
			var ctx = object.getContext("2d");
			ctx.width = 450;
			ctx.height = 450;
			ctx.canvas.width = 450;
			ctx.canvas.height = 450;
			
			ctx.fillStyle = "white";
			ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

			var startX = 44.5;
			var endX = ctx.width - 20;
			var startY = 60;
			var endY = ctx.height - 43.5;
			
			ctx.font = "bold 16px Arial";
			ctx.fillStyle="#000000";
			
			var start = 20;			
			ctx.fillText("Survival:", 20, 30);
			start += ctx.measureText("Survival: ").width;

			ctx.fillStyle="#afb42b";
			ctx.fillText("Cohort", start, 30);
			start += ctx.measureText("Cohort ").width;
			
			ctx.fillStyle="#a7a7a7";
			ctx.fillText("vs. Population", start, 30);

			ctx.fillText(title, start + ctx.measureText("vs. Population").width + 30, 30);
			
			ctx.moveTo(42, ctx.height - 43.5);
			ctx.lineTo(ctx.width - 20,ctx.height - 43.5);
			ctx.strokeStyle="#a7a7a7";
			ctx.stroke();
			
			ctx.moveTo(45.5, 60);
			ctx.lineTo(45.5,ctx.height - 40);
			ctx.stroke();
			
			ctx.font = "normal 12px Arial";
			
			ctx.save();
			ctx.translate(20, ((ctx.height) / 2) + 40 - (ctx.measureText("% surviving").width / 2));
			ctx.rotate(-Math.PI/2);
			ctx.textAlign = "center";
			ctx.fillText("% surviving", 0, 0);
			ctx.restore();
			
			ctx.fillText("time ("+timeLabel[1]+")", (ctx.width / 2) + 18 - (ctx.measureText("time ("+timeLabel[1]+")").width / 2), 440);
			
			var space = 440 / 7;
			var currentFraction = 0;
			for (x = 0; x < 7; x++) {
				var textLength = ctx.measureText(currentFraction).width;
				ctx.fillText(currentFraction, 45 + (space*x) - (textLength / 2), 425);
				ctx.moveTo(Math.floor(45.5 + (space*x))+0.5, 406);
				ctx.lineTo(Math.floor(45.5 + (space*x))+0.5, 412);
				ctx.stroke();
				currentFraction += timeLabel[0];
			}
			
			space = (ctx.height - 18) / 5;
			var start = ctx.height - 40;
			for (x = 0; x <= 100; x += 25) {
				ctx.fillText(x, 38 - ctx.measureText(x).width, start);
				ctx.moveTo(40, Math.floor(start - 3.5) + 0.5);
				ctx.lineTo(44.5, Math.floor(start - 3.5) + 0.5);
				ctx.stroke();
				start = start - space;
			}

			ctx.beginPath();
			ctx.lineWidth = 2;
			ctx.strokeStyle = "#afb42b";
			ctx.fillStyle = "#afb42b";
			
			ctx.moveTo(startX, startY);
			var prevVal = 1;
			var maxTime = timeLabel[0] * getTimeUnitInDays(timeLabel[1]) * 6;
			for (x = 0; x < cohortData.length; x++) {
				var xPos = startX + (Math.round(cohortData[x][0] / maxTime*(endX - startX)));
				var yPos = endY - (prevVal * (endY - startY));
				ctx.lineTo(xPos, yPos);
				if (cohortData[x][2] != 0 && cohortData[x][2].length != 0) {
					ctx.moveTo(xPos, yPos - 5);
					ctx.lineTo(xPos, yPos + 5);
					ctx.moveTo(xPos, yPos);
				}
				prevVal = cohortData[x][1];
				ctx.lineTo(xPos, endY - (prevVal * (endY - startY)));
			}
			ctx.stroke();
			
			ctx.beginPath();
			ctx.lineWidth = 2;
			ctx.strokeStyle = "#a7a7a7";
			ctx.fillStyle = "#a7a7a7";
			
			ctx.moveTo(startX, startY);
			if (globalData != null) {
				var prevVal = 1;
				var maxTime = timeLabel[0] * getTimeUnitInDays(timeLabel[1]) * 6;
				for (x = 0; x < globalData.length; x++) {
					var xPos = startX + (Math.round(globalData[x][0] / maxTime*(endX - startX)));
					var yPos = endY - (prevVal * (endY - startY));
					ctx.lineTo(xPos, yPos);
					if (globalData[x][2] != 0) {
						ctx.moveTo(xPos, yPos - 5);
						ctx.lineTo(xPos, yPos + 5);
						ctx.moveTo(xPos, yPos);
					}
					prevVal = globalData[x][1];
					ctx.lineTo(xPos, endY - (prevVal * (endY - startY)));
				}
			}
			ctx.stroke();			

		}

		function compileSurvivalData(patientCnt, deaths, censoring) {
			var result = [];
			var deathPos = 0;
			var censoringPos = 0;
			var total = patientCnt;
			var surviving = patientCnt;
			while (deathPos < deaths.length && censoringPos < censoring.length) {
				if (censoringPos < censoring.length && deathPos < deaths.length && deaths[deathPos][0] === censoring[censoringPos][0]) {
					surviving = surviving - deaths[deathPos][1];
					result.push([deaths[deathPos][0], surviving / total, censoring[censoringPos]]);
					deathPos++;
					censoringPos++;
				} else if (censoringPos < censoring.length && deathPos < deaths.length && deaths[deathPos][0] < censoring[censoringPos][0]) {
					while (deaths[deathPos][0] < censoring[censoringPos][0]) {
						surviving = surviving - deaths[deathPos][1];
						result.push([deaths[deathPos][0], surviving / total, 0]);
						deathPos++;
						if (deathPos > deaths.length - 1) {
							break;
						}
					}
				} else if (censoringPos < censoring.length && deathPos < deaths.length && censoringPos < censoring.length && deathPos < deaths.length) {
					while (deaths[deathPos][0] > censoring[censoringPos][0]) {
						result.push([deaths[deathPos][0], surviving / total, censoring[censoringPos]]);
						censoringPos++;
						if (censoringPos > censoring.length - 1) {
							break;
						}
					}					
				}
			}
			for (x = deathPos; x < deaths.length; x++) {
				surviving = surviving - deaths[x][1];
				result.push([deaths[x][0], surviving / total, 0]);
			}
			for (x = censoringPos; x < censoring.length; x++) {
				result.push([censoring[x][0], surviving / total, censoring[x]]);
			}
			return result;
		}
		
		function calculatePopulationSurvival() {
			if ($query.getDefaultStats() != null) {
				var uniqueKeys;
				for (x = 0; x < $query.getDefaultStats().deaths.length; x++) {
					uniqueKeys.push($query.getDefaultStats().deaths[x][0]);
				}
			}
		}
