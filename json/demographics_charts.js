		function drawDemographics(male, female) {
			var c = document.getElementById("demographics_chart");
			c.setAttribute('width', 90);
			c.setAttribute('height', 100);
			var ctx = c.getContext("2d");
			
			//line
			ctx.lineWidth=0.5;
			ctx.moveTo(0,c.height - 40);
			ctx.lineTo(c.width,c.height - 40);
			ctx.stroke();
			
			var offset = 5;
			var middle = (c.width) / 2;
			var left = offset + (middle / 2);
			var right = middle + (middle / 2) - offset;
			
			//text
			ctx.font = "12px Arial";
			ctx.fillStyle = "#4a4a4a";
			ctx.fillText("GENDER",c.width / 2 - (ctx.measureText("GENDER").width / 2),c.height - 2);
			ctx.fillText("M",left - (ctx.measureText("M").width / 2),c.height - 24);
			ctx.fillText("F",right - (ctx.measureText("F").width / 2),c.height - 24);
			
			//bars
			var heightMale = (male / (male + female)) * (c.height-55);
			var heightFemale = (female / (male + female)) * (c.height-55);
			
			ctx.fillStyle="#9b9b9b";
			ctx.fillRect(left - 10,c.height - 40 - heightMale,20,heightMale);
			ctx.fillRect(right - 10,c.height - 40 - heightFemale,20,heightFemale);
			
			ctx.fillText(Math.round(male / (male + female)*100), left - (ctx.measureText(Math.round(male / (male + female)*100)).width / 2),c.height - 42 - heightMale);
			ctx.fillText(Math.round(female / (male + female)*100), right - (ctx.measureText(Math.round(female / (male + female)*100)).width / 2),c.height - 42 - heightFemale);
		}

		function drawDemographicsCohort(id, maleCohort, maleGeneral, femaleCohort, femaleGeneral) {
			var c = document.createElement("canvas");
			c.className = "chart";
			c.id = id;
			c.style.paddingLeft = "5px";
			c.style.paddingRight = "0px";
			c.setAttribute('width', 120);
			c.setAttribute('height', 120);
			var ctx = c.getContext("2d");
			
			//line
			ctx.lineWidth=0.5;
			ctx.moveTo(0,c.height - 40);
			ctx.lineTo(c.width,c.height - 40);
			ctx.stroke();
			
			var offset = 5;
			var middle = (c.width) / 2;
			var left = offset + (middle / 2);
			var right = middle + (middle / 2) - offset;
			
			//text
			ctx.font = "12px Arial";
			ctx.fillStyle = "#4a4a4a";
			ctx.fillText("GENDER",c.width / 2 - (ctx.measureText("GENDER").width / 2),c.height - 2);
			ctx.fillText("M",left - 10 + (ctx.measureText("M").width / 2),c.height - 24);
			ctx.fillText("F",right - 8 + (ctx.measureText("F").width / 2),c.height - 24);
			
			//bars
			var heightMaleCohort = (maleCohort / (maleCohort + femaleCohort)) * (c.height - 55);
			var heightFemaleCohort = (femaleCohort / (maleCohort + femaleCohort)) * (c.height - 55);
			var heightMaleGeneral = (maleGeneral / (maleGeneral + femaleGeneral)) * (c.height - 55);
			var heightFemaleGeneral = (femaleGeneral / (maleGeneral + femaleGeneral)) * (c.height - 55);
			
			ctx.fillStyle="#9b9b9b";
			ctx.fillRect(left,c.height - 40 - heightMaleGeneral,20,heightMaleGeneral);
			ctx.fillRect(right,c.height - 40 - heightFemaleGeneral,20,heightFemaleGeneral);

			ctx.fillStyle="#9b9b9b";
			ctx.fillText(Math.round(maleGeneral / (maleGeneral + femaleGeneral)*100), left + 10 - (ctx.measureText(Math.round(maleGeneral / (maleGeneral + femaleGeneral)*100)).width / 2),c.height - 42 - heightMaleGeneral);
			ctx.fillText(Math.round(femaleGeneral / (maleGeneral + femaleGeneral)*100), right + 10 - (ctx.measureText(Math.round(femaleGeneral / (maleGeneral + femaleGeneral)*100)).width / 2),c.height - 42 - heightFemaleGeneral);
			
			ctx.fillStyle="#ccdd1f";
			ctx.fillRect(left - 20,c.height - 40 - heightMaleCohort,20,heightMaleCohort);
			ctx.fillRect(right - 20,c.height - 40 - heightFemaleCohort,20,heightFemaleCohort);
			
			ctx.fillStyle="#afb42b";
			ctx.fillText(Math.round(maleCohort / (maleCohort + femaleCohort)*100), left - 10 - (ctx.measureText(Math.round(maleCohort / (maleCohort + femaleCohort)*100)).width / 2),c.height - 42 - heightMaleCohort);
			ctx.fillText(Math.round(femaleCohort / (maleCohort + femaleCohort)*100), right - 10 - (ctx.measureText(Math.round(femaleCohort / (maleCohort + femaleCohort)*100)).width / 2),c.height - 42 - heightFemaleCohort);
			
			return c;
		}