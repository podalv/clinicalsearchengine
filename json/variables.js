var currentVariables = [];
var updateAll = false;

var Variables = class {		

	constructor() {
	}
	
	 scanLine(text, line) {
		var pos = text.toLowerCase().indexOf("var ");
		if (pos < 0) {
			this.addVariable(null, line);
			return;
		}
		for (var x = 0; x < pos; x++) {
			if (text.charAt(x) != ' ') {
				this.addVariable(null, line);
				return;
			}
		}
		var pos2 = text.indexOf("=", pos);
		if (pos2 < 0) {
			this.addVariable(null, line);
			return;
		}
		var variable = text.substring(pos+3, pos2).trim();
		this.addVariable(variable, line);		
	} 

	findReferences(editor, variable) {
		variable = variable.toLowerCase();
		var resultCoordinates = [];
		for (var x = 0; x < editor.getSession().getLength(); x++) {
			var line = getLine(x);
			var pos = 0;
			var lineLowerCase = null;
			while (pos >= 0) {
				pos = line.indexOf("$", pos);
				if (pos < 0) {				
					break;
				}
				if (lineLowerCase == null) {
					lineLowerCase = line.toLowerCase();
				}
				if (line.indexOf(variable, pos+1) == pos + 1) {
					resultCoordinates.push(pos);
					resultCoordinates.push(x);
				}
				pos++;
			}
		}
		return resultCoordinates;
	}

	getVariablePosition(editor, variableName) {
		variableName = variableName.toLowerCase();
		for (var x = 0; x < currentVariables.length; x++) {
			if (currentVariables[x] != null && currentVariables[x] == variableName) {
				var text = editor.session.getLine(x);
				var pos = text.toLowerCase().indexOf("var ");
				if (pos < 0) {
					return null;
				}
				for (var z = 0; z < pos; z++) {
					if (text.charAt(z) != ' ') {
						return null;
					}
				}
				var pos2 = text.indexOf("=", pos);
				if (pos2 >= 0) {
					for (var y = pos + 4; y < text.length; y++) {
						if (text.charAt(y) != ' ') {
									var result = [];
									result.push(y);
									result.push(x);
									return result;
						}
					}
				}				
			}
		}
	}

	findVariable(prefix) {
		var result = [];
		for (var x = 0; x < currentVariables.length; x++) {
			if (currentVariables[x] != null && currentVariables[x].startsWith(prefix) && currentVariables[x] !== prefix) {
				result.push(currentVariables[x]);
			}
		}
		return result;
	}

	scanAllLines(editor) {
		currentVariables.length = 0;
		for (var x = 0; x < editor.getSession().getLength(); x++) {
			this.scanLine(getLine(x), x);
		} 
		updateAll = false;
	}

	deleteVariable(line) {
		currentVariables[line] = null;
		for (var x = line; x < currentVariables.length-1; x++) {
			currentVariables[x] = currentVariables[x + 1];
		}
		currentVariables.length = currentVariables.length - 1;
	}

	addRow(line) {
		currentVariables.push(null);
		for (var x = currentVariables.length-2; x >= line; x--) {
			currentVariables[x+1] = currentVariables[x];
		}
		currentVariables[line] = null;
	}
	
	addVariable(variable, line) {
		while (currentVariables.length <= line) {
			currentVariables.push(null);
		}
		if (variable != null) {
			currentVariables[line] = variable.toLowerCase();
		}
	}
}
