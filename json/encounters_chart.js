function drawEncountersChart() {
	
	google.setOnLoadCallback(drawAgeChart);

	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Encounters');
	data.addColumn({type: 'string', role: 'tooltip'});
	data.addColumn('number', 'Cohort');

	var array = [];
	for (var key in response.encounters) {
 		if (response.encounters.hasOwnProperty(key)) {
			array.push([response.encounters[key].label, response.encounters[key].code + " (" + response.encounters[key].label + ")", response.encounters[key].cohortPercentage*100]); 
		}
	}
        
	data.addRows(array);

	var formatter = new google.visualization.NumberFormat(
		{fractionDigits: 2, suffix: '%'});
    	
	formatter.format(data, 2);


	var options = {'title':'Encounters',
                       'width':150,
                       'height':120,
                       'bar': {'groupWidth':"80%"},
                       'legend': { position: "none" },
                       hAxis: {textPosition: 'none'},
                       colors: ['#d51c1c', '#5050ff'],
                       curveType: 'function',
                       focusTarget: 'category',
                       chartArea:{left:10,top:15,width:'100%', height: '100%'}                       
                   };

	var chart = new google.visualization.ColumnChart(document.getElementById('encounters_chart_div'));
	chart.draw(data, options);
}