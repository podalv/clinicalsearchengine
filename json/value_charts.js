		function drawValueChart(object, label, values) {
			var ctx = object.getContext("2d");
			ctx.canvas.width = 463;			
			ctx.canvas.height = 260;
			
			ctx.font = "bold 18px Arial";
			ctx.fillStyle="#616161";
			
			ctx.fillText(label, 20, 30);
			
			var start = 350;
			
			ctx.font = "bold 12px Arial";
			ctx.fillText("%", start, 30);
			start += ctx.measureText("% ").width;
			
			ctx.fillStyle="#ccdd1f";
			ctx.fillText("cohort", start, 30);
			
			ctx.fillStyle="#616161";
			start += ctx.measureText("cohort").width;
			ctx.fillText("/general", start, 30);
			
			ctx.fillStyle="#f7f7f7";
			ctx.fillRect(0, 50, 463, 40);
			
			ctx.fillStyle="#f7f7f7";
			ctx.fillRect(0, 130, 463, 40);

			ctx.fillStyle="#f7f7f7";
			ctx.fillRect(0, 210, 463, 40);
			
			ctx.fillStyle="#616161";
			ctx.font = "normal 12px Arial";
			var lineY = 75;
			
			var cnt = Math.min(5, values.length);
			
			for (x = 0; x < cnt; x++) {
				ctx.fillText(values[x].code, 20, lineY);
				lineY += 40;
			}
			
			ctx.font = "bold 12px Arial";
			lineY = 75;			

			var x = 0;
			while (x < cnt) {
				var text = values[x].label;
				var length = ctx.measureText(text).width;
				if (length + 40 > 310) {
					text = shortenString(ctx, text, 260);
				}
				ctx.fillText(text, 100, lineY);
				lineY += 40;
				x++;
			}

			var maxPercent = 0;
			for (x = 0; x < cnt; x++) {
				maxPercent = Math.max(values[x].generalPercentage, maxPercent);
				maxPercent = Math.max(values[x].cohortPercentage, maxPercent);
			}
			lineY = 60;
			for (x = 0; x < cnt; x++) {
				ctx.fillStyle="#ccdd1f";
				ctx.fillRect(395, lineY, (values[x].cohortPercentage / maxPercent) * 50, 10);
				ctx.fillStyle="#9b9b9b";
				ctx.fillRect(395, lineY+10, (values[x].generalPercentage / maxPercent) * 50, 10);
				ctx.fillStyle="#ccdd1f";
				var str = formatPercentage(values[x].cohortPercentage);
				ctx.fillText(str, 390 - ctx.measureText(str).width, lineY+9);
				ctx.fillStyle="#9b9b9b";
				str = formatPercentage(values[x].generalPercentage);
				ctx.fillText(str, 390 - ctx.measureText(str).width, lineY+20);
				lineY += 40;
			}
			
		}
		
		function formatPercentage(value) {
			if (value == 0) {
				return "0";
			}
			var result = Math.round(value * 100);
			if (result == 0) {
				return "<1";
			}
			return ""+result;
		}
		
		function isItemTextTooWide(canvas, text) {
			var ctx = canvas.getContext("2d");
			return ctx.measureText(text).width + 40 > 310;
		}
		
		function getItemIndex(canvas, mouseXPosition, mouseYPosition) {
			var ctx = canvas.getContext("2d");
			var x = 0;
			var lineY = 75;
			while (x < 5) {
				if (mouseXPosition >= 100 && mouseXPosition <= 350 && mouseYPosition >= (lineY - 20) && mouseYPosition <= (lineY + 10)) {
					return x;
				}
				lineY += 40;
				x++;
			}			
		}

		function getItemMiddlePos(canvas, itemIndex) {
			var lineY = 75;
			return lineY + (itemIndex * 40);
		}
		
		function getValueChartItemLabel(responseHistogram, itemId) {
			if (responseHistogram.length > itemId) {
				return responseHistogram[itemId].label;
			}
			return null;
		}

		
		function addMouseMoveEvent(canvas, data) {
			canvas.onmousemove = function(e) {
				var rect = this.getBoundingClientRect(),
				x = e.clientX - rect.left,
				y = e.clientY - rect.top,
				i = 0, r;
				var itemId = getItemIndex(canvas, x, y);
				var text = getValueChartItemLabel(data, itemId);
				var tooltip = document.getElementById("canvas_tooltip");
				tooltip.onmouseleave = function(e) {
					tooltip.style.visibility = "hidden";
					tooltip.textContent = "";					
				};
				if (isItemTextTooWide(canvas, text)) {
					var scrollTop = document.getElementsByTagName("BODY")[0].scrollTop;
					if (tooltip.textContent !== text) {
						tooltip.textContent = text;
						tooltip.style.visibility = "visible";
						tooltip.style="position:absolute; width: 300px; left:"+(e.clientX+20)+"px; top:"+(scrollTop + Math.min(e.clientY, rect.top + getItemMiddlePos(canvas, itemId)))+"px;";
					}
				} else {
					tooltip.style.visibility = "hidden";
					tooltip.textContent = "";
				}
			};
		}
