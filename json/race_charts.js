		function drawRaceCohort(id, title, whiteCnt, whiteGeneral, whiteLabel, asianCnt, asianGeneral, asianLabel, blackCnt, blackGeneral, blackLabel, otherCnt, otherGeneral, otherLabel) {
			var c = document.createElement("canvas");
			c.className = "chart";
			c.id = id;
			c.style.paddingTop = "40px";
			c.style.paddingLeft = "5px";
			c.style.paddingRight = "0px";
			c.setAttribute('width', 200);
			c.setAttribute('height', 120);
			var ctx = c.getContext("2d");
			
			//line
			ctx.lineWidth=0.5;
			ctx.moveTo(0,c.height - 40);
			ctx.lineTo(c.width,c.height - 40);
			ctx.stroke();
			
			var offset = 12;
			var middle = (c.width) / 2;
			var size = (c.width - (offset * 2)) / 4;
			var white = offset + (size) - 20;
			var asian = offset + (size * 2)- 20;
			var black = offset + (size * 3)- 20;
			var other = offset + (size * 4)- 20;
			
			//text
			ctx.font = "12px Arial";
			ctx.fillStyle = "#4a4a4a";
			ctx.fillText(title,c.width / 2 - (ctx.measureText(title).width / 2),c.height - 2);
			ctx.fillText(whiteLabel,white - (ctx.measureText(whiteLabel).width / 2),c.height - 24);
			ctx.fillText(asianLabel,asian - (ctx.measureText(asianLabel).width / 2),c.height - 24);
			ctx.fillText(blackLabel,black - (ctx.measureText(blackLabel).width / 2),c.height - 24);
			ctx.fillText(otherLabel,other - (ctx.measureText(otherLabel).width / 2),c.height - 24);
			
			//bars
			var total = whiteCnt + asianCnt + blackCnt + otherCnt;
			var totalGeneral = whiteGeneral + asianGeneral + blackGeneral + otherGeneral;
			var heightWhite = (whiteCnt / total) * (c.height - 55);
			var heightAsian = (asianCnt / total) * (c.height - 55);
			var heightBlack = (blackCnt / total) * (c.height - 55);
			var heightOther = (otherCnt / total) * (c.height - 55);
			var heightWhiteGeneral = (whiteGeneral / totalGeneral) * (c.height - 55);
			var heightAsianGeneral = (asianGeneral / totalGeneral) * (c.height - 55);
			var heightBlackGeneral = (blackGeneral / totalGeneral) * (c.height - 55);
			var heightOtherGeneral = (otherGeneral / totalGeneral) * (c.height - 55);
			
			ctx.fillStyle="#9b9b9b";
			ctx.fillRect(white,c.height - 40 - heightWhiteGeneral,20,heightWhiteGeneral);
			ctx.fillRect(asian,c.height - 40 - heightAsianGeneral,20,heightAsianGeneral);
			ctx.fillRect(black,c.height - 40 - heightBlackGeneral,20,heightBlackGeneral);
			ctx.fillRect(other,c.height - 40 - heightOtherGeneral,20,heightOtherGeneral);
			
			ctx.fillStyle="#9b9b9b";
			ctx.fillText(Math.round(whiteGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100), white + 10 - (ctx.measureText(Math.round(whiteGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100)).width / 2),c.height - 42 - heightWhiteGeneral);
			ctx.fillText(Math.round(asianGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100), asian + 10 - (ctx.measureText(Math.round(asianGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100)).width / 2),c.height - 42 - heightAsianGeneral);
			ctx.fillText(Math.round(blackGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100), black + 10 - (ctx.measureText(Math.round(blackGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100)).width / 2),c.height - 42 - heightBlackGeneral);
			ctx.fillText(Math.round(otherGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100), other + 10 - (ctx.measureText(Math.round(otherGeneral / (whiteGeneral + asianGeneral + blackGeneral + otherGeneral)*100)).width / 2),c.height - 42 - heightOtherGeneral);
			
			ctx.fillStyle="#ccdd1f";
			ctx.fillRect(white - 20,c.height - 40 - heightWhite,20,heightWhite);
			ctx.fillRect(asian - 20,c.height - 40 - heightAsian,20,heightAsian);
			ctx.fillRect(black - 20,c.height - 40 - heightBlack,20,heightBlack);
			ctx.fillRect(other - 20,c.height - 40 - heightOther,20,heightOther);
			
			ctx.fillStyle="#afb42b";
			ctx.fillText(Math.round(whiteCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), white - 10 - (ctx.measureText(Math.round(whiteCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightWhite);
			ctx.fillText(Math.round(asianCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), asian - 10 - (ctx.measureText(Math.round(asianCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightAsian);
			ctx.fillText(Math.round(blackCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), black - 10 - (ctx.measureText(Math.round(blackCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightBlack);
			ctx.fillText(Math.round(otherCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), other - 10 - (ctx.measureText(Math.round(otherCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightOther);
			
			return c;
		}
		
		function drawRace(elementId, title, whiteCnt, whiteLabel, asianCnt, asianLabel, blackCnt, blackLabel, otherCnt, otherLabel) {
			var c = document.getElementById(elementId);
			c.setAttribute('width', 200);
			c.setAttribute('height', 100);
			var ctx = c.getContext("2d");
			
			//line
			ctx.lineWidth=0.5;
			ctx.moveTo(0,c.height - 40);
			ctx.lineTo(c.width,c.height - 40);
			ctx.stroke();
			
			var offset = 12;
			var middle = (c.width) / 2;
			var size = (c.width - (offset * 2)) / 4;
			var white = offset + (size) - 20;
			var asian = offset + (size * 2)- 20;
			var black = offset + (size * 3)- 20;
			var other = offset + (size * 4)- 20;
			
			//text
			ctx.font = "12px Arial";
			ctx.fillStyle = "#4a4a4a";
			ctx.fillText(title,c.width / 2 - (ctx.measureText(title).width / 2),c.height - 2);
			ctx.fillText(whiteLabel,white - (ctx.measureText(whiteLabel).width / 2),c.height - 24);
			ctx.fillText(asianLabel,asian - (ctx.measureText(asianLabel).width / 2),c.height - 24);
			ctx.fillText(blackLabel,black - (ctx.measureText(blackLabel).width / 2),c.height - 24);
			ctx.fillText(otherLabel,other - (ctx.measureText(otherLabel).width / 2),c.height - 24);
			
			//bars
			var total = whiteCnt + asianCnt + blackCnt + otherCnt;
			var heightWhite = (whiteCnt / total) * (c.height - 55);
			var heightAsian = (asianCnt / total) * (c.height - 55);
			var heightBlack = (blackCnt / total) * (c.height - 55);
			var heightOther = (otherCnt / total) * (c.height - 55);
			
			ctx.fillStyle="#9b9b9b";
			ctx.fillRect(white - 10,c.height - 40 - heightWhite,20,heightWhite);
			ctx.fillRect(asian - 10,c.height - 40 - heightAsian,20,heightAsian);
			ctx.fillRect(black - 10,c.height - 40 - heightBlack,20,heightBlack);
			ctx.fillRect(other - 10,c.height - 40 - heightOther,20,heightOther);
			
			ctx.fillText(Math.round(whiteCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), white - (ctx.measureText(Math.round(whiteCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightWhite);
			ctx.fillText(Math.round(asianCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), asian - (ctx.measureText(Math.round(asianCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightAsian);
			ctx.fillText(Math.round(blackCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), black - (ctx.measureText(Math.round(blackCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightBlack);
			ctx.fillText(Math.round(otherCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100), other - (ctx.measureText(Math.round(otherCnt / (whiteCnt + asianCnt + blackCnt + otherCnt)*100)).width / 2),c.height - 42 - heightOther);
		}