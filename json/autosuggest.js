var commands = [];
var codes = ["CPT", "ICD9", "ICD10", "RX", "GENDER", "RACE", "ETHNICITY", "SNOMED", "ATC", "DEPARTMENT", "NOTE TYPE", "VISIT TYPE", "TEXT", "STATUS", "ROUTE"];
var codes2 = ["LABS", "LAB", "VITALS"];

/*
DISABLE FOR:
  preceded by +( >( <( -(
  INTERVAL(NUMBER,
  EXTEND(??,
  DURATION(??, ??
  COUNT(??, ??
  DRUG(??,

PRIMARY => ONLY ICD
NOTE( => ONLY NOTE TYPE AND TEXTS

=

*/

function initCommands() {
	if (commands.length == 0) {
		commands.push("CSV");
		commands.push("OUTPUT");
		commands.push("DUMP");
		commands.push("EVENTFLOW");
		commands.push("AND");
		commands.push("OR");
		commands.push("NOT");
		commands.push("INTERSECT");
		commands.push("INVERT");
		commands.push("UNION");
		commands.push("START");
		commands.push("END");
		commands.push("INTERVAL");
// interval if it contains numbers
		commands.push("FIRST MENTION");
		commands.push("LAST MENTION");
		commands.push("FIRST_MENTION");
		commands.push("LAST_MENTION");
		commands.push("FIRSTMENTION");
		commands.push("LASTMENTION");
		commands.push("EXTEND BY");
		commands.push("RESIZE");
//only for the first section
		commands.push("DURATION");
//first 2
		commands.push("COUNT");
//first 2
		commands.push("HISTORY OF");
		commands.push("NO HISTORY OF");
		commands.push("NEVER HAD");
		commands.push("EQUALS");
		commands.push("EQUAL");
		commands.push("CONTAINS");
		commands.push("CONTAIN");
		commands.push("IDENTICAL");
		commands.push("SAME");
		commands.push("DIFF");
		commands.push("MERGE");
		commands.push("PRIMARY");
		commands.push("ORIGINAL");
//only icd9/icd10
		commands.push("DEPARTMENT");
		commands.push("NOTE TYPE");
		commands.push("VISIT TYPE");
		commands.push("DRUG");
//only 1st only RX=
		commands.push("RETURN X INTERSECTING Y");
		commands.push("RETURN X INTERSECTING ALL (Y, Z ...)");
		commands.push("RETURN X INTERSECTING ANY (Y, Z ...)");
		commands.push("RETURN X NOT INTERSECTING Y");
		commands.push("RETURN X NOT INTERSECTING ALL (Y, Z ...)");
		commands.push("RETURN X NOT INTERSECTING ANY (Y, Z ...)");
		commands.push("RETURN X NEVER INTERSECTING Y");
		commands.push("RETURN X NEVER INTERSECTING ALL (Y, Z ...)");
		commands.push("RETURN X NEVER INTERSECTING ANY (Y, Z ...)");
		commands.push("RETURN X ALWAYS INTERSECTING Y");
		commands.push("RETURN X ALWAYS INTERSECTING ALL (Y, Z ...)");
		commands.push("RETURN X ALWAYS INTERSECTING ANY (Y, Z ...)");
		commands.push("BEFORE");
//only first 2
		commands.push("SEQUENCE");
//only first 2
		commands.push("CPT");
		commands.push("ICD9");
		commands.push("ICD10");
		commands.push("RX");
		commands.push("GENDER");
		commands.push("YEAR");
// no
		commands.push("RACE");
		commands.push("ETHNICITY");
		commands.push("DEATH");
		commands.push("DEAD");
		commands.push("SNOMED");
		commands.push("ENCOUNTERS");
		commands.push("NOTE");
		commands.push("NOTES");
//only note and texts
		commands.push("RECORD START");
		commands.push("RECORD END");
		commands.push("AGE");
//no
		commands.push("VITAL");
		commands.push("VITALS");
		commands.push("LABS");
		commands.push("NULL");
		commands.push("ATC");
		commands.push("PATIENTS");
//no
		commands.push("TIMELINE");
		commands.push("FOR EACH");
// () and AS ()
		commands.push("RETURN");
		commands.push("CLEAR");
		commands.push("IF");
		commands.push("IF EMPTY");
		commands.push("IF !EMPTY");
		commands.push("EXIT");
		commands.push("PRINT");
		commands.push("CONTINUE");
		commands.push("FAIL PATIENT");
		commands.push("TEXT");
		commands.sort();

		for (x = 0; x < commands.length; x++) {
			if (commands[x] == "TEXT") {
				commands.splice(x+1, 0, "~TEXT");
				commands.splice(x+1, 0, "!TEXT");
				break;
			}			
		}
	}
}

/*
		commands.push("CPT");
		commands.push("ICD9");
		commands.push("ICD10");
		commands.push("RX");
		commands.push("GENDER");
		commands.push("RACE");
		commands.push("ETHNICITY");
		commands.push("SNOMED");
		commands.push("VITAL");
		commands.push("VITALS");
		commands.push("LABS");
		commands.push("ATC");
		commands.push("DEPARTMENT");
		commands.push("NOTE TYPE");
		commands.push("VISIT TYPE");
		commands.push("DRUG");

		commands.push("VITAL");
		commands.push("VITALS");
		commands.push("LABS");
		commands.push("DRUG");

*/

function isPrecededBy(line, texts, position) {
	for (var i = 0; i < texts.length; i++) {
		pos = line.toUpperCase().lastIndexOf(texts[i].toUpperCase(), position);
		if (pos != -1) {
			fnd = true;
			for (var x = pos + texts[i].length; x < position; x++) {
				if (!(line.charAt(x) == ' ' || line.charAt(x) == '\t')) {
					fnd = false;
					break;
				}
			}
			if (fnd) {
				return true;
			}
		}
	}
	return false;
}

function triggerCodeAutosuggest(key, position, line) {	
	if ((key == '=' && isPrecededBy(line, codes, position - 1)) || (key == '(' && isPrecededBy(line, codes2, position - 1))) {
		return true;		
	}
	return false;
}

function cancelAutosuggest(text, caretPos) {
	containsDot = false;
	for (x = caretPos; x >=0; x--) {
		if (text.charAt(x) == ' ' || text.charAt(x) == '\n' || text.charAt(x) == '=') {
			return false;
		}
		if (text.charAt(x) == '.') {
			containsDot = true;
		}
		if (text.charAt(x) == '$') {
			return caretPos - x < 3 || !containsDot; 
		}
	}
}

function replaceQueryText(clickedCode) {
	if (commandAutosuggestSelected) {
		replaceCommandQueryText(clickedCode);
		if ((getCurrentLine().charAt(editor.getSelectionRange().start.column-1) == '=' && isPrecededBy(getCurrentLine(), codes, editor.getSelectionRange().start.column-2)) || (getCurrentLine().charAt(editor.getSelectionRange().start.column-1) == '(' && isPrecededBy(getCurrentLine(), codes2, editor.getSelectionRange().start.column - 2))) {
			return true;
		}
		return false;
	}
  	var request = {
	    	'selectedCode': clickedCode,
    		'cursor': editor.getSelectionRange().start.column,
	    	'text': getCurrentLine()
    	}
	$.ajax({
       		url : $url + '/autosuggest_replace',
       		type: 'post',
       		dataType: 'json',
       		data: JSON.stringify(request),
       		success: function(response){
       			setCurrentLine(response.text);
       			$('.autosuggest_div').hide();
			enableUpDown();
			editor.selection.moveTo(editor.getSelectionRange().start.row, response.cursor);
       		}
       	});		
	return false;
}

function getCommandHelp(name, arguments, resultType, description, example) {
	if (arguments != null) {
		result = "<span class='command_name'>"+name+"(</span>";
		result = result.concat("<span class='command_argument'>"+arguments+"</span>");
		result = result.concat("<span class='command_name'>)</span><br /><br />");
	} else {
		result = "<span class='command_name'>"+name+"</span><br />";
	}
	if (resultType != null) {
		result = result.concat("<span class='command_type'>Result type&nbsp;</span><span class='command_type_description'>"+resultType+"</span><br /><br />");
	}
	result = result.concat("<span class='command_description'>"+description+"</span><br /><br />");
	if (example != null) {
		result = result.concat("<span class='command_type'>Example</span><br />");
		result = result.concat("<span class='command_description'>"+example+"</span><br />");
	}
	return result;
}

function getCommandHelpArg(name, arguments, afterArguments, resultType, description, example) {
	if (arguments != null) {
		result = "<span class='command_name'>"+name+"(</span>";
		result = result.concat("<span class='command_argument'>"+arguments+"</span>");
		result = result.concat("<span class='command_name'>)</span>");
		result = result.concat("<span class='command_argument'>"+afterArguments+"</span><br /><br />");
	} else {
		result = "<span class='command_name'>"+name+"</span><br />";
	}
	if (resultType != null) {
		result = result.concat("<span class='command_type'>Result type&nbsp;</span><span class='command_type_description'>"+resultType+"</span><br /><br />");
	}
	result = result.concat("<span class='command_description'>"+description+"</span><br /><br />");
	if (example != null) {
		result = result.concat("<span class='command_type'>Example</span><br />");
		result = result.concat("<span class='command_description'>"+example+"</span><br />");
	}
	return result;
}

function getCommandHelpEqualsApostrophes(name, arguments, resultType, description, example) {
	result = "<span class='command_name'>"+name+"=</span>";
	result = result.concat("<span class='command_argument'>"+arguments+"</span><br /><br />");
	if (resultType != null) {
		result = result.concat("<span class='command_type'>Result type&nbsp;</span><span class='command_type_description'>"+resultType+"</span><br /><br />");
	}
	result = result.concat("<span class='command_description'>"+description+"</span><br /><br />");
	if (example != null) {
		result = result.concat("<span class='command_type'>Example</span><br />");
		result = result.concat("<span class='command_description'>"+example+"</span><br />");
	}
	return result;
}

function containsPreviousParameter(line, strings, position, howManyBack) {
	line = line.toUpperCase();
	var pos = position;
	for (y = 0; y < howManyBack; y++) {
		pos = line.lastIndexOf(',', pos);
		if (pos == -1) {
			return false;
		}
		pos--;
	}
	for (x = 0; x < strings.length; x++) {
		prevPos = line.lastIndexOf(strings[x].toUpperCase(), pos);
		if (prevPos != -1) {
			isOk = true;
			for (y = prevPos + strings[x].length; y < pos; y++) {
				if ((line.charAt(y) != ' ') && (line.charAt(y) != ',')) {
					isOk = false;
					break;
				}
			}
			if (isOk) {
				return true;
			}
		}
	}
	return false;
}

function isPreviousParameterNumeric(line, position) {
	commaPosLast = line.lastIndexOf(',', position);
	commaPosFirst = line.lastIndexOf(',', commaPosLast-1);
	if (commaPosLast > 0 && commaPosFirst > 0 && commaPosFirst < commaPosLast) {
		text = line.substring(commaPosFirst+1, commaPosLast).trim().toUpperCase();
		if (text !== "" && (text == "MAX" || text == "MIN" || (text.charAt(0) >= '0' && text.charAt(0) <= '9'))) {
			return true;
		}
	}
	return false;
}

function bucketizeStrings(str1, str2) {
	result = [];
	result.push(str1);
	result.push(str2);
	return result;
}

function getParameterHelp(name, commaCnt) {
	if (name == "AGE" && commaCnt == 0) {
		return getCommandHelp(name, "<b>age_from</b>, age_to", null, "Number for <i>age_from</i> parameter and/or time unit expected", "1 week<br />1 month<br />1 minute");
	} else 	if (name == "DUMP" && commaCnt == 1) {
		return getCommandHelp(name, "cohort, <b>\"named_sql_query\"</b>", null, "Name of the SQL query expected", "DUMP(ICD9=222.2, \"costs\")");
	} else if (name == "AGE" && commaCnt == 1) {
		return getCommandHelp(name, "age_from, <b>age_to</b>", null, "Number for <i>age_to</i> parameter and/or time unit expected", "1 week<br />1 month<br />1 minute");
	} else if (name == "AGE") {
		return getCommandHelp(name, "age_from, age_to", null, "Error! AGE command accepts only two paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else 	if (name == "YEAR" && commaCnt == 0) {
		return getCommandHelp(name, "<b>year_from</b>, year_to", null, "Number for <i>year_from</i> parameter expected", "2015<br />2017");
	} else if (name == "YEAR" && commaCnt == 1) {
		return getCommandHelp(name, "year_from, <b>year_to</b>", null, "Number for <i>year_to</i> parameter expected", "2015<br />2017");
	} else if (name == "YEAR") {
		return getCommandHelp(name, "year_from, year_to", null, "Error! YEAR command accepts only two paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name == "VITAL" || name == "VITALS" && commaCnt == 0) {
		return getCommandHelp(name, "\"<b>measurement name</b>\", value_from(optional), value_to(optional)", null, "Name of vital measurement expected", "\"Temperature\"");		
	} else if (name == "VITAL" || name == "VITALS" && commaCnt == 1) {
		return getCommandHelp(name, "\"measurement name\", <b>value_from</b>, value_to", null, "Numeric parameter expected", "0.1<br />MAX<br />MIN");		
	} else if (name == "VITAL" || name == "VITALS" && commaCnt == 2) {
		return getCommandHelp(name, "\"measurement name\", value_from, <b>value_to</b>", null, "Numeric parameter expected", "0.1<br />MAX<br />MIN");		
	} else if (name == "VITAL" || name == "VITALS") {
		return getCommandHelp(name, "\"measurement name\", value_from, <b>value_to</b>", null, "Error! VITALS command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name.startsWith("PATIENT")) {
		return getCmdHelp("PATIENTS");
	} else if (name == "LAB" || name == "LABS" && commaCnt == 0) {
		return getCommandHelp(name, "\"<b>lab name</b>\", value_from(optional), value_to(optional)", null, "Name of lab measurement expected", "\"2134-5\"");		
	} else if (name == "LAB" || name == "LABS" && commaCnt == 1) {
		return getCommandHelp(name, "\"lab name\", <b>value_from</b>, value_to", null, "Numeric parameter expected", "0.1<br />MAX<br />MIN");		
	} else if (name == "LAB" || name == "LABS" && commaCnt == 2) {
		return getCommandHelp(name, "\"lab name\", value_from, <b>value_to</b>", null, "Numeric parameter expected", "0.1<br />MAX<br />MIN");		
	} else if (name == "LAB" || name == "LABS") {
		return getCommandHelp(name, "\"lab name\", value_from, <b>value_to</b>", null, "Error! LABS command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name == "INTERVAL" && commaCnt == 2) {
		return getCommandHelp(name, "parameter1, parameter2, <b>PAIRS</b>", null, "PAIRS keyword expected", "INTERVAL(ICD9=250.50, ICD9=220.20, PAIRS)");
	} else if (name == "INTERVAL" && commaCnt > 2) {
		return getCommandHelp(name, "parameter1, parameter2, PAIRS", null, "Error! INTERVAL command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if ((name == "EXTEND BY" || name == "RESIZE") && commaCnt > 2) {
		return getCommandHelp(name, "parameter, start, end", null, "Error! RESIZE command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
 	} else if ((name == "EXTEND BY" || name == "RESIZE") && commaCnt == 1) {
		return getCommandHelp(name, "parameter, <b>start</b>, end", null, "Numeric parameter or START/END +/- relation expected", "-10<br />10<br />START+1<br />END-2<br />");
 	} else if ((name == "EXTEND BY" || name == "RESIZE") && commaCnt == 2) {
		return getCommandHelp(name, "parameter, start, <b>end</b>", null, "Numeric parameter or START/END relation exprected", "-10<br />10<br />START+1<br />END-2<br />");
	} else if (name == "DURATION" && commaCnt == 1) {
		if (containsPreviousParameter(getCurrentLine(), bucketizeStrings("ALL", "SINGLE"), editor.getSelectionRange().start.column, 1)) {
			return getCommandHelp(name, "parameter1, <b>type</b>, min, max", null, "<b>SINGLE</b> or <b>ALL</b> keyword expected<br /><b>ALL</b> intervals of <i>parameter1</i> within <i>parameter2</i> must have the specified duration<br /><b>SINGLE</b> of <i>parameter1</i> within <i>parameter2</i> must have the specified duration", null);
		}
	} else if (name == "DURATION" && commaCnt == 2) {
		if (containsPreviousParameter(getCurrentLine(), bucketizeStrings("ALL", "SINGLE"), editor.getSelectionRange().start.column, 1)) {
			return getCommandHelp(name, "parameter1, type, <b>min</b>, max", null, "Numeric parameter or MAX/MIN expected", "10<br />MAX<br />MIN");
		}
		return getCommandHelp(name, "parameter1, parameter2, <b>type</b>, min, max", null, "<b>SINGLE</b> or <b>ALL</b> keyword expected<br /><b>ALL</b> intervals of <i>parameter1</i> within <i>parameter2</i> must have the specified duration<br /><b>SINGLE</b> of <i>parameter1</i> within <i>parameter2</i> must have the specified duration", null);
	} else if (name == "DURATION" && commaCnt == 3) {
		if (containsPreviousParameter(getCurrentLine(), bucketizeStrings("ALL", "SINGLE"), editor.getSelectionRange().start.column, 2)) {
			return getCommandHelp(name, "parameter1, type, min, <b>max</b>", null, "Numeric parameter or MAX/MIN expected", "10<br />MAX<br />MIN");
		}
		return getCommandHelp(name, "parameter1, parameter2, type, <b>min</b>, max", null, "Numeric parameter or MAX/MIN expected", "10<br />MAX<br />MIN");
	} else if (name == "DURATION" && commaCnt == 4) {
		if (containsPreviousParameter(getCurrentLine(), bucketizeStrings("ALL", "SINGLE"), editor.getSelectionRange().start.column, 3)) {
			return getCommandHelp(name, "parameter1, type, min, max", null, "Error! DURATION command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
		}
		return getCommandHelp(name, "parameter1, parameter2, type, min, <b>max</b>", null, "Numeric parameter or MAX/MIN expected", "10<br />MAX<br />MIN");
	} else if (name == "DURATION" && commaCnt > 4) {
		return getCommandHelp(name, "parameter1, parameter2, type, min, max", null, "Error! DURATION command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name == "COUNT" && commaCnt == 1) {
		return getCommandHelp(name, "parameter1, <b>parameter2</b>, type, min, max", null, "For <i>parameter2</i> (or whole patient's timeline if <i>parameter2</i> is omitted) and returns all the intervals of <i>parameter1</i>, which have <i>min</i> <= number of time interval <= <i>max</i><br />Asterisk after a parameter specifies which time interval to return if command is evaluated to TRUE", "COUNT(ICD9=250*, VISIT TYPE=\"INPATIENT\", ALL, MIN, 5)");
	} else if (name == "COUNT" && commaCnt == 2 && !isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column)) {
		return getCommandHelp(name, "parameter1, parameter2, <b>type</b>, min, max", null, "For <i>parameter2</i> (or whole patient's timeline if <i>parameter2</i> is omitted) and returns all the intervals of <i>parameter1</i>, which have <i>min</i> <= number of time interval <= <i>max</i><br />Asterisk after a parameter specifies which time interval to return if command is evaluated to TRUE", "COUNT(ICD9=250, 2, MAX)<br />COUNT(ICD9=250*, VISIT TYPE=\"INPATIENT\", ALL, MIN, 5)");
	} else if (name == "COUNT" && commaCnt == 2 && isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column)) {
		return getCommandHelp(name, "parameter1, min, <b>max</b>", null, "For <i>parameter2</i> (or whole patient's timeline if <i>parameter2</i> is omitted) and returns all the intervals of <i>parameter1</i>, which have <i>min</i> <= number of time interval <= <i>max</i><br />Asterisk after a parameter specifies which time interval to return if command is evaluated to TRUE", "COUNT(ICD9=250, 2, MAX)<br />COUNT(ICD9=250*, VISIT TYPE=\"INPATIENT\", ALL, MIN, 5)");
	} else if (name == "COUNT" && commaCnt == 3 && isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column)) {
		return getCommandHelp(name, "parameter1, min, <b>max</b>", null, "Error! COUNT command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name == "COUNT" && commaCnt == 3 && !isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column)) {
		return getCommandHelp(name, "parameter1, parameter2, type, <b>min</b>, max", null, "Numeric parameter or MAX/MIN expected", "10<br />MAX<br />MIN");
	} else if (name == "COUNT" && commaCnt == 4) {
		return getCommandHelp(name, "parameter1, parameter2, type, min, <b>max</b>", null, "Numeric parameter or MAX/MIN expected", "10<br />MAX<br />MIN");
	} else if (name == "COUNT" && commaCnt > 4) {
		return getCommandHelp(name, "parameter1, parameter2, type, min, max", null, "Error! COUNT command accepts only five paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name == "DRUG" && commaCnt == 1) {
		return getCommandHelp(name, "RX=number, <b>ROUTE=\"route\"</b>, STATUS=\"status\"(optional)", null, "STATUS=\" \" or ROUTE=\" \" parameter expected", null);
	} else if (name == "DRUG" && commaCnt == 2) {
		return getCommandHelp(name, "RX=number, ROUTE=\"route\", <b>STATUS=\"status\"</b>", null, "STATUS=\" \" or ROUTE=\" \" parameter expected", null);
	} else if (name == "DRUG" && commaCnt > 2) {
		return getCommandHelp(name, "RX=number, ROUTE=\"route\", STATUS=\"status\"", null, "Error! "+name+" command accepts only three paramters. " + (commaCnt + 1) + " parameters detected", null);
	} else if (name == "AS") {
		return getCommandHelp("FOR EACH(command) AS(label) {}", null, null, "<br />Label name required", "variable_name<br/>some_text");
	}
	console.log(name + " / " + commaCnt + " / " + isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column));
}

function getCmdHelp(name) {
	if (name == "AND") {
		return getCommandHelp(name, "parameter, parameter ...", "boolean", "Returns TRUE if each of the parameter occurred at some point in patient's timeline", "AND(ICD9=250.50, CPT=22222)");
	} else if (name == "EVENTFLOW") {
		return getCommandHelp(name, "parameter", "N/A", "Evaluates the <i>parameter</i> and outputs all the results in the format compatible with EVENT FLOW", "EVENTFLOW(ICD9=250.50)");
	} else if (name == "OUTPUT") {
		return getCommandHelp(name, "parameter", "N/A", "Evaluates the <i>parameter</i> and outputs all the results with the times when the results occured in patient's timeline in the format: PatientId [TAB] START_TIME [TAB] END_TIME", "OUTPUT(ICD9=250.50)");
	} else if (name == "DUMP") {
		return getCommandHelp(name, "cohort, \"named_sql_query\"", "N/A", "Contacts Database Dump Server registered to this ATLAS instance (if available) and executes the dump of a named SQL query specified in the Database Dump Server for all the patients in the <i>cohort</i> query", "DUMP(ICD9=250.50, \"costs\")");
	} else if (name == "OR") {
		return getCommandHelp(name, "parameter, parameter ...", "boolean", "Returns TRUE if at least one of the parameter occurred at some point in patient's timeline", "OR(RX=161, RX=225)");
	} else if (name == "NOT") {
		return getCommandHelp(name, "parameter", "boolean", "Returns TRUE if the parameter never occurred in patient's timeline", "NOT(ICD9=250.50)");
	} else if (name == "AGE") {
		return getCommandHelp(name, "age_from, age_to", "time intervals", "Returns all the time intervals where <i>age_from</i> <= patient's age <= <i>age_to</i></br>Accepts numeric parameters with specification of time unit (no time unit interprets as minute). The result intervals can be discrete, where gaps in data do not carry age information", "AGE(1 month, 2 years)");
	} else if (name == "YEAR") {
		return getCommandHelp(name, "year_from, year_to", "time intervals", "Returns all the time intervals where <i>year_from</i> <= calendar year <= <i>year_to was</i></br>Accepts numeric parameters. Result intervals can be discrete, where gaps in data do not carry year information", "YEAR(2015, 2016)");
	} else if (name == "PATIENTS" || name == "PATIENT") {
		return getCommandHelp(name, "patient_id, patient_id ...", "time interval", "Returns the timelines of all patients specified by the <i>patient_id</i>", "PATIENTS(1, 2, 3)");
	} else if (name == "VITAL" || name == "VITALS") {
		return getCommandHelp(name, '"measurement name", value_from(optional), value_to(optional)', "time points", "Returns time points of all vitals measurements which <i>value_from <= value <= <i>value_to</i>. If no values are specified, returns all time points when vitals measurements were taken. VITALS without any parameters returns all time points when any vitals measurements were taken", 'VITALS("Temperature", 99.5, MAX)<br />VITALS("Temperature")<br />VITALS');
	} else if (name == "LABS" || name == "LAB") {
		return getCommandHelp(name, '"lab name", value_from(optional), value_to(optional)', "time points", "Returns time points of all labs measurements which <i>value_from</i> <= value <= <i>value_to</i>. If no values are specified, returns all time points when labs were taken. LABS without any parameters returns all time points when any labs were taken", 'LABS("1234-5", 99.5, MAX)<br />LABS("1234-5")<br />LABS');
	} else if (name == "INTERSECT") {
		return getCommandHelp(name, "parameter, parameter ...", "time intervals", "Returns time intervals during which all the <i>parameter</i> time intervals intersected (occurred at the same time). If there is no intersection, returns an empty time interval. INTERSECT is a temporal equivalent of the boolean AND command.", "INTERSECT(RX=161, RX=225)");
	} else if (name == "INVERT") {
		return getCommandHelp(name, "parameter", "time intervals", "Returns all time intervals that did not intersect <i>parameter</i> time intervals. INVERT is a temporal equivalent of the boolean NOT command.", "INVERT(ICD9=250.50)");
	} else if (name == "UNION") {
		return getCommandHelp(name, "parameter, parameter ...", "time intervals", "Returns a set of all time intervals of all <i>parameter</i> features. UNION is a temportal equivalent of the boolean OR command.", "UNION(ICD9=250.50, RX=161)");
	} else if (name == "START") {
		return getCommandHelp(name, "parameter", "time points", "Returns start points of the <i>parameter</i>", "START(ICD9=250.50)");
	} else if (name == "END") {
		return getCommandHelp(name, "parameter", "time points", "Returns end points of the <i>parameter</i>", "END(ICD9=250.50)");
	} else if (name == "INTERVAL") {
		return getCommandHelp(name, "parameter1, parameter2, PAIRS(optional)", "time interval", "Returns all the intervals of <start of <i>parameter1</i>, end of <i>parameter2</i>> where start of <i>parameter1</i> occurs before the end of <i>parameter2</i>.<br />Using <i>PAIRS</i> keyword as the third parameter evaluates command so that first occurence of <i>parameter1</i> and first occurence of <i>parameter2</i> form the first interval, second occurence of <i>parameter1</i> and second occurence of <i>parameter2</i> form the second interval, etc.", "INTERVAL(ICD9=250, CPT=22222)<br />INTERVAL(ICD9=250, CPT=22222, PAIRS)<br />INTERVAL(1 year, 2 years)");
	} else if (isCommand(name, "FIRST", "MENTION", false)) {
		return getCommandHelp(name, "parameter", "time interval", "Returns the full time interval of the first mention of the <i>parameter</i>", "FIRST MENTION(ICD9=250.50)");
	} else if (isCommand(name, "LAST", "MENTION", false)) {
		return getCommandHelp(name, "parameter", "time interval", "Returns the full time interval of the last mention of the <i>parameter</i>", "LAST MENTION(ICD9=250.50)");
	} else if (isCommand(name, "EXTEND", "BY", true) || name == "RESIZE") {
		return getCommandHelp(name, "parameter, start, end", "time intervals", "Resizes each mention of <i>parameter's</i> time intervals.<br />If start and end parameters are numeric, resizes start and end by specified time towards past (negative number) or towards future (positive number).<br />Use of START and END keywords references the actual start and end of <i>parameter</i> time interval.<br />To return a time interval of 1 week after the end of ICD9=250, RESIZE(ICD9=250, END+1, END+1 week) can be used", "RESIZE(ICD9=250.50, -1 day, -1)<br />RESIZE(ICD9=250.50, START-1 day, START-1)<br />RESIZE(ICD9=250.50, END+1, END+1 day)");
	} else if (name == "DURATION") {
		return getCommandHelp(name, "parameter1, parameter2(optional), type, min, max", "time intervals", "For <i>parameter2</i> (or whole patient's timeline if <i>parameter2</i> is omitted) and returns all the intervals of <i>parameter1</>, which have <i>min</i> <= duration <= <i>max</i>.<br /><i>Type</i> SINGLE returns all intervals of <i>parameter1</i> that have the specified duration<br />ALL adds up all the intervals of parameter1 and returns all the intervals if cumulative duration is within min/max parameters<br />If the command has <i>parameter1</i> and <i>parameter2</i>, asterisk after the parameter specifies which feature parameter to return", "DURATION(ICD9=250, SINGLE, 2, MAX)<br />DURATION(ICD9=250*, VISIT TYPE=\"INPATIENT\", ALL, MIN, 5)");
	} else if (name == "COUNT") {
		return getCommandHelp(name, "parameter1, parameter2(optional), type(optional), min, max", "time intervals", "For <i>parameter2</i> (or whole patient's timeline if <i>parameter2</i> is omitted) and returns all the intervals of <i>parameter1</i>, which have <i>min</i> <= number of time interval <= <i>max</i>.<br />SINGLE and ALL <i>types</i> are used only if <i>parameter2</i> is specified<br />SINGLE returns the specified time intervals if a single <i>parameter2</i> time interval contains the specified numbers of <i>parameter1</i> intervals.<br />ALL adds up all the <i>parameter1</i> for all the <i>parameter2</i> intervals and returns if cumulative count is within the min/max parameters<br />If the command has <i>parameter1</i> and <i>parameter2</i>, asterisk after a parameter specifies which feature prameter to return", "COUNT(ICD9=250, 2, MAX)<br />COUNT(ICD9=250*, VISIT TYPE=\"INPATIENT\", ALL, MIN, 5)");
	} else if (name == "HISTORY OF") {
		return getCommandHelp(name, "parameter", "time intervals", "Returns time interval from first mention of <i>parameter</i> to the end of timeline<br /> Equivalent to INTERVAL(START(FIRST MENTION(<i>parameter</i>)), END(TIMELINE))", "HISTORY OF(ICD9=250.50)");
	} else if (name == "NO HISTORY OF") {
		return getCommandHelp(name, "parameter", "time intervals", "Returns time interval before the first mention of <i>parameter</i><br /> Equivalent to INTERVAL(START(TIMELINE), START(FIRST MENTION(<i>parameter</i>)))", "NO HISTORY OF(ICD9=250.50)");
	} else if (name == "NEVER HAD") {
		return getCommandHelp(name, "parameter", "time intervals", "Returns patient's timeline if he never had <i>parameter</i><br /> Equivalent to NOT(<i>parameter</i>)", "NEVER HAD(ICD9=250.50)");
	} else if (name == "EQUALS" || name == "EQUAL") {
		return getCommandHelp(name, "parameter1, parameter2", "time intervals", "Compares all time intervals of <i>parameter1</i> and <i>parameter2</i> and returns time intervals which have same starts and ends in both.<br />Does not perform INTERSECT, but compares all time intervals separately", "EQUALS(ICD9=250.50, ICD9=220.20)");
	} else if (name == "CONTAINS" || name == "CONTAIN") {
		return getCommandHelp(name, "parameter1, parameter2", "time intervals", "Returns time interval of <i>parameter1</i> or <i>parameter2</i> (asterisk after a paramter specifies which parameter to return) for which it is true that time interval <i>parameter1</i> fully contains time interval of <i>parameter2</i>", "CONTAINS(ICD9=250.50, ICD9=20000*)");
	} else if (name == "IDENTICAL") {
		return getCommandHelp(name, "parameter1, parameter2", "time intervals", "Returns all time intervals of <i>parameter1</i> if all of the time intervals are identical to time intervals of <i>paramter2</i>. If there is at least one time interval that is not identical in either <i>parameter1</i> or <i>parameter2</i> empty rime interval is returned", "IDENTICAL(ICD9=250.50, ICD9=20000*)");
	} else if (name == "SAME") {
		return getCommandHelp(name, "parameter1, parameter2", "timeline", "Returns timelines of all patients that have at least one occurrence of <i>parameter1</i> and <i>parameter2</i>", "SAME(ICD9=250.50, ICD9=20000)");
	} else if (name == "DIFF") {
		return getCommandHelp(name, "parameter1, parameter2", "timeline", "Returns timelines of all patients that have at least one occurrence of <i>parameter1</i> but do not contain <i>parameter2</i>", "DIFF(ICD9=250.50, ICD9=20000)");
	} else if (name == "MERGE") {
		return getCommandHelp(name, "parameter1, parameter2", "timeline", "Returns timelines of all patients that have at least one occurrence of <i>parameter1</i> or <i>parameter2</i>", "MERGE(ICD9=250.50, ICD9=20000)");
	} else if (name == "ORIGINAL") {
		return getCommandHelp(name, "icd_code", "time intervals", "<i>icd_code</i> parameter can be only ICD9, ICD10 or ORIGINAL commands. Returns all intervals of <i>icd_code</i> which are not in the dataset as a result of hierarchical expansion<br />Cannot be used with the ORIGINAL(ICD9) or ORIGINAL(ICD10) parameters", "ORIGINAL(ICD9=250.50)");
	}
	} else if (name == "PRIMARY") {
		return getCommandHelp(name, "icd_code", "time intervals", "<i>icd_code</i> parameter can be only ICD9, ICD10 or ORIGINAL commands. Returns all intervals of <i>icd_code</i> which have the PRIMARY flag set to TRUE<br />Cannot be used with the PRIMARY(ICD9) or PRIMARY(ICD10) parameters", "PRIMARY(ICD9=250.50)");
	} else if (name == "DEPARTMENT") {
		return getCommandHelpEqualsApostrophes(name, "\"department_name\"", "time intervals", "Returns all time intervals when patient was at <i>department_name</i>", "DEPARTMENT=\"DEPARTMENT NAME\"");
	} else if (name == "NOTE TYPE") {
		return getCommandHelpEqualsApostrophes(name, "\"note_type\"", "time points", "Returns all time points when patient had a note with the specified <i>note_type</i><br />To return all time points when patient had an occurrence of a term with a specified note type, use the NOTE command instead of INTERSECT", "NOTE TYPE=\"TYPE\"");
	} else if (name == "VISIT TYPE") {
		return getCommandHelpEqualsApostrophes(name, "\"visit_type\"", "time intervals", "Returns all time intervals when patient had a visit with the specified <i>visit_type</i>", "VISIT TYPE=\"TYPE\"");
	} else if (name == "DRUG") {
		return getCommandHelp(name, "RX=number, ROUTE=\"route\"(optional), STATUS=\"status\"(optional)", "time intervals", "Returns all time intervals when had a record of a drug of the specified drug route and the drug status", "DRUG(RX=161, ROUTE=\"oral\", STATUS=\"confirmed\")<br />DRUG(RX=161, STATUS=\"continued\")<br />DRUG(RX=161, ROUTE=\"oral\")");
	} else if (name == "RETURN X INTERSECTING Y") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> that intersect <i>Y</i>", "RETURN ICD9=250.50 INTERSECTING VISIT TYPE=\"INPATIENT\"");
	} else if (name == "RETURN X INTERSECTING ALL (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns a full interval <i>X</i> if it intersects <i>Y</i> and <i>Z</i>, etc.", "RETURN ICD9=250.50 INTERSECTING ALL (RX=161, RX=200, RX=111)");
	} else if (name == "RETURN X INTERSECTING ANY (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns a full interval <i>X</i> if it intersects <i>Y</i> or <i>Z</i>, etc.", "RETURN ICD9=250.50 INTERSECTING ANY (RX=161, RX=200, RX=111)");
	} else if (name == "RETURN X NOT INTERSECTING Y") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> that never intersect <i>Y</i>", "RETURN ICD9=250.50 NOT INTERSECTING VISIT TYPE=\"INPATIENT\"");
	} else if (name == "RETURN X NOT INTERSECTING ANY (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> that never intersect <i>Y</i> or <i>Z</i>", "RETURN ICD9=250.50 NOT INTERSECTING ANY(RX=161, RX=200)");
	} else if (name == "RETURN X NOT INTERSECTING ALL (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> that never intersect the combination <i>Y</i> and <i>Z</i><br />If <i>X</i> intersects <i>Y</i> but not <i>Z</i>, <i>X</i> will be returned", "RETURN ICD9=250.50 NOT INTERSECTING ALL(RX=161, RX=200)");
	} else if (name == "RETURN X NEVER INTERSECTING Y") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> only if they never intersect <i>Y</i> during patients whole timeline.<br />Even a single intersection disqualifies all other intervals", "RETURN ICD9=250.50 NEVER INTERSECTING VISIT TYPE=\"INPATIENT\"");
	} else if (name == "RETURN X NEVER INTERSECTING ALL (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns full interval <i>X</i> if it is true that in patient’s timeline it never intersected the combination of all of <i>Y</i> and <i>Z</i>", "RETURN ICD9=250.50 NEVER INTERSECTING ALL (RX=161, RX=200)");
	} else if (name == "RETURN X NEVER INTERSECTING ANY (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns full interval <i>X</i> if it is true that in patient’s timeline it never intersected any <i>Y</i> or <i>Z</i>", "RETURN ICD9=250.50 NEVER INTERSECTING ANY (RX=161, RX=200)");
	} else if (name == "RETURN X ALWAYS INTERSECTING Y") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> only if they always intersect <i>Y</i> during patients whole timeline.<br />Even a single occurrence of <i>X</i> that did not intersect <i>Y</i> disqualifies all other intervals", "RETURN ICD9=250.50 ALWAYS INTERSECTING VISIT TYPE=\"INPATIENT\"");
	} else if (name == "RETURN X ALWAYS INTERSECTING ALL (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> only if they always intersect a combination of <i>Y</i> and <i>Z</i> during patients whole timeline", "RETURN ICD9=250.50 ALWAYS INTERSECTING ALL (RX=161, RX=200)");
	} else if (name == "RETURN X ALWAYS INTERSECTING ANY (Y, Z ...)") {
		return getCommandHelp(name, null, "time intervals", "Returns all time intervals <i>X</i> only if they always intersect either <i>Y</i> or <i>Z</i> during patients whole timeline", "RETURN ICD9=250.50 ALWAYS INTERSECTING ANY (RX=161, RX=200)");
	} else if (name == "BEFORE" || name == "SEQUENCE") {
		return getCommandHelpArg(name, "parameter1, parameter2", "+-<>(start, end)(optional)", "timeline", "Returns all instances of <i>parameter1</i> which occurred before <i>parameter2</i>. Asterisk after one or both parameters specifies which parameter to return if command evaluates to TRUE<br />Command can be followed by any number of any combinations of (start, end) pairs, which specify a time window relative to <i>parameter2</i>.<br />Positive values of <i>start</i> or <i>end</i> signify time after <i>parameter2</i>, negative numbers specify time before the <i>parameter2</i><br />+ or - specifies that <i>parameter1</i> must be or cannot be located in the specified (start, end) time window<br />&gt; or &lt; specify whether the <i>parameter1</i> must start and/or end in the specified (start, end) time window", "BEFORE(ICD9=250*, ICD9=220)<br />BEFORE(ICD9=250, ICD9=220*)+<>(-1 day, -3 days)-(0, 5 days)");
	} else if (name == "CPT") {
		return getCommandHelpEqualsApostrophes(name, "cpt_code", "time intervals", "Returns all time intervals when patient had specific <i>cpt_code</i>", "CPT=20000");
	} else if (name == "ICD9") {
		return getCommandHelpEqualsApostrophes(name, "icd9_code", "time intervals", "Returns all time intervals when patient had specific <i>icd9_code</i>", "ICD9=250.50");
	} else if (name == "ICD10") {
		return getCommandHelpEqualsApostrophes(name, "icd10_code", "time intervals", "Returns all time intervals when patient had specific <i>icd10_code</i>", "ICD10=A00");
	} else if (name == "RX") {
		return getCommandHelpEqualsApostrophes(name, "rx_norm_code", "time intervals", "Returns all time intervals when patient had specific <i>rx_norm_code</i>", "RX=161");
	} else if (name == "GENDER") {
		return getCommandHelpEqualsApostrophes(name, "\"gender\"", "boolean", "Returns TRUE if patient was of the specified <i>gender</i>", "GENDER=\"FEMALE\"");
	} else if (name == "RACE") {
		return getCommandHelpEqualsApostrophes(name, "\"race\"", "boolean", "Returns TRUE if patient was of the specified <i>race</i>", "RACE=\"HISPANIC\"");
	} else if (name == "ETHNICITY") {
		return getCommandHelpEqualsApostrophes(name, "\"ethnicity\"", "boolean", "Returns TRUE if patient was of the specified <i>ethnicity</i>", "ETHNICITY=\"LATINO\"");
	} else if (name == "DEATH" || name == "DEAD") {
		return getCommandHelp(name, null, "time point", "Returns the time point of patient's death (empty time point if patient did not die)", "DEATH");
	} else if (name == "SNOMED") {
		return getCommandHelpEqualsApostrophes(name, "\"snomed_code\"", "time intervals", "Returns all time intervals when patient had specific <i>snomed_code</i>", "SNOMED=123");
	} else if (name == "ENCOUNTERS") {
		return getCommandHelp(name, null, "time intervals", "Returns the list of all days when patient had at least one encounter", "ENCOUNTERS");
	} else if (name == "NOTE" || name == "NOTES") {
		return getCommandHelp(name, "note_type_command(optional), text_command ...", "time intervals", "Returns the list of all time intervals where the specified combination if <i>text_command</i> parameters occurs within the same note<br />Boolean commands AND, OR, NOT can be used to specify the presence of <i>text_commands</i>", "NOTES(NOTE TYPE=\"clinical\", TEXT=\"diabetes\", TEXT=\"untreated\")<br />NOTES(AND(TEXT=\"diabetes\", OR(TEXT=\"untreated\", !TEXT=\"no treatment\")))");
	} else if (name == "RECORD START" || name == "RECORD_START") {
		return getCommandHelp(name, null, "time point", "Returns the first time point of patient's timeline", "RECORD START");
	} else if (name == "RECORD END" || name == "RECORD_END") {
		return getCommandHelp(name, null, "time point", "Returns the last time point of patient's timeline", "RECORD END");
	} else if (name == "NULL") {
		return getCommandHelp(name, null, "time interval", "Returns an empty time interval<br />Can be used to test for non-existence of time intervals in IDENTICAL, EQUAL, etc. commands", "NULL");
	} else if (name == "SINGLE") {
		return getCommandHelp(name, null, null, "A single time interval must meet the criteria", null);
	} else if (name == "ALL") {
		return getCommandHelp(name, null, null, "All time intervals accumulated together must meet the criteria", null);
	} else if (name == "ATC") {
		return getCommandHelpEqualsApostrophes(name, "\"atc\"", "time intervals", "Returns all time intervals when patient had RX norm codes that are classified the specified <i>atc</i> code", "ATC=\"J02\"");
	} else if (name == "TIMELINE") {
		return getCommandHelp(name, null, "time interval", "Returns patient's whole timeline", "TIMELINE");
	} else if (name == "TEXT") {
		return getCommandHelpEqualsApostrophes(name, "\"term\"", "time points", "Returns all time points when patient had a mention of the <i>term</i> in a note", "TEXT=\"diabetes\"");
	} else if (name == "!TEXT") {
		return getCommandHelpEqualsApostrophes(name, "\"term\"", "time points", "Returns all time points when patient had a negated mention of the <i>term</i> in a note", "!TEXT=\"diabetes\"");
	} else if (name == "~TEXT") {
		return getCommandHelpEqualsApostrophes(name, "\"term\"", "time points", "Returns all time points when patient had a mention of the <i>term</i> in a family history section of a note", "~TEXT=\"diabetes\"");
	} else if (name == "FOR EACH") {
		return getCommandHelp(name, null, "N/A", "Allows iteratively processing each intervals of <i>command</i> separately within the curly brackets.<br />Time intervals of <i>command</i> are available in the FOR EACH context as a reference label <i>label</i>.<br />For each allows nesting multiple FOR EACH commands", "FOR EACH(ICD9=250.50) AS (diabetes) {<br />ABC = INTERSECT(diabetes, CPT=20000);<br />IF EMPTY(ABC) {RETURN ABC AS DEF;}<br />}DEF");
	} else if (name == "RETURN") {
		return getCommandHelp(name, null, "time intervals", "Usable only in a FOR EACH loop<br />Returns a command or a FOR EACH label and stores them in a global context or parent context (if nested)", "FOR EACH(ICD9=250.50) AS (diabetes) {<br />ABC = INTERSECT(diabetes, CPT=20000);<br />IF EMPTY(ABC) {RETURN ABC AS DEF;}<br />}DEF");
	} else if (name == "CLEAR") {
		return getCommandHelp(name, null, "N/A", "Usable only in a FOR EACH loop<br />Clears the contents of a global variable", "FOR EACH (ICD9=250.50) AS (DIABETES) {<br />A = INTERSECT(CPT=250000, DIABETES);<br />IF NOT EMPTY(A) {RETURN B AS GLOBAL_B;}<br />IF EMPTY(B) {CLEAR GLOBAL_B;EXIT;}<br />}");
	} else if (name == "IF") {
		return getCommandHelp(name, null, "N/A", "Usable only in a FOR EACH loop<br />Allows testing for equality of time intervals<br />IF (COMMAND1) == (COMMAND2) {} executes code block if two time intervals are identical<br />IF (COMMAND1) != (COMMAND2) {} executes code block if two time intervals are not identical<br />IF EMPTY(COMMAND1) {} executes code block if time interval of COMMAND1 is empty<br />IF !EMPTY(COMMAND1) {} executes code block if time interval of COMMAND1 is not empty", "IF !EMPTY(ABC){RETURN ABC}<br />IF(ABC) == (DEF){RETURN DEF}");
	} else if (name == "EXIT") {
		return getCommandHelp(name, null, "N/A", "Usable only in a FOR EACH loop<br />Stops evaluation of a FOR EACH loop and exits. Results stored in global variables will not be deleted", "IF !EMPTY(ABC){RETURN ABC}<br />IF(ABC) == (DEF){EXIT;}");
	} else if (name == "CONTINUE") {
		return getCommandHelp(name, null, "N/A", "Usable only in a FOR EACH loop<br />Skips the evaluation of the current iteration of a FOR EACH loop and continues to evaluate the next iteration", "IF !EMPTY(ABC){RETURN ABC}<br />IF(ABC) == (DEF){CONTINUE;}");
	} else if (name == "FAIL PATIENT") {
		return getCommandHelp(name, null, "N/A", "Usable only in a FOR EACH loop<br />Stops the evaluation of the FOR EACH loop and skips the whole patient<br />Regardless of other commands, patient will be skipped and will not appear in the search results", "IF !EMPTY(ABC){RETURN ABC}<br />IF(ABC) == (DEF){FAIL PATIENT;}");
	} else if (name == "PRINT") {
		return getCommandHelp(name, null, "N/A", "Usable only in a FOR EACH loop<br />Prints the time intervals of the specified commands / labels", "IF !EMPTY(ABC){RETURN ABC}<br />IF(ABC) == (DEF){PRINT(\"ABC VAR=\"+DEF);}ABC");
	}
}

function replaceCommandQueryText(text) { 
	offset = 0;
	if (text == "DRUG") {
		text = text.concat("(RX=)");
		offset = -1;
	} else
	if (text == "DEPARTMENT" || text == "NOTE TYPE" || text == "VISIT TYPE" || text == "GENDER" || text == "RACE" || text == "ETHNICITY" || text == "ATC") {
		text = text.concat("=");
	} else if (text == "CPT" || text == "ICD9"|| text == "ICD10" || text == "RX" || text == "SNOMED") {
		text = text.concat("=");
	} else if (text != "ALL" && text != "SINGLE" && text != "PAIRS" && text != "ENCOUNTERS" && text != "DEATH" && text != "DEAD" && text != "RECORD END" && text != "RECORD_END" && text != "RECORD START" && text != "RECORD_START" && text != "TIMELINE" && text != "NULL" && text != "EXIT" && text != "CONTINUE" && text != "FAIL PATIENT") {
		if (text =="FOR EACH") {
			text = "FOR EACH () AS () {}";
			offset = -10;
		} else if (text == "RETURN") {
			text = "RETURN  AS";
			offset = -4;
		} else if (text.startsWith("RETURN X")) {
			text = text.replace("X", "").replace("(Y, ", "(").replace("Z ...", " ").replace("NG Y", "NG");
			offset = 7 - text.length;
		} else {
			text = text.concat("()");
			offset = -1;
		}
	}
	line = getCurrentLine();
	row = editor.getCursorPosition().row;
	column = editor.getCursorPosition().column;
	prefix = getPrefix(getCurrentLine(), column);
	sufix = text;
	if (prefix != null) {
		sufix = text.substring(prefix.length);
	}
	pos = line.toUpperCase().lastIndexOf(prefix.toUpperCase(), column);
	if (prefix.trim() != "") {
		prefix = "";
	}
	if (pos < 0) {
		pre = line.substring(0, column);
		post = line.substring(column+1, line.length);
		line = line.substring(0, column).concat(text).concat(line.substring(column, line.length));
	} else {
		pre = line.substring(0, pos);
		post = line.substring(column+1, line.length);
		line = line.substring(0, pos).concat(prefix).concat(text).concat(line.substring(column, line.length));
	}
	setCurrentLine(line);
	$('.autosuggest_div').hide();
	enableUpDown();
	editor.selection.moveTo(editor.getSelectionRange().start.row, pos + prefix.length + text.length + offset);
	commandAutosuggestSelected = false;
}

function isCommand(command, name1, name2, useOnlySpaces) {
	if (!useOnlySpaces && (command == name1.concat(name2) || command == name1.concat("_").concat(name2))) {
		return true;
	}
	pos1 = command.indexOf(name1);
	pos2 = command.indexOf(name2);
	if (pos1 == 0 && pos2 != -1) {
		if (command.substring(name1.length, pos2).trim() == "") {
			return true;
		}
	}
	return false;
}

function getCommands(prefix, parentCommand) {
	prefix = prefix.toUpperCase().trim();
	initCommands();
	var result = [];
	for (var x = 0; x < commands.length; x++) {
		if (commands[x].startsWith(prefix)) {
			result.push(commands[x]);
		}
	}
	if (parentCommand != null) {
		if (parentCommand.command == "DURATION" && (!containsPreviousParameter(getCurrentLine(), bucketizeStrings("ALL", "SINGLE"), editor.getSelectionRange().start.column, 1))) {
			if (parentCommand.commaCount == 1) {
				result.splice(0, 0, "SINGLE", "ALL");
			} else if (parentCommand.commaCount == 2) {
				result = [];
				result.push("SINGLE");
				result.push("ALL");
			}
			prefix = " ";
		}
		if (parentCommand.command == "COUNT" && (!isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column))) {
			if (parentCommand.commaCount == 2) {
				result = [];
				result.push("SINGLE");
				result.push("ALL");
			}
			prefix = " ";
		}
		if (parentCommand.command == "ORIGINAL") {
			result = [];
			result.push("ICD9");
			result.push("ICD10");
			result.push("PRIMARY");
		 	prefix = " ";
		}
		if (parentCommand.command == "PRIMARY") {
			result = [];
			result.push("ICD9");
			result.push("ICD10");
			result.push("ORIGINAL");
		 	prefix = " ";
		}
		if (parentCommand.command.startsWith("NOTE")) {
			console.log("HERE !!!");
			result = [];
			result.push("AND");
			result.push("OR");
			result.push("NOT");
			result.push("NOTE TYPE");
			result.push("TEXT");
			result.push("!TEXT");
			result.push("~TEXT");
		 	prefix = " ";			
		}
	}
	if (prefix == "") {
		return commands;
	}
	return result;
}

function getPrefix(line, position) {
	var start = 0;
	var lastChar = "";
	var lastNonSpacePosition = start;
	for (var x = position-1; x >= 0; x--) {
		if (line.charAt(x) == ',' || line.charAt(x) == '(' || line.charAt(x) == ')' || line.charAt(x) == '=') {
			if (line.charAt(x) == '=') {
				lastChar = line.charAt(x);
			}
			start = x+1;
			break;
		}
		if (line.charAt(x) != ' ') {
			lastNonSpacePosition = x;
		}
	}
	if (start == 0) {
		start = lastNonSpacePosition;
	}
	return line.substring(start, position) + lastChar;
}

function extractCommand(command, pos, commaCount) {
	var result = null;
	for (var y = pos; y >=0; y--) {
		if (command.charAt(y) == ',' || command.charAt(y) == ')' || command.charAt(y) == '(' || command.charAt(y) == '=') {
			result = new Object();
			result.command = command.substring(y+1, pos+1).trim().toUpperCase();
			result.commaCount = commaCount;
			result.startPos = y+1;
			break;
		}
	}
	if (result == null) {
		result = new Object();
		result.command = command.substring(0, pos+1).trim().toUpperCase();
		result.commaCount = commaCount;
		result.startPos = 0;
	}
	return result;
}

function isText(command, pos) {
	var isInParenthesis = false;
	for (var x = 0; x < pos; x++) {
		if (command.charAt(x) == '“' || command.charAt(x) == '"' || command.charAt(x) == '”' || command.charAt(x) == '”') {
			isInParenthesis = !isInParenthesis;
		}
	}
	return isInParenthesis;
}

function getParentCommand(command, pos) {
	var bracketStatus = 0;
	var commaCount = 0;
	var isInParenthesis = isText(command, pos);
	for (var x = pos; x >= 0; x--) {
		if (command.charAt(x) == '“' || command.charAt(x) == '"' || command.charAt(x) == '”' || command.charAt(x) == '”') {
			isInParenthesis = !isInParenthesis;
		}
		if (!isInParenthesis && command.charAt(x) == '(') {
			bracketStatus++;
		} else if (!isInParenthesis && command.charAt(x) == ')') {
			bracketStatus--;
		}
		if (!isInParenthesis && bracketStatus == 0 && command.charAt(x) == ',') {
			commaCount++;
		}
		if (bracketStatus > 0) {
			return extractCommand(command, x-1, commaCount);
		}
	}
	return null;
}

function showCommandAutosuggest(parentCommand) {
	if (parentCommand != null) {
		if ((parentCommand.command == "DUMP") && (parentCommand.commaCount > 0)) {
			return false;
		}
		if ((parentCommand.command == "COUNT") && (parentCommand.commaCount > 0)) {
			return false;
		}
		if ((parentCommand.command == "AS")) {
			return false;
		}
		if ((parentCommand.command == "COUNT") && (parentCommand.commaCount > 1)) {
			if (!isPreviousParameterNumeric(getCurrentLine(), editor.getSelectionRange().start.column) && parentCommand.commaCount == 2) {
				return true;
			}
			return false;
		}
		if ((parentCommand.command == "DURATION") && (parentCommand.commaCount > 2 || containsPreviousParameter(getCurrentLine(), bucketizeStrings("ALL", "SINGLE"), editor.getSelectionRange().start.column, 1))) {
			return false;
		}
		if ((parentCommand.command == "INTERVAL" || parentCommand.command == "SAME" || parentCommand.command == "DIFF" || parentCommand.command == "EQUAL" || parentCommand.command == "EQUALS" || parentCommand.command == "EXTEND BY"
		      || parentCommand.command == "RESIZE" || parentCommand.command == "CONTAIN" || parentCommand.command == "CONTAINS" || parentCommand.command == "IDENTICAL" || parentCommand.command == "MERGE" || parentCommand.command == "BEFORE"
                      || parentCommand.command == "SEQUENCE") && parentCommand.commaCount > 1) {
			return false;
		}
		if ((parentCommand.command == "NOT" || parentCommand.command == "INVERT" || parentCommand.command == "START" || parentCommand.command == "END" || parentCommand.startsWith == "FIRST" || parentCommand.startsWith == "LAST" ||
                     parentCommand.command == "HISTORY OF" || parentCommand.command == "NO HISTORY OF" || parentCommand.command == "NEVER HAD" || parentCommand.command == "PRIMARY" || parentCommand.command == "ORIGINAL") && parentCommand.commaCount > 0) {
			return false;
		}
		if (parentCommand.command != "DRUG" && parentCommand.command != "YEAR" && parentCommand.command != "AGE" && !parentCommand.command.startsWith("VITAL") && !parentCommand.command.startsWith("PATIENT") && !parentCommand.command.startsWith("LAB") && 
	            !parentCommand.command.startsWith('-') && !parentCommand.command.startsWith('+') && !parentCommand.command.startsWith('<') && !parentCommand.command.startsWith('>') && !parentCommand.command.startsWith('*')) {
			return true;
		}
	}
	return parentCommand == null;
}

function commandAlreadyExists(selectedCommands, line, pos) {
	possibleCommands = [];
	if (selectedCommands != null) {
		for (x = 0; x < selectedCommands.length; x++) {
			commandPos = line.toUpperCase().lastIndexOf(selectedCommands[x], pos);
			if (commandPos != -1 && (commandPos + selectedCommands[x].length >= pos)) {
				possibleCommands.push(selectedCommands[x]);
			}
		}
	}
	return possibleCommands;
}

function showAutosuggestHelp() {
	if (commandAutosuggestSelected) {
		var caret = editor.getCursorPosition();
		var p = $( "#query_box" );
		var position = p.offset();
		leftPos = position.left + 20 + (caret.column * 9) + $("#autosuggest_item_"+$selectedAutosuggestItemId).position().left + $("#autosuggest_item_"+$selectedAutosuggestItemId).width();
		$('.autosuggest_help').show();
		$('.autosuggest_help').css({width: 400});
		$('.autosuggest_help').css({height: 200});
        	if (leftPos + $('.autosuggest_help').width() > $(window).width()) {
	       		leftPos = $(window).width() - $('.autosuggest_help').width() - 30;
	        }
		topPos = getEditorTop(caret.row) + 20;
		if ($('.autosuggest_div').is(":visible")) {
			topPos = $('.autosuggest_div').offset().top;
		} else {
			if (topPos + $('.autosuggest_help').height() > $(window).height()) {
				topPos = getEditorTop(caret.row) - 20 - $('.autosuggest_help').height();
			}
		}
	       	$('.autosuggest_help').css({marginLeft: "10px", top: topPos, left: leftPos});
		$('.autosuggest_help_inner').html("");
	       	$('.autosuggest_help_inner').html(getCmdHelp($("#autosuggest_item_"+$selectedAutosuggestItemId).html()));
		$('.autosuggest_help').css({height: $('.autosuggest_help_inner').height() + 20});
		return;
	}
}

function commandAutosuggest(command, pos) {
	var parentCommand = getParentCommand(command, pos-1);
	if (parentCommand != null) {
		console.log("PARENT COMMAND = " + parentCommand.command);
	} else {
		console.log("PARENT COMMAND = " + parentCommand);
	}
	var caret = editor.getCursorPosition();
	var p = $( "#query_box" );
	var position = p.offset();
	if (!showCommandAutosuggest(parentCommand)) {
		if (parentCommand != null) {
			if (parentCommand.command == "INTERVAL" && parentCommand.commaCount == 2) {
				replaceCommandQueryText("PAIRS");
				return;
			}
		}
		$('.autosuggest_help').show();
		$('.autosuggest_help').css({width: 400});
		$('.autosuggest_help').css({height: 200});
		leftPos = position.left + 20 + (caret.column * 9);
        	if (leftPos + $('.autosuggest_help').width() > $(window).width()) {
	       		leftPos = $(window).width() - $('.autosuggest_help').width() - 30;
	        }
		topPos = getEditorTop(caret.row) + 20;
		if (topPos + $('.autosuggest_help').height() > $(window).height()) {
			topPos = getEditorTop(caret.row) - 20 - $('.autosuggest_help').height();
		}
	       	$('.autosuggest_help').css({marginLeft: "10px", top: topPos, left: leftPos});
		$('.autosuggest_help_inner').html("");
	       	$('.autosuggest_help_inner').html(getParameterHelp(parentCommand.command, parentCommand.commaCount));
		$('.autosuggest_help').css({height: $('.autosuggest_help_inner').height() + 20});
		return;
	}
	initCommands();
	selectedCommands = getCommands(getPrefix(command, pos), parentCommand);
	if (selectedCommands == null || selectedCommands.length == 0) {
		return;
	}
	possibleCommands = commandAlreadyExists(selectedCommands, command, pos);
	if (possibleCommands.length == 1) {
		$('.autosuggest_help').show();
		$('.autosuggest_help').css({width: 400});
		$('.autosuggest_help').css({height: 200});
		leftPos = position.left + 20 + (caret.column * 9);
		topPos = getEditorTop(caret.row) + 20;
		if (topPos + $('.autosuggest_help').height() > $(window).height()) {
			topPos = getEditorTop(caret.row) - 20 - $('.autosuggest_help').height();
		}
        	if (leftPos + $('.autosuggest_help').width() > $(window).width()) {
	       		leftPos = $(window).width() - $('.autosuggest_help').width() - 30;
	        }

	       	$('.autosuggest_help').css({marginLeft: "10px", top: topPos, left: leftPos});
		$('.autosuggest_help_inner').html("");
	       	$('.autosuggest_help_inner').html(getCmdHelp(possibleCommands[0]));
		$('.autosuggest_help').css({height: $('.autosuggest_help_inner').height() + 20});
		return;
	}
	$('#autosuggest_list').remove();
	$('.autosuggest_text').remove();
	$('.autosuggest_div').show();
	var caret = editor.getCursorPosition();
       	$('.autosuggest_div').css({marginLeft: "10px", top: getEditorTop(caret.row) + 20, left: position.left + 20 + (caret.column * 9)});
       	var items = "";
       	$selectedAutosuggestItemId = 0;
       	for (x = 0; x < selectedCommands.length; x++) {
		var itemId = 'autosuggest_item_' + x;
       		items = items.concat("<li class='autosuggest_list_item' id="+itemId+">"+selectedCommands[x]+"</li>");
       	}        						
       	var list = "<ul id='autosuggest_list' style='list-style-type: none;padding-left:5px;margin-top:5px;margin-bottom:2px;'>" + items + "</ul>";
       	$('.autosuggest_div').append(list);
	$('#autosuggest_list').css({height: 200});
	$('#autosuggest_list').css({width: 330});
	$('.autosuggest_div').css({width: 330});
	$('#autosuggest_list').css({'list-style-position': 'inside'});
	var list = document.getElementById("autosuggest_list").getElementsByTagName("li");
	height = 0;
	for (x = 0; x < list.length; x++) {
		height += list[x].offsetHeight;
	}
	if (height < 200) {
		$('.autosuggest_div').css({height: height+10});
		$('#autosuggest_list').css({height: height});
	} else {
		$('.autosuggest_div').css({height: $('#autosuggest_list').height()+25});
	}
	$('.autosuggest_div').css({'overflow-x': 'hidden'});
	$('.autosuggest_div').css({'overflow-y': 'scroll'});
	$(".autosuggest_list_item").hover(function(){
		$("#autosuggest_item_"+$selectedAutosuggestItemId).css("background-color", "white");
    		var selected_id = $(this).attr('id');
    		$selectedAutosuggestItemId = parseInt(selected_id.substring(selected_id.lastIndexOf('_') + 1));
    		$("#autosuggest_item_"+$selectedAutosuggestItemId).css("background-color", "lightblue");
    	}, function(){
    		$("#autosuggest_item_"+$selectedAutosuggestItemId).css("background-color", "lightblue");
	});
	var maxHeight = Math.min($( window ).height() - 30, $('.autosuggest_div').position().top + $('.autosuggest_div').height());        						
        if ($('.autosuggest_div').position().left + $('.autosuggest_div').width() + $('.autosuggest_help').width() > $(window).width()) {
        	$('.autosuggest_div').css({left: $(window).width() - $('.autosuggest_div').width() - $('.autosuggest_help').width() - 30});
	       	$('.autosuggest_help').css({left: $('.autosuggest_div').width() + $('.autosuggest_div').position.left});
        }
        if (maxHeight != $('.autosuggest_div').position().top + $('.autosuggest_div').height()) {
        	maxHeight = maxHeight - $('.autosuggest_div').position().top;
		var top = $('.autosuggest_div').position().top - $('.autosuggest_div').height() - 30;
        	$('.autosuggest_div').css({top: top});         							
        }  
	commandAutosuggestSelected = true;
	showAutosuggestHelp();
	disableUpDown();
	return true;
}
