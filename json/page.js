var Page = class {		

	constructor() {
	}
	
	createStarChart() {
		var starChart = document.getElementById("star_chart");
		if (starChart === undefined || starChart == null) {				
			starChart = document.createElement("canvas");
			starChart.id = "star_chart";
		}
		return starChart;
	}

	createCohortCharts() {
		var cohortCharts = document.getElementById("cohort_charts");
		if (cohortCharts === undefined || cohortCharts == null) {
			cohortCharts = document.createElement("div");
			cohortCharts.id = "cohort_charts";
		} else {
			document.getElementById("query_results_div").removeChild(document.getElementById("cohort_charts"));
			cohortCharts = document.createElement("div");
			cohortCharts.id = "cohort_charts";				
		}
		
		return cohortCharts;
	}
		
	createValueChart(className) {
		var canvas = document.createElement("canvas");
		canvas.className = className;			
		return canvas;
	}

	createResultsDiv() {
		document.getElementById("bottom_div").style.top = "664px";
		document.getElementById("patient_cnt").textContent="226 patients";
		document.getElementById("main_div").style.backgroundColor = "#eeeeee";

		var resultsDiv;
		if (document.getElementById("info_bar") !== undefined && document.getElementById("info_bar") !== null) {
			$("#info_bar").hide();
			document.getElementById("main_div").removeChild(document.getElementById("info_bar"));
			resultsDiv = document.createElement("div");
			resultsDiv.id = "query_results_div";
		} else {
			resultsDiv = document.getElementById("query_results_div");
		}
			
		document.getElementById("query_time_text").style.visibility = "visible";
						
		return resultsDiv;
	}
		
	hideCohortDescriptionTop() {
		var cohortDescription1 = document.getElementById("cohort_description_top");
		cohortDescription1.style.backgroundColor = "#eeeeee";
		document.getElementById("cohort_demographics_chart").style.visibility = "hidden";
		document.getElementById("cohort_race_chart").style.visibility = "hidden";
		document.getElementById("cohort_ages_chart").style.visibility = "hidden";
		document.getElementById("cohort_description_top").style.height = "20px";
		cohortDescription1.style.marginBottom = "0px";
		cohortDescription1.style.marginTop = "0px";
		document.getElementById("cohort_description_bottom").marginTop = "0px";
		document.getElementById("cohort_description_top").style.boxShadow = "none";
	}

	showCohortDescriptionTop() {
		var cohortDescription1 = document.getElementById("cohort_description_top");
		cohortDescription1.style.backgroundColor = "white";
		document.getElementById("cohort_demographics_chart").style.visibility = "visible";
		document.getElementById("cohort_race_chart").style.visibility = "visible";
		document.getElementById("cohort_ages_chart").style.visibility = "visible";
		document.getElementById("cohort_description_top").style.height = "160px";
		document.getElementById("cohort_description_top").style.boxShadow = "2px 2px 2px #888888";
		cohortDescription1.style.marginBottom = "10px";
		cohortDescription1.style.marginTop = "19px";
	}
		
	createCohortDemographics() {
		console.log($query.getExportLocation());
		var cohortDemographics = document.getElementById("cohort_demographics");
		if (cohortDemographics === undefined || cohortDemographics == null) {				
			cohortDemographics = document.createElement("div");
			cohortDemographics.id = "cohort_demographics";
			var downloadButton = document.createElement("a");
			downloadButton.setAttribute("id", "downloadButtonTarget");
			downloadButton.href = "#";
			if ($query.getExportLocation() !== null && typeof ($query.getExportLocation()) !== "undefined") {
				downloadButton.href=$query.getExportLocation();
				if (!$query.getExportLocation().startsWith("/dump_status/")) {
					downloadButton.download = $query.getExportLocation().substring($query.getExportLocation().indexOf('/')+1);
				}
			} else {
				downloadButton.onclick = function() {
					savePidFile();
					editor.focus();
					return false;
				}
			}
			downloadButton.innerHTML ="<span id='download_button'>DOWNLOAD DATASET</span>";
			cohortDemographics.appendChild(downloadButton);
		} else {
			var downloadButton = document.getElementById("downloadButtonTarget");
			if ($query.getExportLocation() !== null && typeof ($query.getExportLocation()) !== "undefined") {
				downloadButton.onclick = null;
				downloadButton.href=$query.getExportLocation();
				if (!$query.getExportLocation().startsWith("/dump_status/")) {
					downloadButton.download = $query.getExportLocation().substring($query.getExportLocation().indexOf('/')+1)
				}
			} else {
				downloadButton.onclick = function() {
					savePidFile();
					editor.focus();
					return false;
				}
			}
		}
		
		var panel = document.getElementById("a_top_panel");
		if (panel !== undefined && panel != null) {
			while (panel.firstChild) {
				var child = panel.firstChild;
			    panel.removeChild(child);
			    //delete child;
			}
			panel.parentNode.removeChild(panel);
			//delete panel;
		}
		panel = document.getElementById("a_bottom_panel");
		if (panel !== undefined && panel != null) {
			while (panel.firstChild) {
				var child = panel.firstChild;
			    panel.removeChild(child);
			    //delete child;
			}
			panel.parentNode.removeChild(panel);
			//delete panel;
		}
		
		panel = document.getElementById("cohort_description_top");
		if (panel !== undefined && panel != null) {
			while (panel.firstChild) {
				var child = panel.firstChild;
			    panel.removeChild(child);
			    //delete child;
			}
			panel.parentNode.removeChild(panel);
			//delete panel;
		} 

		panel = document.getElementById("cohort_description_bottom");
		if (panel !== undefined && panel != null) {
			while (panel.firstChild) {
				var child = panel.firstChild;
			    panel.removeChild(child);
			   // delete child;
			}
			panel.parentNode.removeChild(panel);
			//delete panel;
		} 

		cohortDemographics.appendChild(this.createCohortDemographicsDescription($query.getPrevalentGender(), $query.getPrevalentRace(), $query.getPrevalentAge()));
		cohortDemographics.appendChild(this.createCohortTimeDescription($query.getPrevalentEncounters(), $query.getPrevalentDurations()));
		
		return cohortDemographics;
	}		

	
	createCohortDemographicsDescription(gender, race1, race2) {
		var ahref = document.createElement("a");
		ahref.href = "#";
		ahref.id = "a_top_panel";
		var cohortDescription1 = document.createElement("div");
		cohortDescription1.className = "cohort_description";
		cohortDescription1.id = "cohort_description_top";
		cohortDescription1.style.height = "160px";
		cohortDescription1.style.backgroundColor = "#eeeeee";

		ahref.onclick = function(e) {
			if (cohortDescription1.style.backgroundColor == "white") {
				$page.hideCohortDescriptionTop();
			} else {
				$page.showCohortDescriptionTop();
			}
			editor.focus();
			return false;
		};
						
		var cohortDescriptionLabelPrefix = document.createElement("span");
		cohortDescriptionLabelPrefix.className = "cohort_description_label_start";
		cohortDescriptionLabelPrefix.textContent = "Cohort is";
			
		var cohortDescriptionLabelValue1 = document.createElement("span");
		cohortDescriptionLabelValue1.className = "cohort_description_label_bold";
		cohortDescriptionLabelValue1.style.paddingRight = "0px";
		cohortDescriptionLabelValue1.textContent = gender;

		var cohortDescriptionLabelComma = document.createElement("span");
		cohortDescriptionLabelComma.className = "cohort_description_label_normal_no_padding";
		cohortDescriptionLabelComma.style.paddingLeft = "0px";
		if (race1.toLowerCase() !== "unknown" && race1.toLowerCase() !== "other") {
			cohortDescriptionLabelComma.textContent = ",";
		} else {
			cohortDescriptionLabelComma.textContent = "";
		}
			
		var cohortDescriptionLabelValue2 = document.createElement("span");
		cohortDescriptionLabelValue2.className = "cohort_description_label_bold";
		if (race1.toLowerCase() !== "unknown" && race1.toLowerCase() !== "other") {
			cohortDescriptionLabelValue2.textContent = race1;
		} else {
			cohortDescriptionLabelValue2.textContent = "";
		}
	
		var cohortDescriptionLabelAnd = document.createElement("span");
		cohortDescriptionLabelAnd.className = "cohort_description_label_normal";
		cohortDescriptionLabelAnd.textContent = "and";

		var cohortDescriptionLabelValue3 = document.createElement("span");
		cohortDescriptionLabelValue3.className = "cohort_description_label_bold";
		if (race1.toLowerCase() !== "unknown" && race1.toLowerCase() !== "other") {
			cohortDescriptionLabelValue3.textContent = race2;
		} else {
			cohortDescriptionLabelValue3.textContent = race2+"\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0";
		}

		cohortDescription1.appendChild(cohortDescriptionLabelPrefix);
		cohortDescription1.appendChild(cohortDescriptionLabelValue1);
		cohortDescription1.appendChild(cohortDescriptionLabelComma);
		cohortDescription1.appendChild(cohortDescriptionLabelValue2);
		cohortDescription1.appendChild(cohortDescriptionLabelAnd);
		cohortDescription1.appendChild(cohortDescriptionLabelValue3);
		cohortDescription1.appendChild(drawDemographicsCohort("cohort_demographics_chart", $query.getGenderCnt(maleString, true), $query.getGenderCnt(maleString, false), $query.getGenderCnt(femaleString, true), $query.getGenderCnt(femaleString, false)));
		cohortDescription1.appendChild(drawRaceCohort("cohort_race_chart", "RACE", $query.getRaceCnt(whiteString, true), $query.getRaceCnt(whiteString, false), "White", $query.getRaceCnt(asianString, true), $query.getRaceCnt(asianString, false), "Asian", $query.getRaceCnt(blackString, true), $query.getRaceCnt(blackString, false), "Black", $query.getRaceOtherCnt([whiteString, blackString, asianString], true), $query.getRaceOtherCnt([whiteString, blackString, asianString], false), "Other"));
		var globalAgeBuckets = $query.getAges();
		var cohortAgeBuckets = $query.bucketizeAges(true);
		cohortDescription1.appendChild(drawRaceCohort("cohort_ages_chart", "AGE", cohortAgeBuckets[0], globalAgeBuckets[0], ageLabels[0], cohortAgeBuckets[1], globalAgeBuckets[1], ageLabels[1], cohortAgeBuckets[2], globalAgeBuckets[2], ageLabels[2], cohortAgeBuckets[3], globalAgeBuckets[3], ageLabels[3]));
			
		ahref.appendChild(cohortDescription1);
		
		return ahref;
	}

	hideCohortDescriptionBottom() {
		var cohortDescription1 = document.getElementById("cohort_description_bottom");
		cohortDescription1.style.backgroundColor = "#eeeeee";
		document.getElementById("cohort_encounters_chart").style.visibility = "hidden";
		document.getElementById("cohort_durations_chart").style.visibility = "hidden";
		document.getElementById("cohort_description_bottom").style.height = "20px";
		document.getElementById("cohort_description_bottom").style.boxShadow = "none";
		document.getElementById("cohort_description_bottom").style.marginTop = "0px";
		document.getElementById("cohort_description_bottom").style.marginBottom = "0px";
	}

	showCohortDescriptionBottom() {
		var cohortDescription1 = document.getElementById("cohort_description_bottom");
		cohortDescription1.style.backgroundColor = "white";
		document.getElementById("cohort_encounters_chart").style.visibility = "visible";
		document.getElementById("cohort_durations_chart").style.visibility = "visible";
		document.getElementById("cohort_description_bottom").style.height = "160px";
		document.getElementById("cohort_description_bottom").style.boxShadow = "2px 2px 2px #888888";
		document.getElementById("cohort_description_bottom").style.marginTop = "18px";
		document.getElementById("cohort_description_bottom").style.marginBottom = "10px";
	}
		
	createCohortTimeDescription(encounters, durations) {
		var ahref = document.createElement("a");
		ahref.href = "#";
		ahref.id = "a_bottom_panel";
		var cohortDescription1 = document.createElement("div");
		cohortDescription1.className = "cohort_description";
		cohortDescription1.id = "cohort_description_bottom";
		cohortDescription1.style.height = "160px";
		cohortDescription1.style.backgroundColor = "#eeeeee";
			
		ahref.onclick = function(e) {
			if (cohortDescription1.style.backgroundColor == "white") {
				$page.hideCohortDescriptionBottom();
			} else {
				$page.showCohortDescriptionBottom();
			}
			editor.focus();
			return false;
		};
			
		var cohortDescriptionLabelPrefix = document.createElement("span");
		cohortDescriptionLabelPrefix.className = "cohort_description_label_start";
		cohortDescriptionLabelPrefix.textContent = "Mostly";
		
		var cohortDescriptionLabelValue1 = document.createElement("span");
		cohortDescriptionLabelValue1.className = "cohort_description_label_bold";
		cohortDescriptionLabelValue1.style.paddingRight = "0px";
		cohortDescriptionLabelValue1.textContent = encounters;

		var cohortDescriptionLabelComma = document.createElement("span");
		cohortDescriptionLabelComma.className = "cohort_description_label_normal_no_padding";
		cohortDescriptionLabelComma.style.paddingLeft = "0px";
		cohortDescriptionLabelComma.textContent = ",";
			
		var cohortDescriptionLabelAnd = document.createElement("span");
		cohortDescriptionLabelAnd.className = "cohort_description_label_normal";
		cohortDescriptionLabelAnd.textContent = "spanning";

		var cohortDescriptionLabelValue3 = document.createElement("span");
		cohortDescriptionLabelValue3.className = "cohort_description_label_bold";
		cohortDescriptionLabelValue3.textContent = durations;

		cohortDescription1.appendChild(cohortDescriptionLabelPrefix);
		cohortDescription1.appendChild(cohortDescriptionLabelValue1);
		cohortDescription1.appendChild(cohortDescriptionLabelComma);
		cohortDescription1.appendChild(cohortDescriptionLabelAnd);
		cohortDescription1.appendChild(cohortDescriptionLabelValue3);
		var global = bucketizeValues($query.getEncountersGeneral(), encounterRanges, $query.getPatientCntGeneral(), 1);
		var cohort = bucketizeValuesSingle($query.getEncountersCohort(), $query.getPatientCntCohort());
		cohortDescription1.appendChild(drawRaceCohort("cohort_encounters_chart", "ENCOUNTERS", cohort[0], global[0], encounterLabels[0], cohort[1], global[1], encounterLabels[1], cohort[2], global[2], encounterLabels[2], cohort[3], global[3], encounterLabels[3]));
		global = bucketizeValues($query.getDurationsGeneral(), durationRanges, $query.getPatientCntGeneral(), 30);
		cohort = bucketizeValuesSingle($query.getDurationsCohort(), $query.getPatientCntCohort());
		cohortDescription1.appendChild(drawRaceCohort("cohort_durations_chart", "DURATION (YEARS)", cohort[0], global[0], durationLabels[0], cohort[1], global[1], durationLabels[1], cohort[2], global[2], durationLabels[2], cohort[3], global[3], durationLabels[3]));
		
		ahref.appendChild(cohortDescription1);
			
		return ahref;
	}
		
	minimize() {
		if ($query.hasResponse() != null) {
			$('#query_button').toggle();
			$('#maximize_button').toggle();
			$('#query_box').toggle();
			this.resize();
		}
	}

	normalSize() {
		$('#maximize_button').toggle();
		$('#minimize_button').toggle();
		$('#normal_size_button').toggle();
		this.resize();			
	}

	maximize() {
		$('#maximize_button').toggle();
		$('#minimize_button').toggle();
		$('#normal_size_button').toggle();
		this.resize();	
	}
		
	drawDefaultPage() {
		$('.autosuggest_div').hide();
		$('.autosuggest_help').hide();
		updatePopulationDescription($query.getPatientCntGeneral(), $query.getEncounterCntGeneral());
		var maleCnt = $query.getGenderPercentage(maleString);
		var femaleCnt = $query.getGenderPercentage(femaleString);
		drawDemographics(maleCnt, femaleCnt - (100 - femaleCnt - maleCnt));
		var whiteCnt = $query.getRacePercentage(whiteString);
		var asianCnt = $query.getRacePercentage(asianString);
		var blackCnt = $query.getRacePercentage(blackString);
		var otherCnt = 100 - whiteCnt - asianCnt - blackCnt;
		var ages = $query.getAges();
		drawRace('race_chart', 'RACE', whiteCnt, 'White', asianCnt, 'Asian', blackCnt, 'Black', otherCnt, 'Other');
		drawRace('age_chart', 'AGE', ages[0], '18', ages[1], '19-44', ages[2], '45-64', ages[3], '65+');
		$('#minimize_button').toggle();
		$('#maximize_button').toggle();
		$('#normal_size_button').toggle();
		editor.focus();
	}

	drawQueryPage() {
		$('.autosuggest_div').hide();
		$('.autosuggest_help').hide();
		if ($('#maximize_button').is(':visible') == false && $('#minimize_button').is(':visible') == false &&
		    $('#normal_size_button').is(':visible') == false) {
				$('#minimize_button').toggle();
				$('#maximize_button').toggle();
		}
		
		this.displayQueryError();
		
		var resultsDiv = this.createResultsDiv();
				
		if ($query.containsSurvivalData()) {
			resultsDiv.appendChild(createSurvivalChart());
		} else {
			var starChart = this.createStarChart();
			resultsDiv.appendChild(starChart);
		}
		
		resultsDiv.appendChild(this.createCohortDemographics());
		resultsDiv.appendChild(this.createCohortCharts());
						
		document.getElementById("main_div").insertBefore(resultsDiv, document.getElementById("bottom_div"));

		this.updatePatientCnt();
					
		var cohortCharts = document.getElementById("cohort_charts");
		var icd9 = this.createValueChart("value_chart_left");
		var cpt = this.createValueChart("value_chart_right");
		var rx = this.createValueChart("value_chart_left");
		var labs = this.createValueChart("value_chart_right");
			
		addMouseMoveEvent(icd9, $query.getIcd9Statistics());
		addMouseMoveEvent(cpt, $query.getCptStatistics());
		addMouseMoveEvent(rx, $query.getRxStatistics());
			
		document.getElementById("query_results_div").onscroll = function(e) {
			document.getElementById("canvas_tooltip").style.visibility = "hidden";
		}

		document.onscroll = function(e) {
			document.getElementById("canvas_tooltip").style.visibility = "hidden";
		}
			
		cohortCharts.appendChild(icd9);
		cohortCharts.appendChild(cpt);
		cohortCharts.appendChild(rx);
		cohortCharts.appendChild(labs);
			
		drawValueChart(icd9, "ICD", $query.getIcd9Statistics());
		drawValueChart(cpt, "CPT", $query.getCptStatistics());
		drawValueChart(rx, "DRUGS", $query.getRxStatistics());
		drawValueChart(labs, "LABS", $query.getLabsStatistics());
			
		this.hideCohortDescriptionTop();
		this.hideCohortDescriptionBottom();

		var title = document.getElementById("query_box").value;
		if (!$query.containsSurvivalData()) {
			new StarChart(document.getElementById("star_chart"));
		} else {
			drawSurvivalChart(document.getElementById("star_chart"), $query.getCohortSurvival(), null, title.substring(title.indexOf("(")+1, title.length - 1));
		}
			
		this.resize();
		//saveCanvasToDisk(document.getElementById("star_chart"), title);
		editor.focus();
		if ($query.getPatientCntCohort() == 0) {
			console.log("HIDE");
			$('#query_results_div').show().hide();
		} else {
			$('#query_results_div').show();
		}
	}		
		
	
	
	resize() {
		$('.autosuggest_div').hide();
		var size = window.innerHeight;
		var availableSize = size - $('#query_bar').height() - $('#top_bar').height();
		if ($('#maximize_button').is(':visible') == false && $('#query_button').is(':visible')) {
			document.getElementById("query_results_div").style.height = 0;
			document.getElementById("bottom_div").style.height = availableSize;
			$("#bottom_div").css('top', $('#top_bar').height()+'px');			
		} else {
			if ($('#query_button').is(':visible')) {
				if (availableSize < 300) {
					document.getElementById("query_results_div").style.height = 0;
					document.getElementById("bottom_div").style.height = availableSize;
					$("#bottom_div").css('top', $('#top_bar').height()+'px');
				} else {
					$('#query_results_div').css('height', availableSize * 0.6);
					document.getElementById("bottom_div").style.height = (availableSize * 0.3);
					$("#bottom_div").css('top', ($('#query_results_div').height() + 5 + $('#query_bar').height())+'px');
				}
			} else {
				availableSize = size - $('#top_bar').height() - $('#query_bar').height();
				$('#query_results_div').css('height', availableSize-10);
				document.getElementById("bottom_div").style.height = (50);
				$("#bottom_div").css('top', ($('#query_results_div').height() + 5 + $('#query_bar').height())+'px');								
			}
		}
		editor.resize();
		editor.focus();
	}
	
	queryBoxEmpty() {
		var queryText = editor.getValue();
		return (queryText.trim() == '' || queryText == $queryBoxDefaultText);
	}

	updatePatientCnt() {
		if ($query.getSkippedPatientCnt() == 0) {
			document.getElementById("query_time_text").textContent = ($query.getTimeTook() / 1000) + " s";
		} else {
			document.getElementById("query_time_text").textContent = ($query.getTimeTook() / 1000) + " s (" + numeral($query.getSkippedPatientCnt()).format('0,0') + " patients skipped)";
		}
		document.getElementById("patient_cnt").textContent = numeral($query.getPatientCntCohort()).format('0,0') + " patients";
	}
		
	displayQueryError() {
		if (($query.getErrorMessage() !== null) && typeof($query.getErrorMessage()) !== "undefined") {
			displayError($query.getErrorMessage());
		} else if ($query.getWarningMessage() !== null && typeof($query.getWarningMessage()) !== "undefined") {
			displayWarning($query.getWarningMessage());
		}
	}

}
