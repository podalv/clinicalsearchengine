var Query = class {

	constructor(url) {
		this.url = url;
		this.finished = false;
		this.getDefaultStats();
	}

	queryStatus(requestData, check) {
		var object = this;
		try {
			document.getElementById("patient_cnt").textContent = "Querying...";
			document.getElementById("query_time_text").textContent = "";
			$.ajax({
				url : this.url + '/query',
				type: 'post',
				dataType: 'json',
				data: requestData,
				done: function(response){
					if (!check) {
						object.finished = true;
						if (response.consoleOutput.length != 0) {
							for (var x = 0; x < response.consoleOutput.length; x++) {
								console.log(response.consoleOutput[x]);
							}
						}
						object.response = response;
						$page.drawQueryPage(response);
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
						editor.focus();
					} else {
						if (document.getElementById("patient_cnt").textContent = "Querying..." || document.getElementById("patient_cnt").textContent.startsWith("Processed")) {
							document.getElementById("patient_cnt").textContent = response.errorMessage;
						}
					}
				},
				success: function(response){
					if (!check) {
						object.finished = true;
						if (response.consoleOutput.length != 0) {
							for (var x = 0; x < response.consoleOutput.length; x++) {
								console.log(response.consoleOutput[x]);
							}
						}
						object.response = response;
						$page.drawQueryPage(response);
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
						editor.focus();
					} else {
						if (document.getElementById("patient_cnt").textContent = "Querying..." || document.getElementById("patient_cnt").textContent.startsWith("Processed")) {
							document.getElementById("patient_cnt").textContent = response.errorMessage;
						}
					}
				}, 
				fail: function() {
					if (!check) {
						object.finished = true;
						displayShortError("Error querying server");
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
					}
				}
			});
		} catch (err) {
			object.finished = true;
			displayError(err.message);
			$("#query_button").removeClass("query_progress_button");
			$("#query_button").addClass("query_button");
		}
	}
	
	sendQuery(query, returnSurvivalData) {
		$("#query_button").removeClass("query_button");
		$("#query_button").addClass("query_progress_button");		
		var object = this;
		var request = {
	    	'query': query,
		'checkStatus': false,
	    	'returnSurvivalData': returnSurvivalData,
	    	'pidCntLimit': 10000,
	    	'encounterBuckets': encounterRanges,
	    	'durationBuckets': durationRanges
	    }
		var requestStatus = {
	    	'query': query,
		'checkStatus': true,
	    	'returnSurvivalData': returnSurvivalData,
	    	'pidCntLimit': 10000,
	    	'encounterBuckets': encounterRanges,
	    	'durationBuckets': durationRanges
	    }
	    
	    var requestData = JSON.stringify(request);
	    var requestStatusData = JSON.stringify(requestStatus);
	    object.finished = false;
	    console.log(object.finished);
	    var timer = setInterval(function () {
		console.log(object.finished);
	    	if (!object.finished) {
				object.queryStatus(requestStatusData, true);
	    	} else {
	    		console.log("Done");
	    		clearInterval(timer);
	    	}
	    }, 2000);    
	    this.queryStatus(requestData, false);
	}
		
	getUrl() {
		return this.url;
	}
	
	getTimeTook() {
		return this.response.timeTook;
	}
	
	getErrorMessage() {
		return this.response.errorMessage;
	}

	getWarningMessage() {
		return this.response.warningMessage;
	}

	getPrevalentGender() {
		var maxVal = 0;
		var maxLabel = "unknown";
		for (x = 0; x < this.response.genderHistogram.length; x++) {
			if (typeof this.response.genderHistogram[x] !== 'undefined' && this.response.genderHistogram[x].cohortPercentage > maxVal) {
				maxVal = this.response.genderHistogram[x].cohortPercentage;
				maxLabel = this.response.genderHistogram[x].label;
			}
		}
		return maxLabel.toLowerCase();
	}

	getPrevalentRace() {
		var maxVal = 0;
		var maxLabel = "other";
		for (x = 0; x < this.response.raceHistogram.length; x++) {
			if (typeof this.response.raceHistogram[x] !== 'undefined' && this.response.raceHistogram[x].cohortPercentage > maxVal) {
				maxVal = this.response.raceHistogram[x].cohortPercentage;
				maxLabel = this.response.raceHistogram[x].label;
			}
		}
		return maxLabel.toLowerCase();
	}

	getPrevalentAge() {		
		var ageBuckets = this.bucketizeAgesGeneral(this.response, true);
		var maxVal = 0;
		var maxLabel = "?";
		for (x = 0; x < ageBuckets.length; x++) {
			if (ageBuckets[x] > maxVal) {
				maxVal = ageBuckets[x];
				maxLabel = ageLabels[x];
			}
		}
		return maxLabel + " years old";
	}
		
	getPatientIds() {
		return this.response.patientIds;
	}
	
	getParsedQuery() {
		return this.response.parsedQuery;
	}
	
	bucketizeAges(useCohortData) {
		return this.bucketizeAgesGeneral(this.response, useCohortData);
	}
	
	containsSurvivalData() {
		return (this.response.deaths != null && typeof this.response.deaths !== 'undefined' && this.response.deaths.length != 0) ||
			   (this.response.censored != null && typeof this.response.censored !== 'undefined' && this.response.censored.length != 0);
	}
	
	bucketizeAgesGeneral(resp, useCohortData) {
		var result = [0, 0, 0, 0];
		for (x = 0; x < resp.ages.length; x++) {
			if (x >= ageRanges[ageRanges.length - 1]) {
				if (useCohortData) {
					result[result.length - 1] = result[result.length - 1] + (resp.ages[x].cohortPercentage * 100);
				} else {
					result[result.length - 1] = result[result.length - 1] + (resp.ages[x].generalPercentage * 100);
				}
			} else {
				for (var y = 0; y < ageRanges.length; y++) {
					if (ageRanges[y] >= x) {
						if (useCohortData) {
							result[y] = result[y] + (resp.ages[x].cohortPercentage * 100);
						} else {
							result[y] = result[y] + (resp.ages[x].generalPercentage * 100);
						}
						break;
					}
				}
			}
		}
		var total = 0;
		for (x = 0; x < result.length; x++) {
			result[x] = Math.round(result[x]);
			total = total + result[x];
		}
		result[result.length - 1] = result[result.length - 1] - (total - 100);
		return result;
	}
	
	getPrevalentEncounters() {
		var buckets = this.response.encounters;
		var maxVal = 0;
		var maxLabel = "?";
		for (x = 0; x < buckets.length; x++) {
			if (buckets[x] > maxVal) {
				maxVal = buckets[x];
				maxLabel = encounterLabels[x];
			}
		}
		return maxLabel + " encounters";			
	}

	getPrevalentDurations() {
		var buckets = this.response.durations;
		var maxVal = 0;
		var maxLabel = "?";
		for (x = 0; x < buckets.length; x++) {
			if (buckets[x] > maxVal) {
				maxVal = buckets[x];
				maxLabel = durationLabels[x];
			}
		}
		return maxLabel + " years";
	}

	getGenderCnt(label, cohort) {
		return this.getDemographicCnt(this.response.genderHistogram, label, cohort);
	}

	getDemographicCnt(array, label, cohort) {
		for (x = 0; x < array.length; x++) {
			if (array[x].label.toLowerCase() === label.toLowerCase()) {
				if (cohort) {
					return array[x].cohortPercentage * 100;
				} else {
					return array[x].generalPercentage * 100;
				}
			}
		}
		return 0;		
	}
	
	getRaceCnt(label, cohort) {
		return this.getDemographicCnt(this.response.raceHistogram, label, cohort);
	}

	getRaceOtherCnt(array, labels, cohort) {
		return this.getDemographicsOtherCnt(this.response.raceHistogram, labels, cohort);
	}
	
	getDemographicsOtherCnt(array, labels, cohort) {
		var otherCnt = 0;
		for (x = 0; x < array.length; x++) {
			var fnd = false;
			for (var y = 0; y < labels.length; y++) {
				if (array[x].label.toLowerCase() === labels[y].toLowerCase()) {
					if (cohort) {
						otherCnt = otherCnt + (array[x].cohortPercentage * 100);
					} else {
						otherCnt = otherCnt + (array[x].generalPercentage * 100);
					}
				}
			}
		}
		return 100 - otherCnt;
	}
			
	getPatientCntGeneral() {
		return this.defaultStats.patientCnt;
	}

	getSkippedPatientCnt() {
		return this.response.skippedPatientCnt;
	}

	getPatientCntCohort() {
		return this.response.cohortPatientCnt;
	}
	
	getEncounterCntGeneral() {
		return this.defaultStats.encounterCnt;
	}

	getEncountersGeneral() {
		return this.defaultStats.encounters;
	}

	hasResponse() {
		return this.response != null && typeof this.response !== 'undefined' && typeof this.response.patientStatistics !== 'undefined' && this.response.patientStatistics.length != 0;
	}
	
	getPatientStatistics() {
		return this.response.patientStatistics;
	}
	
	getIcd9Statistics() {
		return this.response.icd9Statistics;
	} 

	getCptStatistics() {
		return this.response.cptStatistics;
	} 

	getRxStatistics() {
		return this.response.rxStatistics;
	} 

	getLabsStatistics() {
		return this.response.labsStatistics;
	} 

	
	getExportLocation() {
		return this.response.exportLocation;
	}
	
	getEncountersCohort() {
		return this.response.encounters;
	}
	
	getDurationsGeneral() {
		return this.defaultStats.durations;
	}

	getDurationsCohort() {
		return this.response.durations;
	}

	getCohortSurvival() {
		return compileSurvivalData(this.response.cohortPatientCnt, this.response.deaths, this.response.censored);
	}
	
	getPopulationSurvival() {
		return this.populationSurvival;
	}
	
	getDefaultStats() {
		var object = this;
		$.ajax({
   			url : this.url + '/statistics',
   			type: 'post',
   			dataType: 'json',
   			async: false,
   			done: function(response){
   				object.updateDefaultStats(response);
   			},
   			success: function(response){
   				object.updateDefaultStats(response);
   			}, 
   			fail: function(response){
   				displayShortError("Error querying server"); 
   			},
		});
	}
		
	updateDefaultStats(response) {
		this.defaultStats = response;
		this.populationSurvival = compileSurvivalData(this.defaultStats.patientCnt, this.defaultStats.deaths, this.defaultStats.censored);
	}
		
	query(returnSurvivalData) {
		if ($page.queryBoxEmpty()) {
			displayShortError("There is nothing to query");
		} else {
			this.sendQuery(editor.getValue(), returnSurvivalData);
		}
	}

	getGenderPercentage(label) {
		if (this.defaultStats != null) {
			for (x = 0; x < this.defaultStats.gender.length; x++) {
				if (this.defaultStats.gender[x].label.toLowerCase() === label) {
					return Math.round(this.defaultStats.gender[x].generalPercentage*100);
				}
			}
		}
		return 0;
	}

	getRacePercentage(label) {
		if (this.defaultStats != null) {
			for (x = 0; x < this.defaultStats.race.length; x++) {
				if (this.defaultStats.race[x].label.toLowerCase() === label) {
					return Math.round(this.defaultStats.race[x].generalPercentage*100);
				}
			}
		}
		return 0;
	}

	getAges() {
		if (this.defaultStats != null) {
			return this.bucketizeAgesGeneral(this.defaultStats, false);
		}
		return [0, 0, 0, 0];
	}

	getVariables() {
		$.ajax({
				url : this.url + '/script_names',
				type: 'post',
				dataType: 'json',
				async: false,
				done: function(response){
					setScriptNames(response);
				},
				success: function(response){
					setScriptNames(response);
				}, 
				fail: function(response){
					displayShortError("Error querying server");
				},
			});
	}
	
	getVariableName(element, name) {
		if (element.childElementCount == 1) {
			var request = {
					'scriptName': name 
		    	};
			$.ajax({
				url : this.url + '/script_content',
				type: 'post',
				dataType: 'json',
				async: false,
				data: JSON.stringify(request),
				done: function(response){
					updateVariableContent(element, response.script);
				},
				success: function(response){
					updateVariableContent(element, response.script);
				}, 
				fail: function(response){
					displayShortError("Error querying server");
				},
			});
		} else {
			updateVariableContent(element, "");
		}
	}
	
	startWorkshop() {
		window.location.replace(this.url + "/workshop");
	}
	
	getWorkshop() {
			$.ajax({
				url : this.url + '/status',
				type: 'post',
				dataType: 'json',
				async: false,
				done: function(response){
					if (response.workshop) {
						$query.startWorkshop();
					}
				},
				success: function(response){
					if (response.workshop) {
						$query.startWorkshop();
					}
				}, 
				fail: function(response){
					displayShortError("Explorer is not available");
				},
			});
	}

	getVariableNameToGlobalVariable(name) {
		var request = {
				'scriptName': name 
	    	};
		$.ajax({
			url : this.url + '/script_content',
			type: 'post',
			dataType: 'json',
			async: true,
			data: JSON.stringify(request),
			done: function(response){
				updateVariableContentVar(response.script);
			},
			success: function(response){
				updateVariableContentVar(response.script);
			}, 
			fail: function(response){
				displayShortError("Error querying server");
			},
		});
	}

}
