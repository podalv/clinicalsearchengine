		function formatNumber(nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		function updatePopulationDescription(patientCnt, encounterCnt) {
			document.getElementById("global_patient_cnt").textContent = formatNumber(patientCnt);
			document.getElementById("global_encounter_cnt").textContent = formatNumber(encounterCnt);
		}

		function shortenString(canvas, text, maxLen) {
			for (x = 0; x < text.length; x++) {
				result = text.substring(0, text.length - x);
				if (canvas.measureText(result+"...").width <= maxLen) {
					return result+"...";
				}
			}
			return "...";
		}

		function saveCanvasToDisk(canvas, fileName) {
			var dataURL = canvas.toDataURL( "image/png" );
			var data = atob( dataURL.substring( "data:image/png;base64,".length ) ),
			    asArray = new Uint8Array(data.length);

			for( var i = 0, len = data.length; i < len; ++i ) {
			    asArray[i] = data.charCodeAt(i);    
			}

			var blob = new Blob( [ asArray.buffer ], {type: "image/png"} );
			saveAs(blob, fileName + ".png");
		}
		
		function getTimeUnitInDays(timeUnit) {
			if (timeUnit == "years") {
				return 365;
			} else if (timeUnit == "months") {
				return 30;
			} else if (timeUnit == "weeks") {
				return 7;
			}
			return 1;
		}
		
		function calculateTimeUnit(cohortData) {
			var fraction = Math.round((cohortData[cohortData.length - 1][0] / 365) / 5);
			if (fraction > 0 && (fraction*6*365) >= cohortData[cohortData.length - 1][0] / 365) {
				return [fraction, "years"];
			}
			fraction = Math.round((cohortData[cohortData.length - 1][0] / 30) / 5);
			if (fraction > 0 && (fraction*6*30) >= cohortData[cohortData.length - 1][0] / 30) {
				return [fraction, "months"];
			}
			fraction = Math.round((cohortData[cohortData.length - 1][0] / 7) / 5);
			if (fraction > 0 && (fraction*6*7) >= cohortData[cohortData.length - 1][0] / 7) {
				return [fraction, "weeks"];
			}
			return [Math.round(cohortData[cohortData.length - 1][0] / 5), "days"];
		}
	
		function clearDefaultText(){
			var c = document.getElementById("query_box");
			if (c.value == $queryBoxDefaultText) {
				c.value = "";
			}
		}
		
		function setDefaultText() {
			var c = document.getElementById("query_box");
			if (c.value == "") {
				c.value = $queryBoxDefaultText;
			}
		}

		function displayWarning(text) {
			toastr.remove();
			toastr.options.positionClass = "toast-top-full-width";
			var message = "";
			if (typeof text == 'object') {
				for (x = 0; x < text.length; x++) {
					message = message.concat(String(text[x]));
					if (x != text.length - 1) {
						message = message.concat("<br />");
					}
				}
			} else {
				message = text;
			}
			if (message.trim() != "") {
				toastr.warning(message);
			}
		}

		function displayError(text) {
			console.log(text);		
			toastr.remove();
   			toastr.options.positionClass = "toast-top-full-width";
			var message = "";
			if (typeof text == 'object') {
				for (x = 0; x < text.length; x++) {
					message = message.concat(String(text[x]));
					if (x != text.length - 1) {
						message = message.concat("<br />");
					}
				}
			} else {
				message = text;
			}
			if (message.trim() != "") {
				toastr.error(message);		
			}
			removeMarkers();
			if (message.startsWith("[")) {
				pos = message.indexOf(":");
				left = message.substring(1, pos);
				right = message.substring(pos+1, text.indexOf("]"));
				columnStart = 1;
				columnEnd = 1;
				rowStart = 0;
				rowEnd = 0;
				if (left.indexOf(",") != -1) {
					columnStart = parseInt(left.substring(0, left.indexOf(",")));
					columnEnd = parseInt(left.substring(left.indexOf(",") + 1)) + 1;
				} else {
					columnStart = parseInt(left) + 1;
					columnEnd = 2000;
				}
				if (right.indexOf(",") != -1) {
					rowStart = parseInt(right.substring(0, right.indexOf(",")));
					rowEnd = parseInt(right.substring(right.indexOf(",") + 1));
				} else {
					rowStart = parseInt(right);
					rowEnd = rowStart;
				}
				addMarker(columnStart, rowStart, columnEnd, rowEnd, text.substring(text.indexOf("]") + 2));
			}		
		}

		function displayShortError(text) {
			toastr.remove();
   			toastr.options.positionClass = "toast-top-center";
			toastr.error(text);			
		}

		function bucketizeValuesSingle(responseArray, patientCnt) {
			var result = [0, 0, 0, 0];
			var total = 0;
			for (x = 0; x < result.length; x++) {
				result[x] = Math.round((responseArray[x] / patientCnt)*100);
				total = total + result[x];
			}
			result[result.length - 1] = result[result.length - 1] - (total - 100);
			return result;
		}

		function bucketizeValues(responseArray, ranges, patientCnt, divider) {
			var result = [0, 0, 0, 0];
			for (x = 0; x < responseArray.length; x++) {
				var value = responseArray[x][0] / divider;
				if (value >= ranges[ranges.length - 1]) {
					result[result.length - 1] = result[result.length - 1] + responseArray[x][1];
				} else {
					for (y = 1; y < ranges.length; y++) {
						if (ranges[y] >= value) {
							result[y-1] = result[y-1] + responseArray[x][1];
							break;
						}
					}
				}
			}
			var total = 0;
			for (x = 0; x < result.length; x++) {
				result[x] = Math.round((result[x] / patientCnt)*100);
				total = total + result[x];
			}
			result[result.length - 1] = result[result.length - 1] - (total - 100);
			return result;
		}
