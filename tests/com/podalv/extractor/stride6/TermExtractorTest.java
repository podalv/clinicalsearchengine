package com.podalv.extractor.stride6;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.db.test.MockDataTable;
import com.podalv.db.test.MockResultSet;
import com.podalv.extractor.datastructures.DatabaseExtractionSource;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.stride6.extractors.TermExtractor;

public class TermExtractorTest {

  @Test
  public void smokeTest() throws Exception {
    final MockDataTable table = new MockDataTable("patient_id", "age_at_note_date_in_days", "note_year", "nid", "tid", "negated", "family");
    table.addRow("1", "455665", "2500", Common.POSITIVE_MENTION + "");
    table.addRow("1", "455665", "2600", Common.POSITIVE_MENTION + "");
    table.addRow("1", "455665", "2700", Common.POSITIVE_MENTION + "");
    table.addRow("1", "455665", "2800", Common.POSITIVE_MENTION + "");
    table.addRow("1", "455665", "2900", Common.POSITIVE_MENTION + "");
    table.addRow("1", "455665", "3000", Common.NEGATED_MENTION + "");
    table.addRow("1", "455665", "3100", Common.FH_MENTION + "");
    table.addRow("2", "2", "4100", Common.POSITIVE_MENTION + "");

    final TermExtractor extractor = new TermExtractor(new DatabaseExtractionSource(new MockResultSet(table)));
    Assert.assertTrue(extractor.hasNext());
    PatientRecord<TermRecord> record = extractor.next();
    Assert.assertEquals(1, record.getPatientId());
    Assert.assertEquals(7, record.getRecords().size());
    Assert.assertEquals(455665, record.getRecords().get(0).getNoteId());
    Assert.assertEquals(2500, record.getRecords().get(0).getTermId());
    Assert.assertEquals(0, record.getRecords().get(0).getNegated());
    Assert.assertEquals(0, record.getRecords().get(0).getFamilyHistory());

    Assert.assertEquals(455665, record.getRecords().get(1).getNoteId());
    Assert.assertEquals(2600, record.getRecords().get(1).getTermId());
    Assert.assertEquals(0, record.getRecords().get(1).getNegated());
    Assert.assertEquals(0, record.getRecords().get(1).getFamilyHistory());

    Assert.assertEquals(455665, record.getRecords().get(2).getNoteId());
    Assert.assertEquals(2700, record.getRecords().get(2).getTermId());
    Assert.assertEquals(0, record.getRecords().get(2).getNegated());
    Assert.assertEquals(0, record.getRecords().get(2).getFamilyHistory());

    Assert.assertEquals(455665, record.getRecords().get(3).getNoteId());
    Assert.assertEquals(2800, record.getRecords().get(3).getTermId());
    Assert.assertEquals(0, record.getRecords().get(3).getNegated());
    Assert.assertEquals(0, record.getRecords().get(3).getFamilyHistory());

    Assert.assertEquals(455665, record.getRecords().get(4).getNoteId());
    Assert.assertEquals(2900, record.getRecords().get(4).getTermId());
    Assert.assertEquals(0, record.getRecords().get(4).getNegated());
    Assert.assertEquals(0, record.getRecords().get(4).getFamilyHistory());

    Assert.assertEquals(455665, record.getRecords().get(5).getNoteId());
    Assert.assertEquals(3000, record.getRecords().get(5).getTermId());
    Assert.assertEquals(1, record.getRecords().get(5).getNegated());
    Assert.assertEquals(0, record.getRecords().get(5).getFamilyHistory());

    Assert.assertEquals(455665, record.getRecords().get(6).getNoteId());
    Assert.assertEquals(3100, record.getRecords().get(6).getTermId());
    Assert.assertEquals(0, record.getRecords().get(6).getNegated());
    Assert.assertEquals(1, record.getRecords().get(6).getFamilyHistory());

    Assert.assertTrue(extractor.hasNext());
    record = extractor.next();

    Assert.assertEquals(2, record.getRecords().get(0).getNoteId());
    Assert.assertEquals(4100, record.getRecords().get(0).getTermId());
    Assert.assertEquals(0, record.getRecords().get(0).getNegated());
    Assert.assertEquals(0, record.getRecords().get(0).getFamilyHistory());

    Assert.assertFalse(extractor.hasNext());
    extractor.close();
  }
}
