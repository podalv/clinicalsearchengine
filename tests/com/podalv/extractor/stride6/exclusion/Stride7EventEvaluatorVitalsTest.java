package com.podalv.extractor.stride6.exclusion;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VitalsRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class Stride7EventEvaluatorVitalsTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getChildZeroTime() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVitals(Stride7VitalsRecord.create(1).age(0.0).bmi(24.1).year(0));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getChildNullTime() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVitals(Stride7VitalsRecord.create(1).age(0.0).bmi(24.1).year(null));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getChildCorrectTime() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVitals(Stride7VitalsRecord.create(1).age(0.0).bmi(24.1).year(2012));
    source.addVitals(Stride7VitalsRecord.create(1).age(12.0).bmi(24.1).year(2012));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getHighAge() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVitals(Stride7VitalsRecord.create(1).age(50000.0).bmi(25.1).year(2012));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getEmptydescription() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVitals(Stride7VitalsRecord.create(1).age(12.0).year(2012));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static void assertMissing(final ClinicalSearchEngine engine) throws IOException {
    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void vitals_child_zero_time() throws Exception {
    engine = getChildZeroTime();
    assertMissing(engine);
  }

  @Test
  public void vitals_child_null_time() throws Exception {
    engine = getChildNullTime();
    assertMissing(engine);
  }

  @Test
  public void vitals_child_correct_time() throws Exception {
    engine = getChildCorrectTime();
    assertQuery(query(engine, timelineQuery()), 1);

    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void cpt_high_age() throws Exception {
    engine = getHighAge();
    assertMissing(engine);
  }

  @Test
  public void vitals_empty_code() throws Exception {
    engine = getEmptydescription();
    assertMissing(engine);
  }

}
