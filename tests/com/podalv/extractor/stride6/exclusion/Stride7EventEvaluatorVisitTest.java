package com.podalv.extractor.stride6.exclusion;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitWithSabRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class Stride7EventEvaluatorVisitTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getChildZeroTime() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(0).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(4.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getChildNullTime() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(null).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getChildCorrectTime() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(50.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getHighAge() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(12.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(50000.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getEmptyCode() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(12.0).code("").year(2012).sab("CPT").visitType("INPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getNullCode() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(12.0).code(null).year(2012).sab("CPT").visitType("INPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static void assertMissing(final ClinicalSearchEngine engine) throws IOException {
    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void cpt_child_zero_time() throws Exception {
    engine = getChildZeroTime();
    assertMissing(engine);
  }

  @Test
  public void cpt_child_null_time() throws Exception {
    engine = getChildNullTime();
    assertMissing(engine);
  }

  @Test
  public void cpt_child_correct_time() throws Exception {
    engine = getChildCorrectTime();
    assertQuery(query(engine, timelineQuery()), 1);

    assertQuery(query(engine, cptQuery("25000")), 1);

    assertQuery(query(engine, identicalQuery(cptQuery("25000"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(50),
        Common.daysToTime(50))))), 1);

    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void cpt_high_age() throws Exception {
    engine = getHighAge();
    assertMissing(engine);
  }

  @Test
  public void cpt_empty_code() throws Exception {
    engine = getEmptyCode();
    assertMissing(engine);
  }

  @Test
  public void cpt_null_code() throws Exception {
    engine = getNullCode();
    assertMissing(engine);
  }

}
