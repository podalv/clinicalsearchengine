package com.podalv.extractor.stride6;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.db.test.MockConnection;
import com.podalv.db.test.MockDataTable;
import com.podalv.db.test.MockResultSet;
import com.podalv.extractor.datastructures.ExtractionOptions;
import com.podalv.extractor.stride6.exclusion.DefaultEventEvaluator;
import com.podalv.extractor.stride6.exclusion.StridePatientExclusion;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.PatientExport;
import com.podalv.search.server.PatientExportTestInstance;
import com.podalv.tests.files.TestFileManager;
import com.podalv.utils.file.FileUtils;

public class Stride6ExtractorTestEmptyStrings {

  private static final File      DATA_FOLDER       = new File(new File("."), "extractionTestData");
  private static final File      DICTIONARY_FOLDER = new File(new File("."), "testData");
  private static TestFileManager files;

  @BeforeClass
  public static void cleanup() {
    if (DATA_FOLDER.listFiles() != null) {
      final File[] files = DATA_FOLDER.listFiles();
      if (files != null) {
        for (final File file : files) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
    files = new TestFileManager();
  }

  @AfterClass
  public static void clean() throws IOException {
    final File[] f = files.getFiles();
    for (final File file : f) {
      FileUtils.deleteWithMessage(Common.getDictFile(file));
    }
    files.close();
    cleanup();
  }

  private MockDataTable getDemographicsExtractor() throws SQLException {
    //SELECT patient_id, gender, race, ethnicity, age_at_death_in_days FROM stride6.demographics ORDER BY patient_id
    final MockDataTable table = new MockDataTable("patient_id", "gender", "race", "ethnicity", "age_at_death_in_days");
    table.addRow("1", "", "", "", "200.2");
    table.addRow("2", "FEMALE", "ASIAN", "UNKNOWN", "125.5");
    return table;
  }

  private MockDataTable getVisitExtractor() {
    //SELECT visit_id, patient_id, enc_type, age FROM stride6.visit order by patient_id
    final MockDataTable table = new MockDataTable("visit_id", "patient_id", "visit_year", "age_at_visit_in_days", "src_visit", "duration", "code", "sab", "SOURCE_CODE");
    table.addRow("1", "1", "2002", "100.5", "Outpatient", "1.5", "950.50", "ICD", "");
    table.addRow("1", "1", "2002", "100.5", "Outpatient", "1.5", "20000", "CPT", "");
    table.addRow("1", "3", "2004", "300.5", "Outpatient", "1.8", "720.20", "DX_ID", "200");
    table.addRow("1", "3", "2004", "300.5", "Outpatient", "1.8", "70000", "CPT", "");
    return table;
  }

  private MockDataTable getMedsExtractor() {
    final MockDataTable table = new MockDataTable(
        "SELECT stride6.pharmacy_order.patient_id, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.start_time_year, stride6.pharmacy_order.order_status, ingredient.rxcui, stride6.age_at_start_time_in_days, stride6.pharmacy_order.route FROM stride6.order_med JOIN (stride6.ingredient) ON (stride6.order_med.ingr_set_id = stride6.ingredient.ingr_set_id) order by patient_id");
    table.addRow("1", "100", "2002", "Prescribed", "20", "20", "oral");
    return table;
  }

  private MockDataTable getTidStrDictionaryExtractor() {
    //SELECT CODE, STR  FROM umls2015aa.MRCONSO where SAB=\"ICD9CM\"
    final MockDataTable table = new MockDataTable("tid", "str");
    table.addRow("1", "and");
    return table;
  }

  private MockDataTable getLabComponentExtractor() {
    final MockDataTable table = new MockDataTable("base_name", "common_name");
    table.addRow("a", "b");
    return table;
  }

  private MockDataTable getVitalsExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "flo_meas_id", "age_at_vital_entered_in_days", "detail_display", "meas_value", "-1");
    table.addRow("1", "3", "4.1", Common.VITALS_TEMPERATURE, "140.10", "-1");
    table.addRow("1", "3", "7.8", Common.VITALS_MAP, "50", "-1");
    return table;
  }

  private MockDataTable getVitalsVisitsExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "age_at_contact_in_days", "bp_systolic", "bp_diastolic", "temperature", "pulse", "weight_in_lbs", "height",
        "respirations", "bmi", "bsa");
    table.addRow("1", "5.5", "120", "80", "98.6", "80", "250.5", "5' 10\"", "20", "25.5", "1.95");
    return table;
  }

  private MockDataTable getLpchLabsExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "age_at_effective_time_in_days", "component_code", "labvalue", "reference_range");
    table.addRow("1", "7.5", "WBC", "3.5", "3.1-4.4");
    return table;
  }

  private MockDataTable getShcLabsExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "age_at_taken_time_in_days", "base_name", "ord_num_value", "reference_low", "reference_high", "ref_normal_vals");
    table.addRow("1", "8.5", "ABC", "4.5", "3.1", "4.4", "");
    table.addRow("1", "9.5", "ABC", "4.5", "", "", ">4");
    return table;
  }

  private MockDataTable getVisitDxExtractor() {
    final MockDataTable table = new MockDataTable("visit_id", "dx_id", "primary_dx_yn");
    table.addRow("1", "200", "Y");
    table.addRow("101", "201", null);
    return table;
  }

  private MockDataTable getTermExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "nid", "tid", "negated", "family");
    table.addRow("1", "455665", "2500", "0", "0");
    table.addRow("1", "455665", "2600", "0", "0");
    table.addRow("1", "455665", "2700", "1", "0");
    table.addRow("1", "455665", "2800", "0", "1");
    table.addRow("2", "2", "2900", "0", "0");
    return table;
  }

  private MockDataTable getTermDictionaryTable() {
    final MockDataTable table = new MockDataTable("terminology");
    table.addRow("terminology4");
    return table;
  }

  private MockDataTable getNotesExtractor() {
    final MockDataTable table = new MockDataTable("note_id", "doc_description", "age_at_note_date_in_days", "note_year");
    table.addRow("455665", "doc 1", "100", "2002");
    table.addRow("2", "doc 2", "200", "2003");
    return table;
  }

  private MockDataTable getPatientCnt() {
    final MockDataTable table = new MockDataTable("result");
    table.addRow("3");
    return table;
  }

  @Test
  public void smokeTest() throws Exception {
    PatientExport.setInstance(new PatientExportTestInstance(null));
    final MockConnection connection = new MockConnection();
    connection.addResultSet(Commands.DEMOGRAPHICS, getDemographicsExtractor());
    connection.addResultSet(Commands.PATIENT_COUNT, getPatientCnt());
    connection.addResultSet(Commands.VISITS, getVisitExtractor());
    connection.addResultSet(Commands.getTermsQuery(Commands.TERMS, 0, "patient_id"), getTermExtractor());
    connection.addResultSet(Commands.getTerminologyQuery(Commands.TERM_DICTIONARY, "terminology4"), getTidStrDictionaryExtractor());
    connection.addResultSet(Commands.MEDS, getMedsExtractor());
    connection.addResultSet(Commands.VITALS, getVitalsExtractor());
    connection.addResultSet(Commands.VITALS_VISITS, getVitalsVisitsExtractor());
    connection.addResultSet(Commands.LABS_LPCH, getLpchLabsExtractor());
    connection.addResultSet(Commands.LABS_SHC, getShcLabsExtractor());
    connection.addResultSet(Commands.VISIT_DX, getVisitDxExtractor());
    connection.addResultSet(Commands.NOTES, getNotesExtractor());
    connection.addResultSet(Commands.LABS_COMPONENT, getLabComponentExtractor());
    connection.addResultSet(Commands.TERM_DICTIONARY_TABLE, getTermDictionaryTable());
    Stride6DatabaseDownload.execute(new String[] {"dtvumsnrplc", DATA_FOLDER.getAbsolutePath()}, ConnectionSettings.createFromConnection(connection));

    final Stride6Extractor extractor = new Stride6Extractor(Stride6Extractor.getDatabaseConnectorFromFolder(3, DATA_FOLDER, new ExtractionOptions("dict")),
        new StridePatientExclusion(), new DefaultEventEvaluator());
    extractor.extract(DICTIONARY_FOLDER, DATA_FOLDER, Integer.MAX_VALUE);

    Stride6DatabaseDownload.extractTermDictionary(new MockResultSet(getTidStrDictionaryExtractor()), new File(DATA_FOLDER, Stride6DatabaseDownload.TERM_DICTIONARY_FILE));

    final ClinicalSearchEngine engine = new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
    Assert.assertEquals(1d, engine.search(0, PatientSearchRequest.create("GENDER=\"EMPTY\""), Integer.MAX_VALUE).getPatientIds().getFirst()[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("GENDER=\"EMPTY\""), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("RACE=\"EMPTY\""), Integer.MAX_VALUE).getPatientIds().getFirst()[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("RACE=\"EMPTY\""), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ETHNICITY=\"EMPTY\""), Integer.MAX_VALUE).getPatientIds().getFirst()[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ETHNICITY=\"EMPTY\""), Integer.MAX_VALUE).getPatientIds().size());

    engine.close();
  }
}
