package com.podalv.extractor.stride6;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.podalv.db.test.MockDataTable;
import com.podalv.db.test.MockResultSet;
import com.podalv.extractor.datastructures.BinaryFileExtractionSource;
import com.podalv.tests.files.TestFileManager;
import com.podalv.utils.file.FileUtils;

public class Stride6DatabaseDownloadTest {

  private static TestFileManager files;

  @BeforeClass
  public static void setup() {
    files = new TestFileManager();
  }

  @AfterClass
  public static void tearDown() throws IOException {
    final File[] f = files.getFiles();
    for (final File file : f) {
      FileUtils.deleteWithMessage(Common.getDictFile(file));
    }
    files.close();
  }

  @Test
  public void celsiusToFahrenheit() throws Exception {
    Assert.assertEquals(104, Common.convertCelsiusToFahrenheit(40), 0.001);
  }

  @Test
  public void demographics() throws Exception {
    Assert.assertEquals("SELECT patient_id, gender, race, ethnicity, age_at_death_in_days, -1 FROM stride6.demographics ORDER BY patient_id", Commands.DEMOGRAPHICS);
    //If the SQL command changes this test needs to be changed otherwise this test is irrelevant
    final MockDataTable table = new MockDataTable("patient_id", "gender", "race", "ethnicity", "age_at_death_in_days");
    table.addRow("1", "MALE", "ASIAN", "LATINO", "20.51");
    table.addRow("2", "FEMALE", "WHITE", "UNKNOWN", "0");
    table.addRow("3", "OTHER", "BLACK", "", "0");
    final MockResultSet set = new MockResultSet(table);
    Stride6DatabaseDownload.extractDemographics(set, files.getFile("demo"));
    final BinaryFileExtractionSource src = new BinaryFileExtractionSource(files.getFile("demo"), Common.getDictFile(files.getFile("demo")), Commands.DEMOGRAPHICS_STRUCTURE);
    Assert.assertTrue(src.next());
    Assert.assertEquals(1, src.getInt(1));
    Assert.assertEquals("MALE", src.getString(2));
    Assert.assertEquals("ASIAN", src.getString(3));
    Assert.assertEquals("LATINO", src.getString(4));
    Assert.assertEquals(21, src.getDouble(5), 0.001);
    Assert.assertTrue(src.next());
    Assert.assertEquals(2, src.getInt(1));
    Assert.assertEquals("FEMALE", src.getString(2));
    Assert.assertEquals("WHITE", src.getString(3));
    Assert.assertEquals("UNKNOWN", src.getString(4));
    Assert.assertEquals(0, src.getDouble(5), 0.001);
    Assert.assertTrue(src.next());
    Assert.assertEquals(3, src.getInt(1));
    Assert.assertEquals("OTHER", src.getString(2));
    Assert.assertEquals("BLACK", src.getString(3));
    Assert.assertEquals("", src.getString(4));
    Assert.assertEquals(0, src.getDouble(5), 0.001);
    Assert.assertFalse(src.next());
    src.close();
  }

  @Test
  public void vitals() throws Exception {
    Assert.assertEquals("SELECT patient_id, flo_meas_id, age_at_vital_entered_in_days, detail_display, meas_value, -1 FROM stride6.vitals order by patient_id", Commands.VITALS);
    //If the SQL command changes this test needs to be changed otherwise this test is irrelevant
    final MockDataTable table = new MockDataTable("patient_id", "flo_meas_id", "age_at_vital_entered_in_days", "detail_display", "meas_value", "-1");
    table.addRow("1", "3", "20.20", "TEST 1", "0.01", "-1");
    table.addRow("2", "4", "30.30", "TEST 2", "2.5", "-1");
    table.addRow("3", "5", "40.40", "TEST 3", "3.6", "-1");
    final MockResultSet set = new MockResultSet(table);
    Stride6DatabaseDownload.extractVitals(set, files.getFile("vitals"));
    final BinaryFileExtractionSource src = new BinaryFileExtractionSource(files.getFile("vitals"), Common.getDictFile(files.getFile("vitals")), Commands.VITALS_STRUCTURE);
    Assert.assertTrue(src.next());
    Assert.assertEquals(1, src.getInt(1));
    Assert.assertEquals(3, src.getInt(2));
    Assert.assertEquals(20.20, src.getDouble(3), 0.001);
    Assert.assertEquals("TEST 1", src.getString(4));
    Assert.assertEquals(0.01, src.getDouble(5), 0.001);
    Assert.assertEquals(-1, src.getShort(6));
    Assert.assertTrue(src.next());
    Assert.assertEquals(2, src.getInt(1));
    Assert.assertEquals(4, src.getInt(2));
    Assert.assertEquals(30.30, src.getDouble(3), 0.001);
    Assert.assertEquals("TEST 2", src.getString(4));
    Assert.assertEquals(2.5, src.getDouble(5), 0.001);
    Assert.assertEquals(-1, src.getShort(6));
    Assert.assertTrue(src.next());
    Assert.assertEquals(3, src.getInt(1));
    Assert.assertEquals(5, src.getInt(2));
    Assert.assertEquals(40.40, src.getDouble(3), 0.001);
    Assert.assertEquals("TEST 3", src.getString(4));
    Assert.assertEquals(3.6, src.getDouble(5), 0.001);
    Assert.assertEquals(-1, src.getShort(6));
    Assert.assertFalse(src.next());
    src.close();
  }

  @Test
  public void meds() throws Exception {
    Assert.assertEquals(
        "SELECT stride6.pharmacy_order.patient_id, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.start_time_year, stride6.pharmacy_order.order_status, ingredient.rxcui, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.route FROM stride6.pharmacy_order JOIN (stride6.ingredient) ON (stride6.pharmacy_order.ingr_set_id = stride6.ingredient.ingr_set_id) order by patient_id",
        Commands.MEDS);
    //If the SQL command changes this test needs to be changed otherwise this test is irrelevant
    final MockDataTable table = new MockDataTable("pharmacy_order.patient_id", //
        "pharmacy_order.age_at_start_time_in_days", //
        "pharmacy_order.start_time_year", //
        "pharmacy_order.order_status", //
        "ingredient.rxcui", //
        "pharmacy_order.age_at_start_time_in_days", //
        "pharmacy_order.route");
    table.addRow("1", "20.20", "2012", "Continued", "112", "20.20", "Oral");
    table.addRow("2", "30.30", "2013", "Discontinued", "113", "30.30", "Intravenous");
    table.addRow("3", "40.40", "2014", "Continued", "114", "40.40", "Oral");
    final MockResultSet set = new MockResultSet(table);
    Stride6DatabaseDownload.extractMeds(set, files.getFile("meds"));
    final BinaryFileExtractionSource src = new BinaryFileExtractionSource(files.getFile("meds"), Common.getDictFile(files.getFile("meds")), Commands.MEDS_STRUCTURE);
    Assert.assertTrue(src.next());
    Assert.assertEquals(1, src.getInt(1));
    Assert.assertEquals(20.20, src.getDouble(2), 0.001);
    Assert.assertEquals(2012, src.getShort(3));
    Assert.assertEquals("Continued", src.getString(4));
    Assert.assertEquals(112, src.getInt(5));
    Assert.assertEquals(50.20, src.getDouble(6), 0.001);
    Assert.assertEquals("Oral", src.getString(7));
    Assert.assertTrue(src.next());
    Assert.assertEquals(2, src.getInt(1));
    Assert.assertEquals(30.30, src.getDouble(2), 0.001);
    Assert.assertEquals(2013, src.getShort(3));
    Assert.assertEquals("Discontinued", src.getString(4));
    Assert.assertEquals(113, src.getInt(5));
    Assert.assertEquals(30.30, src.getDouble(6), 0.001);
    Assert.assertEquals("Intravenous", src.getString(7));
    Assert.assertTrue(src.next());
    Assert.assertEquals(3, src.getInt(1));
    Assert.assertEquals(40.40, src.getDouble(2), 0.001);
    Assert.assertEquals(2014, src.getShort(3));
    Assert.assertEquals("Continued", src.getString(4));
    Assert.assertEquals(114, src.getInt(5));
    Assert.assertEquals(70.40, src.getDouble(6), 0.001);
    Assert.assertEquals("Oral", src.getString(7));
    Assert.assertFalse(src.next());
    src.close();
  }

  @Test
  public void visits() throws Exception {
    Assert.assertEquals(
        "SELECT visit_id, patient_id, visit_year, age_at_visit_in_days, src_visit, duration, code, sab, SOURCE_CODE FROM stride6.visit_master UNION SELECT visit_id, patient_id, visit_year, age_at_visit_in_days, src_visit, duration, code, sab, SOURCE_CODE FROM stride6.visit_master_billing order by patient_id",
        Commands.VISITS);
    //If the SQL command changes this test needs to be changed otherwise this test is irrelevant
    final MockDataTable table = new MockDataTable("visit_id", //
        "patient_id", //
        "visit_year", //
        "age_at_visit_in_days", //
        "src_visit", //
        "duration", //
        "code", //
        "sab", //
        "SOURCE_CODE");
    table.addRow("1", "1", "2012", "20.20", "hospital encounter", "0.00", null, "DX_ID", null);
    table.addRow("2", "2", "2013", "30.30", "outpatient encounter", "5.000", "91.5", "DX_ID", null);
    table.addRow("3", "3", "2014", "40.40", "hospital encounter", "10.00", "20.125", "DX_ID", null);
    final MockResultSet set = new MockResultSet(table);
    Stride6DatabaseDownload.extractVisits(set, files.getFile("visits"));
    final BinaryFileExtractionSource src = new BinaryFileExtractionSource(files.getFile("visits"), Common.getDictFile(files.getFile("visits")), Commands.VISITS_STRUCTURE);
    Assert.assertTrue(src.next());
    Assert.assertEquals(1, src.getInt(1));
    Assert.assertEquals(1, src.getInt(2));
    Assert.assertEquals(2012, src.getShort(3));
    Assert.assertEquals(20.20, src.getDouble(4), 0.001);
    Assert.assertEquals("hospital encounter", src.getString(5));
    Assert.assertEquals(0.00, src.getDouble(6), 0.001);
    Assert.assertNull(src.getString(7));
    Assert.assertEquals("DX_ID", src.getString(8));
    Assert.assertTrue(src.next());
    Assert.assertEquals(2, src.getInt(1));
    Assert.assertEquals(2, src.getInt(2));
    Assert.assertEquals(2013, src.getShort(3));
    Assert.assertEquals(30.30, src.getDouble(4), 0.001);
    Assert.assertEquals("outpatient encounter", src.getString(5));
    Assert.assertEquals(5.00, src.getDouble(6), 0.001);
    Assert.assertEquals("91.5", src.getString(7));
    Assert.assertEquals("DX_ID", src.getString(8));
    Assert.assertTrue(src.next());
    Assert.assertEquals(3, src.getInt(1));
    Assert.assertEquals(3, src.getInt(2));
    Assert.assertEquals(2014, src.getShort(3));
    Assert.assertEquals(40.40, src.getDouble(4), 0.001);
    Assert.assertEquals("hospital encounter", src.getString(5));
    Assert.assertEquals(10.00, src.getDouble(6), 0.001);
    Assert.assertEquals("20.125", src.getString(7));
    Assert.assertEquals("DX_ID", src.getString(8));
    Assert.assertFalse(src.next());
    src.close();
  }

}
