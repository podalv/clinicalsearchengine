package com.podalv.extractor.stride6.truven;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.extractor.truven.TruvenConst;
import com.podalv.extractor.truven.TruvenExtractor;
import com.podalv.extractor.truven.test.TruvenSourceFileGenerator;
import com.podalv.extractor.truven.test.TruvenTestHarness;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.StringHashSet;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.server.ClinicalSearchEngine;

public class LargeFunctionalTest {

  private static final UmlsDictionary               umls       = UmlsDictionary.create();
  private static final String[]                     icd9Codes  = new String[] {"25050", "25000", "24950", "12345", "222", "02253"};
  private static final String[]                     procCodes  = new String[] {"12345", "54111", "65555", "32589", "99999"};
  private static final String[]                     ndcCodes   = new String[] {"00002010102", "00002010202", "00002010204", "00002010402", "00002010403", "00002010502",
      "00002010504"};
  private static StringKeyObjectMap<IntOpenHashSet> codeToPids = new StringKeyObjectMap<>();
  private static HashMap<Integer, IntArrayList>     ages       = new HashMap<>();

  private void addPidToCode(final int pid, final String code) {
    IntOpenHashSet set = codeToPids.get(code);
    if (set == null) {
      set = new IntOpenHashSet();
      codeToPids.put(code, set);
    }
    set.add(pid);
  }

  private void addTime(final int pid, final int age, final String date, final String date2, final int offset) throws ParseException {
    final int time1 = (age * 365 * 24 * 60) + (TruvenConst.dateToDays(new SimpleDateFormat("yyyy-dd-MM HH:mm:ss.SSS").parse(date + " 00:00:00.000").getTime()) * 24 * 60);
    final int time2 = offset + (age * 365 * 24 * 60) + (TruvenConst.dateToDays(new SimpleDateFormat("yyyy-dd-MM HH:mm:ss.SSS").parse(date2 + " 00:00:00.000").getTime()) * 24 * 60);
    IntArrayList i = ages.get(pid);
    if (i == null) {
      i = new IntArrayList();
      ages.put(pid, i);
    }
    i.add(time1);
    i.add(time2);
  }

  private void addTime(final int pid, final int age, final String date, final String date2) throws ParseException {
    addTime(pid, age, date, date2, 0);
  }

  private void generateRandomPatient(final TruvenSourceFileGenerator generator, final int pid) throws NumberFormatException, IOException, ParseException {
    final Random random = new Random(pid);
    final String sex = (pid % 2 == 0) ? TruvenConst.MALE : TruvenConst.FEMALE;
    final int age = random.nextInt(80) + 1;
    for (int x = 7; x < 15; x++) {
      final boolean generateYear = random.nextBoolean();
      if (generateYear) {
        final boolean generateI = false;//random.nextBoolean();
        final boolean generateO = false;//random.nextBoolean();
        final boolean generateD = false;//random.nextBoolean();
        final boolean generateS = random.nextBoolean();
        if (generateI) {
          final int cnt = random.nextInt(5);
          for (int y = 0; y < cnt; y++) {
            final StringHashSet uniqueDx = new StringHashSet();
            final int dxCnt = random.nextInt(icd9Codes.length);
            for (int z = 0; z < dxCnt; z++) {
              final String code = icd9Codes[random.nextInt(icd9Codes.length)];
              addPidToCode(pid, TruvenExtractor.convertIcd9Code(code));
              uniqueDx.add(code);
              final String[] hier = umls.getIcd9Parents(TruvenExtractor.convertIcd9Code(code));
              if (hier != null) {
                for (final String parent : hier) {
                  addPidToCode(pid, parent);
                }
              }
            }
            final StringHashSet uniqueProc = new StringHashSet();
            final int procCnt = random.nextInt(procCodes.length);
            for (int z = 0; z < procCnt; z++) {
              final String code = procCodes[random.nextInt(procCodes.length)];
              addPidToCode(pid, code);
              uniqueProc.add(code);
            }
            addTime(pid, age, (2000 + x) + "-01-01", (2000 + x) + "-02-02");
            generator.writeI(x, pid, Integer.parseInt(sex), age, (2000 + x) + "-01-01", (2000 + x) + "-02-02", uniqueDx.toArray(), uniqueProc.toArray());
          }
        }
        if (generateO) {
          final int cnt = random.nextInt(5);
          for (int y = 0; y < cnt; y++) {
            final StringHashSet uniqueDx = new StringHashSet();
            final int dxCnt = random.nextInt(2);
            for (int z = 0; z < dxCnt; z++) {
              final String code = icd9Codes[random.nextInt(icd9Codes.length)];
              addPidToCode(pid, TruvenExtractor.convertIcd9Code(code));
              uniqueDx.add(code);
              final String[] hier = umls.getIcd9Parents(TruvenExtractor.convertIcd9Code(code));
              if (hier != null) {
                for (final String parent : hier) {
                  addPidToCode(pid, parent);
                }
              }
            }
            final String procCode = procCodes[random.nextInt(procCodes.length)];
            addPidToCode(pid, procCode);
            generator.writeO(x, pid, Integer.parseInt(sex), age, (2000 + x) + "-01-01", uniqueDx.toArray(), procCode);
            addTime(pid, age, (2000 + x) + "-01-01", (2000 + x) + "-01-01");
          }
        }
        if (generateS) {
          final int cnt = random.nextInt(5);
          for (int y = 0; y < cnt; y++) {
            final StringHashSet uniqueDx = new StringHashSet();
            final int dxCnt = random.nextInt(2);
            for (int z = 0; z < dxCnt; z++) {
              final String code = icd9Codes[random.nextInt(icd9Codes.length)];
              addPidToCode(pid, TruvenExtractor.convertIcd9Code(code));
              uniqueDx.add(code);
              final String[] hier = umls.getIcd9Parents(TruvenExtractor.convertIcd9Code(code));
              if (hier != null) {
                for (final String parent : hier) {
                  addPidToCode(pid, parent);
                }
              }
            }
            final String procCode = procCodes[random.nextInt(procCodes.length)];
            addPidToCode(pid, procCode);
            generator.writeS(x, pid, Integer.parseInt(sex), age, (2000 + x) + "-01-01", (2000 + x) + "-02-02", uniqueDx.toArray(), procCode);
            addTime(pid, age, (2000 + x) + "-01-01", (2000 + x) + "-02-02");
          }
        }
        if (generateD) {
          final int cnt = random.nextInt(5);
          for (int y = 0; y < cnt; y++) {
            generator.writeD(x, pid, Integer.parseInt(sex), age, (2000 + x) + "-01-01", y, ndcCodes[random.nextInt(ndcCodes.length)]);
            addTime(pid, age, (2000 + x) + "-01-01", (2000 + x) + "-01-01", 30 * 24 * 60);
          }
        }
      }
    }
  }

  private void assertIcd9Code(final PatientSearchResponse response, final String code) throws IOException {
    final IntKeyIntOpenHashMap newToOld = TruvenTestHarness.readNewPidToOldPidMap();
    final IntOpenHashSet oldSet = codeToPids.get(code);
    final IntOpenHashSet newSet = new IntOpenHashSet();
    Assert.assertEquals(oldSet.size(), response.getPatientIds().size());
    final Iterator<double[]> iterator = response.getPatientIds().iterator();
    while (iterator.hasNext()) {
      final double[] val = iterator.next();
      newSet.add(newToOld.get((int) val[0]));
      Assert.assertTrue(oldSet.contains(newToOld.get((int) val[0])));
    }
    System.out.println("DONE");
    System.out.println("MISSING OLD PIDS: ");
    final IntOpenHashSet missing = new IntOpenHashSet(oldSet);
    missing.removeAll(newSet);
    System.out.println(missing.toString("\n"));
    Assert.assertTrue(missing.size() == 0);
  }

  private void assertAge(final PatientSearchResponse response, final int age) throws IOException {
    final IntKeyIntOpenHashMap newToOld = TruvenTestHarness.readNewPidToOldPidMap();
    final Iterator<Entry<Integer, IntArrayList>> i = ages.entrySet().iterator();
    final IntOpenHashSet oldSet = new IntOpenHashSet();
    while (i.hasNext()) {
      final Entry<Integer, IntArrayList> entry = i.next();
      for (int x = 0; x < entry.getValue().size(); x += 2) {
        if (entry.getValue().get(x) <= age && entry.getValue().get(x + 1) >= age) {
          oldSet.add(entry.getKey());
        }
      }
    }
    final IntOpenHashSet newSet = new IntOpenHashSet();
    final Iterator<double[]> iterator = response.getPatientIds().iterator();
    while (iterator.hasNext()) {
      final double[] val = iterator.next();
      newSet.add(newToOld.get((int) val[0]));
      Assert.assertTrue(oldSet.contains(newToOld.get((int) val[0])));
    }
    System.out.println(oldSet.toString());
    System.out.println(newSet.toString());
    System.out.println("DONE");
    System.out.println("MISSING OLD PIDS: ");
    final IntOpenHashSet missing = new IntOpenHashSet(oldSet);
    missing.removeAll(newSet);
    System.out.println(missing.toString("\n"));
    Assert.assertTrue(missing.size() == 0);
    Assert.assertEquals(oldSet.size(), response.getPatientIds().size());
  }

  @Test
  public void largeTest() throws Exception {
    final TruvenSourceFileGenerator generator = TruvenTestHarness.getHarness();
    for (int x = 1; x < 1000; x++) {
      generateRandomPatient(generator, x);
    }
    final ClinicalSearchEngine engine = TruvenTestHarness.getEngine(generator);
    final IntKeyIntOpenHashMap newToOld = TruvenTestHarness.readNewPidToOldPidMap();
    final IntKeyIntMapIterator ix = newToOld.entries();
    while (ix.hasNext()) {
      ix.next();
      System.out.println("NEW/OLD = " + ix.getKey() + "/" + ix.getValue());
    }
    //"25050", "25000", "24950", "12345", "222", "02253"
    //NEW = 777  OLD = 2
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=250.50")), "250.50");
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=250.5")), "250.5");
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=250")), "250");
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=249.50")), "249.50");
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=123.45")), "123.45");
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=222")), "222");
    assertIcd9Code(engine.search(PatientSearchRequest.create("ICD9=022.53")), "022.53");
    final IntOpenHashSet uniqueTimes = new IntOpenHashSet();
    final Iterator<Entry<Integer, IntArrayList>> i = ages.entrySet().iterator();
    while (i.hasNext()) {
      final Entry<Integer, IntArrayList> entry = i.next();
      uniqueTimes.addAll(entry.getValue().toArray());
    }

    final IntIterator ii = uniqueTimes.iterator();
    while (ii.hasNext()) {
      final int val = ii.next();
      assertAge(engine.search(PatientSearchRequest.create("AGE(" + val + "," + val + ")")), val);
    }

    engine.close();
    TruvenTestHarness.clearData();
  }
}