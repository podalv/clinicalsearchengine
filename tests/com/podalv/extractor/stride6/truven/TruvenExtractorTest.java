package com.podalv.extractor.stride6.truven;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.extractor.truven.TruvenExtractor;

public class TruvenExtractorTest {

  @Test
  public void convertIcd9Code() throws Exception {
    Assert.assertEquals("960.1", TruvenExtractor.convertIcd9Code("9601"));
    Assert.assertEquals("486", TruvenExtractor.convertIcd9Code("486"));
    Assert.assertEquals("V96.01", TruvenExtractor.convertIcd9Code("V9601"));
    Assert.assertEquals("E960.01", TruvenExtractor.convertIcd9Code("E96001"));
  }

  @Test
  public void convertIcd10Code() throws Exception {
    Assert.assertEquals("960.1", TruvenExtractor.convertIcd10Code("9601"));
    Assert.assertEquals("V96.01", TruvenExtractor.convertIcd10Code("V9601"));
    Assert.assertEquals("486", TruvenExtractor.convertIcd10Code("486"));
    Assert.assertEquals("V96.001", TruvenExtractor.convertIcd10Code("V96001"));
  }

}