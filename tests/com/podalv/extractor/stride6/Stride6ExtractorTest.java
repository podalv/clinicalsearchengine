package com.podalv.extractor.stride6;

import java.io.File;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.db.test.MockConnection;
import com.podalv.db.test.MockDataTable;
import com.podalv.db.test.MockResultSet;
import com.podalv.extractor.datastructures.ExtractionOptions;
import com.podalv.extractor.stride6.exclusion.DefaultEventEvaluator;
import com.podalv.extractor.stride6.exclusion.StridePatientExclusion;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.Transaction;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.PatientExport;
import com.podalv.search.server.PatientExportTestInstance;
import com.podalv.utils.file.FileUtils;

public class Stride6ExtractorTest {

  private static final File DATA_FOLDER       = new File(new File("."), "extractionTestData");
  private static final File DICTIONARY_FOLDER = new File(new File("."), "testData");

  @BeforeClass
  public static void cleanup() {
    if (DATA_FOLDER.listFiles() != null) {
      final File[] files = DATA_FOLDER.listFiles();
      if (files != null) {
        for (final File file : files) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
    if (DICTIONARY_FOLDER.listFiles() != null) {
      final File[] files = DICTIONARY_FOLDER.listFiles();
      if (files != null) {
        for (final File file : files) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
  }

  @AfterClass
  public static void clean() {
    cleanup();
  }

  private MockDataTable getDemographicsExtractor() throws SQLException {
    //SELECT patient_id, gender, race, ethnicity, age_at_death_in_days FROM stride6.demographics ORDER BY patient_id
    final MockDataTable table = new MockDataTable("patient_id", "gender", "race", "ethnicity", "age_at_death_in_days");
    table.addRow("1", "MALE", "WHITE", "HISPANIC", "200.2");
    table.addRow("2", "FEMALE", "ASIAN", "UNKNOWN", "22125.5");
    table.addRow("3", "MALE", "BLACK", "OTHER", "22252.5");
    return table;
  }

  private MockDataTable getTermExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "nid", "tid", "negated", "family");
    table.addRow("1", "455665", "2500", "0", "0");
    table.addRow("1", "455665", "2600", "0", "0");
    table.addRow("1", "455665", "2700", "1", "0");
    table.addRow("1", "455665", "2800", "0", "1");
    table.addRow("2", "2", "2900", "0", "0");
    return table;
  }

  private MockDataTable getNotesExtractor() {
    final MockDataTable table = new MockDataTable("note_id", "doc_description", "age_at_note_date_in_days", "note_year");
    table.addRow("455665", "doc 1", "100", "2002");
    table.addRow("2", "doc 2", "200", "2003");
    return table;
  }

  private MockDataTable getVisitExtractor() {
    //SELECT visit_id, patient_id, enc_type, age FROM stride6.visit order by patient_id
    final MockDataTable table = new MockDataTable("visit_id", "patient_id", "visit_year", "age_at_visit_in_days", "src_visit", "duration", "code", "sab", "SOURCE_CODE",
        "SOURCE_TABLE");
    table.addRow("1", "1", "2002", "100.5", "Outpatient", "1.5", "250.50", "ICD", "");
    table.addRow("1", "1", "2002", "100.5", "Outpatient", "1.5", "20000", "CPT", "");
    table.addRow("1", "3", "2004", "300.5", "Outpatient", "1.8", "720.20", "DX_ID", "200");
    table.addRow("1", "3", "2004", "300.5", "Outpatient", "1.8", "70000", "CPT", "");
    return table;
  }

  private MockDataTable getMedsExtractor() {
    final MockDataTable table = new MockDataTable(
        "SELECT stride6.pharmacy_order.patient_id, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.start_time_year, stride6.pharmacy_order.order_status, ingredient.rxcui, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.route, -1 FROM stride6.pharmacy_order JOIN (stride6.ingredient) ON (stride6.pharmacy_order.ingr_set_id = stride6.ingredient.ingr_set_id) order by patient_id");
    table.addRow("1", "100", "2002", "Prescribed", "20", "100", "oral", "-1");
    return table;
  }

  private MockDataTable getTermDictionaryTable() {
    final MockDataTable table = new MockDataTable("terminology");
    table.addRow("terminology4");
    return table;
  }

  private MockDataTable getLabComponentExtractor() {
    final MockDataTable table = new MockDataTable("base_name", "common_name");
    table.addRow("a", "b");
    return table;
  }

  private MockDataTable getTidStrDictionaryExtractor() {
    //SELECT CODE, STR  FROM umls2015aa.MRCONSO where SAB=\"ICD9CM\"
    final MockDataTable table = new MockDataTable("tid", "str");
    table.addRow("1", "and");
    return table;
  }

  private MockDataTable getVitalsExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "flo_meas_id", "age_at_vital_entered_in_days", "detail_display", "meas_value", "-1");
    table.addRow("1", "3", "4.1", Common.VITALS_TEMPERATURE, "140.10", "-1");
    table.addRow("1", "3", "7.8", Common.VITALS_MAP, "50", "-1");
    return table;
  }

  private MockDataTable getVitalsVisitsExtractor() {
    final MockDataTable table = new MockDataTable("patient_id", "age_at_contact_in_days", "bp_systolic", "bp_diastolic", "temperature", "pulse", "weight_in_lbs", "height",
        "respirations", "bmi", "bsa");
    table.addRow("1", "5.5", "120", "80", "98.6", "80", "250.5", "5' 10\"", "20", "25.5", "1.95");
    return table;
  }

  private MockDataTable getVisitDxExtractor() {
    final MockDataTable table = new MockDataTable("visit_id", "dx_id", "primary_dx_yn");
    table.addRow("1", "200", "Y");
    table.addRow("101", "201", null);
    return table;
  }

  private MockDataTable getPatientCnt() {
    final MockDataTable table = new MockDataTable("result");
    table.addRow("3");
    return table;
  }

  private void assertIcd9Payload(final PatientSearchModel model, final ClinicalSearchEngine engine, final String icd9Code, final double[] expected) {
    Assert.assertEquals(1, model.getIcd9(engine.getIndices().getIcd9(icd9Code)).size());
    final IntArrayList list = model.getPayload(model.getIcd9(engine.getIndices().getIcd9(icd9Code)).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(expected[0]), Common.daysToTime(expected[1]), (int) expected[2]}, list.toArray());
  }

  private void assertCptPayload(final PatientSearchModel model, final ClinicalSearchEngine engine, final String cptCode, final double[] expected) {
    Assert.assertEquals(1, model.getCpt(engine.getIndices().getCpt(cptCode)).size());
    final IntArrayList list = model.getPayload(model.getCpt(engine.getIndices().getCpt(cptCode)).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(expected[0]), Common.daysToTime(expected[1])}, list.toArray());
  }

  private void assertTerms2(final PatientSearchModel model, final ClinicalSearchEngine engine) {
    Assert.assertEquals(1, model.getPositiveTerms(2900).size());
    final IntArrayList list = model.getPayload(model.getPositiveTerms(2900).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(200), 2, 1}, list.toArray());
  }

  private void assertVitals(final PatientSearchModel model, final ClinicalSearchEngine engine) {
    IntArrayList l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_TEMPERATURE), 100, 150);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(4.1), Common.daysToTime(4.1)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_MAP), 40, 60);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(7.8), Common.daysToTime(7.8)}, l.toArray());
  }

  private void assertVitalsVisits(final PatientSearchModel model, final ClinicalSearchEngine engine) {
    IntArrayList l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_BMI), 25, 26);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_BP_D), 79, 81);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_BP_S), 119, 121);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_BSA), 1.94, 1.96);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_HEIGHT), 69, 71);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_PULSE), 79, 81);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_RESP), 19, 21);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_TEMPERATURE), 98, 99);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
    l = model.getVitals().get(engine.getIndices(), engine.getIndices().getVitalsCode(Common.VITALS_WEIGHT), 250, 251);
    Assert.assertArrayEquals(new int[] {Common.daysToTime(5.5), Common.daysToTime(5.5)}, l.toArray());
  }

  private void assertTerms1(final PatientSearchModel model, final ClinicalSearchEngine engine) {
    Assert.assertEquals(1, model.getPositiveTerms(2500).size());
    IntArrayList list = model.getPayload(model.getPositiveTerms(2500).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(100), 455665, 0}, list.toArray());

    Assert.assertEquals(1, model.getPositiveTerms(2600).size());
    list = model.getPayload(model.getPositiveTerms(2600).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(100), 455665, 0}, list.toArray());

    Assert.assertEquals(1, model.getPositiveTerms(2600).size());
    list = model.getPayload(model.getPositiveTerms(2600).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(100), 455665, 0}, list.toArray());

    Assert.assertEquals(1, model.getNegatedTerms(2700).size());
    list = model.getPayload(model.getNegatedTerms(2700).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(100), 455665, 0}, list.toArray());

    Assert.assertEquals(1, model.getFamilyHistoryTerms(2800).size());
    list = model.getPayload(model.getFamilyHistoryTerms(2800).get(0));
    Assert.assertArrayEquals(new int[] {Common.daysToTime(100), 455665, 0}, list.toArray());
  }

  @Test
  public void smokeTest() throws Exception {
    PatientExport.setInstance(new PatientExportTestInstance(null));
    final MockConnection connection = new MockConnection();
    connection.addResultSet(Commands.DEMOGRAPHICS, getDemographicsExtractor());
    connection.addResultSet(Commands.PATIENT_COUNT, getPatientCnt());
    connection.addResultSet(Commands.VISITS, getVisitExtractor());
    connection.addResultSet(Commands.getTermsQuery(Commands.TERMS, 0, "patient_id"), getTermExtractor());
    connection.addResultSet(Commands.getTerminologyQuery(Commands.TERM_DICTIONARY, "terminology4"), getTidStrDictionaryExtractor());
    connection.addResultSet(Commands.MEDS, getMedsExtractor());
    connection.addResultSet(Commands.VITALS, getVitalsExtractor());
    connection.addResultSet(Commands.VITALS_VISITS, getVitalsVisitsExtractor());
    connection.addResultSet(Commands.VISIT_DX, getVisitDxExtractor());
    connection.addResultSet(Commands.NOTES, getNotesExtractor());
    connection.addResultSet(Commands.LABS_COMPONENT, getLabComponentExtractor());
    connection.addResultSet(Commands.TERM_DICTIONARY_TABLE, getTermDictionaryTable());
    Stride6DatabaseDownload.execute(new String[] {"dtuvmsnrplc", DATA_FOLDER.getAbsolutePath()}, ConnectionSettings.createFromConnection(connection));

    final Stride6Extractor extractor = new Stride6Extractor(Stride6Extractor.getDatabaseConnectorFromFolder(3, DATA_FOLDER, new ExtractionOptions("dict")),
        new StridePatientExclusion(), new DefaultEventEvaluator());
    extractor.extract(DICTIONARY_FOLDER, DATA_FOLDER, Integer.MAX_VALUE);

    Stride6DatabaseDownload.extractTermDictionary(new MockResultSet(getTidStrDictionaryExtractor()), new File(DATA_FOLDER, Stride6DatabaseDownload.TERM_DICTIONARY_FILE));

    final ClinicalSearchEngine engine = new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
    final PatientSearchModel model = new PatientSearchModel(null, engine.getDatabase().getCache(), 1);
    final Transaction transaction = Transaction.create();
    engine.getDatabase().read(transaction, model);
    assertIcd9Payload(model, engine, "250.50", new double[] {100.5, 100.5 + 1.5, 0});
    assertCptPayload(model, engine, "20000", new double[] {100.5, 100.5 + 1.5});
    assertTerms1(model, engine);
    assertVitalsVisits(model, engine);
    assertVitals(model, engine);

    model.setId(2);
    engine.getDatabase().read(transaction, model);
    assertTerms2(model, engine);

    model.setId(3);
    engine.getDatabase().read(transaction, model);
    assertIcd9Payload(model, engine, "720.20", new double[] {300.5, 300.5 + 1.8, PatientBuilder.PRIMARY_TRUE_HIERARCHY_FALSE});
    assertCptPayload(model, engine, "70000", new double[] {300.5, 300.5 + 1.8});

    Assert.assertTrue(engine.getIndices().getDemographics().containsGender(1, engine.getIndices().getDemographics().getGender().getId("male")));
    Assert.assertTrue(engine.getIndices().getDemographics().containsGender(2, engine.getIndices().getDemographics().getGender().getId("female")));
    Assert.assertTrue(engine.getIndices().getDemographics().containsGender(3, engine.getIndices().getDemographics().getGender().getId("male")));

    Assert.assertTrue(engine.getIndices().getDemographics().containsRace(1, engine.getIndices().getDemographics().getRace().getId("white")));
    Assert.assertTrue(engine.getIndices().getDemographics().containsRace(2, engine.getIndices().getDemographics().getRace().getId("asian")));
    Assert.assertTrue(engine.getIndices().getDemographics().containsRace(3, engine.getIndices().getDemographics().getRace().getId("BLACK")));

    Assert.assertTrue(engine.getIndices().getDemographics().containsEthnicity(1, engine.getIndices().getDemographics().getEthnicity().getId("HISPANIC")));
    Assert.assertTrue(engine.getIndices().getDemographics().containsEthnicity(2, engine.getIndices().getDemographics().getEthnicity().getId("UNKNOWN")));
    Assert.assertTrue(engine.getIndices().getDemographics().containsEthnicity(3, engine.getIndices().getDemographics().getEthnicity().getId("OTHER")));
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("PRIMARY(ICD9=720.20)"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=250.5"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=250"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=249-259.99"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=240-279.99"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=001-999.99"), Integer.MAX_VALUE).getPatientIds().size());

    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=250.5"), Integer.MAX_VALUE).getPatientIds().get(0)[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=250"), Integer.MAX_VALUE).getPatientIds().get(0)[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=249-259.99"), Integer.MAX_VALUE).getPatientIds().get(0)[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=240-279.99"), Integer.MAX_VALUE).getPatientIds().get(0)[0], 0.00001);
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("ICD9=001-999.99"), Integer.MAX_VALUE).getPatientIds().get(0)[0], 0.00001);

    Assert.assertEquals(3, engine.search(0, PatientSearchRequest.create("PRIMARY(ICD9=720.20)"), Integer.MAX_VALUE).getPatientIds().get(0)[0], 0.00001);
    Assert.assertEquals(0, engine.search(0, PatientSearchRequest.create("ICD9=720"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(0, engine.search(0, PatientSearchRequest.create("ICD9=720.2"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("DRUG(RX=20, ROUTE=\"oral\", STATUS=\"prescribed\")"), Integer.MAX_VALUE).getPatientIds().size());
    Assert.assertEquals(1, engine.search(0, PatientSearchRequest.create("DRUG(RX=20, ROUTE=\"oral\", STATUS=\"prescribed\")"), Integer.MAX_VALUE).getPatientIds().getFirst()[0],
        0.00001);

    Assert.assertEquals(Common.daysToTime(201), engine.getIndices().getDemographics().getDeathTime(1));
    Assert.assertEquals(Common.daysToTime(22126), engine.getIndices().getDemographics().getDeathTime(2));
    Assert.assertEquals(Common.daysToTime(22253), engine.getIndices().getDemographics().getDeathTime(3));

    engine.close();
  }
}
