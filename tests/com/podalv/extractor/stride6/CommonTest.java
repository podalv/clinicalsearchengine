package com.podalv.extractor.stride6;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.search.server.QueryUtils;
import com.podalv.utils.datastructures.SortPair;

public class CommonTest {

  private class TestClass {

    private final double val1;
    private final String val2;

    public TestClass(final double val1, final String val2) {
      this.val1 = val1;
      this.val2 = val2;
    }

    public double getVal1() {
      return val1;
    }

    public String getVal2() {
      return val2;
    }
  }

  private void assertRange(final double[] actual, final double expected1, final double expected2) {
    Assert.assertEquals(2, actual.length);
    Assert.assertEquals(actual[0], expected1, 0.0001);
    Assert.assertEquals(actual[1], expected2, 0.0001);
  }

  @Test
  public void ageRanges() throws Exception {
    final HashSet<SortPair<Integer, Integer>> ages = new HashSet<>();
    Common.addAgeRange(ages, 10, 5);
    Common.addAgeRange(ages, 5, 2);
    Common.addAgeRange(ages, 15, 20);
    Common.addAgeRange(ages, 10, 4);
    Common.addAgeRange(ages, 10, 6);
    final PersistentIntArrayList list = Common.compressAgeRangesIntArrayList(ages);
    Assert.assertArrayEquals(new int[] {5, 2, 10, 6, 15, 20}, list.toArray());
  }

  @Test
  public void minutesToYears() throws Exception {
    Assert.assertEquals(0, Common.minutesToYears((int) (0.5 * 365 * 24 * 60)));
    Assert.assertEquals(1, Common.minutesToYears(1 * 365 * 24 * 60));
    Assert.assertEquals(1, Common.minutesToYears((int) (1.5 * 365 * 24 * 60)));
    Assert.assertEquals(2, Common.minutesToYears((int) (2.1 * 365 * 24 * 60)));
    Assert.assertEquals(Integer.MAX_VALUE, Common.minutesToYears(Integer.MAX_VALUE));
    Assert.assertEquals(0, Common.minutesToYears(Integer.MIN_VALUE));
  }

  @Test
  public void minutesToDays() throws Exception {
    Assert.assertEquals(Integer.MAX_VALUE, Common.minutesToDays(Integer.MAX_VALUE), 0.00001);
    Assert.assertEquals(0, Common.minutesToDays(Integer.MIN_VALUE), 0.00001);
    Assert.assertEquals(25, Common.minutesToDays(25 * 24 * 60), 0.00001);
  }

  @Test
  public void yearsToTime() throws Exception {
    Assert.assertEquals(Integer.MAX_VALUE, Common.yearsToTime(Integer.MAX_VALUE), 0.00001);
    Assert.assertEquals(0, Common.yearsToTime(Integer.MIN_VALUE), 0.00001);
    Assert.assertEquals(2 * 365 * 24 * 60, Common.yearsToTime(2), 0.00001);
  }

  @Test
  public void daysToTime() throws Exception {
    Assert.assertEquals(Integer.MAX_VALUE, Common.daysToTime(Integer.MAX_VALUE), 0.00001);
    Assert.assertEquals(0, Common.daysToTime(Integer.MIN_VALUE), 0.00001);
    Assert.assertEquals(2.5 * 24 * 60, Common.daysToTime(2.5), 0.00001);
  }

  @Test
  public void parseHeightToInches() throws Exception {
    Assert.assertEquals(59.449, Common.parseHeightToInches("4' 11.449\""), 0.0001);
    Assert.assertEquals(65, Common.parseHeightToInches("5' 5\""), 0.0001);
    Assert.assertEquals((6 * 12) + 10, Common.parseHeightToInchesSilent("6' 10*"), 0.000001);
    Assert.assertEquals(-1, Common.parseHeightToInches("6 10"), 0.000001);
  }

  @Test
  public void parseDoubleValueRange() throws Exception {
    assertRange(Common.parseDoubleValueRange("-5 - +8"), -5.001, 8.001);
    assertRange(Common.parseDoubleValueRange("0.5-0.8"), 0.499, 0.801);
    assertRange(Common.parseDoubleValueRange("0.5 - 0.8"), 0.499, 0.801);
    assertRange(Common.parseDoubleValueRange("5 - 8"), 4.999, 8.001);
    assertRange(Common.parseDoubleValueRange("5-8"), 4.999, 8.001);
    assertRange(Common.parseDoubleValueRange("-0.5 - +0.8"), -0.501, 0.801);
    assertRange(Common.parseDoubleValueRange("> 0.5"), 0.5, Double.MAX_VALUE);
    assertRange(Common.parseDoubleValueRange(">0.5"), 0.5, Double.MAX_VALUE);
    assertRange(Common.parseDoubleValueRange("< 0.5"), Double.MIN_VALUE + 1, 0.5);
    assertRange(Common.parseDoubleValueRange("<0.5"), Double.MIN_VALUE + 1, 0.5);
    assertRange(Common.parseDoubleValueRange("> OR = 0.5"), 0.499, Double.MAX_VALUE);
    assertRange(Common.parseDoubleValueRange("> OR=0.5"), 0.499, Double.MAX_VALUE);
    assertRange(Common.parseDoubleValueRange(">=0.5"), 0.499, Double.MAX_VALUE);
    assertRange(Common.parseDoubleValueRange("< OR = 0.5"), Double.MIN_VALUE, 0.501);
    assertRange(Common.parseDoubleValueRange("< OR=0.5"), Double.MIN_VALUE, 0.501);
    assertRange(Common.parseDoubleValueRange("<=0.5"), Double.MIN_VALUE, 0.501);
    assertRange(Common.parseDoubleValueRange(">1%"), 1, Double.MAX_VALUE);
  }

  @Test
  public void timeInDaysToString() throws Exception {
    Assert.assertEquals("0.5", Common.timeInDaysToString(0.5));
    Assert.assertEquals("0.5", Common.timeInDaysToString(0.5000));
    Assert.assertEquals("1", Common.timeInDaysToString(1.00000001));
  }

  @Test
  public void getJsonString() throws Exception {
    final TestClass test = new TestClass(0.1, "abc");
    Assert.assertEquals(0.1, test.getVal1(), 0.00001);
    Assert.assertEquals("abc", test.getVal2());
    Assert.assertEquals("1", QueryUtils.toJson(Double.valueOf(1.00000000000001)));
    Assert.assertEquals("{\"val1\":1,\"val2\":\"aaa\"}", QueryUtils.toJson(new TestClass(1.000000000001, "aaa")));
  }

  @Test
  public void parseHeightToInchesSilent() throws Exception {
    Assert.assertEquals((6 * 12) + 10, Common.parseHeightToInchesSilent("6' 10\""), 0.000001);
    Assert.assertEquals(-1, Common.parseHeightToInchesSilent("6\" 10\""), 0.000001);
  }

  @Test
  public void encodeNegatedFamilyHistory() throws Exception {
    Assert.assertEquals(Common.POSITIVE_MENTION, Common.encodeNegatedFamilyHistory(0, 0));
    Assert.assertEquals(Common.NEGATED_MENTION, Common.encodeNegatedFamilyHistory(1, 0));
    Assert.assertEquals(Common.FH_MENTION, Common.encodeNegatedFamilyHistory(0, 1));
    Assert.assertEquals(Common.NEGATED_FH_MENTION, Common.encodeNegatedFamilyHistory(1, 1));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void encodeNegatedFamilyHistory_errorNegated() throws Exception {
    Assert.assertEquals(Common.NEGATED_FH_MENTION, Common.encodeNegatedFamilyHistory(2, 0));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void encodeNegatedFamilyHistory_errorFh() throws Exception {
    Assert.assertEquals(Common.NEGATED_FH_MENTION, Common.encodeNegatedFamilyHistory(0, 2));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void encodeNegatedFamilyHistory_errorNegated1() throws Exception {
    Assert.assertEquals(Common.NEGATED_FH_MENTION, Common.encodeNegatedFamilyHistory(2, 1));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void encodeNegatedFamilyHistory_errorFh1() throws Exception {
    Assert.assertEquals(Common.NEGATED_FH_MENTION, Common.encodeNegatedFamilyHistory(1, 2));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void encodeNegatedFamilyHistory_errorBoth() throws Exception {
    Assert.assertEquals(Common.NEGATED_FH_MENTION, Common.encodeNegatedFamilyHistory(2, 2));
  }

  @Test
  public void convertCelsiusToFahrenheit() throws Exception {
    Assert.assertEquals(72.5, Common.convertCelsiusToFahrenheit(22.5), 0.0001);
  }

  @Test
  public void timeValueToTimeInMinutes() throws Exception {
    Assert.assertEquals(20, Common.timeValueToTimeInMinutes("20", "MINUTES"));
    Assert.assertEquals(20 * 60, Common.timeValueToTimeInMinutes("20", "HOURS"));
    Assert.assertEquals(20 * 60 * 24 * 7, Common.timeValueToTimeInMinutes("20", "WEEKS"));
    Assert.assertEquals(20 * 60 * 24 * 30, Common.timeValueToTimeInMinutes("20", "MONTHS"));
    Assert.assertEquals(20 * 60 * 24 * 365, Common.timeValueToTimeInMinutes("20", "YEARS"));
  }
}
