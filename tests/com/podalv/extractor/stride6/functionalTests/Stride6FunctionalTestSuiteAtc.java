package com.podalv.extractor.stride6.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.atcQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.equalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.rxQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockRxRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.StringKeyObjectIterator;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

public class Stride6FunctionalTestSuiteAtc {

  private static IntKeyObjectMap<HashSet<String>>   rxToAtcMap = new IntKeyObjectMap<HashSet<String>>();
  private static StringKeyObjectMap<IntOpenHashSet> atcToRxMap = new StringKeyObjectMap<IntOpenHashSet>();

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void readTestTable() {
    BufferedReader reader = null;
    final ClassLoader classLoader = getClass().getClassLoader();
    String line = null;
    try {
      System.out.println(classLoader.getResource(".").getFile());
      reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("atc_to_rx_test.txt")));
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        final int rx = Integer.parseInt(data[1]);
        final String[] atc = parseAtc(data[0]);
        HashSet<String> set = rxToAtcMap.get(rx);
        if (set == null) {
          set = new HashSet<String>();
          rxToAtcMap.put(rx, set);
        }
        for (final String atcItem : atc) {
          set.add(atcItem);
          IntOpenHashSet rxSet = atcToRxMap.get(atcItem);
          if (rxSet == null) {
            rxSet = new IntOpenHashSet();
            atcToRxMap.put(atcItem, rxSet);
          }
          rxSet.add(rx);
        }
      }
    }
    catch (final IOException e) {
      rxToAtcMap = null;
      atcToRxMap = null;
      e.printStackTrace();
    }
    finally {
      FileUtils.close(reader);
    }
  }

  private static String[] parseAtc(final String atc) {
    final String[] result = new String[4];
    result[0] = atc;
    result[1] = atc.substring(0, 1);
    result[2] = atc.substring(0, 3);
    result[3] = atc.substring(0, 4);
    return result;
  }

  @Test
  public void testTable() throws Exception {
    readTestTable();
    Assert.assertTrue(rxToAtcMap.size() != 0);
    Assert.assertTrue(atcToRxMap.size() != 0);
    final IntKeyIntOpenHashMap rxToPatientId = new IntKeyIntOpenHashMap();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    final IntKeyObjectIterator<HashSet<String>> iterator = rxToAtcMap.entries();
    int cnt = 1;
    while (iterator.hasNext()) {
      iterator.next();
      rxToPatientId.put(iterator.getKey(), cnt);
      source.addDemographics(MockDemographicsRecord.create(cnt));
      source.addRx(MockRxRecord.create(cnt).rxCui(iterator.getKey()).age(cnt * 0.1).route("ROUTE" + cnt).status("continued").year(2012));
      cnt++;
    }
    Assert.assertTrue(cnt != 1);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);

    final ExecutorService executor = Executors.newFixedThreadPool(5);

    final StringKeyObjectIterator<IntOpenHashSet> i = atcToRxMap.entries();
    while (i.hasNext()) {
      i.next();
      final IntIterator ii = i.getValue().iterator();
      while (ii.hasNext()) {
        final int rx = ii.next();
        final int pid = rxToPatientId.get(rx);

        assertQuery(query(engine, equalQuery(rxQuery(rx), intervalQuery(daysToMinutes(pid * 0.1), daysToMinutes((pid * 0.1) + 30)))), pid);
        assertQuery(query(engine, equalQuery(atcQuery(i.getKey()), intervalQuery(daysToMinutes(pid * 0.1), daysToMinutes((pid * 0.1) + 30)))), pid);
      }
    }
    executor.shutdown();
    executor.awaitTermination(1000, TimeUnit.DAYS);
  }
}
