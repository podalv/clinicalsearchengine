package com.podalv.extractor.stride6.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.anyIcd9QueryString;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitDxRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride6FunctionalTestSuiteIcd9 {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(null).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1d).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")));
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(null).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")));
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void zeroAge() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(0).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")));
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void zeroYear() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(0).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")));
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void nullDuration() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(null).code("234.56").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void zeroDuration() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void nullType() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("NULL")));
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void emptyType() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit(""));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void nullPrimaryCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit("").sourceCode(null).sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery(icd9Query("234.56"))));
    assertQuery(query(engine, primaryQuery(icd9Query("134.45"))));
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void emptyPrimaryCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit("").sourceCode("").sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery(icd9Query("234.56"))));
    assertQuery(query(engine, primaryQuery(icd9Query("134.45"))));
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void emptyVisitDxTable() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit("").sourceCode("123").sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery(icd9Query("234.56"))));
    assertQuery(query(engine, primaryQuery(icd9Query("134.45"))));
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void nonExistingPrimaryCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit("").sourceCode("123").sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));
    src.addVisitDx(MockVisitDxRecord.create(1).primary());

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery(icd9Query("234.56"))));
    assertQuery(query(engine, primaryQuery(icd9Query("134.45"))));
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void primaryCodeWithFalse() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("234.56").year(2012).srcVisit("").sourceCode("123").sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));
    src.addVisitDx(MockVisitDxRecord.create(123).primary("N"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery(icd9Query("234.56"))));
    assertQuery(query(engine, primaryQuery(icd9Query("134.45"))));
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void primaryCodeWithTrue() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).visitId(1).duration(0).code("234.56").year(2012).srcVisit("").sourceCode("123").sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(0).code("123.45").year(2012).srcVisit("INPATIENT"));
    src.addVisitDx(MockVisitDxRecord.create(123).visitId(1).primary("y"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery(icd9Query("234.56"))), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("134.45"))));
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void negativeDuration() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(-1).code("234.56").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(1).duration(-2).code("123.45").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void invalidAgeInvalidCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(0).duration(0).code(null).year(2012).srcVisit(""));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(null).duration(0).code("").year(2012).srcVisit(""));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(0).code("").year(0).srcVisit(""));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(0).code(null).year(null).srcVisit(""));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("234.56")), 1);
  }

  @Test
  public void anyIcd9Code() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(0).code("250.50").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(3).duration(0).code("23456").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, anyIcd9QueryString()), 1);
    assertQuery(query(engine, identicalQuery(anyIcd9QueryString(), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(daysToMinutes(2), daysToMinutes(
        2))))), 1);
  }

}
