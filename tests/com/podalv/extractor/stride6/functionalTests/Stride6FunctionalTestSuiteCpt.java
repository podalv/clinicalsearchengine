package com.podalv.extractor.stride6.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.anyCptQueryString;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.rxQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockRxRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride6FunctionalTestSuiteCpt {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullCptCodes() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addDemographics(MockDemographicsRecord.create(3));
    src.addDemographics(MockDemographicsRecord.create(4));
    src.addDemographics(MockDemographicsRecord.create(5));
    src.addDemographics(MockDemographicsRecord.create(6));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).duration(1).code("43500").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.6).duration(0).code(null).year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.7).duration(1).code(null).year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.8).duration(2).code("44550").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1.6).duration(2).code(null).year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1.7).duration(0).code(null).year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 3).age(2.2).duration(1).code("43500").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(0.5).duration(1).code(null).year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(1.2).duration(2).code("44550").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(1.4).duration(3).code(null).year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 5).age(8.5).duration(2).code("43500").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(cptQuery("43500"), intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("43500"), intervalQuery(daysToMinutes(2.2), daysToMinutes(3.2)))), 3);
    assertQuery(query(engine, identicalQuery(cptQuery("43500"), intervalQuery(daysToMinutes(8.5), daysToMinutes(10.5)))), 5);
    assertQuery(query(engine, cptQuery("43500")), 1, 3, 5);
    assertQuery(query(engine, cptQuery("44550")), 1, 4);
    assertQuery(query(engine, identicalQuery(cptQuery("44550"), intervalQuery(daysToMinutes(1.2), daysToMinutes(3.2)))), 4);
    assertQuery(query(engine, identicalQuery(cptQuery("44550"), intervalQuery(daysToMinutes(1.8), daysToMinutes(3.8)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 3, 4, 5);
  }

  @Test
  public void emptyCptCodes() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addDemographics(MockDemographicsRecord.create(3));
    src.addDemographics(MockDemographicsRecord.create(4));
    src.addDemographics(MockDemographicsRecord.create(5));
    src.addDemographics(MockDemographicsRecord.create(6));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).duration(1).code("43500").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.6).duration(0).code("").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.7).duration(1).code("").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.8).duration(2).code("44550").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1.6).duration(2).code("").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1.7).duration(0).code("").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 3).age(2.2).duration(1).code("43500").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(0.5).duration(1).code("").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(1.2).duration(2).code("44550").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(1.4).duration(3).code("").year(2012));
    src.addVisit(MockVisitRecord.create(CPT, 5).age(8.5).duration(2).code("43500").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(cptQuery("43500"), intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("43500"), intervalQuery(daysToMinutes(2.2), daysToMinutes(3.2)))), 3);
    assertQuery(query(engine, identicalQuery(cptQuery("43500"), intervalQuery(daysToMinutes(8.5), daysToMinutes(10.5)))), 5);
    assertQuery(query(engine, cptQuery("43500")), 1, 3, 5);
    assertQuery(query(engine, cptQuery("44550")), 1, 4);
    assertQuery(query(engine, identicalQuery(cptQuery("44550"), intervalQuery(daysToMinutes(1.2), daysToMinutes(3.2)))), 4);
    assertQuery(query(engine, identicalQuery(cptQuery("44550"), intervalQuery(daysToMinutes(1.8), daysToMinutes(3.8)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 3, 4, 5);
  }

  @Test
  public void encounterTypes() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(21.2).duration(1).code("23000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(45.2).duration(1).code("23100").year(2012).srcVisit("OUTPATIENT").sab("IDX"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(45.2).duration(1).code("23200").year(2012).srcVisit("SURGERY").sab("DX_ID"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(55.2).duration(1).code("23300").year(2012).srcVisit("ORAL SURGERY").sab(""));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(65.2).duration(1).code("23400").year(2012).srcVisit("ORAL SURGERY").sab(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(75.2).duration(1).code("23500").year(2012).srcVisit("LETTER"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(100.2).duration(1.2).code("24500").year(2012).srcVisit("INPATIENT").sab(null));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(200.2).duration(1.2).code("25500").year(2012).srcVisit("OUTPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(cptQuery("23000"), intervalQuery(daysToMinutes(21.2), daysToMinutes(22.2)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("23100"), intervalQuery(daysToMinutes(35.2), daysToMinutes(36.2)))));
    assertQuery(query(engine, identicalQuery(cptQuery("23200"), intervalQuery(daysToMinutes(45.2), daysToMinutes(46.2)))));
    assertQuery(query(engine, identicalQuery(cptQuery("23300"), intervalQuery(daysToMinutes(55.2), daysToMinutes(56.2)))));
    assertQuery(query(engine, identicalQuery(cptQuery("23400"), intervalQuery(daysToMinutes(65.2), daysToMinutes(66.2)))));
    assertQuery(query(engine, identicalQuery(cptQuery("23500"), intervalQuery(daysToMinutes(75.2), daysToMinutes(76.2)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("24500"), intervalQuery(daysToMinutes(100.2), daysToMinutes(101.2)))));
    assertQuery(query(engine, identicalQuery(cptQuery("25500"), intervalQuery(daysToMinutes(200.2), daysToMinutes(201.4)))), 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1, 2);
    assertQuery(query(engine, visitTypeQuery("OUTPATIENT")), 1, 2);
    assertQuery(query(engine, visitTypeQuery("SURGERY")), 1);
    assertQuery(query(engine, visitTypeQuery("ORAL SURGERY")), 1);
    assertQuery(query(engine, visitTypeQuery("LETTER")), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("INPATIENT"), intervalQuery(daysToMinutes(21.2), daysToMinutes(22.2)))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("INPATIENT"), intervalQuery(daysToMinutes(100.2), daysToMinutes(101.4)))), 2);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("OUTPATIENT"), intervalQuery(daysToMinutes(200.2), daysToMinutes(201.4)))), 2);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("OUTPATIENT"), intervalQuery(daysToMinutes(45.2), daysToMinutes(46.2)))), 1);
  }

  @Test
  public void interleavedIcd9AndCpt() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addDemographics(MockDemographicsRecord.create(3));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(2).code("12345").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(3).duration(2).code("123.45").year(2012).srcVisit("OUTPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(2).code(null).year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(3).duration(3).code("23456").year(2012).srcVisit("SURGERY"));
    src.addVisit(MockVisitRecord.create(CPT, 3).age(2).duration(2).code("22222").year(2012).srcVisit("LETTER"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("12345")), 1);
    assertQuery(query(engine, cptQuery("23456")), 2);
    assertQuery(query(engine, icd9Query("123.45")), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(2), daysToMinutes(4)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("23456"), intervalQuery(daysToMinutes(3), daysToMinutes(6)))), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("22222"), intervalQuery(daysToMinutes(2), daysToMinutes(4)))), 3);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(daysToMinutes(3), daysToMinutes(5)))), 1);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1);
    assertQuery(query(engine, visitTypeQuery("OUTPATIENT")), 1);
    assertQuery(query(engine, visitTypeQuery("SURGERY")), 2);
    assertQuery(query(engine, visitTypeQuery("LETTER")), 3);
    assertQuery(query(engine, timelineQuery()), 1, 2, 3);
  }

  @Test
  public void interleavedCptAndRx() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addDemographics(MockDemographicsRecord.create(3));
    src.addDemographics(MockDemographicsRecord.create(4));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(2).code("12345").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(3).duration(1).code("22222").year(2012).srcVisit("OUTPATIENT"));
    src.addRx(MockRxRecord.create(2).age(3d).year(2012).rxCui(1234));
    src.addVisit(MockVisitRecord.create(CPT, 3).age(2).duration(2).code("33333").year(2012).srcVisit("LETTER"));
    src.addVisit(MockVisitRecord.create(CPT, 4).age(2).duration(2).code("44444").year(2012).srcVisit("LETTER"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("12345")), 1);
    assertQuery(query(engine, cptQuery("22222")), 1);
    assertQuery(query(engine, cptQuery("33333")), 3);
    assertQuery(query(engine, cptQuery("44444")), 4);
    assertQuery(query(engine, rxQuery(1234)), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(2), daysToMinutes(4)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("22222"), intervalQuery(daysToMinutes(3), daysToMinutes(4)))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("33333"), intervalQuery(daysToMinutes(2), daysToMinutes(4)))), 3);
    assertQuery(query(engine, identicalQuery(cptQuery("44444"), intervalQuery(daysToMinutes(2), daysToMinutes(4)))), 4);
    assertQuery(query(engine, identicalQuery(rxQuery(1234), intervalQuery(daysToMinutes(3), daysToMinutes(33)))), 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1);
    assertQuery(query(engine, visitTypeQuery("OUTPATIENT")), 1);
    assertQuery(query(engine, visitTypeQuery("LETTER")), 3, 4);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(null).duration(0).code("23456").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1d).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")));
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(0).code("23456").year(null).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")));
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void zeroAge() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(0).duration(0).code("23456").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")));
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void zeroYear() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(0).code("23456").year(0).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")));
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void nullDuration() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(null).code("23456").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("23456"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void zeroDuration() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(0).code("23456").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("23456"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void nullType() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(0).code("23456").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("23456"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("NULL")));
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void emptyType() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(0).code("23456").year(2012).srcVisit(""));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(0).code("12345").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("12345"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 2);
    assertQuery(query(engine, identicalQuery(cptQuery("23456"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 2);
    assertQuery(query(engine, visitTypeQuery("EMPTY")), 1);
  }

  @Test
  public void invalidAgeInvalidCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(0).duration(0).code(null).year(2012).srcVisit(""));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(null).duration(0).code("").year(2012).srcVisit(""));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(0).code("").year(0).srcVisit(""));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(0).code(null).year(null).srcVisit(""));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(0).code("23456").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("23456")), 1);
  }

  @Test
  public void anyCptCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(0).code("23456").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(0).code("25050").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(3).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, anyCptQueryString()), 1);
    assertQuery(query(engine, identicalQuery(anyCptQueryString(), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(daysToMinutes(2), daysToMinutes(
        2))))), 1);
  }

  @Test
  public void years() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).duration(1).code("43500").year(2004));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(8.5).duration(2).code("43500").year(2008));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, yearQuery(2007, 2007)));
    assertQuery(query(engine, yearQuery(2003, 2003)));
    assertQuery(query(engine, yearQuery(2005, 2005)));
    assertQuery(query(engine, yearQuery(2006, 2006)));
    assertQuery(query(engine, yearQuery(2009, 2009)));
    assertQuery(query(engine, yearQuery(2010, 2010)));
    assertQuery(query(engine, identicalQuery(yearQuery(2008, 2008), intervalQuery(daysToMinutes(8.5), daysToMinutes(8.5)))));
    assertQuery(query(engine, identicalQuery(yearQuery(2004, 2004), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))));
  }
}