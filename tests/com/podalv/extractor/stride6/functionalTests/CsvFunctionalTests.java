package com.podalv.extractor.stride6.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.allCptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.allIcd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.allLabsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.allVitalsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.csvQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.deathQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.genderQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.orQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockLabsShcRecord;
import com.podalv.extractor.test.datastructures.MockVisitDxRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE;
import com.podalv.extractor.test.datastructures.MockVitalsRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitHl7Record;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.language.node.CsvNode;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.Format;

public class CsvFunctionalTests {

  public static final String CSV_HEADER = "PID\tVARIABLE\tYEAR\tVALUE1\tVALUE2\tBOOLEAN\tSTART_AGE_IN_DAYS\tEND_AGE_IN_DAYS\tVARIABLE_DEFINITION";

  @Before
  public void setup() {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  public static ArrayList<String> readLines(final PatientSearchResponse response) throws IOException {
    return FileUtils.readFileArrayList(new File(new File("."), response.getExportLocation()), Charset.forName("UTF-8"));
  }

  @Test
  public void headerNotChanged() throws Exception {
    Assert.assertEquals(CSV_HEADER + "\n", CsvNode.getHeader());
  }

  private String generateString(final MockVisitRecord record, final HashSet<String> primaryCodes, final boolean generateValues) {
    return (record.getPatientId() + "\t" + //
        ((record.getSab().equals("ICD") || record.getSab().equals("DX_ID")) ? "ICD9" : record.getSab()) + "=" + record.getCode() + "\t" + //
        (record.getVisitYear() == null || record.getVisitYear() <= 0 ? "NA" : record.getVisitYear()) + "\t" + //
        (generateValues && (record.getSab().equals("ICD") || record.getSab().equals("DX_ID")) ? "PRIMARY=" + primaryCodes.contains(record.getCode()) : "") + "\t\t" + //
        "1\t" + Format.format(record.getAge(), 1) + "\t" + //
        Format.format(record.getAge() + record.getDuration(), 1) + "\t").toUpperCase();
  }

  private void containsVisit(final ArrayList<String> lines, final ArrayList<MockVisitRecord> visits, final HashSet<String> primaryCodes, final boolean generateValues) {
    for (final String line : lines) {
      System.out.println(line);
    }
    Assert.assertEquals(visits.size(), lines.size() - 1);
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    for (final MockVisitRecord record : visits) {
      final String line = generateString(record, primaryCodes, generateValues);
      boolean fnd = false;
      for (final String l : lines) {
        if (l.equals(line)) {
          fnd = true;
          break;
        }
      }
      Assert.assertTrue("LINE '" + line + "' NOT FOUND", fnd);
    }
  }

  private HashSet<String> getSet(final String[] values) {
    final HashSet<String> result = new HashSet<>();
    for (final String s : values) {
      result.add(s);
    }
    return result;
  }

  @Test
  public void icd9CptTests() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    cptCodes.add(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012));

    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), allIcd9Query()))), icd9Codes, getSet(new String[] {"220.50"}), true);
    containsVisit(readLines(query(engine, csvQuery(allCptQuery(), allCptQuery()))), cptCodes, getSet(new String[] {}), true);
  }

  @Test
  public void labValueTest() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addLabsShc(MockLabsShcRecord.create(1).value(1.5, null, null, null).age(12.2).name("aaa"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(1).code("25000").duration(20).year(2012).sab("CPT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), allLabsQuery())));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tLABS=aaa\t2012\t\t1.5\t1\t12.2\t12.2\t", lines.get(1));
  }

  @Test
  public void vitalsValueTest() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVitals(MockVitalsRecord.create(1).age(12.2).display("aaa").value(1.5));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(10).code("25000").duration(20).year(2012).sab("CPT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), allVitalsQuery())));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tVITALS=aaa\t2012\t1.5\t\t1\t12.2\t12.2\t", lines.get(1));
  }

  @Test
  public void booleanTest() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(orQuery(genderQuery("MALE"), genderQuery("FEMALE")), "\"male\"=" + "GENDER=\"MALE\"")));
    Assert.assertEquals(3, lines.size());
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tMALE\tNA\t\t\t1\t\t\tGENDER=\"MALE\"", lines.get(1));
    Assert.assertEquals("2\tMALE\tNA\t\t\t0\t\t\t", lines.get(2));
  }

  @Test
  public void calculatedTemporal() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    cptCodes.add(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012));

    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), icd9Query("125.50"))));
    Assert.assertEquals(3, lines.size());
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tICD9=125.50\t2012\tPRIMARY=FALSE\t\t1\t3.5\t4.5\t", lines.get(1));
    Assert.assertEquals("1\tICD9=125.50\t2013\tPRIMARY=FALSE\t\t1\t5.5\t5.5\t", lines.get(2));
  }

  @Test
  public void calculatedBoolean() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    cptCodes.add(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012));

    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), orQuery(icd9Query("125.50")))));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tOR(ICD9=125.50)\tNA\t\t\t1\t\t\t", lines.get(1));
  }

  @Test
  public void timePoint() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE").death(8.5));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(6.1).code("45000").duration(1).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), deathQuery())));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tDEATH\tNA\t\t\t1\t9.0\t9.0\t", lines.get(1));
  }

  @Test
  public void timeSpecified() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> icd9Selection = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE").death(8.5));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(6.1).code("45000").duration(1).year(2012));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Selection.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), "TIME=INTERVAL(6624 MINUTES, MAX)", icd9Query("125.50")))), icd9Selection, getSet(new String[] {}), true);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), "TIME=INTERVAL(6624 MINUTES, MAX)", allIcd9Query()))), icd9Codes, getSet(new String[] {"220.50"}), true);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("FEMALE"), "TIME=INTERVAL(6 DAYS, MAX)", allCptQuery()))), cptCodes, getSet(new String[] {}), true);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), "TIME=INTERVAL(6624 MINUTES, MAX)", deathQuery())));
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tDEATH\tNA\t\t\t1\t9.0\t9.0\t", lines.get(1));
  }

  @Test
  public void startSpecified() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> icd9Selection = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE").death(8.5));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(6.1).code("45000").duration(1).year(2012));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(1).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Selection.add(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("FEMALE"), "START=INTERVAL(6624 MINUTES, 6624 MINUTES)", allCptQuery()))), cptCodes, getSet(new String[] {}), true);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), "START=INTERVAL(6624 MINUTES, 6624 MINUTES)", icd9Query("125.50")))), icd9Selection, getSet(
        new String[] {}), true);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), "START=INTERVAL(6624 MINUTES, 6624 MINUTES)", allIcd9Query()))), icd9Codes, getSet(new String[] {"220.50"}),
        true);
    final ArrayList<String> lines = readLines(query(engine, csvQuery(genderQuery("MALE"), "START=INTERVAL(6624 MINUTES, 6624 MINUTES)", deathQuery())));
    Assert.assertEquals(CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tDEATH\tNA\t\t\t1\t9.0\t9.0\t", lines.get(1));
  }

  @Test
  public void startEndSpecified() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE").death(8.5));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(6.1).code("45000").duration(1).year(2012));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("125.50").duration(0).year(2012));
    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), "START=INTERVAL(2161 MINUTES, 2161 MINUTES)", "END=INTERVAL(5184 MINUTES, 5184 MINUTES)", icd9Query(
        "125.50")))), icd9Codes, getSet(new String[] {}), true);
  }

  @Test
  public void endSpecified() throws Exception {
    final ArrayList<MockVisitRecord> icd9Codes = new ArrayList<>();
    final ArrayList<MockVisitRecord> cptCodes = new ArrayList<>();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE").death(8.5));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    cptCodes.add(MockVisitRecord.create(CPT, 2).age(6.1).code("45000").duration(1).year(2012));

    icd9Codes.add(MockVisitRecord.create(ICD9, 1).age(1.5).code("125.50").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("125.50").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("125.50").duration(0).year(2013));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(7.5).code("220.50").duration(0).year(2013).visitId(2).sourceCode("2").sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(2).primary().visitId(2));

    for (final MockVisitRecord rec : icd9Codes) {
      source.addVisit(rec);
    }
    for (final MockVisitRecord rec : cptCodes) {
      source.addVisit(rec);
    }

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    containsVisit(readLines(query(engine, csvQuery(genderQuery("MALE"), "END=INTERVAL(2160 MINUTES, 2160 MINUTES)", icd9Query("125.50")))), icd9Codes, getSet(new String[] {}),
        true);
  }

  @Test
  public void deathYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("MALE").death(8.5).setDeathYear(2015));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(1.1, 2.2).code("250.50").year(2012));

    final ClinicalSearchEngine engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 1);
    PatientExport.setInstance(new PatientExport(EventFlowDispatcher.getInstance().getExportFolder()));
    final ArrayList<String> lines = readLines(query(engine, csvQuery("END(TIMELINE)", "END(TIMELINE)")));
    Assert.assertEquals("1\tEND(TIMELINE)\t2015\t\t\t1\t9.0\t9.0\t", lines.get(1));
  }

}
