package com.podalv.extractor.stride6.functionalTests;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.Assert;

import com.podalv.objectdb.Transaction;
import com.podalv.search.datastructures.DemographicsHistogram;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.language.node.AgeNode;
import com.podalv.search.language.node.AnyRxNormNode;
import com.podalv.search.language.node.AtcNode;
import com.podalv.search.language.node.DeathNode;
import com.podalv.search.language.node.EncountersNode;
import com.podalv.search.language.node.EthnicityNode;
import com.podalv.search.language.node.FamilyHistoryTextNode;
import com.podalv.search.language.node.GenderNode;
import com.podalv.search.language.node.HasAnyCptNode;
import com.podalv.search.language.node.HasAnyIcd9Node;
import com.podalv.search.language.node.HasCptNode;
import com.podalv.search.language.node.HasIcd10Node;
import com.podalv.search.language.node.HasIcd9Node;
import com.podalv.search.language.node.LabValuesNode;
import com.podalv.search.language.node.LabsNode;
import com.podalv.search.language.node.NegatedTextNode;
import com.podalv.search.language.node.NoteTypeNode;
import com.podalv.search.language.node.NullNode;
import com.podalv.search.language.node.PatientsNode;
import com.podalv.search.language.node.RaceNode;
import com.podalv.search.language.node.RxNormNode;
import com.podalv.search.language.node.TextNode;
import com.podalv.search.language.node.TimelineNode;
import com.podalv.search.language.node.VisitTypeNode;
import com.podalv.search.language.node.YearNode;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.workshop.datastructures.DumpRequest;
import com.podalv.workshop.datastructures.DumpResponse;

public class Stride6FunctionalTestCommon {

  public static final File DATA_FOLDER = new File(new File("."), "extractionTestData");

  private static String complexQuery(final String name, final String ... arguments) {
    final StringBuilder result = new StringBuilder(name + "(");
    for (int x = 0; x < arguments.length; x++) {
      result.append(arguments[x]);
      if (x != arguments.length - 1) {
        result.append(",");
      }
    }
    result.append(")");
    return result.toString();
  }

  public static String countQuery(final String command, final String num1, final String num2) {
    return "COUNT(" + command + ", " + num1 + ", " + num2 + ")";
  }

  public static String departmentQuery(final String departmentName) {
    return "DEPT=\"" + departmentName + "\"";
  }

  public static String countQuery(final String command, final String command1, final String type, final String num1, final String num2) {
    return "COUNT(" + command + ", " + command1 + ", " + type + ", " + num1 + ", " + num2 + ")";
  }

  public static String durationQuery(final String command, final String type, final String num1, final String num2) {
    return "DURATION(" + command + ", " + type + ", " + num1 + ", " + num2 + ")";
  }

  public static String durationQuery(final String command, final String command1, final String type, final String num1, final String num2) {
    return "DURATION(" + command + ", " + command1 + ", " + type + ", " + num1 + ", " + num2 + ")";
  }

  public static String nullQuery() {
    return new NullNode().toString();
  }

  public static String intersectQuery(final String ... arguments) {
    return complexQuery("INTERSECT", arguments);
  }

  public static String invertQuery(final String argument) {
    return "INVERT(" + argument + ")";
  }

  public static String ageQuery(final int age1, final int age2) {
    return new AgeNode(age1, age2).toString();
  }

  public static String deathQuery() {
    return new DeathNode().toString();
  }

  public static String textQuery(final String text) {
    return new TextNode(text).toString();
  }

  public static String textNegatedQuery(final String text) {
    return new NegatedTextNode(text).toString();
  }

  public static String notesQuery() {
    return "NOTES";
  }

  public static String resizeQuery(final String argument, final String time1, final String time2) {
    return "RESIZE(" + argument + ", " + time1 + ", " + time2 + ")";
  }

  public static String firstMentionQuery(final String argument) {
    return "FIRST MENTION(" + argument + ")";
  }

  public static String lastMentionQuery(final String argument) {
    return "LAST MENTION(" + argument + ")";
  }

  public static String textFhQuery(final String text) {
    return new FamilyHistoryTextNode(text).toString();
  }

  public static String noteTypeQuery(final String noteType) {
    return new NoteTypeNode(noteType).toString();
  }

  public static String yearQuery(final int start, final int end) {
    return new YearNode(start, end).toString();
  }

  public static String identicalQuery(final String ... arguments) {
    return complexQuery("IDENTICAL", arguments);
  }

  public static int daysToMinutes(final double days) {
    return (int) Math.round(days * 24 * 60);
  }

  public static String intervalQuery(final String ... arguments) {
    return complexQuery("INTERVAL", arguments);
  }

  public static String encountersQuery() {
    return EncountersNode.getInstance().toString();
  }

  public static String visitTypeQuery(final String type) {
    return new VisitTypeNode(type).toString();
  }

  public static String intervalQuery(final int ... arguments) {
    final String[] args = new String[arguments.length];
    for (int x = 0; x < arguments.length; x++) {
      args[x] = arguments[x] + "";
    }
    return complexQuery("INTERVAL", args);
  }

  public static String andQuery(final String ... arguments) {
    return complexQuery("AND", arguments);
  }

  public static String csvQuery(final String ... arguments) {
    return complexQuery("CSV", arguments);
  }

  public static String cptQuery(final String cpt) {
    return new HasCptNode(cpt).toString();
  }

  public static String rxQuery(final int rxId) {
    return new RxNormNode(rxId).toString();
  }

  public static String anyRxQuery() {
    return new AnyRxNormNode().toString();
  }

  public static String labsQuery(final String name) {
    return new LabsNode(name).toString();
  }

  public static String labsQuery(final String name, final String value) {
    return new LabsNode(name, value).toString();
  }

  public static String labsQuery(final String name, final double min, final double max) {
    return new LabValuesNode(name, min, max).toString();
  }

  public static String icd9Query(final String icd9) {
    return new HasIcd9Node(icd9).toString();
  }

  public static String icd10Query(final String icd10) {
    return new HasIcd10Node(icd10).toString();
  }

  public static String anyIcd9QueryString() {
    return new HasAnyIcd9Node().toString();
  }

  public static String anyCptQueryString() {
    return new HasAnyCptNode().toString();
  }

  public static String orQuery(final String ... arguments) {
    return complexQuery("OR", arguments);
  }

  public static String vitalsQuery(final String name, final String min, final String max) {
    return "VITALS(\"" + name + "\", " + min + ", " + max + ")";
  }

  public static String allVitalsQuery() {
    return "VITALS";
  }

  public static String allLabsQuery() {
    return "LABS";
  }

  public static String allIcd9Query() {
    return "ICD9";
  }

  public static String allCptQuery() {
    return "CPT";
  }

  public static String notQuery(final String argument) {
    return "NOT(" + argument + ")";
  }

  public static String genderQuery(final String gender) {
    return new GenderNode(gender).toString();
  }

  public static String raceQuery(final String race) {
    return new RaceNode(race).toString();
  }

  public static String ethnicityQuery(final String ethnicity) {
    return new EthnicityNode(ethnicity).toString();
  }

  public static String timelineQuery() {
    return new TimelineNode().toString();
  }

  public static String noteQuery(final String ... arguments) {
    return complexQuery("NOTE", arguments);
  }

  public static PatientSearchResponse query(final ClinicalSearchEngine engine, final String query) {
    return engine.search(0, PatientSearchRequest.create(query));
  }

  public static DumpResponse dump(final ClinicalSearchEngine engine, final int patientId) {
    return engine.dump(Transaction.create(), DumpRequest.createFull(patientId));
  }

  public static DumpResponse dump(final ClinicalSearchEngine engine, final int patientId, final String query, final boolean containsStart, final boolean containsEnd) {
    return engine.dump(Transaction.create(), DumpRequest.createFull(patientId).setQuery(query, containsStart, containsEnd));
  }

  public static String primaryQuery(final String query) {
    return "PRIMARY(" + query + ")";
  }

  public static String originalQuery(final String query) {
    return "ORIGINAL(" + query + ")";
  }

  public static String startQuery(final String query) {
    return "START(" + query + ")";
  }

  public static String endQuery(final String query) {
    return "END(" + query + ")";
  }

  public static String statusQuery(final Integer rxNorm, final String status) {
    return "DRUG(RX=" + rxNorm + ", STATUS=\"" + status + "\")";
  }

  public static String routeQuery(final Integer rxNorm, final String route) {
    return "DRUG(RX=" + rxNorm + ", ROUTE=\"" + route + "\")";
  }

  public static String drugQuery(final Integer rxNorm, final String route, final String status) {
    return "DRUG(RX=" + rxNorm + ", ROUTE=\"" + route + "\", STATUS=\"" + status + "\")";
  }

  public static String atcQuery(final String atc) {
    return new AtcNode(atc).toString();
  }

  public static String equalQuery(final String ... arguments) {
    return complexQuery("EQUAL", arguments);
  }

  public static String unionQuery(final String ... arguments) {
    return complexQuery("UNION", arguments);
  }

  public static String patientsQuery(final int ... patients) {
    final ArrayList<String> parameters = new ArrayList<>();
    for (final int patient : patients) {
      parameters.add(patient + "");
    }
    return new PatientsNode(parameters).toString();
  }

  public static void assertError(final PatientSearchResponse response, final String error) {
    Assert.assertTrue(response.getErrorMessage().indexOf(error) != -1);
  }

  public static void assertQuery(final PatientSearchResponse response, final int ... patientIds) {
    final LinkedList<double[]> list = response.getPatientIds();
    int pos = 0;
    Assert.assertEquals(patientIds.length, list.size());
    for (final double[] patientId : list) {
      Assert.assertEquals(patientIds[pos++], patientId[0], 0.00001);
    }
    if (response.getErrorMessage() != null && !response.getErrorMessage().isEmpty()) {
      Assert.fail(response.getErrorMessage());
    }
  }

  public static void assertGenderHistograms(final PatientSearchResponse response, final int maleCohort, final int femaleCohort, final int maleGeneral, final int femaleGeneral) {
    final ArrayList<DemographicsHistogram> histograms = response.getGenderHistogram();
    int actualCohortMale = 0;
    int actualCohortFemale = 0;
    int actualGeneralMale = 0;
    int actualGeneralFemale = 0;
    for (final DemographicsHistogram histogram : histograms) {
      if (response.getCohortPatientCnt() != 0) {
        Assert.assertEquals(histogram.getCohortCnt() / (double) response.getCohortPatientCnt(), histogram.getCohortPercentage(), 0.001);
      }
      if (response.getCohortPatientCnt() != 0) {
        Assert.assertEquals(histogram.getGeneralCnt() / (double) response.getGeneralPatientCnt(), histogram.getGeneralPercentage(), 0.001);
      }
      if (histogram.getLabel().equalsIgnoreCase("MALE")) {
        actualCohortMale = histogram.getCohortCnt();
        actualGeneralMale = histogram.getGeneralCnt();
      }
      if (histogram.getLabel().equalsIgnoreCase("FEMALE")) {
        actualCohortFemale = histogram.getCohortCnt();
        actualGeneralFemale = histogram.getGeneralCnt();
      }
    }
    Assert.assertEquals(maleCohort, actualCohortMale);
    Assert.assertEquals(femaleCohort, actualCohortFemale);
    Assert.assertEquals(maleCohort, actualCohortMale);
    Assert.assertEquals(femaleCohort, actualCohortFemale);
    Assert.assertEquals(maleGeneral, actualGeneralMale);
    Assert.assertEquals(femaleGeneral, actualGeneralFemale);
  }

  public static void assertRaceHistograms(final PatientSearchResponse response, final String race, final int cohort, final int general) {
    final ArrayList<DemographicsHistogram> histograms = response.getRaceHistogram();
    int actualCohort = 0;
    int actualGeneral = 0;
    for (final DemographicsHistogram histogram : histograms) {
      if (response.getCohortPatientCnt() != 0) {
        Assert.assertEquals(histogram.getCohortCnt() / (double) response.getCohortPatientCnt(), histogram.getCohortPercentage(), 0.001);
      }
      if (response.getCohortPatientCnt() != 0) {
        Assert.assertEquals(histogram.getGeneralCnt() / (double) response.getGeneralPatientCnt(), histogram.getGeneralPercentage(), 0.001);
      }
      if (histogram.getLabel().equalsIgnoreCase(race)) {
        actualCohort = histogram.getCohortCnt();
        actualGeneral = histogram.getGeneralCnt();
        break;
      }
    }
    Assert.assertEquals(cohort, actualCohort);
    Assert.assertEquals(general, actualGeneral);
  }

  public static void assertRaceHistogramsEmpty(final PatientSearchResponse response, final String ... races) {
    final ArrayList<DemographicsHistogram> histograms = response.getRaceHistogram();
    for (final String race : races) {
      int actualCohort = 0;
      int actualGeneral = 0;
      for (final DemographicsHistogram histogram : histograms) {
        if (response.getCohortPatientCnt() != 0) {
          Assert.assertEquals(histogram.getCohortCnt() / (double) response.getCohortPatientCnt(), histogram.getCohortPercentage(), 0.001);
        }
        if (response.getCohortPatientCnt() != 0) {
          Assert.assertEquals(histogram.getGeneralCnt() / (double) response.getGeneralPatientCnt(), histogram.getGeneralPercentage(), 0.001);
        }
        if (histogram.getLabel().equalsIgnoreCase(race)) {
          actualCohort = 0;
          actualGeneral = 0;
          break;
        }
      }
      Assert.assertEquals(0, actualCohort);
      Assert.assertEquals(0, actualGeneral);
    }
  }

}
