package com.podalv.extractor.stride6.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.andQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.orQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride6FunctionalTestSuiteBoolean {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  protected ClinicalSearchEngine getEngine() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(12.5).code("250.50").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(12.5).code("300.00").duration(0).year(2012));

    source.addVisit(MockVisitRecord.create(ICD9, 2).age(12.5).code("100.00").duration(0).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(12.5).code("33500").duration(0).year(2012));

    return Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
  }

  @Test
  public void andCommand() throws Exception {
    final ClinicalSearchEngine engine = getEngine();
    assertQuery(query(engine, andQuery("NULL")));
    assertQuery(query(engine, andQuery(cptQuery("43500"))), 1);
    assertQuery(query(engine, andQuery(cptQuery("43500"), icd9Query("250.50"), icd9Query("300.00"))), 1);
    assertQuery(query(engine, andQuery(icd9Query("250.50"), icd9Query("222.22"))));
    assertQuery(query(engine, identicalQuery(andQuery(cptQuery("43500")), intervalQuery(daysToMinutes(1.5), daysToMinutes(12.5)))), 1);
  }

  @Test
  public void orCommand() throws Exception {
    final ClinicalSearchEngine engine = getEngine();
    assertQuery(query(engine, orQuery("NULL")));
    assertQuery(query(engine, orQuery(cptQuery("43500"))), 1);
    assertQuery(query(engine, orQuery(cptQuery("43500"), icd9Query("250.50"), icd9Query("300.00"))), 1);
    assertQuery(query(engine, orQuery(icd9Query("250.50"), icd9Query("222.22"))), 1);
    assertQuery(query(engine, identicalQuery(orQuery(icd9Query("250.50"), icd9Query("222.22")), intervalQuery(daysToMinutes(1.5), daysToMinutes(12.5)))), 1);
  }

  @Test
  public void notCommand() throws Exception {
    final ClinicalSearchEngine engine = getEngine();
    assertQuery(query(engine, notQuery("NULL")), 1, 2);
    assertQuery(query(engine, notQuery(cptQuery("43500"))), 2);
    assertQuery(query(engine, notQuery(andQuery(cptQuery("43500"), icd9Query("250.50"), icd9Query("300.00")))), 2);
    assertQuery(query(engine, notQuery(orQuery(icd9Query("250.50"), icd9Query("222.22")))), 2);
    assertQuery(query(engine, identicalQuery(notQuery(orQuery(icd9Query("250.50"), icd9Query("222.22"))), intervalQuery(daysToMinutes(1.5), daysToMinutes(12.5)))));
  }

}
