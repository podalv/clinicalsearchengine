package com.podalv.extractor.stride6;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.podalv.extractor.datastructures.CommonWordsFilter;
import com.podalv.extractor.test.OhdsiTestHarness;
import com.podalv.extractor.test.datastructures.OhdsiTestDataSource;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestCondition;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestDemographics;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestDrugs;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestMeasurement;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestNotes;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestObservation;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestProcedures;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.PatientExport;
import com.podalv.search.server.PatientExportTestInstance;
import com.podalv.utils.text.Format;

public class OhdsiExtractorTest {

  private static final File   DATA_FOLDER = new File(new File("."), "extractionTestData");
  private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  private static Date         startDate   = null;

  @BeforeClass
  public static void init() {
    try {
      initCommonWords();
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
  }

  @Before
  public void cleanup() {
    UmlsDictionary.clearCache();
    if (DATA_FOLDER.listFiles() != null) {
      final File[] files = DATA_FOLDER.listFiles();
      if (files != null) {
        for (final File file : files) {
          file.setWritable(true);
          if (!file.delete()) {
            System.out.println("Could not remove file '" + file.getAbsolutePath() + "'");
          }
        }
      }
    }
    PatientExport.setInstance(new PatientExportTestInstance(DATA_FOLDER));
  }

  private static void initCommonWords() throws IOException {
    final Iterator<String> iterator = CommonWordsFilter.getCommonWords();
    final StringBuilder commonTids = new StringBuilder();
    int cnt = Integer.MAX_VALUE;
    while (iterator.hasNext()) {
      commonTids.append((cnt--) + "\t" + iterator.next() + "\n");
    }
    CommonWordsFilter.init(new ByteArrayInputStream(commonTids.toString().getBytes(Charset.forName("UTF-8"))));
  }

  @After
  public void clean() {
    cleanup();
  }

  private ClinicalSearchEngine prepareDemographicsTestData() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final OhdsiTestDataSource source = new OhdsiTestDataSource();
    source.add(OhdsiTestDemographics.add(1).gender("MALE").race("ASIAN").ethnicity("UNKNOWN").dob(2012, 1, 1).death(2012, 10, 10));
    source.add(OhdsiTestDemographics.add(2).gender("FEMALE").race("WHITE").ethnicity("HISPANIC").dob(2013, 1, 1).death(2013, 5, 5));
    source.add(OhdsiTestDemographics.add(3).gender("OTHER").race("BLACK").ethnicity("NOT HISPANIC").dob(2014, 1, 1).death(2014, 7, 7));

    source.add(OhdsiTestNotes.add(1).date("2012-01-01", "10:00:00").noteId(1).noteType(source.addConcept("INPATIENT")).positiveTerms(new String[] {"diabetes"}));
    source.add(OhdsiTestNotes.add(2).date("2013-01-01", "10:00:00").noteId(2).noteType(source.addConcept("OUTPATIENT")).positiveTerms(new String[] {"diabetes"}));
    source.add(OhdsiTestNotes.add(3).date("2014-01-01", "10:00:00").noteId(3).noteType(source.addConcept("INPATIENT")).positiveTerms(new String[] {"diabetes"}));
    return OhdsiTestHarness.engine(source, DATA_FOLDER, new File("."), 3);
  }

  private ClinicalSearchEngine prepareIcd9TestData() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final OhdsiTestDataSource source = new OhdsiTestDataSource();
    source.add(OhdsiTestDemographics.add(1).gender("MALE").race("ASIAN").ethnicity("UNKNOWN").dob(2012, 1, 1).death(2015, 10, 10));
    source.add(OhdsiTestDemographics.add(2).gender("FEMALE").race("WHITE").ethnicity("HISPANIC").dob(2013, 1, 1).death(2015, 5, 5));

    source.add(OhdsiTestCondition.add(1).primaryDiagnosis().conditionConceptId(source.addIcd9("250.00", "Diabetes")).visitConceptId(source.addConcept("INPATIENT")).startTime(
        "2012-10-10", "15:35:00").endTime("2012-10-12", "16:00:00"));
    source.add(OhdsiTestCondition.add(1).conditionConceptId(source.addIcd9("250.00", "Diabetes")).visitConceptId(source.addConcept("OUTPATIENT")).startTime("2012-12-10",
        "15:35:00").endTime("2012-12-12", "16:00:00"));
    source.add(OhdsiTestCondition.add(2).conditionConceptId(source.addIcd9("255.00", "Diabetes no complication")).visitConceptId(source.addConcept("ELSE")).startTime("2013-3-3",
        "15:35:00").endTime("2013-3-3", "15:35:00"));
    source.add(OhdsiTestCondition.add(2).conditionConceptId(source.addSnomed("11223345", "SNOMED code")).visitConceptId(source.addConcept("SURGERY")).startTime("2013-5-5",
        "15:35:00").endTime("2013-5-5", "15:35:00"));

    source.add(OhdsiTestObservation.add(1).concept(source.addSnomed("111222", "OBSERVATION code")).date("2012-12-12", "18:00:00"));
    source.add(OhdsiTestObservation.add(1).concept(source.addIcd9("250.55", "Diabetes x")).date("2012-12-13", "19:00:00"));
    source.add(OhdsiTestObservation.add(1).concept(source.addCpt("25055", "Diabetes CPT")).date("2012-12-14", "20:00:00"));
    return OhdsiTestHarness.engine(source, DATA_FOLDER, new File("."), 3);
  }

  private ClinicalSearchEngine prepareCptTestData() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final OhdsiTestDataSource source = new OhdsiTestDataSource();
    source.add(OhdsiTestDemographics.add(1).gender("MALE").race("ASIAN").ethnicity("UNKNOWN").dob(2012, 1, 1).death(2012, 10, 10));
    source.add(OhdsiTestDemographics.add(2).gender("FEMALE").race("WHITE").ethnicity("HISPANIC").dob(2013, 1, 1).death(2013, 5, 5));

    source.add(OhdsiTestProcedures.add(1).date("2012-05-05", "01:00:00").procedureConceptId(source.addCpt("45678", "Cpt")).visitConceptId(1));
    source.add(OhdsiTestProcedures.add(1).date("2012-05-05", "01:00:00").procedureConceptId(source.addCpt("45679", "Cpt")).visitConceptId(1));
    source.add(OhdsiTestProcedures.add(2).date("2013-05-05", "01:00:00").procedureConceptId(source.addCpt("45680", "Cpt")).visitConceptId(2));
    return OhdsiTestHarness.engine(source, DATA_FOLDER, new File("."), 3);
  }

  private ClinicalSearchEngine prepareRxNormTestData() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final OhdsiTestDataSource source = new OhdsiTestDataSource();
    source.add(OhdsiTestDemographics.add(1).gender("MALE").race("ASIAN").ethnicity("UNKNOWN").dob(2012, 1, 1));
    source.add(OhdsiTestDemographics.add(2).gender("FEMALE").race("WHITE").ethnicity("HISPANIC").dob(2013, 1, 1).death(2015, 5, 5));

    source.add(OhdsiTestDrugs.add(1).drugConceptId(source.addRxNorm("12345", "drug")).drugTypeConceptId(source.addConcept("drug type")).routeConceptId(source.addConcept(
        "intravenous")).startTime("2012-05-05", "10:00:00").endTime("2012-06-06", "10:00:00"));
    source.add(OhdsiTestDrugs.add(2).drugConceptId(source.addRxNorm("22345", "drug2")).drugTypeConceptId(source.addConcept("drug type2")).routeConceptId(source.addConcept(
        "oral")).startTime("2013-05-05", "10:00:00").endTime("2013-06-06", "10:00:00"));

    return OhdsiTestHarness.engine(source, DATA_FOLDER, new File("."), 3);
  }

  private ClinicalSearchEngine prepareLabsTestData() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final OhdsiTestDataSource source = new OhdsiTestDataSource();
    source.add(OhdsiTestDemographics.add(1).gender("FEMALE").race("WHITE").ethnicity("HISPANIC").dob(2013, 1, 1).death(2013, 5, 5));

    source.add(OhdsiTestMeasurement.add(1).date("2013-04-04", "10:00:00").vitalsValue(21.5).measurementConceptId(source.addConcept("BMI")));

    return OhdsiTestHarness.engine(source, DATA_FOLDER, new File("."), 3);
  }

  private ClinicalSearchEngine prepareNotesTestData() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final OhdsiTestDataSource source = new OhdsiTestDataSource();
    source.add(OhdsiTestDemographics.add(1).gender("MALE").race("ASIAN").ethnicity("UNKNOWN").dob(2012, 1, 1).death(2012, 10, 10));
    source.add(OhdsiTestDemographics.add(2).gender("FEMALE").race("WHITE").ethnicity("HISPANIC").dob(2013, 1, 1).death(2013, 5, 5));

    source.add(OhdsiTestNotes.add(1).date("2012-03-03", "10:00:00").familyHistoryTerms(new String[] {"fh1", "fh2"}).negatedTerms(new String[] {"neg1", "neg2"}).positiveTerms(
        new String[] {"pos1", "pos2"}).noteId(1).noteType(source.addConcept("Discharge note")));
    source.add(OhdsiTestNotes.add(1).date("2012-04-04", "10:00:00").familyHistoryTerms(new String[] {"fh3", "fh4"}).negatedTerms(new String[] {"neg3", "neg4"}).positiveTerms(
        new String[] {"pos3", "pos4"}).noteId(2).noteType(source.addConcept("Discharge note")));

    source.add(OhdsiTestNotes.add(2).date("2013-04-04", "10:00:00").familyHistoryTerms(new String[] {"fh1", "fh2"}).negatedTerms(new String[] {"neg1", "neg2"}).positiveTerms(
        new String[] {"pos1", "pos2"}).noteId(3).noteType(source.addConcept("Progress note")));

    return OhdsiTestHarness.engine(source, DATA_FOLDER, new File("."), 3);
  }

  private void assertQuery(final ClinicalSearchEngine engine, final String query, final int ... patientIds) {
    final LinkedList<double[]> list = engine.search(0, PatientSearchRequest.create(query)).getPatientIds();
    int pos = 0;
    Assert.assertEquals(patientIds.length, list.size());
    for (final double[] patientId : list) {
      Assert.assertEquals(patientIds[pos++], patientId[0], 0.000001);
    }
  }

  public static int getMinutes(final String date1, final String date2) {
    startDate = Format.format(date1, DATE_FORMAT);
    return getMinutes(date2);
  }

  public static int getMinutes(final String date2) {
    return (int) Math.round((OhdsiDatabaseDownload.calculateDays(OhdsiDatabaseDownload.getOffset(startDate, Format.format(date2, DATE_FORMAT))) * 24 * 60));
  }

  public static String getInterval(final String dob, final String ... dates) {
    final StringBuilder result = new StringBuilder("UNION(");
    result.append("INTERVAL(" + getMinutes(dob, dates[0]));
    for (int x = 1; x < dates.length; x++) {
      if (x % 2 == 0) {
        result.append(",INTERVAL(" + getMinutes(dates[x]));
      }
      else {
        result.append(", " + getMinutes(dates[x]) + ")");
      }
    }
    result.append(")");
    return result.toString();
  }

  public static String addTime(final String initialDate, final double incrementInDays) throws ParseException {
    final Date date = new SimpleDateFormat(DATE_FORMAT).parse(initialDate);
    date.setTime(date.getTime() + Math.round(incrementInDays * 24 * 60 * 60 * 1000));
    return new SimpleDateFormat(DATE_FORMAT).format(date);
  }

  @Test
  public void rxNorm() throws Exception {
    final ClinicalSearchEngine engine = prepareRxNormTestData();
    try {
      assertQuery(engine, "IDENTICAL(RX=12345, " + getInterval("2012-01-01 00:00:00", "2012-05-05 10:00:00", addTime("2012-06-06 10:00:00", 30)) + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(RX=22345, " + getInterval("2013-01-01 00:00:00", "2013-05-05 10:00:00", addTime("2013-06-06 10:00:00", 30)) + ")", new int[] {2});
      assertQuery(engine, "RX=12345", 1);
      assertQuery(engine, "RX=22345", 2);
      assertQuery(engine, "DRUG(RX=12345, ROUTE=\"ORAL\")");
      assertQuery(engine, "DRUG(RX=22345, ROUTE=\"ORAL\")", 2);
      assertQuery(engine, "DRUG(RX=12345, ROUTE=\"INTRAVENOUS\")", 1);
      assertQuery(engine, "DRUG(RX=12345, STATUS=\"DRUG TYPE\")", 1);
    }
    finally {
      engine.close();
    }
  }

  @Test
  public void cpt() throws Exception {
    final ClinicalSearchEngine engine = prepareCptTestData();
    try {
      assertQuery(engine, "CPT=45678", 1);
      assertQuery(engine, "CPT=45679", 1);
      assertQuery(engine, "CPT=45680", 2);
      assertQuery(engine, "IDENTICAL(CPT=45678, " + getInterval("2012-01-01 00:00:00", "2012-05-05 01:00:00", "2012-05-05 01:00:00") + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(CPT=45679, " + getInterval("2012-01-01 00:00:00", "2012-05-05 01:00:00", "2012-05-05 01:00:00") + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(CPT=45680, " + getInterval("2013-01-01 00:00:00", "2013-05-05 01:00:00", "2013-05-05 01:00:00") + ")", new int[] {2});
    }
    finally {
      engine.close();
    }
  }

  @Test
  public void icd9() throws Exception {
    final ClinicalSearchEngine engine = prepareIcd9TestData();
    try {
      assertQuery(engine, "IDENTICAL(PRIMARY(ICD9=250.00), " + getInterval("2012-01-01 00:00:00", "2012-10-10 15:35:00", addTime("2012-10-12 16:00:00", 0)) + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(VISIT TYPE=\"INPATIENT\", " + getInterval("2012-01-01 00:00:00", "2012-10-10 15:35:00", addTime("2012-10-12 16:00:00", 0)) + ")", new int[] {
          1});
      assertQuery(engine, "IDENTICAL(VISIT TYPE=\"OUTPATIENT\", " + getInterval("2012-01-01 00:00:00", "2012-12-10 15:35:00", addTime("2012-12-12 16:00:00", 0)) + ")", new int[] {
          1});
      assertQuery(engine, "IDENTICAL(VISIT TYPE=\"ELSE\", " + getInterval("2013-01-01 00:00:00", "2013-03-03 15:35:00", addTime("2013-03-03 15:35:00", 0)) + ")", new int[] {2});
      assertQuery(engine, "IDENTICAL(ICD9=255.00, " + getInterval("2013-01-01 00:00:00", "2013-03-03 15:35:00", addTime("2013-03-03 15:35:00", 0)) + ")", new int[] {2});
      assertQuery(engine, "IDENTICAL(SNOMED=11223345, " + getInterval("2013-01-01 00:00:00", "2013-05-05 15:35:00", addTime("2013-05-05 15:35:00", 0)) + ")", new int[] {2});
      assertQuery(engine, "IDENTICAL(SNOMED=111222, " + getInterval("2012-01-01 00:00:00", "2012-12-12 18:00:00", "2012-12-12 18:00:00") + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(ICD9=250.55, " + getInterval("2012-01-01 00:00:00", "2012-12-13 19:00:00", "2012-12-13 19:00:00") + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(CPT=25055, " + getInterval("2012-01-01 00:00:00", "2012-12-14 20:00:00", "2012-12-14 20:00:00") + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(VISIT TYPE=\"SURGERY\", " + getInterval("2013-01-01 00:00:00", "2013-05-05 15:35:00", addTime("2013-05-05 15:35:00", 0)) + ")", new int[] {2});
      assertQuery(engine, "IDENTICAL(ICD9=250.00, " + getInterval("2012-01-01 00:00:00", "2012-10-10 15:35:00", addTime("2012-10-12 16:00:00", 0), "2012-12-10 15:35:00", addTime(
          "2012-12-12 16:00:00", 0)) + ")", new int[] {1});
      assertQuery(engine, "PRIMARY(ICD9=255.00)");
      assertQuery(engine, "PRIMARY(ICD9=250.00)", 1);
      assertQuery(engine, "RACE=\"BLACK\"");
      assertQuery(engine, "ICD9=250.00", 1);
      assertQuery(engine, "ICD9=255.00", 2);
      assertQuery(engine, "VISIT TYPE=\"INPATIENT\"", 1);
      assertQuery(engine, "VISIT TYPE=\"OUTPATIENT\"", 1);
      assertQuery(engine, "VISIT TYPE=\"ELSE\"", 2);
      assertQuery(engine, "VISIT TYPE=\"SURGERY\"", 2);
      assertQuery(engine, "ICD9=255.00", 2);

      assertQuery(engine, "SNOMED", 1, 2);
    }
    finally {
      engine.close();
    }
  }

  @Test
  public void demographics() throws Exception {
    final ClinicalSearchEngine engine = prepareDemographicsTestData();
    try {
      // date of death had to be adjusted due to rounding error (the difference is less than 1 minute)
      assertQuery(engine, "IDENTICAL(INTERSECT(GENDER=\"FEMALE\", DEATH), " + getInterval("2013-01-01 00:00:00", "2013-05-05 00:00:00", "2013-05-05 00:00:00") + ")", new int[] {
          2});
      assertQuery(engine, "GENDER=\"FEMALE\"", 2);
      assertQuery(engine, "GENDER=\"OTHER\"", 3);

      assertQuery(engine, "YEAR(2012, 2012)", 1);
      assertQuery(engine, "YEAR(2013, 2013)", 2);
      assertQuery(engine, "YEAR(2014, 2014)", 3);

      assertQuery(engine, "PATIENTS(1, 2, 3)", 1, 2, 3);

      assertQuery(engine, "RACE=\"ASIAN\"", 1);
      assertQuery(engine, "RACE=\"WHITE\"", 2);
      assertQuery(engine, "RACE=\"BLACK\"", 3);

      assertQuery(engine, "ETHNICITY=\"UNKNOWN\"", 1);
      assertQuery(engine, "ETHNICITY=\"HISPANIC\"", 2);
      assertQuery(engine, "ETHNICITY=\"NOT HISPANIC\"", 3);

      assertQuery(engine, "DEATH", 1, 2, 3);
    }
    finally {
      engine.close();
    }
  }

  @Test
  public void labs() throws Exception {
    final ClinicalSearchEngine engine = prepareLabsTestData();
    try {
      assertQuery(engine, "VITALS(\"BMI\", 20, 30)", 1);
      assertQuery(engine, "VITALS(\"BMI\", 25, 30)");
      assertQuery(engine, "IDENTICAL(VITALS(\"BMI\", 20, 30), " + getInterval("2013-01-01 00:00:00", "2013-04-04 10:00:00", "2013-04-04 10:00:00") + ")", new int[] {1});
    }
    finally {
      engine.close();
    }
  }

  @Test
  public void notes() throws Exception {
    final ClinicalSearchEngine engine = prepareNotesTestData();
    try {
      assertQuery(engine, "!TEXT=\"neg1\"", 1, 2);
      assertQuery(engine, "NOTE TYPE=\"discharge note\"", 1);
      assertQuery(engine, "NOTE TYPE=\"Progress note\"", 2);
      assertQuery(engine, "TEXT=\"pos1\"", 1, 2);
      assertQuery(engine, "TEXT=\"pos2\"", 1, 2);
      assertQuery(engine, "TEXT=\"pos3\"", 1);
      assertQuery(engine, "TEXT=\"pos4\"", 1);
      assertQuery(engine, "!TEXT=\"neg2\"", 1, 2);
      assertQuery(engine, "!TEXT=\"neg3\"", 1);
      assertQuery(engine, "!TEXT=\"neg4\"", 1);
      assertQuery(engine, "~TEXT=\"fh1\"", 1, 2);
      assertQuery(engine, "~TEXT=\"fh2\"", 1, 2);
      assertQuery(engine, "~TEXT=\"fh3\"", 1);
      assertQuery(engine, "~TEXT=\"fh4\"", 1);
      assertQuery(engine, "NOTE(TEXT=\"pos1\", !TEXT=\"neg1\", ~TEXT=\"fh1\")", 1, 2);
      assertQuery(engine, "NOTE(TEXT=\"pos3\", !TEXT=\"neg1\", ~TEXT=\"fh1\")");
      assertQuery(engine, "IDENTICAL(NOTE TYPE=\"discharge note\", " + getInterval("2012-01-01 00:00:00", "2012-03-03 10:00:00", "2012-03-03 10:00:00", "2012-04-04 10:00:00",
          "2012-04-04 10:00:00") + ")", new int[] {1});
      assertQuery(engine, "IDENTICAL(NOTE TYPE=\"progress note\", " + getInterval("2013-01-01 00:00:00", "2013-04-04 10:00:00", "2013-04-04 10:00:00") + ")", new int[] {2});
    }
    finally {
      engine.close();
    }
  }

}