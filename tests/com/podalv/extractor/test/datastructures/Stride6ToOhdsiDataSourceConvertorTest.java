package com.podalv.extractor.test.datastructures;

import org.junit.Assert;
import org.junit.Test;

public class Stride6ToOhdsiDataSourceConvertorTest {

  @Test
  public void daysToYearMonthDay() throws Exception {
    Assert.assertArrayEquals(new int[] {1950, 01, 16}, Stride6ToOhdsiDataSourceConvertor.daysToYearMonthDay(15));
    Assert.assertArrayEquals(new int[] {1950, 01, 01}, Stride6ToOhdsiDataSourceConvertor.daysToYearMonthDay(0.5));
    Assert.assertArrayEquals(new int[] {1951, 01, 01}, Stride6ToOhdsiDataSourceConvertor.daysToYearMonthDay(365));
  }
}
