package com.podalv.extractor.hierarchies.icd10;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class Icd10HierarchyTest {

  @Test
  public void doesNotContainInvalidEntries() throws Exception {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    Assert.assertNull(umls.getIcd10Parents(-1, indices));
  }
}
