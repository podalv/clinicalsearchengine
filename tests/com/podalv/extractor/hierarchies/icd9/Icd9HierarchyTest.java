package com.podalv.extractor.hierarchies.icd9;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class Icd9HierarchyTest {

  @Test
  public void doesNotContainInvalidEntries() throws Exception {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    Assert.assertNull(umls.getIcd9Parents(-1, indices));
  }
}
