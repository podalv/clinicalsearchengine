package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.csvQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.functionalTests.CsvFunctionalTests;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7LabDeRecord;
import com.podalv.extractor.test.datastructures.Stride7LabEpicRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class Stride7LabsDeTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    tearDown();
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void valueNotNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(null).value(1.2));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("aaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void ageNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(null).year(2012).componentText("aaa").loinc(null).value(1.2));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void zeroAgeNotChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(null).value(1.2));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(null).value(1.2));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1.0).year(2012).componentText("aaa").loinc(null).value(1.2));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "NO VALUE")));
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("aaa"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(1),
        Common.daysToTime(1))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void valueNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(null).value(null));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "NO VALUE")), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("aaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void normal_not_interpreted() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(null).value(1.0));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("aaa", "NO VALUE")));
    assertQuery(query(engine, labsQuery("aaa", 1.0, 1.0)), 1);
  }

  @Test
  public void high_not_interpreted() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(null).value(6.0));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("aaa", "NO VALUE")));
    assertQuery(query(engine, labsQuery("aaa", 6.0, 6.0)), 1);
  }

  @Test
  public void low_not_interpreted() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(null).value(0.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("aaa", "NO VALUE")));
    assertQuery(query(engine, labsQuery("aaa", 0.5, 0.5)), 1);
  }

  @Test
  public void loinc() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(12.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("0")), 1);
    assertQuery(query(engine, labsQuery("0", "NO VALUE")));
    assertQuery(query(engine, labsQuery("0", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("0", 12.5, 12.5)), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("0"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void loincAndNonLoinc() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(12.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").value(12.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("0")), 1);
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("0", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("0", "NO VALUE")));
    assertQuery(query(engine, labsQuery("0", 12.5, 12.5)), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("0"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void componentNullLoincNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText(null).loinc(null).value(12.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void abnormal() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(12.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("0")), 1);
    assertQuery(query(engine, labsQuery("0", "Abnormally low")));
    assertQuery(query(engine, labsQuery("0", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("0", "NO VALUE")));
    assertQuery(query(engine, labsQuery("0", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("0", 12.5, 12.5)), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("0"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void csv_no_value_no_reference_range() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(null));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals("1\tLABS=0\t2012\tNO VALUE\t\t1\t12.0\t12.0\t", lines.get(1));
  }

  @Test
  public void csv_value_invalid_lab() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(source.addLoincText("aaa")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1000.0).year(2012).componentText("aaa").loinc(source.addLoincText("aaa")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1000.0).year(2012).componentText("bbb").loinc(source.addLoincText("bbb")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    Assert.assertEquals(3, lines.size());
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.5\t1\t1000.0\t1000.0\t", lines.get(1));
    Assert.assertEquals("1\tLABS=1\t2012\t\t15.5\t1\t1000.0\t1000.0\t", lines.get(2));

  }

  @Test
  public void csv_value_invalid_lab_error() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(10.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(10.0).year(2012).componentText("bbb").loinc(source.addLoincText("abc")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    Assert.assertEquals(3, lines.size());
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.5\t1\t0.0\t0.0\t", lines.get(1));
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.5\t1\t10.0\t10.0\t", lines.get(2));
  }

  @Test
  public void csv_value_no_reference_range() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.5\t1\t12.0\t12.0\t", lines.get(1));
  }

  @Test
  public void manyTextValues_short() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(3).gender("female").ethnicity("eth1").race("race1"));

    for (int x = 0; x < 1800; x++) {
      source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(1.1).year(2012).labName("aaa").loinc("0").value("Abnormally low" + x));
      source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(1.1).year(2012).labName("aaa").loinc("1").value("Abnormally low" + x));
      source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(1.1).year(2012).labName("aaa").loinc("2").value("Abnormally low" + x));
    }

    for (int x = 0; x < 1800; x++) {
      source.addLabEpicLpch(Stride7LabEpicRecord.create(2).age(1.1).year(2012).labName("aaa").loinc("0").value("Abnormally" + x));
      source.addLabEpicLpch(Stride7LabEpicRecord.create(2).age(1.1).year(2012).labName("aaa").loinc("1").value("Abnormally" + x));
      source.addLabEpicLpch(Stride7LabEpicRecord.create(2).age(1.1).year(2012).labName("aaa").loinc("2").value("Abnormally" + x));
    }

    source.addLabEpicLpch(Stride7LabEpicRecord.create(3).age(1.1).year(2012).labName("aaa").loinc("2").value("Abnormally1000"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    for (int x = 0; x < 1800; x++) {
      assertQuery(query(engine, labsQuery("0", "Abnormally low" + x)), 1);
      assertQuery(query(engine, labsQuery("1", "Abnormally low" + x)), 1);
      assertQuery(query(engine, labsQuery("2", "Abnormally low" + x)), 1);
      assertQuery(query(engine, labsQuery("0", "Abnormally" + x)), 2);
      assertQuery(query(engine, labsQuery("1", "Abnormally" + x)), 2);
      if (x == 1000) {
        assertQuery(query(engine, labsQuery("2", "Abnormally" + x)), 2, 3);
      }
      else {
        assertQuery(query(engine, labsQuery("2", "Abnormally" + x)), 2);
      }
    }

    engine.close();

    engine = new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, DATA_FOLDER);
    engine.close();

    engine = new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, DATA_FOLDER);

    for (int x = 0; x < 1800; x++) {
      assertQuery(query(engine, labsQuery("0", "Abnormally low" + x)), 1);
      assertQuery(query(engine, labsQuery("1", "Abnormally low" + x)), 1);
      assertQuery(query(engine, labsQuery("2", "Abnormally low" + x)), 1);
      assertQuery(query(engine, labsQuery("0", "Abnormally" + x)), 2);
      assertQuery(query(engine, labsQuery("1", "Abnormally" + x)), 2);
      if (x == 1000) {
        assertQuery(query(engine, labsQuery("2", "Abnormally" + x)), 2, 3);
      }
      else {
        assertQuery(query(engine, labsQuery("2", "Abnormally" + x)), 2);
      }
    }

    engine.close();
  }

  @Test
  public void csv_value_reference_range() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.5\t1\t12.0\t12.0\t", lines.get(1));
  }

  @Test
  public void duplicate() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.6));
    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    for (final String line : lines) {
      System.out.println(line);
    }
    Assert.assertEquals(3, lines.size());
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.5\t1\t12.0\t12.0\t", lines.get(1));
    Assert.assertEquals("1\tLABS=0\t2012\t\t15.6\t1\t12.0\t12.0\t", lines.get(2));
  }

  @Test
  public void greaterThanLesserThan() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(12.2).year(2012).componentText("aaa").loinc("1234-5").value(1.2));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("1234-5")), 1);
    assertQuery(query(engine, labsQuery("1234-5", 1.2, 1.2)), 1);
  }

}