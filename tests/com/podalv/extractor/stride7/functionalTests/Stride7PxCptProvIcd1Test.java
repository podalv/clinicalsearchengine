package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd10Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitPrimaryRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7PxCptProvIcd1Test {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    tearDown();
  }

  @After
  public void tearDown() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient(final int number) throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013), number);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getPatientPrimary() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.51", "A00.1")).year(2012).primary("Y"), 1);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getPatientNullYear(final int number) throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(null, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(null).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(null, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(null), number);

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(1200.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(1200.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013), number);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getPatientNullAge(final int number) throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(null, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(null, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013), number);

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013), number);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getPatientNullDxId(final int number) throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(null).year(2012).primary("Y"), number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(null).year(2013), number);

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013), number);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getPatientNullDuration(final int number) throws InstantiationException, IllegalAccessException, SQLException, IOException,
      InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptProvLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, null).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"),
        number);
    source.addVisitPxCptProvShc(Stride7VisitPrimaryRecord.create(2).age(12.2, null).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013), number);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    for (int x = 1; x < 6; x++) {
      engine = getPatient(x);
      assertQuery(query(engine, timelineQuery()), 1, 2);

      assertQuery(query(engine, icd9Query("250.50")), 1);
      assertQuery(query(engine, icd9Query("255.55")), 2);

      assertQuery(query(engine, primaryQuery(icd9Query("250.50"))), 1);
      assertQuery(query(engine, primaryQuery(icd9Query("255.55"))));

      assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
      assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 2);

      assertQuery(query(engine, yearQuery(2012, 2012)), 1);
      assertQuery(query(engine, yearQuery(2013, 2013)), 2);
      tearDown();
    }
  }

  @Test
  public void nullAge() throws Exception {
    for (int x = 1; x < 6; x++) {
      engine = getPatientNullAge(x);
      assertQuery(query(engine, timelineQuery()));
      tearDown();
    }
  }

  @Test
  public void primary() throws Exception {
    engine = getPatientPrimary();
    assertQuery(query(engine, primaryQuery(icd9Query("250.50"))), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("250.51"))), 1);
    assertQuery(query(engine, primaryQuery(icd10Query("A00.0"))), 1);
    assertQuery(query(engine, primaryQuery(icd10Query("A00.1"))), 1);
    tearDown();
  }

  @Test
  public void nullDuration() throws Exception {
    for (int x = 1; x < 6; x++) {
      engine = getPatientNullDuration(x);
      assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
      assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);
      tearDown();
    }
  }

  @Test
  public void nullDxId() throws Exception {
    for (int x = 1; x < 6; x++) {
      engine = getPatientNullDxId(x);
      assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
      assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 2);
      tearDown();
    }
  }

  @Test
  public void nullYear() throws Exception {
    for (int x = 1; x < 6; x++) {
      engine = getPatientNullYear(x);
      assertQuery(query(engine, timelineQuery()));
      tearDown();
    }
  }

}