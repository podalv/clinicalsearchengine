package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Stride7DatabaseDownload;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7NoteRecord;
import com.podalv.extractor.test.datastructures.Stride7TermMentionRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class EmptyPatientTermsTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void emptyPositive() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(0.0).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(0.0).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void emptyNegated() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(0.0).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(0.0).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(1).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(1).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void emptyFh() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(0.0).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(0.0).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(1).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(1).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

}
