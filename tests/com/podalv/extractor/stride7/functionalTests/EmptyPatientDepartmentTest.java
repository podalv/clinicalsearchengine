package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class EmptyPatientDepartmentTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void singleInvalid() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(3).gender("other"));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(2).age(0.0).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(0.0).departmentId(source.getDepartmentIdShc("dept2")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(3).age(0.0).departmentId(source.getDepartmentIdShc("dept2")).year(2013));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(4).age(0.0).departmentId(source.getDepartmentIdLpch("dept3")).year(2012));
    source.addDepartmentApptShc(Stride7DepartmentRecord.create(5).age(0.0).departmentId(source.getDepartmentIdShc("dept4")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void multipleInvalid() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1").death(22.0));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(0.0).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(0.0).departmentId(source.getDepartmentIdShc("dept2")).year(2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

}
