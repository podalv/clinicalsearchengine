package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.vitalsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VitalsRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;

public class Stride7VitalsTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(6).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(7).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(8).gender("male").death(22.0));

    source.addVitals(Stride7VitalsRecord.create(1).age(12.1).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(12.1).bp(120, 80).year(2013));
    source.addVitals(Stride7VitalsRecord.create(3).age(12.1).bsa(21.5).year(2014));
    source.addVitals(Stride7VitalsRecord.create(4).age(12.1).height("6' 5\"").year(2015));
    source.addVitals(Stride7VitalsRecord.create(5).age(12.1).pulse(80).year(2016));
    source.addVitals(Stride7VitalsRecord.create(6).age(12.1).resp(80).year(2017));
    source.addVitals(Stride7VitalsRecord.create(7).age(12.1).temp(100.0).year(2018));
    source.addVitals(Stride7VitalsRecord.create(8).age(12.1).weight(155.5).year(2019));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void years() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
    assertQuery(query(engine, yearQuery(2014, 2014)), 3);
    assertQuery(query(engine, yearQuery(2015, 2015)), 4);
    assertQuery(query(engine, yearQuery(2016, 2016)), 5);
    assertQuery(query(engine, yearQuery(2017, 2017)), 6);
    assertQuery(query(engine, yearQuery(2018, 2018)), 7);
    assertQuery(query(engine, yearQuery(2019, 2019)), 8);
  }

  @Test
  public void values() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "80", "80")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "120", "120")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "21.5", "21.5")), 3);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "77", "77")), 4);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "80", "80")), 5);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "80", "80")), 6);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "100.0", "100.0")), 7);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "155.5", "155.5")), 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "0", "0")));

  }

  @Test
  public void nullAgeNullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(6).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(7).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(8).gender("male").death(22.0));

    source.addVitals(Stride7VitalsRecord.create(1).age(null).bmi(21.5).year(null));
    source.addVitals(Stride7VitalsRecord.create(2).age(null).bp(120, 80).year(null));
    source.addVitals(Stride7VitalsRecord.create(3).age(null).bsa(21.5).year(null));
    source.addVitals(Stride7VitalsRecord.create(4).age(null).height("6' 5\"").year(null));
    source.addVitals(Stride7VitalsRecord.create(5).age(null).pulse(80).year(null));
    source.addVitals(Stride7VitalsRecord.create(6).age(null).resp(80).year(null));
    source.addVitals(Stride7VitalsRecord.create(7).age(null).temp(100.0).year(null));
    source.addVitals(Stride7VitalsRecord.create(8).age(null).weight(155.5).year(null));

    source.addVitals(Stride7VitalsRecord.create(1).age(1200.1).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(1200.1).bp(120, 80).year(2013));
    source.addVitals(Stride7VitalsRecord.create(3).age(1200.1).bsa(21.5).year(2014));
    source.addVitals(Stride7VitalsRecord.create(4).age(1200.1).height("6' 5\"").year(2015));
    source.addVitals(Stride7VitalsRecord.create(5).age(1200.1).pulse(80).year(2016));
    source.addVitals(Stride7VitalsRecord.create(6).age(1200.1).resp(80).year(2017));
    source.addVitals(Stride7VitalsRecord.create(7).age(1200.1).temp(100.0).year(2018));
    source.addVitals(Stride7VitalsRecord.create(8).age(1200.1).weight(155.5).year(2019));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4, 5, 6, 7, 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "80", "80")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "120", "120")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "21.5", "21.5")), 3);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "77", "77")), 4);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "80", "80")), 5);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "80", "80")), 6);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "100.0", "100.0")), 7);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "155.5", "155.5")), 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "0", "0")));
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(6).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(7).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(8).gender("male").death(22.0));

    source.addVitals(Stride7VitalsRecord.create(1).age(null).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(null).bp(120, 80).year(2012));
    source.addVitals(Stride7VitalsRecord.create(3).age(null).bsa(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(4).age(null).height("6' 5\"").year(2012));
    source.addVitals(Stride7VitalsRecord.create(5).age(null).pulse(80).year(2012));
    source.addVitals(Stride7VitalsRecord.create(6).age(null).resp(80).year(2012));
    source.addVitals(Stride7VitalsRecord.create(7).age(null).temp(100.0).year(2012));
    source.addVitals(Stride7VitalsRecord.create(8).age(null).weight(155.5).year(2012));

    source.addVitals(Stride7VitalsRecord.create(1).age(1200.1).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(1200.1).bp(120, 80).year(2013));
    source.addVitals(Stride7VitalsRecord.create(3).age(1200.1).bsa(21.5).year(2014));
    source.addVitals(Stride7VitalsRecord.create(4).age(1200.1).height("6' 5\"").year(2015));
    source.addVitals(Stride7VitalsRecord.create(5).age(1200.1).pulse(80).year(2016));
    source.addVitals(Stride7VitalsRecord.create(6).age(1200.1).resp(80).year(2017));
    source.addVitals(Stride7VitalsRecord.create(7).age(1200.1).temp(100.0).year(2018));
    source.addVitals(Stride7VitalsRecord.create(8).age(1200.1).weight(155.5).year(2019));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4, 5, 6, 7, 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "80", "80")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "120", "120")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "21.5", "21.5")), 3);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "77", "77")), 4);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "80", "80")), 5);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "80", "80")), 6);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "100.0", "100.0")), 7);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "155.5", "155.5")), 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "0", "0")));
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(6).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(7).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(8).gender("male").death(22.0));

    source.addVitals(Stride7VitalsRecord.create(1).age(0.0).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(0.0).bp(120, 80).year(2012));
    source.addVitals(Stride7VitalsRecord.create(3).age(0.0).bsa(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(4).age(0.0).height("6' 5\"").year(2012));
    source.addVitals(Stride7VitalsRecord.create(5).age(0.0).pulse(80).year(2012));
    source.addVitals(Stride7VitalsRecord.create(6).age(0.0).resp(80).year(2012));
    source.addVitals(Stride7VitalsRecord.create(7).age(0.0).temp(100.0).year(2012));
    source.addVitals(Stride7VitalsRecord.create(8).age(0.0).weight(155.5).year(2012));

    source.addVitals(Stride7VitalsRecord.create(1).age(12.1).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(12.1).bp(120, 80).year(2013));
    source.addVitals(Stride7VitalsRecord.create(3).age(12.1).bsa(21.5).year(2014));
    source.addVitals(Stride7VitalsRecord.create(4).age(12.1).height("6' 5\"").year(2015));
    source.addVitals(Stride7VitalsRecord.create(5).age(12.1).pulse(80).year(2016));
    source.addVitals(Stride7VitalsRecord.create(6).age(12.1).resp(80).year(2017));
    source.addVitals(Stride7VitalsRecord.create(7).age(12.1).temp(100.0).year(2018));
    source.addVitals(Stride7VitalsRecord.create(8).age(12.1).weight(155.5).year(2019));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4, 5, 6, 7, 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "80", "80")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "120", "120")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "21.5", "21.5")), 3);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "77", "77")), 4);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "80", "80")), 5);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "80", "80")), 6);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "100.0", "100.0")), 7);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "155.5", "155.5")), 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "0", "0")));
  }

  @Test
  public void zeroAgeNullYearChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(6).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(7).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(8).gender("male").death(22.0));

    source.addVitals(Stride7VitalsRecord.create(1).age(0.0).bmi(21.5).year(null));
    source.addVitals(Stride7VitalsRecord.create(2).age(0.0).bp(120, 80).year(null));
    source.addVitals(Stride7VitalsRecord.create(3).age(0.0).bsa(21.5).year(null));
    source.addVitals(Stride7VitalsRecord.create(4).age(0.0).height("6' 5\"").year(null));
    source.addVitals(Stride7VitalsRecord.create(5).age(0.0).pulse(80).year(null));
    source.addVitals(Stride7VitalsRecord.create(6).age(0.0).resp(80).year(null));
    source.addVitals(Stride7VitalsRecord.create(7).age(0.0).temp(100.0).year(null));
    source.addVitals(Stride7VitalsRecord.create(8).age(0.0).weight(155.5).year(null));

    source.addVitals(Stride7VitalsRecord.create(1).age(12.1).bmi(21.5).year(2012));
    source.addVitals(Stride7VitalsRecord.create(2).age(12.1).bp(120, 80).year(2013));
    source.addVitals(Stride7VitalsRecord.create(3).age(12.1).bsa(21.5).year(2014));
    source.addVitals(Stride7VitalsRecord.create(4).age(12.1).height("6' 5\"").year(2015));
    source.addVitals(Stride7VitalsRecord.create(5).age(12.1).pulse(80).year(2016));
    source.addVitals(Stride7VitalsRecord.create(6).age(12.1).resp(80).year(2017));
    source.addVitals(Stride7VitalsRecord.create(7).age(12.1).temp(100.0).year(2018));
    source.addVitals(Stride7VitalsRecord.create(8).age(12.1).weight(155.5).year(2019));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4, 5, 6, 7, 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "80", "80")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "120", "120")), 2);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "21.5", "21.5")), 3);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "77", "77")), 4);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "80", "80")), 5);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "80", "80")), 6);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "100.0", "100.0")), 7);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "155.5", "155.5")), 8);

    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "21.5", "21.5")), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_HEIGHT, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "0", "0")));
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "0", "0")));
  }

  @Test
  public void onlyInvalid() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(6).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(7).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(8).gender("male").death(22.0));

    source.addVitals(Stride7VitalsRecord.create(1).age(0.0).bmi(21.5).year(null));
    source.addVitals(Stride7VitalsRecord.create(2).age(0.0).bp(120, 80).year(null));
    source.addVitals(Stride7VitalsRecord.create(3).age(0.0).bsa(21.5).year(null));
    source.addVitals(Stride7VitalsRecord.create(4).age(0.0).height("6' 5\"").year(null));
    source.addVitals(Stride7VitalsRecord.create(5).age(0.0).pulse(80).year(null));
    source.addVitals(Stride7VitalsRecord.create(6).age(0.0).resp(80).year(null));
    source.addVitals(Stride7VitalsRecord.create(7).age(0.0).temp(100.0).year(null));
    source.addVitals(Stride7VitalsRecord.create(8).age(0.0).weight(155.5).year(null));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

  }

}
