package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.departmentQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.invertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class InvertNotTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws IOException {
    tearDown();
  }

  @After
  public void tearDown() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.5).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(13.5).departmentId(source.getDepartmentIdShc("dept2")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(15.5).departmentId(source.getDepartmentIdShc("dept2")).year(2013));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(12.5).departmentId(source.getDepartmentIdShc("dept3")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, notQuery(departmentQuery("dept1"))), 2);
    assertQuery(query(engine, invertQuery(departmentQuery("dept1"))), 1, 2);
    assertQuery(query(engine, notQuery(departmentQuery("dept2"))));
    assertQuery(query(engine, invertQuery(departmentQuery("dept2"))), 1, 2);
    assertQuery(query(engine, notQuery(departmentQuery("dept3"))), 1);
    assertQuery(query(engine, invertQuery(departmentQuery("dept3"))), 1, 2);
  }

}