package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitHl7Record;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;
import com.podalv.workshop.datastructures.DumpRequest;
import com.podalv.workshop.datastructures.DumpResponse;

public class Stride7VisitHlTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code("250.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(1).age(13d, 13d).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(2012));
    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(14d, 14d).code("250.50").primaryType("Primary code").visitType("Other1").year(2013));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(1).age(15d, 15d).code("220.50").primaryType("Secondary code").visitType("Other2").year(2014));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(2).age(1d, 1d).code("123.45").primaryType(null).visitType(null).year(2013));

    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(1d, 3d).code("222.22").primaryType(null).visitType(null).year(2013));
    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(2).age(5d, 10d).code("333.33").primaryType(null).visitType(null).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd9Query("220.50")), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(14))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.50"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13)), intervalQuery(Common.daysToTime(15),
        Common.daysToTime(15))))), 1);

    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), intervalQuery(Common.daysToTime(1), Common.daysToTime(1)))), 2);
  }

  @Test
  public void primary() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, primaryQuery(icd9Query("250.50"))), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("220.50"))));
    assertQuery(query(engine, primaryQuery(icd9Query("123.45"))));

    assertQuery(query(engine, identicalQuery(primaryQuery(icd9Query("250.50")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12)), intervalQuery(
        Common.daysToTime(14), Common.daysToTime(14))))), 1);
  }

  @Test
  public void visitTypes() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, visitTypeQuery("Inpatient")), 1);
    assertQuery(query(engine, visitTypeQuery("Outpatient")), 1);
    assertQuery(query(engine, visitTypeQuery("Other1")), 1);
    assertQuery(query(engine, visitTypeQuery("Other2")), 1);
    assertQuery(query(engine, visitTypeQuery("Empty")), 2);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("Inpatient"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("Outpatient"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("Other1"), intervalQuery(Common.daysToTime(14), Common.daysToTime(14)))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("Other2"), intervalQuery(Common.daysToTime(15), Common.daysToTime(15)))), 1);
  }

  @Test
  public void years() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, yearQuery(2013, 2013)), 1, 2);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2014, 2014)), 1);
  }

  @Test
  public void duration() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, identicalQuery(icd9Query("222.22"), intervalQuery(Common.daysToTime(1), Common.daysToTime(3)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("333.33"), intervalQuery(Common.daysToTime(5), Common.daysToTime(10)))), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(null, 12d).code("250.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(null, 13d).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(1200d, 12d).code("250.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(1300d, 13d).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void nullDuration() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, null).code("250.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, null).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(2013));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(3).age(13d, null).code(null).primaryType("Secondary code").visitType("Outpatient").year(2013));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(3).age(13d, null).code("250.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.50"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);

    final DumpResponse resp = engine.dump(null, DumpRequest.createFull(3));
    Assert.assertEquals(6, resp.getIcd9().size());
    Assert.assertTrue(resp.getIcd9().containsKey("240-279.99"));
    Assert.assertTrue(resp.getIcd9().containsKey("250"));
    Assert.assertTrue(resp.getIcd9().containsKey("250.5"));
    Assert.assertTrue(resp.getIcd9().containsKey("249-259.99"));
    Assert.assertTrue(resp.getIcd9().containsKey("001-999.99"));
    Assert.assertTrue(resp.getIcd9().containsKey("250.50"));
  }

  @Test
  public void nullCode() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code(null).primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, 13d).code(null).primaryType("Secondary code").visitType("Outpatient").year(2013));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code("250.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, 13d).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.50"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
  }

  @Test
  public void nullPrimaryType() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code("250.50").primaryType(null).visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, 13d).code("220.50").primaryType(null).visitType("Outpatient").year(2013));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code("260.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, 13d).code("270.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.50"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("260.50"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("270.50"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
  }

  @Test
  public void nullVisitType() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code("250.50").primaryType("Primary code").visitType(null).year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, 13d).code("220.50").primaryType("Secondary code").visitType(null).year(2013));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(12d, 12d).code("260.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(13d, 13d).code("270.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.50"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("260.50"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("270.50"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(null, 12d).code("250.50").primaryType("Primary code").visitType("Inpatient").year(null));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(null, 13d).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(null));

    source.addVisitHl7Lpch(Stride7VisitHl7Record.create(1).age(1200d, 12d).code("250.50").primaryType("Primary code").visitType("Inpatient").year(2012));
    source.addVisitHl7Shc(Stride7VisitHl7Record.create(2).age(1300d, 13d).code("220.50").primaryType("Secondary code").visitType("Outpatient").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

}