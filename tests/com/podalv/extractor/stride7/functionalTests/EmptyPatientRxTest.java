package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.rxQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DrugRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class EmptyPatientRxTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getInvalidPatientSingleEvent() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(0.0).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(0.0).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getInvalidPatientMultipleEvents() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(0.0).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedLpch(Stride7DrugRecord.create(1).age(0.0).medicationId(source.addMedicationIdLpch(3000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(0.0).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getValidOneFeatureValidOneInvalidPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException,
      InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addMedLpch(Stride7DrugRecord.create(1).age(0.0).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedLpch(Stride7DrugRecord.create(1).age(1000.0).medicationId(source.addMedicationIdLpch(3000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void emptySingleEvent() throws Exception {
    engine = getInvalidPatientSingleEvent();
    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void emptyMultipleEvents() throws Exception {
    engine = getInvalidPatientMultipleEvents();
    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void validPatient() throws Exception {
    engine = getValidOneFeatureValidOneInvalidPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    assertQuery(query(engine, rxQuery(2000)));
    assertQuery(query(engine, rxQuery(3000)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

}
