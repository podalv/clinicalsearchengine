package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7CultureDictionaryRecord;
import com.podalv.extractor.test.datastructures.Stride7CultureRecord;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;

public class Stride7CulturesTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    tearDown();
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addCultureDictionary(new Stride7CultureDictionaryRecord("aaa", "aaax", "OK"));
    source.addCultureDictionary(new Stride7CultureDictionaryRecord("bbb", "bbbx", "IGNORE"));
    source.addCultureDictionary(new Stride7CultureDictionaryRecord("ccc", "cccx", "OK"));

    source.addCulturesShc(Stride7CultureRecord.create(1).age(1.5).labName("culture_a").value("[aaa]").year(2012));
    source.addCulturesShc(Stride7CultureRecord.create(1).age(5.0).labName("culture_a").value("[bbb]").year(2012));
    source.addCulturesShc(Stride7CultureRecord.create(1).age(1.5).labName("culture_b").value("[ccc]").year(2012));

    source.addCulturesShc(Stride7CultureRecord.create(2).age(2.5).labName("culture_b").value("[ccc]").year(2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("culture_a")), 1);
    assertQuery(query(engine, labsQuery("culture_b", "cccx")), 1, 2);
    assertQuery(query(engine, labsQuery("culture_b", "ccc")));
    assertQuery(query(engine, labsQuery("culture_b", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("culture_a", "bbbx")));
    assertQuery(query(engine, labsQuery("culture_a", "bbb")));
    assertQuery(query(engine, identicalQuery(labsQuery("culture_b"), unionQuery(intervalQuery(Common.daysToTime(1), Common.daysToTime(1))))), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("culture_b"), unionQuery(intervalQuery(Common.daysToTime(2), Common.daysToTime(2))))), 2);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1, 2);
  }

  @Test
  public void labNameTransformation() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addCultureLabName("culture_a", "BLOOD");

    source.addCulturesShc(Stride7CultureRecord.create(1).age(1.5).labName("culture_a").value("[aaa]").year(2012));
    source.addCulturesShc(Stride7CultureRecord.create(1).age(1.5).labName("culture_b").value("[ccc]").year(2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("CULTURE [blood]")), 1);
    assertQuery(query(engine, labsQuery("CULTURE [blood]", "bbbx")));
  }
}
