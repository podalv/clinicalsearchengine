package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.noteTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textFhQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textNegatedQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride7DatabaseDownload;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7NoteRecord;
import com.podalv.extractor.test.datastructures.Stride7TermMentionRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7Notes {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(textQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void negatedFamilyHistory() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(1).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(1).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(textFhQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(textNegatedQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(1200.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(null).noteId(3).noteType("typeX").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(3).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 2);

    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(0.0).noteId(3).noteType("typeX").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(3).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(textQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(12),
        Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
  }

  @Test
  public void nullNoteType() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(14.4).noteId(3).noteType(null).year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(3).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
    assertQuery(query(engine, identicalQuery(textQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(14))))), 1);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("empty"), unionQuery(intervalQuery(Common.daysToTime(14), Common.daysToTime(14))))), 1);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(1200.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(null).noteId(3).noteType("type3").year(null));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(3).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 2);

    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullNoteId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(14.4).noteId(null).noteType("type3").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(3).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(textQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
  }

  @Test
  public void nullNoteIdTermMention() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(14.4).noteId(3).noteType("type3").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(null).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(textQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
  }

  @Test
  public void nullTermIdTermMention() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addNotes(Stride7NoteRecord.create(1).age(12.2).noteId(1).noteType("type1").year(2012));
    source.addNotes(Stride7NoteRecord.create(1).age(14.4).noteId(3).noteType("type3").year(2012));
    source.addNotes(Stride7NoteRecord.create(2).age(13.2).noteId(2).noteType("type2").year(2013));

    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(1).termId(source.getTermId("aaaaaa")));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(3).termId(null));
    source.addTermMention(Stride7TermMentionRecord.create().familyHistory(0).negated(0).noteId(2).termId(source.getTermId("bbbbbb")));

    Stride7DatabaseDownload.MAX_PATIENT_CNT = 100;
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(textQuery("aaaaaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("bbbbbb"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
  }

}