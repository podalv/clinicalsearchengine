package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd10Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class Stride7VisitAdmitTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(12d, 13d).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(1).age(14d, 15d).dxId(source.addDxIdShc("250.50", "X00.0", "250.50", "X00.0")).year(2012));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(2).age(12d, 13d).dxId(source.addDxIdLpch("220.20", "B00.0", "220.20", "B00.0")).year(2013));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(14d, 15d).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(2013));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(3).age(15d, 16d).dxId(source.addDxIdLpch("123.45,222.22", "C00.0,D00.0", "123.45", "C00.0")).year(2014));
    source.addVisitAdmitShc(Stride7VisitRecord.create(3).age(17d, 18d).dxId(source.addDxIdShc("234.56,333.33", "E00.0,F00.0", "234.56", "E00.0")).year(2014));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void icd9Codes() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2, 3);

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd9Query("220.20")), 2);
    assertQuery(query(engine, icd9Query("123.45")), 3);
    assertQuery(query(engine, icd9Query("333.33")), 3);
    assertQuery(query(engine, icd9Query("222.22")), 3);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(15))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.20"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(15))))), 2);
    assertQuery(query(engine, identicalQuery(icd9Query("123.45"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(16))))), 3);
    assertQuery(query(engine, identicalQuery(icd9Query("222.22"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(16))))), 3);

    assertQuery(query(engine, identicalQuery(icd9Query("234.56"), unionQuery(intervalQuery(Common.daysToTime(17), Common.daysToTime(18))))), 3);
    assertQuery(query(engine, identicalQuery(icd9Query("333.33"), unionQuery(intervalQuery(Common.daysToTime(17), Common.daysToTime(18))))), 3);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
    assertQuery(query(engine, yearQuery(2014, 2014)), 3);
  }

  @Test
  public void icd10Codes() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, icd10Query("X00.0")), 1);
    assertQuery(query(engine, icd10Query("B00.0")), 2);
    assertQuery(query(engine, icd10Query("C00.0")), 3);
    assertQuery(query(engine, icd10Query("D00.0")), 3);
    assertQuery(query(engine, icd10Query("E00.0")), 3);
    assertQuery(query(engine, icd10Query("F00.0")), 3);

    assertQuery(query(engine, identicalQuery(icd10Query("X00.0"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(15))))), 1);
    assertQuery(query(engine, identicalQuery(icd10Query("B00.0"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(15))))), 2);
    assertQuery(query(engine, identicalQuery(icd10Query("C00.0"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(16))))), 3);
    assertQuery(query(engine, identicalQuery(icd10Query("D00.0"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(16))))), 3);

    assertQuery(query(engine, identicalQuery(icd10Query("E00.0"), unionQuery(intervalQuery(Common.daysToTime(17), Common.daysToTime(18))))), 3);
    assertQuery(query(engine, identicalQuery(icd10Query("F00.0"), unionQuery(intervalQuery(Common.daysToTime(17), Common.daysToTime(18))))), 3);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
    assertQuery(query(engine, yearQuery(2014, 2014)), 3);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(null, 13d).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(null, 15d).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(2013));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(12d, 13d).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(14d, 15d).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullDuration() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(12d, null).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(14d, null).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.20"), unionQuery(intervalQuery(Common.daysToTime(14), Common.daysToTime(14))))), 2);
  }

  @Test
  public void nullDxId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(12d, 13d).dxId(null).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(14d, 15d).dxId(null).year(2013));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(12d, 13d).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(14d, 15d).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("220.20"), unionQuery(intervalQuery(Common.daysToTime(14), Common.daysToTime(15))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(null, 13d).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(null));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(null, 15d).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(null));

    source.addVisitAdmitLpch(Stride7VisitRecord.create(1).age(1200d, 13d).dxId(source.addDxIdLpch("250.50", "X00.0", "250.50", "X00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(2).age(1400d, 15d).dxId(source.addDxIdShc("220.20", "B00.00", "220.20", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

}