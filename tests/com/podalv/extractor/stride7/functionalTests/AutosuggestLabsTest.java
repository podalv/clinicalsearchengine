package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7LabEpicRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestResponse;
import com.podalv.search.server.Atlas;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;

public class AutosuggestLabsTest {

  private ClinicalSearchEngine engine = null;
  private Atlas                atlas  = null;

  @Before
  public void setup() throws Exception {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    tearDown();
    cleanup();
  }

  @After
  public void tearDown() throws Exception {
    cleanup();
    FileUtils.deleteFolderContents(DATA_FOLDER);
    if (atlas != null) {
      atlas.stop();
    }
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void valueNotNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(3).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(4).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(5).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(6).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(7).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(8).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(9).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(10).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc("1234-5").value("aaa"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc("1234-5").value("bbb"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(3).age(12.2).year(2012).labName("aaa").loinc("1234-5").value("ccc"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(4).age(12.2).year(2012).labName("aaa").loinc("1234-5").value("ddd"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(5).age(12.2).year(2012).labName("aaa").loinc("1234-5").value("eee"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(6).age(12.2).year(2012).labName("aaax").loinc("2234-5").value("aaa"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(7).age(12.2).year(2012).labName("aaac").loinc("2234-5").value("bbb"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(8).age(12.2).year(2012).labName("aaav").loinc("2234-5").value("bbb"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(9).age(12.2).year(2012).labName("aaab").loinc("2234-5").value("ddd"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(10).age(12.2).year(2012).labName("aaas").loinc("2234-5").value("eee"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    atlas = new Atlas(engine, 8080);
    atlas.start(false, null);

    AutosuggestRequest r = new AutosuggestRequest("LABS(\"1234-5\", bb", 16);
    URL url = new URL("http://localhost:8080" + "/autosuggest");
    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
    urlc.setDoOutput(true);
    urlc.setRequestMethod("POST");
    urlc.setAllowUserInteraction(false);

    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(urlc.getOutputStream(), "UTF-8"));
    bw.write(new Gson().toJson(r));
    bw.flush();
    bw.close();

    AutosuggestResponse response = new Gson().fromJson(new InputStreamReader(urlc.getInputStream()), AutosuggestResponse.class);

    r = new AutosuggestRequest("LABS(\"2234-5\", bb", 16);
    url = new URL("http://localhost:8080" + "/autosuggest");
    urlc = (HttpURLConnection) url.openConnection();
    urlc.setDoOutput(true);
    urlc.setRequestMethod("POST");
    urlc.setAllowUserInteraction(false);

    bw = new BufferedWriter(new OutputStreamWriter(urlc.getOutputStream(), "UTF-8"));
    bw.write(new Gson().toJson(r));
    bw.flush();
    bw.close();

    response = new Gson().fromJson(new InputStreamReader(urlc.getInputStream()), AutosuggestResponse.class);

    Assert.assertArrayEquals(new String[] {"BBB [2]"}, response.getResponse());
  }

}
