package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitWithSabRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class EmptyPatientCptTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getInvalidSingleEventPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspShc(Stride7VisitWithSabRecord.create(2).age(0.0).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getInvalidMultipleFeaturesPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("35000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspShc(Stride7VisitWithSabRecord.create(2).age(0.0).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));
    source.addVisitPxHspShc(Stride7VisitWithSabRecord.create(2).age(0.0).code("45000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getvalidPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(0.0).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspLpch(Stride7VisitWithSabRecord.create(1).age(1000.0).code("35000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxHspShc(Stride7VisitWithSabRecord.create(2).age(0.0).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));
    source.addVisitPxHspShc(Stride7VisitWithSabRecord.create(2).age(1000.0).code("45000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void singleEvent() throws Exception {
    engine = getInvalidSingleEventPatient();
    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void multipleEvents() throws Exception {
    engine = getInvalidMultipleFeaturesPatient();
    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void valid() throws Exception {
    engine = getvalidPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

}
