package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7LabDeRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class EmptyPatientLabTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    tearDown();
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void singleLabEmpty() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void multipleLabsEmpty() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1000.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("bbb").loinc(source.addLoincText("def")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1000.0).year(2012).componentText("bbb").loinc(source.addLoincText("def")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);

    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void validLabsEmpty() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabDe(Stride7LabDeRecord.create(1).age(0.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1000.0).year(2012).componentText("aaa").loinc(source.addLoincText("abc")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(10.0).year(2012).componentText("bbb").loinc(source.addLoincText("def")).value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(1000.0).year(2012).componentText("bbb").loinc(source.addLoincText("def")).value(15.5));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);

    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

}
