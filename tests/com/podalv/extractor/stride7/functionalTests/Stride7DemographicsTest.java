package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.deathQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.departmentQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.ethnicityQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.genderQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intersectQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.raceQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7DemographicsTest {

  private ClinicalSearchEngine engine;

  @Before
  public void setup() throws IOException {
    tearDown();
  }

  @After
  public void tearDown() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1").death(120.5).setDeathYear(2015));
    source.addDemographics(Stride7Demographics.create(3).gender("other"));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(2).age(12.5).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(3).age(15.5).departmentId(source.getDepartmentIdShc("dept2")).year(2013));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(4).age(12.5).departmentId(source.getDepartmentIdLpch("dept3")).year(2012));
    source.addDepartmentApptShc(Stride7DepartmentRecord.create(5).age(15.5).departmentId(source.getDepartmentIdShc("dept4")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()), 2, 3, 4, 5);
    assertQuery(query(engine, departmentQuery("dept1")), 2);
    assertQuery(query(engine, departmentQuery("dept2")), 3);
    assertQuery(query(engine, departmentQuery("dept3")), 4);
    assertQuery(query(engine, departmentQuery("dept4")), 5);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 2);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), intervalQuery(Common.daysToTime(15), Common.daysToTime(15)))), 3);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept3"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 4);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept4"), intervalQuery(Common.daysToTime(15), Common.daysToTime(15)))), 5);

    assertQuery(query(engine, genderQuery("male")), 4);
    assertQuery(query(engine, genderQuery("female")), 2);
    assertQuery(query(engine, genderQuery("other")), 3);
    assertQuery(query(engine, genderQuery("empty")), 5);

    assertQuery(query(engine, raceQuery("race1")), 2);
    assertQuery(query(engine, raceQuery("empty")), 3, 4, 5);
    assertQuery(query(engine, ethnicityQuery("eth1")), 2);
    assertQuery(query(engine, ethnicityQuery("empty")), 3, 4, 5);
    assertQuery(query(engine, deathQuery()), 2, 4);

    assertQuery(query(engine, yearQuery(2015, 2015)), 2);
    assertQuery(query(engine, intersectQuery(yearQuery(2015, 2015), "END(TIMELINE)")), 2);

    assertQuery(query(engine, identicalQuery(deathQuery(), intervalQuery(Common.daysToTime(23), Common.daysToTime(23)))), 4);
  }
}
