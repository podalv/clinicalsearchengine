package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.invertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitPrimaryRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class InvalidDatapointAbsenceTests {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(0.0, 0.0).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(1).age(1000.0, 1.0).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void notTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());

    assertQuery(query(engine, notQuery(icd9Query("250.50"))));
    assertQuery(query(engine, notQuery(icd9Query("251.50"))), 1);
  }

  @Test
  public void invertTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());

    assertQuery(query(engine, invertQuery(icd9Query("250.50"))));
    assertQuery(query(engine, invertQuery(icd9Query("251.50"))), 1);
  }

  @Test
  public void beforeTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());

    assertQuery(query(engine, "BEFORE(ICD9=250.50, ICD9=255.55*)-(MIN, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=251.50, ICD9=255.55*)-(MIN, -1)"), 1);
  }

  @Test
  public void forEachParent() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());

    assertQuery(query(engine, "FOR EACH(ICD9=250.50) AS (AAA) {RETURN AAA AS AAA;}\nAAA"));
    assertQuery(query(engine, "FOR EACH(ICD9=255.55) AS (AAA) {RETURN AAA AS AAA;}\nAAA"), 1);
  }

  @Test
  public void forEachChild() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());

    assertQuery(query(engine, "FOR EACH(ICD9=255.55) AS (AAA) {BBB = NOT(ICD9=250.50);\nRETURN INTERSECT(BBB,AAA) AS AAA;}\nAAA"));
    assertQuery(query(engine, "FOR EACH(ICD9=255.55) AS (AAA) {BBB = NOT(ICD9=256.55);\nRETURN INTERSECT(BBB,AAA) AS AAA;}\nAAA"), 1);
  }

}
