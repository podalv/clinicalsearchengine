package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.csvQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.functionalTests.CsvFunctionalTests;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7LabEpicRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestResponse;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class Stride7LabsEpicTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    tearDown();
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullLoinc() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("Negative"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("Negative"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", "NEGATIVE")), 1, 2);
    assertQuery(query(engine, identicalQuery(labsQuery("aaa"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1, 2);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1, 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(null).year(2012).labName("aaa").loinc(null).value("Negative"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(0.0).year(2012).labName("aaa").loinc(null).value("Negative"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(1.0).year(2012).labName("aaa").loinc(null).value("Negative"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "NEGATIVE")), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("aaa"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(1),
        Common.daysToTime(1))))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
  }

  @Test
  public void zeroAgeNotChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(0.0).year(2012).labName("aaa").loinc(null).value("Negative"));
    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(1000.0).year(2012).labName("aaa").loinc(null).value("Negative"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void nonNullLoinc() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(source.addLoincText("abc")).value("Negative"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(source.addLoincText("def")).value("Negative"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("0")), 1);
    assertQuery(query(engine, labsQuery("1")), 2);
    assertQuery(query(engine, identicalQuery(labsQuery("0"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1, 2);

    final AutosuggestResponse autosuggestResponse = engine.getAutosuggest().search(new AutosuggestRequest("LABS(\"a", 7));
    System.out.println(autosuggestResponse.getResponse()[0]);
  }

  @Test
  public void numericValue() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", "undefined")));
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
  }

  @Test
  public void numericValueResultFlag() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", "undefined")));
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
  }

  @Test
  public void numericValueAbnormal() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "undefined")));
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
  }

  @Test
  public void numericValueReferenceRange() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "undefined")));
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
  }

  @Test
  public void inRange() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
  }

  @Test
  public void referenceRange() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "HIGH")));
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
    assertQuery(query(engine, labsQuery("aaa", "NO VALUE")));
  }

  @Test
  public void reference_second_range() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.7"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.7, 1.7)), 1);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
  }

  @Test
  public void reference_first_range() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.7"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.7, 1.7)), 1);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "UNDEFINED")));
  }

  @Test
  public void referenceRangeLowNumericRange() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "LOW")));
  }

  @Test
  public void resultFlag() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(12.2).year(2012).labName("aaa").loinc(null).value("1.2"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(2).age(12.2).year(2012).labName("aaa").loinc(null).value("1.5"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, labsQuery("aaa")), 1, 2);
    assertQuery(query(engine, labsQuery("aaa", 1.2, 1.2)), 1);
    assertQuery(query(engine, labsQuery("aaa", 1.5, 1.5)), 2);
    assertQuery(query(engine, labsQuery("aaa", "NORMAL")));
    assertQuery(query(engine, labsQuery("aaa", "LOW")));
  }

  @Test
  public void csv_no_value() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.2).year(2012).loinc("2345-7").value(null));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals("1\tLABS=2345-7\t2012\tNO VALUE\t\t1\t12.0\t12.0\t", lines.get(1));
  }

  @Test
  public void csv_value() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.2).year(2012).value("115").loinc("2345-7"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    for (int x = 0; x < lines.size(); x++) {
      System.out.println(lines.get(x));
    }
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals("1\tLABS=2345-7\t2012\t\t115.0\t1\t12.0\t12.0\t", lines.get(1));
  }

  @Test
  public void duplicate() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.2).year(2012).value("115").loinc("2345-7"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.2).year(2012).value("115").loinc("2345-7"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.3).year(2012).value("116").loinc("2345-7"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("PATIENTS(1)", "LABS")));
    for (int x = 0; x < lines.size(); x++) {
      System.out.println(lines.get(x));
    }
    Assert.assertEquals(3, lines.size());
    Assert.assertEquals("1\tLABS=2345-7\t2012\t\t115.0\t1\t12.0\t12.0\t", lines.get(1));
    Assert.assertEquals("1\tLABS=2345-7\t2012\t\t116.0\t1\t12.0\t12.0\t", lines.get(2));
  }

  @Test
  public void values() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.2).year(2012).loinc("2345-7").value("115"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.2).year(2012).loinc("2345-7").value("115"));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.3).year(2012).loinc("2345-7").value("116"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, csvQuery("LABS(\"2345-7\", 1, 116)", "LABS(\"2345-7\", 1, 116)")));
    for (int x = 0; x < lines.size(); x++) {
      System.out.println(lines.get(x));
    }
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals("1\tLABS(\"2345-7\", 1.0, 116.0)\t2012\t\t\t1\t12.0\t12.0\t", lines.get(1));
  }

}