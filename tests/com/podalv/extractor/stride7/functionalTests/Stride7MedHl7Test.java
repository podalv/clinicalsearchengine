package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.drugQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.patientsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DrugHl7Record;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7MedHl7Test {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getEndTimePatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012).ageEnd(15.5));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013).ageEnd(17.5));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getAdminTimePatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012).ageEnd(15.5).medId(5));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013).ageEnd(17.5).medId(7));

    source.addAdminLpch(1, 5, 13);
    source.addAdminLpch(1, 5, 17);

    source.addAdminShc(2, 7, 14);
    source.addAdminShc(2, 7, 16);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "unknown (hl7)")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "unknown (hl7)")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void endTimeTest() throws Exception {
    engine = getEndTimePatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "unknown (hl7)")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "unknown (hl7)")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(15))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(17))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void adminTimeTest() throws Exception {
    engine = getAdminTimePatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "inpatient ADMINISTERED")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "inpatient ADMINISTERED")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "inpatient ADMINISTERED"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13)),
        intervalQuery(Common.daysToTime(17), Common.daysToTime(17))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "inpatient ADMINISTERED"), unionQuery(intervalQuery(Common.daysToTime(14), Common.daysToTime(14)), intervalQuery(
        Common.daysToTime(16), Common.daysToTime(16))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(null).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(null).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(1200.0).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(1200.0).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, patientsQuery(1, 2)));
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(0.0).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(0.0).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.0).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.0).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, patientsQuery(1, 2)), 1, 2);
    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(
        Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(
        Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullMedicationId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(null).route(source.addMedicationRouteLpch("ORAL")).status("DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(null).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(2013));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(null).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(null).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullRoute() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(null).status("DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(null).status("STATUS2").year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "EMPTY", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "EMPTY", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullStatus() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(null).year(
        2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status(null).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "unknown (hl7)"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(null));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        null));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(null).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(2).age(null).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2013));

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullYearOtherNonNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(12.2).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(null));
    source.addMedHl7Shc(Stride7DrugHl7Record.create(1).age(13.2).medicationId(source.addMedicationIdHl7(3000)).route(source.addMedicationRouteShc("IV")).status("STATUS2").year(
        2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    source.addMedHl7Lpch(Stride7DrugHl7Record.create(1).age(null).medicationId(source.addMedicationIdHl7(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        "DISPENSED").year(2012));

    assertQuery(query(engine, timelineQuery()), 1);
  }

}