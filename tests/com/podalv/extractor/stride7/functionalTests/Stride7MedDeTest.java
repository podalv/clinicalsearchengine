package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.drugQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DrugRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7MedDeTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getPatientHistorical() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012).orderClassId(source.addMedicationClassLpch("Historical order")));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013).orderClassId(source.addMedicationClassShc("Patient supplied")));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getAdminPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012).medId(1));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013).medId(3));

    source.addAdminLpch(1, 1, 22);
    source.addAdminLpch(1, 1, 24);
    source.addAdminLpch(1, 1, 26);

    source.addAdminShc(2, 3, 32);
    source.addAdminShc(2, 3, 34);
    source.addAdminShc(2, 3, 36);

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getInpatientPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012).medId(1).orderingMode(2));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013).medId(3).orderingMode(1));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getEndDatePatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012).ageEnd(15.2));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013).ageEnd(17.2));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getStartDatePatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(10.2).ageStart(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(10.2).ageStart(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getDisconDatePatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012).ageDiscon(15.2));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013).ageDiscon(17.2));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getEndDisconDatePatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012).ageDiscon(15.2).ageEnd(17.2));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013).ageDiscon(17.2).ageEnd(15.2));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void alternatingShcLpch() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "outpatient")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "outpatient")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void historical() throws Exception {
    engine = getPatientHistorical();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "historical")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "historical")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "historical"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "historical"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void admin() throws Exception {
    engine = getAdminPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "inpatient administered")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "inpatient administered")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "inpatient administered"), unionQuery(intervalQuery(Common.daysToTime(22), Common.daysToTime(22)),
        intervalQuery(Common.daysToTime(24), Common.daysToTime(24)), intervalQuery(Common.daysToTime(26), Common.daysToTime(26))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "inpatient administered"), unionQuery(intervalQuery(Common.daysToTime(32), Common.daysToTime(32)), intervalQuery(
        Common.daysToTime(34), Common.daysToTime(34)), intervalQuery(Common.daysToTime(36), Common.daysToTime(36))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void inpatient() throws Exception {
    engine = getInpatientPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "inpatient prescribed")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "outpatient")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "inpatient prescribed"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void startDate() throws Exception {
    engine = getStartDatePatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "outpatient")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "outpatient")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void endDate() throws Exception {
    engine = getEndDatePatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "outpatient")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "outpatient")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(15))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(17))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void disconDate() throws Exception {
    engine = getDisconDatePatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "outpatient")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "outpatient")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(15))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(17))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void endDisconDate() throws Exception {
    engine = getEndDisconDatePatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, drugQuery(2000, "ORAL", "outpatient")), 1);
    assertQuery(query(engine, drugQuery(3000, "IV", "outpatient")), 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(15))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(17))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(null).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(null).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    source.addMedLpch(Stride7DrugRecord.create(1).age(1200.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(1300.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(0.0).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(0.0).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(
        Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(
        Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullMedicationId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(null).route(source.addMedicationRouteLpch("ORAL")).status(source.addMedicationStatusLpch(
        "DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(null).route(source.addMedicationRouteShc("IV")).status(source.addMedicationStatusShc("STATUS2")).year(
        2013));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullRoute() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(null).status(source.addMedicationStatusLpch("DISPENSED")).year(
        2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(null).status(source.addMedicationStatusShc("STATUS2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "EMPTY", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "EMPTY", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullStatus() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(12.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(null).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(13.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(null).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(drugQuery(2000, "ORAL", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(drugQuery(3000, "IV", "outpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addMedLpch(Stride7DrugRecord.create(1).age(null).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(null));
    source.addMedShc(Stride7DrugRecord.create(2).age(null).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(null));

    source.addMedLpch(Stride7DrugRecord.create(1).age(1200.2).medicationId(source.addMedicationIdLpch(2000)).route(source.addMedicationRouteLpch("ORAL")).status(
        source.addMedicationStatusLpch("DISPENSED")).year(2012));
    source.addMedShc(Stride7DrugRecord.create(2).age(1300.2).medicationId(source.addMedicationIdShc(3000)).route(source.addMedicationRouteShc("IV")).status(
        source.addMedicationStatusShc("STATUS2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

}