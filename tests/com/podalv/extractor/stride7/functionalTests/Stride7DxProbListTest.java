package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd10Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.originalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitPrimaryRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7DxProbListTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd9Query("255.55")), 2);

    assertQuery(query(engine, primaryQuery(icd9Query("250.50"))), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("255.55"))));

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 2);
  }

  @Test
  public void primary_original() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, icd9Query("250.50")), 1);

    assertQuery(query(engine, primaryQuery(icd9Query("250.5"))), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("250"))), 1);

    assertQuery(query(engine, originalQuery(icd9Query("250.50"))), 1);
    assertQuery(query(engine, originalQuery(icd9Query("250.5"))));
    assertQuery(query(engine, originalQuery(icd9Query("250"))));

    assertQuery(query(engine, primaryQuery(icd9Query("255.55"))));
    assertQuery(query(engine, originalQuery(icd9Query("255.55"))), 2);
    assertQuery(query(engine, primaryQuery(originalQuery(icd9Query("255.55")))));

    assertQuery(query(engine, originalQuery(icd10Query("A00.0"))), 1);
    assertQuery(query(engine, originalQuery(icd10Query("A00"))));

    assertQuery(query(engine, primaryQuery(originalQuery(icd9Query("250.50")))), 1);
    assertQuery(query(engine, primaryQuery(originalQuery(icd9Query("250.5")))));
    assertQuery(query(engine, primaryQuery(originalQuery(icd9Query("250")))));

    assertQuery(query(engine, primaryQuery(originalQuery(icd10Query("A00.0")))), 1);
    assertQuery(query(engine, primaryQuery(originalQuery(icd10Query("A00")))));

    assertQuery(query(engine, identicalQuery(primaryQuery(icd9Query("250.50")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(originalQuery(icd9Query("250.50")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(primaryQuery(icd9Query("250")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);

    assertQuery(query(engine, identicalQuery(primaryQuery(icd10Query("A00.0")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(originalQuery(icd10Query("A00.0")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(primaryQuery(icd10Query("A00")), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("250.5"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("250"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
  }

  @Test
  public void hierarchyTest() throws Exception {
    engine = getPatient();

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd9Query("250.5")), 1);
    assertQuery(query(engine, icd9Query("250")), 1);
    assertQuery(query(engine, icd9Query("001-999.99")), 1);
    assertQuery(query(engine, icd9Query("240-279.99")), 1);
    assertQuery(query(engine, icd9Query("249-259.99")), 1);
  }

  @Test
  public void years() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(null, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(null, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(13.3, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(0.0, 0.0).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(0.0, 0.0).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(13.3, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd9Query("255.55")), 2);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(12),
        Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(13),
        Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullDuration() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, null).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(12.2, null).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd9Query("255.55")), 2);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);
  }

  @Test
  public void nullAgeDuration() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(null, null).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(null, null).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(13.3, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullDxId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(null).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(null).year(2013));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("255.55"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male"));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(null).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(12.2, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(null));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("251.50", "A01.0", "251.50", "A01.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(13.3, 13.3).dxId(source.addDxIdShc("256.55", "B01.0", "256.55", "B01.0")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, identicalQuery(icd9Query("251.50"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(13))))), 1);
    assertQuery(query(engine, identicalQuery(icd9Query("256.55"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 2);
  }

  @Test
  public void primary() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.60", "A00.1")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(13.3, 13.3).dxId(source.addDxIdShc("255.55", "B00.0", "255.65", "B00.1")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, primaryQuery("ICD9=250.50")), 1);
    assertQuery(query(engine, primaryQuery("ICD9=250.60")), 1);

    assertQuery(query(engine, primaryQuery("ICD10=A00.0")), 1);
    assertQuery(query(engine, primaryQuery("ICD10=A00.1")), 1);

  }

}
