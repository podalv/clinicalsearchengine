package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitWithSabRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7PxCptTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, timelineQuery()), 1, 2);

    assertQuery(query(engine, cptQuery("25000")), 1);
    assertQuery(query(engine, cptQuery("35000")), 2);

    assertQuery(query(engine, identicalQuery(cptQuery("25000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("35000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("INPATIENT"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("OUTPATIENT"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(null).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(null).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(1200.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(1200.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullCode() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code(null).year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code(null).year(2013).sab("CPT").visitType("OUTPATIENT"));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(cptQuery("25000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("35000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(null).code("25000").year(null).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(null).code("35000").year(null).sab("CPT").visitType("OUTPATIENT"));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(1200.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(1200.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullSab() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab(null).visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab(null).visitType("OUTPATIENT"));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(cptQuery("25000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("35000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);
  }

  @Test
  public void nullVisitType() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab("CPT").visitType(null));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab("CPT").visitType(null));

    source.addVisitPxCptDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxCptDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(cptQuery("25000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(cptQuery("35000"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 2);
  }

}