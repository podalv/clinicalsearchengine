package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitPrimaryRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class EmptyPatientIcdTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getInvalidSingleEventPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(0.0, 0.0).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(0.0, 0.0).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getInvalidMultipleEventPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(0.0, 0.0).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(0.0, 0.0).dxId(source.addDxIdLpch("251.50", "A01.0", "251.50", "A01.0")).year(2012).primary("Y"));
    source.addDxProbListShc(Stride7VisitPrimaryRecord.create(2).age(0.0, 0.0).dxId(source.addDxIdShc("255.55", "B00.0", "255.55", "B00.0")).year(2013));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  private static ClinicalSearchEngine getValidPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(0.0, 0.0).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012).primary("Y"));
    source.addDxProbListLpch(Stride7VisitPrimaryRecord.create(1).age(1000.0, 10.0).dxId(source.addDxIdLpch("251.50", "A01.0", "251.50", "A01.0")).year(2012).primary("Y"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void emptySingleEvent() throws Exception {
    engine = getInvalidSingleEventPatient();
    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void emptyMultipleEvent() throws Exception {
    engine = getInvalidMultipleEventPatient();
    assertQuery(query(engine, timelineQuery()));
    assertQuery(query(engine, yearQuery(2000, 2020)));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void validEvent() throws Exception {
    engine = getValidPatient();
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, yearQuery(2000, 2020)), 1);
    Assert.assertTrue(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

}