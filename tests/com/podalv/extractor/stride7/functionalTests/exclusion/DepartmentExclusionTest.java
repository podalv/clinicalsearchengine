package com.podalv.extractor.stride7.functionalTests.exclusion;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.departmentQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class DepartmentExclusionTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws IOException {
    tearDown();
  }

  @After
  public void tearDown() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullAge_single_departmentId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(1225d).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(null).departmentId(source.getDepartmentIdShc("dept1")).year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void negativeAge_single_departmentId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(-1d).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(null).departmentId(source.getDepartmentIdShc("dept1")).year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void highAge_single_departmentId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(80000d).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(null).departmentId(source.getDepartmentIdShc("dept1")).year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()));
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void nullYear_single_departmentId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(1226d).departmentId(source.getDepartmentIdShc("dept1")).year(null));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void nullAge_multiple_departmentId() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(1225d).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(null).departmentId(source.getDepartmentIdShc("dept1")).year(2012));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(1225d).departmentId(source.getDepartmentIdLpch("dept2")).year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), intervalQuery(Common.daysToTime(1225), Common.daysToTime(1225)))), 1);
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, departmentQuery("dept1")), 1);
    assertQuery(query(engine, departmentQuery("dept2")), 1);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(1225), Common.daysToTime(1225)))));
  }

  @Test
  public void nullAge_single_departmentId_child() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(1d).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(0.0).departmentId(source.getDepartmentIdShc("dept1")).year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, departmentQuery("dept1")), 1);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(1), Common.daysToTime(1)))));
    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(0), Common.daysToTime(0)))));
  }

  @Test
  public void zeroAge_single_departmentId_child() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(0d).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(1).age(1d).departmentId(source.getDepartmentIdShc("dept1")).year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, departmentQuery("dept1")), 1);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(1),
        Common.daysToTime(1))))), 1);
  }

}
