package com.podalv.extractor.stride7.functionalTests.exclusion;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd10Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class HospAcctExclusionTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void ageNull() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addHospAcctLpch(Stride7VisitRecord.create(1).age(null, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012));
    source.addHospAcctShc(Stride7VisitRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")).year(2013));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void ageNegative() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addHospAcctLpch(Stride7VisitRecord.create(1).age(-1.1, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012));
    source.addHospAcctShc(Stride7VisitRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")).year(2013));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void ageHigh() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addHospAcctLpch(Stride7VisitRecord.create(1).age(80000.1, 13.3).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012));
    source.addHospAcctShc(Stride7VisitRecord.create(1).age(12.2, 13.3).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")).year(2013));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void age_zero_adult() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addHospAcctLpch(Stride7VisitRecord.create(1).age(0d, 13.3d).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012));
    source.addHospAcctShc(Stride7VisitRecord.create(1).age(1222.2d, 1223.3d).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")).year(2013));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
  }

  @Test
  public void age_zero_child() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addHospAcctLpch(Stride7VisitRecord.create(1).age(0d, 13.3d).dxId(source.addDxIdLpch("250.50", "A00.0", "250.50", "A00.0")).year(2012));
    source.addHospAcctShc(Stride7VisitRecord.create(1).age(14.2d, 15.3d).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")).year(2013));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);

    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, icd10Query("A00.0")), 1);

    assertQuery(query(engine, identicalQuery(icd9Query("250.50"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(13)), intervalQuery(Common.daysToTime(14),
        Common.daysToTime(15))))), 1);
  }

}