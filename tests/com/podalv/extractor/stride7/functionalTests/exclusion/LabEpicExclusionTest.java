package com.podalv.extractor.stride7.functionalTests.exclusion;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7LabEpicRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class LabEpicExclusionTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void ageNegative() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(-1000.1).loinc("2005-5").value("2.8").year(2012));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(1000.1).loinc("2005-5").value("2.8").year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("2005-5")), 1);

    assertQuery(query(engine, identicalQuery(labsQuery("2005-5"), unionQuery(intervalQuery(Common.daysToTime(1000), Common.daysToTime(1000))))), 1);
  }

  @Test
  public void ageHigh() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(80000.1).loinc("2005-5").value("2.8").year(2012));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(1000.1).loinc("2005-5").value("2.8").year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("2005-5")), 1);

    assertQuery(query(engine, identicalQuery(labsQuery("2005-5"), unionQuery(intervalQuery(Common.daysToTime(1000), Common.daysToTime(1000))))), 1);
  }

  @Test
  public void zeroChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(0.0).loinc("2005-5").value("2.8").year(2012));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(12.1).loinc("2005-5").value("2.8").year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("2005-5")), 1);

    assertQuery(query(engine, identicalQuery(labsQuery("2005-5"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(12),
        Common.daysToTime(12))))), 1);
  }

  @Test
  public void zeroAdult() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));

    source.addLabEpicLpch(Stride7LabEpicRecord.create(1).age(0.0).loinc("2005-5").value("2.8").year(2012));
    source.addLabEpicShc(Stride7LabEpicRecord.create(1).age(1200.1).loinc("2005-5").value("2.8").year(2012));

    engine = Stride7TestHarness.engineWithExclusion(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, labsQuery("2005-5")), 1);

    assertQuery(query(engine, identicalQuery(labsQuery("2005-5"), unionQuery(intervalQuery(Common.daysToTime(1200), Common.daysToTime(1200))))), 1);
  }

}