package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.departmentQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.workshop.ContainsPatientRequest;

public class Stride7DepartmentTest {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws IOException {
    tearDown();
  }

  @After
  public void tearDown() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));
    source.addDemographics(Stride7Demographics.create(2).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(3).gender("other"));
    source.addDemographics(Stride7Demographics.create(4).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(5));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(2).age(12.5).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.5).departmentId(source.getDepartmentIdShc("dept2")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(3).age(15.5).departmentId(source.getDepartmentIdShc("dept2")).year(2013));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(4).age(12.5).departmentId(source.getDepartmentIdLpch("dept3")).year(2012));
    source.addDepartmentApptShc(Stride7DepartmentRecord.create(5).age(15.5).departmentId(source.getDepartmentIdShc("dept4")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()), 2, 3, 4, 5);
    assertQuery(query(engine, departmentQuery("dept1")), 2);
    assertQuery(query(engine, departmentQuery("dept2")), 2, 3);
    assertQuery(query(engine, departmentQuery("dept3")), 4);
    assertQuery(query(engine, departmentQuery("dept4")), 5);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 2);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), intervalQuery(Common.daysToTime(15), Common.daysToTime(15)))), 3);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept3"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 4);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept4"), intervalQuery(Common.daysToTime(15), Common.daysToTime(15)))), 5);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("other"));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(null).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(null).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.2).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.3).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()));
  }

  @Test
  public void zeroAgeChild() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("other"));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(0.0).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(0.0).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.2).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.3).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(12),
        Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), unionQuery(intervalQuery(Common.daysToTime(0), Common.daysToTime(0)), intervalQuery(Common.daysToTime(13),
        Common.daysToTime(13))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("other"));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(null).departmentId(source.getDepartmentIdLpch("dept1")).year(null));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(null).departmentId(source.getDepartmentIdShc("dept2")).year(null));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(1200.2).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(1300.3).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
    assertQuery(query(engine, timelineQuery()));

    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(1)).getResponse());
    Assert.assertFalse(engine.containsPatient(new ContainsPatientRequest(2)).getResponse());
  }

  @Test
  public void nullDepartment() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("other"));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.2).departmentId(source.getDepartmentIdLpch(null)).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.3).departmentId(source.getDepartmentIdShc(null)).year(2013));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.2).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.3).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
  }

  @Test
  public void emptyDepartment() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("other"));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.2).departmentId(source.getDepartmentIdLpch("")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.3).departmentId(source.getDepartmentIdShc("")).year(2013));

    source.addDepartmentLpch(Stride7DepartmentRecord.create(1).age(12.2).departmentId(source.getDepartmentIdLpch("dept1")).year(2012));
    source.addDepartmentShc(Stride7DepartmentRecord.create(2).age(13.3).departmentId(source.getDepartmentIdShc("dept2")).year(2013));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(departmentQuery("dept1"), intervalQuery(Common.daysToTime(12), Common.daysToTime(12)))), 1);
    assertQuery(query(engine, identicalQuery(departmentQuery("dept2"), intervalQuery(Common.daysToTime(13), Common.daysToTime(13)))), 2);
  }

}
