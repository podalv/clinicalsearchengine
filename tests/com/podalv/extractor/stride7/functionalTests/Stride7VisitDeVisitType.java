package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitTypeRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class Stride7VisitDeVisitType {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(12.1).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(2012));
    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(12.1).encType(source.getVisitTypeIdLpch("type2")).hospAdm(13.5, 14.5).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(1).age(13.2).encType(source.getVisitTypeIdShc("type3")).year(2012));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(1).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));

    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(15.2).encType(source.getVisitTypeIdShc("type4")).year(2014));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(16.2).encType(source.getVisitTypeIdShc("type5")).year(2014));
    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(2).age(17.2).encType(source.getVisitTypeIdLpch("type6")).year(2014));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(2).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2014));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, visitTypeQuery("type1")), 1);
    assertQuery(query(engine, visitTypeQuery("inpatient")), 1);
    assertQuery(query(engine, visitTypeQuery("type2")), 1);
    assertQuery(query(engine, visitTypeQuery("type3")), 1);

    assertQuery(query(engine, visitTypeQuery("type4")), 2);
    assertQuery(query(engine, visitTypeQuery("type5")), 2);
    assertQuery(query(engine, visitTypeQuery("type6")), 2);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type2"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("inpatient"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(14))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type3"), unionQuery(intervalQuery(Common.daysToTime(13), Common.daysToTime(13))))), 1);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("type4"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(15))))), 2);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type5"), unionQuery(intervalQuery(Common.daysToTime(16), Common.daysToTime(16))))), 2);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type6"), unionQuery(intervalQuery(Common.daysToTime(17), Common.daysToTime(17))))), 2);

    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2014, 2014)), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(null).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(null).encType(source.getVisitTypeIdShc("type4")).year(2014));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(12.1).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(15.1).encType(source.getVisitTypeIdShc("type4")).year(2014));

    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(1).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(2).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type4"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(15))))), 2);
  }

  @Test
  public void nullVisitType() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(12.1).encType(null).hospAdm(null, null).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(15.2).encType(null).year(2014));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(12.1).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(15.1).encType(source.getVisitTypeIdShc("type4")).year(2014));

    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(1).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(2).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type4"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(15))))), 2);
  }

  @Test
  public void nullAgeNonNullHospAdm() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(null).encType(source.getVisitTypeIdLpch("type1")).hospAdm(12.2, 15.5).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(null).encType(source.getVisitTypeIdShc("type4")).hospAdm(15.2, 17.5).year(2013));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(12.1).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(15.1).encType(source.getVisitTypeIdShc("type4")).year(2014));

    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(1).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));
    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(2).age(1.1).departmentId(source.getDepartmentIdLpch("a1")).year(2012));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, identicalQuery(visitTypeQuery("type1"), unionQuery(intervalQuery(Common.daysToTime(12), Common.daysToTime(12))))), 1);
    assertQuery(query(engine, identicalQuery(visitTypeQuery("type4"), unionQuery(intervalQuery(Common.daysToTime(15), Common.daysToTime(15))))), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));
    source.addDemographics(Stride7Demographics.create(3).gender("male").death(22.0));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(null).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(null));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(null).encType(source.getVisitTypeIdShc("type4")).year(null));

    source.addVisitTypeLpch(Stride7VisitTypeRecord.create(1).age(1200.1).encType(source.getVisitTypeIdLpch("type1")).hospAdm(null, null).year(2012));
    source.addVisitTypeShc(Stride7VisitTypeRecord.create(2).age(1500.1).encType(source.getVisitTypeIdShc("type4")).year(2014));

    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);

    assertQuery(query(engine, timelineQuery()));
  }

}
