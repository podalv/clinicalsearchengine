package com.podalv.extractor.stride7.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.ageQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;

public class AgesTests {

  private static ClinicalSearchEngine engine;

  @Before
  public void setup() throws IOException {
    tearDown();
  }

  @After
  public void tearDown() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private class PatientStartEnd {

    private final int pid;
    private final int start;
    private final int end;

    public PatientStartEnd(final int pid, final int start, final int end) {
      this.pid = pid;
      this.start = start;
      this.end = end;
    }

    public int getPid() {
      return pid;
    }

    public int getEnd() {
      return end;
    }

    public int getStart() {
      return start;
    }
  }

  private void getEngineWithPatients(final PatientStartEnd ... patients) throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException,
      ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    for (final PatientStartEnd patient : patients) {
      source.addDemographics(Stride7Demographics.create(patient.getPid()));
      source.addVisitAdmitShc(Stride7VisitRecord.create(patient.getPid()).age(Common.minutesToDays(Common.yearsToTime(patient.getStart())), Common.minutesToDays(Common.yearsToTime(
          patient.getEnd()))).year(2012).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")));
    }
    engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void smokeTest() throws Exception {
    getEngineWithPatients(new PatientStartEnd[] {new PatientStartEnd(1, 1, 5), new PatientStartEnd(2, 2, 3), new PatientStartEnd(3, 3, 5), new PatientStartEnd(4, 7, 10)});

    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Integer.MIN_VALUE, Common.yearsToTime(8))), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Integer.MIN_VALUE, Common.yearsToTime(1))), 1);
    assertQuery(query(engine, ageQuery(Integer.MIN_VALUE, Common.yearsToTime(3))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Integer.MIN_VALUE, Common.yearsToTime(2))), 1, 2);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Common.yearsToTime(1))), 1);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Common.yearsToTime(2))), 1, 2);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Common.yearsToTime(3))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Common.yearsToTime(5))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Common.yearsToTime(7))), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Common.yearsToTime(10))), 1, 2, 3, 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(2), Common.yearsToTime(2))), 1, 2);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(2), Common.yearsToTime(3))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(2), Common.yearsToTime(4))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(2), Common.yearsToTime(7))), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(2), Common.yearsToTime(10))), 1, 2, 3, 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(3), Common.yearsToTime(3))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(3), Common.yearsToTime(6))), 1, 2, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(3), Common.yearsToTime(8))), 1, 2, 3, 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(4), Common.yearsToTime(4))), 1, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(4), Common.yearsToTime(5))), 1, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(4), Common.yearsToTime(6))), 1, 3);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(4), Common.yearsToTime(7))), 1, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(4), Common.yearsToTime(11))), 1, 3, 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(5), Common.yearsToTime(8))), 1, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(5), Common.yearsToTime(11))), 1, 3, 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(5), Common.yearsToTime(5))), 1, 3);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(6), Common.yearsToTime(6))));
    assertQuery(query(engine, ageQuery(Common.yearsToTime(6), Common.yearsToTime(7))), 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(7), Common.yearsToTime(7))), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(7), Common.yearsToTime(10))), 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(8), Common.yearsToTime(9))), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(8), Common.yearsToTime(11))), 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(10), Common.yearsToTime(10))), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(10), Common.yearsToTime(12))), 4);

    assertQuery(query(engine, ageQuery(Common.yearsToTime(11), Common.yearsToTime(12))));

    assertQuery(query(engine, ageQuery(Common.yearsToTime(0), Integer.MAX_VALUE)), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(1), Integer.MAX_VALUE)), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(2), Integer.MAX_VALUE)), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(3), Integer.MAX_VALUE)), 1, 2, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(4), Integer.MAX_VALUE)), 1, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(5), Integer.MAX_VALUE)), 1, 3, 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(6), Integer.MAX_VALUE)), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(7), Integer.MAX_VALUE)), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(9), Integer.MAX_VALUE)), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(10), Integer.MAX_VALUE)), 4);
    assertQuery(query(engine, ageQuery(Common.yearsToTime(11), Integer.MAX_VALUE)));
  }

  @Test
  public void forEachTest() throws Exception {
    getEngineWithPatients(new PatientStartEnd[] {new PatientStartEnd(1, 1, 5), new PatientStartEnd(2, 2, 3), new PatientStartEnd(3, 3, 5), new PatientStartEnd(4, 7, 10)});
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(DATA_FOLDER));
    PatientExport.setInstance(new PatientExport(DATA_FOLDER));
    query(engine, "FOR EACH (ICD9=250.50) AS (AAA) {RETURN AAA AS AAA;}\n CSV(AAA, TIME=AAA, LABS)");
  }

}
