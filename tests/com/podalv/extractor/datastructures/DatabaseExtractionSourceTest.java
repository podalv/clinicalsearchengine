package com.podalv.extractor.datastructures;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.db.test.MockDataTable;
import com.podalv.db.test.MockResultSet;

public class DatabaseExtractionSourceTest {

  @Test
  public void allTypes() throws Exception {
    final MockDataTable table = new MockDataTable("1", "2", "3", "4", "5", "6");
    table.addRow(Integer.MAX_VALUE + "", Long.MAX_VALUE + "", 1 + "", 2 + "", 0.1 + "", "str");
    final DatabaseExtractionSource src = new DatabaseExtractionSource(new MockResultSet(table));
    Assert.assertTrue(src.next());
    Assert.assertEquals(Integer.MAX_VALUE, src.getInt(1));
    Assert.assertEquals(Long.MAX_VALUE, src.getLong(2));
    Assert.assertEquals(1, src.getShort(3));
    Assert.assertEquals(2, src.getByte(4));
    Assert.assertEquals(0.1, src.getDouble(5), 0.001);
    Assert.assertEquals("str", src.getString(6));
    Assert.assertFalse(src.isAfterLast());
    Assert.assertFalse(src.next());
    Assert.assertTrue(src.isAfterLast());
    src.close();
  }
}
