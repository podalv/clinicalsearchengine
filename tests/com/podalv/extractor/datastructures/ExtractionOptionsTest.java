package com.podalv.extractor.datastructures;

import org.junit.Assert;
import org.junit.Test;

public class ExtractionOptionsTest {

  @Test
  public void all_true() throws Exception {
    final ExtractionOptions options = new ExtractionOptions("dit");
    Assert.assertTrue(options.extractDictionary());
    Assert.assertTrue(options.extractIcd9());
    Assert.assertTrue(options.extractTerms());
  }

  @Test
  public void all_false() throws Exception {
    final ExtractionOptions options = new ExtractionOptions("");
    Assert.assertFalse(options.extractDictionary());
    Assert.assertFalse(options.extractIcd9());
    Assert.assertFalse(options.extractTerms());
  }

}