package com.podalv.extractor.datastructures;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;

public class BinaryFileWriterTest {

  @Test
  public void smokeTest() throws Exception {
    final DATA_TYPE[] structure = new DATA_TYPE[] {DATA_TYPE.BYTE, DATA_TYPE.DOUBLE, DATA_TYPE.INT, DATA_TYPE.SHORT, DATA_TYPE.STRING};
    final ByteArrayOutputStream dataOut = new ByteArrayOutputStream();
    final ByteArrayOutputStream dictOut = new ByteArrayOutputStream();
    final BinaryFileWriter writer = new BinaryFileWriter(dataOut, dictOut, structure);
    for (int x = 0; x < 100; x++) {
      writer.write(x, x + 0.1, x, x, x + "");
    }
    writer.close();
    final BinaryFileExtractionSource source = new BinaryFileExtractionSource(new ByteArrayInputStream(dataOut.toByteArray()), new ByteArrayInputStream(dictOut.toByteArray()),
        structure);
    for (int x = 0; x < 100; x++) {
      Assert.assertTrue(source.next());
      Assert.assertEquals(x, source.getByte(1));
      Assert.assertEquals(x + 0.1, source.getDouble(2), 0.001);
      Assert.assertEquals(x, source.getInt(3));
      Assert.assertEquals(x, source.getShort(4));
      Assert.assertEquals(x + "", source.getString(5));
    }
    Assert.assertFalse(source.next());
    source.close();
  }

  @Test
  public void typeChanges() throws Exception {
    final DATA_TYPE[] structure = new DATA_TYPE[] {DATA_TYPE.STRING};
    final ByteArrayOutputStream dataOut = new ByteArrayOutputStream();
    final ByteArrayOutputStream dictOut = new ByteArrayOutputStream();
    final BinaryFileWriter writer = new BinaryFileWriter(dataOut, dictOut, structure);
    for (int x = 0; x < 100000; x++) {
      writer.write(x + "");
    }
    writer.close();
    final BinaryFileExtractionSource source = new BinaryFileExtractionSource(new ByteArrayInputStream(dataOut.toByteArray()), new ByteArrayInputStream(dictOut.toByteArray()),
        structure);
    for (int x = 0; x < 100000; x++) {
      Assert.assertTrue(source.next());
      Assert.assertEquals(x + "", source.getString(1));
    }
    Assert.assertFalse(source.next());
    source.close();
  }

}
