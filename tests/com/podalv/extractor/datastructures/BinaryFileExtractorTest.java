package com.podalv.extractor.datastructures;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;

public class BinaryFileExtractorTest {

  private static final File TEST_FOLDER = new File(".", "extractionTestData");

  @SuppressWarnings("resource")
  @Test
  public void constructorNull() throws Exception {
    Assert.assertTrue(new BinaryFileExtractionSource(null, new File("."), DATA_TYPE.SHORT).isAfterLast());
    Assert.assertTrue(new BinaryFileExtractionSource(new File("."), null, DATA_TYPE.BYTE).isAfterLast());
    Assert.assertTrue(new BinaryFileExtractionSource(null, BinaryFileExtractorTest.class.getResourceAsStream("test.txt"), DATA_TYPE.BYTE).isAfterLast());
    Assert.assertTrue(new BinaryFileExtractionSource(BinaryFileExtractorTest.class.getResourceAsStream("test.txt"), null, DATA_TYPE.SHORT).isAfterLast());
  }

  @Test
  public void multipleFiledTypes() throws Exception {
    final DATA_TYPE[] structure = new DATA_TYPE[] {DATA_TYPE.BYTE, DATA_TYPE.DOUBLE, DATA_TYPE.INT, DATA_TYPE.LONG, DATA_TYPE.SHORT, DATA_TYPE.STRING};
    final File data = new File(TEST_FOLDER, "binary_extraction_test");
    final File dict = new File(TEST_FOLDER, "binary_extraction_test_dict");
    final BinaryFileWriter writer = new BinaryFileWriter(data, structure);
    writer.write(1, 0.1, 2, 3, 4, "5");
    writer.close();
    final BinaryFileExtractionSource src = new BinaryFileExtractionSource(data, dict, structure);
    Assert.assertTrue(src.next());
    Assert.assertEquals(1, src.getByte(1));
    Assert.assertEquals(0.1, src.getDouble(2), 0.00001);
    Assert.assertEquals(2, src.getInt(3));
    Assert.assertEquals(3, src.getLong(4));
    Assert.assertEquals(4, src.getShort(5));
    Assert.assertEquals("5", src.getString(6));
    Assert.assertEquals("1", src.getString(1));
    Assert.assertEquals("0.1", src.getString(2));
    Assert.assertEquals("2", src.getString(3));
    Assert.assertEquals("3", src.getString(4));
    Assert.assertEquals("4", src.getString(5));
    Assert.assertEquals("1, 0.1, 2, 3, 4, 5, ", src.toString());
    Assert.assertFalse(src.next());
    src.close();
  }
}
