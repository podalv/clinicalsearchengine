package com.podalv.extractor.datastructures.records;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

public class LabsRecordTest {

  @Test
  public void getCalculatedValue() throws Exception {
    Assert.assertEquals("AAA", new LabsRecord("a", 1, 2016, "AAA").getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, Double.MIN_VALUE, Double.MIN_VALUE, 1, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, Double.MIN_VALUE, 1, Double.MIN_VALUE, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, Double.MIN_VALUE, 1, 1, 2016, null).getCalculatedValue());
    Assert.assertEquals("NO VALUE", new LabsRecord("a", 1, Integer.MIN_VALUE, 1, 1, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 100, -1, 1, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 100, 1, -1, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 100, 0, 0, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 100, 0, 1, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 5, 7, 10, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 5, 4, 10, 2016, null).getCalculatedValue());
    Assert.assertNull(new LabsRecord("a", 1, 5, 5, 5, 2016, null).getCalculatedValue());
  }

  @Test
  public void sort() throws Exception {
    final ArrayList<LabsRecord> labs = new ArrayList<LabsRecord>();
    labs.add(new LabsRecord("a", 1, 2, "a"));
    labs.add(new LabsRecord("a", 1, 2, "b"));
    labs.add(new LabsRecord("a", 2, 2, "a"));
    labs.add(new LabsRecord("b", 1, 2, "a"));
    labs.add(new LabsRecord("a", 1, 2, "a"));
    Collections.sort(labs);
    Assert.assertEquals("a", labs.get(0).getCode());
    Assert.assertEquals(1, labs.get(0).getTime());
    Assert.assertEquals(2, labs.get(0).getYear());
    Assert.assertEquals("a", labs.get(0).getCalculatedValue());
    Assert.assertFalse(labs.get(0).equals(labs.get(1)));
    Assert.assertEquals("a", labs.get(2).getCode());
    Assert.assertEquals(2, labs.get(2).getTime());
    Assert.assertEquals("a", labs.get(3).getCalculatedValue());
    Assert.assertEquals("b", labs.get(4).getCode());
  }

  @Test
  public void hashMap() throws Exception {
    final HashSet<LabsRecord> map = new HashSet<LabsRecord>();
    final ArrayList<LabsRecord> list = new ArrayList<LabsRecord>();
    int cnt = 0;
    for (int code = 0; code < 2; code++) {
      for (int calculatedValue = 0; calculatedValue < 2; calculatedValue++) {
        for (int value = 0; value < 2; value++) {
          for (int high = 0; high < 2; high++) {
            for (int low = 0; low < 2; low++) {
              for (int time = 0; time < 2; time++) {
                for (int year = 0; year < 2; year++) {
                  cnt++;
                  list.add(new LabsRecord(code + "", time, value, low, high, year, calculatedValue + ""));
                  map.add(new LabsRecord(code + "", time, value, low, high, year, calculatedValue + ""));
                  map.add(new LabsRecord(code + "", time, value, low, high, year, calculatedValue + ""));
                }
              }
            }
          }
        }
      }
    }

    for (int x = 0; x < list.size(); x++) {
      for (int y = 0; y < list.size(); y++) {
        if (x == y) {
          Assert.assertTrue(list.get(x).equals(list.get(y)));
          Assert.assertEquals(list.get(x).hashCode(), list.get(y).hashCode());
        }
        else {
          Assert.assertFalse(list.get(x).equals(list.get(y)));
        }
      }
    }
    Assert.assertEquals(cnt, map.size());
  }

  @Test
  public void equalsDifferentObjects() throws Exception {
    final LabsRecord rec = new LabsRecord("1", 2, 4, 4, 4, 3, "a");
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
  }
}
