package com.podalv.extractor.datastructures.records;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

public class TermRecordTest {

  @Test
  public void sorting() throws Exception {
    final ArrayList<TermRecord> terms = new ArrayList<>();
    terms.add(new TermRecord(1, 2, 5, 4, 1));
    terms.add(new TermRecord(2, 1, 5, 4, 1));
    terms.add(new TermRecord(3, 3, 4, 4, 1));
    terms.add(new TermRecord(4, 1, 4, 4, 1));
    terms.get(0).setAge(4);
    terms.get(1).setAge(3);
    terms.get(2).setAge(2);
    terms.get(3).setAge(1);
    Collections.sort(terms);
    Assert.assertEquals(4, terms.get(0).getTermId());
    Assert.assertEquals(3, terms.get(1).getTermId());
    Assert.assertEquals(2, terms.get(2).getTermId());
    Assert.assertEquals(1, terms.get(3).getTermId());
  }

  @Test
  public void hashCodeEquals() throws Exception {
    final TermRecord rec = new TermRecord(1, 2, 5, 4, 6);
    Assert.assertEquals(rec.getNoteId(), rec.hashCode());
    Assert.assertTrue(rec.equals(new TermRecord(1, 2, 5, 4, 6)));
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
    Assert.assertFalse(rec.equals(new TermRecord(2, 2, 5, 4, 6)));
    Assert.assertFalse(rec.equals(new TermRecord(1, 3, 5, 4, 6)));
    Assert.assertFalse(rec.equals(new TermRecord(1, 2, 2, 4, 6)));
    Assert.assertFalse(rec.equals(new TermRecord(1, 2, 5, 5, 6)));
    Assert.assertFalse(rec.equals(new TermRecord(1, 2, 5, 4, 7)));
    Assert.assertTrue(rec.equals(new TermRecord(1, 2, 5, 4, 6)));
  }
}
