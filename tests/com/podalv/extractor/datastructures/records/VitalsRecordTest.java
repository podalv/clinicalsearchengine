package com.podalv.extractor.datastructures.records;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

public class VitalsRecordTest {

  @Test
  public void sorting() throws Exception {
    final ArrayList<VitalsRecord> values = new ArrayList<>();
    values.add(new VitalsRecord(10, "value1", 0.5));
    values.add(new VitalsRecord(9, "value1", 0.4));
    values.add(new VitalsRecord(8, "value1", 0.5));
    values.add(new VitalsRecord(12, "value2", 0.5));
    Collections.sort(values);
    Assert.assertEquals(Integer.valueOf(9), values.get(0).getAge());
    Assert.assertEquals(Integer.valueOf(8), values.get(1).getAge());
    Assert.assertEquals(Integer.valueOf(10), values.get(2).getAge());
    Assert.assertEquals(Integer.valueOf(12), values.get(3).getAge());
  }

  @Test
  public void hashCodeEquals() throws Exception {
    final VitalsRecord rec = new VitalsRecord(10, "value1", 0.5);
    Assert.assertEquals(rec.getDescription().hashCode(), rec.hashCode());
    Assert.assertTrue(rec.equals(new VitalsRecord(10, "value1", 0.5)));
    Assert.assertFalse(rec.equals(new VitalsRecord(1, "value1", 0.5)));
    Assert.assertFalse(rec.equals(new VitalsRecord(10, "value2", 0.5)));
    Assert.assertFalse(rec.equals(new VitalsRecord(10, "value1", 0.1)));
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
  }
}
