package com.podalv.extractor.datastructures.records;

import org.junit.Assert;
import org.junit.Test;

public class VisitRecordTest {

  @Test
  public void sab_icd9() throws Exception {
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "ICD", -1, null).isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "DX_ID", -1, null).isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "250", null).isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "250.1", null).isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "250.12", null).isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "E250.12", null).isIcd9Code());
  }

  @Test
  public void sab_cpt() throws Exception {
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "CPT", "25000", null).isCptCode());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "25000", null).isCptCode());
  }

  @Test
  public void hashCodeEquals() throws Exception {
    final VisitRecord rec = new VisitRecord(1, 2002, 15, 25, "CPT", "25000", "aaa");
    Assert.assertEquals(rec.getAge(), Integer.valueOf(rec.hashCode()));
    Assert.assertTrue(rec.equals(new VisitRecord(1, 2002, 15, 25, "CPT", "25000", "aaa")));
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
    Assert.assertFalse(rec.equals(new VisitRecord(2, 2002, 15, 25, "CPT", "25000", "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2003, 15, 25, "CPT", "25000", "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 11, 25, "CPT", "25000", "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 15, "CPT", "25000", "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 25, "xCPT", "25000", "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 25, "CPT", null, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 25, "CPT", "25000", null)));
    Assert.assertFalse(rec.equals(new VisitRecord(2, 2002, 15, 25, "CPT", "25000", "aaa").setPrimary(true)));
  }

  @Test
  public void equalsCodeIsInt() throws Exception {
    final VisitRecord rec = new VisitRecord(1, 2002, 15, 25, "CPT", 1, "aaa");
    Assert.assertEquals(rec.getAge(), Integer.valueOf(rec.hashCode()));
    Assert.assertTrue(rec.equals(new VisitRecord(1, 2002, 15, 25, "CPT", 1, "aaa")));
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
    Assert.assertFalse(rec.equals(new VisitRecord(2, 2002, 15, 25, "CPT", 1, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2003, 15, 25, "CPT", 1, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 11, 25, "CPT", 1, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 15, "CPT", 1, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 25, "xCPT", 1, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 25, "CPT", null, "aaa")));
    Assert.assertFalse(rec.equals(new VisitRecord(1, 2002, 15, 25, "CPT", 1, null)));
    Assert.assertFalse(rec.equals(new VisitRecord(2, 2002, 15, 25, "CPT", 1, "aaa").setPrimary(true)));
  }

  @Test
  public void codeTypesCpt() throws Exception {
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "CPT", "25000", "aaa").isCptCode());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "25000", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, null, null, "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, null, "25000", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", "2500.", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", "2500.", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", "250000", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", "250000", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", null, "aaa").isCptCode());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "IDX", "A2500", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "25000", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "Abcde", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "Ab111", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "A1b11", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "A11b1", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "A111b", "aaa").isCptCode());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "IDX", "A111", "aaa").isCptCode());
  }

  @Test
  public void codeTypesIcd9() throws Exception {
    //(sab != null && (sab.startsWith("ICD") || sab.equals("DX_ID") || (sab.equalsIgnoreCase("BILLING") && code != null && (code.length() < 4 || code.indexOf('.') != -1))))
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "ICD", "250.00", "aaa").isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "DX_ID", "250.00", "aaa").isIcd9Code());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, null, "250.00", "aaa").isIcd9Code());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", null, "aaa").isIcd9Code());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", null, "aaa").isIcd9Code());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", "122222", "aaa").isIcd9Code());
    Assert.assertFalse(new VisitRecord(1, 2002, 15, 25, "BILLING", "12345", "aaa").isIcd9Code());
    Assert.assertTrue(new VisitRecord(1, 2002, 15, 25, "BILLING", "123.45", "aaa").isIcd9Code());
  }

}
