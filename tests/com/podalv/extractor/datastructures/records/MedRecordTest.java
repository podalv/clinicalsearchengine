package com.podalv.extractor.datastructures.records;

import org.junit.Assert;
import org.junit.Test;

public class MedRecordTest {

  @Test
  public void equalsTest() throws Exception {
    final MedRecord rec = new MedRecord(1, 2, 2002, "a", 1, "b");
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
    Assert.assertTrue(rec.equals(new MedRecord(1, 2, 2002, "a", 1, "b")));
    Assert.assertFalse(rec.equals(new MedRecord(2, 2, 2002, "a", 1, "b")));
    Assert.assertFalse(rec.equals(new MedRecord(1, 1, 2002, "a", 1, "b")));
    Assert.assertFalse(rec.equals(new MedRecord(1, 2, 2003, "a", 1, "b")));
    Assert.assertFalse(rec.equals(new MedRecord(1, 2, 2002, null, 1, "b")));
    Assert.assertFalse(rec.equals(new MedRecord(1, 2, 2002, "a", 2, "b")));
    Assert.assertFalse(rec.equals(new MedRecord(1, 2, 2002, "a", 1, "c")));
  }

  @Test
  public void hashCodeTest() throws Exception {
    final MedRecord rec = new MedRecord(1, 2, 2002, "a", 1, "b");
    Assert.assertEquals(rec.getRxCui().intValue(), rec.hashCode());
  }

  @Test
  public void settersGetters() throws Exception {
    final MedRecord rec = new MedRecord(1, 2, 2002, "a", 1, "b");
    rec.setDuration(22);
    Assert.assertEquals(22, rec.getDuration());
    Assert.assertEquals(2, rec.getEnd());
  }

}
