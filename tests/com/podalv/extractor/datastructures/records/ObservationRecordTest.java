package com.podalv.extractor.datastructures.records;

import org.junit.Assert;
import org.junit.Test;

public class ObservationRecordTest {

  @Test
  public void equalsHashCodeTest() throws Exception {
    final ObservationRecord rec = new ObservationRecord("111", 1, 2002, "bbb");
    Assert.assertEquals(rec.getAge(), Integer.valueOf(rec.hashCode()));
    Assert.assertTrue(rec.equals(new ObservationRecord("111", 1, 2002, "bbb")));
    Assert.assertFalse(rec.equals(null));
    Assert.assertFalse(rec.equals("a"));
    Assert.assertFalse(rec.equals(new ObservationRecord("11", 1, 2002, "bbb")));
    Assert.assertFalse(rec.equals(new ObservationRecord("111", 2, 2002, "bbb")));
    Assert.assertFalse(rec.equals(new ObservationRecord("111", 1, 2003, "bbb")));
    Assert.assertFalse(rec.equals(new ObservationRecord("111", 1, 2003, "cbbb")));
  }
}
