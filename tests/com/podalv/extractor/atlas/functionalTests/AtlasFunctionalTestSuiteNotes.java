package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.andQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.noteQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.noteTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notesQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.orQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textFhQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textNegatedQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.datastructures.CommonWordsFilter;
import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockNoteRecord;
import com.podalv.extractor.test.datastructures.MockTermDictionaryRecord;
import com.podalv.extractor.test.datastructures.MockTermRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalTestSuiteNotes {

  @Before
  public void setup() {
    CommonWordsFilter.reset();
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullNoteId() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(null).docDescription("abc").age(1.2).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(null).termId(1));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertError(query(engine, noteTypeQuery("abc")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("EMPTY")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("null")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, textQuery("abcd")), "TEXT information is missing in this dataset");
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void nullTid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(1).docDescription("abc").age(1.2).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(null));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("EMPTY")));
    assertQuery(query(engine, noteTypeQuery("null")));
    assertQuery(query(engine, noteTypeQuery("abc")), 1);
    assertQuery(query(engine, textQuery("abcd")));
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void nonExistingTid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(1).docDescription("abc").age(1.2).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "abcd"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("abc")), 1);
    assertQuery(query(engine, noteTypeQuery("EMPTY")));
    assertQuery(query(engine, noteTypeQuery("null")));
    assertQuery(query(engine, textQuery("abcd")));
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void nullDocDescription() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(3).docDescription(null).age(1.2).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("EMPTY")), 1);
    assertQuery(query(engine, noteTypeQuery("null")));
    assertQuery(query(engine, textQuery("abcd")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void emptyDocDescription() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(3).docDescription("").age(1.2).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("EMPTY")), 1);
    assertQuery(query(engine, noteTypeQuery("null")));
    assertQuery(query(engine, textQuery("abcd")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(null).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertError(query(engine, noteTypeQuery("EMPTY")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("null")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("abc")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, textQuery("abcd")), "TEXT information is missing in this dataset");
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void zeroAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(0d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertError(query(engine, noteTypeQuery("EMPTY")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("null")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("abc")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, textQuery("abcd")), "TEXT information is missing in this dataset");
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void nullYear() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(1.2).year(null));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("EMPTY")));
    assertQuery(query(engine, noteTypeQuery("null")));
    assertQuery(query(engine, noteTypeQuery("abc")));
    assertQuery(query(engine, noteTypeQuery("description")), 1);
    assertQuery(query(engine, textQuery("abcd")), 1);
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void commonWords() throws Exception {
    CommonWordsFilter.reset();
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(1.2).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "the"));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 2).age(2).duration(2).srcVisit("INPATIENT").code("12345").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    CommonWordsFilter.reset();
    assertError(query(engine, noteTypeQuery("EMPTY")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("null")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, noteTypeQuery("abc")), "NOTE TYPE information is missing in this dataset");
    assertError(query(engine, textQuery("the")), "TEXT information is missing in this dataset");
    assertQuery(query(engine, cptQuery("12345")), 2);
  }

  @Test
  public void interleavedEmptyAndNonEmptyNotes() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(3).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(4).docDescription("description2").age(2d).year(null));
    source.addNote(MockNoteRecord.create(5).docDescription("description3").age(3d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5));
    source.addTermMention(MockTermRecord.create(1).noteId(4).termId(6));
    source.addTermMention(MockTermRecord.create(1).noteId(5).termId(7));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(6, "xyz"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(7, "zyx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("description1")), 1);
    assertQuery(query(engine, noteTypeQuery("description2")), 1);
    assertQuery(query(engine, noteTypeQuery("description3")), 1);
    assertQuery(query(engine, textQuery("abcd")), 1);
    assertQuery(query(engine, textQuery("xyz")), 1);
    assertQuery(query(engine, textQuery("zyx")), 1);
    assertQuery(query(engine, identicalQuery(textQuery("abcd"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("xyz"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("zyx"), intervalQuery(daysToMinutes(3), daysToMinutes(3)))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("description1"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("description2"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("description3"), intervalQuery(daysToMinutes(3), daysToMinutes(3)))), 1);
  }

  @Test
  public void negatedNote() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(1d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5).negated());
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcde"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, textNegatedQuery("abcde")), 1);
    assertError(query(engine, textQuery("abcde")), "TEXT information is missing in this dataset");
    assertError(query(engine, textFhQuery("abcde")), "~TEXT information is missing in this dataset");
    assertQuery(query(engine, identicalQuery(textNegatedQuery("abcde"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
  }

  @Test
  public void familyHistoryNote() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(1d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5).familyHistory());
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcde"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, textFhQuery("abcde")), 1);
    assertError(query(engine, textQuery("abcde")), "TEXT information is missing in this dataset");
    assertError(query(engine, textNegatedQuery("abcde")), "!TEXT information is missing in this dataset");
    assertQuery(query(engine, identicalQuery(textFhQuery("abcde"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
  }

  @Test
  public void negatedAndFamilyHistoryNote() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(3).docDescription("description").age(1d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(5).familyHistory().negated());
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcde"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, textFhQuery("abcde")), 1);
    assertError(query(engine, textQuery("abcde")), "TEXT information is missing in this dataset");
    assertQuery(query(engine, textNegatedQuery("abcde")), 1);
    assertQuery(query(engine, identicalQuery(textFhQuery("abcde"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(textNegatedQuery("abcde"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
  }

  @Test
  public void booleanAndCommandImplicit() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(1).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(2).docDescription("description2").age(2d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "abc"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "def"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "xyz"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "zyx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(noteQuery(textQuery("abc"), textFhQuery("def"), textNegatedQuery("xyz")), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("abc"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(textFhQuery("def"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(textNegatedQuery("xyz"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(textQuery("zyx"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, noteQuery(textQuery("abc"), textFhQuery("def"), textNegatedQuery("xyz"), textQuery("zyx"))));
    assertQuery(query(engine, noteQuery(textQuery("abc"), textQuery("zyx"))));
    assertQuery(query(engine, noteQuery(textFhQuery("def"), textQuery("zyx"))));
    assertQuery(query(engine, noteQuery(textNegatedQuery("xyz"), textQuery("zyx"))));
    assertQuery(query(engine, identicalQuery(textQuery("zyx"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
  }

  @Test
  public void booleanAndCommandExplicit() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(1).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(2).docDescription("description2").age(2d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aBc"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "dEf"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "XyZ"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "ZYx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(noteQuery(andQuery(textQuery("zyx"))), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(
        query(engine, identicalQuery(noteQuery(andQuery(textQuery("abc"), textFhQuery("def"), textNegatedQuery("xyz"))), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, noteQuery(andQuery(textQuery("abc"), textFhQuery("def"), textNegatedQuery("xyz"), textQuery("zyx")))));
    assertQuery(query(engine, noteQuery(andQuery(textQuery("abc"), textQuery("zyx")))));
    assertQuery(query(engine, noteQuery(andQuery(textFhQuery("def"), textQuery("zyx")))));
    assertQuery(query(engine, noteQuery(andQuery(textNegatedQuery("xyz"), textQuery("zyx")))));
  }

  @Test
  public void booleanOrCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(1).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(2).docDescription("description2").age(2d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aBc"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "dEf"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "XyZ"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "ZYx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(
        query(engine, identicalQuery(noteQuery(orQuery(textQuery("abc"), textFhQuery("def"), textNegatedQuery("xyz"))), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(noteQuery(orQuery(textQuery("abc"), textQuery("zyx"))), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(
        daysToMinutes(2), daysToMinutes(2))))), 1);
    assertQuery(query(engine, identicalQuery(noteQuery(orQuery(textFhQuery("def"), textQuery("zyx"))), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(
        daysToMinutes(2), daysToMinutes(2))))), 1);
    assertQuery(query(engine, identicalQuery(noteQuery(orQuery(textNegatedQuery("xyz"), textQuery("zyx"))), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)),
        intervalQuery(daysToMinutes(2), daysToMinutes(2))))), 1);
  }

  @Test
  public void booleanNotCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(1).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(2).docDescription("description2").age(2d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aBc"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "dEf"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "XyZ"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "ZYx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, noteQuery(notQuery(orQuery(textQuery("abc"), textQuery("zyx"))))));
    assertQuery(query(engine, identicalQuery(noteQuery(notQuery(textQuery("zyx"))), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(noteQuery(notQuery(textQuery("abc"))), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
    assertQuery(query(engine, identicalQuery(noteQuery(notQuery(textQuery("xxxx"))), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(daysToMinutes(2),
        daysToMinutes(2))))), 1);
    assertQuery(query(engine, noteQuery(notQuery(andQuery(textQuery("abc"), textQuery("zyx"))))), 1);
    assertQuery(query(engine, noteQuery(notQuery(andQuery(textQuery("xxxx"), textQuery("zyx"))))), 1);
  }

  @Test
  public void docDescriptionTime() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(1).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(2).docDescription("description2").age(2d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aBc"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "dEf"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "XyZ"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "ZYx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(noteTypeQuery("description1"), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
    assertQuery(query(engine, identicalQuery(noteTypeQuery("description2"), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 1);
  }

  @Test
  public void notesCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addNote(MockNoteRecord.create(1).docDescription("description1").age(1d).year(2012));
    source.addNote(MockNoteRecord.create(2).docDescription("description2").age(2d).year(2012));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(3).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aBc"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "dEf"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "XyZ"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "ZYx"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(notesQuery(), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(daysToMinutes(2), daysToMinutes(2))))), 1);
  }

}
