package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.nullQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.server.ClinicalSearchEngine;

public class AtlasFunctionalTestSuiteMacro {

  @Test
  public void recordStartEnd() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(1).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("INTERVAL(RECORD START, RECORD END)", unionQuery(cptQuery("20000"), cptQuery("10000")))), 1);
  }

  @Test
  public void historyOf() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(1).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("HISTORY OF(CPT=20000)", intervalQuery(daysToMinutes(2), daysToMinutes(3)))), 1);
    assertQuery(query(engine, identicalQuery("HISTORY OF(CPT=10000)", intervalQuery(daysToMinutes(1), daysToMinutes(3)))), 1);
  }

  @Test
  public void noHistoryOf() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(1).duration(1).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("NO HISTORY OF(INTERVAL(FIRST MENTION(CPT=20000), RECORD END))", intervalQuery(daysToMinutes(1), daysToMinutes(2) - 1))), 1);
    assertQuery(query(engine, identicalQuery("NO HISTORY OF(CPT=20000)", intervalQuery(daysToMinutes(1), daysToMinutes(2) - 1))), 1);
    assertQuery(query(engine, identicalQuery("NO HISTORY OF(CPT=10000)", nullQuery())), 1);
  }

  @Test
  public void contains() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(5).duration(5).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(3).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(6).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(9).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(9).duration(2).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(12).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("CONTAINS(CPT=20000*, CPT=10000)", nullQuery())), 1);
    assertQuery(query(engine, identicalQuery("CONTAINS(CPT=10000*, CPT=20000)", intervalQuery(daysToMinutes(5), daysToMinutes(10)))), 1);
    assertQuery(query(engine, identicalQuery("CONTAINS(CPT=10000, CPT=20000*)", intervalQuery(daysToMinutes(6), daysToMinutes(7)))), 1);
  }

  @Test
  public void containsOverlap() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(5).duration(5).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(2).duration(3).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(5).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(6).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(9).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(9).duration(2).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(12).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("CONTAINS(CPT=20000*, CPT=10000)", nullQuery())), 1);
    assertQuery(query(engine, identicalQuery("CONTAINS(CPT=10000, CPT=20000*)", nullQuery())), 1);
  }

  @Test
  public void neverHad() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1).death(5d));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(5).duration(5).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "NEVER HAD(CPT=10000)"), 2);
    assertQuery(query(engine, "NEVER HAD(CPT=20000)"), 1);
  }

  @Test
  public void output() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1).death(6d));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(5).duration(1).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(10).duration(1).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(3).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    PatientSearchResponse response = query(engine, "OUTPUT(CPT=10000)");
    Iterator<double[]> iterator = response.getPatientIds().iterator();
    double[] o = iterator.next();
    Assert.assertEquals(3, o.length);
    Assert.assertEquals(1, o[0], 0.00001);
    Assert.assertEquals(5, o[1], 0.00001);
    Assert.assertEquals(6, o[2], 0.00001);
    o = iterator.next();
    Assert.assertEquals(3, o.length);
    Assert.assertEquals(1, o[0], 0.00001);
    Assert.assertEquals(10, o[1], 0.00001);
    Assert.assertEquals(11, o[2], 0.00001);
    Assert.assertFalse(iterator.hasNext());
    response = query(engine, "OUTPUT(CPT=20000)");
    iterator = response.getPatientIds().iterator();
    o = iterator.next();
    Assert.assertEquals(3, o.length);
    Assert.assertEquals(2, o[0], 0.00001);
    Assert.assertEquals(1, o[1], 0.00001);
    Assert.assertEquals(2, o[2], 0.00001);
    o = iterator.next();
    Assert.assertEquals(3, o.length);
    Assert.assertEquals(2, o[0], 0.00001);
    Assert.assertEquals(3, o[1], 0.00001);
    Assert.assertEquals(4, o[2], 0.00001);
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void patientLevelQueries() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1).gender("MALE"));
    src.addDemographics(MockDemographicsRecord.create(2).gender("MALE"));
    src.addVisit(MockVisitRecord.create(CPT, 1).age(5).duration(5).code("10000").year(2012).srcVisit(null));
    src.addVisit(MockVisitRecord.create(CPT, 2).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "SAME(GENDER=\"MALE\", CPT=10000)"), 1);
    assertQuery(query(engine, "DIFF(GENDER=\"MALE\", CPT=10000)"), 2);
    assertQuery(query(engine, "MERGE(GENDER=\"MALE\", CPT=10000)"), 1, 2);
  }

}