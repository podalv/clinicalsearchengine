package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.allVitalsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.vitalsQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.MockVitalsRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalTestVitals {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(null).floMeasId(null).display("pulse").value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("pulse", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void zeroAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(0d).floMeasId(null).display("pulse").value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("pulse", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void nullDisplay() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display(null).value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("empty", "MIN", "MAX")), "VITALS information is missing in this dataset");
    assertError(query(engine, vitalsQuery("null", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void emptyDisplay() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display("").value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("empty", "MIN", "MAX")), "VITALS information is missing in this dataset");
    assertError(query(engine, vitalsQuery("null", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void nullMeasValue() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display("pulse").value(null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("pulse", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void zeroMeasValue() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display("pulse").value(0d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("pulse", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void negativeMeasValue() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display("pulse").value(-1.5));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery("pulse", "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void correctValues() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display("pulse").value(10d));
    source.addVitals(MockVitalsRecord.create(2).age(1.5).floMeasId(null).display("abc").value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, vitalsQuery("pulse", "10", "10")), 1);
    assertQuery(query(engine, vitalsQuery("abc", "10", "10")), 2);
    assertQuery(query(engine, identicalQuery(vitalsQuery("pulse", "10", "10"), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, identicalQuery(vitalsQuery("abc", "10", "10"), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 2);
  }

  @Test
  public void correctValuesAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(11.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(111.5).floMeasId(null).display("pulse").value(10d));
    source.addVitals(MockVitalsRecord.create(2).age(1111.5).floMeasId(null).display("abc").value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, "INTERSECT(VITALS)"), 1, 2);
    assertQuery(query(engine, vitalsQuery("pulse", "10", "10")), 1);
    assertQuery(query(engine, vitalsQuery("abc", "10", "10")), 2);
    assertQuery(query(engine, identicalQuery(vitalsQuery("pulse", "10", "10"), intervalQuery(daysToMinutes(111.5), daysToMinutes(111.5)))), 1);
    assertQuery(query(engine, identicalQuery(vitalsQuery("abc", "10", "10"), intervalQuery(daysToMinutes(1111.5), daysToMinutes(1111.5)))), 2);
  }

  @Test
  public void allVitals() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).floMeasId(null).display("pulse").value(10d));
    source.addVitals(MockVitalsRecord.create(1).age(2.5).floMeasId(null).display("abc").value(10d));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, allVitalsQuery()), 1);
    assertQuery(query(engine, identicalQuery(allVitalsQuery(), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)), intervalQuery(daysToMinutes(2.5), daysToMinutes(
        2.5))))), 1);
  }

}
