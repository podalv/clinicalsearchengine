package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.ethnicityQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalTestSuiteEthnicity {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void add(final Stride6TestDataSource source, final String ethnicity) {
    final int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId).gender("").ethnicity(ethnicity));
    source.addVisit(MockVisitRecord.create(ICD9, patientId).visitId(patientId).age(1.5).code("250.00").duration(0).year(2012).srcVisit("INPATIENT"));
  }

  private Stride6TestDataSource get(final String ethnicity) {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    add(source, ethnicity);
    return source.toAtlasSchema();
  }

  @Test
  public void nullEthnicity() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get(null), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, ethnicityQuery("EMPTY")), 1);
  }

  @Test
  public void emptyEthnicity() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get(""), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, ethnicityQuery("EMPTY")), 1);
  }

  @Test
  public void unrecognizedEthnicity() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get("xxxxXx"), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, ethnicityQuery("XXXXXX")), 1);
  }

  @Test
  public void multipleEthnicities() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    add(source, "xXx");
    add(source, "YyY");
    add(source, "yyy");
    add(source, "");
    add(source, null);

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, ethnicityQuery("xxx")), 1);
    assertQuery(query(engine, ethnicityQuery("YYY")), 2, 3);
    assertQuery(query(engine, ethnicityQuery("EMPTY")), 4, 5);
    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4, 5);
  }

}
