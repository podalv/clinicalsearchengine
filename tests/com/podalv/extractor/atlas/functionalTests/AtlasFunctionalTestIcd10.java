package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.equalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd10Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.AtlasMockDemographics;
import com.podalv.extractor.test.datastructures.AtlasMockEncounters;
import com.podalv.extractor.test.datastructures.AtlasTestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalTestIcd10 {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));

    source.addEncounter(AtlasMockEncounters.create(1).code("T33.821D").codeType("ICD10").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd10Query("T33.821D")), 1);
    assertQuery(query(engine, icd10Query("T33.821")), 1);
    assertQuery(query(engine, icd10Query("T33.82")), 1);
    assertQuery(query(engine, icd10Query("T33.8")), 1);
    assertQuery(query(engine, icd10Query("T33")), 1);
    assertQuery(query(engine, icd10Query("T33-T34")), 1);
    assertQuery(query(engine, icd10Query("S00-T88")), 1);
    assertQuery(query(engine, primaryQuery(icd10Query("T33.821D"))));
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1);
    assertQuery(query(engine, equalQuery(icd10Query("T33.821D"), intervalQuery(5784180, 5785620))), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }
}