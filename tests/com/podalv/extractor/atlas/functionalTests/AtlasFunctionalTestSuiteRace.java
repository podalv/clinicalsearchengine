package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.andQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertRaceHistograms;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertRaceHistogramsEmpty;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.genderQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.orQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.raceQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.memory.MemoryStatistics;

public class AtlasFunctionalTestSuiteRace {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    if (engine != null) {
      System.out.println("AVAIABLE MEMORY = " + MemoryStatistics.getAvailableMemory());
      FileUtils.close(engine);
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void addRace(final Stride6TestDataSource source, final String race) {
    final int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId).race(race).gender(""));
    source.addVisit(MockVisitRecord.create(CPT, patientId).visitId(patientId).age(2.1).code("45300").duration(0).year(2012).srcVisit("INPATIENT"));
  }

  private void addRace(final Stride6TestDataSource source, final String gender, final String race) {
    final int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId).race(race).gender(gender));
    source.addVisit(MockVisitRecord.create(CPT, patientId).visitId(patientId).age(2.1).code("45300").duration(0).year(2012).srcVisit("INPATIENT"));
  }

  private Stride6TestDataSource addRace(final String gender, final String race) {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    final int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId).race(race).gender(gender));
    source.addVisit(MockVisitRecord.create(CPT, patientId).visitId(patientId).age(2.1).code("45300").duration(0).year(2012).srcVisit("INPATIENT"));
    return source.toAtlasSchema();
  }

  @Test
  public void nullRace() throws Exception {
    engine = Stride6TestHarness.engine(addRace("male", null), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, raceQuery("EMPTY"));
    assertQuery(response, 1);
    assertRaceHistogramsEmpty(response, "asian", "black", "white");
    assertRaceHistograms(response, "empty", 1, 1);
  }

  @Test
  public void emptyRace() throws Exception {
    engine = Stride6TestHarness.engine(addRace("male", ""), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, raceQuery("EMPTY"));
    assertQuery(response, 1);
    assertRaceHistogramsEmpty(response, "asian", "black", "white");
    assertRaceHistograms(response, "empty", 1, 1);
  }

  @Test
  public void unrecognizedRace() throws Exception {
    engine = Stride6TestHarness.engine(addRace("male", "xxxxxx"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, raceQuery("XXXXXX"));
    assertQuery(response, 1);
    assertRaceHistogramsEmpty(response, "asian", "black", "white");
    assertRaceHistograms(response, "xxxxxx", 1, 1);
  }

  @Test
  public void asianRace() throws Exception {
    engine = Stride6TestHarness.engine(addRace("male", "aSiAn"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, raceQuery("asian"));
    assertQuery(response, 1);
    assertRaceHistogramsEmpty(response, "black", "white");
    assertRaceHistograms(response, "asian", 1, 1);
  }

  @Test
  public void whiteRace() throws Exception {
    engine = Stride6TestHarness.engine(addRace("male", "WhiTe"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, raceQuery("white"));
    assertQuery(response, 1);
    assertRaceHistogramsEmpty(response, "asian", "black");
    assertRaceHistograms(response, "white", 1, 1);
  }

  @Test
  public void blackRace() throws Exception {
    engine = Stride6TestHarness.engine(addRace("male", "bLaCK"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, raceQuery("black"));
    assertQuery(response, 1);
    assertRaceHistogramsEmpty(response, "asian", "white");
    assertRaceHistograms(response, "black", 1, 1);
  }

  @Test
  public void multipleRaces() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    addRace(source, "AsiAn");
    addRace(source, "wHiTe");
    addRace(source, "ASIAN");
    addRace(source, "XXxxXx");
    addRace(source, "bLack");
    addRace(source, "");
    addRace(source, null);

    engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    PatientSearchResponse response = query(engine, raceQuery("black"));
    assertRaceHistograms(response, "BLACK", 1, 1);
    assertRaceHistograms(response, "WHITE", 0, 1);
    assertRaceHistograms(response, "ASIAN", 0, 2);
    assertQuery(response, 5);
    response = query(engine, raceQuery("asian"));
    assertRaceHistograms(response, "ASIAN", 2, 2);
    assertRaceHistograms(response, "WHITE", 0, 1);
    assertRaceHistograms(response, "BLACK", 0, 1);
    assertQuery(response, 1, 3);
    response = query(engine, raceQuery("white"));
    assertRaceHistograms(response, "WHITE", 1, 1);
    assertRaceHistograms(response, "ASIAN", 0, 2);
    assertRaceHistograms(response, "BLACK", 0, 1);
    assertQuery(response, 2);
    response = query(engine, raceQuery("xxxxxx"));
    assertRaceHistograms(response, "XXXXXX", 1, 1);
    assertRaceHistograms(response, "ASIAN", 0, 2);
    assertRaceHistograms(response, "BLACK", 0, 1);
    assertRaceHistograms(response, "WHITE", 0, 1);
    assertQuery(response, 4);
    response = query(engine, raceQuery("EMPTY"));
    assertRaceHistograms(response, "EMPTY", 2, 2);
    assertRaceHistograms(response, "XXXXXX", 0, 1);
    assertRaceHistograms(response, "ASIAN", 0, 2);
    assertRaceHistograms(response, "BLACK", 0, 1);
    assertRaceHistograms(response, "WHITE", 0, 1);
    assertQuery(response, 6, 7);
    response = query(engine, timelineQuery());
    assertRaceHistograms(response, "EMPTY", 2, 2);
    assertRaceHistograms(response, "XXXXXX", 1, 1);
    assertRaceHistograms(response, "ASIAN", 2, 2);
    assertRaceHistograms(response, "BLACK", 1, 1);
    assertRaceHistograms(response, "WHITE", 1, 1);
    assertQuery(response, 1, 2, 3, 4, 5, 6, 7);
  }

  @Test
  public void booleanCommands() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    addRace(source, "female", "asian");
    addRace(source, "male", "wHiTe");
    addRace(source, "female", "white");
    addRace(source, "female", "ASIAN");
    addRace(source, "female", "XXxxXx");
    addRace(source, "male", "bLack");
    addRace(source, "male", "");
    addRace(source, "male", null);

    engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, andQuery(raceQuery("asian"))), 1, 4);
    assertQuery(query(engine, orQuery(raceQuery("black"))), 6);
    assertQuery(query(engine, notQuery(raceQuery("black"))), 1, 2, 3, 4, 5, 7, 8);
    assertQuery(query(engine, andQuery(raceQuery("asian"), genderQuery("male"))));
    assertQuery(query(engine, andQuery(raceQuery("white"), genderQuery("male"))), 2);
    assertQuery(query(engine, andQuery(raceQuery("white"), genderQuery("female"))), 3);
    assertQuery(query(engine, andQuery(raceQuery("white"), notQuery(genderQuery("female")))), 2);
  }

}