package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockLabsLpchRecord;
import com.podalv.extractor.test.datastructures.MockLabsShcRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalTestLabs {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(null).name("aBc").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(null).code("DeF").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertQuery(query(engine, timelineQuery()), 2, 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
  }

  @Test
  public void zeroAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(0d).name("aBc").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(0d).code("DeF").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertQuery(query(engine, timelineQuery()), 2, 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
  }

  @Test
  public void nullBaseName() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name(null).value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(1.2).code(null).value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertQuery(query(engine, timelineQuery()), 2, 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("EMPTY")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("null")), "LAB information is missing in this dataset");
  }

  @Test
  public void emptyBaseName() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(1.2).code("").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertQuery(query(engine, timelineQuery()), 2, 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("EMPTY")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("null")), "LAB information is missing in this dataset");
  }

  @Test
  public void spacesInBaseName() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("   ").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(1.2).code("   ").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("EMPTY")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("null")), "LAB information is missing in this dataset");
    assertQuery(query(engine, timelineQuery()), 2, 4);
  }

  @Test
  public void shcInvalidValueRefValueExists() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(10000000d, "0.1", "1.5", "negative"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, labsQuery("abc", "negative")), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("abc", "negative"), intervalQuery(daysToMinutes(1.2), daysToMinutes(1.2)))), 1);
    assertQuery(query(engine, identicalQuery(labsQuery("abc"), intervalQuery(daysToMinutes(1.2), daysToMinutes(1.2)))), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcInvalidValueRefValueNull() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(10000000d, "0.1", "1.5", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "no value")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcInvalidValueRefValueEmpty() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(10000000d, "0.1", "1.5", ""));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowNullRangeHighNullRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, null, null, "neg"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "neg")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowEmptyRangeHighEmptyRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "", "", "neg"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "neg")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowSpacesRangeHighSpacesRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "   ", " ", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowNullRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, null, "5.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowEmptyRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "", "5.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowSpacesRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "  ", "5.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighNullRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", null, "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighEmptyRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", "", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighSpacesRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", "  ", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcLowValueRangeLowValidRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "1.2", "2.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcInRangeValueRangeLowValidRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2d, "1.2", "2.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcHighValueRangeLowValidRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(3d, "1.2", "2.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1.56 until 5.68")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcHighValueRangeLowValidRangeHighValid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(3d, "0", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "EMPTY")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcLowValueRangeLowValidRangeHighValid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(0.5d, "1", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "EMPTY")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcInRangeLowValidRangeHighValid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(1.5d, "1", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "EMPTY")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void labValues() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(1.5d, "1", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, labsQuery("abc", 0.5, 1)));
    assertQuery(query(engine, labsQuery("abc", 1.55, 2)));
    assertQuery(query(engine, labsQuery("abc", 1.49, 1.51)), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void shcContradictingRanges() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(1.5d, "1", "2", ">3"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", ">3")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchInvalidValueRefValueNull() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(20141000000000d).range(null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1-2")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchInvalidValueRefValueEmpty() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(20141000000000d).range(""));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1-2")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchInvalidValueRefValueSpaces() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(20141000000000d).range("  "));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "1-2")));
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchInvalidValueRefValueUnparseable() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(20141000000000d).range("1.1.2-2.3.4"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueUnparseable() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(1.5).range("1.1.2-2.3.4"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueLessThan() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(2d).range("<5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("def").value(7d).range("<5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, labsQuery("def", "undefined")));
    assertQuery(query(engine, labsQuery("def", "low")));
    assertQuery(query(engine, labsQuery("def", "high")));
    assertQuery(query(engine, labsQuery("def", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueGreaterThan() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(7d).range(">5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("def").value(2d).range(">5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, labsQuery("def", "undefined")));
    assertQuery(query(engine, labsQuery("def", "low")));
    assertQuery(query(engine, labsQuery("def", "high")));
    assertQuery(query(engine, labsQuery("def", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueRange() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(2d).range("1 - 5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("def").value(0.5).range("1-5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("ghi").value(7d).range("1-5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, labsQuery("abc")), 1);
    assertQuery(query(engine, labsQuery("abc", "undefined")));
    assertQuery(query(engine, labsQuery("abc", "low")));
    assertQuery(query(engine, labsQuery("abc", "high")));
    assertQuery(query(engine, labsQuery("abc", "normal")));
    assertQuery(query(engine, labsQuery("def", "undefined")));
    assertQuery(query(engine, labsQuery("def", "low")));
    assertQuery(query(engine, labsQuery("def", "high")));
    assertQuery(query(engine, labsQuery("def", "normal")));
    assertQuery(query(engine, labsQuery("ghi", "undefined")));
    assertQuery(query(engine, labsQuery("ghi", "low")));
    assertQuery(query(engine, labsQuery("ghi", "high")));
    assertQuery(query(engine, labsQuery("ghi", "normal")));
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void invalidTimeInvalidValue() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addLabsShc(MockLabsShcRecord.create(1).age(0d).name("abc").value(100000000d, "0", "2", ">1"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(null).name("abc").value(100000000d, "0", "2", ">1"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(1.5).name("abc").value(10d, "0", "2", ">1"));

    source.addLabsLpch(MockLabsLpchRecord.create(2).age(0d).code("DeF").value(100000000d).range("1-2"));
    source.addLabsLpch(MockLabsLpchRecord.create(2).age(null).code("DeF").value(100000000d).range("1-2"));
    source.addLabsLpch(MockLabsLpchRecord.create(2).age(1.5).code("DeF").value(10d).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);

    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
  }

}
