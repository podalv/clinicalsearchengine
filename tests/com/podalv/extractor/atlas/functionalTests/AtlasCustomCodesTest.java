package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.rxQuery;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.AtlasMockCodeDictionary;
import com.podalv.extractor.test.datastructures.AtlasMockDemographics;
import com.podalv.extractor.test.datastructures.AtlasMockEncounters;
import com.podalv.extractor.test.datastructures.AtlasMockPrescriptions;
import com.podalv.extractor.test.datastructures.AtlasTestDataSource;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestResponse;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasCustomCodesTest {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
    UmlsDictionary.clearCache();
    UmlsDictionary.create();
  }

  private void responseContains(final AutosuggestResponse resp, final String expected) {
    boolean result = false;
    for (final String line : resp.getResponse()) {
      if (line.startsWith(expected)) {
        result = true;
        break;
      }
    }
    Assert.assertTrue(result);
  }

  @Test
  public void customCodes_icd9() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00").death("2010-01-01 20:00:00"));
    source.addCodeDictionary(AtlasMockCodeDictionary.create("ICD9", "250.50", "Custom 1"));
    source.addCodeDictionary(AtlasMockCodeDictionary.create("ICD9", "999.99", "Custom 2"));
    source.addCodeDictionary(AtlasMockCodeDictionary.create("CPT", "45000", "Custom 3"));
    source.addCodeDictionary(AtlasMockCodeDictionary.create("CPT", "99999", "Custom 4"));
    source.addCodeDictionary(AtlasMockCodeDictionary.create("RX", "161", "Custom 5"));
    source.addCodeDictionary(AtlasMockCodeDictionary.create("RX", "99999999", "Custom 6"));

    source.addEncounter(AtlasMockEncounters.create(1).code("250.50").codeType("ICD9").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00").primary());
    source.addEncounter(AtlasMockEncounters.create(1).code("999.99").codeType("ICD9").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00").primary());

    source.addEncounter(AtlasMockEncounters.create(1).code("45000").codeType("CPT").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00"));
    source.addEncounter(AtlasMockEncounters.create(1).code("99999").codeType("CPT").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00"));

    source.addPrescription(AtlasMockPrescriptions.create(1).code("161").codeType("RX").route("route1").status("status1").startTime("2012-01-01 10:00:00").endTime(
        "2012-01-01 10:00:00"));
    source.addPrescription(AtlasMockPrescriptions.create(1).code("99999999").codeType("RX").route("route1").status("status1").startTime("2012-01-01 10:00:00").endTime(
        "2012-01-01 10:00:00"));

    UmlsDictionary.clearCache();
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, primaryQuery(icd9Query("250.50"))), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("999.99"))), 1);
    assertQuery(query(engine, cptQuery("45000")), 1);
    assertQuery(query(engine, cptQuery("99999")), 1);
    assertQuery(query(engine, rxQuery(161)), 1);
    assertQuery(query(engine, rxQuery(99999999)), 1);

    responseContains(engine.getAutosuggest().search(new AutosuggestRequest("ICD9=250.5", 10)), "250.50=Custom 1 [");
    responseContains(engine.getAutosuggest().search(new AutosuggestRequest("ICD9=999.9", 10)), "999.99=Custom 2 [");

    responseContains(engine.getAutosuggest().search(new AutosuggestRequest("CPT=4500", 8)), "45000=Custom 3 [");
    responseContains(engine.getAutosuggest().search(new AutosuggestRequest("CPT=9999", 8)), "99999=Custom 4 [");

    responseContains(engine.getAutosuggest().search(new AutosuggestRequest("RX=16", 5)), "161=Custom 5 [");
    responseContains(engine.getAutosuggest().search(new AutosuggestRequest("RX=9999999", 10)), "99999999=Custom 6 [");

  }
}
