package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.anyRxQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.drugQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.equalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.routeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.rxQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.statusQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockRxRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalTestSuiteRx {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void addOtherTestPatients(final Stride6TestDataSource source) {
    int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId));
    source.addVisit(MockVisitRecord.create(ICD9, patientId).code("123.45").age(1).duration(0).srcVisit("INPATIENT").year(2012));
    patientId++;
    source.addDemographics(MockDemographicsRecord.create(patientId));
    source.addRx(MockRxRecord.create(patientId).rxCui(345).age(2.5).route("def").status("ghi").year(2013));
  }

  private void testNegative(final ClinicalSearchEngine engine) {
    assertQuery(query(engine, rxQuery(345)), 3);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, timelineQuery()), 2, 3);
  }

  private void testPositive(final ClinicalSearchEngine engine) {
    assertQuery(query(engine, rxQuery(345)), 3);
    assertQuery(query(engine, rxQuery(123)), 1);
    assertQuery(query(engine, icd9Query("123.45")), 2);
    assertQuery(query(engine, timelineQuery()), 1, 2, 3);
  }

  @Test
  public void nullRxNormCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(null).route("abc").status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testNegative(engine);
  }

  @Test
  public void zeroRxNormCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(0).route("abc").status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testNegative(engine);
  }

  @Test
  public void negativeRxNormCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(-1).route("abc").status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testNegative(engine);
  }

  @Test
  public void zeroAgeCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(0d).rxCui(123).route("abc").status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testNegative(engine);
  }

  @Test
  public void nullAgeCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(null).rxCui(123).route("abc").status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testNegative(engine);
  }

  @Test
  public void nullStatus() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status(null).year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testPositive(engine);
    assertQuery(query(engine, statusQuery(123, null)));
    assertQuery(query(engine, statusQuery(456, "ghi")));
  }

  @Test
  public void emptyStatus() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testPositive(engine);
    assertQuery(query(engine, statusQuery(123, null)));
    assertQuery(query(engine, statusQuery(456, "ghi")));
  }

  @Test
  public void emptyRoute() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("").status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testPositive(engine);
    assertQuery(query(engine, routeQuery(123, null)));
    assertQuery(query(engine, routeQuery(456, "def")));
  }

  @Test
  public void nullRoute() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route(null).status("def").year(2012));
    addOtherTestPatients(src);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    testPositive(engine);
    assertQuery(query(engine, routeQuery(123, null)));
    assertQuery(query(engine, routeQuery(456, "def")));
  }

  @Test
  public void duration() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(1).age(25.5).rxCui(123).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(1).age(40d).rxCui(123).route("abc").status("discontinued").year(2012));
    src.addRx(MockRxRecord.create(1).age(42d).rxCui(123).route("abc").status("else").year(2012));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(25.5), daysToMinutes(25.5)))), 1);
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(40), daysToMinutes(40)))), 1);
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(42), daysToMinutes(42)))), 1);
  }

  @Test
  public void identicalTimesIdenticalValues() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("continued").year(2012));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, routeQuery(123, "abc")), 1);
    assertQuery(query(engine, statusQuery(123, "continued")), 1);
    assertQuery(query(engine, identicalQuery(rxQuery(123), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)), intervalQuery(daysToMinutes(1.5), daysToMinutes(
        1.5))))), 1);
  }

  @Test
  public void identicalTimesDifferentRoute() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("def").status("continued").year(2012));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))));
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, routeQuery(123, "abc")), 1);
    assertQuery(query(engine, routeQuery(123, "def")), 1);
    assertQuery(query(engine, statusQuery(123, "continued")), 1);
    assertQuery(query(engine, identicalQuery(rxQuery(123), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)), intervalQuery(daysToMinutes(1.5), daysToMinutes(
        1.5))))));
  }

  @Test
  public void identicalTimesDifferentStatus() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("discontinued").year(2012));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))));
    assertQuery(query(engine, equalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, routeQuery(123, "abc")), 1);
    assertQuery(query(engine, statusQuery(123, "continued")), 1);
    assertQuery(query(engine, statusQuery(123, "discontinued")), 1);
    assertQuery(query(engine, identicalQuery(rxQuery(123), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)), intervalQuery(daysToMinutes(1.5), daysToMinutes(
        1.5))))));
  }

  @Test
  public void completeDrugQuery() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(2).age(2.5).rxCui(123).route("abc").status("discontinued").year(2012));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(rxQuery(123), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, identicalQuery(rxQuery(123), intervalQuery(daysToMinutes(2.5), daysToMinutes(2.5)))), 2);
    assertQuery(query(engine, routeQuery(123, "abc")), 1, 2);
    assertQuery(query(engine, statusQuery(123, "continued")), 1);
    assertQuery(query(engine, statusQuery(123, "discontinued")), 2);
    assertQuery(query(engine, drugQuery(123, "abc", "continued")), 1);
    assertQuery(query(engine, drugQuery(123, "abc", "discontinued")), 2);
  }

  @Test
  public void invalidTimeInvalidCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addRx(MockRxRecord.create(1).age(null).rxCui(null).route("abc").status("continued").year(2012));
    src.addRx(MockRxRecord.create(1).age(0d).rxCui(0).route("abc").status("discontinued").year(2012));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(null).route("abc").status("continued").year(null));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(0).route("abc").status("discontinued").year(0));
    src.addRx(MockRxRecord.create(1).age(1.5).rxCui(123).route("abc").status("discontinued").year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, rxQuery(123)), 1);
  }

  @Test
  public void anyRxNormCode() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addRx(MockRxRecord.create(1).age(1d).rxCui(123).route("abc").status("discontinued").year(2012));
    src.addRx(MockRxRecord.create(1).age(2d).rxCui(456).route("abc").status("discontinued").year(2012));
    src.addVisit(MockVisitRecord.create(ICD9, 2).age(3).duration(0).code("234.56").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toAtlasSchema(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, anyRxQuery()), 1);
    assertQuery(query(engine, identicalQuery(anyRxQuery(), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1)), intervalQuery(daysToMinutes(2), daysToMinutes(2))))), 1);
  }
}
