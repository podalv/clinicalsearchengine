package com.podalv.extractor.atlas.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.deathQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.drugQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.equalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.noteQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.noteTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.primaryQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.rxQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textFhQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textNegatedQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.textQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.visitTypeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.vitalsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.AtlasMockDemographics;
import com.podalv.extractor.test.datastructures.AtlasMockEncounters;
import com.podalv.extractor.test.datastructures.AtlasMockMeasurements;
import com.podalv.extractor.test.datastructures.AtlasMockNotes;
import com.podalv.extractor.test.datastructures.AtlasMockPrescriptions;
import com.podalv.extractor.test.datastructures.AtlasMockTermMentions;
import com.podalv.extractor.test.datastructures.AtlasTestDataSource;
import com.podalv.extractor.test.datastructures.MockTermDictionaryRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class AtlasFunctionalSmokeTests {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void icd9() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));

    source.addEncounter(AtlasMockEncounters.create(1).code("250.50").codeType("ICD9").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("250.50"))));
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1);
    assertQuery(query(engine, equalQuery(icd9Query("250.50"), intervalQuery(5784180, 5785620))), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void primary_icd9() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));

    source.addEncounter(AtlasMockEncounters.create(1).code("250.50").codeType("ICD9").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00").primary());

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, primaryQuery(icd9Query("250.50"))), 1);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1);
    assertQuery(query(engine, equalQuery(icd9Query("250.50"), intervalQuery(5784180, 5785620))), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void cpt() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));
    source.addDemographics(AtlasMockDemographics.create(2).birth("2001-01-01 15:00:00"));

    source.addEncounter(AtlasMockEncounters.create(1).code("250.50").codeType("ICD9").dept("department 1").visitType("INPATIENT").start("2012-01-01 10:00:00").end(
        "2012-01-02 10:00:00"));

    source.addEncounter(AtlasMockEncounters.create(2).code("25000").codeType("CPT").dept("department 1").visitType("PATIENT").start("2013-01-01 10:00:00").end(
        "2013-01-02 10:00:00"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, icd9Query("250.50")), 1);
    assertQuery(query(engine, cptQuery("25000")), 2);
    assertQuery(query(engine, yearQuery(2012, 2012)), 1);
    assertQuery(query(engine, yearQuery(2013, 2013)), 2);
    assertQuery(query(engine, visitTypeQuery("INPATIENT")), 1);
    assertQuery(query(engine, visitTypeQuery("PATIENT")), 2);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void rx() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));

    source.addPrescription(AtlasMockPrescriptions.create(1).code("1234").codeType("RX").route("route1").status("status1").startTime("2012-01-01 10:00:00").endTime(
        "2012-01-01 10:00:00"));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, rxQuery(1234)), 1);
    assertQuery(query(engine, drugQuery(1234, "route1", "status1")), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void vitals() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));

    source.addMeasurement(AtlasMockMeasurements.create(1).code("aaa").codeType("VITAL").computedValue("HIGH").value(1.5).date("2012-01-01 10:00:00"));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, vitalsQuery("AAA", "0.5", "2")), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void labs() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00"));

    source.addMeasurement(AtlasMockMeasurements.create(1).code("aaa").codeType("LAB").computedValue("HIGH").value(1.5).date("2012-01-01 10:00:00"));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, labsQuery("aaa", 0.5d, 2d)), 1);
    assertError(query(engine, deathQuery()), "DEATH information is missing in this dataset");
    assertQuery(query(engine, labsQuery("aaa")), 1);
    assertQuery(query(engine, labsQuery("aaa", "HIGH")), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void demo() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00").death("2010-01-01 20:00:00"));

    source.addMeasurement(AtlasMockMeasurements.create(1).code("aaa").codeType("LAB").computedValue("HIGH").value(1.5).date("2012-01-01 10:00:00"));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, deathQuery()), 1);
    assertQuery(query(engine, equalQuery(deathQuery(), intervalQuery(4733580, 4733580))), 1);
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void notes() throws Exception {
    final AtlasTestDataSource source = new AtlasTestDataSource();
    source.addDemographics(AtlasMockDemographics.create(1).birth("2001-01-01 15:00:00").death("2010-01-01 20:00:00"));

    source.addNote(AtlasMockNotes.create(1, 5).date("2001-02-02 15:00:00").noteType("aaaaa"));
    source.addNote(AtlasMockNotes.create(1, 8).date("2001-02-02 18:00:00").noteType("bbbbb"));
    source.addTermMention(AtlasMockTermMentions.create(1, 5).tid(2).negated(0).familyHistory(0));
    source.addTermMention(AtlasMockTermMentions.create(1, 5).tid(3).negated(0).familyHistory(0));
    source.addTermMention(AtlasMockTermMentions.create(1, 5).tid(4).negated(1).familyHistory(0));
    source.addTermMention(AtlasMockTermMentions.create(1, 5).tid(5).negated(1).familyHistory(1));
    source.addTermMention(AtlasMockTermMentions.create(1, 5).tid(6).negated(0).familyHistory(1));
    source.addTermMention(AtlasMockTermMentions.create(1, 8).tid(7).negated(0).familyHistory(0));

    source.addTermDictionary(MockTermDictionaryRecord.create(2, "abc"));
    source.addTermDictionary(MockTermDictionaryRecord.create(3, "def"));
    source.addTermDictionary(MockTermDictionaryRecord.create(4, "ghi"));
    source.addTermDictionary(MockTermDictionaryRecord.create(5, "jkl"));
    source.addTermDictionary(MockTermDictionaryRecord.create(6, "mno"));
    source.addTermDictionary(MockTermDictionaryRecord.create(7, "pqr"));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, noteTypeQuery("aaaaa")), 1);
    assertQuery(query(engine, noteTypeQuery("bbbbb")), 1);
    assertQuery(query(engine, textQuery("abc")), 1);
    assertQuery(query(engine, textQuery("def")), 1);
    assertQuery(query(engine, textQuery("pqr")), 1);
    assertQuery(query(engine, textNegatedQuery("ghi")), 1);
    assertQuery(query(engine, textNegatedQuery("jkl")), 1);
    assertQuery(query(engine, textFhQuery("jkl")), 1);
    assertQuery(query(engine, textFhQuery("mno")), 1);
    assertQuery(query(engine, noteQuery(textQuery("abc"), textQuery("def"))), 1);
    assertQuery(query(engine, noteQuery(textQuery("abc"), textQuery("pqr"))));
    assertQuery(query(engine, timelineQuery()), 1);
  }

}