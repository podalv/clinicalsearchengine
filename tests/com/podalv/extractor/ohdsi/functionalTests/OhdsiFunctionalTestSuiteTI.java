package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.countQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.durationQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.endQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.firstMentionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intersectQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.invertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.lastMentionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.nullQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.resizeQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.startQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class OhdsiFunctionalTestSuiteTI {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void startCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(2.5).code("43500").duration(9).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(3.5).code("43500").duration(9).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, startQuery(icd9Query("43500"))), 1, 2);
    assertQuery(query(engine, identicalQuery(startQuery(icd9Query("43500")), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, identicalQuery(startQuery(icd9Query("43500")), unionQuery(intervalQuery(daysToMinutes(2.5), daysToMinutes(2.5)), intervalQuery(daysToMinutes(3.5),
        daysToMinutes(3.5))))), 2);
  }

  @Test
  public void endCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(2.5).code("43500").duration(9).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(3.5).code("43500").duration(9).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, endQuery(icd9Query("43500"))), 1, 2);
    assertQuery(query(engine, identicalQuery(endQuery(icd9Query("43500")), intervalQuery(daysToMinutes(2.5), daysToMinutes(2.5)))), 1);
    assertQuery(query(engine, identicalQuery(endQuery(icd9Query("43500")), unionQuery(intervalQuery(daysToMinutes(11.5), daysToMinutes(11.5)), intervalQuery(daysToMinutes(12.5),
        daysToMinutes(12.5))))), 2);
  }

  @Test
  public void firstMentionCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(2.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(2.5).code("45000").duration(9).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, firstMentionQuery(icd9Query("43500"))), 1);
    assertQuery(query(engine, identicalQuery(firstMentionQuery(icd9Query("43500")), intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)))), 1);
    assertQuery(query(engine, identicalQuery(firstMentionQuery(icd9Query("45000")), unionQuery(intervalQuery(daysToMinutes(2.5), daysToMinutes(11.5))))), 2);
  }

  @Test
  public void lastMentionCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(2.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(2.5).code("45000").duration(9).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, lastMentionQuery(icd9Query("43500"))), 1);
    assertQuery(query(engine, identicalQuery(lastMentionQuery(icd9Query("43500")), intervalQuery(daysToMinutes(5.5), daysToMinutes(6.5)))), 1);
    assertQuery(query(engine, identicalQuery(lastMentionQuery(icd9Query("45000")), unionQuery(intervalQuery(daysToMinutes(2.5), daysToMinutes(11.5))))), 2);
  }

  @Test
  public void resizeCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(2.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("46000").duration(20).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("43500"), "0 minutes", "0 minutes"), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)),
        intervalQuery(daysToMinutes(2.5), daysToMinutes(3.5))))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "100 days", "100 days"), intervalQuery(daysToMinutes(21.5), daysToMinutes(21.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "2 days", "2 days"), intervalQuery(daysToMinutes(7.5), daysToMinutes(17.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "-2 days", "-2 days"), intervalQuery(daysToMinutes(3.5), daysToMinutes(13.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "-2 days", "2 days"), intervalQuery(daysToMinutes(3.5), daysToMinutes(17.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "-100 days", "100 days"), intervalQuery(daysToMinutes(1.5), daysToMinutes(21.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "-100 days", "-100 days"), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("43500"), "0 day", "1 day"), intervalQuery(daysToMinutes(1.5), daysToMinutes(4.5)))), 1);
  }

  @Test
  public void resizeStartEndParameters() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(2.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("46000").duration(20).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("43500"), "END", "END + 2 days"), intervalQuery(daysToMinutes(3.5), daysToMinutes(5.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "START - 1 day", "START + 1 day"), intervalQuery(daysToMinutes(4.5), daysToMinutes(6.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "END - 1 day", "END + 1 day"), intervalQuery(daysToMinutes(14.5), daysToMinutes(16.5)))), 1);
    assertQuery(query(engine, identicalQuery(resizeQuery(icd9Query("45000"), "START - 1 day", "END + 1 day"), intervalQuery(daysToMinutes(4.5), daysToMinutes(16.5)))), 1);
  }

  @Test
  public void invertCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(2.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1).code("46000").duration(20).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(invertQuery(icd9Query("43500")), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(1.5) - 1), intervalQuery(
        daysToMinutes(3.5) + 1, daysToMinutes(21))))), 1);
    assertQuery(query(engine, identicalQuery(invertQuery(icd9Query("45000")), unionQuery(intervalQuery(daysToMinutes(1), daysToMinutes(5.5) - 1), intervalQuery(
        daysToMinutes(15.5) + 1, daysToMinutes(21))))), 1);
    assertQuery(query(engine, identicalQuery(invertQuery(icd9Query("46000")), nullQuery())), 1);
  }

  @Test
  public void countCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(2.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1).code("46000").duration(20).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, countQuery(icd9Query("43500"), "0", "2")));
    assertQuery(query(engine, countQuery(icd9Query("43500"), "3", "MAX")), 1);
    assertQuery(query(engine, countQuery(icd9Query("43500"), "4", "MAX")));
    assertQuery(query(engine, countQuery(icd9Query("43500"), "MIN", "3")), 1);
  }

  @Test
  public void countComplexCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1).code("46000").duration(20).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(countQuery(icd9Query("43500"), icd9Query("45000"), "ALL", "0", "1"), intervalQuery(daysToMinutes(5.5), daysToMinutes(6.5)))), 1);
    assertQuery(query(engine, identicalQuery(countQuery(icd9Query("43500"), "0", "3"), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)), intervalQuery(
        daysToMinutes(3.5), daysToMinutes(4.5)), intervalQuery(daysToMinutes(5.5), daysToMinutes(6.5))))), 1);
  }

  @Test
  public void countComplexCommandParameters() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(25.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(20).code("45000").duration(10).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(countQuery(icd9Query("43500"), icd9Query("45000"), "ALL", "0", "4"), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)),
        intervalQuery(daysToMinutes(3.5), daysToMinutes(4.5)), intervalQuery(daysToMinutes(5.5), daysToMinutes(6.5)), intervalQuery(daysToMinutes(25.5), daysToMinutes(26.5))))), 1);
    assertQuery(query(engine, countQuery(icd9Query("43500"), icd9Query("45000"), "SINGLE", "4", "MAX")));
  }

  @Test
  public void intersectCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1).code("45000").duration(10).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(10).code("46000").duration(10).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(intersectQuery(icd9Query("43500"), icd9Query("45000")), icd9Query("43500"))), 1);
    assertQuery(query(engine, intersectQuery(icd9Query("43500"), icd9Query("46000"))));
  }

  @Test
  public void durationCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(2).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1).code("45000").duration(10).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, durationQuery(icd9Query("43500"), "SINGLE", "MIN", "2 days")), 1);
    assertQuery(query(engine, durationQuery(icd9Query("43500"), "SINGLE", "0", "1 days")));
    assertQuery(query(engine, durationQuery(icd9Query("43500"), "SINGLE", "0", "2 days")), 1);
    assertQuery(query(engine, durationQuery(icd9Query("45000"), "SINGLE", "10 days", "MAX")), 1);
  }

  @Test
  public void durationComplexCommand() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(5.5).code("43500").duration(2).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(1.5).code("45000").duration(10).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, identicalQuery(durationQuery(icd9Query("43500"), icd9Query("45000"), "SINGLE", "2 days", "MAX"), unionQuery(intervalQuery(daysToMinutes(5.5),
        daysToMinutes(7.5))))), 1);
    assertQuery(query(engine, identicalQuery(durationQuery(icd9Query("43500"), icd9Query("45000"), "SINGLE", "0", "1 day"), unionQuery(intervalQuery(daysToMinutes(1.5),
        daysToMinutes(2.5)), intervalQuery(daysToMinutes(3.5), daysToMinutes(4.5))))), 1);
    assertQuery(query(engine, durationQuery(icd9Query("43500"), icd9Query("45000"), "ALL", "0", "2 day")));
    assertQuery(query(engine, durationQuery(icd9Query("43500"), icd9Query("45000"), "SINGLE", "3 days", "MAX")));
    assertQuery(query(engine, durationQuery(icd9Query("43500"), icd9Query("45000"), "SINGLE", "0", "1 day")), 1);
    assertQuery(query(engine, durationQuery(icd9Query("43500"), icd9Query("45000"), "ALL", "0", "5 day")), 1);

  }
}