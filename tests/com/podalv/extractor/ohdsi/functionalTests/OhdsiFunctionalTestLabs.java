package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.allVitalsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.labsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.vitalsQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockLabsLpchRecord;
import com.podalv.extractor.test.datastructures.MockLabsShcRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class OhdsiFunctionalTestLabs {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(null).name("aBc").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(null).code("DeF").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertQuery(query(engine, timelineQuery()), 2, 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
  }

  @Test
  public void emptyBaseName() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(1.2).code("").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertQuery(query(engine, timelineQuery()), 2, 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("EMPTY")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("null")), "LAB information is missing in this dataset");
  }

  @Test
  public void spacesInBaseName() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("   ").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(1.2).code("   ").value(1.5).range("1-2"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, cptQuery("45000")), 4);
    assertError(query(engine, labsQuery("abc")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("def")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("EMPTY")), "LAB information is missing in this dataset");
    assertError(query(engine, labsQuery("null")), "LAB information is missing in this dataset");
    assertQuery(query(engine, timelineQuery()), 2, 4);
  }

  @Test
  public void shcInvalidValueRefValueExists() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(10000000d, "0.1", "1.5", "negative"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void shcInvalidValueRefValueSpaces() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(10000000d, "0.1", "1.5", "   "));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
  }

  @Test
  public void shcValidValueRangeLowNullRangeHighNullRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, null, null, "neg"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowEmptyRangeHighEmptyRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "", "", "neg"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowSpacesRangeHighSpacesRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "   ", " ", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowNullRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, null, "5.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowEmptyRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "", "5.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowSpacesRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "  ", "5.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighNullRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", null, "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighEmptyRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", "", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighSpacesRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", "  ", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcLowValueRangeLowValidRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "1.2", "2.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcInRangeValueRangeLowValidRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2d, "1.2", "2.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2", "2")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcHighValueRangeLowValidRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(3d, "1.2", "2.5", "1.56 until 5.68"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "3", "3")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcHighValueRangeLowValidRangeHighValid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(3d, "0", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "3", "3")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcLowValueRangeLowValidRangeHighValid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(0.5d, "1", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "0.5", "0.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcInRangeLowValidRangeHighValid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(1.5d, "1", "2", null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "1.5", "1.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueLessThan() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(2d).range("<5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("def").value(7d).range("<5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2", "2")), 1);
    assertQuery(query(engine, vitalsQuery("def", "7", "7")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueGreaterThan() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(7d).range(">5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("def").value(2d).range(">5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "7", "7")), 1);
    assertQuery(query(engine, vitalsQuery("def", "2", "2")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void lpchValidValueRefValueRange() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("abc").value(2d).range("1 - 5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("def").value(0.5).range("1-5"));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(1.2).code("ghi").value(7d).range("1-5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2", "2")), 1);
    assertQuery(query(engine, vitalsQuery("def", "0.5", "0.5")), 1);
    assertQuery(query(engine, vitalsQuery("ghi", "7", "7")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowValidRangeHighUnparseableRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "0.1", "1.2.3", "5.56 until 6.85"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void shcValidValueRangeLowUnparseableRangeHighValidRefNormValsInvalid() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(2.5, "1.2.3", "0.1", "5.56 until 6.85"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, vitalsQuery("abc", "2.5", "2.5")), 1);
    assertQuery(query(engine, timelineQuery()), 1, 2);
  }

  @Test
  public void allLabs() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(1.2).name("abc").value(1.5, "1.2.3", "2.3.4", "1-5"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(2.2).name("def").value(1.5, "1.2.3", "2.3.4", "1-5"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);

    assertQuery(query(engine, allVitalsQuery()), 1);
    assertQuery(query(engine, identicalQuery(allVitalsQuery(), unionQuery(intervalQuery(daysToMinutes(1.2), daysToMinutes(1.2)), intervalQuery(daysToMinutes(2.2), daysToMinutes(
        2.2))))), 1);
  }

}
