package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.cptQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.vitalsQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.MockVitalsVisitsRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class OhdsiFunctionalTestVitalsVisits {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void nullAge() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsVisitsRecord.create(1).age(null).bmi(1.5));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 2);
    assertError(query(engine, vitalsQuery(Common.VITALS_BMI, "MIN", "MAX")), "VITALS information is missing in this dataset");
  }

  @Test
  public void zeroValues() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));

    source.addVitals(MockVitalsVisitsRecord.create(1).age(1.5).bsa(2.6));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(2.5).bmi(50.5));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(3.5).resp(50));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(5.5).weight(150.4));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(6.5).pulse(120));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(7.5).temp(12.5));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(8.5).bp(0, 200));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(9.5).bp(120, 0));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, cptQuery("43500")), 2);
    assertQuery(query(engine, timelineQuery()), 1, 2);
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_BMI, "50.5", "50.5"), intervalQuery(daysToMinutes(2.5), daysToMinutes(2.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BMI, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_BSA, "2.6", "2.6"), intervalQuery(daysToMinutes(1.5), daysToMinutes(1.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BSA, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_RESP, "50", "50"), intervalQuery(daysToMinutes(3.5), daysToMinutes(3.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_RESP, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_WEIGHT, "150.4", "150.4"), intervalQuery(daysToMinutes(5.5), daysToMinutes(5.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_WEIGHT, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_PULSE, "120", "120"), intervalQuery(daysToMinutes(6.5), daysToMinutes(6.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_PULSE, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_TEMPERATURE, "12.5", "12.5"), intervalQuery(daysToMinutes(7.5), daysToMinutes(7.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_TEMPERATURE, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_BP_S, "120", "120"), intervalQuery(daysToMinutes(9.5), daysToMinutes(9.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_S, "0", "0")));
    assertQuery(query(engine, identicalQuery(vitalsQuery(Common.VITALS_BP_D, "200", "200"), intervalQuery(daysToMinutes(8.5), daysToMinutes(8.5)))), 1);
    assertQuery(query(engine, vitalsQuery(Common.VITALS_BP_D, "0", "0")));
  }

}
