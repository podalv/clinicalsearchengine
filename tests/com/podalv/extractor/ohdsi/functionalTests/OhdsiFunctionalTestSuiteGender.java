package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertGenderHistograms;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.genderQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class OhdsiFunctionalTestSuiteGender {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void add(final Stride6TestDataSource source, final String gender) {
    final int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId).gender(gender));
    source.addVisit(MockVisitRecord.create(ICD9, patientId).visitId(patientId).age(1.5).code("250.00").duration(0).year(2012).srcVisit("INPATIENT"));
  }

  private Stride6TestDataSource get(final String gender) {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    add(source, gender);
    return source.toOhdsi();
  }

  @Test
  public void unrecognizedGender() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get("xxxxxx"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, genderQuery("xxxxxx"));
    assertQuery(response, 1);
    assertGenderHistograms(response, 0, 0, 0, 0);
  }

  @Test
  public void emptyGender() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get(""), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, genderQuery("EMPTY"));
    assertQuery(response, 1);
    assertGenderHistograms(response, 0, 0, 0, 0);
  }

  @Test
  public void maleGender() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get("MaLe"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, genderQuery("MALE"));
    assertQuery(response, 1);
    assertGenderHistograms(response, 1, 0, 1, 0);
  }

  @Test
  public void femaleGender() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get("fEmAlE"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, genderQuery("FEMALE"));
    assertQuery(response, 1);
    assertGenderHistograms(response, 0, 1, 0, 1);
  }

  @Test
  public void timeIntervals() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(get("fEmAlE"), DATA_FOLDER, DATA_FOLDER);
    final PatientSearchResponse response = query(engine, "IDENTICAL(" + genderQuery("FEMALE") + ", INTERVAL(2160, 2160))");
    assertQuery(response, 1);
    assertGenderHistograms(response, 0, 1, 0, 1);
  }

}