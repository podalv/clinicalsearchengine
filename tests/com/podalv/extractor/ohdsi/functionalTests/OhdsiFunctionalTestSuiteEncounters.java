package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.ageQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.encountersQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.patientsQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.unionQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.yearQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockLabsLpchRecord;
import com.podalv.extractor.test.datastructures.MockLabsShcRecord;
import com.podalv.extractor.test.datastructures.MockNoteRecord;
import com.podalv.extractor.test.datastructures.MockRxRecord;
import com.podalv.extractor.test.datastructures.MockTermDictionaryRecord;
import com.podalv.extractor.test.datastructures.MockTermRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE;
import com.podalv.extractor.test.datastructures.MockVitalsRecord;
import com.podalv.extractor.test.datastructures.MockVitalsVisitsRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class OhdsiFunctionalTestSuiteEncounters {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void singleTimePoint() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addLabsLpch(MockLabsLpchRecord.create(1).age(6.5).code("def").value(1.5).range("1-2"));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(1.5).code("43500").duration(0d).year(2012));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.ICD9, 1).age(12.5).code("250.50").duration(0d).year(2013));
    src.addRx(MockRxRecord.create(1).age(15.5).route("def").status("discontinued").year(2014).rxCui(1234));
    src.addNote(MockNoteRecord.create(3).docDescription("description1").age(17.5).year(2015));
    src.addTermMention(MockTermRecord.create(1).termId(5).noteId(3));
    src.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "abcd"));
    src.addLabsShc(MockLabsShcRecord.create(1).age(5.5).name("aBc").value(1.1, "0.1", "1.5", "<1.1"));
    src.addVitals(MockVitalsRecord.create(1).age(8.5).floMeasId(null).display("pulse").value(100d));
    src.addVitals(MockVitalsVisitsRecord.create(1).age(17.5).bsa(2.65));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(encountersQuery(), unionQuery(intervalQuery(daysToMinutes(1.5), daysToMinutes(2.5)), intervalQuery(daysToMinutes(12.5),
        daysToMinutes(13.5))))), 1);
    assertQuery(query(engine, identicalQuery(yearQuery(1950, 1950), intervalQuery(daysToMinutes(1.5), daysToMinutes(45.5)))), 1);
  }

  @Test
  public void overlap() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(1.5).code("43500").duration(0d).year(2013));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.ICD9, 1).age(1.75).code("250.50").duration(2d).year(2013));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(encountersQuery(), unionQuery(intervalQuery(2160, 3600), intervalQuery(3601, 5041), intervalQuery(5042, 5400)))), 1);
    assertQuery(query(engine, identicalQuery("NULL", intervalQuery(0, 0))));
  }

  @Test
  public void multiplePatients() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addDemographics(MockDemographicsRecord.create(3));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(1.5).code("43500").duration(3d).year(2012));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.ICD9, 2).age(1.75).code("250.50").duration(2d).year(2012));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 3).age(2.5).code("45500").duration(4d).year(2012));
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, patientsQuery(1)), 1);
    assertQuery(query(engine, patientsQuery(2)), 2);
    assertQuery(query(engine, patientsQuery(3)), 3);
    assertQuery(query(engine, patientsQuery(1, 2, 3)), 1, 2, 3);
    assertQuery(query(engine, patientsQuery(1, 3)), 1, 3);
  }

  @Test
  public void ages() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addDemographics(MockDemographicsRecord.create(2));
    src.addDemographics(MockDemographicsRecord.create(3));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(1).code("43500").duration(0d).year(2012));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(2).code("43500").duration(0d).year(2012));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(3).code("43500").duration(0d).year(2012));
    src.addVisit(MockVisitRecord.create(VISIT_TYPE.CPT, 1).age(4).code("43500").duration(0d).year(2012));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, ageQuery(daysToMinutes(6), daysToMinutes(6))));
    assertQuery(query(engine, ageQuery(daysToMinutes(5), daysToMinutes(5))));
    assertQuery(query(engine, ageQuery(daysToMinutes(1), daysToMinutes(1))), 1);
    assertQuery(query(engine, ageQuery(daysToMinutes(2), daysToMinutes(2))), 1);
    assertQuery(query(engine, ageQuery(daysToMinutes(3), daysToMinutes(3))), 1);
    assertQuery(query(engine, ageQuery(daysToMinutes(4), daysToMinutes(4))), 1);
    assertQuery(query(engine, ageQuery(daysToMinutes(2), daysToMinutes(5))), 1);
    assertQuery(query(engine, ageQuery(daysToMinutes(1), daysToMinutes(5))), 1);
    assertQuery(query(engine, ageQuery(daysToMinutes(0), daysToMinutes(0))));
  }

}