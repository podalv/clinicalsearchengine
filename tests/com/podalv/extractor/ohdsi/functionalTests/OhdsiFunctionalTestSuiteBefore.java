package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.icd9Query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;

public class OhdsiFunctionalTestSuiteBefore {

  @Test
  public void identicalIntervals_12_1() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)"));
  }

  @Test
  public void identicalIntervalsWithZeroRange_12_2() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(0, 0)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)+(0, 0)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)+(0, 0)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)+(0, 0)", icd9Query("20000"))), 1);
  }

  @Test
  public void identicalIntervalsWithZeroRange_12_3() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-(0, 0)"));
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-(0, 0)"));
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-(0, 0)"));
  }

  @Test
  public void identicalIntervalsWithZeroRange_12_4() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(0, 0)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(0, 0)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-(0, 0)"));
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-(0, 0)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+>(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+>(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+>(0, 0)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(0, 0)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(0, 0)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<(0, 0)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<(0, 0)"), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-<(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-<(0, 0)"));
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-<(0, 0)"));
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-<(0, 0)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)->(0, 0)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)->(0, 0)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)->(0, 0)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)->(0, 0)"), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<>(0, 0)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<>(0, 0)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-<>(0, 0)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-<>(0, 0)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-<>(0, 0)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-<>(0, 0)"), 1);
  }

  @Test
  public void identicalIntervalsRangeAfterIncludingEnd_12_5() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(1 days, 2 days)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(1 days, 2 days)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-(1 days, 2 days)"));
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-(1 days, 2 days)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+>(1 days, 2 days)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)+>(1 days, 2 days)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)+>(1 days, 2 days)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)+>(1 days, 2 days)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<(1 days, 2 days)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)->(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)->(1 days, 2 days)"));
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)->(1 days, 2 days)"));
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)->(1 days, 2 days)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(1 days, 2 days)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<(1 days, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<(1 days, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<(1 days, 2 days)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(1 days, 2 days)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<>(1 days, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<>(1 days, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<>(1 days, 2 days)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<>(1 days, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<>(1 days, 2 days)"));
  }

  @Test
  public void identicalIntervalsRangeAfter_12_6() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(1441, 2 days)"));
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-(1441, 2 days)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-(1441, 2 days)"), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+>(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+>(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+>(1441, 2 days)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<(1441, 2 days)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)->(1441, 2 days)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)->(1441, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)->(1441, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)->(1441, 2 days)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(1441, 2 days)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<(1441, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<(1441, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<(1441, 2 days)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(1441, 2 days)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<>(1441, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<>(1441, 2 days)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<>(1441, 2 days)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<>(1441, 2 days)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<>(1441, 2 days)"));
  }

  @Test
  public void identicalIntervalsRangeBefore_12_7() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(-1 day, -1)"));
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-(-1 day, -1)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-(-1 day, -1)"), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+>(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+>(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+>(-1 day, -1)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<(-1 day, -1)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)->(-1 day, -1)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)->(-1 day, -1)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)->(-1 day, -1)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)->(-1 day, -1)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(-1 day, -1)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<(-1 day, -1)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<(-1 day, -1)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<(-1 day, -1)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(-1 day, -1)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<>(-1 day, -1)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<>(-1 day, -1)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<>(-1 day, -1)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<>(-1 day, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<>(-1 day, -1)"));
  }

  @Test
  public void identicalIntervalsRangeWithin_12_8() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(1, 1439)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(1, 1439)"), 1);
    //assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)-(1, 1439)"));
    //assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)-(1, 1439)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+>(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+>(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+>(1, 1439)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<(1, 1439)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)->(1, 1439)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)->(1, 1439)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)->(1, 1439)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)->(1, 1439)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(1, 1439)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<(1, 1439)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<(1, 1439)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<(1, 1439)", icd9Query("20000"))), 1);

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(1, 1439)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<>(1, 1439)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)-<>(1, 1439)", icd9Query("10000"))), 1);
    //assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000*, ICD9=10000)-<>(1, 1439)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000*, ICD9=20000)+<>(1, 1439)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)+<>(1, 1439)"));
  }

  @Test
  public void beforeOverlapNoRange_12_9() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)", icd9Query("20000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)"));
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)", icd9Query("10000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)"));
  }

  @Test
  public void beforeOverlapRangeB_12_10() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(0, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+>(0, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(0, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(0, 1 day)"));

    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(0, 1 day)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+>(0, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(0, 1 day)"), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(0, 1 day)"));

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-(0, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)->(0, 1 day)"));
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(0, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(0, 1 day)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-(0, 1 day)"));
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)->(0, 1 day)", icd9Query("10000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-<(0, 1 day)"));
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<>(0, 1 day)", icd9Query("10000"))), 1);
  }

  @Test
  public void beforeOverlapBeforeA_12_11() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(-2 days, -1441)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(-2 days, -1441)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(-2 days, -1441)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(-2 days, -1441)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(-2 days, -1441)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(-2 days, -1441)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)->(-2 days, -1441)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(-2 days, -1441)", icd9Query("20000"))), 1);
  }

  @Test
  public void beforeOverlapAfterA_12_12() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+(1, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(1, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(1, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(1, 1 day)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(1, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(1, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)->(1, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(1, 1 day)", icd9Query("20000"))), 1);
  }

  @Test
  public void beforeShortDistance_12_13() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(3).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)", icd9Query("20000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)"));
  }

  @Test
  public void beforeShortDistanceRangeA_12_14() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(1).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(3).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(-2 days, -1)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(-2 days, -1)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(-2 days, -1)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(-2 days, -1)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-(-2 days, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)->(-2 days, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-<(-2 days, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-<>(-2 days, -1)"));

    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+(-2 days, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+>(-2 days, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<(-2 days, -1)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)+<>(-2 days, -1)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-(-2 days, -1)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)->(-2 days, -1)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<(-2 days, -1)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)-<>(-2 days, -1)", icd9Query("10000"))), 1);
  }

  @Test
  public void beforeBwithinA_12_15() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(3).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000*, ICD9=20000)", icd9Query("10000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000*, ICD9=10000)"));
  }

  @Test
  public void beforeBwithinArange_12_16() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(1).duration(3).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(2).duration(1).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)+(0, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+>(0, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<(0, 1 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)+<>(0, 1 day)"));

    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)+(0, 4 day)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)+>(0, 4 day)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)+<(0, 4 day)", icd9Query("10000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=20000, ICD9=10000*)+<>(0, 4 day)", icd9Query("10000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=10000, ICD9=20000*)-(0, 1 day)"));
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)->(0, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<(0, 1 day)", icd9Query("20000"))), 1);
    assertQuery(query(engine, identicalQuery("BEFORE(ICD9=10000, ICD9=20000*)-<>(0, 1 day)", icd9Query("20000"))), 1);

    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-(0, 4 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)->(0, 4 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-<(0, 4 day)"));
    assertQuery(query(engine, "BEFORE(ICD9=20000, ICD9=10000*)-<>(0, 4 day)"));
  }

  @Test
  public void error() throws Exception {
    final Stride6TestDataSource src = new Stride6TestDataSource();
    src.addDemographics(MockDemographicsRecord.create(1));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(21638).duration(0).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(21802).duration(0).code("10000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(21678).duration(0).code("20000").year(2012).srcVisit("INPATIENT"));
    src.addVisit(MockVisitRecord.create(ICD9, 1).age(21819).duration(0).code("20000").year(2012).srcVisit("INPATIENT"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(src.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, "SEQUENCE(ICD9=10000, ICD9=20000*) + (-5 days, 0 days)"));
  }

}
