package com.podalv.extractor.ohdsi.functionalTests;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertError;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.deathQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.identicalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.intervalQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.notQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.timelineQuery;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;

public class OhdsiFunctionalTestSuiteDeath {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void addDeath(final Stride6TestDataSource source, final Double value) {
    final int patientId = source.getPatientCnt() + 1;
    source.addDemographics(MockDemographicsRecord.create(patientId).death(value));
    source.addVisit(MockVisitRecord.create(ICD9, patientId).visitId(patientId).age(1.5).code("250.00").duration(0).year(2012).srcVisit("INPATIENT"));
  }

  private Stride6TestDataSource getDeath(final Double value) {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).death(value));
    source.addVisit(MockVisitRecord.create(ICD9, 1).visitId(1).age(1.5).code("250.00").duration(0).year(2012).srcVisit("INPATIENT"));
    return source.toOhdsi();
  }

  @Test
  public void nullDeath() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(getDeath(null), DATA_FOLDER, DATA_FOLDER);
    assertError(query(engine, deathQuery()), "DEATH information is missing in this dataset");
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void zeroDeath() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(getDeath(Double.valueOf(0)), DATA_FOLDER, DATA_FOLDER);
    assertError(query(engine, deathQuery()), "DEATH information is missing in this dataset");
    assertQuery(query(engine, timelineQuery()), 1);
  }

  @Test
  public void death() throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(getDeath(Double.valueOf(1.5)), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, deathQuery()), 1);
    assertQuery(query(engine, timelineQuery()), 1);
    assertQuery(query(engine, identicalQuery(deathQuery(), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1);
  }

  @Test
  public void mixedPatients() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    addDeath(source, 1.5);
    addDeath(source, null);
    addDeath(source, 2.5);
    addDeath(source, 3.5);
    addDeath(source, 0d);
    addDeath(source, 0d);
    addDeath(source, 1.5);
    addDeath(source, 0d);
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source.toOhdsi(), DATA_FOLDER, DATA_FOLDER);
    assertQuery(query(engine, identicalQuery(deathQuery(), intervalQuery(daysToMinutes(1), daysToMinutes(1)))), 1, 7);
    assertQuery(query(engine, identicalQuery(deathQuery(), intervalQuery(daysToMinutes(2), daysToMinutes(2)))), 3);
    assertQuery(query(engine, identicalQuery(deathQuery(), intervalQuery(daysToMinutes(3), daysToMinutes(3)))), 4);
    assertQuery(query(engine, notQuery(deathQuery())), 2, 5, 6, 8);
    assertQuery(query(engine, timelineQuery()), 1, 2, 3, 4, 5, 6, 7, 8);
  }
}
