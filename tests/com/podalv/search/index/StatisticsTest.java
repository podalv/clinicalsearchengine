package com.podalv.search.index;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayList;

public class StatisticsTest {

  @Test
  public void getSameSorted_start_end() throws Exception {
    final int[] patientIds = new int[] {1, 5, 10};
    final IntArrayList list = new IntArrayList(new int[] {1, 2, 3, 6, 10, 11});
    Assert.assertEquals(2, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_no_verlap() throws Exception {
    final int[] patientIds = new int[] {4, 8};
    final IntArrayList list = new IntArrayList(new int[] {1, 2, 3, 6, 10, 11});
    Assert.assertEquals(0, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_start() throws Exception {
    final int[] patientIds = new int[] {1};
    final IntArrayList list = new IntArrayList(new int[] {1, 2, 3, 6, 10, 11});
    Assert.assertEquals(1, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_end() throws Exception {
    final int[] patientIds = new int[] {11};
    final IntArrayList list = new IntArrayList(new int[] {1, 2, 3, 6, 10, 11});
    Assert.assertEquals(1, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_end_middle() throws Exception {
    final int[] patientIds = new int[] {6};
    final IntArrayList list = new IntArrayList(new int[] {1, 2, 3, 6, 10, 11});
    Assert.assertEquals(1, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_start_end_reversed() throws Exception {
    final int[] patientIds = new int[] {1, 2, 3, 6, 10, 11};
    final IntArrayList list = new IntArrayList(new int[] {1, 5, 10});
    Assert.assertEquals(2, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_no_verlap_reversed() throws Exception {
    final int[] patientIds = new int[] {1, 2, 3, 6, 10, 11};
    final IntArrayList list = new IntArrayList(new int[] {4, 8});
    Assert.assertEquals(0, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_start_reversed() throws Exception {
    final int[] patientIds = new int[] {1, 2, 3, 6, 10, 11};
    final IntArrayList list = new IntArrayList(new int[] {1});
    Assert.assertEquals(1, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_end_reversed() throws Exception {
    final int[] patientIds = new int[] {1, 2, 3, 6, 10, 11};
    final IntArrayList list = new IntArrayList(new int[] {11});
    Assert.assertEquals(1, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void getSameSorted_end_middle_reversed() throws Exception {
    final int[] patientIds = new int[] {1, 2, 3, 6, 10, 11};
    final IntArrayList list = new IntArrayList(new int[] {6});
    Assert.assertEquals(1, Statistics.getSameSorted(patientIds, list.iterator()));
  }

  @Test
  public void findAge() {
    final IntArrayList list = new IntArrayList();
    list.add(3);
    list.add(1);
    list.add(5);

    list.add(2);
    list.add(2);
    list.add(6);

    list.add(5);
    list.add(3);
    list.add(7);

    list.add(6);
    list.add(4);
    list.add(7);

    list.add(7);
    list.add(6);
    list.add(7);

    list.add(7);
    list.add(7);
    list.add(7);

    Assert.assertEquals(Statistics.findAge(list, 0, 1), 0);
    Assert.assertEquals(Statistics.findAge(list, 1, 1), 0);
    Assert.assertEquals(Statistics.findAge(list, 2, 1), 1);
    Assert.assertEquals(Statistics.findAge(list, 3, 1), 2);
    Assert.assertEquals(Statistics.findAge(list, 4, 1), 3);
    Assert.assertEquals(Statistics.findAge(list, 6, 1), 4);
    Assert.assertEquals(Statistics.findAge(list, 7, 1), 5);
    Assert.assertEquals(Statistics.findAge(list, 5, 1), 3);
    Assert.assertEquals(Statistics.findAge(list, 8, 1), 5);
    Assert.assertEquals(Statistics.findAge(list, Integer.MIN_VALUE, 1), 0);
    Assert.assertEquals(Statistics.findAge(list, Integer.MAX_VALUE, 1), 5);
  }

  @Test
  public void findAge_index2() {
    final IntArrayList list = new IntArrayList();
    list.add(3);
    list.add(1);
    list.add(1);

    list.add(2);
    list.add(2);
    list.add(2);

    list.add(5);
    list.add(3);
    list.add(5);

    list.add(6);
    list.add(4);
    list.add(9);

    list.add(7);
    list.add(6);
    list.add(11);

    list.add(7);
    list.add(7);
    list.add(15);

    Assert.assertEquals(Statistics.findAge(list, 0, 2), 0);
    Assert.assertEquals(Statistics.findAge(list, 1, 2), 0);
    Assert.assertEquals(Statistics.findAge(list, 2, 2), 1);
    Assert.assertEquals(Statistics.findAge(list, 3, 2), 1);
    Assert.assertEquals(Statistics.findAge(list, 4, 2), 1);
    Assert.assertEquals(Statistics.findAge(list, 5, 2), 2);
    Assert.assertEquals(Statistics.findAge(list, 6, 2), 2);
    Assert.assertEquals(Statistics.findAge(list, 7, 2), 2);
    Assert.assertEquals(Statistics.findAge(list, 8, 2), 2);
    Assert.assertEquals(Statistics.findAge(list, 9, 2), 3);
    Assert.assertEquals(Statistics.findAge(list, 10, 2), 3);
    Assert.assertEquals(Statistics.findAge(list, 11, 2), 4);
    Assert.assertEquals(Statistics.findAge(list, 12, 2), 4);
    Assert.assertEquals(Statistics.findAge(list, 13, 2), 4);
    Assert.assertEquals(Statistics.findAge(list, 14, 2), 4);
    Assert.assertEquals(Statistics.findAge(list, 15, 2), 5);
    Assert.assertEquals(Statistics.findAge(list, 16, 2), 5);
    Assert.assertEquals(Statistics.findAge(list, Integer.MIN_VALUE, 2), 0);
    Assert.assertEquals(Statistics.findAge(list, Integer.MAX_VALUE, 2), 5);
  }

}