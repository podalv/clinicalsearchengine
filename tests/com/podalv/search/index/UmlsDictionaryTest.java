package com.podalv.search.index;

import org.junit.Assert;
import org.junit.Test;

public class UmlsDictionaryTest {

  @Test
  public void smokeTest() throws Exception {
    final UmlsDictionary dict = UmlsDictionary.create();
    Assert.assertTrue(dict.getAtcCodes().hasNext());
    Assert.assertTrue(dict.getIcd9Codes().hasNext());
    Assert.assertTrue(dict.getCptCodes().iterator().hasNext());
    Assert.assertTrue(dict.getRxNorms().size() != 0);
  }

}
