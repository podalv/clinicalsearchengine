package com.podalv.search.index;

import org.junit.Assert;
import org.junit.Test;

public class StatisticsAgeGeneratorTest {

  @Test
  public void sort() throws Exception {
    final StatisticsAgeGenerator generator = new StatisticsAgeGenerator();
    generator.addRecord(1, 5, 5);
    generator.addRecord(2, 5, 4);
    generator.addRecord(3, 5, 6);
    generator.addRecord(4, 3, 1);
    generator.addRecord(5, 7, 1);
    Assert.assertArrayEquals(new int[] {4, 3, 1, 3, 5, 6, 1, 5, 5, 2, 5, 4, 5, 7, 1}, generator.getSortedByStart().toArray());
    Assert.assertArrayEquals(new int[] {5, 7, 1, 4, 3, 1, 2, 5, 4, 1, 5, 5, 3, 5, 6}, generator.getSortedByEnd().toArray());
  }
}
