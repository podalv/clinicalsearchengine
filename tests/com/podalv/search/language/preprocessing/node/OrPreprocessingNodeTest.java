package com.podalv.search.language.preprocessing.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.OrPreprocessingNode;

public class OrPreprocessingNodeTest {

  @Test
  public void all_identical() throws Exception {
    final OrPreprocessingNode orNode = new OrPreprocessingNode();
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(5, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(10, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(15, orNode.getValue());
    Assert.assertFalse(orNode.next());
    Assert.assertTrue(orNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, orNode.getValue());
  }

  @Test
  public void one_empty() throws Exception {
    final OrPreprocessingNode orNode = new OrPreprocessingNode();
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(5, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(10, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(15, orNode.getValue());
    Assert.assertFalse(orNode.next());
    Assert.assertTrue(orNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, orNode.getValue());
  }

  @Test
  public void all_empty() throws Exception {
    final OrPreprocessingNode orNode = new OrPreprocessingNode();
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    Assert.assertFalse(orNode.next());
    Assert.assertTrue(orNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, orNode.getValue());
  }

  @Test
  public void different() throws Exception {
    final OrPreprocessingNode orNode = new OrPreprocessingNode();
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 5, 12, 18}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 18}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 12, 18}))));
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(1, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(5, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(12, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(18, orNode.getValue());
    Assert.assertFalse(orNode.next());
    Assert.assertTrue(orNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, orNode.getValue());
  }

  @Test
  public void different_not_matching() throws Exception {
    final OrPreprocessingNode orNode = new OrPreprocessingNode();
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 5, 12, 18}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {6, 17}))));
    orNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 12, 18}))));
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(1, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(5, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(6, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(12, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(17, orNode.getValue());
    Assert.assertTrue(orNode.next());
    Assert.assertFalse(orNode.afterLast());
    Assert.assertEquals(18, orNode.getValue());
    Assert.assertFalse(orNode.next());
    Assert.assertTrue(orNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, orNode.getValue());
  }

}
