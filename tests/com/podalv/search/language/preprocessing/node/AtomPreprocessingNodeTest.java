package com.podalv.search.language.preprocessing.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;

public class AtomPreprocessingNodeTest {

  @Test
  public void smokeTest() throws Exception {
    final AtomPreprocessingNode node = new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15})));
    Assert.assertTrue(node.next());
    Assert.assertFalse(node.afterLast());
    Assert.assertEquals(5, node.getValue());
    Assert.assertTrue(node.next());
    Assert.assertFalse(node.afterLast());
    Assert.assertEquals(10, node.getValue());
    Assert.assertTrue(node.next());
    Assert.assertFalse(node.afterLast());
    Assert.assertEquals(15, node.getValue());
    Assert.assertFalse(node.next());
    Assert.assertTrue(node.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, node.getValue());
  }
}
