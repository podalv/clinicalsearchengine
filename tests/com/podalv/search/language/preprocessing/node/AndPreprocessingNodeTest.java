package com.podalv.search.language.preprocessing.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AndPreprocessingNode;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;

public class AndPreprocessingNodeTest {

  @Test
  public void all_identical() throws Exception {
    final AndPreprocessingNode andNode = new AndPreprocessingNode();
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(5, andNode.getValue());
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(10, andNode.getValue());
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(15, andNode.getValue());
    Assert.assertFalse(andNode.next());
    Assert.assertTrue(andNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, andNode.getValue());
  }

  @Test
  public void one_empty() throws Exception {
    final AndPreprocessingNode andNode = new AndPreprocessingNode();
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    Assert.assertFalse(andNode.next());
    Assert.assertTrue(andNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, andNode.getValue());
  }

  @Test
  public void all_empty() throws Exception {
    final AndPreprocessingNode andNode = new AndPreprocessingNode();
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {}))));
    Assert.assertFalse(andNode.next());
    Assert.assertTrue(andNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, andNode.getValue());
  }

  @Test
  public void different() throws Exception {
    final AndPreprocessingNode andNode = new AndPreprocessingNode();
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 5, 12, 18}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 18}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 12, 18}))));
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(18, andNode.getValue());
    Assert.assertFalse(andNode.next());
    Assert.assertTrue(andNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, andNode.getValue());
  }

  @Test
  public void different_not_matching() throws Exception {
    final AndPreprocessingNode andNode = new AndPreprocessingNode();
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 5, 12, 18}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {6, 17}))));
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {1, 12, 18}))));
    Assert.assertFalse(andNode.next());
    Assert.assertTrue(andNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, andNode.getValue());
  }

  @Test
  public void single_child() throws Exception {
    final AndPreprocessingNode andNode = new AndPreprocessingNode();
    andNode.addChild(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(new int[] {5, 10, 15}))));
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(5, andNode.getValue());
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(10, andNode.getValue());
    Assert.assertTrue(andNode.next());
    Assert.assertFalse(andNode.afterLast());
    Assert.assertEquals(15, andNode.getValue());
    Assert.assertFalse(andNode.next());
    Assert.assertTrue(andNode.afterLast());
    Assert.assertEquals(Integer.MIN_VALUE, andNode.getValue());
  }
}
