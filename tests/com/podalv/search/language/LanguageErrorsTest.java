package com.podalv.search.language;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.script.LanguageParser;

public class LanguageErrorsTest {

  @Test
  public void missingForEachVariable() throws Exception {
    final LanguageNode node = LanguageParser.parse(("var aaa = icd9=asdasda\n" + "bbdb").toUpperCase());
    Assert.assertEquals("[0,3:1] Variable BBDB is never declared", node.toString());
  }

  @Test
  public void missingForEachVariables() throws Exception {
    final LanguageNode node = LanguageParser.parse(("var aaa = icd9=asdasda\n" + "INTERSECT(bbdb, CCAC)").toUpperCase());
    Assert.assertEquals("[10,13:1] Variables [BBDB, CCAC] are never declared", node.toString());
  }

  @Test
  public void missingVariable() throws Exception {
    final LanguageNode node = LanguageParser.parse(("var aaa = icd9=asdasda\n" + "$bbb").toUpperCase());
    Assert.assertEquals("[1,4:1] Could not find variable '$bbb'", node.toString());
  }

}