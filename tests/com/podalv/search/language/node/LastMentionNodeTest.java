package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class LastMentionNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void complexPatient() {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3, 4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {5, 6, 7}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {10, 30, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {50, 90, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {150, 200, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {100, 210, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {180, 250, 0}));

  }

  @Test
  public void smokeTest() throws Exception {
    final RootNode root = LanguageParser.parse("AND(LAST MENTION(ICD9=100.1))");
    complexPatient();
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    while (p.hasNext()) {
      p.next();
      System.out.println(p.getStartId());
      System.out.println(p.getEndId());
    }
    Assert.assertEquals(150, p.getStartId());
    Assert.assertEquals(200, p.getEndId());
    Assert.assertFalse(p.hasNext());
  }
}
