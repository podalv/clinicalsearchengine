package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class IntersectNodeTest {

  public static PayloadIterator getValues(final int ... values) {
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < values.length; x += 2) {
      result.add(values[x]);
      result.add(values[x + 1]);
    }
    return new EmptyPayloadIterator(result.iterator());
  }

  private int[] intersect(final PayloadIterator i1, final PayloadIterator i2) {
    final IntArrayList instance = new IntArrayList();
    final PayloadIterator pi = IntersectNode.intersect(instance, new PatientSearchModel(null, null, 1), i1, i2);
    final IntArrayList result = new IntArrayList();
    while (pi.hasNext()) {
      pi.next();
      result.add(pi.getStartId());
      result.add(pi.getEndId());
    }
    return result.toArray();
  }

  @Test
  public void identical() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 10}, intersect(getValues(5, 10), getValues(5, 10)));
  }

  @Test
  public void first_smaller() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 10}, intersect(getValues(5, 10), getValues(4, 11)));
  }

  @Test
  public void second_smaller() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 10}, intersect(getValues(4, 11), getValues(5, 10)));
  }

  @Test
  public void first_earlier() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 6}, intersect(getValues(3, 6), getValues(5, 10)));
  }

  @Test
  public void second_earlier() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 6}, intersect(getValues(5, 10), getValues(3, 6)));
  }

  @Test
  public void first_later() throws Exception {
    Assert.assertArrayEquals(new int[] {6, 10}, intersect(getValues(5, 10), getValues(6, 12)));
  }

  @Test
  public void second_later() throws Exception {
    Assert.assertArrayEquals(new int[] {6, 10}, intersect(getValues(6, 12), getValues(5, 10)));
  }

  @Test
  public void first_too_early() throws Exception {
    Assert.assertArrayEquals(new int[] {}, intersect(getValues(1, 3), getValues(5, 10)));
  }

  @Test
  public void second_too_early() throws Exception {
    Assert.assertArrayEquals(new int[] {}, intersect(getValues(5, 10), getValues(1, 3)));
  }

  @Test
  public void one_point_start_intersect() throws Exception {
    Assert.assertArrayEquals(new int[] {10, 10}, intersect(getValues(5, 10), getValues(10, 15)));
  }

  @Test
  public void one_point_end_intersect() throws Exception {
    Assert.assertArrayEquals(new int[] {10, 10}, intersect(getValues(10, 15), getValues(5, 10)));
  }

  @Test
  public void multiple_small_ones_one_big_one() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 7, 10, 12, 15, 18}, intersect(getValues(5, 7, 10, 12, 15, 18), getValues(4, 20)));
  }

  @Test
  public void multiple_disconnected_ones() throws Exception {
    Assert.assertArrayEquals(new int[] {8, 10, 20, 22, 25, 30}, intersect(getValues(1, 3, 8, 22, 25, 40), getValues(5, 10, 20, 30)));
  }

  @Test
  public void multiple_nodes() throws Exception {
    Assert.assertArrayEquals(new int[] {1, 3, 5, 10, 15, 20}, intersect(getValues(1, 20), getValues(1, 3, 5, 10, 15, 20)));
  }

  @Test
  public void multiple_nodes_after_first_no_intersection() throws Exception {
    Assert.assertArrayEquals(new int[] {}, intersect(getValues(1, 2), getValues(5, 10, 15, 20, 25, 30)));
  }

  @Test
  public void multiple_nodes_after_first_intersection() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 6}, intersect(getValues(1, 6), getValues(5, 10, 15, 20, 25, 30)));
  }

  @Test
  public void multiple_nodes_moving_at_the_same_time() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 6}, intersect(getValues(1, 6), getValues(5, 10, 15, 20, 25, 30)));
  }

  @Test
  public void repeated_shorter() throws Exception {
    Assert.assertArrayEquals(new int[] {1, 2, 1, 5, 1, 10}, intersect(getValues(1, 2, 1, 5, 1, 10), getValues(1, 10)));
  }

}
