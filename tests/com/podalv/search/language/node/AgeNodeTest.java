package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class AgeNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void complexPatient() {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {0, 1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5, 6}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(20), 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(50), Common.yearsToTime(90), 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(150), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(210), 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(180), Common.yearsToTime(250), 0}));

    Mockito.when(patient.getAgeRanges()).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), Common.yearsToTime(50), Common.yearsToTime(90),
        Common.yearsToTime(100), Common.yearsToTime(250)}));

  }

  private void assertAges(final String expression, final int ... expected) {
    final RootNode root = LanguageParser.parse(expression, false);
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    int pos = 0;
    if (expected == null || expected.length == 0) {
      Assert.assertFalse(p.hasNext());
    }
    else {
      Assert.assertTrue(p.hasNext());
      while (p.hasNext()) {
        p.next();
        Assert.assertEquals(Common.yearsToTime(expected[pos++]), p.getStartId());
        Assert.assertEquals(Common.yearsToTime(expected[pos++]), p.getEndId());
      }
      Assert.assertFalse(p.hasNext());
    }
    Assert.assertEquals(TimeIntervals.class, new AgeNode(1, 2).generatesResultType());
  }

  @Test
  public void smokeTest() throws Exception {
    //10, 30, 50, 90, 100, 250
    complexPatient();
    assertAges("AND(AGE(0, 20 YEARS))", 10, 20);
    assertAges("AND(AGE(20 YEARS, 0))", 10, 20);
    assertAges("AND(AGE(10 YEARS, 150 YEARS))", 10, 30, 50, 90, 100, 150);
    assertAges("AND(AGE(150 YEARS, 10 YEARS))", 10, 30, 50, 90, 100, 150);
    assertAges("AND(AGE(MIN, 50 YEARS))", 10, 30, 50, 50);
    assertAges("AND(AGE(50 YEARS, MIN))", 10, 30, 50, 50);
    assertAges("AND(AGE(50 YEARS, MAX))", 50, 90, 100, 250);
    assertAges("AND(AGE(MAX, 50 YEARS))", 50, 90, 100, 250);
    assertAges("AND(AGE(50 YEARS, 50 YEARS))", 50, 50);
  }

}
