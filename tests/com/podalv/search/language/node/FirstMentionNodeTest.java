package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class FirstMentionNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void complexPatient() {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3, 4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {5, 6, 7}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {10, 30, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {50, 90, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {150, 200, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {100, 210, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {180, 250, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(250);
  }

  public static void multipleOverlaps() {
    //100.1 10-100 200-300 400-500
    //100.2 1-5 8-15 20-30 45-60 210-220 250-280 490-510 550-580
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5, 6, 7, 8, 9, 10, 11}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 100, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {200, 300, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {400, 500, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {1, 5, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {8, 15, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {20, 30, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {45, 60, 0}));
    Mockito.when(patient.getPayload(8)).thenReturn(new IntArrayList(new int[] {210, 220, 0}));
    Mockito.when(patient.getPayload(9)).thenReturn(new IntArrayList(new int[] {250, 280, 0}));
    Mockito.when(patient.getPayload(10)).thenReturn(new IntArrayList(new int[] {490, 510, 0}));
    Mockito.when(patient.getPayload(11)).thenReturn(new IntArrayList(new int[] {550, 580, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(1);
    Mockito.when(patient.getEndTime()).thenReturn(580);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  @Test
  public void smokeTest() throws Exception {
    final RootNode root = LanguageParser.parse("AND(FIRST MENTION(ICD9=100.1))");
    complexPatient();
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    while (p.hasNext()) {
      p.next();
      System.out.println(p.getStartId());
      System.out.println(p.getEndId());
    }
    Assert.assertEquals(10, p.getStartId());
    Assert.assertEquals(20, p.getEndId());
    Assert.assertFalse(p.hasNext());
  }

  @Test
  public void complexFirstMention() throws Exception {
    final RootNode root = LanguageParser.parse("AND(FIRST MENTION(ICD9=100.1, ICD9=100.2))");
    complexPatient();
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    while (p.hasNext()) {
      p.next();
      System.out.println(p.getStartId());
      System.out.println(p.getEndId());
    }
    Assert.assertEquals(150, p.getStartId());
    Assert.assertEquals(200, p.getEndId());
    Assert.assertFalse(p.hasNext());
  }

  @Test
  public void multipleOverlapsTest() throws Exception {
    //100.1 10-100 200-300 400-500
    //100.2 1-5 8-15 20-30 45-60 210-220 250-280 490-510 550-580
    multipleOverlaps();

    BeforeNodeTest.evaluate(patient, indices, "FIRST MENTION(ICD9=100.2, ICD9=100.1)", new int[] {10, 15, 210, 220, 490, 500});
    BeforeNodeTest.evaluate(patient, indices, "LAST MENTION(ICD9=100.2, ICD9=100.1)", new int[] {45, 60, 250, 280, 490, 500});

    BeforeNodeTest.evaluate(patient, indices, "FIRST MENTION(ICD9=100.1)", new int[] {10, 100});
    BeforeNodeTest.evaluate(patient, indices, "FIRST MENTION(ICD9=100.2)", new int[] {1, 5});
    BeforeNodeTest.evaluate(patient, indices, "LAST MENTION(ICD9=100.1)", new int[] {400, 500});
    BeforeNodeTest.evaluate(patient, indices, "LAST MENTION(ICD9=100.2)", new int[] {550, 580});

  }

  @Test
  public void nonIntersectTest() throws Exception {
    final RootNode root = LanguageParser.parse("AND(RETURN ICD9=100.1 NOT INTERSECTING ANY (ICD9=100.2, ICD9=100.3))");
    complexPatient();
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final int[] values = new int[4];

    final PayloadIterator p = result.iterator(patient);
    int pos = 0;
    while (p.hasNext()) {
      p.next();
      values[pos++] = p.getStartId();
      values[pos++] = p.getEndId();
    }
    Assert.assertArrayEquals(new int[] {10, 30, 50, 90}, values);
    Assert.assertFalse(p.hasNext());
  }

}
