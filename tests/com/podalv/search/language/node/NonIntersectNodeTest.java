package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class NonIntersectNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void identicalPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getStartTime()).thenReturn(5);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void leftSinglePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));
    Mockito.when(patient.getStartTime()).thenReturn(5);
    Mockito.when(patient.getEndTime()).thenReturn(15);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {11, 15, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void leftMultiplePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4}));
    Mockito.when(patient.getStartTime()).thenReturn(5);
    Mockito.when(patient.getEndTime()).thenReturn(200);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {15, 20, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {30, 50, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void rightMultiplePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5}));
    Mockito.when(patient.getStartTime()).thenReturn(5);
    Mockito.when(patient.getEndTime()).thenReturn(500);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {15, 20, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {30, 50, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {300, 500, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void intervleavePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5, 6, 7}));
    Mockito.when(patient.getStartTime()).thenReturn(1);
    Mockito.when(patient.getEndTime()).thenReturn(2005);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {1, 5, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {30, 50, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {201, 250, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {2001, 2005, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void neverIntersectPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4}));
    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(2000);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {9, 11, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void alwaysIntersectAll() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {6, 7}));
    Mockito.when(patient.getStartTime()).thenReturn(9);
    Mockito.when(patient.getEndTime()).thenReturn(2002);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {9, 11, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {190, 1003, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {19, 101, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {1999, 2002, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3")}));
  }

  public static void neverIntersectMultiplePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {5}));
    Mockito.when(patient.getStartTime()).thenReturn(9);
    Mockito.when(patient.getEndTime()).thenReturn(2000);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {9, 11, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {150, 151, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3")}));
  }

  public static void neverIntersectAllPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {5}));
    Mockito.when(patient.getStartTime()).thenReturn(9);
    Mockito.when(patient.getEndTime()).thenReturn(2000);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {9, 11, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {15, 19, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3")}));
  }

  public static void alwaysIntersectAnyPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");
    indices.addIcd9Code("100.4");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {5}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.4"))).thenReturn(new IntArrayList(new int[] {6, 7}));
    Mockito.when(patient.getStartTime()).thenReturn(9);
    Mockito.when(patient.getEndTime()).thenReturn(2000);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {9, 11, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {15, 19, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {150, 200, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {1000, 1001, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3"), indices.getIcd9(
        "100.4")}));
  }

  public static void multiplePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    //100.1 = 10-20, 30-40, 50-60
    //100.2 = 1-11, 22-28
    //100.3 = 55-70 80-90
    //100.4 = 1-5, 100, 200
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");
    indices.addIcd9Code("100.4");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {6, 7}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.4"))).thenReturn(new IntArrayList(new int[] {8, 9}));
    Mockito.when(patient.getStartTime()).thenReturn(1);
    Mockito.when(patient.getEndTime()).thenReturn(200);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {30, 40, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {50, 60, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {1, 11, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {22, 28, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {55, 70, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {80, 90, 0}));
    Mockito.when(patient.getPayload(8)).thenReturn(new IntArrayList(new int[] {1, 5, 0}));
    Mockito.when(patient.getPayload(9)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3"), indices.getIcd9(
        "100.4")}));
  }

  private void assertAges(final String expression, final int ... expected) {
    final RootNode root = LanguageParser.parse(expression);
    final TimeIntervals result = root.children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    int pos = 0;
    if (expected == null || expected.length == 0) {
      Assert.assertFalse(p.hasNext());
    }
    else {
      Assert.assertTrue(p.hasNext());
      while (p.hasNext()) {
        p.next();
        Assert.assertEquals(expected[pos++], p.getStartId());
        Assert.assertEquals(expected[pos++], p.getEndId());
      }
      Assert.assertFalse(p.hasNext());
    }
  }

  public static PayloadIterator getValues(final int ... values) {
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < values.length; x += 2) {
      result.add(values[x]);
      result.add(values[x + 1]);
    }
    return new EmptyPayloadIterator(result.iterator());
  }

  @Test
  public void identical() throws Exception {
    identicalPatient();
    //100.1 = 5-10
    //100.2 = 5-10
    assertAges("RETURN ICD9=100.1 INTERSECTING ICD9=100.2", 5, 10);
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ICD9=100.2", 5, 10);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ICD9=100.1");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.3", 5, 10);
    assertAges("RETURN ICD9=100.3 NOT INTERSECTING ICD9=100.1");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ANY (ICD9=100.1, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ALL (ICD9=100.1, ICD9=100.3)", 5, 10);
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.1, ICD9=100.3)", 5, 10);
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.1, ICD9=100.3)");
  }

  @Test
  public void leftSingle() throws Exception {
    leftSinglePatient();
    //100.1 = 5-10
    //100.2 = 11-15
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2", 5, 10);
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ICD9=100.1", 11, 15);
  }

  @Test
  public void leftMultiple() throws Exception {
    leftMultiplePatient();
    //100.1 = 5-10, 15-20, 30-50
    //100.2 = 100-200
    assertAges("RETURN ICD9=100.1 INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2", 5, 10, 15, 20, 30, 50);
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ICD9=100.1", 100, 200);
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ANY (ICD9=100.1, ICD9=100.3)", 100, 200);
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ALL (ICD9=100.1, ICD9=100.3)", 100, 200);
    assertAges("RETURN ICD9=100.2 NEVER INTERSECTING ANY (ICD9=100.1, ICD9=100.3)", 100, 200);
    assertAges("RETURN ICD9=100.2 NEVER INTERSECTING ALL (ICD9=100.1, ICD9=100.3)", 100, 200);
  }

  @Test
  public void rightMultiple() throws Exception {
    rightMultiplePatient();
    //100.1 = 5-10, 15-20, 30-50
    //100.2 = 100-200, 300-500
    assertAges("RETURN ICD9=100.1 INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2", 5, 10, 15, 20, 30, 50);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 5, 10, 15, 20, 30, 50);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 5, 10, 15, 20, 30, 50);
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 5, 10, 15, 20, 30, 50);
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 5, 10, 15, 20, 30, 50);
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ICD9=100.1", 100, 200, 300, 500);
  }

  @Test
  public void interleave() throws Exception {
    intervleavePatient();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 1-5, 30-50, 201-250, 2001-2005
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.2 NOT INTERSECTING ICD9=100.1", 1, 5, 30, 50, 201, 250, 2001, 2005);
  }

  @Test
  public void leftEmpty() throws Exception {
    intervleavePatient();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 1-5, 30-50, 201-250, 2001-2005
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.3", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.3 NOT INTERSECTING ICD9=100.1");
  }

  @Test
  public void multiplePatientTest() throws Exception {
    multiplePatient();
    //100.1 = 10-20, 30-40, 50-60
    //100.2 = 1-11, 22-28
    //100.3 = 55-70 80-90
    //100.4 = 1-5, 100, 200
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ANY (ICD9=100.2, ICD9=100.3, ICD9=100.4)", 30, 40);
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.2, ICD9=100.3, ICD9=100.4)");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.2, ICD9=100.3, ICD9=100.4)", 10, 20, 30, 40, 50, 60);
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3, ICD9=100.4)", 10, 20, 50, 60);
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ICD9=100.3");
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ICD9=100.4");
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ICD9=100.1", 10, 20, 30, 40, 50, 60);
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3, ICD9=100.4)");
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.5)");
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.5)", 10, 20);
  }

  @Test
  public void neverIntersectTest() throws Exception {
    neverIntersectPatient();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 9-11
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2", 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ALL(ICD9=100.2)", 100, 200, 1000, 2000);
  }

  @Test
  public void neverIntersectMultipleTest() throws Exception {
    neverIntersectMultiplePatient();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 9-11
    //100.3 = 150-151
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 1000, 2000);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
  }

  @Test
  public void neverIntersectAllTest() throws Exception {
    neverIntersectAllPatient();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 9-11
    //100.3 = 15-20
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 10, 20);
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 10, 20);
  }

  @Test
  public void alwaysIntersectAnyTest() throws Exception {
    alwaysIntersectAnyPatient();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 9-11
    //100.3 = 15-20
    //100.4 = 150-200, 1000-1001
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ANY (ICD9=100.2, ICD9=100.3, ICD9=100.4)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ALL (ICD9=100.2, ICD9=100.3, ICD9=100.4)");
  }

  @Test
  public void alwaysIntersectAllTest() throws Exception {
    alwaysIntersectAll();
    //100.1 = 10-20, 100-200, 1000-2000
    //100.2 = 9-11, 190-1003
    //100.3 = 19-101, 1999, 2002
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3, ICD9=100.4)");
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3, ICD9=100.4)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 INTERSECTING ICD9=100.2", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 INTERSECTING ICD9=100.3", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.2");
    assertAges("RETURN ICD9=100.1 NOT INTERSECTING ICD9=100.3");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ALL (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 NEVER INTERSECTING ANY (ICD9=100.2, ICD9=100.3)");
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ALL (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
    assertAges("RETURN ICD9=100.1 ALWAYS INTERSECTING ANY (ICD9=100.2, ICD9=100.3)", 10, 20, 100, 200, 1000, 2000);
  }

}
