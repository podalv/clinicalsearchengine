package com.podalv.search.language.node;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;

public class AtcNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;
  private static UmlsDictionary     umls;

  static {
    umls = getUmls();
  }

  private static UmlsDictionary getUmls() {
    final UmlsDictionary result = Mockito.mock(UmlsDictionary.class);

    final HashSet<String> atcResult = new HashSet<String>();
    atcResult.add("A001a");
    atcResult.add("A001b");
    final String[] atcResultList = new String[] {"A001a", "A001b"};
    Mockito.when(result.getAtcHierarchy(1)).thenReturn(atcResult);
    Mockito.when(result.getAtcHierarchy("A001")).thenReturn(atcResultList);
    Mockito.when(result.getAtcText("A001a")).thenReturn("A001a text");
    Mockito.when(result.getAtcText("A001b")).thenReturn("A001b text");

    return result;
  }

  private static void simplePatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addAtcCode("A001a");
    indices.addAtcCode("A001b");
    indices.addAtcCode("A001");
    indices.addAtcCode("A");
    indices.setUmls(umls);

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getRxNormCodesFromAtc(indices.getAtcCode("A"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(patient.getRxNormCodesFromAtc(indices.getAtcCode("A001a"))).thenReturn(new IntArrayList(new int[] {1, 2}));
    Mockito.when(patient.getRxNormCodesFromAtc(indices.getAtcCode("A001b"))).thenReturn(new IntArrayList(new int[] {3}));
    Mockito.when(patient.getPayload(10)).thenReturn(new IntArrayList(new int[] {10, 11}));
    Mockito.when(patient.getPayload(20)).thenReturn(new IntArrayList(new int[] {20, 21}));
    Mockito.when(patient.getPayload(30)).thenReturn(new IntArrayList(new int[] {30, 31}));
    Mockito.when(patient.getRxNorm(1)).thenReturn(new IntArrayList(new int[] {10}));
    Mockito.when(patient.getRxNorm(2)).thenReturn(new IntArrayList(new int[] {20}));
    Mockito.when(patient.getRxNorm(3)).thenReturn(new IntArrayList(new int[] {30}));
  }

  @BeforeClass
  public static void setup() {
    simplePatient();
  }

  @Test
  public void smokeTest() throws Exception {
    final AtcNode atc = new AtcNode("A");
    final EvaluationResult result = atc.evaluate(indices, patient);
    Assert.assertEquals(TimeIntervals.class, atc.generatesResultType());
    Assert.assertTrue(result.toBooleanResult().result());
  }

  @Test
  public void time_intervals_child() throws Exception {
    final IdenticalNode identical = new IdenticalNode();
    identical.addChild(new IntervalNumericNode(30, 31));
    identical.addChild(new AtcNode("A001b"));
    final EvaluationResult result = identical.evaluate(indices, patient);
    Assert.assertTrue(result.toBooleanResult().result());
  }

  @Test
  public void time_intervals_parent() throws Exception {
    final IdenticalNode identical = new IdenticalNode();
    final UnionNode union = new UnionNode();
    union.addChild(new IntervalNumericNode(10, 11));
    union.addChild(new IntervalNumericNode(20, 21));
    union.addChild(new IntervalNumericNode(30, 31));
    identical.addChild(union);
    identical.addChild(new AtcNode("A"));
    final EvaluationResult result = identical.evaluate(indices, patient);
    Assert.assertTrue(result.toBooleanResult().result());
  }

}
