package com.podalv.search.language.node;

import static com.podalv.search.language.node.BeforeNodeTest.evaluate;

import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class BeforeNodeBeforeWithOverlap {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void overlapPatient() {
    // 100.1 10-20
    // 100.2 20-30

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {20, 30, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  @Test
  public void overlapCorrectOverlapSpecified() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, START)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END, END)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END, END)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, START)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, START)", new int[] {20, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, START)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END, END)", new int[] {20, 30});
  }

  @Test
  public void overlapIncorrectOverlapSpecified() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END, END)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, START)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, START)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END, END)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, START)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, START)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END, END)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, START)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, START)", new int[] {20, 30});
  }

  @Test
  public void noOverlapLeft() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-1, START-10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-1, START-10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-1, START-10)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-1, START-10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END-1)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-1, START-10)", new int[] {20, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-1, START-10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-1, START-10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-1, START-10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END-1)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-1, START-10)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-1, START-10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END-1)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-1, START-10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-1, START-10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END-1)", new int[] {});
  }

  @Test
  public void noOverlapRight() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+1, END+10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+1, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START+1, END)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START+1, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+1, END+10)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+1, END+10)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+1, END+10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+1, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START+1, END)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START+1, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+1, END+10)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+1, END+10)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START+1, END)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START+1, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+1, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+1, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+1, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+1, END+10)", new int[] {20, 30});
  }

  @Test
  public void leftNoStart() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-11, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-11, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-11, START-15)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-11, START-15)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-5, START-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START-5, START-1)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-11, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-11, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-11, START-15)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-11, START-15)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-5, START-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START-5, START-1)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-11, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-11, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-11, START-15)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-11, START-15)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-5, START-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START-5, START-1)", new int[] {20, 30});
  }

  @Test
  public void leftStart() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-10, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-10, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-5, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START-5, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-10, START-15)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-10, START-15)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-5, START)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START-5, START)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-10, START-15)", new int[] {20, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-10, START-15)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-5, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START-5, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-10, START-15)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-10, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-5, START)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START-5, START)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-10, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-10, START-15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-5, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START-5, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-10, START-15)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-10, START-15)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-5, START)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START-5, START)", new int[] {20, 30});
  }

  @Test
  public void rightEnd() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+10, END+11)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+10, END+11)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END, END+1)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+10, END+11)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+10, END+11)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+10, END+11)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+10, END+11)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END, END+1)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+10, END+11)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+10, END+11)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+10, END+11)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+10, END+11)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END, END+1)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+10, END+11)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+10, END+11)", new int[] {});
  }

  @Test
  public void rightNoEnd() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+11, END+12)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+11, END+12)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END+1, END+2)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+11, END+12)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+11, END+12)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+11, END+12)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+11, END+12)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END+1, END+2)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+11, END+12)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+11, END+12)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+11, END+12)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+11, END+12)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END+1, END+2)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+11, END+12)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+11, END+12)", new int[] {20, 30});
  }

  @Test
  public void leftRightNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-15, START-11)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-15, START-11)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-1, START-5)+<>(END+11, END+12)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START-1, START-5)+<>(END+11, END+12)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-15, START-11)-<>(END+1, END+2)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-15, START-11)-<>(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-1, START-5)-<>(END+11, END+12)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START-1, START-5)-<>(END+11, END+12)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-15, START-11)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-15, START-11)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-1, START-5)+<(END+11, END+12)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START-1, START-5)+<(END+11, END+12)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-15, START-11)-<(END+1, END+2)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-15, START-11)-<(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-1, START-5)-<(END+11, END+12)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START-1, START-5)-<(END+11, END+12)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-15, START-11)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-15, START-11)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-1, START-5)+>(END+11, END+12)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START-1, START-5)+>(END+11, END+12)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-15, START-11)->(END+1, END+2)", new int[] {20, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-15, START-11)->(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-1, START-5)->(END+11, END+12)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START-1, START-5)->(END+11, END+12)", new int[] {20, 30});
  }

  @Test
  public void containsAll() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-10, END)", new int[] {20, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-10, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END+10)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-10, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-10, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-10, END)", new int[] {20, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-10, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END+10)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-10, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-10, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-10, END)", new int[] {20, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-10, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END+10)", new int[] {20, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-10, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-10, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END+10)", new int[] {});
  }

  @Test
  public void conflicting() throws Exception {
    //100.1 10-20
    //100.2 20-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-10, END)+<>(START-15, START-11)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-10, END)+<>(START-15, START-11)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END+10)+<>(START-5, START-1)+<>(END+11, END+15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END+10)+<>(START-5, START-1)+<>(END+11, END+15)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-10, END)-<>(START-15, START-11)-<>(END+1, END+2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-10, END)-<>(START-15, START-11)-<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END+10)-<>(START-5, START-1)-<>(END+11, END+15)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END+10)-<>(START-5, START-1)-<>(END+11, END+15)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-10, END)+<(START-15, START-11)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-10, END)+<(START-15, START-11)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END+10)+<(START-5, START-1)+<(END+11, END+15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END+10)+<(START-5, START-1)+<(END+11, END+15)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-10, END)-<(START-15, START-11)-<(END+1, END+2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-10, END)-<(START-15, START-11)-<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END+10)-<(START-5, START-1)-<(END+11, END+15)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END+10)-<(START-5, START-1)-<(END+11, END+15)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-10, END)+>(START-15, START-11)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-10, END)+>(START-15, START-11)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END+10)+>(START-5, START-1)+>(END+11, END+15)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END+10)+>(START-5, START-1)+>(END+11, END+15)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-10, END)->(START-15, START-11)->(END+1, END+2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-10, END)->(START-15, START-11)->(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END+10)->(START-5, START-1)->(END+11, END+15)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END+10)->(START-5, START-1)->(END+11, END+15)", new int[] {});
  }

}
