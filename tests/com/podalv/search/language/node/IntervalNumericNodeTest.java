package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.language.script.LanguageParser;

public class IntervalNumericNodeTest {

  @Test
  public void minutes() throws Exception {
    final LanguageNodeWithChildren root = LanguageParser.parse("INTERVAL(100, 200)");
    final IntervalNumericNode node = (IntervalNumericNode) root.getChildren().get(0);
    Assert.assertEquals("INTERVAL(100, 200)", node.toString());
  }

  @Test
  public void years() throws Exception {
    final LanguageNodeWithChildren root = LanguageParser.parse("INTERVAL(10 YEARS, 20 YEARS)");
    final IntervalNumericNode node = (IntervalNumericNode) root.getChildren().get(0);
    Assert.assertEquals("INTERVAL(" + (365 * 10 * 24 * 60) + ", " + (365 * 20 * 24 * 60) + ")", node.toString());
  }

}
