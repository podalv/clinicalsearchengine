package com.podalv.search.language.node;

import static com.podalv.search.language.node.BeforeNodeTest.evaluate;

import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class BeforeNodeBeforeNoOverlap {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void overlapPatient() {
    // 100.1 10-20
    // 100.2 21-30

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {21, 30, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  @Test
  public void containsBoth() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 10-30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-11, END)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-11, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END+10)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-11, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-11, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-11, END)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-11, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END+10)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-11, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-11, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-11, END)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-11, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END+10)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-11, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-11, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END+10)", new int[] {});
  }

  @Test
  public void outerNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 11-29
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-10, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-10, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, END+9)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+1, END+9)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-10, END-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-10, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, END+9)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+1, END+9)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-10, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-10, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, END+9)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+1, END+9)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-10, END-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-10, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, END+9)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+1, END+9)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-10, END-1)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-10, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, END+9)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+1, END+9)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-10, END-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-10, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, END+9)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+1, END+9)", new int[] {21, 30});
  }

  @Test
  public void leftStartEnd() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-11, START-1)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-11, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-11, START-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-11, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-11, START-1)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-11, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-11, START-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-11, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-11, START-1)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-11, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-11, START-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-11, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END)", new int[] {21, 30});
  }

  @Test
  public void leftNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 11-19
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-10, START-2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-10, START-2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-10, START-2)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-10, START-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+1, END-1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-10, START-2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-10, START-2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-10, START-2)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-10, START-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+1, END-1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-10, START-2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-10, START-2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-10, START-2)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-10, START-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+1, END-1)", new int[] {21, 30});
  }

  @Test
  public void leftStartRightStart() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 10-21
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-11, START)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-11, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-11, START)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-11, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END+1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END+1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-11, START)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-11, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END+1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-11, START)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-11, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END+1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-11, START)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-11, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-11, START)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-11, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END+1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END+1)", new int[] {21, 30});
  }

  @Test
  public void leftNoStartRightNoStart() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 11-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-10, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-10, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-10, START-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-10, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+1, END-1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-10, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-10, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-10, START-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-10, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+1, END-1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-10, START-1)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-10, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-10, START-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-10, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, END-1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+1, END-1)", new int[] {21, 30});
  }

  @Test
  public void leftEndRightStart() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 20, 21
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-1, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-1, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-1, START)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-1, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END, END+1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END, END+1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-1, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-1, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END, END+1)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-1, START)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-1, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END, END+1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-1, START)", new int[] {21, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-1, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-1, START)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-1, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END, END+1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END, END+1)", new int[] {21, 30});
  }

  @Test
  public void rightStartEnd() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 21, 30
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+1, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+1, END+10)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, END)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+1, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+1, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+1, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+1, END+10)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, END)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+1, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+1, END+10)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+1, END+10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+1, END+10)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, END)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+1, END+10)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+1, END+10)", new int[] {});
  }

  @Test
  public void rightNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 21-30
    //INTERVAL = 22, 29
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+2, END+9)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+2, END+9)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START+1, END-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+2, END+9)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+2, END+9)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+2, END+9)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+2, END+9)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START+1, END-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+2, END+9)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+2, END+9)", new int[] {21, 30});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+2, END+9)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+2, END+9)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START+1, END-1)", new int[] {21, 30});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+2, END+9)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+2, END+9)", new int[] {21, 30});
  }

}