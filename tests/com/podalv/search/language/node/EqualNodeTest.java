package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class EqualNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void complexPatient() {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250
    //100.3 10-30 150-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3, 4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {5, 6, 7}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {2, 4}));
    Mockito.when(patient.getStartTime()).thenReturn(Common.yearsToTime(5));
    Mockito.when(patient.getEndTime()).thenReturn(Common.yearsToTime(300));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(20), 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(50), Common.yearsToTime(90), 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(150), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(210), 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(180), Common.yearsToTime(250), 0}));

    Mockito.when(patient.getAgeRanges()).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), Common.yearsToTime(50), Common.yearsToTime(90),
        Common.yearsToTime(100), Common.yearsToTime(250)}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3")}));
  }

  private void assertAges(final String expression, final int ... expected) {
    final RootNode root = LanguageParser.parse(expression);
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    int pos = 0;
    if (expected == null || expected.length == 0) {
      Assert.assertFalse(p.hasNext());
    }
    else {
      Assert.assertTrue(p.hasNext());
      while (p.hasNext()) {
        p.next();
        Assert.assertEquals(Common.yearsToTime(expected[pos++]), p.getStartId());
        Assert.assertEquals(Common.yearsToTime(expected[pos++]), p.getEndId());
      }
      Assert.assertFalse(p.hasNext());
    }
  }

  @Test
  public void smokeTest() throws Exception {
    //10, 30, 50, 90, 100, 250
    complexPatient();
    assertAges("AND(INVERT(EQUAL(ICD9=100.1, ICD9=100.2)))", 5, 300);
    assertAges("AND(EQUAL(ICD9=100.1, ICD9=100.2))");
    assertAges("AND(EQUAL(ICD9=100.1, ICD9=100.3))", 10, 30, 150, 200);
  }

}
