package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class ReturnNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void complexPatient() {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2, 3, 4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {5, 6, 7}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {10, 30, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {50, 90, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {150, 200, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {100, 210, 0}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {180, 250, 0}));

  }

  public static void complexPatient2() {
    //100.1 100-200
    //100.2 10-20 80-110
    //100.3 120-180 300-500
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");
    indices.addIcd9Code("100.4");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {4, 5}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.4"))).thenReturn(new IntArrayList(new int[] {6}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {80, 110, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {120, 180, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {300, 500, 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {50, 70, 0}));
  }

  @Test
  public void returnTest() throws Exception {
    final RootNode root = LanguageParser.parse("RETURN ICD9=100.1 INTERSECTING ICD9=100.2");
    complexPatient();
    final TimeIntervals result = root.children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    while (p.hasNext()) {
      p.next();
    }
    Assert.assertEquals(150, p.getStartId());
    Assert.assertEquals(200, p.getEndId());
    Assert.assertFalse(p.hasNext());
  }

  @Test
  public void returnAllTest() throws Exception {
    final RootNode root = LanguageParser.parse("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3)");
    complexPatient2();
    final TimeIntervals result = root.children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    while (p.hasNext()) {
      p.next();
      System.out.println(p.getStartId());
      System.out.println(p.getEndId());
    }
    Assert.assertEquals(100, p.getStartId());
    Assert.assertEquals(200, p.getEndId());
    Assert.assertFalse(p.hasNext());
  }

  @Test
  public void returnAllNegativeTest() throws Exception {
    final RootNode root = LanguageParser.parse("RETURN ICD9=100.1 INTERSECTING ALL (ICD9=100.2, ICD9=100.3, ICD9=100.4)");
    complexPatient2();
    final TimeIntervals result = root.children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    Assert.assertFalse(p.hasNext());
  }

  @Test
  public void returnAnyTest() throws Exception {
    final RootNode root = LanguageParser.parse("RETURN ICD9=100.1 INTERSECTING ANY (ICD9=100.2, ICD9=100.3, ICD9=100.4)");
    complexPatient2();
    final TimeIntervals result = root.children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    while (p.hasNext()) {
      p.next();
      System.out.println(p.getStartId());
      System.out.println(p.getEndId());
    }
    Assert.assertEquals(100, p.getStartId());
    Assert.assertEquals(200, p.getEndId());
    Assert.assertFalse(p.hasNext());
  }

}
