package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class DiffNodeTest {

  private static PatientSearchModel patient1;
  private static PatientSearchModel patient2;
  private static IndexCollection    indices;

  private static void setup() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient1 = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient1.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient1.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));
    Mockito.when(patient1.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient1.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient1.containsIcd9(indices.getIcd9("100.1"))).thenReturn(true);
    Mockito.when(patient1.containsIcd9(indices.getIcd9("100.2"))).thenReturn(true);

    patient2 = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient2.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient2.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient2.containsIcd9(indices.getIcd9("100.1"))).thenReturn(true);

  }

  @Test
  public void smokeTest() throws Exception {
    setup();
    final DiffNode diff = new DiffNode();
    diff.addChild(new HasIcd9Node("100.1"));
    diff.addChild(new HasIcd9Node("100.2"));
    Assert.assertFalse(diff.evaluate(indices, patient1).toBooleanResult().result());
    Assert.assertTrue(diff.evaluate(indices, patient2).toBooleanResult().result());
  }
}
