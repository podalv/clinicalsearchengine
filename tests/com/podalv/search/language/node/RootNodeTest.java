package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.language.node.base.LimitNodeCorrectEvaluator;
import com.podalv.search.language.node.base.LimitNodeEvaluator;
import com.podalv.search.language.node.base.LimitNodeProcessedEvaluator;

public class RootNodeTest {

  @Test
  public void limitNode() throws Exception {
    final RootNode node = RootNode.parse("LIMIT(100, ICD9=250.50)", false, null);
    final LimitNodeEvaluator evaluator = node.getEvaluator(100, 1000);
    Assert.assertTrue(evaluator instanceof LimitNodeCorrectEvaluator);
    for (int x = 0; x < 99; x++) {
      Assert.assertFalse(evaluator.terminate(true));
    }
    Assert.assertTrue(evaluator.terminate(true));
  }

  @Test
  public void estimateNode() throws Exception {
    final RootNode node = RootNode.parse("ESTIMATE(100, ICD9=250.50)", false, null);
    final LimitNodeEvaluator evaluator = node.getEvaluator(100, 1000);
    Assert.assertTrue(evaluator instanceof LimitNodeCorrectEvaluator);
    for (int x = 0; x < 99; x++) {
      Assert.assertFalse(evaluator.terminate(true));
    }
    Assert.assertTrue(evaluator.terminate(true));
  }

  @Test
  public void cacheNode() throws Exception {
    final RootNode node = RootNode.parse("LIMIT(CACHE, ICD9=250.50)", false, null);
    final LimitNodeEvaluator evaluator = node.getEvaluator(100, 1000);
    Assert.assertTrue(evaluator instanceof LimitNodeProcessedEvaluator);
    for (int x = 0; x < 99; x++) {
      Assert.assertFalse(evaluator.terminate(true));
    }
    Assert.assertTrue(evaluator.terminate(true));
  }

  @Test
  public void simplification_none() throws Exception {
    Assert.assertEquals("INTERSECT(ICD9=250.50, ICD9=250)", RootNode.parse("INTERSECT(ICD9=250.50, ICD9=250)").toString());
    Assert.assertEquals("UNION(ICD9=250.50, ICD9=250)", RootNode.parse("UNION(ICD9=250.50, ICD9=250)").toString());
    Assert.assertEquals("OR(ICD9=250.50, ICD9=250)", RootNode.parse("OR(ICD9=250.50, ICD9=250)").toString());
    Assert.assertEquals("AND(ICD9=250.50, ICD9=250)", RootNode.parse("AND(ICD9=250.50, ICD9=250)").toString());
  }

  @Test
  public void no_direct_parent() throws Exception {
    Assert.assertEquals("INTERSECT(ICD9=250.50, ICD9=250, BEFORE(INTERSECT(ICD9=250.50, ICD9=250), CPT=20000*))", RootNode.parse(
        "INTERSECT(ICD9=250.50, ICD9=250, BEFORE(INTERSECT(ICD9=250.50, ICD9=250), CPT=20000*))").toString());
    Assert.assertEquals("UNION(ICD9=250.50, ICD9=250, BEFORE(UNION(ICD9=250.50, ICD9=250), CPT=20000*))", RootNode.parse(
        "UNION(ICD9=250.50, ICD9=250, BEFORE(UNION(ICD9=250.50, ICD9=250), CPT=20000*))").toString());
  }

  @Test
  public void direct_parent() throws Exception {
    Assert.assertEquals("INTERSECT(ICD9=250.50, ICD9=250)", RootNode.parse("INTERSECT(INTERSECT(ICD9=250.50, ICD9=250))").toString());
    Assert.assertEquals("UNION(ICD9=250.50, ICD9=250)", RootNode.parse("UNION(UNION(ICD9=250.50, ICD9=250))").toString());
    Assert.assertEquals("AND(ICD9=250.50, ICD9=250)", RootNode.parse("AND(AND(ICD9=250.50, ICD9=250))").toString());
    Assert.assertEquals("OR(ICD9=250.50, ICD9=250)", RootNode.parse("OR(OR(ICD9=250.50, ICD9=250))").toString());
  }

  @Test
  public void direct_parent_multiple() throws Exception {
    Assert.assertEquals("INTERSECT(ICD9=250.50, ICD9=250)", RootNode.parse("INTERSECT(INTERSECT(INTERSECT(ICD9=250.50, ICD9=250)))").toString());
    Assert.assertEquals("UNION(ICD9=250.50, ICD9=250)", RootNode.parse("UNION(UNION(UNION(ICD9=250.50, ICD9=250)))").toString());
    Assert.assertEquals("AND(ICD9=250.50, ICD9=250)", RootNode.parse("AND(AND(AND(ICD9=250.50, ICD9=250)))").toString());
    Assert.assertEquals("OR(ICD9=250.50, ICD9=250)", RootNode.parse("OR(OR(OR(ICD9=250.50, ICD9=250)))").toString());
  }

  @Test
  public void direct_parent_multiple_levels() throws Exception {
    Assert.assertEquals("INTERSECT(ICD9=200.60, ICD9=200.50, ICD9=250.50, ICD9=250)", RootNode.parse(
        "INTERSECT(INTERSECT(INTERSECT(ICD9=250.50, ICD9=250), ICD9=200.50), ICD9=200.60)").toString());
    Assert.assertEquals("UNION(ICD9=200.60, ICD9=200.50, ICD9=250.50, ICD9=250)", RootNode.parse(
        "UNION(UNION(UNION(ICD9=250.50, ICD9=250), ICD9=200.50), ICD9=200.60)").toString());
    Assert.assertEquals("AND(ICD9=200.60, ICD9=200.50, ICD9=250.50, ICD9=250)", RootNode.parse("AND(AND(AND(ICD9=250.50, ICD9=250), ICD9=200.50), ICD9=200.60)").toString());
    Assert.assertEquals("OR(ICD9=200.60, ICD9=200.50, ICD9=250.50, ICD9=250)", RootNode.parse("OR(OR(OR(ICD9=250.50, ICD9=250), ICD9=200.50), ICD9=200.60)").toString());
  }

}
