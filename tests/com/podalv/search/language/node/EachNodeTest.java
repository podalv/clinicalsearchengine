package com.podalv.search.language.node;

import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class EachNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void simplePatient() {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {3, 4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {5}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {50, 60, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {100, 101, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {105, 110, 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {30, 40, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2"), indices.getIcd9("100.3")}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(110);
  }

  @Test
  public void eachSimple() throws Exception {
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=BEFORE(ICD9=100.2, ABC*)+(END+40, END+60);" + //
        "   B=INTERSECT(EXTEND(A, END+40, END+60), ICD9=100.2);" + //
        "   IF EMPTY (B) {" + //
        "     EXIT;" + //
        "   }" + //
        "   RETURN ABC AS DEF;}\n" + //
        "DEF", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=BEFORE(ICD9=100.2, ABC*)+(END+40, END+60);" + //
        "   B=INTERSECT(EXTEND(A, END+40, END+60), ICD9=100.2);" + //
        "   IF EMPTY (B)" + //
        "   {" + //
        "        CONTINUE;" + //
        "    }" + //
        "    RETURN ABC AS DEF;}\n" + //
        "DEF", new int[] {50, 60});
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "  A=BEFORE(ICD9=100.2, ABC*)+(END+40, END+60);" + //
        "  B=INTERSECT(EXTEND(A, END+40, END+60), ICD9=100.2);" + //
        "  IF EMPTY (B) {" + //
        "    FAIL PATIENT;" + //
        "  }" + //
        "  RETURN ABC AS DEF;}\n" + //
        "DEF", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "  A=BEFORE(ICD9=100.2, ABC*)+(END+40, END+60);" + //
        "  B=INTERSECT(EXTEND(A, END+40, END+60), ICD9=100.2);" + //
        "  RETURN B AS DEF;}\n" + //
        "UNION(DEF)", new int[] {100, 101, 105, 110});
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "  A=BEFORE(ICD9=100.2, ABC*)+(END+40, END+60);" + //
        "  B=INTERSECT(EXTEND(A, END+40, END+60), ICD9=100.2);" + //
        "  EXIT;" + //
        "  RETURN B AS DEF;}\n" + //
        "UNION(DEF)", new int[] {});
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "  A=BEFORE(ICD9=100.2, ABC*)+(END+40, END+60);" + //
        "  B=INTERSECT(EXTEND(A, END+40, END+60), ICD9=100.2);" + //
        "  CONTINUE;" + //
        "  RETURN B AS DEF;}\n" + //
        "UNION(DEF)", new int[] {});
  }

  @Test
  public void nestedIf() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=BEFORE(ABC*, ICD9=100.2);" + //
        "   B=BEFORE(ICD9=100.3*, A);" + //
        "   IF EMPTY (A)" + //
        "   {" + //
        "        IF EMPTY(B) {" + //
        "            CONTINUE;" + //
        "        }" + //
        "    }" + //
        "    RETURN B AS DEF;}\n" + //
        "DEF", new int[] {30, 40});
  }

  @Test
  public void multipleIf() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=BEFORE(ABC*, ICD9=100.2);" + //
        "   B=BEFORE(ICD9=100.3*, A);" + //
        "   IF EMPTY (A)" + //
        "   {" + //
        "       CONTINUE;" + //
        "   }" + //
        "   IF EMPTY(B) {" + //
        "            CONTINUE;" + //
        "   }" + //
        "   RETURN B AS DEF;}\n" + //
        "DEF", new int[] {30, 40});
  }

  @Test
  public void multipleEach() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=BEFORE(ABC*, ICD9=100.2);" + //
        "   B=BEFORE(ICD9=100.3*, A);" + //
        "   IF EMPTY (A)" + //
        "   {" + //
        "       CONTINUE;" + //
        "   }" + //
        "   IF EMPTY(B) {" + //
        "            CONTINUE;" + //
        "   }" + //
        "   RETURN B AS DEF;}\n" + //
        "FOR EACH(ICD9=100.3) AS (D) {\n" + //
        "   A = BEFORE(D*, ICD9=100.2);\n" + //
        "   RETURN D AS XYZ;\n" + //
        "}" + //
        "INTERSECT(XYZ, DEF)", new int[] {30, 40});
  }

  @Test
  public void assignMainVariable() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=INTERVAL(START(TIMELINE), ABC);" + //
        "   B=DURATION(A, SINGLE, 20, MAX);" + //
        "   IF !EMPTY (B)" + //
        "   {" + //
        "       ABC = EXTEND(ABC, START-10, END);" + //
        "   }" + //
        "   RETURN ABC AS DEF;}\n" + //
        "DEF", new int[] {10, 20, 40, 60});
  }

  @Test
  public void reassignVariable() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=INTERVAL(START(TIMELINE), ABC);" + //
        "   A=DURATION(A, SINGLE, 20, MAX);" + //
        "   IF !EMPTY (A)" + //
        "   {" + //
        "       ABC = EXTEND(ABC, START-10, END);" + //
        "   }" + //
        "   RETURN ABC AS DEF;}\n" + //
        "DEF", new int[] {10, 20, 40, 60});
  }

  @Test
  public void ifEqualsVariable() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=INTERVAL(START(TIMELINE), ABC);" + //
        "   B=DURATION(A, SINGLE, 20, MAX);" + //
        "   IF !EMPTY (B)" + //
        "   {" + //
        "       A = EXTEND(ABC, START-10, END);" + //
        "   }" + //
        "   IF (A) == (ABC)" + //
        "   {" + //
        "       RETURN ABC AS DEF;" + //
        "   }" + //
        "   }\n" + //
        "DEF", new int[] {10, 20});
  }

  @Test
  public void printNode() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    //100.3 30-40
    simplePatient();
    BeforeNodeTest.evaluate(patient, indices, "" + //
        "FOR EACH(ICD9=100.1) AS (ABC) {" + //
        "   A=INTERVAL(START(TIMELINE), ABC);" + //
        "   B=DURATION(A, SINGLE, 20, MAX);" + //
        "   IF !EMPTY(TEMP) {" + //
        "      C = INTERVAL(END(TEMP), START(ABC));" + //
        "      PRINT(C);" + //
        "      CLEAR TEMP;" + //
        "   }" + //
        "   RETURN ABC AS TEMP;" + //
        "   RETURN C AS DEF;" + //
        "   PRINT(C + \" / \" + DEF);" + //
        "   }\n" + //
        "DEF", new int[] {20, 50});
  }
}
