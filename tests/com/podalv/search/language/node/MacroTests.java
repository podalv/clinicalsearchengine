package com.podalv.search.language.node;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class MacroTests {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void complexPatient() throws IOException {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {0, 1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5, 6}));
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.2"))).thenReturn(true);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.1"))).thenReturn(true);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.6"))).thenReturn(false);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.5"))).thenReturn(false);
    Mockito.when(patient.getStartTime()).thenReturn(Common.yearsToTime(10));
    Mockito.when(patient.getEndTime()).thenReturn(Common.yearsToTime(250));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(20), 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(50), Common.yearsToTime(90), 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(150), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(210), 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(180), Common.yearsToTime(250), 0}));

    Mockito.when(patient.getAgeRanges()).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), Common.yearsToTime(50), Common.yearsToTime(90),
        Common.yearsToTime(100), Common.yearsToTime(250)}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));

    indices.save(new StreamOutput(new ByteArrayOutputStream()));
  }

  public static void complexDeadPatient() throws IOException {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addDemographicRecord(new DemographicsRecord(0, "male", "white", "not hispanic", 365 * 100, -1));

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {0, 1, 2, 3}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {4, 5, 6}));
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.2"))).thenReturn(true);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.1"))).thenReturn(true);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.6"))).thenReturn(false);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.5"))).thenReturn(false);
    Mockito.when(patient.getStartTime()).thenReturn(Common.yearsToTime(10));
    Mockito.when(patient.getEndTime()).thenReturn(Common.yearsToTime(250));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(20), 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(50), Common.yearsToTime(90), 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(150), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(200), 0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(100), Common.yearsToTime(210), 0}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(180), Common.yearsToTime(250), 0}));

    Mockito.when(patient.getAgeRanges()).thenReturn(new IntArrayList(new int[] {Common.yearsToTime(10), Common.yearsToTime(30), Common.yearsToTime(50), Common.yearsToTime(90),
        Common.yearsToTime(100), Common.yearsToTime(250)}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
    indices.save(new StreamOutput(new ByteArrayOutputStream()));
  }

  private void assertAges(final String expression, final int ... expected) {
    final RootNode root = LanguageParser.parse(expression);
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);
    System.out.println(root.toString());

    final PayloadIterator p = result.iterator(patient);
    int pos = 0;
    if (expected == null || expected.length == 0) {
      Assert.assertFalse(p.hasNext());
    }
    else {
      Assert.assertTrue(p.hasNext());
      while (p.hasNext()) {
        p.next();
        Assert.assertEquals(Common.yearsToTime(expected[pos++]), p.getStartId());
        Assert.assertEquals(Common.yearsToTime(expected[pos++]), p.getEndId());
      }
      Assert.assertFalse(p.hasNext());
    }
  }

  @Test
  public void smokeTest() throws Exception {
    //10, 30, 50, 90, 100, 250
    complexDeadPatient();
    complexPatient();
    assertAges("AND(NOT(ICD9=100.6))", 10, 250);
    assertAges("AND(NOT(ICD9=100.2))");
    assertAges("AND(NEVER HAD(ICD9=100.2))");
    assertAges("AND(NEVER HAD(ICD9=100.6))", 10, 250);
    assertAges("AND(INVERT(INTERVAL(START(FIRST MENTION(ICD9=100.3)), END(TIMELINE))))", 10, 250);
    assertAges("AND(UNION(NOT(ICD9=100.3), INVERT(INTERVAL(START(FIRST MENTION(ICD9=100.3)), END(TIMELINE))))))", 10, 250);
    assertAges("AND(INTERVAL(START(FIRST MENTION(ICD9=100.3)), END(TIMELINE)))");
    assertAges("AND(NO HISTORY OF(ICD9=100.3))", 10, 250);
    assertAges("AND(NOT(ICD9=100.3))", 10, 250);
    assertAges("AND(HISTORY OF(ICD9=100.3))");
    assertAges("AND(INVERT(HISTORY OF(ICD9=100.3)))", 10, 250);
    assertAges("AND(RECORD  START)", 10, 10);
    assertAges("AND(RECORD_START)", 10, 10);
    assertAges("AND(RECORD END)", 250, 250);
    assertAges("AND(RECORD_END)", 250, 250);
    assertAges("AND(HISTORY_OF(ICD9=100.2))", 100, 250);
  }

}
