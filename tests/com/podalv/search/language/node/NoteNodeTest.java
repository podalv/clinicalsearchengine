package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.language.evaluation.iterators.NotesIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;

public class NoteNodeTest {

  @Test
  public void returns_uncompressed_data() throws Exception {
    final PatientSearchModel patient = Mockito.mock(PatientSearchModel.class);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {1, 10, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {1, 30, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {10, 10, 0}));
    final NotesIterator n1 = new NotesIterator(new RawPayloadIterator(patient, new IntArrayList(new int[] {1, 1, 2, 3})));
    final NotesIterator n2 = new NotesIterator(new RawPayloadIterator(patient, new IntArrayList(new int[] {1, 1, 3})));
    final NotesIterator result = NoteNode.intersect(patient, new IntArrayList(), n1, n2, Integer.MIN_VALUE);
    final IntArrayList out = new IntArrayList();
    while (result.hasNext()) {
      result.next();
      out.add(result.getStartId());
      out.add(result.getEndId());
      out.add(result.getNoteId());
    }
    Assert.assertArrayEquals(new int[] {1, 1, 10, 1, 1, 10, 10, 10, 10}, out.toArray());
  }
}
