package com.podalv.search.language.node;

import static com.podalv.search.language.node.BeforeNodeTest.evaluate;

import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class BeforeNodeContains {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void overlapPatient() {
    // 100.1 10-20
    // 100.2 12-18

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {12, 18, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  @Test
  public void innerIntervalStartEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 12-18
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+2, END-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+2, END-2)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, END)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+2, END-2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+2, END-2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+2, END-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+2, END-2)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, END)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+2, END-2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+2, END-2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+2, END-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+2, END-2)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, END)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+2, END-2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+2, END-2)", new int[] {});
  }

  @Test
  public void innerIntervalNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 11-17
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+3, END-3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+3, END-3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, END)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+2, END-2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+2, END-2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+2, END-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+2, END-2)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, END)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+2, END-2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+2, END-2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+2, END-2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+2, END-2)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, END)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+2, END-2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+2, END-2)", new int[] {});
  }

  @Test
  public void outerIntervalStartEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-2, END+2)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-2, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, END)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-2, END+2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-2, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-2, END+2)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-2, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, END)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-2, END+2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-2, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-2, END+2)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-2, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, END)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-2, END+2)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-2, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, END)", new int[] {});
  }

  @Test
  public void outerIntervalNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 11-19
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+1, END-1)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-1, END+1)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-1, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, END-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+1, END-1)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-1, END+1)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-1, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, END-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+1, END-1)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-1, END+1)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-1, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, END-1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+1, END-1)", new int[] {});
  }

  @Test
  public void outerIntervalBeforeNoStart() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 5-9
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-3, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-3, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-1, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START-1, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-3, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-3, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-1, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START-1, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-3, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-3, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-1, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START-1, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-3, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-3, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-1, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START-1, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-3, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-3, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-1, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START-1, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-3, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-3, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-1, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START-1, START-4)", new int[] {12, 18});
  }

  @Test
  public void outerIntervalBeforeWithStart() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 5-12
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-2, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-2, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-2, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-2, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-2, START-5)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-2, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-2, START-5)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-2, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-2, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-2, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-2, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-2, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START, START-4)", new int[] {12, 18});
  }

  @Test
  public void innerIntervalBeforeWithStart() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 5-12
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+2, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+2, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+2, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+2, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, START-5)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+2, START-4)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+2, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, START-5)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+2, START-4)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+2, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+2, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+2, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+2, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+2, START-4)", new int[] {12, 18});
  }

  @Test
  public void innerIntervalBeforeNoStart() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 5-11
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-1, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START-1, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(START+1, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-1, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(START-1, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(START+1, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-1, START-5)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(START-1, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(START+1, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-1, START-5)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(START-1, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(START+1, START-4)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-1, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(START-1, START-5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, START-4)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(START+1, START-4)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-1, START-5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(START-1, START-5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, START-4)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(START+1, START-4)", new int[] {12, 18});
  }

  @Test
  public void innerIntervalAfterNoEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 13-23
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END+1, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END+1, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+3, END+8)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+3, END+8)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END+1, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END+1, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+3, END+8)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+3, END+8)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END+1, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END+1, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+3, END+8)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+3, END+8)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END+1, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END+1, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+3, END+8)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+3, END+8)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END+1, END+5)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END+1, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+3, END+8)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+3, END+8)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END+1, END+5)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END+1, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+3, END+8)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+3, END+8)", new int[] {12, 18});
  }

  @Test
  public void innerIntervalAfterWithEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 12-23
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END-2, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END-2, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END-2, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END-2, END)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END-2, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END-2, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END-2, END)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END-2, END)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END, END+5)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END-2, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END-2, END)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END, END+5)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END-2, END)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END-2, END)", new int[] {});
  }

  @Test
  public void outerIntervalAfterNoEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 21-25
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END+3, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END+3, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+3, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END+3, END+3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END+3, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END+3, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+3, END+3)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END+3, END+3)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END+3, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END+3, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+3, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END+3, END+3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END+3, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END+3, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+3, END+3)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END+3, END+3)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END+3, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END+3, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+3, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END+3, END+3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END+3, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END+3, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+3, END+3)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END+3, END+3)", new int[] {12, 18});
  }

  @Test
  public void outerIntervalAfterWithEnd() throws Exception {
    //100.1 10-20
    //100.2 12-18
    //INTERVAL = 20-25
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END+2, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END+2, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<>(END, END+3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END+2, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<>(END+2, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END, END+3)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<>(END, END+3)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END+2, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<(END+2, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+<(END, END+3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END+2, END+5)", new int[] {12, 18});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-<(END+2, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END, END+3)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)-<(END, END+3)", new int[] {12, 18});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END+2, END+5)", new int[] {12, 18});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+>(END+2, END+5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END, END+3)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+>(END, END+3)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END+2, END+5)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)->(END+2, END+5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END, END+3)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)->(END, END+3)", new int[] {12, 18});
  }

}
