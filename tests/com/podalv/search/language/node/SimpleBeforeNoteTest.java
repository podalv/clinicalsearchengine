package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class SimpleBeforeNoteTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void simplePatient() {
    // 100.1 10-20
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {3, 2}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {1}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {15, 30, 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));

  }

  private static void complexPatient() {
    // 100.1 10-20 30-50 150-200
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {3, 2}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {1}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {15, 30, 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  private static void evaluate(final PatientSearchModel patient, final IndexCollection indices, final String expression, final int[] expected) {
    final RootNode node = LanguageParser.parse(expression);
    final TimeIntervals intervals = node.evaluate(indices, patient).toTimeIntervals(patient);
    int cnt = 0;
    final PayloadIterator iterator = intervals.iterator(patient);
    while (iterator.hasNext()) {
      iterator.next();
      Assert.assertEquals(expected[cnt++], iterator.getStartId());
      Assert.assertEquals(expected[cnt++], iterator.getEndId());
    }
    Assert.assertEquals(expected.length, cnt);
  }

  @Test
  public void smokeTest() throws Exception {
    simplePatient();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)", new int[] {10, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)", new int[] {});

    complexPatient();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)", new int[] {10, 30});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)", new int[] {});
  }
}
