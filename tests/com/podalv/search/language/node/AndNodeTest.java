package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.EvaluationResult;

public class AndNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void simplePatient() {
    // 100.1 10-20
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.1"))).thenReturn(true);
    Mockito.when(patient.containsIcd9(indices.getIcd9("100.2"))).thenReturn(true);
    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {1, 2}));
  }

  @BeforeClass
  public static void setup() {
    simplePatient();
  }

  @Test
  public void smokeTest_true() throws Exception {
    final AndNode node = new AndNode();
    node.addChild(new HasIcd9Node("100.1"));
    node.addChild(new HasIcd9Node("100.2"));
    final EvaluationResult result = node.evaluate(indices, patient);
    Assert.assertTrue(result.toBooleanResult().result());
  }

  @Test
  public void smokeTest_false() throws Exception {
    final AndNode node = new AndNode();
    node.addChild(new HasIcd9Node("100.2"));
    node.addChild(new HasIcd9Node("100.3"));
    final EvaluationResult result = node.evaluate(indices, patient);
    Assert.assertFalse(result.toBooleanResult().result());
  }

  @Test
  public void not_node() throws Exception {
    final AndNode node = new AndNode();
    node.addChild(new HasIcd9Node("100.2"));
    final NotNode not = new NotNode();
    not.addChild(new HasIcd9Node("100.3"));
    node.addChild(not);
    final EvaluationResult result = node.evaluate(indices, patient);
    Assert.assertTrue(result.toBooleanResult().result());
  }

  @Test
  public void equalsTest() throws Exception {
    final AndNode node1 = new AndNode();
    node1.addChild(new HasIcd9Node("250.50"));
    node1.addChild(new HasCptNode("25050"));
    final IntersectNode node2 = new IntersectNode();
    node2.addChild(new HasIcd9Node("250.50"));
    node2.addChild(new HasCptNode("25050"));

    Assert.assertEquals(node1, node2);
  }

  @Test
  public void equalsOrTest() throws Exception {
    final UnionNode node1 = new UnionNode();
    node1.addChild(new HasIcd9Node("250.50"));
    node1.addChild(new HasCptNode("25050"));
    final OrNode node2 = new OrNode();
    node2.addChild(new HasIcd9Node("250.50"));
    node2.addChild(new HasCptNode("25050"));

    Assert.assertEquals(node1, node2);
  }

}
