package com.podalv.search.language.node;

import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class NegationTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  public static void cptMissingPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getStartTime()).thenReturn(5);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
    Mockito.when(patient.getUniqueCptCodes()).thenReturn(new IntArrayList());
    Mockito.when(patient.containsFamilyHistoryTerms()).thenReturn(false);
    Mockito.when(patient.containsPositiveTerms()).thenReturn(false);
    Mockito.when(patient.containsNegatedTerms()).thenReturn(false);
  }

  public static void icd9MissingPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addCptCode("45000");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getCpt(indices.getCpt("45000"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getStartTime()).thenReturn(5);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 0}));

    Mockito.when(patient.getUniqueCptCodes()).thenReturn(new IntArrayList(new int[] {indices.getCpt("45000")}));
    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList());
  }

  @Test
  public void cpt_notNode() throws Exception {
    cptMissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "NOT(CPT=45000)", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "NOT(CPT)", new int[] {5, 10});
  }

  @Test
  public void cpt_invertNode() throws Exception {
    cptMissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "INVERT(CPT=45000)", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "INVERT(CPT)", new int[] {5, 10});
  }

  @Test
  public void cpt_beforeNode() throws Exception {
    cptMissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "BEFORE(CPT=45000, ICD9=100.1*)-(0, 0)", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "BEFORE(CPT, ICD9=100.1*)-(0, 0)", new int[] {5, 10});
  }

  @Test
  public void icd9_notNode() throws Exception {
    icd9MissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "NOT(ICD9=100.1)", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "NOT(ICD9)", new int[] {5, 10});
  }

  @Test
  public void icd9_invertNode() throws Exception {
    icd9MissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "INVERT(ICD9=100.1)", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "INVERT(ICD9)", new int[] {5, 10});
  }

  @Test
  public void icd9_beforeNode() throws Exception {
    icd9MissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "BEFORE(ICD9=100.1, CPT=45000*)-(0, 0)", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "BEFORE(ICD9, CPT=45000*)-(0, 0)", new int[] {5, 10});
  }

  @Test
  public void textNode() throws Exception {
    cptMissingPatient();
    BeforeNodeTest.evaluate(patient, indices, "NOTE(NOT(TEXT=\"AAA\"))", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "NOTE(NOT(!TEXT=\"AAA\"))", new int[0]);
    BeforeNodeTest.evaluate(patient, indices, "NOTE(NOT(~TEXT=\"AAA\"))", new int[0]);
  }

}
