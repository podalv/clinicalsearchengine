package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class TimeNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void simplePatient() {
    // 100.1 10-20
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(200);

  }

  private static void complexPatient() {
    // 100.1 10-20
    // 100.3 15-25
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");
    indices.addIcd9Code("100.3");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.3"))).thenReturn(new IntArrayList(new int[] {3}));
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {15, 25, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(200);

  }

  private static void multipleIntervalsPatient() {
    // 100.1 10-20
    // 100.2 100-200, 1000-2000

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2, 3}));
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {1000, 2000, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(2000);

  }

  static void evaluate(final PatientSearchModel patient, final IndexCollection indices, final String expression, final int[] expected) {
    final RootNode node = LanguageParser.parse(expression, false);
    final TimeIntervals intervals = node.evaluate(indices, patient).toTimeIntervals(patient);
    int cnt = 0;
    final PayloadIterator iterator = intervals.iterator(patient);
    while (iterator.hasNext()) {
      iterator.next();
      Assert.assertEquals(expected[cnt++], iterator.getStartId());
      Assert.assertEquals(expected[cnt++], iterator.getEndId());
    }
    Assert.assertEquals(expected.length, cnt);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void simpleBefore() throws Exception {
    // 100.1 10-20
    // 100.2 100-200
    simplePatient();
    evaluate(patient, indices, "(ICD9=100.1) BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.2) BEFORE ICD9=100.1*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.1*) BEFORE ICD9=100.2", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.1) 90 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.1) 80 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.1) 78 MINUTES BEFORE ICD9=100.2*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.1) 90 MINUTES* BEFORE ICD9=100.2", new int[] {9, 99});
    evaluate(patient, indices, "(ICD9=100.1) 80 MINUTES* BEFORE ICD9=100.2", new int[] {19, 99});
    evaluate(patient, indices, "(ICD9=100.1) 78 MINUTES* BEFORE ICD9=100.2", new int[] {});
    evaluate(patient, indices, "NO (ICD9=100.1) BEFORE ICD9=100.2*", new int[] {});
    evaluate(patient, indices, "NO (ICD9=100.2) BEFORE ICD9=100.1*", new int[] {10, 20});
    evaluate(patient, indices, "NO (ICD9=100.1) BEFORE ICD9=100.2*", new int[] {});
    evaluate(patient, indices, "NO (ICD9=100.1) 90 MINUTES* BEFORE ICD9=100.2", new int[] {});
    evaluate(patient, indices, "NO (ICD9=100.1) 80 MINUTES* BEFORE ICD9=100.2", new int[] {});
    evaluate(patient, indices, "NO (ICD9=100.1) 78 MINUTES* BEFORE ICD9=100.2", new int[] {21, 99});
    evaluate(patient, indices, "NO (ICD9=100.1) 78 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "NO (ICD9=100.3) BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "NO (ICD9=100.3) 10 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.1) AND NO (ICD9=100.3) BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.1*) AND NO (ICD9=100.3) BEFORE ICD9=100.2", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.1*) AND NO (ICD9=100.3) 100 MINUTES BEFORE ICD9=100.2", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.1) BEFORE ICD9=100.2", new int[] {100, 200});
    Assert.fail("Should have thrown exception");
  }

  @Test
  public void complexBefore() throws Exception {
    // 100.1 10-20
    // 100.3 15-25
    // 100.2 100-200
    complexPatient();
    evaluate(patient, indices, "(ICD9=100.1 AND ICD9=100.3) BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.1* AND ICD9=100.3) BEFORE ICD9=100.2", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.1 AND ICD9=100.3*) BEFORE ICD9=100.2", new int[] {15, 25});
    evaluate(patient, indices, "(ICD9=100.1* AND ICD9=100.3*) BEFORE ICD9=100.2", new int[] {10, 20, 15, 25});
    evaluate(patient, indices, "(ICD9=100.1* AND ICD9=100.3* AND ICD9=100.4) BEFORE ICD9=100.2", new int[] {});
    evaluate(patient, indices, "(ICD9=100.1) AND NO (ICD9=100.3) BEFORE ICD9=100.2*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.3) AND NO (ICD9=100.1) 78 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.3) AND NO (ICD9=100.1) 80 MINUTES BEFORE ICD9=100.2*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.3 AND ICD9=100.1) 80 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});

    evaluate(patient, indices, "(ICD9=100.2) AFTER ICD9=100.1*", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.2*) AFTER ICD9=100.1", new int[] {100, 200});
    evaluate(patient, indices, "(ICD9=100.3) AFTER ICD9=100.1*", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.3*) AFTER ICD9=100.1", new int[] {15, 25});
    evaluate(patient, indices, "(ICD9=100.2*) AND NO (ICD9=100.3) AFTER ICD9=100.1", new int[] {});
    evaluate(patient, indices, "(ICD9=100.3*) AND NO (ICD9=100.2) 30 MINUTES AFTER ICD9=100.1", new int[] {15, 25});
  }

  @Test
  public void multipleBefore() throws Exception {
    // 100.1 10-20
    // 100.2 100-200 1000-2000
    multipleIntervalsPatient();
    evaluate(patient, indices, "(ICD9=100.1) BEFORE ICD9=100.2*", new int[] {100, 200, 1000, 2000});
    evaluate(patient, indices, "(ICD9=100.1) 100 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});
    evaluate(patient, indices, "(START(ICD9=100.1)) 80 MINUTES BEFORE ICD9=100.2*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.1) 80 MINUTES BEFORE ICD9=100.2*", new int[] {100, 200});

    evaluate(patient, indices, "(ICD9=100.2) AFTER ICD9=100.1*", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.2*) AFTER ICD9=100.1", new int[] {100, 200, 1000, 2000});
    evaluate(patient, indices, "(ICD9=100.2) AFTER ICD9=100.3*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.2*) AND NO(ICD9=100.3) AFTER ICD9=100.1", new int[] {100, 200, 1000, 2000});
    evaluate(patient, indices, "(ICD9=100.2) 89 MINUTES AFTER ICD9=100.1*", new int[] {});
    evaluate(patient, indices, "(ICD9=100.2*) 89 MINUTES AFTER ICD9=100.1", new int[] {});
    evaluate(patient, indices, "(ICD9=100.2) 99 MINUTES AFTER ICD9=100.1*", new int[] {10, 20});
    evaluate(patient, indices, "(ICD9=100.2*) 99 MINUTES AFTER ICD9=100.1", new int[] {100, 200});
  }

}