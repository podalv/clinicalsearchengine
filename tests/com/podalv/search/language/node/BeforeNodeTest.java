package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class BeforeNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void simplePatient() {
    // 100.1 10-20
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(200);

  }

  private static void complexPatient1() {
    // 100.1 10-20 30-40 50-60 70-80
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {5, 2, 3, 4}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {1}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {30, 40, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {50, 60, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {70, 80, 0}));

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(200);

  }

  private static void simplePatientYears() {
    // 100.1 10-20
    // 100.2 100-200

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10 * 365 * 24 * 60, 20 * 365 * 24 * 60, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {100 * 365 * 24 * 60, 200 * 365 * 24 * 60, 0}));
    Mockito.when(patient.getStartTime()).thenReturn(10 * 365 * 24 * 60);
    Mockito.when(patient.getEndTime()).thenReturn(200 * 365 * 24 * 60);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));

  }

  private static void containsPatient() {
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {3, 2}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {1}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {110, 120, 0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {100, 200, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {130, 250, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(100);
    Mockito.when(patient.getEndTime()).thenReturn(250);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  private static void singleIntervalPatient() {
    //100.1 10-20
    //100.2 - missing
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(20);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void complexPatient() {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1, 2}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {3, 4}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {50, 60, 0}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {100, 101, 0}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {105, 110, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(110);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  public static void evaluate(final PatientSearchModel patient, final IndexCollection indices, final String expression, final int[] expected) {
    final RootNode node = LanguageParser.parse(expression, false);
    final EvaluationResult result = node.evaluate(indices, patient);
    if (result instanceof AbortedResult) {
      Assert.assertEquals(0, expected.length);
      return;
    }
    final TimeIntervals intervals = result.toTimeIntervals(patient);
    int cnt = 0;
    final PayloadIterator iterator = intervals.iterator(patient);
    while (iterator.hasNext()) {
      iterator.next();
      Assert.assertEquals(expected[cnt++], iterator.getStartId());
      Assert.assertEquals(expected[cnt++], iterator.getEndId());
    }
    Assert.assertEquals(expected.length, cnt);
  }

  @Test
  public void positive_before() throws Exception {
    simplePatient();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -200)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-10, -1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-1, -10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-10, MIN)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -89)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, MAX)", new int[] {10, 20});

    singleIntervalPatient();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -10)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+(MIN, -10)", new int[] {});
  }

  @Test
  public void positive_before_years() throws Exception {
    simplePatientYears();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-10 YEARS, -1 YEAR)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -10 YEARS)", new int[] {Common.yearsToTime(10), Common.yearsToTime(20)});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -89 YEAR)", new int[] {Common.yearsToTime(10), Common.yearsToTime(20)});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -200 YEAR)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-200 YEAR, MIN)", new int[] {});
  }

  @Test
  public void positive_in() throws Exception {
    simplePatient();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-150, -5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-150, -111)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-10, -5)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-90, -5)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-5, -90)", new int[] {10, 20});

    singleIntervalPatient();
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-10, 50)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)+(-10, -5)", new int[] {});
  }

  @Test
  public void negative() throws Exception {
    simplePatient();
    // 100.1 10-20
    // 100.2 100-200
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-(-10, -1)", new int[] {100, 200});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)-*(-10, -1)", new int[] {90, 99});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-10, -1)", new int[] {10, 20});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-1, -10)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-10, -1)+<>(-100, -70)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-1, -10)+<>(-70, -100)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-10, -1)+<>(-100, -90)", new int[] {});
  }

  @Test
  public void complexEvaluation() throws Exception {
    //100.1 10-20 50-60
    //100.2 100-101 105-110
    complexPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)-*(-98, -95)", new int[] {2, 5});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-98, -95)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)-*(-95, -98)", new int[] {2, 5});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-95, -98)", new int[] {10, 20});

  }

  @Test
  public void contains() throws Exception {
    //100.1 = 110-120
    //100.2 = 100-200, 130-250
    containsPatient();
    evaluate(patient, indices, "CONTAINS(ICD9=100.1*, ICD9=100.2)", new int[] {});
    evaluate(patient, indices, "CONTAINS(ICD9=100.1, ICD9=100.2*)", new int[] {});
    evaluate(patient, indices, "CONTAINS(ICD9=100.1*, ICD9=100.2*)", new int[] {});
    evaluate(patient, indices, "CONTAINS(ICD9=100.2*, ICD9=100.1)", new int[] {100, 200});
    evaluate(patient, indices, "CONTAINS(ICD9=100.2, ICD9=100.1*)", new int[] {110, 120});
    evaluate(patient, indices, "CONTAINS(ICD9=100.2*, ICD9=100.1*)", new int[] {100, 200});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(START, END)", new int[] {110, 120});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+<>(END, START)", new int[] {110, 120});
  }

  @Test
  public void negativeBefore() throws Exception {
    // 100.1 10-20 30-40 50-60 70-80
    // 100.2 100-200

    complexPatient1();
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)-*(-50, -1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-(-50, -1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-50, -1)", new int[] {});
    //evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-19, -1)", new int[] {10, 20, 30, 40, 50, 60, 70, 80});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-19, -1)+(MIN, -51)", new int[] {10, 20, 30, 40});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-(-10, -1)", new int[] {100, 200});
  }

  @Test
  public void negativeNeverBefore() throws Exception {
    // 100.1 10-20 30-40 50-60 70-80
    // 100.2 100-200

    complexPatient1();
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)+<>*(-31, -1)", new int[] {69, 99});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2*)+<>*(-31, -1)+*(-81, -91)", new int[] {9, 20, 69, 99, 100, 200});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)-(-10, -1)+(-30, -1)+(-50, -40)", new int[] {50, 60, 70, 80});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-(-10, -1)", new int[] {100, 200});
    evaluate(patient, indices, "BEFORE(ICD9=100.3, ICD9=100.2*)-(-10, -1)", new int[] {100, 200});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)-*(-10, -1)", new int[] {90, 99});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)+*(-10, -1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)+*(-20, -1)", new int[] {80, 99});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2)-(-10, -1)+*(-20, -1)", new int[] {80, 99});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)+(-10, -1)+(-20, -1)", new int[] {});
    evaluate(patient, indices, "INTERSECT(NOT(INTERSECT(BEFORE(ICD9=100.1, ICD9=100.2)-*(-10, -1), ICD9=100.1)), BEFORE(ICD9=100.1, ICD9=100.2)-*(-10, -1))", new int[] {90, 99});
    evaluate(patient, indices, "INTERSECT(NOT(INTERSECT(BEFORE(ICD9=100.1, ICD9=100.2)-*(-50, -1), ICD9=100.1)), BEFORE(ICD9=100.1, ICD9=100.2)-*(-50, -1))", new int[] {});
  }

  @Test
  public void missingLeft() throws Exception {
    // 100.1 10-20 30-40 50-60 70-80
    // 100.2 100-200

    complexPatient1();
    evaluate(patient, indices, "BEFORE(ICD9=100.3, ICD9=100.2*)-(-10, -1)", new int[] {100, 200});
    evaluate(patient, indices, "BEFORE(ICD9=100.3, ICD9=100.2*)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.3*, ICD9=100.2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.3)-*(-10, -1)", new int[] {});
  }

}