package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class UnionNodeTest {

  private int[] union(final PayloadIterator i1, final PayloadIterator i2) {
    final IntArrayList instance = new IntArrayList();
    final PayloadIterator pi = UnionNode.union(instance, i1, i2);
    final IntArrayList result = new IntArrayList();
    while (pi.hasNext()) {
      pi.next();
      result.add(pi.getStartId());
      result.add(pi.getEndId());
    }
    return result.toArray();
  }

  @Test
  public void identical() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 10}, union(IntersectNodeTest.getValues(5, 10), IntersectNodeTest.getValues(5, 10)));
  }

  @Test
  public void overlap() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 12}, union(IntersectNodeTest.getValues(5, 12), IntersectNodeTest.getValues(5, 10)));
  }

  @Test
  public void non_overlap() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 8, 10, 12}, union(IntersectNodeTest.getValues(5, 8), IntersectNodeTest.getValues(10, 12)));
  }

  @Test
  public void overlap_single_point() throws Exception {
    Assert.assertArrayEquals(new int[] {5, 12}, union(IntersectNodeTest.getValues(5, 8), IntersectNodeTest.getValues(8, 12)));
  }

  @Test
  public void multiple_non_overlaps() throws Exception {
    Assert.assertArrayEquals(new int[] {1, 3, 5, 6, 7, 8, 10, 15, 16, 20}, union(IntersectNodeTest.getValues(5, 6, 10, 15), IntersectNodeTest.getValues(1, 3, 7, 8, 16, 20)));
  }

  @Test
  public void multiple_some_overlaps() throws Exception {
    Assert.assertArrayEquals(new int[] {1, 3, 5, 6, 7, 8, 10, 15, 16, 20}, union(IntersectNodeTest.getValues(5, 6, 10, 15), IntersectNodeTest.getValues(1, 3, 5, 6, 7, 8, 16, 20)));
  }

}
