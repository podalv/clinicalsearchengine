package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.language.script.LanguageParser;

public class IdenticalNodeTest {

  @Test
  public void smokeTest() throws Exception {
    Assert.assertTrue(LanguageParser.parse("IDENTICAL(INTERVAL(100, 200), INTERVAL(100, 200))").evaluate(null, null).toBooleanResult().result());
    Assert.assertFalse(LanguageParser.parse("IDENTICAL(INTERVAL(99, 200), INTERVAL(100, 200))").evaluate(null, null).toBooleanResult().result());
    Assert.assertFalse(LanguageParser.parse("IDENTICAL(UNION(INTERVAL(100, 200), INTERVAL(250, 300)), INTERVAL(100, 200))").evaluate(null, null).toBooleanResult().result());
    Assert.assertFalse(LanguageParser.parse("IDENTICAL(INTERVAL(100, 200), UNION(INTERVAL(100, 200), INTERVAL(250, 300)))").evaluate(null, null).toBooleanResult().result());
    Assert.assertTrue(LanguageParser.parse("IDENTICAL(NULL, NULL)").evaluate(null, null).toBooleanResult().result());
    Assert.assertFalse(LanguageParser.parse("IDENTICAL(NULL, INTERVAL(100, 200))").evaluate(null, null).toBooleanResult().result());
  }
}
