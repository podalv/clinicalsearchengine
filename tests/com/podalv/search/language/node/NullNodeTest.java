package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;

public class NullNodeTest {

  @Test
  public void smokeTest() throws Exception {
    final NullNode node = new NullNode();
    Assert.assertFalse(node.evaluate(null, null).toTimeIntervals(null).iterator(null).hasNext());
  }
}
