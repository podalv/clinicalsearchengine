package com.podalv.search.language.node;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.LabsOffHeapData;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.script.LanguageParser;

public class LabsNodeTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;
  private static Statistics         statistics;

  public static void complexPatient() {
    //100.1 10-20 10-30 50-90 150-200
    //100.2 100-200 100-210 180-250

    indices = Mockito.mock(IndexCollection.class);
    Mockito.when(indices.getLabsCode("wbc")).thenReturn(1);

    statistics = Mockito.mock(Statistics.class);
    Mockito.when(statistics.getLabs(1)).thenReturn(1);
    Mockito.when(statistics.getLabsPatients(1)).thenReturn(new IntArrayCloneableIterator(new IntArrayList(new int[] {1})));

    patient = Mockito.mock(PatientSearchModel.class);

    Mockito.when(indices.getLabValueId("HIGH")).thenReturn(0);
    Mockito.when(indices.getLabValueId("LOW")).thenReturn(1);
    Mockito.when(indices.getLabValueId("NORMAL")).thenReturn(2);
    Mockito.when(indices.getLabValueId("UNDEFINED")).thenReturn(3);

    Mockito.when(indices.getLabValueText(0)).thenReturn("HIGH");
    Mockito.when(indices.getLabValueText(1)).thenReturn("LOW");
    Mockito.when(indices.getLabValueText(2)).thenReturn("NORMAL");
    Mockito.when(indices.getLabValueText(3)).thenReturn("UNDEFINED");

    final LabsOffHeapData labs = Mockito.mock(LabsOffHeapData.class);
    Mockito.when(labs.get(1, indices.getLabValueId("HIGH"))).thenReturn(new IntArrayList(new int[] {1, 2, 3}));
    Mockito.when(labs.get(1, indices.getLabValueId("LOW"))).thenReturn(new IntArrayList(new int[] {4}));
    Mockito.when(labs.get(1, indices.getLabValueId("NORMAL"))).thenReturn(new IntArrayList(new int[] {5, 6}));
    Mockito.when(labs.get(1, indices.getLabValueId("UNDEFINED"))).thenReturn(new IntArrayList(new int[] {7}));
    Mockito.when(patient.getLabs()).thenReturn(labs);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {100}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {200}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {300}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {50}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {250}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {350}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {10}));
  }

  private void assertAges(final String expression, final int ... expected) {
    final RootNode root = LanguageParser.parse(expression);
    final TimeIntervals result = ((LanguageNodeWithChildren) root.children.get(0)).children.get(0).evaluate(indices, patient).toTimeIntervals(patient);

    final PayloadIterator p = result.iterator(patient);
    int pos = 0;
    if (expected == null || expected.length == 0) {
      Assert.assertFalse(p.hasNext());
    }
    else {
      Assert.assertTrue(p.hasNext());
      while (p.hasNext()) {
        p.next();
        Assert.assertEquals(expected[pos++], p.getStartId());
        Assert.assertEquals(expected[pos++], p.getEndId());
      }
      Assert.assertFalse(p.hasNext());
    }
  }

  @Test
  public void smokeTest() throws Exception {
    complexPatient();
    assertAges("AND(LABS(\"WBC\", \"HIGH\"))", 100, 100, 200, 200, 300, 300);
    assertAges("AND(LABS(\"WBC\", \"LOW\"))", 50, 50);
    assertAges("AND(LABS(\"WBC\", \"NORMAL\"))", 250, 250, 350, 350);
    assertAges("AND(LABS(\"WBC\", \"UNDEFINED\"))", 10, 10);
  }
}