package com.podalv.search.language.node;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayList;

public class PatientsNodeTest {

  @Test
  public void constructor_sorted_1() throws Exception {
    final ArrayList<String> patientIds = new ArrayList<>();
    patientIds.add("4");
    patientIds.add("11");
    patientIds.add("1");
    patientIds.add("3");
    patientIds.add("2");
    final PatientsNode node = new PatientsNode(patientIds);
    Assert.assertArrayEquals(new int[] {1, 2, 3, 4, 11}, node.getPatientListCopy());
  }

  @Test
  public void constructor_sorted_2() throws Exception {
    final IntArrayList patientIds = new IntArrayList();
    patientIds.add(4);
    patientIds.add(11);
    patientIds.add(1);
    patientIds.add(3);
    patientIds.add(2);
    final PatientsNode node = new PatientsNode(patientIds);
    Assert.assertArrayEquals(new int[] {1, 2, 3, 4, 11}, node.getPatientListCopy());
  }

  @Test
  public void constructor_sorted_3() throws Exception {
    final PatientsNode node = new PatientsNode("4, 11, 1, 3, 2");
    Assert.assertArrayEquals(new int[] {1, 2, 3, 4, 11}, node.getPatientListCopy());
  }

}
