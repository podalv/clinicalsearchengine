package com.podalv.search.language.node;

import static com.podalv.search.language.node.BeforeNodeTest.evaluate;

import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

public class BeforeNodeOverlapTest {

  private static PatientSearchModel patient;
  private static IndexCollection    indices;

  private static void overlapPatient() {
    // 100.1 10-20
    // 100.2 10-20

    indices = new IndexCollection(Mockito.mock(UmlsDictionary.class));
    indices.addIcd9Code("100.1");
    indices.addIcd9Code("100.2");

    patient = Mockito.mock(PatientSearchModel.class);
    Mockito.when(patient.getIcd9(indices.getIcd9("100.1"))).thenReturn(new IntArrayList(new int[] {1}));
    Mockito.when(patient.getIcd9(indices.getIcd9("100.2"))).thenReturn(new IntArrayList(new int[] {2}));

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {10, 20, 0}));

    Mockito.when(patient.getStartTime()).thenReturn(10);
    Mockito.when(patient.getEndTime()).thenReturn(10);

    Mockito.when(patient.getUniqueIcd9Codes()).thenReturn(new IntArrayList(new int[] {indices.getIcd9("100.1"), indices.getIcd9("100.2")}));
  }

  @Test
  public void overlapBefore() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START+1, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, END)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START+1, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, END)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START+1, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, END)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START+1, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, END)", new int[] {});

    evaluate(patient, indices, "RETURN ICD9=100.1 INTERSECTING ICD9=100.2", new int[] {10, 20});
    evaluate(patient, indices, "RETURN ICD9=100.2 INTERSECTING ICD9=100.1", new int[] {10, 20});
  }

  @Test
  public void overlapAfter() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END-1)", new int[] {10, 20});
  }

  @Test
  public void overlapWithin() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START+1, END-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START+1, END-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START+1, END-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START+1, END-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START+1, END-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START+1, END-1)", new int[] {10, 20});
  }

  @Test
  public void overlapLeftRight() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-1, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-1, END+1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-1, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-1, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-1, END+1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-1, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-1, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-1, END+1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-1, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-1, END+1)", new int[] {});
  }

  @Test
  public void overlapLeftContainsStart() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-5, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-5, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-5, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-5, START)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-5, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-5, START)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-5, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-5, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-5, START)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-5, START)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-5, START)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-5, START)", new int[] {10, 20});
  }

  @Test
  public void overlapLeftNoStart() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START-5, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START-5, START-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START-5, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START-5, START-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START-5, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START-5, START-1)", new int[] {10, 20});
  }

  @Test
  public void overlapRightContainsEnd() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END, END+1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END, END+1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END, END+1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END, END+1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END, END+1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END, END+1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END, END+1)", new int[] {});
  }

  @Test
  public void overlapLeftRightNoStartNoEnd() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END+1, END+2)+<>(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+1, END+2)+<>(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END+1, END+2)-<>(START-5, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+1, END+2)-<>(START-5, START-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END+1, END+2)+<(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+1, END+2)+<(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END+1, END+2)-<(START-5, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+1, END+2)-<(START-5, START-1)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END+1, END+2)+>(START-5, START-1)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+1, END+2)+>(START-5, START-1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END+1, END+2)->(START-5, START-1)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+1, END+2)->(START-5, START-1)", new int[] {10, 20});
  }

  @Test
  public void overlapRightNoEnd() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(END+1, END+2)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(END+1, END+2)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(END+1, END+2)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(END+1, END+2)", new int[] {10, 20});
  }

  @Test
  public void overlap() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.1*, ICD9=100.2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2*, ICD9=100.1)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, END)", new int[] {10, 20});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END)", new int[] {10, 20});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, END)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END)", new int[] {});
  }

  @Test
  public void overlapConflicting() throws Exception {
    //100.1 10-20
    //100.2 10-20
    overlapPatient();

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<(START, END)+<(START-5, START-1)+<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<(START, END)+<(START-5, START-1)+<(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+>(START, END)+>(START-5, START-1)+>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+>(START, END)+>(START-5, START-1)+>(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)+<>(START, END)+<>(START-5, START-1)+<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)+<>(START, END)+<>(START-5, START-1)+<>(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<(START, END)-<(START-5, START-1)-<(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<(START, END)-<(START-5, START-1)-<(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)->(START, END)->(START-5, START-1)->(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)->(START, END)->(START-5, START-1)->(END+1, END+2)", new int[] {});

    evaluate(patient, indices, "BEFORE(ICD9=100.1, ICD9=100.2*)-<>(START, END)-<>(START-5, START-1)-<>(END+1, END+2)", new int[] {});
    evaluate(patient, indices, "BEFORE(ICD9=100.2, ICD9=100.1*)-<>(START, END)-<>(START-5, START-1)-<>(END+1, END+2)", new int[] {});
  }

}
