package com.podalv.search.language;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.language.script.LanguageParser;

public class LanguageToStringTest {

  @Test
  public void ageNode() throws Exception {
    Assert.assertEquals("AGE(100, 200)", LanguageParser.parse("AGE (100, 200)", false).toString());
  }

  @Test
  public void andNode() throws Exception {
    Assert.assertEquals("AND(ICD9=100.2, ICD9=200.2)", LanguageParser.parse("AND (ICD9=100.2, ICD9=200.2)", false).toString());
  }

  @Test
  public void atcNode() throws Exception {
    Assert.assertEquals("ATC=\"something\"", LanguageParser.parse("ATC = \"something\"", false).toString());
  }

  @Test
  public void beforeNode() throws Exception {
    Assert.assertEquals("BEFORE(ICD9=100.2*, ICD9=200.2*)+*<>(-200, -100)", LanguageParser.parse("BEFORE(ICD9=100.2*, ICD9=200.2*)+<>*(-100, -200)", false).toString());
  }

  @Test
  public void countNode() throws Exception {
    Assert.assertEquals("COUNT(ICD9=100.2, ICD9=200.2, SINGLE, 5, MAX)", LanguageParser.parse("COUNT(ICD9=100.2, ICD9=200.2, SINGLE, 5, MAX)", false).toString());
    Assert.assertEquals("COUNT(ICD9=100.2, ICD9=200.2, ALL, MIN, MAX)", LanguageParser.parse("COUNT(ICD9=100.2, ICD9=200.2, ALL, MIN, MAX)", false).toString());
    Assert.assertEquals("COUNT(ICD9=100.2, MIN, MAX)", LanguageParser.parse("COUNT(ICD9=100.2, MIN, MAX)", false).toString());
  }

  @Test
  public void deathNode() throws Exception {
    Assert.assertEquals("DEATH", LanguageParser.parse("DEATH", false).toString());
  }

  @Test
  public void diffNode() throws Exception {
    Assert.assertEquals("DIFF(ICD9=200.2, ICD9=100.1)", LanguageParser.parse("DIFF(ICD9=200.2, ICD9=100.1)", false).toString());
  }

  @Test
  public void sameNode() throws Exception {
    Assert.assertEquals("SAME(ICD9=200.2, ICD9=100.1)", LanguageParser.parse("SAME(ICD9=200.2, ICD9=100.1)", false).toString());
  }

  @Test
  public void mergeNode() throws Exception {
    Assert.assertEquals("MERGE(ICD9=200.2, ICD9=100.1)", LanguageParser.parse("MERGE(ICD9=200.2, ICD9=100.1)", false).toString());
  }

  @Test
  public void durationNode() throws Exception {
    Assert.assertEquals("DURATION(ICD9=100.2, ICD9=200.2, SINGLE, 5, MAX)", LanguageParser.parse("DURATION(ICD9=100.2, ICD9=200.2, SINGLE, 5, MAX)", false).toString());
    Assert.assertEquals("DURATION(ICD9=100.2, ICD9=200.2, ALL, MIN, MAX)", LanguageParser.parse("DURATION(ICD9=100.2, ICD9=200.2, ALL, MIN, MAX)", false).toString());
    Assert.assertEquals("DURATION(ICD9=100.2, SINGLE, MIN, MAX)", LanguageParser.parse("DURATION(ICD9=100.2, SINGLE, MIN, MAX)", false).toString());
  }

  @Test
  public void encountersNode() throws Exception {
    Assert.assertEquals("ENCOUNTERS", LanguageParser.parse("ENCOUNTERS", false).toString());
  }

  @Test
  public void endNode() throws Exception {
    Assert.assertEquals("END(ICD9=200.2)", LanguageParser.parse("END(ICD9=200.2)", false).toString());
  }

  @Test
  public void startNode() throws Exception {
    Assert.assertEquals("START(ICD9=200.2)", LanguageParser.parse("START(ICD9=200.2)", false).toString());
  }

  @Test
  public void equalNode() throws Exception {
    Assert.assertEquals("EQUAL(ICD9=100.2, ICD9=200.2)", LanguageParser.parse("EQUAL(ICD9=100.2, ICD9=200.2)", false).toString());
  }

  @Test
  public void raceNode() throws Exception {
    Assert.assertEquals("RACE=\"BLACK\"", LanguageParser.parse("RACE=\"BLACK\"", false).toString());
    Assert.assertEquals("RACE=\"WHITE\"", LanguageParser.parse("RACE=\"WHITE\"", false).toString());
    Assert.assertEquals("RACE=\"ASIAN\"", LanguageParser.parse("RACE=\"ASIAN\"", false).toString());
  }

  @Test
  public void ethnicityNode() throws Exception {
    Assert.assertEquals("ETHNICITY=\"LATINO\"", LanguageParser.parse("ETHNICITY=\"LATINO\"", false).toString());
  }

  @Test
  public void eventFlowNode() throws Exception {
    Assert.assertEquals("EVENT FLOW(ICD9=100.2)", LanguageParser.parse("EVENTFLOW(ICD9=100.2)", false).toString());
  }

  @Test
  public void extendByNode() throws Exception {
    Assert.assertEquals("EXTEND BY(ICD9=100.2, 100, 200)", LanguageParser.parse("EXTEND BY(ICD9=100.2, 100, 200)", false).toString());
    Assert.assertEquals("EXTEND BY(ICD9=100.2, END-200, START+100)", LanguageParser.parse("EXTEND BY(ICD9=100.2, START+100, END-200)", false).toString());
  }

  @Test
  public void familyHistoryTextNode() throws Exception {
    Assert.assertEquals("~TEXT=\"aaa\"", LanguageParser.parse("~TEXT=\"aaa\"", false).toString());
  }

  @Test
  public void negatedHistoryTextNode() throws Exception {
    Assert.assertEquals("!TEXT=\"aaa\"", LanguageParser.parse("!TEXT=\"aaa\"", false).toString());
  }

  @Test
  public void firstMentionNode() throws Exception {
    Assert.assertEquals("FIRST MENTION(ICD9=100.1)", LanguageParser.parse("FIRST MENTION(ICD9=100.1)", false).toString());
  }

  @Test
  public void lastMentionNode() throws Exception {
    Assert.assertEquals("LAST MENTION(ICD9=100.1)", LanguageParser.parse("LAST MENTION(ICD9=100.1)", false).toString());
  }

  @Test
  public void genderNode() throws Exception {
    Assert.assertEquals("GENDER=\"MALE\"", LanguageParser.parse("GENDER=\"MALE\"", false).toString());
  }

  @Test
  public void icd9Node() throws Exception {
    Assert.assertEquals("ICD9=100.1", LanguageParser.parse("ICD9=100.1", false).toString());
  }

  @Test
  public void cptNode() throws Exception {
    Assert.assertEquals("CPT=50000", LanguageParser.parse("CPT=50000", false).toString());
  }

  @Test
  public void identicalNode() throws Exception {
    Assert.assertEquals("IDENTICAL(CPT=50000, CPT=60000)", LanguageParser.parse("IDENTICAL(CPT=50000, CPT=60000)", false).toString());
  }

  @Test
  public void intersectNode() throws Exception {
    Assert.assertEquals("INTERSECT(ICD9=200.2, ICD9=100.1)", LanguageParser.parse("INTERSECT(ICD9=200.2, ICD9=100.1)", false).toString());
  }

  @Test
  public void intervalNode() throws Exception {
    Assert.assertEquals("INTERVAL(ICD9=200.2, ICD9=100.1)", LanguageParser.parse("INTERVAL(ICD9=200.2, ICD9=100.1)", false).toString());
  }

  @Test
  public void labsNode() throws Exception {
    Assert.assertEquals("LABS(\"aaa\")", LanguageParser.parse("LABS(\"aaa\")", false).toString());
  }

  @Test
  public void notesBoolean() throws Exception {
    Assert.assertEquals("NOTE(AND(TEXT=\"aaa\", OR(TEXT=\"bbb\", TEXT=\"ccc\"), NOT(TEXT=\"ddd\")))", LanguageParser.parse(
        "NOTE(AND(TEXT=\"aaa\", OR(TEXT=\"bbb\", TEXT=\"ccc\"), NOT(TEXT=\"ddd\")))", false).toString());
  }

  @Test
  public void notesTimeInstances() throws Exception {
    Assert.assertEquals("NOTES", LanguageParser.parse("NOTE", false).toString());
  }

  @Test
  public void notNode() throws Exception {
    Assert.assertEquals("NOT(ICD9=100.2)", LanguageParser.parse("NOT(ICD9=100.2)", false).toString());
  }

  @Test
  public void nullNode() throws Exception {
    Assert.assertEquals("NULL", LanguageParser.parse("NULL", false).toString());
  }

  @Test
  public void orNode() throws Exception {
    Assert.assertEquals("OR(NULL, ICD9=100.2)", LanguageParser.parse("OR(NULL, ICD9=100.2)", false).toString());
  }

  @Test
  public void primaryNode() throws Exception {
    Assert.assertEquals("PRIMARY(ICD9=100.2)", LanguageParser.parse("PRIMARY(ICD9=100.2)", false).toString());
  }

  @Test
  public void rxNormNode() throws Exception {
    Assert.assertEquals("RX=200", LanguageParser.parse("RXNORM=200", false).toString());
    Assert.assertEquals("RX=200", LanguageParser.parse("RX=200", false).toString());
  }

  @Test
  public void textNode() throws Exception {
    Assert.assertEquals("TEXT=\"aaa\"", LanguageParser.parse("TEXT=\"aaa\"", false).toString());
  }

  @Test
  public void timelineNode() throws Exception {
    Assert.assertEquals("TIMELINE", LanguageParser.parse("TIMELINE", false).toString());
  }

  @Test
  public void unionNode() throws Exception {
    Assert.assertEquals("UNION(ICD9=100.1, ICD9=200.2)", LanguageParser.parse("UNION(ICD9=100.1, ICD9=200.2)", false).toString());
  }

  @Test
  public void vitalsNode() throws Exception {
    Assert.assertEquals("VITALS(\"temp\", 0.0, 100.0)", LanguageParser.parse("VITALS(\"temp\", 0, 100)", false).toString());
  }

  @Test
  public void yearNode() throws Exception {
    Assert.assertEquals("YEAR(2001, 2002)", LanguageParser.parse("YEAR(2001, 2002)", false).toString());
  }

}