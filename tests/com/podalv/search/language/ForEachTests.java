package com.podalv.search.language;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.assertQuery;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.query;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.functionalTests.CsvFunctionalTests;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitWithSabRecord;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.utils.file.FileUtils;

public class ForEachTests {

  private ClinicalSearchEngine engine = null;

  @Before
  public void setup() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException {
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    PatientExport.setInstance(new PatientExport(new File(".", "export")));
    cleanup();
  }

  @After
  public void tearDown() throws IOException {
    cleanup();
  }

  private void cleanup() throws IOException {
    if (engine != null) {
      engine.close();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private static ClinicalSearchEngine getPatient() throws InstantiationException, IllegalAccessException, SQLException, IOException, InterruptedException, ClassNotFoundException {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1).gender("female").ethnicity("eth1").race("race1"));
    source.addDemographics(Stride7Demographics.create(2).gender("male").death(22.0));

    source.addVisitPxIcdDeLpch(Stride7VisitWithSabRecord.create(1).age(12.2).code("25000").year(2012).sab("CPT").visitType("INPATIENT"));
    source.addVisitPxIcdDeShc(Stride7VisitWithSabRecord.create(2).age(12.2).code("35000").year(2013).sab("CPT").visitType("OUTPATIENT"));

    return Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 100);
  }

  @Test
  public void singleForEach() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, "FOR EACH (CPT=25000) AS (ABC) {RETURN ABC AS DEF;}DEF"), 1);
    assertQuery(query(engine, "FOR EACH (CPT=35000) AS (ABC) {RETURN ABC AS DEF;}DEF"), 2);
    assertQuery(query(engine, "FOR EACH (CPT=50000) AS (ABC) {RETURN ABC AS DEF;}DEF"));
  }

  @Test
  public void nestedForEach() throws Exception {
    engine = getPatient();
    assertQuery(query(engine, "FOR EACH (CPT=25000) AS (ABC) {RETURN ABC AS DEF;}INTERSECT(DEF)"), 1);
    assertQuery(query(engine, "FOR EACH (CPT=35000) AS (ABC) {RETURN ABC AS DEF;}INTERSECT(DEF)"), 2);
    assertQuery(query(engine, "FOR EACH (CPT=35000) AS (ABC) {RETURN ABC AS DEF;}\nFOR EACH (CPT=25000) AS (ABC) {RETURN ABC AS XXX;}\nUNION(XXX, DEF)"), 1, 2);
  }

  @Test
  public void csvForEach() throws Exception {
    engine = getPatient();
    final ArrayList<String> lines = CsvFunctionalTests.readLines(query(engine, "FOR EACH (CPT=25000) AS (ABC) {RETURN ABC AS DEF;}CSV(DEF, CPT)"));
    Assert.assertEquals(2, lines.size());
    Assert.assertEquals(CsvFunctionalTests.CSV_HEADER, lines.get(0));
    Assert.assertEquals("1\tCPT=25000\t2012\t\t\t1\t12.0\t12.0\t", lines.get(1));
  }

}
