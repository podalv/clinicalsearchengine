package com.podalv.search.language.script;

import org.junit.Assert;
import org.junit.Test;

public class ScriptParserTest {

  @Test
  public void smokeTest() throws Exception {
    final String sample = "VAR DIABETES = INTERSECT(ICD9=250.50, ICD9=250.01)\nVAR STROKE = INTERSECT(ICD9=220.20, ICD9=220.10)\nVAR BOTH = INTERSECT($DIABETES, $STROKE)\nUNION(ICD9=200.20, $BOTH)\n\n";
    Assert.assertEquals("UNION(ICD9=200.20, INTERSECT(INTERSECT(ICD9=250.50, ICD9=250.01), INTERSECT(ICD9=220.20, ICD9=220.10)))", new ScriptParser().parseScripts(
        sample).getResult().trim());
  }

  @Test
  public void persistentScripts() throws Exception {
    final String sample = "VAR PODALV.DIABETES = INTERSECT(250.00, 250.03)\n$PODALV.DIABETES";
    final ScriptParser parser = new ScriptParser(new ScriptStorage());
    Assert.assertEquals("INTERSECT(250.00, 250.03)", parser.parseScripts(sample).getResult());
  }

  @Test(expected = UnsupportedOperationException.class)
  public void complex_nested_test() throws Exception {
    final String sample = "VAR $A = ICD9=100.10\n" + //
        "VAR $B = ICD9=100.20\n" + //
        "VAR $C = INTERSECT($B, $A)\n" + //
        "VAR $D = INTERSECT($C, $B, $A)\n" + //
        "$E";
    new ScriptParser().parseScripts(sample);
    throw new RuntimeException("Should have thrown exception");
  }

  @Test
  public void complex_nested_test_correct() throws Exception {
    final String sample = "VAR A = ICD9=100.10\n" + //
        "VAR B = ICD9=100.20\n" + //
        "VAR C = INTERSECT($B, $A)\n" + //
        "VAR D = INTERSECT($C, $B, $A)\n" + //
        "$D";
    Assert.assertEquals("INTERSECT(INTERSECT(ICD9=100.20, ICD9=100.10), ICD9=100.20, ICD9=100.10)", new ScriptParser().parseScripts(sample).getResult());
  }

  @Test(expected = UnsupportedOperationException.class)
  public void simple_circular_reference() throws Exception {
    final String sample = "VAR A = $B\n" + //
        "VAR B = $A\n" + //
        "$A";
    new ScriptParser().parseScripts(sample);
    throw new RuntimeException("Should have thrown exception");
  }

  @Test(expected = UnsupportedOperationException.class)
  public void complex_circular_reference() throws Exception {
    final String sample = "VAR A = $B\n" + //
        "VAR B = $C\n" + //
        "VAR C = $D\n" + //
        "VAR D = $A\n" + //
        "$A";
    new ScriptParser().parseScripts(sample);
    throw new RuntimeException("Should have thrown exception");
  }

  @Test
  public void not_enough_parenthesis() throws Exception {
    final String sample = "VAR A = INTERSECT(OR(ICD9=250.50)\n" + //
        "UNION($A)";
    Assert.assertEquals("UNION(INTERSECT(OR(ICD9=250.50)))", new ScriptParser().parseScripts(sample).getResult());
  }

  @Test
  public void too_many_parenthesis() throws Exception {
    final String sample = "VAR A = INTERSECT(OR(ICD9=250.50)))\n" + //
        "UNION($A)";
    Assert.assertEquals("UNION(INTERSECT(OR(ICD9=250.50)))", new ScriptParser().parseScripts(sample).getResult());
  }

  @Test
  public void multiple_command_uses() throws Exception {
    final String sample = "VAR A = INTERSECT(OR(ICD9=250.50)))\n" + //
        "UNION($A, $A)";
    Assert.assertEquals("UNION(INTERSECT(OR(ICD9=250.50)), INTERSECT(OR(ICD9=250.50)))", new ScriptParser().parseScripts(sample).getResult());
  }

  @Test
  public void invalidCarraigeReturn() throws Exception {
    final String sample = "VAR A = ICD9=250.50\r\r" + //
        "$A\r\r";
    Assert.assertEquals("ICD9=250.50", new ScriptParser().parseScripts(sample).getResult());
  }

}