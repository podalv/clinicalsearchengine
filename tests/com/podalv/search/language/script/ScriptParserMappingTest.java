package com.podalv.search.language.script;

import org.junit.Assert;
import org.junit.Test;

public class ScriptParserMappingTest {

  @Test
  public void noVariables() throws Exception {
    final String query = "ICD9=220.20";
    final ScriptParser parser = new ScriptParser();
    final ScriptParserResult result = parser.parseScripts(query);
    for (int x = 0; x < query.length(); x++) {
      Assert.assertEquals(x, result.getOriginalPosition(x, 0).getColumn());
      Assert.assertEquals(0, result.getOriginalPosition(x, 0).getRow());
    }
  }

  @Test
  public void noVariablesTrim() throws Exception {
    final String query = "  ICD9=220.20";
    final ScriptParser parser = new ScriptParser();
    final ScriptParserResult result = parser.parseScripts(query);
    for (int x = 0; x < query.length(); x++) {
      Assert.assertEquals(x, result.getOriginalPosition(x, 0).getColumn());
      Assert.assertEquals(0, result.getOriginalPosition(x, 0).getRow());
    }
  }

}
