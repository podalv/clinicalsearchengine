package com.podalv.search.language.evaluation;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class PayloadPointersTest {

  @Test
  public void multiple_iterator() throws Exception {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {5, 8}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {8, 15}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {20, 25}));

    final PayloadPointers data = new PayloadPointers(TYPE.OTHER, null, patient, new IntArrayList(new int[] {1, 2, 3, 4}));
    final PayloadIterator iterator = data.iterator(patient);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(15, iterator.getEndId());
    iterator.next();
    Assert.assertEquals(20, iterator.getStartId());
    Assert.assertEquals(25, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void continuous_iterator() throws Exception {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 12}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {12, 15}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {15, 25}));

    final PayloadPointers data = new PayloadPointers(TYPE.OTHER, null, patient, new IntArrayList(new int[] {0, 1, 2, 3}));
    final PayloadIterator iterator = data.iterator(patient);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(25, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void empty() throws Exception {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 12}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {12, 15}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {15, 25}));

    final PayloadPointers data = new PayloadPointers(TYPE.OTHER, null, patient, new IntArrayList(new int[] {}));
    final PayloadIterator iterator = data.iterator(patient);
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void rawPayloadIterator() throws Exception {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);

    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10, 1}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {5, 12, 2}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {12, 15, 3}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {15, 25, 4}));

    final PayloadPointers data = new PayloadPointers(TYPE.ICD9_RAW, null, patient, new IntArrayList(new int[] {1, 2, 3, 4}));
    final PayloadIterator iterator = data.iterator(patient);
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(10, iterator.getEndId());
    Assert.assertArrayEquals(new int[] {5, 10, 1}, iterator.getPayload().toArray());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(12, iterator.getEndId());
    Assert.assertArrayEquals(new int[] {5, 12, 2}, iterator.getPayload().toArray());
    iterator.next();
    Assert.assertEquals(12, iterator.getStartId());
    Assert.assertEquals(15, iterator.getEndId());
    Assert.assertArrayEquals(new int[] {12, 15, 3}, iterator.getPayload().toArray());
    iterator.next();
    Assert.assertEquals(15, iterator.getStartId());
    Assert.assertEquals(25, iterator.getEndId());
    Assert.assertArrayEquals(new int[] {15, 25, 4}, iterator.getPayload().toArray());
    Assert.assertFalse(iterator.hasNext());
  }
}
