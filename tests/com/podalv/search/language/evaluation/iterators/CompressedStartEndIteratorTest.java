package com.podalv.search.language.evaluation.iterators;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayList;

public class CompressedStartEndIteratorTest {

  public static void compare(final CompressedStartEndIterator i, final int ... values) {
    final IntArrayList l = new IntArrayList();
    while (i.hasNext()) {
      i.next();
      l.add(i.getStartId());
      l.add(i.getEndId());
    }
    Assert.assertArrayEquals(values, l.toArray());
  }

  @Test
  public void testName() throws Exception {
    compare(new CompressedStartEndIterator(new IntArrayList(new int[] {1, 2})), 1, 2);
  }

  @Test
  public void empty() throws Exception {
    compare(new CompressedStartEndIterator(new IntArrayList(new int[0])));
  }

  @Test
  public void noOverlap() throws Exception {
    compare(new CompressedStartEndIterator(new IntArrayList(new int[] {1, 2, 4, 5})), 1, 2, 4, 5);
  }

  @Test
  public void overlap() throws Exception {
    compare(new CompressedStartEndIterator(new IntArrayList(new int[] {1, 2, 3, 4})), 1, 2, 3, 4);
  }

  @Test
  public void overlapSingle() throws Exception {
    compare(new CompressedStartEndIterator(new IntArrayList(new int[] {1, 2, 1, 3})), 1, 3);
  }

  @Test
  public void overlapMultiple() throws Exception {
    compare(new CompressedStartEndIterator(new IntArrayList(new int[] {1, 2, 1, 3, 2, 10, 5, 20})), 1, 20);
  }

}
