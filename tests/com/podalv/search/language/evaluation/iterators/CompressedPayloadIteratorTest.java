package com.podalv.search.language.evaluation.iterators;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientBuilder;

public class CompressedPayloadIteratorTest {

  private PatientBuilder getTestPatient() {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {5, 12}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {12, 15}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {15, 25}));
    return patient;
  }

  private PatientBuilder getTestPatient2() {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {5, 12}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {15, 18}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {22, 25}));
    return patient;
  }

  private PatientBuilder getTestPatient3() {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {5, 12}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {15, 18}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {22, 25}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {24, 27}));
    return patient;
  }

  private PatientBuilder getTestPatient4() {
    final PatientBuilder patient = Mockito.mock(PatientBuilder.class);
    Mockito.when(patient.getPayload(0)).thenReturn(new IntArrayList(new int[] {0}));
    Mockito.when(patient.getPayload(1)).thenReturn(new IntArrayList(new int[] {5, 10}));
    Mockito.when(patient.getPayload(2)).thenReturn(new IntArrayList(new int[] {5, 12}));
    Mockito.when(patient.getPayload(3)).thenReturn(new IntArrayList(new int[] {15, 18}));
    Mockito.when(patient.getPayload(4)).thenReturn(new IntArrayList(new int[] {15, 18}));
    Mockito.when(patient.getPayload(5)).thenReturn(new IntArrayList(new int[] {15, 18}));
    Mockito.when(patient.getPayload(6)).thenReturn(new IntArrayList(new int[] {22, 25}));
    Mockito.when(patient.getPayload(7)).thenReturn(new IntArrayList(new int[] {24, 27}));
    return patient;
  }

  @Test
  public void array() throws Exception {
    final IntArrayList payloads = new IntArrayList(new int[] {1, 2, 3, 4});
    final CompressedPayloadIterator iterator = new CompressedPayloadIterator(getTestPatient(), payloads);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(25, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void iterator() throws Exception {
    final IntArrayList payloads = new IntArrayList(new int[] {1, 2, 3, 4});

    final PayloadIterator i = new RawPayloadIterator(getTestPatient(), payloads);
    final CompressedPayloadIterator iterator = new CompressedPayloadIterator(getTestPatient(), i);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(25, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void iterator2() throws Exception {
    final IntArrayList payloads = new IntArrayList(new int[] {1, 2, 3, 4});

    final PayloadIterator i = new RawPayloadIterator(getTestPatient2(), payloads);
    final CompressedPayloadIterator iterator = new CompressedPayloadIterator(getTestPatient(), i);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(12, iterator.getEndId());
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(15, iterator.getStartId());
    Assert.assertEquals(18, iterator.getEndId());
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(22, iterator.getStartId());
    Assert.assertEquals(25, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void iterator3() throws Exception {
    final IntArrayList payloads = new IntArrayList(new int[] {1, 2, 3, 4, 5});

    final PayloadIterator i = new RawPayloadIterator(getTestPatient3(), payloads);
    final CompressedPayloadIterator iterator = new CompressedPayloadIterator(getTestPatient(), i);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(12, iterator.getEndId());
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(15, iterator.getStartId());
    Assert.assertEquals(18, iterator.getEndId());
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(22, iterator.getStartId());
    Assert.assertEquals(27, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void iterator4() throws Exception {
    final IntArrayList payloads = new IntArrayList(new int[] {1, 2, 3, 4, 5, 6, 7});

    final PayloadIterator i = new RawPayloadIterator(getTestPatient4(), payloads);
    final CompressedPayloadIterator iterator = new CompressedPayloadIterator(getTestPatient(), i);
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(5, iterator.getStartId());
    Assert.assertEquals(12, iterator.getEndId());
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(15, iterator.getStartId());
    Assert.assertEquals(18, iterator.getEndId());
    Assert.assertTrue(iterator.hasNext());
    iterator.next();
    Assert.assertEquals(22, iterator.getStartId());
    Assert.assertEquals(27, iterator.getEndId());
    Assert.assertFalse(iterator.hasNext());
  }

}
