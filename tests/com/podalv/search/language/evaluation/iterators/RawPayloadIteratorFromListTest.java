package com.podalv.search.language.evaluation.iterators;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.maps.primitive.list.IntArrayList;

public class RawPayloadIteratorFromListTest {

  @Test
  public void smokeTest() throws Exception {
    final IntArrayList data = new IntArrayList();
    data.add(10);
    data.add(20);
    data.add(1);
    data.add(20);
    data.add(30);
    data.add(2);
    data.add(30);
    data.add(40);
    data.add(3);
    final RawPayloadIteratorFromList i = new RawPayloadIteratorFromList(data, 3);
    i.next();
    Assert.assertEquals(10, i.getStartId());
    Assert.assertEquals(20, i.getEndId());
    Assert.assertArrayEquals(new int[] {10, 20, 1}, i.getPayload().toArray());

    i.next();
    Assert.assertEquals(20, i.getStartId());
    Assert.assertEquals(30, i.getEndId());
    Assert.assertArrayEquals(new int[] {20, 30, 2}, i.getPayload().toArray());

    i.next();
    Assert.assertEquals(30, i.getStartId());
    Assert.assertEquals(40, i.getEndId());
    Assert.assertArrayEquals(new int[] {30, 40, 3}, i.getPayload().toArray());

    Assert.assertFalse(i.hasNext());
  }
}
