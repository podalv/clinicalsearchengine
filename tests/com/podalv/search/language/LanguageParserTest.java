package com.podalv.search.language;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import com.podalv.extractor.hierarchies.icd10.Icd10Hierarchy;
import com.podalv.extractor.hierarchies.icd9.Icd9Hierarchy;
import com.podalv.search.language.node.AtcNode;
import com.podalv.search.language.node.ErrorNode;
import com.podalv.search.language.node.HasIcd9Node;
import com.podalv.search.language.node.LanguageNodeWithChildren;
import com.podalv.search.language.node.NoteAndNode;
import com.podalv.search.language.node.NoteNode;
import com.podalv.search.language.node.NoteOrNode;
import com.podalv.search.language.node.RootNode;
import com.podalv.search.language.node.TextNode;
import com.podalv.search.language.script.LanguageParser;
import com.podalv.search.language.script.ScriptParser;
import com.podalv.search.language.script.ScriptStorage;
import com.podalv.utils.file.FileUtils;

public class LanguageParserTest {

  @After
  public void cleanup() {
    try {
      final File[] files = new File("extractionTestData").listFiles();
      if (files != null) {
        for (final File file : files) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void text() throws Exception {
    final RootNode root = LanguageParser.parse("AND(TEXT=\"diabetes\")", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final TextNode node = (TextNode) n.getChildren().get(0);
    Assert.assertEquals("diabetes", node.getText());
  }

  @Test
  public void atc() throws Exception {
    final RootNode root = LanguageParser.parse("AND(ATC=\"A12254X\")", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final AtcNode node = (AtcNode) n.getChildren().get(0);
    Assert.assertEquals("a12254x", node.getAtcString());
  }

  @Test
  public void icd9() throws Exception {
    final RootNode root = LanguageParser.parse("AND(ICD9=100.2)", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final HasIcd9Node node = (HasIcd9Node) n.getChildren().get(0);
    Assert.assertEquals("100.2", node.getIcd9());
  }

  @Test
  public void icd9_number() throws Exception {
    final RootNode root = LanguageParser.parse("AND(ICD9=100)", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final HasIcd9Node node = (HasIcd9Node) n.getChildren().get(0);
    Assert.assertEquals("100", node.getIcd9());
  }

  @Test
  public void note() throws Exception {
    final RootNode root = LanguageParser.parse("AND(NOTE(TEXT=\"diabetes\"))", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final NoteNode node = (NoteNode) n.getChildren().get(0);
    Assert.assertEquals(1, node.getChildren().size());
    final TextNode text = (TextNode) node.getChildren().get(0);
    Assert.assertEquals("diabetes", text.getText());
  }

  @Test
  public void text_special_characters() throws Exception {
    final RootNode root = LanguageParser.parse("AND(NOTE(TEXT=\"diabetes and something else\"))", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final NoteNode node = (NoteNode) n.getChildren().get(0);
    Assert.assertEquals(1, node.getChildren().size());
    final TextNode text = (TextNode) node.getChildren().get(0);
    Assert.assertEquals("diabetes and something else", text.getText());
  }

  @Test
  public void icd9_extended() throws Exception {
    final RootNode root = LanguageParser.parse("AND(ICD9=E100-E100.99)", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final HasIcd9Node node = (HasIcd9Node) n.getChildren().get(0);
    Assert.assertEquals("E100-E100.99", node.getIcd9());
  }

  @Test
  public void icd9_extended_2() throws Exception {
    final RootNode root = LanguageParser.parse("AND(ICD9=780-789.99)", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final HasIcd9Node node = (HasIcd9Node) n.getChildren().get(0);
    Assert.assertEquals("780-789.99", node.getIcd9());
  }

  @Test
  public void text_empty_line() throws Exception {
    final RootNode root = LanguageParser.parse("AND(NOTE(TEXT=\"diabetes and something else\"))\n", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final NoteNode node = (NoteNode) n.getChildren().get(0);
    Assert.assertEquals(1, node.getChildren().size());
    final TextNode text = (TextNode) node.getChildren().get(0);
    Assert.assertEquals("diabetes and something else", text.getText());
  }

  @Test
  public void boolean_notes() throws Exception {
    final RootNode root = LanguageParser.parse("NOTE(AND(TEXT=\"diabetes and something else\")))\n", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final NoteAndNode node = (NoteAndNode) n.getChildren().get(0);
    Assert.assertEquals(1, node.getChildren().size());
    final TextNode text = (TextNode) node.getChildren().get(0);
    Assert.assertEquals("diabetes and something else", text.getText());
  }

  @Test
  public void boolean_notes_multiple() throws Exception {
    final RootNode root = LanguageParser.parse("NOTE(AND(TEXT=\"aa\", TEXT=\"diabetes and something else\")))\n", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final NoteAndNode node = (NoteAndNode) n.getChildren().get(0);
    Assert.assertEquals(2, node.getChildren().size());
    TextNode text = (TextNode) node.getChildren().get(0);
    Assert.assertEquals("aa", text.getText());
    text = (TextNode) node.getChildren().get(1);
    Assert.assertEquals("diabetes and something else", text.getText());
  }

  @Test
  public void boolean_notes_complex() throws Exception {
    final RootNode root = LanguageParser.parse("NOTE(AND(OR(TEXT=\"aa\"), TEXT=\"diabetes and something else\")))\n", false);
    final LanguageNodeWithChildren n = ((LanguageNodeWithChildren) root.getChildren().get(0));
    final NoteAndNode node = (NoteAndNode) n.getChildren().get(0);
    Assert.assertEquals(2, node.getChildren().size());
    final NoteOrNode text = (NoteOrNode) node.getChildren().get(0);
    Assert.assertEquals("TEXT=\"aa\"", text.getChildren().get(0).toString());
    final TextNode t = (TextNode) node.getChildren().get(1);
    Assert.assertEquals("diabetes and something else", t.getText());
  }

  @Test
  public void notes_incorrect_child() throws Exception {
    final RootNode root = LanguageParser.parse("NOTE(INTERSECT(TEXT=\"diabetes and something else\")))\n", false);
    Assert.assertTrue(root.getChildren().get(0) instanceof ErrorNode);
  }

  @Test
  public void notes_empty_child() throws Exception {
    final RootNode root = LanguageParser.parse("NOTE(AND()))\n", false);
    Assert.assertTrue(root.getChildren().get(0) instanceof ErrorNode);
  }

  @Test
  public void time_units_before() throws Exception {
    Assert.assertEquals("BEFORE(ICD9=100, ICD9=200)+*(-2880, -1440)", LanguageParser.parse("BEFORE(ICD9=100, ICD9=200)+*(-1 day, -2 days)".toUpperCase(), false).toString());
    Assert.assertEquals("BEFORE(ICD9=100, ICD9=200)+*(-120, -60)", LanguageParser.parse("BEFORE(ICD9=100, ICD9=200)+*(-1 hour, -2 hours)".toUpperCase(), false).toString());
    Assert.assertEquals("BEFORE(ICD9=100, ICD9=200)+*(-2, -1)", LanguageParser.parse("BEFORE(ICD9=100, ICD9=200)+*(-1 minute, -2 minutes)".toUpperCase(), false).toString());
    Assert.assertEquals("BEFORE(ICD9=100, ICD9=200)+*(-20160, -10080)", LanguageParser.parse("BEFORE(ICD9=100, ICD9=200)+*(-1 week, -2 weeks)".toUpperCase(), false).toString());
    Assert.assertEquals("BEFORE(ICD9=100, ICD9=200)+*(-86400, -43200)", LanguageParser.parse("BEFORE(ICD9=100, ICD9=200)+*(-1 month, -2 months)".toUpperCase(), false).toString());
    Assert.assertEquals("BEFORE(ICD9=100, ICD9=200)+*(-1051200, -525600)", LanguageParser.parse("BEFORE(ICD9=100, ICD9=200)+*(-1 year, -2 years)".toUpperCase(), false).toString());
  }

  @Test
  public void time_units_extend_by() throws Exception {
    Assert.assertEquals("EXTEND BY(ICD9=100, 43200, 86400)", LanguageParser.parse("EXTEND BY(ICD9=100, 1 month, 2 months)".toUpperCase(), false).toString());
  }

  @Test
  public void time_units_extend_by_start_end() throws Exception {
    Assert.assertEquals("EXTEND BY(ICD9=100, START-14400, START+14400)", LanguageParser.parse("EXTEND BY(ICD9=100, START+10 days, START - 10 days)".toUpperCase(),
        false).toString());
    Assert.assertEquals("EXTEND BY(ICD9=100, START-14400, START+14400)", LanguageParser.parse("EXTEND BY(ICD9=100, START +  10 days, START -10 days)".toUpperCase(),
        false).toString());
  }

  @Test
  public void time_units_age() throws Exception {
    Assert.assertEquals("AGE(10, 20)", LanguageParser.parse("AGE(10,20)".toUpperCase(), false).toString());
  }

  @Test
  public void primary_icd9() throws Exception {
    Assert.assertEquals("PRIMARY(ICD9=250.50)", LanguageParser.parse("PRIMARY(ICD9=250.50)".toUpperCase(), false).toString());
  }

  @Test
  public void original_icd9() throws Exception {
    Assert.assertEquals("ORIGINAL(ICD9=250.50)", LanguageParser.parse("ORIGINAL(ICD9=250.50)".toUpperCase(), false).toString());
    Assert.assertEquals("PRIMARY(ORIGINAL(ICD9=250.50))", LanguageParser.parse("ORIGINAL(PRIMARY(ICD9=250.50))".toUpperCase(), false).toString());
    Assert.assertEquals("PRIMARY(ORIGINAL(ICD9=250.50))", LanguageParser.parse("PRIMARY(ORIGINAL(ICD9=250.50))".toUpperCase(), false).toString());
  }

  @Test
  public void original_icd10() throws Exception {
    Assert.assertEquals("ORIGINAL(ICD10=A10)", LanguageParser.parse("ORIGINAL(ICD10=A10)".toUpperCase(), false).toString());
    Assert.assertEquals("PRIMARY(ORIGINAL(ICD10=A10))", LanguageParser.parse("ORIGINAL(PRIMARY(ICD10=A10))".toUpperCase(), false).toString());
    Assert.assertEquals("PRIMARY(ORIGINAL(ICD10=A10))", LanguageParser.parse("PRIMARY(ORIGINAL(ICD10=A10))".toUpperCase(), false).toString());
  }

  @Test
  public void primary_non_icd9() throws Exception {
    Assert.assertTrue(LanguageParser.parse("PRIMARY(CPT=43200)".toUpperCase(), false).toString().indexOf("Variable name cannot contain the specified keyword near") != -1);
  }

  @Test
  public void event_flow() throws Exception {
    Assert.assertEquals("EVENT FLOW(ICD9=250.01)", LanguageParser.parse("EVENT FLOW(ICD9=250.01)".toUpperCase(), false).toString());
  }

  @Test
  public void before_incorrect() throws Exception {
    Assert.assertTrue(LanguageParser.parse("BEFORE(ICD9=200.2, ICD9=100.1)<>(-10, -20)".toUpperCase(), false).toString().indexOf(
        "BEFORE command parameters must start with '+' or '-'") != -1);
  }

  @Test
  public void patients() throws Exception {
    Assert.assertEquals("PATIENTS(1, 3, 5, 7, 10)", LanguageParser.parse("PATIENTS(1, 3, 5, 7, 10)".toUpperCase(), false).toString());
  }

  @Test
  public void patients_single_patient() throws Exception {
    Assert.assertEquals("PATIENTS(1)", LanguageParser.parse("PATIENTS(1)".toUpperCase(), false).toString());
  }

  @Test
  public void note_type() throws Exception {
    Assert.assertEquals("NOTE(NOTE TYPE=\"aaa\", TEXT=\"aaa\")", LanguageParser.parse("NOTE(NOTE TYPE=\"aaa\", TEXT=\"aaa\")".toUpperCase(), false).toString());
  }

  @Test
  public void multiple_lines() throws Exception {
    Assert.assertTrue(LanguageParser.parse("PATIENTS(1, 3, 5, 7, 10) ICD9=250.00".toUpperCase(), false).toString().indexOf("Extraneous input. Cannot parse query") != -1);
  }

  @Test
  public void patients_multiple() throws Exception {
    Assert.assertEquals("PATIENTS(323129, 1334703, 1378005, 1517574, 1601940, 1608831, 1691838, 1854393, 1964261, 1998606)", LanguageParser.parse(
        "patients(323129, 1334703, 1608831, 1691838, 1854393, 1998606, 1378005, 1517574, 1601940, 1964261)".toUpperCase(), false).toString());
  }

  @Test
  public void complex_query() throws Exception {
    final String query = "var stroke = AND(OR(icd9=436, icd9=434), NOT(OR(icd9=393, icd9=394, icd9=397.1, icd9=397.9, icd9=398, icd9=246, icd9=424.9, icd9=V43, icd9=433.1, icd9=431, icd9=434.11, icd9=434.01)), AGE (40 years, 90 years), VISIT TYPE=\"INPATIENT\", NOT(TEXT=\"thyroid diseases\"), NOT(TEXT=\"heart valve prosthesis\"), NOT(TEXT=\"disease of mitral valve\"), NOT(TEXT=\"rheumatic heart disease\")) \n"
        + //
        "\n" + //
        "var afib = FIRST_MENTION(icd9=427.3)\n" + //
        "\n" + //
        "BEFORE ($stroke*, $afib)+(-5 years, -1 year)".toUpperCase();
    Assert.assertEquals(
        "BEFORE(AND(OR(ICD9=436, ICD9=434), NOT(OR(ICD9=393, ICD9=394, ICD9=397.1, ICD9=397.9, ICD9=398, ICD9=246, ICD9=424.9, ICD9=V43, ICD9=433.1, ICD9=431, ICD9=434.11, ICD9=434.01)), AGE(21024000, 47304000), VISIT TYPE=\"INPATIENT\", NOT(TEXT=\"thyroid diseases\"), NOT(TEXT=\"heart valve prosthesis\"), NOT(TEXT=\"disease of mitral valve\"), NOT(TEXT=\"rheumatic heart disease\"))*, FIRST MENTION(ICD9=427.3))+(-2628000, -525600)",
        LanguageParser.parse(query, false).toString());
  }

  @Test
  public void text_parsing() throws Exception {
    Assert.assertEquals("TEXT=\"~`!@%^&*()_-+=\\|[]{};':,.<>/?\"", LanguageParser.parse("TEXT=\"~`!@%^&*()_-+=\\|[]{};':,.<>/?\"", false).toString());
  }

  @Test
  public void diff() throws Exception {
    Assert.assertEquals("DIFF(ICD9=250.00, ICD9=230.55)", LanguageParser.parse("DIFF(ICD9=250.00, ICD9=230.55)".toUpperCase(), false).toString());
  }

  @Test
  public void hugeQuery() throws Exception {
    final StringBuilder query = new StringBuilder();
    query.append("DIFF(TEXT=\"aaa\", OR(TEXT=\"aaa\"");
    for (int x = 0; x < 500; x++) {
      query.append(", TEXT=\"aaa\"");
    }
    query.append("))");
    final RootNode node = LanguageParser.parse(query.toString(), false);
    Assert.assertEquals(query.toString(), node.toString());
  }

  @Test
  public void gender() throws Exception {
    Assert.assertEquals("GENDER=\"FEMALE\"", LanguageParser.parse("GENDER=\"FEMALE\"".toUpperCase(), false).toString());
  }

  @Test
  public void race() throws Exception {
    Assert.assertEquals("RACE=\"AAA\"", LanguageParser.parse("RACE=\"AAA\"".toUpperCase(), false).toString());
  }

  @Test
  public void ethnicity() throws Exception {
    Assert.assertEquals("ETHNICITY=\"XXX\"", LanguageParser.parse("ETHNICITY=\"XXX\"".toUpperCase(), false).toString());
  }

  @Test
  public void before_command_asterisks() throws Exception {
    final String command = "var mi_icd9 = ICD9=410\n" + //
        "var trop = UNION(LABS(\"TNI\", \"HIGH\"), LABS(\"TNT\", \"HIGH\"))\n" //
        + "var mi = INTERSECT($mi_icd9, EXTEND BY($trop, -5 hour, 0))\n" //
        + "var inpatient_stay_after_mi = INTERSECT(VISIT TYPE=\"inpatient\", EXTEND BY(BEFORE($mi, VISIT TYPE=\"inpatient\"), 0, 72 hours))\n"//
        + "BEFORE(ICD9=401-405.99, $inpatient_stay_after_mi*)+(-1 year, 0)";
    ScriptParser.setInstance(new ScriptParser());
    Assert.assertEquals(
        "BEFORE(ICD9=401-405.99, INTERSECT(VISIT TYPE=\"INPATIENT\", EXTEND BY(BEFORE(INTERSECT(ICD9=410, EXTEND BY(UNION(LABS(\"tni\", \"HIGH\"), LABS(\"tnt\", \"HIGH\")), -300, 0)), VISIT TYPE=\"INPATIENT\"), 0, 4320))*)+(-525600, 0)",
        LanguageParser.parse(command.toUpperCase(), false).toString());
  }

  @Test
  public void before_nested() throws Exception {
    Assert.assertEquals("BEFORE(ICD9=111.11, BEFORE(ICD9=222.22, BEFORE(ICD9=333.33, ICD9=444.44)))+(-100, 0)", LanguageParser.parse(
        "BEFORE(ICD9=111.11, BEFORE(ICD9=222.22, BEFORE(ICD9=333.33, ICD9=444.44)))+(-100, 0)", false).toString());
    Assert.assertEquals("BEFORE(ICD9=111.11, BEFORE(ICD9=222.22, BEFORE(ICD9=333.33, ICD9=444.44))+(-100, 0))", LanguageParser.parse(
        "BEFORE(ICD9=111.11, BEFORE(ICD9=222.22, BEFORE(ICD9=333.33, ICD9=444.44))+(-100, 0))", false).toString());
    Assert.assertEquals("BEFORE(ICD9=111.11, BEFORE(ICD9=222.22, BEFORE(ICD9=333.33, ICD9=444.44)+(-100, 0)))", LanguageParser.parse(
        "BEFORE(ICD9=111.11, BEFORE(ICD9=222.22, BEFORE(ICD9=333.33, ICD9=444.44)+(-100, 0)))", false).toString());
  }

  @Test
  public void nested_variables() throws Exception {
    final String nestedExpression = "var podalv.aaa = ICD9=250.00\n" + //
        "var podalv.bbb = ICD9=255.00\n" + //
        "var podalv.ccc = AND($podalv.aaa, $podalv.bbb)\n" + //
        "$podalv.ccc";
    ScriptParser.setInstance(new ScriptParser(new ScriptStorage(new File("extractionTestData"))));
    Assert.assertEquals("AND(ICD9=250.00, ICD9=255.00)", LanguageParser.parse(nestedExpression.toUpperCase(), false).toString());
  }

  @Test
  public void comments() throws Exception {
    final String nestedExpression = "var podalv.aaa = ICD9=250.00 //some comments\n" + //
        "var podalv.bbb = /* nevermind */ ICD9=255.00\n" + //
        "/* multi\n" + //
        "line */\n" + //
        "var podalv.ccc = AND($podalv.aaa, $podalv.bbb)\n" + //
        "$podalv.ccc";
    ScriptParser.setInstance(new ScriptParser(new ScriptStorage(new File("extractionTestData"))));
    Assert.assertEquals("AND(ICD9=250.00, ICD9=255.00)", LanguageParser.parse(nestedExpression.toUpperCase(), false).toString());
  }

  @Test
  public void hashTagComments() throws Exception {
    final String nestedExpression = "var podalv.aaa = ICD9=250.00 #some comments\n" + //
        "var podalv.bbb = /* nevermind */ ICD9=255.00\n" + //
        "/* multi\n" + //
        "line */\n" + //
        "var podalv.ccc = AND($podalv.aaa, $podalv.bbb)\n" + //
        "$podalv.ccc";
    ScriptParser.setInstance(new ScriptParser(new ScriptStorage(new File("extractionTestData"))));
    Assert.assertEquals("AND(ICD9=250.00, ICD9=255.00)", LanguageParser.parse(nestedExpression.toUpperCase(), false).toString());
  }

  @Test
  public void outputTest() throws Exception {
    Assert.assertEquals("OUTPUT(ICD9=250.50)", LanguageParser.parse("OUTPUT(ICD9=250.50)", false).toString());
  }

  @Test
  public void snomedTest() throws Exception {
    Assert.assertEquals("SNOMED=123", LanguageParser.parse("SNOMED = 123", false).toString());
  }

  @Test
  public void npeTest() throws Exception {
    Assert.assertEquals("Expected a NUMERIC parameter, but found TEXT", LanguageParser.parse("SNOMED = \"aaa\"", false).toString());
  }

  @Test
  public void extraneousError() throws Exception {
    Assert.assertTrue(LanguageParser.parse("SNOMED = 123    SNOMED=122", false).toString().indexOf("Extraneous input. Cannot parse query") != -1);
  }

  @Test
  public void spacesTest() throws Exception {
    Assert.assertEquals("ICD9=123", LanguageParser.parse("ICD9  =  123", false).toString());
  }

  @Test
  public void warningInterval() throws Exception {
    Assert.assertEquals("[]", Arrays.toString(LanguageParser.parse("HISTORY OF(ICD9=250.00)", false).getWarning()));
  }

  @Test
  public void unusedVariable() throws Exception {
    Assert.assertEquals("A variable was defined but nothing was queried. To query the defined variable invoke it with a $ sign", LanguageParser.parse("VAR X = ICD9=250.50",
        false).toString());
  }

  @Test
  public void emptyQuery() throws Exception {
    Assert.assertEquals("Query string is empty", LanguageParser.parse("  ", false).toString());
  }

  @Test
  public void eCodes() throws Exception {
    Assert.assertEquals("ICD9=E220-E221.1", LanguageParser.parse("ICD9=E220-E221.1", false).toString());
  }

  @Test
  public void count() throws Exception {
    Assert.assertEquals("COUNT(ICD9=250.50, 150, 200)", LanguageParser.parse("COUNT(ICD9=250.50, 150, 200)", false).toString());
  }

  @Test
  public void vitals() throws Exception {
    Assert.assertEquals("VITALS(\"body mass index (bmi)\", 55.6, 55.6)", LanguageParser.parse("VITALS(\"Body mass index (BMI)\", 55.6, 55.6)", false).toString());
  }

  @Test
  public void tooLargeInteger() throws Exception {
    Assert.assertEquals("INTERVAL(2147483647, 2147483647)", LanguageParser.parse("INTERVAL(3000000000, 5000000000)", false).toString());
    Assert.assertEquals("COUNT(ICD9=250.50, MAX, MAX)", LanguageParser.parse("COUNT(ICD9=250.50, 3000000000, 5000000000)", false).toString());
    Assert.assertEquals("DURATION(ICD9=250.50, ALL, MAX, MAX)", LanguageParser.parse("DURATION(ICD9=250.50, ALL, 3000000000, 5000000000)", false).toString());
    Assert.assertEquals("EXTEND BY(ICD9=250.50, MAX, MAX)", LanguageParser.parse("RESIZE(ICD9=250.50, 3000000000, 5000000000)", false).toString());
    Assert.assertEquals("YEAR(-2147483648, 2147483647)", LanguageParser.parse("YEAR(3000000000, 5000000000)", false).toString());
    Assert.assertEquals("AGE(MAX, MAX)", LanguageParser.parse("AGE(3000000000, 5000000000)", false).toString());
    Assert.assertEquals("BEFORE(ICD9=250.50, ICD9=240.40)+(MIN, MAX)", LanguageParser.parse("BEFORE(ICD9=250.50, ICD9=240.40)+(-10000000000, 10000000000)", false).toString());
  }

  @Test
  public void additionalParameters() throws Exception {
    Assert.assertEquals("OUTPUT(ICD9=222.22)", LanguageParser.parse("ICD9=222.22", true).toString());
    Assert.assertEquals("OUTPUT(INTERSECT(PATIENTS(1, 2, 3), ICD9=222.22))", RootNode.parse("ICD9=222.22", true, new int[] {1, 2, 3}).toString());
  }

  @Test
  public void limit() throws Exception {
    Assert.assertEquals("LIMIT(100,ICD9=222.22)", LanguageParser.parse("LIMIT(100, ICD9=222.22)", false).toString());
    Assert.assertEquals("LIMIT(CACHED,ICD9=222.22)", LanguageParser.parse("LIMIT(CACHED, ICD9=222.22)", false).toString());
  }

  @Test
  public void estimate() throws Exception {
    Assert.assertEquals("ESTIMATE(10SECONDS,ICD9=222.22)", LanguageParser.parse("ESTIMATE(10 SECONDS, ICD9=222.22)", false).toString());
    Assert.assertEquals("ESTIMATE(10MINS,ICD9=222.22)", LanguageParser.parse("ESTIMATE(10 MINS, ICD9=222.22)", false).toString());
    Assert.assertEquals("ESTIMATE(100,ICD9=222.22)", LanguageParser.parse("ESTIMATE(100, ICD9=222.22)", false).toString());
    Assert.assertEquals("ESTIMATE(CACHED,ICD9=222.22)", LanguageParser.parse("ESTIMATE(CACHED, ICD9=222.22)", false).toString());
  }

  @Test
  public void topLevelNodes() throws Exception {
    Assert.assertEquals("OUTPUT(ESTIMATE(1000,ICD9=222.22))", LanguageParser.parse("OUTPUT(ESTIMATE(1000, ICD9=222.22))", false).toString());
    Assert.assertEquals("OUTPUT(ICD9=222.22)", LanguageParser.parse("OUTPUT(ICD9=222.22)", false).toString());
  }

  @Test
  public void hasAnyIcd9Codes() throws Exception {
    Assert.assertEquals("ICD9", LanguageParser.parse("ICD9", false).toString());
    Assert.assertEquals("PRIMARY(ICD9)", LanguageParser.parse("PRIMARY(ICD9)", false).toString());
  }

  @Test
  public void hasAnyCptCodes() throws Exception {
    Assert.assertEquals("CPT", LanguageParser.parse("CPT", false).toString());
  }

  @Test
  public void hasAnyRxCodes() throws Exception {
    Assert.assertEquals("RX", LanguageParser.parse("RX", false).toString());
  }

  @Test
  public void hasAnySnomedCodes() throws Exception {
    Assert.assertEquals("SNOMED", LanguageParser.parse("SNOMED", false).toString());
  }

  @Test
  public void hasAnyLabs() throws Exception {
    Assert.assertEquals("LABS", LanguageParser.parse("LABS", false).toString());
  }

  @Test
  public void hasAnyVitals() throws Exception {
    Assert.assertEquals("VITALS", LanguageParser.parse("VITALS", false).toString());
  }

  @Test
  public void resize() throws Exception {
    Assert.assertEquals("EXTEND BY(DEATH, START+1, END+525600)", LanguageParser.parse("EXTEND BY(DEATH, START + 1, END + 1 YEAR)", false).toString());
    Assert.assertEquals("EXTEND BY(DEATH, -525600, -10080)", LanguageParser.parse("EXTEND BY(DEATH, -1 week, -1 year)", false).toString());
  }

  @Test
  public void hcpcs() throws Exception {
    Assert.assertEquals("CPT=V1205", LanguageParser.parse("CPT=V1205", false).toString());
    Assert.assertEquals("CPT=A1205", LanguageParser.parse("CPT=A1205", false).toString());
    Assert.assertEquals("CPT=B1205", LanguageParser.parse("CPT=B1205", false).toString());
    Assert.assertEquals("CPT=C1205", LanguageParser.parse("CPT=C1205", false).toString());
  }

  @Test
  public void csv() throws Exception {
    Assert.assertEquals("CSV(INTERSECT(ICD9=250.00, CPT=23456),\"ABC\"=ICD9=220.30,CPT=22222,\"GHI\"=CPT=33333,ICD9,CPT)", LanguageParser.parse(
        "CSV(INTERSECT(ICD9=250.00, CPT=23456), \"ABC\"=ICD9=220.30, CPT=22222, \"GHI\"=CPT=33333,ICD9,CPT)", false).toString());
  }

  @Test
  public void csv_simple() throws Exception {
    Assert.assertEquals("CSV(ICD9=250.50,CPT,VITALS)", LanguageParser.parse("CSV(ICD9=250.50, CPT, VITALS)", false).toString());
  }

  @Test
  public void csv_time() throws Exception {
    Assert.assertEquals("CSV(INTERSECT(ICD9=250.00, CPT=23456),TIME=ICD9=222,\"ABC\"=ICD9=220.30,CPT=22222,\"GHI\"=CPT=33333,ICD9,CPT)", LanguageParser.parse(
        "CSV(INTERSECT(ICD9=250.00, CPT=23456), TIME=ICD9=222, \"ABC\"=ICD9=220.30, CPT=22222, \"GHI\"=CPT=33333,ICD9,CPT)", false).toString());
  }

  @Test
  public void csv_start_no_end() throws Exception {
    Assert.assertEquals(
        "CSV(INTERSECT(ICD9=250.00, CPT=23456),START=START(FIRST MENTION(ICD9=222)),END=END(LAST MENTION(TIMELINE)),\"ABC\"=ICD9=220.30,CPT=22222,\"GHI\"=CPT=33333,ICD9,CPT)",
        LanguageParser.parse("CSV(INTERSECT(ICD9=250.00, CPT=23456), START=ICD9=222, \"ABC\"=ICD9=220.30, CPT=22222, \"GHI\"=CPT=33333,ICD9,CPT)", false).toString());
  }

  @Test
  public void csv_start_and_end() throws Exception {
    Assert.assertEquals(
        "CSV(INTERSECT(ICD9=250.00, CPT=23456),START=START(FIRST MENTION(ICD9=222)),END=END(LAST MENTION(ICD9=333)),\"ABC\"=ICD9=220.30,CPT=22222,\"GHI\"=CPT=33333,ICD9,CPT)",
        LanguageParser.parse("CSV(INTERSECT(ICD9=250.00, CPT=23456), START=ICD9=222, END=ICD9=333, \"ABC\"=ICD9=220.30, CPT=22222, \"GHI\"=CPT=33333,ICD9,CPT)", false).toString());
  }

  @Test
  public void csv_end_no_start() throws Exception {
    Assert.assertEquals(
        "CSV(INTERSECT(ICD9=250.00, CPT=23456),START=START(FIRST MENTION(TIMELINE)),END=END(LAST MENTION(ICD9=333)),\"ABC\"=ICD9=220.30,CPT=22222,\"GHI\"=CPT=33333,ICD9,CPT)",
        LanguageParser.parse("CSV(INTERSECT(ICD9=250.00, CPT=23456), END=ICD9=333, \"ABC\"=ICD9=220.30, CPT=22222, \"GHI\"=CPT=33333,ICD9,CPT)", false).toString());
  }

  @Test
  public void timeParse() throws Exception {
    System.out.println(LanguageParser.parse("(ICD9=250.50*) BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("(ICD9=250.50* AND ICD9=220.22) BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("NO (ICD9=250.50) BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("NO (ICD9=250.50 AND ICD9=220.22) BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("(ICD9=250.50*) 3 MONTHS BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("(ICD9=250.50* AND ICD9=220.22) 3 MONTHS BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("NO (ICD9=250.50) 3 MONTHS BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("NO (ICD9=250.50 AND ICD9=220.22) 3 MONTHS BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("(ICD9=250.50* AND ICD9=220.22) AND NO (ICD9=221.21) BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("(ICD9=250.50* AND ICD9=220.22) AND NO (ICD9=221.21) 3 MONTHS BEFORE ICD9=220.10*", false).toString());
    System.out.println(LanguageParser.parse("(ICD9=250.50* AND ICD9=220.22) AND NO (ICD9=221.21) 3 MONTHS* BEFORE ICD9=220.10*", false).toString());
  }

  @Test
  public void specialQuotationMarks() throws Exception {
    Assert.assertEquals("GENDER=\"MALE\"", LanguageParser.parse("GENDER=“MALE”", false).toString());
  }

  @Test
  public void allIcd10() throws Exception {
    final Icd10Hierarchy hier = Icd10Hierarchy.create();
    final Iterator<String> codes = hier.getCodes();
    while (codes.hasNext()) {
      final String code = codes.next();
      Assert.assertEquals("ICD10=" + code, LanguageParser.parse("ICD10=" + code, false).toString());
    }
  }

  @Test
  public void allIcd9() throws Exception {
    final Icd9Hierarchy hier = Icd9Hierarchy.create();
    final Iterator<String> codes = hier.getCodes();
    while (codes.hasNext()) {
      final String code = codes.next();
      Assert.assertEquals("ICD9=" + code, LanguageParser.parse("ICD9=" + code, false).toString());
    }
  }

  @Test
  public void department() throws Exception {
    Assert.assertEquals("DEPARTMENT=\"" + "AAABBB" + "\"", LanguageParser.parse("DEPARTMENT=\"AAABBB\"", false).toString());
    Assert.assertEquals("DEPARTMENT=\"" + "AAA BBB" + "\"", LanguageParser.parse("DEPT=\"AAA BBB\"", false).toString());
  }

  @Test
  public void contains() throws Exception {
    Assert.assertEquals("BEFORE(ICD9=220.20, ICD9=250.50)+<>(START, END)", LanguageParser.parse("CONTAINS(ICD9=250.50, ICD9=220.20)", false).toString());
  }

  @Test
  public void years() throws Exception {
    Assert.assertEquals("YEAR(2001, 2010)", LanguageParser.parse("YEAR(2001, 2010)", false).toString());
    Assert.assertEquals("YEAR(2001, 2010)", LanguageParser.parse("YEARS(2001, 2010)", false).toString());
  }

  @Test
  public void labsWithValue() throws Exception {
    Assert.assertEquals("LABS(\"aaa\", 2000.0, 2001.0)", LanguageParser.parse("LABS(\"aaa\", 2000, 2001)", false).toString());
    Assert.assertEquals("LABS(\"aaa\", 2000.0, 2001.0)", LanguageParser.parse("LAB(\"aaa\", 2000, 2001)", false).toString());
  }

  @Test
  public void variableStartsWithAll() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (ALL) {A = ALL;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(ALL'") != -1);
  }

  @Test
  public void variableStartsWithAllx() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (ALLX) {A = ALLX;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(ALL'") != -1);
  }

  @Test
  public void variableStartsWithSingle() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (SINGLE) {A = SINGLE;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(SINGLE'") != -1);
  }

  @Test
  public void variableStartsWithSinglex() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (SINGLEX) {A = SINGLEX;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(SINGLE'") != -1);
  }

  @Test
  public void variableStartsWithPrior() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (PRIOR) {A = PRIOR;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(PRIOR'") != -1);
  }

  @Test
  public void variableStartsWithPriorx() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (PRIORX) {A = PRIORX;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(PRIOR'") != -1);
  }

  @Test
  public void variableEndsWithPriorx() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (X_PRIOR) {A = X_PRIOR;} ICD9=222.22", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(X_PRIOR'") != -1);
  }

  @Test
  public void variableEndsWithMin() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (X_MIN) {A = X_MIN;RETURN A AS A;}A", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(X_MIN'") != -1);
  }

  @Test
  public void variableEndsWithCache() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (SOMETHINGCACHE) {RETURN SOMETHINGCACHE AS SOMETHING;} SOMETHING", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(SOMETHINGCACHE'") != -1);
  }

  @Test
  public void variableEndsWithMax() throws Exception {
    final String errorMessage = LanguageParser.parse("FOR EACH (ICD9=220.20) AS (X_MAX) {A = X_MAX;} ICD9=222.20", false).toString();
    Assert.assertTrue(errorMessage.indexOf("Variable name cannot contain the specified keyword near 'FOREACH(ICD9=220.20)AS(X_MAX'") != -1);
  }

  @Test
  public void countNode() throws Exception {
    Assert.assertEquals("COUNT(ICD9=250.50, ICD9=251.21*, SINGLE, 3, 4)", LanguageParser.parse("COUNT(ICD9=250.50, ICD9=251.21*, SINGLE, 3, 4)", false).toString());
    Assert.assertEquals("COUNT(ICD9=250.50, ICD9=251.21, SINGLE, 3, 4)", LanguageParser.parse("COUNT(ICD9=250.50*, ICD9=251.21, SINGLE, 3, 4)", false).toString());
    Assert.assertEquals("COUNT(ICD9=250.50*, ICD9=251.21*, SINGLE, 3, 4)", LanguageParser.parse("COUNT(ICD9=250.50*, ICD9=251.21*, SINGLE, 3, 4)", false).toString());
  }

  @Test
  public void invalidWhitespaceCharacter() throws Exception {
    Assert.assertEquals("LABS(\"aaa\", 2000.0, 2001.0)", LanguageParser.parse("LABS(\"aaa\", 2000, 2001)", false).toString());
  }

  @Test
  public void forEachNeverDeclared() throws Exception {
    Assert.assertEquals("[68,70:0] Variables [EEE, DDD] are never declared", LanguageParser.parse("FOR EACH (ICD9=222.22) AS (ABC) {RETURN DEF AS CCC;} INTERSECT(DDD, EEE)",
        false).toString());
    Assert.assertEquals("[63,65:0] Variable EEE is never declared", LanguageParser.parse("FOR EACH (ICD9=222.22) AS (ABC) {RETURN DEF AS CCC;} INTERSECT(EEE)", false).toString());
  }

  @Test
  public void forEachNothingReturned() throws Exception {
    Assert.assertEquals("FOR EACH (ICD9=222.22) AS (ABC) {}ICD9=223.33", LanguageParser.parse("FOR EACH (ICD9=222.22) AS (ABC) {} ICD9=223.33", false).toString());
  }

  @Test
  public void forEachUnknownAtom() throws Exception {
    Assert.assertEquals("FOR EACH (ICD9=222.22) AS (ABC) {RETURN DEF AS DDD;}FOR EACH (ICD9=222.22) AS (ABC) {RETURN DEF AS CCC;}INTERSECT(CCC, DDD)", LanguageParser.parse(
        "FOR EACH (ICD9=222.22) AS (ABC) {RETURN DEF AS DDD;} FOR EACH (ICD9=222.22) AS (ABC) {RETURN DEF AS CCC;} INTERSECT(CCC, DDD)", false).toString());
  }

  @Test
  public void print() throws Exception {
    Assert.assertEquals("FOR EACH (ICD9=222.22) AS (ABC) {PRINT(\"AAA\")}ICD9=222.22", LanguageParser.parse("FOR EACH (ICD9=222.22) AS (ABC) {PRINT(\"AAA\");}ICD9=222.22",
        false).toString());
  }

  @Test
  public void printVariable() throws Exception {
    Assert.assertEquals("FOR EACH (ICD9=222.22) AS (ABC) {PRINT(ABC)}ICD9=222.22", LanguageParser.parse("FOR EACH (ICD9=222.22) AS (ABC) {PRINT(ABC);}ICD9=222.22",
        false).toString());
  }

  @Test
  public void incorrectCsv() throws Exception {
    Assert.assertEquals("CSV(ICD9=250.50,TIME=BEFORE(RX=7052, ICD9=250.50*)+(-43200, 0),ICD9=250.50,ICD9,ICD9=220.20,CPT)", LanguageParser.parse(
        "CSV(ICD9=250.50, TIME=BEFORE(Rx=7052,ICD9=250.50*)+(0, -30 days),ICD9=250.50, ICD9, ICD9=220.20, CPT)", false).toString());
  }

  @Test
  public void incorrectCsv2() throws Exception {
    Assert.assertEquals("CSV(ICD9=250.50,START=START(FIRST MENTION(BEFORE(RX=7052, ICD9=250.50*)+(-43200, 0))),END=END(LAST MENTION(TIMELINE)),ICD9=250.50,ICD9,ICD9=220.20,CPT)",
        LanguageParser.parse("CSV(ICD9=250.50, START=BEFORE(Rx=7052,ICD9=250.50*)+(0, -30 days),ICD9=250.50, ICD9, ICD9=220.20, CPT)", false).toString());
  }

  @Test
  public void incorrectCsv3() throws Exception {
    Assert.assertEquals("CSV(ICD9=250.50,START=START(FIRST MENTION(TIMELINE)),END=END(LAST MENTION(BEFORE(RX=7052, ICD9=250.50*)+(-43200, 0))),ICD9=250.50,ICD9,ICD9=220.20,CPT)",
        LanguageParser.parse("CSV(ICD9=250.50, END=BEFORE(Rx=7052,ICD9=250.50*)+(0, -30 days),ICD9=250.50, ICD9, ICD9=220.20, CPT)", false).toString());
  }

  @Test
  public void incorrectCsv4() throws Exception {
    Assert.assertEquals(
        "CSV(ICD9=250.50,START=START(FIRST MENTION(ICD9=250.50)),END=END(LAST MENTION(BEFORE(RX=7052, ICD9=250.50*)+(-43200, 0))),ICD9=250.50,ICD9,ICD9=220.20,CPT)",
        LanguageParser.parse("CSV(ICD9=250.50, START=ICD9=250.50, END=BEFORE(Rx=7052,ICD9=250.50*)+(0, -30 days),ICD9=250.50, ICD9, ICD9=220.20, CPT)", false).toString());
  }

  @Test
  public void dump() throws Exception {
    Assert.assertEquals("DUMP(ICD9=222.22,\"AAA\")", LanguageParser.parse("DUMP(ICD9=222.22, \"AAA\")", false).toString());
  }

}