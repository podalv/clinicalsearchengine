package com.podalv.search.language;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.podalv.search.language.node.RootNode;
import com.podalv.search.language.script.LanguageParser;

public class LanguageNodeWarnings {

  private void assertWarning(final String query, final String ... warnings) {
    final RootNode node = LanguageParser.parse(query, false);
    final String[] actual = node.getWarning();
    Assert.assertEquals(actual.length, warnings.length);
    for (int x = 0; x < actual.length; x++) {
      boolean fnd = false;
      for (int y = 0; y < warnings.length; y++) {
        if (warnings[y].equals(actual[x])) {
          fnd = true;
          break;
        }
      }
      Assert.assertTrue(fnd);
    }
    Arrays.sort(warnings);
    Arrays.sort(node.getWarning());
    Assert.assertArrayEquals(warnings, node.getWarning());
  }

  @Test
  public void union() throws Exception {
    assertWarning("UNION(OR(ICD9=250.00), TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT, ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    assertWarning("UNION(ICD9=250.00, TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
  }

  @Test
  public void before() throws Exception {
    assertWarning("BEFORE(ICD9=250.00, TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    assertWarning("BEFORE(OR(ICD9=250.00), TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT, ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
  }

  @Test
  public void not() throws Exception {
    assertWarning("NOT(TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_EMPTY);
  }

  @Test
  public void invert() throws Exception {
    assertWarning("INVERT(OR(ICD9=250.00))", ErrorMessages.TEMPORAL_COMMAND_EMPTY);
    assertWarning("INVERT(TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_EMPTY);
  }

  @Test
  public void firstMention() throws Exception {
    assertWarning("FIRST MENTION(OR(ICD9=250.00))", ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT);
    assertWarning("FIRST MENTION(TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
  }

  @Test
  public void lastMention() throws Exception {
    assertWarning("LAST MENTION(OR(ICD9=250.00))", ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT);
    assertWarning("LAST MENTION(TIMELINE)", ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
  }

  @Test
  public void before_intersect_boolean() throws Exception {
    assertWarning("BEFORE(INTERSECT(OR(ICD9=250.00)), ICD9=220.20)", ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
  }

  @Test
  public void extendBy() throws Exception {
    assertWarning("EXTEND BY(OR(ICD9=250.00), -10, -10))", ErrorMessages.TEMPORAL_COMMAND_EXTEND_BY);
    assertWarning("EXTEND BY(TIMELINE, -10, -10)", ErrorMessages.TEMPORAL_COMMAND_EXTEND_BY);
  }

  @Test
  public void complex() throws Exception {
    assertWarning("EXTEND BY(UNION(ICD9=200.20, INTERSECT(OR(ICD9=250.00))), -10, -10))", ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
  }

  @Test
  public void interval() throws Exception {
    assertWarning("INTERVAL(OR(ICD9=200.20), OR(ICD9=250.50))", ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    assertWarning("INTERVAL(INTERSECT(OR(ICD9=200.20)), OR(ICD9=250.50))", ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    assertWarning("INTERVAL(END(INTERSECT(OR(ICD9=200.20))), END(OR(ICD9=250.50)))");
  }

}
