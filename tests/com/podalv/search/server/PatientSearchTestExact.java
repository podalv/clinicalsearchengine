package com.podalv.search.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.utils.file.FileUtils;

public class PatientSearchTestExact {

  private final static int  TIME1       = 100;
  private final static int  TIME2       = 200;
  private final static int  TIME3       = 150;
  private final static int  TIME4       = 250;
  private final static int  TIME5       = 125;
  private final static int  TIME6       = 135;
  private static final File DATA_FOLDER = new File(new File("."), "testData");

  @Before
  public void setup() {
    PatientExport.setInstance(new PatientExportTestInstance(DATA_FOLDER));
    final File[] files = DATA_FOLDER.listFiles();
    if (files != null) {
      for (final File file : files) {
        if (!file.getAbsolutePath().endsWith("dictionary")) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
  }

  @After
  public void tearDown() {
    setup();
  }

  private static ClinicalSearchEngine prepareSmokeTestDatabase() throws IOException, InterruptedException, InstantiationException, IllegalAccessException {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    indices.addDemographicRecord(new DemographicsRecord(0, "male", "asian", "other", 50000, -1));

    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(100000, 4, DATA_FOLDER);
    final PatientBuilder patient = new PatientBuilder(indices);
    patient.setId(0);
    final PatientRecord<VisitRecord> visits = new PatientRecord<>(0);
    visits.add(new VisitRecord(1, 2002, TIME1, TIME2 - TIME1, "ICD", "100.1", "Outpatient"));
    visits.add(new VisitRecord(1, 2002, TIME3, TIME4 - TIME3, "ICD", "100.2", "Inpatient"));

    final PatientRecord<LabsRecord> labs = new PatientRecord<>(0);
    labs.add(new LabsRecord("WBC", TIME5, 3.5, 1.5, 1.7, null));
    labs.add(new LabsRecord("WBC", TIME6, 1.6, 1.5, 1.7, null));
    patient.recordLabs(labs);
    patient.recordVisit(visits);
    patient.close();
    generator.add(patient);
    final Statistics statistics = new Statistics();
    for (int x = 0; x < 4000; x++) {
      statistics.addCpt(x, 0);
      statistics.addIcd9(x, 0);
      statistics.addRxNorm(x, 0);
      statistics.addLabs(x, 0);
      statistics.addVisitType(x, 0);
    }
    statistics.recordPatientId(patient);

    generator.close();
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "indices")));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();
    final BufferedOutputStream statsCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "statistics")));
    statistics.save(new StreamOutput(statsCollection));
    statsCollection.close();

    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
  }

  private static void assertCommand(final ClinicalSearchEngine engine, final String command, final int expectedStart, final int expectedEnd) {
    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("IDENTICAL(" + command + ", " + getInterval(expectedStart, expectedEnd) + ")"),
        Integer.MAX_VALUE);
    Assert.assertNull(response.getErrorMessage());
    PatientSearchTest.assertContainsAll(response, new double[] {0});
  }

  private static void assertCommandMultiple(final ClinicalSearchEngine engine, final String command, final int ... times) {
    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("IDENTICAL(" + command + ", " + getIntervalMultiple(times) + ")"), Integer.MAX_VALUE);
    Assert.assertNull(response.getErrorMessage());
    PatientSearchTest.assertContainsAll(response, new double[] {0});
  }

  private static void assertEmpty(final ClinicalSearchEngine engine, final String command) {
    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("IDENTICAL(" + command + ", NULL)"), Integer.MAX_VALUE);
    Assert.assertNull(response.getErrorMessage());
    PatientSearchTest.assertContainsAll(response, new double[] {0});
  }

  private static String getIntervalMultiple(final int ... values) {
    final StringBuilder result = new StringBuilder("UNION(");
    for (int x = 0; x < values.length; x += 2) {
      if (x != 0) {
        result.append(", ");
      }
      result.append("INTERVAL(" + values[x] + ", " + values[x + 1] + ")");
    }
    return result.toString() + ")";
  }

  private static String getInterval(final int start, final int end) {
    return "INTERVAL(" + start + ", " + end + ")";
  }

  @Test
  public void positive() throws Exception {
    final ClinicalSearchEngine engine = prepareSmokeTestDatabase();

    //ICD9, INTERSECT, UNION, LABS, TIMELINE, ENCOUNTERS, INPATIENT, OUTPATIENT, YEAR
    //ICD9=100.1 [100, 200]
    //ICD9=100.2 [150, 250]
    //WBC        [125, 135]
    assertCommandMultiple(engine, "LABS(\"WBC\")", TIME5, TIME5, TIME6, TIME6);
    assertCommand(engine, "ICD9=100.1", TIME1, TIME2);
    assertCommand(engine, "EXTEND BY(LABS(\"WBC\"), END+10, START-10)", TIME5 - 10, TIME6 + 10);
    assertEmpty(engine, "NOT(ICD9=100.1)");
    assertCommand(engine, "VISIT TYPE=\"INPATIENT\"", TIME3, TIME4);
    assertCommand(engine, "VISIT TYPE=\"OUTPATIENT\"", TIME1, TIME2);
    assertCommand(engine, "INTERSECT(ICD9=100.1, ICD9=100.2)", TIME3, TIME2);
    assertCommand(engine, "UNION(ICD9=100.1, ICD9=100.2)", TIME1, TIME4);
    assertCommand(engine, "TIMELINE", TIME1, TIME4);
    assertCommand(engine, "ENCOUNTERS", TIME1, TIME4);
    assertCommand(engine, "VISIT TYPE=\"INPATIENT\"", TIME3, TIME4);
    assertCommand(engine, "VISIT TYPE=\"OUTPATIENT\"", TIME1, TIME2);
    assertCommand(engine, "INTERSECT(VISIT TYPE=\"INPATIENT\", VISIT TYPE=\"OUTPATIENT\")", TIME3, TIME2);
    assertCommand(engine, "UNION(VISIT TYPE=\"INPATIENT\", VISIT TYPE=\"OUTPATIENT\")", TIME1, TIME4);
    assertCommand(engine, "YEAR(2002, 2002)", TIME1, TIME4);

    engine.close();
  }

  @Test
  public void negative() throws Exception {
    final ClinicalSearchEngine engine = prepareSmokeTestDatabase();

    //ICD9, INTERSECT, UNION, LABS, TIMELINE, ENCOUNTERS, INPATIENT, OUTPATIENT, YEAR
    //ICD9=100.1 [100, 200]
    //ICD9=100.2 [150, 250]
    //WBC        [125, 135]
    assertEmpty(engine, "YEAR(2001, 2001)");

    engine.close();
  }

}