package com.podalv.search.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.index.StringIntegerFastIndex;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.utils.file.FileUtils;

public class TermToConceptMapTest {

  private static final File DATA_FOLDER = new File(new File("."), "testData");

  @Before
  public void setup() {
    PatientExport.setInstance(new PatientExportTestInstance(DATA_FOLDER));
    final File[] files = DATA_FOLDER.listFiles();
    if (files != null) {
      for (final File file : files) {
        if (!file.getAbsolutePath().endsWith("dictionary")) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
  }

  @After
  public void tearDown() {
    setup();
  }

  private static ClinicalSearchEngine prepareTextTestDatabase() throws IOException, InterruptedException, InstantiationException, IllegalAccessException, SQLException {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    indices.addDemographicRecord(new DemographicsRecord(0, "male", "asian", "other", 50000, -1));
    indices.addDemographicRecord(new DemographicsRecord(1, "female", "asian", "other", 50000, -1));
    final StringIntegerFastIndex docDescription = new StringIntegerFastIndex();
    docDescription.add("first");
    docDescription.add("second");
    indices.setDocDescription(docDescription);
    indices.addTid(0, "stroke");
    indices.addTid(1, "influenza");
    indices.addIcd9Code("250.00");

    final Statistics statistics = new Statistics();
    for (int x = 0; x < 6; x++) {
      statistics.addTid(indices.getTid("stroke"), 0);
      statistics.addTid(indices.getTid("influenza"), 1);
    }
    statistics.addNoteType(0, 0);
    for (int x = 1; x < 10; x++) {
      for (int y = 0; y < 10; y++) {
        statistics.addIcd9(y, x);
      }
    }

    for (int x = 11; x < 15; x++) {
      for (int y = 1; y < 15; y++) {
        statistics.addCpt(y, x);
      }
    }

    for (int x = 16; x < 20; x++) {
      for (int y = 16; y < 20; y++) {
        statistics.addAtc(y, x);
      }
    }

    for (int x = 21; x < 25; x++) {
      for (int y = 21; y < 25; y++) {
        statistics.addLabs(y, x);
      }
    }

    for (int x = 26; x < 30; x++) {
      for (int y = 26; y < 30; y++) {
        statistics.addNoteType(y, x);
      }
    }

    for (int x = 31; x < 35; x++) {
      for (int y = 31; y < 35; y++) {
        statistics.addRxNorm(y, x);
      }
    }

    for (int x = 36; x < 40; x++) {
      for (int y = 36; y < 40; y++) {
        statistics.addVisitType(y, x);
      }
    }

    for (int x = 41; x < 45; x++) {
      for (int y = 41; y < 45; y++) {
        statistics.addVitals(y, x);
      }
    }

    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(100000, 1, DATA_FOLDER);
    PatientBuilder patient = new PatientBuilder(indices);
    patient.setId(0);

    PatientRecord<TermRecord> texts = new PatientRecord<>(0);
    texts.add(new TermRecord(indices.getTid("stroke"), 1, 0, 0, 1));

    for (int x = 0; x < texts.getRecords().size(); x++) {
      texts.getRecords().get(x).setAge(20);
      texts.getRecords().get(x).setYear(2012);
    }

    patient.recordTerms(texts);
    patient.close();
    statistics.recordPatientId(patient);
    generator.add(patient);

    patient = new PatientBuilder(indices);

    patient.setId(1);

    texts = new PatientRecord<>(1);
    texts.add(new TermRecord(indices.getTid("influenza"), 2, 0, 0, 1));

    for (int x = 0; x < texts.getRecords().size(); x++) {
      texts.getRecords().get(x).setAge(50);
      texts.getRecords().get(x).setYear(2015);
    }

    patient.recordTerms(texts);
    patient.close();
    statistics.recordPatientId(patient);
    generator.add(patient);

    generator.close();
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "indices")));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();
    final BufferedOutputStream statsCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "statistics")));
    statistics.save(new StreamOutput(statsCollection));
    statsCollection.close();

    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
  }

  @Test
  public void smokeTest() throws Exception {
    final ClinicalSearchEngine engine = prepareTextTestDatabase();

    PatientSearchResponse response = engine.search(PatientSearchRequest.create("TEXT=\"stroke\")"), Integer.MAX_VALUE);
    PatientSearchTest.assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("TEXT=\"influenza\")"), Integer.MAX_VALUE);
    PatientSearchTest.assertContainsAll(response, new double[] {1});

    for (int x = 1; x < 10; x++) {
      for (int y = 0; y < 10; y++) {
        Assert.assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9}, IntArrayList.create(engine.getStatistics().getIcd9Patients(y)).toArray());
      }
    }

    for (int x = 11; x < 15; x++) {
      for (int y = 1; y < 15; y++) {
        Assert.assertArrayEquals(new int[] {11, 12, 13, 14}, IntArrayList.create(engine.getStatistics().getCptPatients(y)).toArray());
      }
    }

    for (int x = 16; x < 20; x++) {
      for (int y = 16; y < 20; y++) {
        Assert.assertArrayEquals(new int[] {16, 17, 18, 19}, IntArrayList.create(engine.getStatistics().getAtcPatients(y)).toArray());
      }
    }

    for (int x = 21; x < 25; x++) {
      for (int y = 21; y < 25; y++) {
        Assert.assertArrayEquals(new int[] {21, 22, 23, 24}, IntArrayList.create(engine.getStatistics().getLabsPatients(y)).toArray());
      }
    }

    for (int x = 26; x < 30; x++) {
      for (int y = 26; y < 30; y++) {
        Assert.assertArrayEquals(new int[] {26, 27, 28, 29}, IntArrayList.create(engine.getStatistics().getNoteTypesPatients(y)).toArray());
      }
    }

    for (int x = 31; x < 35; x++) {
      for (int y = 31; y < 35; y++) {
        Assert.assertArrayEquals(new int[] {31, 32, 33, 34}, IntArrayList.create(engine.getStatistics().getRxNormPatients(y)).toArray());
      }
    }

    for (int x = 36; x < 40; x++) {
      for (int y = 36; y < 40; y++) {
        Assert.assertArrayEquals(new int[] {36, 37, 38, 39}, IntArrayList.create(engine.getStatistics().getVisitTypesPatients(y)).toArray());
      }
    }

    for (int x = 41; x < 45; x++) {
      for (int y = 41; y < 45; y++) {
        Assert.assertArrayEquals(new int[] {41, 42, 43, 44}, IntArrayList.create(engine.getStatistics().getVitalsPatients(y)).toArray());
      }
    }
  }

}
