package com.podalv.search.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.utils.file.FileUtils;

public class BooleanNotesTest {

  private final static int  TIME1       = 100;
  private final static int  TIME2       = 200;
  private final static int  TIME3       = 150;
  private static final File DATA_FOLDER = new File(new File("."), "testData");

  @Before
  public void setup() {
    PatientExport.setInstance(new PatientExportTestInstance(DATA_FOLDER));
    final File[] files = DATA_FOLDER.listFiles();
    if (files != null) {
      for (final File file : files) {
        if (!file.getAbsolutePath().endsWith("dictionary")) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
  }

  @After
  public void tearDown() {
    setup();
  }

  public static void assertContainsAll(final LinkedList<double[]> values, final double[] expected) {
    Assert.assertEquals(expected.length, values.size());
    for (int x = 0; x < expected.length; x++) {
      boolean fnd = false;
      for (final double[] value : values) {
        if (Double.compare(value[0], expected[x]) == 0) {
          fnd = true;
          break;
        }
      }
      if (!fnd) {
        Assert.fail();
      }
    }
  }

  private static ClinicalSearchEngine prepareSmokeTestDatabase() throws IOException, InterruptedException, InstantiationException, IllegalAccessException {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    indices.addDemographicRecord(new DemographicsRecord(0, "male", "asian", "other", 50000, 2012));

    final AtomicInteger tid = new AtomicInteger();

    final Statistics statistics = new Statistics();
    indices.addTid(tid.incrementAndGet(), "diabetes");
    indices.addTid(tid.incrementAndGet(), "stroke");
    indices.addTid(tid.incrementAndGet(), "influenza");
    indices.addTid(tid.incrementAndGet(), "treat");
    indices.addTid(tid.incrementAndGet(), "toxoplasmosis");
    statistics.addTid(indices.getTid("diabetes"), 0);
    statistics.addTid(indices.getTid("stroke"), 0);
    statistics.addTid(indices.getTid("influenza"), 0);
    statistics.addTid(indices.getTid("treat"), 0);
    statistics.addTid(indices.getTid("toxoplasmosis"), 0);

    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(100000, 4, DATA_FOLDER);
    final PatientBuilder patient = new PatientBuilder(indices);
    patient.setId(0);
    final PatientRecord<TermRecord> terms = new PatientRecord<>(0);
    terms.add(new TermRecord(indices.getTid("diabetes"), 0, 0, 0, 1));
    terms.add(new TermRecord(indices.getTid("stroke"), 0, 0, 0, 1));
    terms.add(new TermRecord(indices.getTid("influenza"), 1, 0, 0, 2));
    terms.add(new TermRecord(indices.getTid("treat"), 1, 0, 0, 2));
    terms.add(new TermRecord(indices.getTid("toxoplasmosis"), 2, 0, 0, 2));

    terms.getRecords().get(0).setAge(TIME1);
    terms.getRecords().get(1).setAge(TIME1);
    terms.getRecords().get(2).setAge(TIME2);
    terms.getRecords().get(3).setAge(TIME2);
    terms.getRecords().get(4).setAge(TIME3);

    terms.getRecords().get(0).setYear(2002);
    terms.getRecords().get(1).setYear(2002);
    terms.getRecords().get(2).setYear(2002);
    terms.getRecords().get(3).setYear(2002);
    terms.getRecords().get(4).setYear(2002);

    patient.recordTerms(terms);
    patient.close();
    generator.add(patient);
    statistics.recordPatientId(patient);

    generator.close();
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "indices")));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();
    final BufferedOutputStream statsCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "statistics")));
    statistics.save(new StreamOutput(statsCollection));
    statsCollection.close();

    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
  }

  private static void assertCommand(final ClinicalSearchEngine engine, final String command, final int expectedStart, final int expectedEnd) {
    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("IDENTICAL(" + command + ", " + getInterval(expectedStart, expectedEnd) + ")"),
        Integer.MAX_VALUE);
    Assert.assertNull(response.getErrorMessage());
    assertContainsAll(response.getPatientIds(), new double[] {0});
  }

  private static void assertCommandMultiple(final ClinicalSearchEngine engine, final String command, final int ... times) {
    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("IDENTICAL(" + command + ", " + getIntervalMultiple(times) + ")"), Integer.MAX_VALUE);
    Assert.assertNull(response.getErrorMessage());
    assertContainsAll(response.getPatientIds(), new double[] {0});
  }

  private static void assertEmpty(final ClinicalSearchEngine engine, final String command) {
    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("IDENTICAL(" + command + ", NULL)"), Integer.MAX_VALUE);
    Assert.assertNull(response.getErrorMessage());
    assertContainsAll(response.getPatientIds(), new double[] {0});
  }

  private static String getIntervalMultiple(final int ... values) {
    final StringBuilder result = new StringBuilder("UNION(");
    for (int x = 0; x < values.length; x += 2) {
      if (x != 0) {
        result.append(", ");
      }
      result.append("INTERVAL(" + values[x] + ", " + values[x + 1] + ")");
    }
    return result.toString() + ")";
  }

  private static String getInterval(final int start, final int end) {
    return "INTERVAL(" + start + ", " + end + ")";
  }

  @Test
  public void positive() throws Exception {
    final ClinicalSearchEngine engine = prepareSmokeTestDatabase();

    assertCommand(engine, "NOTES(AND(TEXT=\"diabetes\", TEXT=\"stroke\"))", TIME1, TIME1);
    assertCommand(engine, "NOTES(AND(TEXT=\"diabetes\"))", TIME1, TIME1);
    assertCommand(engine, "NOTES(AND(TEXT=\"influenza\", TEXT=\"treat\"))", TIME2, TIME2);
    assertCommandMultiple(engine, "NOTES(OR(TEXT=\"diabetes\", TEXT=\"toxoplasmosis\", TEXT=\"influenza\", TEXT=\"treat\"))", TIME1, TIME1, TIME3, TIME3, TIME2, TIME2);
    assertCommandMultiple(engine, "NOTES(NOT(TEXT=\"diabetes\"))", TIME2, TIME2, TIME3, TIME3);
    assertCommandMultiple(engine, "NOTES(OR(AND(TEXT=\"diabetes\", TEXT=\"stroke\"), AND(TEXT=\"influenza\", TEXT=\"treat\")))", TIME1, TIME1, TIME2, TIME2);
    assertEmpty(engine, "NOTES(AND(TEXT=\"diabetes\", TEXT=\"influenza\"))");
    assertEmpty(engine, "NOTES(NOT(OR(TEXT=\"diabetes\", TEXT=\"toxoplasmosis\", TEXT=\"influenza\")))");
    assertCommandMultiple(engine, "NOTES", TIME1, TIME1, TIME2, TIME2, TIME3, TIME3);
    assertEmpty(engine, "COUNT(NOTES, 4, MAX)");
    assertCommandMultiple(engine, "COUNT(NOTES, MIN, 3)", TIME1, TIME1, TIME2, TIME2, TIME3, TIME3);

    engine.close();
  }

}