package com.podalv.search.server.api;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.datastructures.DemographicsHistogram;
import com.podalv.search.datastructures.StatisticsResponse;
import com.podalv.search.server.Atlas;
import com.podalv.search.server.QueryUtils;
import com.podalv.utils.file.FileUtils;

public class StatisticsTest {

  private Atlas atlas;

  @Before
  public void setup() throws Exception {
    tearDown();
  }

  @After
  public void tearDown() throws Exception {
    if (atlas != null) {
      atlas.stop();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  @Test
  public void smokeTest() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).gender("MALE").race("WHITE").ethnicity("LATINO"));
    source.addDemographics(MockDemographicsRecord.create(2).gender("FEMALE").race("ASIAN"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012));

    final StatisticsResponse response = new StatisticsResponse();
    response.setGender(new DemographicsHistogram[] {new DemographicsHistogram("MALE", 0, 1, 0, 0.5, 2, 2), new DemographicsHistogram("FEMALE", 0, 1, 0, 0.5, 2, 2)});
    response.setRace(new DemographicsHistogram[] {new DemographicsHistogram("WHITE", 0, 1, 0, 0.5, 2, 2), new DemographicsHistogram("ASIAN", 0, 1, 0, 0.5, 2, 2)});
    response.setAges(new DemographicsHistogram[] {new DemographicsHistogram("0", 0, 2, 0, 1, 2, 2)});
    final ArrayList<long[]> encounters = new ArrayList<long[]>();
    encounters.add(new long[] {1, 2});
    response.setEncounters(encounters);
    response.setDurations(encounters);
    response.setCensored(encounters);
    response.setPatientCnt(2);
    response.setEncounterCnt(2);

    atlas = ServerTestCommon.startAtlas(source, ServerTestCommon.TEST_SERVER_PORT);

    Assert.assertEquals(QueryUtils.toJson(response) + "\n", QueryUtils.query("http://localhost:8080/statistics", "", 1, 1000));
  }
}
