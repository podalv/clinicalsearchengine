package com.podalv.search.server.api;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.objectdb.datastructures.DatabaseSettings;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.server.Atlas;
import com.podalv.search.server.ClinicalSearchEngine;

public class ServerTestCommon {

  public static int TEST_SERVER_PORT = 8080;

  private ServerTestCommon() {
    // Static access only
  }

  public static Atlas startAtlas(final Stride6TestDataSource source, final int port) throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final Atlas atlas = new Atlas(engine, port);

    atlas.start(false, null);

    return atlas;
  }

  private static int getMaxShard() {
    int result = 0;
    final File[] files = DATA_FOLDER.listFiles();
    for (final File file : files) {
      try {
        result = Math.max(result, Integer.parseInt(file.getName()));
      }
      catch (final Exception e) {
        // ignore
      }
    }
    return result;
  }

  public static Atlas startAtlasMaster(final Stride6TestDataSource source) throws Exception {
    final DatabaseSettings settings = DatabaseSettings.create(PatientBuilder.class, DATA_FOLDER);
    settings.setLoadReducedStatistics(false);
    Stride6TestHarness.extract(source, DATA_FOLDER, settings);
    final int maxShard = getMaxShard();
    if (maxShard <= 2) {
      throw new Exception("Test cannot proceed there are not enough shards to build a distributed model");
    }
    settings.withShards(0, (int) Math.ceil(maxShard / 2d));
    settings.withOffHeapSize(200000000);
    final ClinicalSearchEngine engine = Stride6TestHarness.startWithoutExtraction(SERVER_TYPE.MASTER_WORKSHOP, source, new File("."), settings);
    final Atlas atlas = new Atlas(engine, TEST_SERVER_PORT);

    atlas.start(false, null);

    return atlas;
  }

  public static Atlas startAtlasSlave() throws Exception {
    Thread.sleep(2000);
    final DatabaseSettings settings = DatabaseSettings.create(PatientBuilder.class, DATA_FOLDER);
    settings.setLoadReducedStatistics(true);
    settings.clearDiskCache(true);
    final int maxShard = getMaxShard();
    if (maxShard <= 2) {
      throw new Exception("Test cannot proceed there are not enough shards to build a distributed model");
    }
    settings.withShards((int) Math.ceil(maxShard / 2d) + 1, getMaxShard());
    settings.withOffHeapSize(200000000);
    generateReducedStatistics(settings);
    final ClinicalSearchEngine engine = new ClinicalSearchEngine(SERVER_TYPE.SLAVE, new File("."), settings);
    final Atlas atlas = new Atlas(engine, TEST_SERVER_PORT + 1);

    atlas.start(false, null);

    return atlas;
  }

  private static void generateReducedStatistics(final DatabaseSettings settings) throws InstantiationException, IllegalAccessException, IOException, SQLException,
      InterruptedException, Exception {
    final ClinicalSearchEngine engine = new ClinicalSearchEngine(SERVER_TYPE.SLAVE, new File("."), settings);
    final Atlas atlas = new Atlas(engine, TEST_SERVER_PORT + 1);
    atlas.start(false, null);
    atlas.stop();
  }

}
