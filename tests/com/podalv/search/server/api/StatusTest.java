package com.podalv.search.server.api;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockLabsLpchRecord;
import com.podalv.extractor.test.datastructures.MockLabsShcRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.server.Atlas;
import com.podalv.search.server.AtlasVersion;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.QueryUtils;
import com.podalv.utils.file.FileUtils;

public class StatusTest {

  private Atlas atlas;

  @Before
  public void setup() throws Exception {
    tearDown();
  }

  @After
  public void tearDown() throws Exception {
    if (atlas != null) {
      atlas.stop();
    }
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  private void startAtlas(final Stride6TestDataSource source, final SERVER_TYPE type) throws Exception {
    final ClinicalSearchEngine engine = Stride6TestHarness.engine(type, source, DATA_FOLDER, DATA_FOLDER);
    atlas = new Atlas(engine, ServerTestCommon.TEST_SERVER_PORT);

    atlas.start(false, null);
  }

  private boolean workshopBoolean(final SERVER_TYPE type) {
    return (type == SERVER_TYPE.WORKSHOP || type == SERVER_TYPE.MASTER_WORKSHOP);
  }

  private void test(final SERVER_TYPE type) throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));
    source.addDemographics(MockDemographicsRecord.create(3));
    source.addDemographics(MockDemographicsRecord.create(4));

    source.addVisit(MockVisitRecord.create(CPT, 2).age(1.5).code("43500").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(CPT, 4).age(2.5).code("45000").duration(1).year(2012));

    source.addLabsShc(MockLabsShcRecord.create(1).age(null).name("aBc").value(1.1, "0.1", "1.5", "<1.1"));
    source.addLabsLpch(MockLabsLpchRecord.create(3).age(null).code("DeF").value(1.5).range("1-2"));

    startAtlas(source, type);

    Assert.assertEquals("{\"status\":\"OK\"," + "\"version\":\"" + AtlasVersion.getVersion() + "\"," + "\"workshop\":" + workshopBoolean(type) + "}\n", QueryUtils.query(
        "http://localhost:8080/status", "", 1, 1000));
  }

  @Test
  public void independent() throws Exception {
    test(SERVER_TYPE.INDEPENDENT);
  }

  @Test
  public void slave() throws Exception {
    test(SERVER_TYPE.SLAVE);
  }

  @Test
  public void master() throws Exception {
    test(SERVER_TYPE.MASTER);
  }

  @Test
  public void workshop() throws Exception {
    test(SERVER_TYPE.WORKSHOP);
  }

  @Test
  public void masterWorkshop() throws Exception {
    test(SERVER_TYPE.MASTER_WORKSHOP);
  }

}