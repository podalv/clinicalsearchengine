package com.podalv.search.server.distributed;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.podalv.search.datastructures.ServerStatusResponse;
import com.podalv.search.server.Atlas;
import com.podalv.search.server.QueryUtils;
import com.podalv.search.server.SearchEngineSlaveManager;
import com.podalv.search.server.api.ServerTestCommon;

/** Harness that starts 2 instances in master / slave mode
 *
 * @author podalv
 *
 */
public class DistributedServerTestHarness {

  public static void startDistributed(final Atlas master, final Atlas slave) throws Exception {
    final ArrayList<String> slaveLines = new ArrayList<String>();
    slaveLines.add("SHARD;http://localhost:" + slave.getPort());
    master.getEngine().setSlaveManager(new SearchEngineSlaveManager(slaveLines));
    ServerStatusResponse response = new Gson().fromJson(QueryUtils.query("http://localhost:" + (ServerTestCommon.TEST_SERVER_PORT + 1) + "/status", "", -1),
        ServerStatusResponse.class);
    if (!response.isOk()) {
      throw new Exception("SLAVE could not be started");
    }
    response = new Gson().fromJson(QueryUtils.query("http://localhost:" + (ServerTestCommon.TEST_SERVER_PORT) + "/status", "", -1), ServerStatusResponse.class);
    if (!response.isOk()) {
      throw new Exception("MASTER could not be started");
    }
  }
}
