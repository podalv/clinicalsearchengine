package com.podalv.search.server.distributed;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.search.datastructures.DemographicsHistogram;
import com.podalv.search.datastructures.Histogram;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.StatisticsResponse;
import com.podalv.search.server.Atlas;
import com.podalv.search.server.AtlasRequestHandler;
import com.podalv.search.server.QueryUtils;
import com.podalv.search.server.api.ServerTestCommon;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;
import com.podalv.workshop.AggregatedPatientResponse;
import com.podalv.workshop.BooleanResponse;
import com.podalv.workshop.ContainsPatientRequest;
import com.podalv.workshop.PatientDataRequest;
import com.podalv.workshop.datastructures.DumpRequest;
import com.podalv.workshop.datastructures.DumpResponse;

public class DistributedTest {

  private Atlas master;
  private Atlas slave;

  @Before
  public void setup() throws Exception {
    tearDown();
  }

  @After
  public void tearDown() throws Exception {
    FileUtils.deleteFolderContents(DATA_FOLDER);
    try {
      new File(new File("."), "statistics.reduced").delete();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    try {
      new File(new File("."), "offheap.pos").delete();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    if (master != null) {
      master.stop();
    }
    if (slave != null) {
      slave.stop();
    }
  }

  private void assertHistogram(final Histogram[] histo, final String name, final int cohortCnt, final int generalCnt) {
    for (final Histogram h : histo) {
      if (h.getLabel().equalsIgnoreCase(name)) {
        Assert.assertEquals(cohortCnt, h.getCohortCnt());
        Assert.assertEquals(generalCnt, h.getGeneralCnt());
        return;
      }
    }
    Assert.fail("Histogram with label '" + name + "' not found");
  }

  private void assertHistogram(final DemographicsHistogram[] histo, final String name, final int cohortCnt, final int generalCnt) {
    for (final DemographicsHistogram h : histo) {
      if (h.getLabel().equalsIgnoreCase(name)) {
        Assert.assertEquals(cohortCnt, h.getCohortCnt());
        Assert.assertEquals(generalCnt, h.getGeneralCnt());
        if (h.getCohortCnt() != 0) {
          Assert.assertEquals(cohortCnt / (double) h.getCohortCnt(), h.getCohortPercentage(), 0.01);
        }
        else {
          Assert.assertEquals(0, h.getCohortPercentage(), 0.01);
        }
        Assert.assertEquals(generalCnt / (double) h.getTotalPatientCnt(), h.getGeneralPercentage(), 0.01);
        return;
      }
    }
    Assert.fail("Histogram with label '" + name + "' not found");
  }

  @Test
  public void statisticsResponse() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 500; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 501; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(20000.5).code("45000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    final StatisticsResponse response = new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + "/statistics", "", 1, 10000),
        StatisticsResponse.class);
    Assert.assertEquals(2, response.getGender().length);
    assertHistogram(response.getGender(), "female", 0, 500);
    assertHistogram(response.getGender(), "male", 0, 500);

    Assert.assertEquals(2, response.getRace().length);
    assertHistogram(response.getRace(), "white", 0, 500);
    assertHistogram(response.getRace(), "asian", 0, 500);

    assertHistogram(response.getAges(), "54", 0, 500);
    assertHistogram(response.getAges(), "0", 0, 500);

    Assert.assertEquals(1, response.getDurations().size());
    Assert.assertEquals(1, response.getDurations().get(0)[0]);
    Assert.assertEquals(1000, response.getDurations().get(0)[1]);

    Assert.assertEquals(1, response.getEncounters().size());
    Assert.assertEquals(1, response.getEncounters().get(0)[0]);
    Assert.assertEquals(1000, response.getEncounters().get(0)[1]);

    Assert.assertEquals(1000, response.getPatientCnt());

    Assert.assertEquals(1000, response.getEncounterCnt());

    Assert.assertEquals(1, response.getCensored().size());
    Assert.assertEquals(1, response.getCensored().get(0)[0]);
    Assert.assertEquals(1000, response.getCensored().get(0)[1]);

    Assert.assertEquals(0, response.getDeaths().size());
  }

  @Test
  public void queryResponse() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    final PatientSearchResponse response = new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.SEARCH_QUERY,
        new Gson().toJson(PatientSearchRequest.create("CPT=47000").setDurationBuckets(new int[] {0, 5, 10, 15}).setEncounterBuckets(new int[] {0, 5, 10, 15})), 1, 10000),
        PatientSearchResponse.class);

    Assert.assertEquals(100, response.getCohortPatientCnt());
    Assert.assertEquals(1000, response.getGeneralPatientCnt());
    assertHistogram(response.getAges().toArray(new Histogram[response.getAges().size()]), "32", 100, 100);
    assertHistogram(response.getGenderHistogram().toArray(new DemographicsHistogram[response.getGenderHistogram().size()]), "FEMALE", 100, 100);
    assertHistogram(response.getRaceHistogram().toArray(new DemographicsHistogram[response.getRaceHistogram().size()]), "ASIAN", 100, 100);
    Assert.assertArrayEquals(new int[] {100, 0, 0, 0}, response.getEncounters());
    Assert.assertArrayEquals(new int[] {100, 0, 0, 0}, response.getDurations());
  }

  @Test
  public void pidList() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    final String list = QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.PID_LIST, new Gson().toJson(PatientSearchRequest.create(
        "CPT=47000").setDurationBuckets(new int[] {0, 5, 10, 15}).setEncounterBuckets(new int[] {0, 5, 10, 15})), 1, 10000);

    final HashSet<String> values = new HashSet<>();
    final String[] val = TextUtils.split(list, '\n');
    for (int x = 0; x < val.length; x++) {
      if (!val[x].isEmpty()) {
        values.add(val[x]);
      }
    }

    for (int x = 901; x < 1001; x++) {
      Assert.assertTrue(values.contains(String.valueOf(x)));
    }
    Assert.assertEquals(100, values.size());
  }

  @Test
  public void containsPid() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    Assert.assertTrue(new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.CONTAINS_PID_QUERY, new Gson().toJson(
        new ContainsPatientRequest(1)), 1), BooleanResponse.class).getResponse());
    Assert.assertTrue(new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.CONTAINS_PID_QUERY, new Gson().toJson(
        new ContainsPatientRequest(901)), 1), BooleanResponse.class).getResponse());
    Assert.assertFalse(new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.CONTAINS_PID_QUERY, new Gson().toJson(
        new ContainsPatientRequest(1001)), 1), BooleanResponse.class).getResponse());
  }

  @Test
  public void dump() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    Assert.assertNull(new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.DUMP_QUERY, new Gson().toJson(
        DumpRequest.createFull(1)), 1, 10000), DumpResponse.class).getError());
    Assert.assertNull(new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.DUMP_QUERY, new Gson().toJson(
        DumpRequest.createFull(950)), 1, 10000), DumpResponse.class).getError());
    Assert.assertNotNull(new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.DUMP_QUERY, new Gson().toJson(
        DumpRequest.createFull(1001)), 1, 10000), DumpResponse.class).getError());
  }

  @Test
  public void demographics() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    System.out.println(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.DEMOGRAPHICS, new Gson().toJson(new PatientDataRequest(
        new int[] {1})), 1, 10000));

    String response = QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.DEMOGRAPHICS, new Gson().toJson(new PatientDataRequest(
        new int[] {1})), 1, 10000);
    Assert.assertTrue(response.indexOf("patientIds\":[1]") != -1);

    response = QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.DEMOGRAPHICS, new Gson().toJson(new PatientDataRequest(new int[] {
        905})), 1, 10000);
    Assert.assertTrue(response.indexOf("patientIds\":[905]") != -1);
  }

  @Test
  public void aggregate() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    AggregatedPatientResponse[] response = new AggregatedPatientResponse[0];
    response = new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.AGGREGATE, new Gson().toJson(
        new PatientDataRequest(new int[] {1, 905})), 1, 10000), response.getClass());

    Assert.assertEquals(2, response.length);
    Assert.assertEquals(1, response[0].getPatientId());
    Assert.assertEquals(4, response[0].getCpt().length);
    Assert.assertEquals(905, response[1].getPatientId());
    Assert.assertEquals(2, response[1].getCpt().length);

    response = new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.AGGREGATE, new Gson().toJson(
        new PatientDataRequest(new int[] {1, 905, 900, 1001})), 1, 10000), response.getClass());

    Assert.assertEquals(4, response.length);
    Assert.assertEquals(1, response[0].getPatientId());
    Assert.assertEquals(4, response[0].getCpt().length);
    Assert.assertEquals(905, response[1].getPatientId());
    Assert.assertEquals(2, response[1].getCpt().length);
    Assert.assertEquals(900, response[2].getPatientId());
    Assert.assertEquals(0, response[2].getCpt().length);
    Assert.assertEquals(1001, response[3].getPatientId());
    Assert.assertEquals(0, response[3].getCpt().length);
  }

  @Test
  public void csv() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 2;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    final PatientSearchResponse response = new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.SEARCH_QUERY,
        new Gson().toJson(PatientSearchRequest.create("CSV(OR(GENDER=\"MALE\", GENDER=\"FEMALE\"), CPT)").setDurationBuckets(new int[] {0, 5, 10, 15}).setEncounterBuckets(
            new int[] {0, 5, 10, 15})), 1, 10000), PatientSearchResponse.class);
    final ArrayList<String> lines = FileUtils.readFileArrayList(new File(new File("."), response.getExportLocation()), Charset.forName("UTF-8"));
    final HashSet<String> lineSet = new HashSet<>();
    lineSet.addAll(lines);
    for (int x = 0; x < 900; x++) {
      Assert.assertTrue(lineSet.contains(x + "\t" + "CPT=43500" + "\t" + "2012" + "\t\t\t" + "1" + "\t" + "1.5" + "\t" + "2.5\t"));
      Assert.assertTrue(lineSet.contains(x + "\t" + "CPT=45000" + "\t" + "2012" + "\t\t\t" + "1" + "\t" + "200.5" + "\t" + "201.5\t"));
    }
    System.out.println(lines.size());
  }

  @Test
  public void missingCodeInSlave() throws Exception {
    final Stride6TestDataSource sourceMaster = new Stride6TestDataSource();
    for (int x = 0; x < 100; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(200.5).code("45000").duration(1).year(2012));
    }
    for (int x = 100; x < 900; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("MALE").race("WHITE").ethnicity("LATINO"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(1.5).code("43500").duration(1).year(2012));
    }

    for (int x = 901; x < 1001; x++) {
      sourceMaster.addDemographics(MockDemographicsRecord.create(x).gender("FEMALE").race("ASIAN"));
      sourceMaster.addVisit(MockVisitRecord.create(CPT, x).age(12000.5).code("47000").duration(1).year(2012));
    }

    Stride6Extractor.SHARD_COUNT = 1;
    Stride6Extractor.SHARD_SIZE = 100000;

    master = ServerTestCommon.startAtlasMaster(sourceMaster);
    slave = ServerTestCommon.startAtlasSlave();

    DistributedServerTestHarness.startDistributed(master, slave);

    final PatientSearchResponse response = new Gson().fromJson(QueryUtils.query("http://localhost:" + ServerTestCommon.TEST_SERVER_PORT + AtlasRequestHandler.SEARCH_QUERY,
        new Gson().toJson(PatientSearchRequest.create("CPT=45000").setReturnTimeIntervals(true).setDurationBuckets(new int[] {0, 5, 10, 15}).setEncounterBuckets(new int[] {0, 5,
            10, 15})), 1, 10000), PatientSearchResponse.class);

    int pid = 0;
    for (final double[] val : response.getPatientIds()) {
      System.out.println(val[0]);
      Assert.assertEquals(pid, val[0], 0.001);
      Assert.assertEquals(200.5, val[1], 0.00001);
      Assert.assertEquals(201.5, val[2], 0.00001);
      pid++;
    }
    Assert.assertEquals(100, pid);
  }
}