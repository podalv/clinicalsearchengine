package com.podalv.search.server;

import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.DATA_FOLDER;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.daysToMinutes;
import static com.podalv.extractor.stride6.functionalTests.Stride6FunctionalTestCommon.dump;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.CPT;
import static com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE.ICD9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.Stride6TestHarness;
import com.podalv.extractor.test.Stride7TestHarness;
import com.podalv.extractor.test.datastructures.MockDemographicsRecord;
import com.podalv.extractor.test.datastructures.MockLabsLpchRecord;
import com.podalv.extractor.test.datastructures.MockLabsShcRecord;
import com.podalv.extractor.test.datastructures.MockNoteRecord;
import com.podalv.extractor.test.datastructures.MockRxRecord;
import com.podalv.extractor.test.datastructures.MockTermDictionaryRecord;
import com.podalv.extractor.test.datastructures.MockTermRecord;
import com.podalv.extractor.test.datastructures.MockVisitDxRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord;
import com.podalv.extractor.test.datastructures.MockVisitRecord.VISIT_TYPE;
import com.podalv.extractor.test.datastructures.MockVitalsRecord;
import com.podalv.extractor.test.datastructures.MockVitalsVisitsRecord;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7Demographics;
import com.podalv.extractor.test.datastructures.Stride7DepartmentRecord;
import com.podalv.extractor.test.datastructures.Stride7LabDeRecord;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.extractor.test.datastructures.Stride7VisitRecord;
import com.podalv.extractor.test.datastructures.Stride7VitalsRecord;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;
import com.podalv.workshop.datastructures.DumpResponse;

public class PatientDataDumpTest {

  @Before
  public void setup() {
    tearDown();
  }

  @After
  public void tearDown() {
    FileUtils.deleteFolderContents(DATA_FOLDER);
  }

  /** Example:
   *  43500=2544,2555,'PRIMARY';2555,2666,OTHER
   *
   * @param encoded
   * @return
   */
  private HashMap<String, ArrayList<String>> parseEncodedValues(final String encoded) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final String[] keyValuePairs = TextUtils.split(encoded, ';');
    for (final String line : keyValuePairs) {
      if (!line.isEmpty()) {
        final String[] keyToValues = TextUtils.split(line, '=');
        final String key = keyToValues[0];
        final String[] objects = TextUtils.split(keyToValues[1], ',');
        final ArrayList<String> l = new ArrayList<>();
        for (final String object : objects) {
          if (object.startsWith("'") && object.endsWith("'")) {
            l.add(object.substring(1, object.length() - 1));
          }
          else if (object.toLowerCase().startsWith("t")) {
            l.add(daysToMinutes(Double.parseDouble(object.substring(1))) + "");
          }
          else {
            try {
              l.add(Integer.parseInt(object) + "");
            }
            catch (final Exception e) {
              l.add(Double.parseDouble(object) + "");
            }
          }
        }
        result.put(key, l);
      }
    }
    return result;
  }

  private void assertMap(final HashMap<String, ArrayList<Integer>> map, final String encodedValues) {
    final HashMap<String, ArrayList<String>> expected = parseEncodedValues(encodedValues);
    Assert.assertEquals(expected.size(), map == null ? 0 : map.size());
    final Iterator<Entry<String, ArrayList<String>>> iterator = expected.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, ArrayList<String>> entry = iterator.next();
      final ArrayList<Integer> actualEntry = map.get(entry.getKey());
      Assert.assertEquals(entry.getValue().size(), actualEntry.size());
      for (int x = 0; x < entry.getValue().size(); x++) {
        Assert.assertEquals(entry.getValue().get(x), actualEntry.get(x) + "");
      }
    }
  }

  private void assertMapGeneral(final HashMap<String, ArrayList<String>> map, final String encodedValues) {
    final HashMap<String, ArrayList<String>> expected = parseEncodedValues(encodedValues);
    Assert.assertEquals(expected.size(), map == null ? 0 : map.size());
    final Iterator<Entry<String, ArrayList<String>>> iterator = expected.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, ArrayList<String>> entry = iterator.next();
      final ArrayList<String> actualEntry = map.get(entry.getKey());
      Assert.assertEquals(entry.getValue().size(), actualEntry.size());
      for (int x = 0; x < entry.getValue().size(); x++) {
        Assert.assertEquals(entry.getValue().get(x), actualEntry.get(x));
      }
    }
  }

  @Test
  public void visits() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012).srcVisit("aaa"));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012).srcVisit("bbb"));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(3.5).code("45000").duration(1).year(2012).srcVisit("ccc"));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("435.00").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(4.5).code("435.00").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(5.5).code("450.00").duration(1).year(2012).sourceCode("1234").visitId(1234).sab("DX_ID"));

    source.addVisitDx(MockVisitDxRecord.create(1234).primary("y").visitId(1234));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMap(response.getCpt(), "43500=t1.5,t2.5;");
    assertMap(response.getVisitTypes(), "aaa=t1.5,t2.5;empty=t3.5,t4.5,t4.5,t5.5");
    assertMapGeneral(response.getIcd9(), "435.00=t3.5,t4.5,'OTHER',t4.5,t5.5,'OTHER'");
    response = dump(engine, 2);
    assertMap(response.getCpt(), "45000=t2.5,t3.5,t3.5,t4.5");
    assertMapGeneral(response.getIcd9(), "450.00=t5.5,t6.5,'PRIMARY'");
    assertMap(response.getVisitTypes(), "bbb=t2.5,t3.5;ccc=t3.5,t4.5;empty=t5.5,t6.5");
  }

  @Test
  public void snomed() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("abc").duration(1).year(2012).srcVisit("aaa").sab("SNOMED"));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(5.5).code("def").duration(1).year(2012).sourceCode("1234").visitId(1234).sab("SNOMED"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMap(response.getCpt(), "");
    assertMap(response.getSnomed(), "abc=t1.5,t2.5");
    assertMap(response.getVisitTypes(), "aaa=t1.5,t2.5");
    assertMapGeneral(response.getIcd9(), "");
    response = dump(engine, 2);
    assertMap(response.getCpt(), "");
    assertMapGeneral(response.getIcd9(), "");
    assertMap(response.getSnomed(), "def=t5.5,t6.5");
    assertMap(response.getVisitTypes(), "empty=t5.5,t6.5");
  }

  @Test
  public void drugs() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addRx(MockRxRecord.create(1).rxCui(1234).age(3.5).year(2012).route("intravenous").status("continued"));
    source.addRx(MockRxRecord.create(2).rxCui(22345).age(3.5).year(2012).route("intravenous").status("discontinued"));
    source.addRx(MockRxRecord.create(2).rxCui(22345).age(4.5).year(2012).route("intravenous").status("continued"));
    source.addRx(MockRxRecord.create(2).rxCui(1234).age(4.5).year(2012).route("other").status("bla"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMapGeneral(response.getRx(), "1234=t3.5,t33.5,'intravenous','continued'");
    response = dump(engine, 2);
    assertMapGeneral(response.getRx(), "1234=t4.5,t34.5,'other','bla';22345=t3.5,t3.5,'intravenous','discontinued',t4.5,t34.5,'intravenous','continued'");
  }

  @Test
  public void labsLpch() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addLabsLpch(MockLabsLpchRecord.create(1).age(3.5).code("abc").range("1-5").value(2.5));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(4.5).code("def").range("> 12").value(1.1));
    source.addLabsLpch(MockLabsLpchRecord.create(1).age(5.5).code("def").range("> 12").value(1000000000d));

    source.addLabsLpch(MockLabsLpchRecord.create(2).age(1.5).code("aaa").range(null).value(3.5));
    source.addLabsLpch(MockLabsLpchRecord.create(2).age(4.5).code("aaa").range(null).value(4.4));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMapGeneral(response.getLabs(), "def=t5.5,'NO VALUE'");
    assertMapGeneral(response.getLabsRaw(), "abc=t3.5,'2.5';def=t4.5,'1.1'");
    response = dump(engine, 2);
    Assert.assertNull(response.getLabs());
    assertMapGeneral(response.getLabsRaw(), "aaa=t1.5,'3.5',t4.5,'4.4'");
  }

  @Test
  public void labsShc() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addLabsShc(MockLabsShcRecord.create(1).age(3.5).name("abc").value(2.5d, "1", "5", "normal"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(4.5).name("def").value(1.1d, "12", "10000", "low"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(4.5).name("xxx").value(1000000000d, "12", "10000", null));
    source.addLabsShc(MockLabsShcRecord.create(1).age(4.5).name("xxx").value(100d, "1", "50", null));

    source.addLabsShc(MockLabsShcRecord.create(2).age(1.5).name("aaa").value(3.5d, null, null, null));
    source.addLabsShc(MockLabsShcRecord.create(2).age(4.5).name("aaa").value(4.5d, null, null, null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMapGeneral(response.getLabs(), "abc=t3.5,'NORMAL';def=t4.5,'LOW';xxx=t4.5,'NO VALUE'");
    response = dump(engine, 2);
    Assert.assertNull(response.getLabs());
    assertMapGeneral(response.getLabsRaw(), "aaa=t1.5,'3.5',t4.5,'4.5'");
  }

  @Test
  public void demographics() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).death(1.5).ethnicity("eth1").race("race1").gender("gender1"));
    source.addDemographics(MockDemographicsRecord.create(2).ethnicity("eth2").race("race2").gender("gender2"));

    source.addLabsShc(MockLabsShcRecord.create(1).age(3.5).name("abc").value(2.5d, "1", "5", "normal"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(4.5).name("def").value(1.1d, "12", "10000", "low"));
    source.addLabsShc(MockLabsShcRecord.create(1).age(4.5).name("xxx").value(1000000000d, "12", "10000", null));
    source.addLabsShc(MockLabsShcRecord.create(1).age(4.5).name("xxx").value(100d, "1", "50", null));

    source.addLabsShc(MockLabsShcRecord.create(2).age(1.5).name("aaa").value(3.5d, null, null, null));
    source.addLabsShc(MockLabsShcRecord.create(2).age(4.5).name("aaa").value(4.5d, null, null, null));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    Assert.assertEquals(daysToMinutes(2), response.getDeath());
    Assert.assertEquals("ETH1", response.getEthnicity());
    Assert.assertEquals("RACE1", response.getRace());
    Assert.assertEquals("GENDER1", response.getGender());
    Assert.assertEquals(daysToMinutes(2), response.getRecordStart());
    Assert.assertEquals(daysToMinutes(4.5), response.getRecordEnd());

    response = dump(engine, 2);
    Assert.assertEquals(0, response.getDeath());
    Assert.assertEquals("ETH2", response.getEthnicity());
    Assert.assertEquals("RACE2", response.getRace());
    Assert.assertEquals("GENDER2", response.getGender());
    Assert.assertEquals(daysToMinutes(1.5), response.getRecordStart());
    Assert.assertEquals(daysToMinutes(4.5), response.getRecordEnd());

  }

  @Test
  public void notes() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addNote(MockNoteRecord.create(1).age(1.5).docDescription("description1").year(2012));
    source.addNote(MockNoteRecord.create(2).age(2.5).docDescription("description2").year(2012));
    source.addNote(MockNoteRecord.create(3).age(3.5).docDescription("description3").year(2012));

    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "xxxxx"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "yyyyy"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "zzzzz"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(4, "aaaaa"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(5, "bbbbb"));

    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));

    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(2).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(3).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(4));

    source.addTermMention(MockTermRecord.create(2).noteId(3).termId(5).negated());
    source.addTermMention(MockTermRecord.create(2).noteId(3).termId(5).familyHistory());
    source.addTermMention(MockTermRecord.create(2).noteId(3).termId(5));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMapGeneral(response.getPositiveTerms(), "xxxxx=t1.5,1,'description1';aaaaa=t2.5,2,'description2'");
    assertMapGeneral(response.getNegatedTerms(), "xxxxx=t1.5,1,'description1';yyyyy=t2.5,2,'description2'");
    assertMapGeneral(response.getFhTerms(), "xxxxx=t1.5,1,'description1';zzzzz=t2.5,2,'description2'");
    response = dump(engine, 2);
    assertMapGeneral(response.getPositiveTerms(), "bbbbb=t3.5,3,'description3'");
    assertMapGeneral(response.getNegatedTerms(), "bbbbb=t3.5,3,'description3'");
    assertMapGeneral(response.getFhTerms(), "bbbbb=t3.5,3,'description3'");
  }

  @Test
  public void encounterDays() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1).ethnicity("eth1").race("race1").gender("gender1"));
    source.addDemographics(MockDemographicsRecord.create(2).ethnicity("eth2").race("race2").gender("gender2"));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(1.5).code("43500").duration(1).year(2012).srcVisit("aaa"));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(2.5).code("45000").duration(1).year(2012).srcVisit("bbb"));
    source.addVisit(MockVisitRecord.create(CPT, 2).age(3.5).code("45000").duration(1).year(2012).srcVisit("ccc"));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("435.00").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(4.5).code("435.00").duration(1).year(2012));
    source.addVisit(MockVisitRecord.create(ICD9, 2).age(5.5).code("450.00").duration(1).year(2012).sourceCode("1234").visitId(1234).sab("DX_ID"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    int[] encDays = new int[response.getEncounterDays().size()];
    for (int x = 0; x < response.getEncounterDays().size(); x++) {
      encDays[x] = response.getEncounterDays().get(x);
    }
    Assert.assertArrayEquals(new int[] {daysToMinutes(1.5), daysToMinutes(2.5), daysToMinutes(3.5), daysToMinutes(4.5), daysToMinutes(4.5) + 1, daysToMinutes(5.5)}, encDays);

    int[] ageRangeDays = new int[response.getAgeRanges().size()];
    for (int x = 0; x < response.getAgeRanges().size(); x++) {
      ageRangeDays[x] = response.getAgeRanges().get(x);
    }
    Assert.assertArrayEquals(new int[] {daysToMinutes(1.5), daysToMinutes(2.5), daysToMinutes(3.5), daysToMinutes(4.5), daysToMinutes(4.5), daysToMinutes(5.5)}, ageRangeDays);

    response = dump(engine, 2);
    encDays = new int[response.getEncounterDays().size()];
    for (int x = 0; x < response.getEncounterDays().size(); x++) {
      encDays[x] = response.getEncounterDays().get(x);
    }
    Assert.assertArrayEquals(new int[] {daysToMinutes(2.5), daysToMinutes(3.5), daysToMinutes(3.5) + 1, daysToMinutes(4.5) + 1, daysToMinutes(4.5) + 2, daysToMinutes(5.5) + 2,
        daysToMinutes(5.5) + 3, daysToMinutes(6.5)}, encDays);

    ageRangeDays = new int[response.getAgeRanges().size()];
    for (int x = 0; x < response.getAgeRanges().size(); x++) {
      ageRangeDays[x] = response.getAgeRanges().get(x);
    }
    Assert.assertArrayEquals(new int[] {daysToMinutes(2.5), daysToMinutes(3.5), daysToMinutes(3.5), daysToMinutes(4.5), daysToMinutes(5.5), daysToMinutes(6.5)}, ageRangeDays);
  }

  @Test
  public void vitals() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));
    source.addDemographics(MockDemographicsRecord.create(2));

    source.addVitals(MockVitalsRecord.create(1).age(1.5).display("aaa").floMeasId(1).value(5.5));
    source.addVitals(MockVitalsVisitsRecord.create(1).age(2.5).bmi(1.5).bp(120, 150).bsa(2.5).height("6' 00\"").pulse(100).resp(100).temp(1.5).weight(1.5));

    source.addVitals(MockVitalsRecord.create(2).age(1.5).display("aaa").floMeasId(1).value(5.5));
    source.addVitals(MockVitalsVisitsRecord.create(2).age(2.5).bmi(1.5).bp(120, 150).bsa(2.5).height("6' 00\"").pulse(100).resp(100).temp(1.5).weight(1.5));

    source.addVisitDx(MockVisitDxRecord.create(1234).primary("y").visitId(1234));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    DumpResponse response = dump(engine, 1);
    assertMapGeneral(response.getVitals(),
        "aaa=t1.5,5.5;Heart Rate (Pulse)=t2.5,100.0;Temperature=t2.5,1.5;Weight (lbs)=t2.5,1.5;Respiration rate=t2.5,100.0;Body surface area (BSA)=t2.5,2.5;Blood pressure systolic (BP)=t2.5,120.0;Body mass index (BMI)=t2.5,1.5;Blood pressure diastolic (BP)=t2.5,150.0;Height (inch)=t2.5,72.0");
    response = dump(engine, 2);
    assertMapGeneral(response.getVitals(),
        "aaa=t1.5,5.5;Heart Rate (Pulse)=t2.5,100.0;Temperature=t2.5,1.5;Weight (lbs)=t2.5,1.5;Respiration rate=t2.5,100.0;Body surface area (BSA)=t2.5,2.5;Blood pressure systolic (BP)=t2.5,120.0;Body mass index (BMI)=t2.5,1.5;Blood pressure diastolic (BP)=t2.5,150.0;Height (inch)=t2.5,72.0");
  }

  @Test
  public void atc() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addRx(MockRxRecord.create(1).rxCui(197).age(3.5).year(2012).route("intravenous").status("continued"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    final DumpResponse response = dump(engine, 1);
    assertMap(response.getAtc(), "r05cb=197;v03=197;v03a=197;s01xa08=197;v03ab=197;v03ab23=197;v=197;r05=197;s01x=197;s=197;r=197;r05cb01=197;r05c=197;s01=197;s01xa=197");
  }

  @Test
  public void yearDumpIcd9() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("400.00").duration(1).year(2010));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(7.5).code("401.00").duration(1).year(2011));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(9.5).code("400.01").duration(1).year(2015));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getIcd9());
    assertMapGeneral(dump(engine, 1, "YEARS(MIN, 2010)", true, false).getIcd9(), "400.00=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5) + "," + "'OTHER'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getIcd9(), "400.00=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5) + "," + "'OTHER'");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getIcd9(), "401.00=" + Common.daysToTime(7.5) + "," + Common.daysToTime(8.5) + "," + "'OTHER'");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getIcd9(), "400.01=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5) + "," + "'OTHER'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getIcd9(), "400.00=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5) + "," + "'OTHER'" + ";"
        + "401.00=" + Common.daysToTime(7.5) + "," + Common.daysToTime(8.5) + "," + "'OTHER'" + ";" + "400.01=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5) + ","
        + "'OTHER'");
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getIcd9());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getIcd9());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getIcd9());
  }

  @Test
  public void yearDumpCpt() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(CPT, 1).age(3.5).code("40000").duration(1).year(2010));
    source.addVisit(MockVisitRecord.create(CPT, 1).age(7.5).code("41000").duration(1).year(2011));
    source.addVisit(MockVisitRecord.create(CPT, 1).age(9.5).code("42000").duration(1).year(2015));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMap(dump(engine, 1, "YEARS(0, 2010)", true, false).getCpt(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2010)", true, false).getCpt(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5));
    assertMap(dump(engine, 1, "YEARS(2011, 2011)", true, false).getCpt(), "41000=" + Common.daysToTime(7.5) + "," + Common.daysToTime(8.5));
    assertMap(dump(engine, 1, "YEARS(2015, 2021)", true, false).getCpt(), "42000=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2015)", true, false).getCpt(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5) + ";" + "41000=" + Common.daysToTime(7.5)
        + "," + Common.daysToTime(8.5) + ";" + "42000=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5));
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getCpt());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getCpt());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getCpt());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getCpt());
  }

  @Test
  public void yearDumpRx() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addRx(MockRxRecord.create(1).age(3.5).rxCui(40000).status("Discontinued").year(2010));
    source.addRx(MockRxRecord.create(1).age(7.5).rxCui(41000).status("Discontinued").year(2011));
    source.addRx(MockRxRecord.create(1).age(9.5).rxCui(42000).status("Discontinued").year(2015));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getRx(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(3.5) + ",'empty','discontinued'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getRx(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(3.5) + ",'empty','discontinued'");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getRx(), "41000=" + Common.daysToTime(7.5) + "," + Common.daysToTime(7.5) + ",'empty','discontinued'");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getRx(), "42000=" + Common.daysToTime(9.5) + "," + Common.daysToTime(9.5) + ",'empty','discontinued'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getRx(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(3.5) + ",'empty','discontinued'" + ";"
        + "41000=" + Common.daysToTime(7.5) + "," + Common.daysToTime(7.5) + ",'empty','discontinued'" + ";" + "42000=" + Common.daysToTime(9.5) + "," + Common.daysToTime(9.5)
        + ",'empty','discontinued'");
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getRx());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getRx());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getRx());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getRx());
  }

  @Test
  public void yearVisitTypes() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(ICD9, 1).age(3.5).code("400.00").duration(1).year(2010).srcVisit("aaa"));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(7.5).code("401.00").duration(1).year(2011).srcVisit("bbb"));
    source.addVisit(MockVisitRecord.create(ICD9, 1).age(9.5).code("400.01").duration(1).year(2015).srcVisit("ccc"));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMap(dump(engine, 1, "YEARS(0, 2010)", true, false).getVisitTypes(), "aaa=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2010)", true, false).getVisitTypes(), "aaa=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5));
    assertMap(dump(engine, 1, "YEARS(2011, 2011)", true, false).getVisitTypes(), "bbb=" + Common.daysToTime(7.5) + "," + Common.daysToTime(8.5));
    assertMap(dump(engine, 1, "YEARS(2015, 2021)", true, false).getVisitTypes(), "ccc=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2015)", true, false).getVisitTypes(), "aaa=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5) + ";" + "bbb=" + Common.daysToTime(
        7.5) + "," + Common.daysToTime(8.5) + ";" + "ccc=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5));
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getVisitTypes());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getVisitTypes());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getVisitTypes());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getVisitTypes());
  }

  @Test
  public void yearNoteTypes() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addNote(MockNoteRecord.create(1).age(3.5).docDescription("aaa").year(2010));
    source.addNote(MockNoteRecord.create(2).age(7.5).docDescription("bbb").year(2011));
    source.addNote(MockNoteRecord.create(3).age(9.5).docDescription("ccc").year(2015));

    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "abcd"));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(1));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMap(dump(engine, 1, "YEARS(0, 2010)", true, false).getNoteTypes(), "aaa=" + Common.daysToTime(3.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2010)", true, false).getNoteTypes(), "aaa=" + Common.daysToTime(3.5));
    assertMap(dump(engine, 1, "YEARS(2011, 2011)", true, false).getNoteTypes(), "bbb=" + Common.daysToTime(7.5));
    assertMap(dump(engine, 1, "YEARS(2015, 2021)", true, false).getNoteTypes(), "ccc=" + Common.daysToTime(9.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2015)", true, false).getNoteTypes(), "aaa=" + Common.daysToTime(3.5) + ";" + "bbb=" + Common.daysToTime(7.5) + ";" + "ccc="
        + Common.daysToTime(9.5));
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getNoteTypes());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getNoteTypes());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getNoteTypes());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getNoteTypes());
  }

  @Test
  public void yearNotePositive() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addNote(MockNoteRecord.create(1).age(3.5).docDescription("aaa").year(2010));
    source.addNote(MockNoteRecord.create(2).age(7.5).docDescription("bbb").year(2011));
    source.addNote(MockNoteRecord.create(3).age(9.5).docDescription("ccc").year(2015));

    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aaa"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "bbb"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "ccc"));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1));
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(2));
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(3));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getPositiveTerms(), "aaa=" + Common.daysToTime(3.5) + ",1," + "'aaa'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getPositiveTerms(), "aaa=" + Common.daysToTime(3.5) + ",1,'aaa'");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getPositiveTerms(), "bbb=" + Common.daysToTime(7.5) + ",2,'bbb'");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getPositiveTerms(), "ccc=" + Common.daysToTime(9.5) + ",3,'ccc'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getPositiveTerms(), "aaa=" + Common.daysToTime(3.5) + ",1,'aaa';" + "bbb=" + Common.daysToTime(7.5)
        + ",2,'bbb';" + "ccc=" + Common.daysToTime(9.5) + ",3,'ccc'");
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getPositiveTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getPositiveTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getPositiveTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getPositiveTerms());
  }

  @Test
  public void yearNoteNegated() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addNote(MockNoteRecord.create(1).age(3.5).docDescription("aaa").year(2010));
    source.addNote(MockNoteRecord.create(2).age(7.5).docDescription("bbb").year(2011));
    source.addNote(MockNoteRecord.create(3).age(9.5).docDescription("ccc").year(2015));

    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aaa"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "bbb"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "ccc"));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(2).negated());
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(3).negated());

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getNegatedTerms(), "aaa=" + Common.daysToTime(3.5) + ",1," + "'aaa'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getNegatedTerms(), "aaa=" + Common.daysToTime(3.5) + ",1,'aaa'");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getNegatedTerms(), "bbb=" + Common.daysToTime(7.5) + ",2,'bbb'");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getNegatedTerms(), "ccc=" + Common.daysToTime(9.5) + ",3,'ccc'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getNegatedTerms(), "aaa=" + Common.daysToTime(3.5) + ",1,'aaa';" + "bbb=" + Common.daysToTime(7.5)
        + ",2,'bbb';" + "ccc=" + Common.daysToTime(9.5) + ",3,'ccc'");
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getNegatedTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getNegatedTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getNegatedTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getNegatedTerms());
  }

  @Test
  public void yearNoteFamilyHistory() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addNote(MockNoteRecord.create(1).age(3.5).docDescription("aaa").year(2010));
    source.addNote(MockNoteRecord.create(2).age(7.5).docDescription("bbb").year(2011));
    source.addNote(MockNoteRecord.create(3).age(9.5).docDescription("ccc").year(2015));

    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(1, "aaa"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(2, "bbb"));
    source.addTermDictionaryRecord(MockTermDictionaryRecord.create(3, "ccc"));
    source.addTermMention(MockTermRecord.create(1).noteId(1).termId(1).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(2).termId(2).familyHistory());
    source.addTermMention(MockTermRecord.create(1).noteId(3).termId(3).familyHistory());

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getFhTerms(), "aaa=" + Common.daysToTime(3.5) + ",1," + "'aaa'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getFhTerms(), "aaa=" + Common.daysToTime(3.5) + ",1,'aaa'");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getFhTerms(), "bbb=" + Common.daysToTime(7.5) + ",2,'bbb'");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getFhTerms(), "ccc=" + Common.daysToTime(9.5) + ",3,'ccc'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getFhTerms(), "aaa=" + Common.daysToTime(3.5) + ",1,'aaa';" + "bbb=" + Common.daysToTime(7.5) + ",2,'bbb';"
        + "ccc=" + Common.daysToTime(9.5) + ",3,'ccc'");
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getFhTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getFhTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getFhTerms());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getFhTerms());
  }

  @Test
  public void yearDumpSnomed() throws Exception {
    final Stride6TestDataSource source = new Stride6TestDataSource();
    source.addDemographics(MockDemographicsRecord.create(1));

    source.addVisit(MockVisitRecord.create(VISIT_TYPE.SNOMED, 1).age(3.5).code("40000").duration(1).year(2010));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.SNOMED, 1).age(7.5).code("41000").duration(1).year(2011));
    source.addVisit(MockVisitRecord.create(VISIT_TYPE.SNOMED, 1).age(9.5).code("42000").duration(1).year(2015));

    final ClinicalSearchEngine engine = Stride6TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER);
    assertMap(dump(engine, 1, "YEARS(0, 2010)", true, false).getSnomed(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2010)", true, false).getSnomed(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5));
    assertMap(dump(engine, 1, "YEARS(2011, 2011)", true, false).getSnomed(), "41000=" + Common.daysToTime(7.5) + "," + Common.daysToTime(8.5));
    assertMap(dump(engine, 1, "YEARS(2015, 2021)", true, false).getSnomed(), "42000=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5));
    assertMap(dump(engine, 1, "YEARS(2010, 2015)", true, false).getSnomed(), "40000=" + Common.daysToTime(3.5) + "," + Common.daysToTime(4.5) + ";" + "41000=" + Common.daysToTime(
        7.5) + "," + Common.daysToTime(8.5) + ";" + "42000=" + Common.daysToTime(9.5) + "," + Common.daysToTime(10.5));
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getSnomed());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getSnomed());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getSnomed());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getSnomed());
  }

  @Test
  public void yearDumpLabs() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addLabDe(Stride7LabDeRecord.create(1).age(4d).value(1.5).year(2010).componentText("40000"));
    source.addLabDe(Stride7LabDeRecord.create(1).age(7d).value(1.5).year(2011).componentText("41000"));
    source.addLabDe(Stride7LabDeRecord.create(1).age(9d).value(1.5).year(2015).componentText("42000"));

    final ClinicalSearchEngine engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 1);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getLabsRaw(), "40000=" + Common.daysToTime(4) + ",'1.5'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getLabsRaw(), "40000=" + Common.daysToTime(4) + ",'1.5'");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getLabsRaw(), "41000=" + Common.daysToTime(7) + ",'1.5'");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getLabsRaw(), "42000=" + Common.daysToTime(9) + ",'1.5'");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getLabsRaw(), "40000=" + Common.daysToTime(4) + ",'1.5'" + ";" + "41000=" + Common.daysToTime(7) + ",'1.5'"
        + ";" + "42000=" + Common.daysToTime(9) + ",'1.5'");
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2010)", true, false).getLabs());
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getLabsRaw());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getLabsRaw());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getLabsRaw());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getLabsRaw());
  }

  @Test
  public void yearDumpLabsRaw() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addLabDe(Stride7LabDeRecord.create(1).age(4d).value(1.5).year(2010).componentText("40000").value(15.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(7d).value(1.5).year(2011).componentText("41000").value(16.5));
    source.addLabDe(Stride7LabDeRecord.create(1).age(9d).value(1.5).year(2015).componentText("42000").value(17.5));

    final ClinicalSearchEngine engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 1);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getLabsRaw(), "40000=" + Common.daysToTime(4) + ",15.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getLabsRaw(), "40000=" + Common.daysToTime(4) + ",15.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getLabsRaw(), "41000=" + Common.daysToTime(7) + ",16.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getLabsRaw(), "42000=" + Common.daysToTime(9) + ",17.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getLabsRaw(), "40000=" + Common.daysToTime(4) + ",15.5" + ";" + "41000=" + Common.daysToTime(7) + ",16.5"
        + ";" + "42000=" + Common.daysToTime(9) + ",17.5");
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getLabsRaw());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getLabsRaw());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getLabsRaw());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getLabsRaw());
  }

  @Test
  public void yearDumpVitals() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addVitals(Stride7VitalsRecord.create(1).age(4d).bmi(15.5).year(2010));
    source.addVitals(Stride7VitalsRecord.create(1).age(7d).bsa(16.5).year(2011));
    source.addVitals(Stride7VitalsRecord.create(1).age(9d).resp(17).year(2015));

    final ClinicalSearchEngine engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 1);
    assertMapGeneral(dump(engine, 1, "YEARS(0, 2010)", true, false).getVitals(), "Body mass index (BMI)=" + Common.daysToTime(4) + ",15.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2010)", true, false).getVitals(), "Body mass index (BMI)=" + Common.daysToTime(4) + ",15.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2011, 2011)", true, false).getVitals(), "Body surface area (BSA)=" + Common.daysToTime(7) + ",16.5");
    assertMapGeneral(dump(engine, 1, "YEARS(2015, 2021)", true, false).getVitals(), "Respiration rate=" + Common.daysToTime(9) + ",17.0");
    assertMapGeneral(dump(engine, 1, "YEARS(2010, 2015)", true, false).getVitals(), "Body mass index (BMI)=" + Common.daysToTime(4) + ",15.5" + ";" + "Body surface area (BSA)="
        + Common.daysToTime(7) + ",16.5" + ";" + "Respiration rate=" + Common.daysToTime(9) + ",17.0");
    Assert.assertNull(dump(engine, 1, "YEARS(2000, 2009)", true, false).getVitals());
    Assert.assertNull(dump(engine, 1, "YEARS(0, 2009)", true, false).getVitals());
    Assert.assertNull(dump(engine, 1, "YEARS(2012, 2014)", true, false).getVitals());
    Assert.assertNull(dump(engine, 1, "YEARS(2016, 3000)", true, false).getVitals());
  }

  @Test
  public void icd10() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addVisitAdmitShc(Stride7VisitRecord.create(1).age(3d, 5d).dxId(source.addDxIdShc("250.50", "A00.0", "250.50", "A00.0")).year(2012));
    source.addVisitAdmitShc(Stride7VisitRecord.create(1).age(3d, 5d).dxId(source.addDxIdShc("220.20", "B00.0", "220.20", "B00.0")).year(2012));

    final ClinicalSearchEngine engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 1);
    assertMapGeneral(dump(engine, 1, null, true, false).getIcd10(), "A00-B99=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";" + //
        "A00=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";" + //
        "B00=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";" + //
        "A00-A09=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";" + //
        "A00.0=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";" + //
        "B00.0=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";" + //
        "B00-B09=" + Common.daysToTime(3) + "," + Common.daysToTime(5) + ",'OTHER'" + ";");
  }

  @Test
  public void departments() throws Exception {
    final Stride7TestDataSource source = new Stride7TestDataSource();
    source.addDemographics(Stride7Demographics.create(1));

    source.addDepartmentApptLpch(Stride7DepartmentRecord.create(1).age(5d).departmentId(source.getDepartmentIdLpch("aaa")).year(2012));

    final ClinicalSearchEngine engine = Stride7TestHarness.engine(source, DATA_FOLDER, DATA_FOLDER, 1);
    assertMap(dump(engine, 1, null, true, false).getDepartments(), "aaa=" + Common.daysToTime(5) + "," + Common.daysToTime(5));
  }

}