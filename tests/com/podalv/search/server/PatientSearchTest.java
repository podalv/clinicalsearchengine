package com.podalv.search.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.index.StringIntegerFastIndex;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.utils.file.FileUtils;

public class PatientSearchTest {

  private static final File DATA_FOLDER = new File(new File("."), "testData");

  @Before
  public void setup() {
    PatientExport.setInstance(new PatientExportTestInstance(DATA_FOLDER));
    final File[] files = DATA_FOLDER.listFiles();
    if (files != null) {
      for (final File file : files) {
        if (!file.getAbsolutePath().endsWith("dictionary")) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
  }

  @After
  public void tearDown() {
    setup();
  }

  public static void assertContainsAll(final PatientSearchResponse response, final double[] expected) {
    if (expected.length > 0) {
      Assert.assertNull(response.getErrorMessage());
    }
    Assert.assertEquals(expected.length, response.getPatientIds().size());
    for (final double element : expected) {
      boolean fnd = false;
      for (final double[] value : response.getPatientIds()) {
        if (Double.compare(value[0], element) == 0) {
          fnd = true;
          break;
        }
      }
      if (!fnd) {
        Assert.fail();
      }
    }
  }

  private static PatientRecord<VisitRecord> getVisits(final int patientId, final int start, final String icd9Code) {
    final PatientRecord<VisitRecord> result = new PatientRecord<>(patientId);
    int s = start;
    for (int x = 0; x < 5; x++) {
      result.add(new VisitRecord(x, 2002, s, s + 10, "ICD", icd9Code, "Outpatient"));
      s += 20;
    }
    result.add(new VisitRecord(1, 2002, start, start + 10, "ICD", icd9Code + "1", "Outpatient"));
    final VisitRecord r = new VisitRecord(1, 2002, s, s + 10, "ICD", "250.00", "Inpatients");
    r.setPrimary(true);
    result.add(r);
    //result.add(new VisitRecord(1, patientId, 2002, s + 100, s + 200, "ICD", icd9Code + "2", "Outpatient"));
    return result;
  }

  private static PatientRecord<TermRecord> getTestTerms(final IndexCollection indices, final int patientId) {
    final PatientRecord<TermRecord> result = new PatientRecord<>(patientId);
    result.add(new TermRecord(indices.getTid("diabetes"), 1, 0, 0, 0));
    result.add(new TermRecord(indices.getTid("stroke"), 1, 1, 0, 0));
    result.add(new TermRecord(indices.getTid("influenza"), 2, 0, 1, 0));
    for (int x = 0; x < result.getRecords().size(); x++) {
      result.getRecords().get(x).setAge(20);
      result.getRecords().get(x).setYear(2012);
    }
    return result;
  }

  private static PatientRecord<MedRecord> getMeds(final UmlsDictionary umls, final int patientId) {
    final PatientRecord<MedRecord> result = new PatientRecord<>(patientId);
    result.add(new MedRecord(10, 30, 2012, "Continue", 1000027, "oral"));
    result.add(new MedRecord(21, 21, 2012, "Discontinue", 1000027, "oral"));
    return result;
  }

  private static ClinicalSearchEngine prepareSmokeTestDatabase() throws IOException, InterruptedException, InstantiationException, IllegalAccessException {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    indices.addDemographicRecord(new DemographicsRecord(0, "male", "asian", "other", 50000, -1));
    indices.addDemographicRecord(new DemographicsRecord(1, "female", "white", "jew", 0, -1));
    indices.addDemographicRecord(new DemographicsRecord(2, "male", "black", "unknown", 1000, -1));
    indices.addDemographicRecord(new DemographicsRecord(3, "female", "pacific islander", "other", 50000, -1));
    indices.addDemographicRecord(new DemographicsRecord(4, "male", "white", "hispanic", 2000, -1));
    indices.addDemographicRecord(new DemographicsRecord(5, "unknown", "other", "hispanic", 2000, -1));

    final Statistics statistics = new Statistics();
    for (int x = 0; x < 4000; x++) {
      for (int y = 0; y < 6; y++) {
        statistics.addCpt(x, y);
        statistics.addIcd9(x, y);
        statistics.addRxNorm(x, y);
      }
    }
    statistics.addRxNorm(1000027, 5);

    for (int x = 0; x < 5; x++) {
      statistics.addVisitType(indices.addVisitTypeCode("outpatient"), x);
      statistics.addVisitType(indices.addVisitTypeCode("inpatient"), x);
    }

    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(100000, 4, DATA_FOLDER);
    for (int x = 0; x < 5; x++) {
      final PatientBuilder patient = new PatientBuilder(indices);
      patient.setId(x);
      final PatientRecord<VisitRecord> visits = getVisits(x, x * 100, "100." + x);
      if (x == 2) {
        visits.add(new VisitRecord(x, 2002, 10000, 10001, "ICD", "100.22", "Outpatient"));
      }
      patient.recordVisit(visits);
      patient.close();
      generator.add(patient);
      statistics.recordPatientId(patient);
    }
    indices.addTid(0, "diabetes");
    indices.addTid(1, "stroke");
    indices.addTid(2, "influenza");
    indices.addTid(3, "treat");
    indices.addTid(4, "toxoplasmosis");

    for (int x = 0; x < 6; x++) {
      statistics.addTid(indices.getTid("diabetes"), x);
      statistics.addTid(indices.getTid("stroke"), x);
      statistics.addTidNegated(indices.getTid("stroke"), x);
      statistics.addTid(indices.getTid("influenza"), x);
      statistics.addTidFamilyHistory(indices.getTid("influenza"), x);
      statistics.addTid(indices.getTid("treat"), x);
      statistics.addTid(indices.getTid("toxoplasmosis"), x);
    }

    final PatientBuilder patient = new PatientBuilder(indices);
    patient.setId(5);
    patient.recordTerms(getTestTerms(indices, 5));
    patient.recordMeds(getMeds(umls, 5));
    patient.recordAtc(Stride6Extractor.generateAtcToRxNormMap(indices, umls, getMeds(umls, 5)));

    statistics.addAtc(indices.getAtcCode("J"), 5);
    statistics.addAtc(indices.getAtcCode("B"), 5);
    statistics.addAtc(indices.getAtcCode("A"), 5);
    statistics.addAtc(indices.getAtcCode("B05XA09"), 5);
    statistics.addAtc(indices.getAtcCode("A06AG01"), 5);
    statistics.addAtc(indices.getAtcCode("A06AD17"), 5);
    statistics.addAtc(indices.getAtcCode("J01XX05"), 5);
    patient.close();
    statistics.recordPatientId(patient);
    generator.add(patient);

    generator.close();
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "indices")));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();
    final BufferedOutputStream statsCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "statistics")));
    statistics.save(new StreamOutput(statsCollection));
    statsCollection.close();

    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
  }

  private static PatientRecord<VitalsRecord> getVitals() {
    final PatientRecord<VitalsRecord> result = new PatientRecord<>(0);
    result.add(new VitalsRecord(1500, "Temperature", 119.5));
    result.add(new VitalsRecord(1700, "MAP", 41.25));
    for (int x = 0; x < 500; x++) {
      result.add(new VitalsRecord(1500, "Temperature", 500 * (x + 1)));
    }
    return result;
  }

  private static PatientRecord<LabsRecord> getLabs() {
    final PatientRecord<LabsRecord> result = new PatientRecord<>(0);
    result.add(new LabsRecord("WBC", 105, 3.5, 0.1, 3.8, null));
    result.add(new LabsRecord("WBC", 101, 3.5, 0.1, 3.8, null));
    result.add(new LabsRecord("WBC", 100, 3.9, 0.1, 3.8, null));
    result.add(new LabsRecord("WBC", 102, 0.05, 0.1, 3.8, null));
    return result;
  }

  private static ClinicalSearchEngine prepareTextTestDatabase() throws IOException, InterruptedException, InstantiationException, IllegalAccessException {
    final UmlsDictionary umls = UmlsDictionary.create();
    final IndexCollection indices = new IndexCollection(umls);
    indices.addDemographicRecord(new DemographicsRecord(0, "male", "asian", "other", 50000, -1));
    final StringIntegerFastIndex docDescription = new StringIntegerFastIndex();
    docDescription.add("first");
    docDescription.add("second");
    docDescription.add("third");
    indices.setDocDescription(docDescription);
    indices.addTid(0, "diabetes");
    indices.addTid(1, "stroke");
    indices.addTid(2, "influenza");
    indices.addTid(3, "treat");
    indices.addTid(4, "toxoplasmosis");

    final Statistics statistics = new Statistics();
    for (int x = 0; x < 6; x++) {
      statistics.addTid(indices.getTid("diabetes"), 0);
      statistics.addTid(indices.getTid("stroke"), 0);
      statistics.addTid(indices.getTid("influenza"), 0);
      statistics.addTid(indices.getTid("treat"), 0);
      statistics.addTid(indices.getTid("toxoplasmosis"), 0);
    }
    statistics.addNoteType(0, 0);
    statistics.addNoteType(1, 0);
    statistics.addNoteType(2, 0);
    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(100000, 1, DATA_FOLDER);
    final PatientBuilder patient = new PatientBuilder(indices);
    patient.setId(0);

    final PatientRecord<TermRecord> texts = new PatientRecord<>(0);
    texts.add(new TermRecord(indices.getTid("diabetes"), 1, 0, 0, 1));
    texts.add(new TermRecord(indices.getTid("stroke"), 1, 0, 0, 1));
    texts.add(new TermRecord(indices.getTid("influenza"), 2, 0, 0, 2));
    texts.add(new TermRecord(indices.getTid("diabetes"), 2, 0, 0, 2));
    texts.add(new TermRecord(indices.getTid("treat"), 2, 0, 0, 2));
    texts.add(new TermRecord(indices.getTid("influenza"), 3, 0, 0, 2));
    texts.add(new TermRecord(indices.getTid("toxoplasmosis"), 3, 0, 0, 2));

    for (int x = 0; x < texts.getRecords().size(); x++) {
      texts.getRecords().get(x).setAge(20);
      texts.getRecords().get(x).setYear(2012);
    }

    final PatientRecord<VisitRecord> visits = getVisits(0, 5 * 100, "100.1");
    visits.add(new VisitRecord(0, 2012, 10000, 10001, "ICD", "100.22", "Outpatient"));
    patient.recordVisit(visits);

    patient.recordVitals(getVitals());

    patient.recordLabs(getLabs());

    statistics.addLabs(indices.getLabsCode("WBC"), 0);

    statistics.addVitals(0, 0);
    statistics.addVitals(1, 0);

    patient.recordTerms(texts);
    patient.close();
    statistics.recordPatientId(patient);
    generator.add(patient);

    generator.close();
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "indices")));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();
    final BufferedOutputStream statsCollection = new BufferedOutputStream(new FileOutputStream(new File(DATA_FOLDER, "statistics")));
    for (int x = 0; x < 18000; x++) {
      statistics.addIcd9(x, 100);
    }
    statistics.save(new StreamOutput(statsCollection));
    statsCollection.close();

    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, DATA_FOLDER, new File("."));
  }

  @Test
  public void smokeTest() throws Exception {
    final ClinicalSearchEngine engine = prepareSmokeTestDatabase();

    PatientSearchResponse response = engine.search(PatientSearchRequest.create("AND(NOTE(TEXT=\"diabetes\", !TEXT=\"stroke\"))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.2, ICD9=100.1)-*(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1});

    response = engine.search(PatientSearchRequest.create("PRIMARY(ICD9=100.2)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("TEXT=\"AAAA\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("PRIMARY(ICD9=250.00)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 3, 4});

    response = engine.search(PatientSearchRequest.create("YEAR(2005, 2020)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("YEAR(2001, 2020)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 3, 4, 5});

    response = engine.search(PatientSearchRequest.create("YEAR(2002, 2002)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 3, 4});

    response = engine.search(PatientSearchRequest.create("VISIT TYPE=\"outpatient\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 3, 4});

    response = engine.search(PatientSearchRequest.create("YEAR(2000, 2002)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 3, 4});

    response = engine.search(PatientSearchRequest.create("YEAR(2002, 2003)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 3, 4});

    response = engine.search(PatientSearchRequest.create("ICD9=100.2"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {2});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.2*, ICD9=100.1)+(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.2, ICD9=100.1*)+(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.1, ICD9=100.2*)+(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.1*, ICD9=100.2)+(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.1, ICD9=100.2)-*(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {2});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.2, ICD9=100.1*)-(MIN, -1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1});

    response = engine.search(PatientSearchRequest.create("BEFORE(ICD9=100.2, ICD9=100.1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("AND(ICD9=100.1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1});

    response = engine.search(PatientSearchRequest.create("AND(GENDER=\"MALE\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 2, 4});

    response = engine.search(PatientSearchRequest.create("SAME(GENDER=\"MALE\", ICD9=100.1)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("SAME(GENDER=\"MALE\", YEAR(2002, 2003))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 2, 4});

    response = engine.search(PatientSearchRequest.create("DIFF(GENDER=\"MALE\", YEAR(2002, 2003))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("DIFF(GENDER=\"MALE\", YEAR(2005, 2020))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 2, 4});

    response = engine.search(PatientSearchRequest.create("MERGE(GENDER=\"MALE\", YEAR(2005, 2020))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 2, 4, 5});

    response = engine.search(PatientSearchRequest.create("AND(GENDER=\"FEMALE\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1, 3});

    response = engine.search(PatientSearchRequest.create("AND(ICD9=100.1, GENDER=\"FEMALE\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1});

    response = engine.search(PatientSearchRequest.create("AND(ICD9=100.2, GENDER=\"FEMALE\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("AND(RACE=\"WHITE\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1, 4});

    response = engine.search(PatientSearchRequest.create("AND(ETHNICITY=\"JEW\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1});

    response = engine.search(PatientSearchRequest.create("OR(ICD9=100.1, GENDER=\"MALE\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0, 1, 2, 4});

    response = engine.search(PatientSearchRequest.create("AND(ICD9=100.1,ICD9=100.11)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {1});

    response = engine.search(PatientSearchRequest.create("INTERSECT(ICD9=100.0,ICD9=100.01)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("INTERSECT(ICD9=999.9,ICD9=888.8)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("INTERSECT(ICD9=050.0,ICD9=040.0)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("ICD9=100.22"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {2});

    response = engine.search(PatientSearchRequest.create("INTERSECT(ICD9=100.2,ICD9=100.22)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("UNION(ICD9=100.2,ICD9=100.22)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {2});

    response = engine.search(PatientSearchRequest.create("AND(TEXT=\"diabetes\", !TEXT=\"stroke\", ~TEXT=\"influenza\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(NOTE(TEXT=\"diabetes\", !TEXT=\"stroke\", ~TEXT=\"influenza\"))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("AND(TEXT=\"xxabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("AND(TEXT=\"hyperlipidemia\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("AND(RX=1000027)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"j01xx05\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"A06AD17\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"A06AG01\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"B05XA09\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"A\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"B\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("AND(ATC=\"J\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("INTERSECT(RX=1000027, TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    response = engine.search(PatientSearchRequest.create("INTERSECT(ATC=\"B\", TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {5});

    engine.close();
  }

  @Test
  public void textTest() throws Exception {
    final ClinicalSearchEngine engine = prepareTextTestDatabase();

    PatientSearchResponse response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(NOTETYPE=\"second\", TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(NOTETYPE=\"second\", TEXT=\"diabetes\", TEXT=\"stroke\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(NOTETYPE=\"second\", NOT(TEXT=\"diabetes\"))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(NOTETYPE=\"third\", NOT(TEXT=\"stroke\"))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(NOTETYPE=\"second\", NOT(TEXT=\"aaaax\"))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(NOTETYPE=\"third\", OR(TEXT=\"diabetes\", TEXT=\"treat\"))"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTETYPE=\"second\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE TYPE=\"SECOND\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE_TYPE=\"THIRD\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("VITALS(\"Temperature\", 100, 200)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("LABS(\"WBC\", \"UNDEFINED\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[0]);

    response = engine.search(PatientSearchRequest.create("LABS(\"WBC\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("LABS(\"WBC\", \"LOW\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[0]);

    response = engine.search(PatientSearchRequest.create("LABS(\"WBC\", \"HIGH\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[0]);

    response = engine.search(PatientSearchRequest.create("VITALS(\"MAP\", 20, 50)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("YEAR(2001, 2020)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("YEAR(2003, 2020)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("YEAR(2001, 2011)"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"stroke\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"influenza\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"toxoplasmosis\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("TEXT=\"stroke\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("TEXT=\"toxoplasmosis\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("TEXT=\"diabetes\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("TEXT=\"influenza\""), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"stroke\", TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"influenza\", TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"influenza\", TEXT=\"toxoplasmosis\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"stroke\", TEXT=\"diabetes\", TEXT=\"influenza\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"stroke\", TEXT=\"diabetes\", TEXT=\"toxoplasmosis\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"influenza\", TEXT=\"diabetes\", TEXT=\"toxoplasmosis\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"diabetes\", TEXT=\"toxoplasmosis\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"stroke\", TEXT=\"toxoplasmosis\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"stroke\", TEXT=\"influenza\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});

    response = engine.search(PatientSearchRequest.create("NOTE(TEXT=\"treat\", TEXT=\"influenza\", TEXT=\"diabetes\")"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {0});

    engine.close();
  }

  @Test
  public void unparseable_command() throws Exception {
    final ClinicalSearchEngine engine = prepareTextTestDatabase();

    final PatientSearchResponse response = engine.search(PatientSearchRequest.create("XXXXXXXXX"), Integer.MAX_VALUE);
    assertContainsAll(response, new double[] {});
  }
}
