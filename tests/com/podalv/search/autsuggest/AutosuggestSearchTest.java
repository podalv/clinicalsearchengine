package com.podalv.search.autsuggest;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.search.autosuggest.AutosuggestSearch;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestResponse;
import com.podalv.search.datastructures.AutosuggestTextRequest;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;

public class AutosuggestSearchTest {

  private static UmlsDictionary umls;

  static {
    umls = getUmls();
  }

  private static HashSet<String> getSet(final String ... strings) {
    final HashSet<String> result = new HashSet<>();
    for (int x = 0; x < strings.length; x++) {
      result.add(strings[x]);
    }
    return result;
  }

  private static UmlsDictionary getUmls() {
    final UmlsDictionary result = Mockito.mock(UmlsDictionary.class);
    Mockito.when(result.getCptCodes()).thenReturn(getSet("50000", "60000", "70000"));
    Mockito.when(result.getIcd9Codes()).thenReturn(getSet("100.50", "100.60", "100.70").iterator());
    Mockito.when(result.getCptText("50000")).thenReturn("Diabetic ulcer of left leg");
    Mockito.when(result.getCptText("60000")).thenReturn("Non-diabetic ulcer of left leg");
    Mockito.when(result.getCptText("70000")).thenReturn("Ulceritive colitis");
    Mockito.when(result.getIcd9Text("100.50")).thenReturn("Pain in left arm");
    Mockito.when(result.getIcd9Text("100.60")).thenReturn("Pain in left leg");
    Mockito.when(result.getIcd9Text("100.70")).thenReturn("Vomiting and pain in left arm");
    Mockito.when(result.getAtcCodes()).thenReturn(getSet("A1245", "A2254").iterator());
    Mockito.when(result.getAtcText("A1245")).thenReturn("Liquid paraffin");
    Mockito.when(result.getAtcText("A2254")).thenReturn("Lipid modifying agents");

    final StringKeyObjectMap<IntOpenHashSet> rxMap = new StringKeyObjectMap<>();
    rxMap.put("Xanax", new IntOpenHashSet(new int[] {23}));
    rxMap.put("Idax", new IntOpenHashSet(new int[] {23}));

    Mockito.when(result.getRxNormText(23)).thenReturn("Xanax");
    Mockito.when(result.getCodeFromRxNormText("xanax")).thenReturn(new IntOpenHashSet(new int[] {23}));
    Mockito.when(result.getCodeFromRxNormText("idax")).thenReturn(new IntOpenHashSet(new int[] {23}));
    Mockito.when(result.getRxNorms()).thenReturn(rxMap);
    return result;
  }

  private IndexCollection getIndices() {
    final IndexCollection result = Mockito.mock(IndexCollection.class);
    Mockito.when(result.getCpt(0)).thenReturn("50000");
    Mockito.when(result.getCpt(1)).thenReturn("60000");
    Mockito.when(result.getCpt(2)).thenReturn("70000");
    Mockito.when(result.getIcd9(0)).thenReturn("100.50");
    Mockito.when(result.getIcd9(1)).thenReturn("100.60");
    Mockito.when(result.getIcd9(2)).thenReturn("100.70");
    Mockito.when(result.getVisitType("surgery")).thenReturn(0);
    Mockito.when(result.getVisitType("inpatient")).thenReturn(1);
    Mockito.when(result.getVisitType("outpatient visit")).thenReturn(2);
    Mockito.when(result.getVisitTypeNames()).thenReturn(new String[] {"surgery", "inpatient", "outpatient visit"});
    Mockito.when(result.getLabsNames()).thenReturn(new String[] {"001", "002", "003"});
    Mockito.when(result.getLabsCode("001")).thenReturn(1);
    Mockito.when(result.getLabsCode("002")).thenReturn(2);
    Mockito.when(result.getLabsCode("003")).thenReturn(3);
    Mockito.when(result.getLabValueId("normal")).thenReturn(1);
    Mockito.when(result.getLabValueId("high")).thenReturn(2);
    Mockito.when(result.getLabValueId("low")).thenReturn(2);
    Mockito.when(result.getLabValueText(1)).thenReturn("normal");
    Mockito.when(result.getLabValueText(2)).thenReturn("high");
    Mockito.when(result.getLabValueText(3)).thenReturn("low");
    Mockito.when(result.getLabValues()).thenReturn(new String[] {"normal", "high", "low"});
    Mockito.when(result.getLabCommonName(1)).thenReturn("First");
    Mockito.when(result.getLabCommonName(2)).thenReturn("Second");
    Mockito.when(result.getLabCommonName(3)).thenReturn("Third");
    Mockito.when(result.getVitalsNames()).thenReturn(new String[] {"BP", "Map"});
    Mockito.when(result.getVitalsCode("BP")).thenReturn(0);
    Mockito.when(result.getVitalsCode("Map")).thenReturn(1);
    Mockito.when(result.getVitalsName(0)).thenReturn("BP");
    Mockito.when(result.getVitalsName(1)).thenReturn("Map");
    Mockito.when(result.getAtcCode("A1245")).thenReturn(0);
    Mockito.when(result.getAtcCode("A2245")).thenReturn(1);
    Mockito.when(result.getUmls()).thenReturn(umls);

    result.setUmls(umls);
    return result;
  }

  private Statistics getStatistics() {
    final Statistics result = Mockito.mock(Statistics.class);
    Mockito.when(result.getCpt(0)).thenReturn(1);
    Mockito.when(result.getCpt(1)).thenReturn(1);
    Mockito.when(result.getCpt(2)).thenReturn(1);
    Mockito.when(result.getIcd9(0)).thenReturn(1);
    Mockito.when(result.getIcd9(1)).thenReturn(1);
    Mockito.when(result.getIcd9(2)).thenReturn(1);
    Mockito.when(result.getRxNorm(23)).thenReturn(1);
    Mockito.when(result.getTid(1)).thenReturn(100);
    Mockito.when(result.getTid(2)).thenReturn(200);
    Mockito.when(result.getVitals(0)).thenReturn(50);
    Mockito.when(result.getVitals(1)).thenReturn(70);
    Mockito.when(result.getVisitType(0)).thenReturn(70);
    Mockito.when(result.getVisitType(1)).thenReturn(60);
    Mockito.when(result.getVisitType(2)).thenReturn(50);
    Mockito.when(result.getAtc(0)).thenReturn(50);
    Mockito.when(result.getAtc(1)).thenReturn(50);
    Mockito.when(result.getLabs(1)).thenReturn(50);
    Mockito.when(result.getLabs(2)).thenReturn(60);
    Mockito.when(result.getLabs(3)).thenReturn(70);
    Mockito.when(result.getCpt(0, null)).thenReturn(1);
    Mockito.when(result.getCpt(1, null)).thenReturn(1);
    Mockito.when(result.getCpt(2, null)).thenReturn(1);
    Mockito.when(result.getIcd9(0, null)).thenReturn(1);
    Mockito.when(result.getIcd9(1, null)).thenReturn(1);
    Mockito.when(result.getIcd9(2, null)).thenReturn(1);
    Mockito.when(result.getRxNorm(23, null)).thenReturn(1);
    Mockito.when(result.getRxNormKeyCnt()).thenReturn(24);
    Mockito.when(result.getIcd9KeyCnt()).thenReturn(4000);
    Mockito.when(result.getCptKeyCnt()).thenReturn(4000);
    Mockito.when(result.getTid(1, null)).thenReturn(100);
    Mockito.when(result.getTid(2, null)).thenReturn(200);
    Mockito.when(result.getVitals(0, null)).thenReturn(50);
    Mockito.when(result.getVitals(1, null)).thenReturn(70);
    Mockito.when(result.getVisitType(0, null)).thenReturn(70);
    Mockito.when(result.getVisitType(1, null)).thenReturn(60);
    Mockito.when(result.getVisitType(2, null)).thenReturn(50);
    Mockito.when(result.getAtc(0, null)).thenReturn(50);
    Mockito.when(result.getAtc(1, null)).thenReturn(50);
    Mockito.when(result.getLabs(1, null)).thenReturn(50);
    Mockito.when(result.getLabs(2, null)).thenReturn(60);
    Mockito.when(result.getLabs(3, null)).thenReturn(70);
    Mockito.when(result.getLabValuesToPatientCounts(1)).thenReturn(getLabValueStatisticsIterator());
    return result;
  }

  private IntKeyIntMapIterator getLabValueStatisticsIterator() {
    final IntKeyIntOpenHashMap result = new IntKeyIntOpenHashMap();
    result.put(1, 20);
    result.put(2, 50);
    return result.entries();
  }

  private AutosuggestRequest getRequest(final String query, final int pos) {
    return new AutosuggestRequest(query, pos);
  }

  private AutosuggestTextRequest getTextRequest(final String query, final int pos, final String selectedCode) {
    final AutosuggestTextRequest request = new AutosuggestTextRequest();
    request.setCursor(pos);
    request.setText(query);
    request.setSelectedCode(selectedCode);
    return request;
  }

  private static void assertContains(final AutosuggestResponse response, final String ... values) {
    assertContainsSome(response, values);
    Assert.assertEquals(response.getResponse().length, values.length);
  }

  private static void assertContainsSome(final AutosuggestResponse response, final String ... values) {
    for (final String value : values) {
      boolean fnd = false;
      for (final String item : response.getResponse()) {
        if (item.equals(value)) {
          fnd = true;
          break;
        }
      }
      if (!fnd) {
        Assert.fail("Item '" + value + "' not found " + Arrays.toString(response.getResponse()));
      }
    }
  }

  @Test
  public void smokeTest() throws Exception {
    final AutosuggestSearch search = new AutosuggestSearch(getUmls(), getStatistics(), getIndices());
    assertContains(search.search(getRequest("vitals(\"", 8)), "BP [50]", "Map [70]");
    assertContains(search.search(getRequest("labs(\"001\", \"", 13)), "high [50]", "normal [20]");
    assertContains(search.search(getRequest("ATC = \"li", 9)), "A1245=Liquid paraffin [50]", "A2254=Lipid modifying agents [50]");
    assertContains(search.search(getRequest("visit type = \"", 14)), "inpatient [60]", "outpatient visit [50]", "surgery [70]");
    assertContains(search.search(getRequest("visit_type = \"", 14)), "inpatient [60]", "outpatient visit [50]", "surgery [70]");
    assertContains(search.search(getRequest("ATC=\"li", 7)), "A1245=Liquid paraffin [50]", "A2254=Lipid modifying agents [50]");
    assertContains(search.search(getRequest("ATC=\"A1", 7)), "A1245=Liquid paraffin [50]");
    assertContains(search.search(getRequest("ATC=\"liquid par", 10)), "A1245=Liquid paraffin [50]");
    assertContains(search.search(getRequest("ATC=\"li pa", 10)), "A1245=Liquid paraffin [50]");
    assertContains(search.search(getRequest("rx=xana", 7)), "23=Xanax [1]");
    assertContains(search.search(getRequest("drugs(rx=xana", 13)), "23=Xanax [1]");
    assertContains(search.search(getRequest("drug(rx=xana", 12)), "23=Xanax [1]");
    assertContains(search.search(getRequest("meds(rx=xana", 12)), "23=Xanax [1]");
    assertContains(search.search(getRequest("med(rx=xana", 11)), "23=Xanax [1]");
    assertContains(search.search(getRequest("icd9=pain", 8)), "100.50=Pain in left arm [1]", "100.60=Pain in left leg [1]", "100.70=Vomiting and pain in left arm [1]");
    assertContains(search.search(getRequest("cpt=coli", 8)), "70000=Ulceritive colitis [1]");
    assertContains(search.search(getRequest("icd9=arm", 6)), "100.50=Pain in left arm [1]", "100.70=Vomiting and pain in left arm [1]");
    assertContains(search.search(getRequest("icd9=left leg", 13)), "100.60=Pain in left leg [1]");
    assertContains(search.search(getRequest("icd9=vomiting", 13)), "100.70=Vomiting and pain in left arm [1]");
    assertContains(search.search(getRequest("icd9=vom pa lef arm", 11)), "100.70=Vomiting and pain in left arm [1]");
    assertContains(search.search(getRequest("cpt=diabetic ulcer", 18)), "50000=Diabetic ulcer of left leg [1]", "60000=Non-diabetic ulcer of left leg [1]");
    assertContains(search.search(getRequest("labs(\"", 6)), "001 [50]", "002 [60]", "003 [70]");
    assertContains(search.search(getRequest("lab(\"", 5)), "001 [50]", "002 [60]", "003 [70]");
  }

  @Test
  public void autosuggest_before() throws Exception {
    final AutosuggestSearch search = new AutosuggestSearch(getUmls(), getStatistics(), getIndices());
    assertContains(search.search(getRequest("icd9=pain icd9=200.50", 9)), "100.50=Pain in left arm [1]", "100.60=Pain in left leg [1]", "100.70=Vomiting and pain in left arm [1]");
    assertContains(search.search(getRequest("icd9=307.80 icd9=pain   cpt=1014624", 21)), "100.50=Pain in left arm [1]", "100.60=Pain in left leg [1]",
        "100.70=Vomiting and pain in left arm [1]");
  }

  @Test
  public void autosuggest_replace() throws Exception {
    final AutosuggestSearch search = new AutosuggestSearch(getUmls(), getStatistics(), getIndices());
    final String query = "ICD9=100.50, RX=23, ICD9=V58, ICD9=100.";
    Assert.assertEquals("ICD9=100.50, RX=23, ICD9=V58, ICD9=aaa", search.replace(getTextRequest(query, query.length(), "aaa")).getText());
    Assert.assertEquals("ICD9=100.50, RX=23, ICD9=V58, ICD9=aaa", search.replace(getTextRequest(query, query.length(), "aaa")).getText());
    Assert.assertEquals("ICD9=100.50, RX=23, ICD9=V58, ICD9=100.", search.replace(getTextRequest(query, 11, "100.50")).getText());
  }
}
