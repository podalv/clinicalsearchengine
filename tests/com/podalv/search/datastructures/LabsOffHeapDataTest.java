package com.podalv.search.datastructures;

import java.util.Arrays;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyOpenHashMap;
import com.podalv.maps.primitive.set.IntSet;
import com.podalv.objectdb.datastructures.OffHeapIntArrayList.COMPRESSION_TYPE;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class LabsOffHeapDataTest {

  private OffHeapMemory mem = null;

  @Before
  public void setup() {
    mem = OffHeapMemory.allocateMemory(10000);
  }

  @After
  public void tearDown() {
    mem.free();
  }

  @Test
  public void singleLabSingleValueType() throws Exception {
    final PatientBuilder builder = new PatientBuilder();
    builder.idToPayload.put(1, PersistentIntArrayList.create(new int[] {100}));
    builder.idToPayload.put(2, PersistentIntArrayList.create(new int[] {101}));
    final LabsOffHeapData data = new LabsOffHeapData(mem, 0, builder, COMPRESSION_TYPE.INT);
    final IntKeyOpenHashMap map = new IntKeyOpenHashMap();
    map.put(0, new IntArrayList(new int[] {0, 1, 0, 2}));
    data.save(map);
    builder.close();
    Assert.assertArrayEquals(new int[] {100, 100, 101, 101}, data.getAllTimes().toArray());
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 0)).iterator(builder), new int[] {100, 1, 101, 2});
  }

  private void assertValues(final PayloadIterator pp, final int ... values) {
    for (int x = 0; x < values.length; x += 2) {
      Assert.assertTrue(pp.hasNext());
      pp.next();
      Assert.assertEquals(values[x], pp.getStartId());
      Assert.assertEquals(values[x], pp.getEndId());
      Assert.assertEquals(values[x + 1], pp.getPayloadId());
    }
    Assert.assertFalse(pp.hasNext());
  }

  @Test
  public void singleLabMultipleValueTypes() throws Exception {
    final PatientBuilder builder = new PatientBuilder();
    builder.idToPayload.put(1, PersistentIntArrayList.create(new int[] {100}));
    builder.idToPayload.put(2, PersistentIntArrayList.create(new int[] {101}));
    builder.idToPayload.put(3, PersistentIntArrayList.create(new int[] {102}));
    builder.idToPayload.put(4, PersistentIntArrayList.create(new int[] {103}));
    builder.idToPayload.put(5, PersistentIntArrayList.create(new int[] {104}));
    builder.idToPayload.put(6, PersistentIntArrayList.create(new int[] {105}));

    final LabsOffHeapData data = new LabsOffHeapData(mem, 0, builder, COMPRESSION_TYPE.INT);
    final IntKeyOpenHashMap map = new IntKeyOpenHashMap();
    map.put(0, new IntArrayList(new int[] {0, 1, 0, 2, 1, 3, 1, 4, 2, 5, 2, 6}));
    data.save(map);
    builder.close();

    final IntSet set = data.getUniqueLabIds();
    Assert.assertEquals(1, set.size());
    final int[] keys = set.toArray();
    Arrays.sort(keys);
    Assert.assertArrayEquals(new int[] {0}, keys);

    Assert.assertArrayEquals(new int[] {100, 100, 101, 101, 102, 102, 103, 103, 104, 104, 105, 105}, data.getAllTimes().toArray());

    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 0)).iterator(builder), new int[] {100, 1, 101, 2});
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 1)).iterator(builder), new int[] {102, 3, 103, 4});
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 2)).iterator(builder), new int[] {104, 5, 105, 6});
  }

  @Test
  public void multipleLabsMultipleValueTypes() throws Exception {
    final PatientBuilder builder = new PatientBuilder();
    final LabsOffHeapData data = new LabsOffHeapData(mem, 0, builder, COMPRESSION_TYPE.INT);
    final IntKeyOpenHashMap map = new IntKeyOpenHashMap();
    builder.idToPayload.put(1, PersistentIntArrayList.create(new int[] {100}));
    builder.idToPayload.put(2, PersistentIntArrayList.create(new int[] {101}));
    builder.idToPayload.put(3, PersistentIntArrayList.create(new int[] {102}));
    builder.idToPayload.put(4, PersistentIntArrayList.create(new int[] {103}));
    builder.idToPayload.put(5, PersistentIntArrayList.create(new int[] {104}));
    builder.idToPayload.put(6, PersistentIntArrayList.create(new int[] {105}));
    builder.idToPayload.put(7, PersistentIntArrayList.create(new int[] {200}));
    builder.idToPayload.put(8, PersistentIntArrayList.create(new int[] {201}));
    builder.idToPayload.put(9, PersistentIntArrayList.create(new int[] {202}));
    builder.idToPayload.put(10, PersistentIntArrayList.create(new int[] {203}));
    builder.idToPayload.put(11, PersistentIntArrayList.create(new int[] {204}));
    builder.idToPayload.put(12, PersistentIntArrayList.create(new int[] {205}));

    map.put(0, new IntArrayList(new int[] {0, 1, 0, 2, 1, 3, 1, 4, 2, 5, 2, 6}));
    map.put(1, new IntArrayList(new int[] {0, 7, 0, 8, 1, 9, 1, 10, 2, 11, 2, 12}));
    data.save(map);

    final IntSet set = data.getUniqueLabIds();
    Assert.assertEquals(2, set.size());
    final int[] keys = set.toArray();
    Arrays.sort(keys);
    Assert.assertArrayEquals(new int[] {0, 1}, keys);

    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 0)).iterator(builder), new int[] {100, 1, 101, 2});
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 1)).iterator(builder), new int[] {102, 3, 103, 4});
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(0, 2)).iterator(builder), new int[] {104, 5, 105, 6});

    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(1, 0)).iterator(builder), new int[] {200, 7, 201, 8});
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(1, 1)).iterator(builder), new int[] {202, 9, 203, 10});
    assertValues(new PayloadPointers(TYPE.LABS_PAYLOADS, null, builder, data.get(1, 2)).iterator(builder), new int[] {204, 11, 205, 12});
  }

  @Test
  public void header() throws Exception {
    Assert.assertEquals(0, LabsOffHeapData.calculateHeader(254, 254));
    Assert.assertEquals(10, LabsOffHeapData.calculateHeader(20000, 254));
    Assert.assertEquals(20, LabsOffHeapData.calculateHeader(70000, 254));
    Assert.assertEquals(1, LabsOffHeapData.calculateHeader(254, 20000));
    Assert.assertEquals(2, LabsOffHeapData.calculateHeader(254, 70000));
    Assert.assertEquals(11, LabsOffHeapData.calculateHeader(20000, 20000));
    Assert.assertEquals(12, LabsOffHeapData.calculateHeader(20000, 70000));
    Assert.assertEquals(21, LabsOffHeapData.calculateHeader(70000, 20000));
    Assert.assertEquals(22, LabsOffHeapData.calculateHeader(70000, 70000));
  }
}