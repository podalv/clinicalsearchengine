package com.podalv.search.datastructures.index;

import org.junit.Assert;
import org.junit.Test;

public class StringEnumIndexTest {

  @Test
  public void smokeTest() throws Exception {
    final StringEnumIndexBuilder builder = new StringEnumIndexBuilder();
    builder.add(10, "aaa");
    builder.add(20, "bbb");
    builder.add(30, "bbb");
    final StringEnumIndex index = builder.build();
    Assert.assertEquals(2, index.getId("aaa"));
    Assert.assertEquals(3, index.getId("bbb"));
    Assert.assertEquals(2, index.getValue(10));
    Assert.assertEquals(3, index.getValue(20));
    Assert.assertEquals(1, index.getCount("aaa"));
    Assert.assertEquals(2, index.getCount("bbb"));
    StringEnumIndexIterator iterator = index.iterator(index.getId("aaa"));
    Assert.assertTrue(iterator.hasNext());
    Assert.assertEquals(10, iterator.next());
    Assert.assertFalse(iterator.hasNext());
    iterator = index.iterator(index.getId("bbb"));
    Assert.assertTrue(iterator.hasNext());
    Assert.assertEquals(20, iterator.next());
    Assert.assertTrue(iterator.hasNext());
    Assert.assertEquals(30, iterator.next());
  }

  @Test
  public void getCount() throws Exception {
    final StringEnumIndexBuilder builder = new StringEnumIndexBuilder();
    builder.add(10, "aaa");
    builder.add(20, "bbb");
    builder.add(30, "bbb");
    final StringEnumIndex index = builder.build();
    Assert.assertEquals(2, index.getCount("bbb", new int[] {20, 30}));
    Assert.assertEquals(1, index.getCount("bbb", new int[] {20}));
    Assert.assertEquals(0, index.getCount("bbb", new int[] {40}));
  }
}
