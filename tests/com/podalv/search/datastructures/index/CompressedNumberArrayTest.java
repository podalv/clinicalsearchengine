package com.podalv.search.datastructures.index;

import org.junit.Assert;
import org.junit.Test;

public class CompressedNumberArrayTest {

  @Test
  public void booleanTest() throws Exception {
    final int values[] = new int[] {0, 1, 1, 0, 1, 0};
    final CompressedArray array = CompressedNumberArray.build(values);
    Assert.assertTrue(array instanceof CompressedBooleanArray);
    Assert.assertEquals(6, array.size());
    for (int x = 0; x < values.length; x++) {
      Assert.assertEquals(values[x], array.get(x));
    }
  }

  @Test
  public void byteTest() throws Exception {
    final int values[] = new int[] {0, 2, 161, 255, 11, 0};
    final CompressedArray array = CompressedNumberArray.build(values);
    Assert.assertTrue(array instanceof CompressedByteArray);
    Assert.assertEquals(6, array.size());
    for (int x = 0; x < values.length; x++) {
      Assert.assertEquals(values[x], array.get(x));
    }
  }

  @Test
  public void shortTest() throws Exception {
    final int values[] = new int[] {0, 20, 2500, 65535, 12, 0};
    final CompressedArray array = CompressedNumberArray.build(values);
    Assert.assertTrue(array instanceof CompressedShortArray);
    Assert.assertEquals(6, array.size());
    for (int x = 0; x < values.length; x++) {
      Assert.assertEquals(values[x], array.get(x));
    }
  }

  @Test
  public void intTest() throws Exception {
    final int values[] = new int[] {0, 20, 2500, 65535, Integer.MAX_VALUE, 0};
    final CompressedArray array = CompressedNumberArray.build(values);
    Assert.assertTrue(array instanceof CompressedIntArray);
    Assert.assertEquals(6, array.size());
    for (int x = 0; x < values.length; x++) {
      Assert.assertEquals(values[x], array.get(x));
    }
  }

}
