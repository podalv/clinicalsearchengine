package com.podalv.search.datastructures;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.input.StreamInput;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.objectdb.datastructures.PersistentIntKeyObjectMap;
import com.podalv.output.StreamOutput;
import com.podalv.search.index.IndexCollection;

public class VitalsOffHeapDataTest {

  private OffHeapMemory memory = null;

  @Before
  public void setup() {
    memory = OffHeapMemory.allocateMemory(100000);
  }

  @After
  public void teardown() {
    if (memory != null) {
      memory.free();
    }
  }

  @Test
  public void smokeTest() throws Exception {
    final IndexCollection indices = Mockito.mock(IndexCollection.class);
    Mockito.when(indices.getDoubleIndex(5, Double.valueOf(0.3))).thenReturn(0);
    Mockito.when(indices.getDoubleIndex(5, Double.valueOf(0.2))).thenReturn(1);
    Mockito.when(indices.getDoubleIndex(5, Double.valueOf(0.1))).thenReturn(2);
    Mockito.when(indices.getDoubleIndex(10, Double.valueOf(1.3))).thenReturn(0);
    Mockito.when(indices.getDoubleIndex(10, Double.valueOf(1.2))).thenReturn(1);
    Mockito.when(indices.getDoubleValue(5, 0)).thenReturn(0.3);
    Mockito.when(indices.getDoubleValue(5, 1)).thenReturn(0.2);
    Mockito.when(indices.getDoubleValue(5, 2)).thenReturn(0.1);
    Mockito.when(indices.getDoubleValue(10, 0)).thenReturn(1.3);
    Mockito.when(indices.getDoubleValue(10, 1)).thenReturn(1.2);
    Mockito.when(indices.getDoubleIndexSize(5)).thenReturn(3);
    Mockito.when(indices.getDoubleIndexSize(10)).thenReturn(2);
    final PersistentIntKeyObjectMap map = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
    final PersistentIntArrayList list = new PersistentIntArrayList();
    list.addAll(new int[] {2, 100, 2, 101, 2, 102, 1, 50, 1, 55, 0, 200, 0, 202});
    map.put(5, list);
    final PersistentIntArrayList list2 = new PersistentIntArrayList();
    list2.addAll(new int[] {1, 50, 1, 60, 0, 205, 0, 210});
    map.put(10, list2);

    final PatientBuilder patient = new PatientBuilder(indices);
    patient.setVitals(map);
    final ByteArrayOutputStream stream = new ByteArrayOutputStream();
    final StreamOutput out = new StreamOutput(stream);
    patient.close();
    patient.save(out);
    patient.load(new StreamInput(new ByteArrayInputStream(stream.toByteArray())), 0);
    patient.saveOffHeap(memory, 0);

    final PatientSearchModel search = new PatientSearchModel(indices, memory, 1);
    search.setPosition(0);

    Assert.assertArrayEquals(new int[] {50, 50, 55, 55, 100, 100, 101, 101, 102, 102, 200, 200, 202, 202}, search.getVitals().get(indices, 5, 0.1, 0.3).toArray());
    Assert.assertNull(search.getVitals().get(indices, 5, 0, 0.05));
    Assert.assertNull(search.getVitals().get(indices, 5, 0.4, 100));
    Assert.assertArrayEquals(new int[] {100, 100, 101, 101, 102, 102}, search.getVitals().get(indices, 5, 0.1, 0.1).toArray());
    Assert.assertArrayEquals(new int[] {50, 50, 55, 55}, search.getVitals().get(indices, 5, 0.2, 0.2).toArray());
    Assert.assertArrayEquals(new int[] {200, 200, 202, 202}, search.getVitals().get(indices, 5, 0.3, 0.3).toArray());
    Assert.assertNull(search.getVitals().get(indices, 15, 1.2, 1.3));
    Assert.assertArrayEquals(new int[] {50, 50, 60, 60, 205, 205, 210, 210}, search.getVitals().get(indices, 10, 1.2, 1.3).toArray());
  }
}