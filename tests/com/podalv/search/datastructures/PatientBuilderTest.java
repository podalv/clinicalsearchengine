package com.podalv.search.datastructures;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Assert;
import org.junit.Test;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.input.StreamInput;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.objectdb.datastructures.PersistentIntKeyObjectMap;
import com.podalv.output.StreamOutput;
import com.podalv.search.index.IndexCollection;

public class PatientBuilderTest {

  @Test
  public void saveLoad() throws Exception {
    final IndexCollection indices = new IndexCollection(null);
    indices.addDemographicRecord(new DemographicsRecord(1, "female", "asian", "jew", 100, 2012));
    final PatientBuilder builder = new PatientBuilder(indices);
    final PatientRecord<VisitRecord> visits = new PatientRecord<>(1);
    //final int visitId, final int patientId, final int year, final int age, final int duration, final String sab, final String code, final String src_visit
    visits.add(new VisitRecord(1, 2002, 10, 10, "ICD", "100.1", "Outpatient"));
    visits.add(new VisitRecord(1, 2002, 10, 10, "CPT", "99999", "Outpatient"));
    visits.add(new VisitRecord(1, 2005, 10, 10, "CPT", "88888", "Outpatient"));
    builder.recordVisit(visits);

    final IntKeyIntOpenHashMap nidToPatientId = new IntKeyIntOpenHashMap();
    nidToPatientId.put(1, 1);
    nidToPatientId.put(2, 1);
    nidToPatientId.put(3, 1);
    nidToPatientId.put(5, 1);

    final PatientRecord<TermRecord> terms = new PatientRecord<>(1);
    terms.add(new TermRecord(1, 1, 0, 0, 1));
    terms.add(new TermRecord(2, 2, 0, 0, 1));
    terms.add(new TermRecord(3, 3, 1, 0, 1));
    terms.add(new TermRecord(1, 1, 0, 1, 1));
    terms.add(new TermRecord(5, 5, 1, 1, 1));
    terms.add(new TermRecord(6, 1, 0, 0, 1));
    terms.add(new TermRecord(7, 1, 0, 0, 1));

    builder.recordTerms(terms);

    final PatientRecord<VitalsRecord> vitals = new PatientRecord<>(1);
    vitals.add(new VitalsRecord(10, "aaa", 0.5));
    vitals.add(new VitalsRecord(20, "bbb", 1.5));

    builder.recordVitals(vitals);

    final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    final StreamOutput output = new StreamOutput(outStream);
    builder.close();
    builder.save(output);

    final StreamInput input = new StreamInput(new ByteArrayInputStream(outStream.toByteArray()));
    final PatientBuilder loaded = new PatientBuilder(indices);
    loaded.load(input, 0);

    Assert.assertEquals(0, loaded.getId());

    final OffHeapMemory memory = OffHeapMemory.allocateMemory(10000);
    loaded.close();
    loaded.saveOffHeap(memory, 0);
    final PatientSearchModel model = new PatientSearchModel(indices, memory, 1);
    model.setPosition(0);

    IntArrayList data = model.getIcd9(indices.getIcd9("100.1"));
    Assert.assertEquals(1, data.size());
    for (int x = 0; x < data.size(); x++) {
      Assert.assertArrayEquals(new int[] {10, 20, 0}, model.getPayload(data.get(x)).toArray());
    }

    data = model.getCpt(indices.getCpt("99999"));
    Assert.assertEquals(1, data.size());
    for (int x = 0; x < data.size(); x++) {
      Assert.assertArrayEquals(new int[] {10, 20}, model.getPayload(data.get(x)).toArray());
    }

    data = model.getPositiveTerms(1);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 1, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getPositiveTerms(6);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 1, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getPositiveTerms(2);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 2, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getNegatedTerms(3);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 3, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getFamilyHistoryTerms(5);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 5, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getFamilyHistoryTerms(1);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 1, 1}, model.getPayload(data.get(0)).toArray());

    final MeasurementData v = model.getVitals();
    data = v.get(indices, indices.getVitalsCode("aaa"), 0, 1);
    Assert.assertEquals(2, data.size());
    Assert.assertArrayEquals(new int[] {10, 10}, data.toArray());

    data = v.get(indices, indices.getVitalsCode("bbb"), 0, 2);
    Assert.assertEquals(2, data.size());
    Assert.assertArrayEquals(new int[] {20, 20}, data.toArray());

    memory.free();
  }

  @Test
  public void saveLoad_multiple() throws Exception {
    final IndexCollection indices = new IndexCollection(null);
    indices.addDemographicRecord(new DemographicsRecord(1, "female", "asian", "jew", 100, 2012));
    final PatientBuilder builder = new PatientBuilder(indices);
    final PatientRecord<VisitRecord> visits = new PatientRecord<>(1);
    //final int visitId, final int patientId, final int year, final int age, final int duration, final String sab, final String code, final String src_visit
    visits.add(new VisitRecord(1, 2002, 10, 10, "ICD", "100.1", "Outpatient"));
    visits.add(new VisitRecord(1, 2002, 10, 10, "CPT", "99999", "Outpatient"));
    visits.add(new VisitRecord(1, 2005, 10, 10, "CPT", "88888", "Outpatient"));
    builder.recordVisit(visits);

    final PatientRecord<VitalsRecord> vitals = new PatientRecord<>(1);
    vitals.add(new VitalsRecord(10, "aaa", 0.5));
    vitals.add(new VitalsRecord(20, "bbb", 1.5));

    builder.recordVitals(vitals);

    final IntKeyIntOpenHashMap nidToPatientId = new IntKeyIntOpenHashMap();
    nidToPatientId.put(1, 1);
    nidToPatientId.put(2, 1);
    nidToPatientId.put(3, 1);
    nidToPatientId.put(5, 1);

    final PatientRecord<TermRecord> terms = new PatientRecord<>(1);
    terms.add(new TermRecord(1, 1, 0, 0, 1));
    terms.add(new TermRecord(2, 2, 0, 0, 1));
    terms.add(new TermRecord(3, 3, 1, 0, 1));
    terms.add(new TermRecord(1, 1, 0, 1, 1));
    terms.add(new TermRecord(5, 5, 1, 1, 1));
    terms.add(new TermRecord(6, 1, 0, 0, 1));
    terms.add(new TermRecord(7, 1, 0, 0, 1));

    builder.recordTerms(terms);

    final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    final StreamOutput output = new StreamOutput(outStream);
    builder.close();
    builder.save(output);

    final StreamInput input = new StreamInput(new ByteArrayInputStream(outStream.toByteArray()));
    final PatientBuilder loaded = new PatientBuilder(indices);
    loaded.load(input, 0);

    Assert.assertEquals(0, loaded.getId());

    final OffHeapMemory memory = OffHeapMemory.allocateMemory(100000);
    loaded.close();
    final long pos = loaded.saveOffHeap(memory, 0);
    System.out.println(OffHeapMemory.printRange(memory, 0, pos));
    loaded.saveOffHeap(memory, pos);
    final PatientSearchModel model = new PatientSearchModel(indices, memory, 1);
    model.setPosition(pos);

    IntArrayList data = model.getIcd9(indices.getIcd9("100.1"));
    Assert.assertEquals(1, data.size());
    for (int x = 0; x < data.size(); x++) {
      Assert.assertArrayEquals(new int[] {10, 20, 0}, model.getPayload(data.get(x)).toArray());
    }

    data = model.getCpt(indices.getCpt("99999"));
    Assert.assertEquals(1, data.size());
    for (int x = 0; x < data.size(); x++) {
      Assert.assertArrayEquals(new int[] {10, 20}, model.getPayload(data.get(x)).toArray());
    }

    data = model.getPositiveTerms(1);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 1, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getPositiveTerms(6);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 1, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getPositiveTerms(2);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 2, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getNegatedTerms(3);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 3, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getFamilyHistoryTerms(5);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 5, 1}, model.getPayload(data.get(0)).toArray());

    data = model.getFamilyHistoryTerms(1);
    Assert.assertEquals(1, data.size());
    Assert.assertArrayEquals(new int[] {0, 1, 1}, model.getPayload(data.get(0)).toArray());

    final MeasurementData v = model.getVitals();
    data = v.get(indices, indices.getVitalsCode("aaa"), 0, 1);
    Assert.assertEquals(2, data.size());
    Assert.assertArrayEquals(new int[] {10, 10}, data.toArray());

    data = v.get(indices, indices.getVitalsCode("bbb"), 0, 2);
    Assert.assertEquals(2, data.size());
    Assert.assertArrayEquals(new int[] {20, 20}, data.toArray());

    memory.free();
  }

  @Test
  public void convertEncountersToEncounterDays_single_day() throws Exception {
    final PatientBuilder patient = new PatientBuilder();
    patient.endTime = 5000;
    patient.close();
    Assert.assertArrayEquals(new int[] {100, 100 + (24 * 60)}, PatientBuilder.convertEncountersToEncounterDays(patient.getEndTime(), new IntArrayList(new int[] {100,
        110})).toArray());
  }

  @Test
  public void convertEncountersToEncounterDays_multiple_separate_days() throws Exception {
    final PatientBuilder patient = new PatientBuilder();
    patient.endTime = 5000;
    patient.close();
    Assert.assertArrayEquals(new int[] {100, 100 + (24 * 60)}, PatientBuilder.convertEncountersToEncounterDays(patient.getEndTime(), new IntArrayList(new int[] {100, 110, 10000,
        10000})).toArray());
  }

  private static PersistentIntArrayList getPayload(final int start, final int end) {
    final PersistentIntArrayList result = new PersistentIntArrayList();
    result.add(start);
    result.add(end);
    return result;
  }

  @Test
  public void sort() throws Exception {
    final PatientBuilder builder = new PatientBuilder();
    builder.idToPayload.put(0, getPayload(100, 101));
    builder.idToPayload.put(1, getPayload(100, 100));
    builder.idToPayload.put(2, getPayload(10, 10));
    Assert.assertArrayEquals(new int[] {2, 1, 0}, PatientBuilder.sortPayloadsByTime(builder, new IntArrayList(new int[] {0, 1, 2})).toArray());
  }

  @Test
  public void labValuesSort() throws Exception {
    final PersistentIntKeyObjectMap map = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
    map.put(3, PersistentIntArrayList.create(new IntArrayList(new int[] {8, 100, 3, 50, 2, 150, 8, 50, 2, 100, 3, 20})));
    map.put(1, PersistentIntArrayList.create(new IntArrayList(new int[] {8, 100, 8, 50, 8, 10, 3, 20, 8, 5})));
    PatientBuilder.sortLabsWithTypeId(map.entries());
    Assert.assertEquals(2, map.size());
    Assert.assertArrayEquals(new int[] {2, 100, 2, 150, 3, 20, 3, 50, 8, 50, 8, 100}, ((IntArrayList) map.get(3)).toArray());
    Assert.assertArrayEquals(new int[] {3, 20, 8, 5, 8, 10, 8, 50, 8, 100}, ((IntArrayList) map.get(1)).toArray());
  }

  @Test
  public void convertEncountersToEncounterDays_short() throws Exception {
    Assert.assertArrayEquals(new int[] {100, 1540, 1541, 2981}, PatientBuilder.convertEncountersToEncounterDays(10000, new IntArrayList(new int[] {100, 150, 500, 1600, 1800, 2500,
        2600, 2700})).toArray());
  }

  @Test
  public void convertEncountersToEncounterDays_long() throws Exception {
    Assert.assertArrayEquals(new int[] {100, 1540, 1541, 2981, 2982, 4422, 4423, 5863}, PatientBuilder.convertEncountersToEncounterDays(10000, new IntArrayList(new int[] {100,
        2000, 2001, 5000})).toArray());
  }

  @Test
  public void convertEncountersToEncounterDays_long_space() throws Exception {
    Assert.assertArrayEquals(new int[] {100, 1540, 1541, 2981, 5000, 6440}, PatientBuilder.convertEncountersToEncounterDays(10000, new IntArrayList(new int[] {100, 2000, 5000,
        6000})).toArray());
  }

}