var TimelineLayer = class {

	TimelineLayer() {
		
	}
	
	ID() {
		return "timeline_layer1";
	}
	
	getCanvas() {
		var result = document.getElementById(this.ID());
		if (result === undefined || result == null) {
			result = document.createElement("canvas");
			result.id = this.ID();
			QueryResultsDiv.getElement().appendChild(result);
			result.style.position = "absolute";
			result.style.left = 0;
			result.style.top = 0;		
			this.getElem().width($timelines.getWidth());
			this.getElem().height($timelines.TIMELINE_HEIGHT());
			this.getElem().css('z-index', 2);
		}
		return result;
	}
	
	clearCanvas() {
		var canvas = this.getCanvas();
		var ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	}
	
	getElem() {
		return $('#'.concat(this.ID()));
	}
	
	getContext() {
		var result = this.getCanvas().getContext("2d");
		if (result.canvas.width != this.getWidth()) {
			result.canvas.width = this.getWidth();
		}
		if (result.canvas.height != this.getHeight()) {
			result.canvas.height = this.getHeight();
		}
		return result;
	}
	
	getWidth() {
		return this.getElem().width();
	}
	
	getHeight() {
		return this.getElem().height();
	}

	setWidth(width) {
		this.getElem().width(width);
	}
	
	setHeight(height) {
		this.getElem().height(height);
	}

	update(xPosition, yPosition) {	
		if ($demographics != null && $demographics.sortedPids.length != 0) {
      		var context = this.getContext();
			context.clearRect(0, 0, context.canvas.width, context.canvas.height);
			this.highlightClickedPatient();
			if (xPosition < 10) {
				xPosition = 10;
			}
			context.fillStyle = "#cdcdcd";
			if (yPosition >= this.getHeight() - 20 && $firstPatient + $timelines.getMaxRowCnt() < $demographics.sortedPids.length-1) {
				context.fillStyle = "white";
				context.fillRect(0, this.getHeight() - 30, this.getWidth(), 30);
				context.fillStyle = "#cdcdcd";
				context.fillRect(0, this.getHeight() - 20, this.getWidth(), 20);
				context.fillStyle = "blue";
				var text = "Next set of patients...";
				context.fillText(text, (this.getWidth() / 2) - (context.measureText(text).width / 2), this.getHeight() - 5);
				return;
			}
			if (yPosition <= 20 && $firstPatient != 0) {
				context.fillRect(0, 0, this.getWidth(), 20);
				context.fillStyle = "blue";
				var text = "Previous set of patients...";
				context.fillText(text, (this.getWidth() / 2) - (context.measureText(text).width / 2), 13);
				return;
			}
			this.drawPositionIndicator(context, xPosition);
			this.drawTime(context, xPosition);
			this.drawPid(context, xPosition, yPosition);
		}
	}

	drawPid(context, xPosition, yPosition) {
		var demoPos = this.getDemoPos(yPosition);
		if (demoPos >= 0 && demoPos < $demographics.sortedPids.length) {
			context.globalAlpha = 1;
			context.fillStyle = "white";
			var pid = "PID="+this.getPatientId(yPosition);
			context.fillRect(xPosition - (context.measureText(pid).width / 2), this.getHeight() - 30, context.measureText(pid).width, 15);
			context.fillStyle = "#464646";
			context.fillText(pid, xPosition - (context.measureText(pid).width / 2), this.getHeight() - 20);
		}
	}
	
	drawPositionIndicator(context, xPosition) {
		context.globalAlpha = 1;
		context.fillStyle = "red";
		context.fillRect(xPosition, this.getHeight() - 15, 2, 6);
	}
	
	highlightClickedPatient() {
		if ($demographics == null) {
			return;
		}
		var maxPos = $timelines.getMaxRowCnt();
		if ($clickedPatientPos >= 0 && (20 + ($clickedPatientPos*$timelines.TIMELINE_ROW_HEIGHT())) < $timelines.getCanvasHeight() - 30 && $clickedPatientPos < Math.min($firstPatient + maxPos, $demographics.sortedPids.length)) {
      		var context = this.getContext();
      		context.globalAlpha = 0.15;
      		context.fillStyle = TIMELINES_SELECTED_PATIENT_COLOR;
      		context.fillRect(0, Math.round(20 + (($clickedPatientPos) * $timelines.TIMELINE_ROW_HEIGHT())), context.canvas.width, $timelines.TIMELINE_ROW_HEIGHT());
      		context.globalAlpha = 1;
		}
	}
	
	drawTime(context, xPosition) {
		var unit = $timelines.getTimeAxisTimeUnitInMinutes();
		var time = Math.floor((this.getClickedTimeInMinutes(xPosition) / DAY)) + " " + DAYS_LABEL;
		if (unit == DAY) {
			time = Math.floor((this.getClickedTimeInMinutes(xPosition) / HOUR)) + " " + HOURS_LABEL;
		} else if (unit == HOUR) {
			time = Math.floor((this.getClickedTimeInMinutes(xPosition))) + + " " + MINUTES_LABEL;
		}
		context.fillStyle = "white";
		context.fillRect(xPosition - (context.measureText(time).width / 2), this.getHeight() - 10, context.measureText(time).width, 10);
		context.fillStyle = "red";
		context.fillText(time, xPosition - (context.measureText(time).width / 2), this.getHeight() - 2);
	}

	getClickedTimeInMinutes(xPos) {
		return (Math.max(0, (xPos - 12)) / ($timelines.getContext().canvas.width - (20)) * $maxTimelineSize);
	} 

	getDemoPos(yPos) {
		return $firstPatient + Math.floor((yPos - (20)) / $timelines.TIMELINE_ROW_HEIGHT());
	}

	getPatientPos(yPos) {
		return Math.floor((yPos - (20)) / $timelines.TIMELINE_ROW_HEIGHT());
	}
	
	getPatientId(yPos) {
		var demoPos = this.getPatientPos(yPos);
		for (var x = $firstPatient; x < $demographics.sortedPids.length; x++) {
			if ($timelines.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
				demoPos--;
			}
			if (demoPos < 0) {
				return $demographics.patientIds[$demographics.sortedPids[x]];
			}
		}
		return -1;
	}

}