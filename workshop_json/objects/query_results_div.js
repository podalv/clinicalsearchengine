const QUERY_RESULTS_DIV_ID = "query_results_div";

var QueryResultsDiv = class {

		static getJQueryElement() {
			return $('#'+QUERY_RESULTS_DIV_ID);
		}
		
		static getElement() {
			return document.getElementById(QUERY_RESULTS_DIV_ID);
		}
		
		static create() {
			document.getElementById("bottom_div").style.top = "664px";
			document.getElementById("patient_cnt").textContent="ATLAS Explorer";
			document.getElementById("main_div").style.backgroundColor = "white";

			var resultsDiv;
			if (document.getElementById("info_bar") !== undefined && document.getElementById("info_bar") !== null) {
				$("#info_bar").hide();
				document.getElementById("main_div").removeChild(document.getElementById("info_bar"));
				resultsDiv = document.createElement("div");
				resultsDiv.id = QUERY_RESULTS_DIV_ID;
			} else {
				resultsDiv = document.getElementById(QUERY_RESULTS_DIV_ID);
			}
			
			document.getElementById("query_time_text").style.visibility = "visible";
			
			resultsDiv.appendChild($timelines.getCanvas());
			resultsDiv.appendChild($timelineDetails.getCanvas());
			
			return resultsDiv;
		}
		    	 
		static width() {
			return QueryResultsDiv.getJQueryElement().width();
		}
		
		static height() {
			return QueryResultsDiv.getJQueryElement().height();
		}

}
    	    	    	    			