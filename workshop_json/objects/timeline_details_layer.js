var TimelineDetailsLayer = class {
	
	ID() {
		return "timeline_detail_layer1";
	}
	
	clearCanvas() {
		var canvas = this.getCanvas();
		var ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	}
	
	getCanvas() {
		var resultsDiv = QueryResultsDiv.getElement();
		var layer1 = document.getElementById(this.ID());

		if (layer1 === undefined || layer1 === null) {
			layer1 = document.createElement("canvas");
			layer1.id = this.ID();
			resultsDiv.appendChild(layer1);
			layer1.style.position = "absolute";
			layer1 = $('#'+this.ID());
			layer1.width($('#'+$timelineDetails.ID()).width());
			layer1.height($timelines.TIMELINE_HEIGHT());
			layer1.css({ top: ($timelines.TIMELINE_HEIGHT() + 4) +'px' });					
			layer1.css('z-index', 2);		
		}
		return layer1;
	}

}