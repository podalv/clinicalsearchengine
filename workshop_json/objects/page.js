var Page = class {

	static drawQueryPage(response) {
			$('.autosuggest_div').hide();
			$('.autosuggest_help').hide();
			document.getElementById("main_div").insertBefore(QueryResultsDiv.create(), document.getElementById("bottom_div"));

			Errors.displayQueryError(response);
					
			QueryResultsDiv.getElement().onscroll = function(e) {
				document.getElementById("canvas_tooltip").style.visibility = "hidden";
			}

			document.onscroll = function(e) {
				document.getElementById("canvas_tooltip").style.visibility = "hidden";
			}
			
			WorkshopQuery.updateBasicTimelines();

			$timelineDetailsLayer.getCanvas();
			
			this.resize();
		}		
		
		static resize() {
			var size = window.innerHeight;
			var availableSize = size - $('#query_bar').height() - 
			$('#top_bar').height() -
			$('#var_top_bar').height();  
			if (availableSize < 300) {
				QueryResultsDiv.getElement().style.height = 0;
				document.getElementById("bottom_div").style.height = availableSize;
				$("#bottom_div").css('top', $('#top_bar').height()+'px');
			} else {
				QueryResultsDiv.getJQueryElement().css('height', availableSize * 0.7);
				document.getElementById("bottom_div").style.height = (availableSize * 0.3);
				$("#bottom_div").css('top', (QueryResultsDiv.getJQueryElement().height() + 5 + $('#query_bar').height())+'px');
			}
			editor.resize();
			editor.focus();
		}
		
		static redirectToAtlas() {
			window.location.replace($url);
		}
}