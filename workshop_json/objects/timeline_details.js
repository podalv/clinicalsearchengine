var TimelineDetails = class
{

	ID()
	{
		return "timeline_detail";
	}
	
	clearCanvas() {
		var canvas = this.getCanvas();
		var ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	}
	
	getCanvas()
	{
		var detail = document.getElementById(this.ID());
		if (detail === undefined || detail == null) {
			detail = document.createElement("canvas");
			detail.id = this.ID();
			detail.style.backgroundColor = "white";
			detail.style.position = "relative";
			detail.style.width = "100%";
			$('#' + this.ID()).css('z-index', 1);
		}
		return detail;
	}

	getWidth() {
		return this.getCanvas().width;
	}
	
	mouseClick(evt, height)
	{
		if ($demographics == null) {
			return;
		}
		var timelines = document.getElementById("timeline_detail_layer1");
		$('#timeline_detail_layer1').width($('#timeline_detail').width());
		var context = timelines.getContext('2d');
		var mousePos = getMousePos(timelines, evt);
		var layer1 = document.getElementById("timeline_detail_layer1");
		var ctx = layer1.getContext("2d");
		$selectedDetailTime = Math.round(mousePos.x - 2);
		
		var timelineEnd = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord($detailStartTime, $demographics.recordEnd[$currentPatientDemographics] - $demographics.recordStart[$currentPatientDemographics], $DETAILS_ZOOM_MINUTES));		

		if ($demographics != null && $demographics.sortedPids.length != 0) {
			if (mousePos.x < DETAILS_LEFT_OFFSET && $detailStartTime > 0) {
				$detailStartTime -= (ZOOM_SCROLL * $DETAILS_ZOOM_MINUTES);
				$timelineDetails.drawDetailedTimeline($selectedPid, $detailStartTime);
				$timelineDetails.mouseMove(evt, ctx.canvas.height);
				return;					
			}
			if (mousePos.x > ctx.canvas.width - DETAILS_LEFT_OFFSET && timelineEnd >= ctx.canvas.width - DETAILS_LEFT_OFFSET - 10) {
				$detailStartTime += (ZOOM_SCROLL * $DETAILS_ZOOM_MINUTES);
				$timelineDetails.drawDetailedTimeline($selectedPid, $detailStartTime);
				$timelineDetails.mouseMove(evt, ctx.canvas.height);
				return;
			}
			if (mousePos.x > ctx.canvas.width - 60 && mousePos.x < ctx.canvas.width - 40 && mousePos.y > 0 && mousePos.y < 20) {
				//zoom out				
				$DETAILS_ZOOM_MINUTES = Math.floor($DETAILS_ZOOM_MINUTES / 0.5);
				if ($DETAILS_ZOOM_MINUTES < 1) {
					$DETAILS_ZOOM_MINUTES = 1;
				}
				this.drawDetailedTimeline($selectedPid, $detailStartTime);
				return;
			}
			if (mousePos.x > ctx.canvas.width - 40 && mousePos.x < ctx.canvas.width - 20 && mousePos.y > 0 && mousePos.y < 20) {
				//zoom in
				$DETAILS_ZOOM_MINUTES = Math.ceil($DETAILS_ZOOM_MINUTES * 0.5);
				this.drawDetailedTimeline($selectedPid, $detailStartTime);
				return;
			} 
		}

		if ($detailClickedTime != -1) {
			$detailClickedTime = -1;
			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		} else {
			$detailClickedTime = $detailStartTime;
		}
		
		if ($selectedDetailTime > DETAILS_TEXT_COLUMN_WIDTH) {
			if ($demographics != null && $demographics.sortedPids.length != 0) {
				var mousePos = getMousePos(timelines, evt);
				ctx.height = height;
				ctx.canvas.height = height;
				ctx.canvas.width = $('#timeline_detail').width();
				ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
				if ($detailClickedTime != -1) {
					ctx.fillStyle = "red";
					var prevTimeInMinutes = $detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES);
					ctx.fillRect(Math.floor((prevTimeInMinutes - $detailStartTime) / $DETAILS_ZOOM_MINUTES), 30, 1, ctx.canvas.height);
					ctx.fillText(formatTime($detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES)), DETAILS_LEFT_OFFSET, 20);
				}
				var rect = layer1.getBoundingClientRect();
				var y = evt.clientY - rect.top;
				y = Math.floor(((y - 30) / 10));
				if (y >= 0 && y <= $rowHeaders.length - 1) {
					ctx.fillStyle = "green";
					ctx.globalAlpha = 0.2;
					ctx.fillRect(0, 28 + y * 10, ctx.canvas.width, 10);
					var txt = $rowHeaders[y][1];
					if (((txt != $rowHeaders[y][0] && mousePos.x <= DETAILS_TEXT_COLUMN_WIDTH) || textTooLong(ctx, txt, DETAILS_TEXT_COLUMN_WIDTH - 20)) && mousePos.x <= DETAILS_TEXT_COLUMN_WIDTH) {
						if (textTooLong(ctx, txt, DETAILS_TEXT_COLUMN_WIDTH - 20)) {
							ctx.globalAlpha = 1;
							ctx.fillStyle = "white";
							var width = ctx.measureText(txt).width + 10;
							ctx.fillRect(DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + y * 10, width, 10);
							ctx.fillStyle = "black";
							ctx.fillText(txt, DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + (y + 1) * 10, width, 10);
						} else {
							ctx.globalAlpha = 1;
							ctx.fillStyle = "white";
							width = ctx.measureText(txt).width + 10;
							ctx.fillRect(DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + y * 10, width, 10);
							ctx.fillStyle = "black";
							ctx.fillText(txt, DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + (y + 1) * 10);
						}
					}
				}
			}
		}
	}

	mouseMove(evt, height)
	{
		if ($demographics == null) {
			return;
		}
		var timelines = document.getElementById("timeline_layer1");
		var layer1 = document.getElementById("timeline_detail_layer1");
		var context = timelines.getContext('2d');
		context.clearRect(0, 0, context.canvas.width, context.canvas.height);
		$timelineLayer.highlightClickedPatient();
		var canvas = document.getElementById("timeline_detail");
		var ctx = canvas.getContext("2d");
		if ($demographics != null && $demographics.sortedPids.length != 0) {
			var startTime = $demographics.recordStart[$currentPatientDemographics];
			var endTime = $demographics.recordEnd[$currentPatientDemographics];
			var timelineEnd = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord($detailStartTime, endTime - startTime, $DETAILS_ZOOM_MINUTES));
			var mousePos = getMousePos(timelines, evt);
			ctx = layer1.getContext("2d");
			ctx.height = height;
			ctx.canvas.height = height;
			ctx.canvas.width = $('#timeline_detail').width();
			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
			var rect = layer1.getBoundingClientRect();
			var y = evt.clientY - rect.top;
			y = Math.floor(((y - 30) / 10));
			if ($detailClickedTime != -1 && $selectedDetailTime > DETAILS_TEXT_COLUMN_WIDTH && mousePos.x > DETAILS_TEXT_COLUMN_WIDTH) {
				var t = document.getElementById("timeline_detail_layer1");
				var ctx1 = t.getContext('2d');
				ctx1.fillStyle = "red";
				var prevTimeInMinutes = $detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES);
				ctx1.fillRect(Math.floor((prevTimeInMinutes - $detailStartTime) / $DETAILS_ZOOM_MINUTES), 30, 1, ctx1.canvas.height);
				var time = formatTime($detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES));
				ctx1.fillText(time, DETAILS_LEFT_OFFSET, 20);
				ctx1.fillStyle = "white";

				if ((mousePos.x <= timelineEnd) && evt.clientY - rect.top > 20 && (evt.clientY - rect.top < ctx1.canvas.height - 10)) {
					var timeText = formatTime($detailStartTime + (mousePos.x * $DETAILS_ZOOM_MINUTES) - ($detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES)));
					var xPos = mousePos.x;
					var yPos = evt.clientY - rect.top;
					if (xPos > ctx1.canvas.width - 30 - (ctx1.measureText(timeText).width + 10)) {
						xPos = xPos - 40 - (ctx1.measureText(timeText).width);
					}
					ctx1.globalAlpha = 0.8;
					ctx1.fillRect(xPos + 20, yPos - 7, ctx1.measureText(timeText).width + 20, 14);
					ctx1.fillStyle = "blue";
					ctx1.globalAlpha = 1;
					ctx1.fillText(timeText, xPos + 30, yPos + 5);
				}
			}
			if (y >= 0 && y <= $rowHeaders.length - 1) {
				ctx.fillStyle = "green";
				ctx.globalAlpha = 0.2;
				ctx.fillRect(0, 28 + y * 10, ctx.canvas.width, 10);
				var txt = $rowHeaders[y][1];
				if (((txt != $rowHeaders[y][0] && mousePos.x > DETAILS_LEFT_OFFSET && mousePos.x <= DETAILS_TEXT_COLUMN_WIDTH) || textTooLong(ctx, txt, DETAILS_TEXT_COLUMN_WIDTH - 20)) && mousePos.x > DETAILS_LEFT_OFFSET && mousePos.x <= DETAILS_TEXT_COLUMN_WIDTH) {
					if (textTooLong(ctx, txt, DETAILS_TEXT_COLUMN_WIDTH - 20)) {
						ctx.globalAlpha = 1;
						ctx.fillStyle = "white";
						var width = ctx.measureText(txt).width + 10;
						ctx.fillRect(DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + y * 10, width, 10);
						ctx.fillStyle = "black";
						ctx.fillText(txt, DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + (y + 1) * 10, width, 10);
					} else {
						ctx.globalAlpha = 1;
						ctx.fillStyle = "white";
						var width = ctx.measureText(txt).width + 10;
						ctx.fillRect(DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + y * 10, width, 10);
						ctx.fillStyle = "black";
						ctx.fillText(txt, DETAILS_TEXT_COLUMN_WIDTH - 1, 28 + (y + 1) * 10);
					}
				}
			}
			if (mousePos.x > ctx.canvas.width - DETAILS_LEFT_OFFSET && timelineEnd >= ctx.canvas.width - DETAILS_LEFT_OFFSET - 10) {
				ctx.globalAlpha = 1;
				ctx.fillStyle = "#cdcdcd";
				ctx.fillRect(ctx.canvas.width - DETAILS_LEFT_OFFSET, 0, DETAILS_LEFT_OFFSET, ctx.canvas.height);
			}
			if (mousePos.x < DETAILS_LEFT_OFFSET && $detailStartTime > 0) {
				ctx.globalAlpha = 1;
				ctx.fillStyle = "#cdcdcd";
				ctx.fillRect(0, 0, DETAILS_LEFT_OFFSET, ctx.canvas.height);
			}
		}
	}

	drawDetailedTimeline(pid, clickedTime)
	{
		$rowHeaders = [];
		var canvas = document.getElementById("timeline_detail");
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = "white";
		ctx.fillRect(0, 30, ctx.canvas.width, ctx.canvas.height);
		var startTime = $demographics.recordStart[$currentPatientDemographics];
		var endTime = $demographics.recordEnd[$currentPatientDemographics];
		var cnt = 30;

		var img = new Image;
		img.onload = function() {
		    ctx.drawImage(img, ctx.canvas.width - 40,0);
		}
		img.src = "images/zoom_in.png";

		var img2 = new Image;
		img2.onload = function() {
		    ctx.drawImage(img2, ctx.canvas.width - 60,0);
		}
		img2.src = "images/zoom_out.png";
		
		var rowCnt = this.calculateRowCnt(pid, ctx, clickedTime, startTime, $DETAILS_ZOOM_MINUTES);
		var height = (40 + (rowCnt * 10));
		ctx.height = height;
		ctx.canvas.height = height;
		$('#timeline_detail_layer1').height(height);
		$('#timeline_detail').height(height);

		var timelineEnd = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, endTime - startTime, $DETAILS_ZOOM_MINUTES));

		ctx.fillStyle = DETAILS_BACKGROUND_COLOR;
		if (timelineEnd + 2 < ctx.canvas.width - DETAILS_LEFT_OFFSET) {
			ctx.fillRect(timelineEnd + 2, cnt - 2, 1, ctx.canvas.height - 40);
		}

		for (var i = 0; i < $highlights.length; i++) {
			var highlightsDrawn = false;
			if ($highlightTypes[i] == HIGHLIGHT_TYPE_ENABLED || $highlightTypes[i] == HIGHLIGHT_TYPE_SHOW) {
				for (var x = 0; x < $highlights[i].length; x++) {
					if ($highlights[i][x][0] == pid) {
						var left = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, $highlights[i][x][1] * (60 * 24) - startTime, $DETAILS_ZOOM_MINUTES));
						var right = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, $highlights[i][x][2] * (60 * 24) - startTime, $DETAILS_ZOOM_MINUTES));
						if (!this.isRowVisible(left, right, ctx)) {
							continue;
						}
						if (right - left < 2) {
							right = left + 2;
						}
						right = Math.min(ctx.canvas.width - DETAILS_LEFT_OFFSET, right);
						ctx.fillStyle = $highlightColors[i];
						ctx.fillRect(left, cnt, right - left, 6);
						highlightsDrawn = true;
					}
				}
				if (highlightsDrawn) {
					ctx.fillStyle = $highlightColors[i];
					var shortText = shortenText(ctx, $highlightAlias[i], DETAILS_TEXT_COLUMN_WIDTH - 25);
					ctx.fillText(shortText, DETAILS_LEFT_OFFSET, cnt + 7);
					$rowHeaders.push([$highlightAlias[i], $highlightAlias[i] ]);
					cnt += 10;
				}	
			}
		}

		if ($displayedFeatures[VISIT_TYPE_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.visitTypes, null, 2, true, cnt);
		}
		if ($displayedFeatures[ICD9_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.icd9, $codeNames.icd9, 2, true, cnt);
		}
		if ($displayedFeatures[ICD10_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.icd10, $codeNames.icd10, 2, true, cnt);
		}
		if ($displayedFeatures[CPT_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.cpt, $codeNames.cpt, 2, true, cnt);
		}
		if ($displayedFeatures[DEPARTMENT_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.departments, null, 2, false, cnt);
		}
		if ($displayedFeatures[RX_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.rx, $codeNames.rxNorm, 4, true, cnt);
		}
		if ($displayedFeatures[LABS_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.labs, $codeNames.labs, 2, false, cnt);
		}
		if ($displayedFeatures[VITALS_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.vitals, null, 2, false, cnt);
		}
		if ($displayedFeatures[NOTE_TYPE_ID]) {
			cnt = this.drawMapContents(clickedTime, ctx, $dump.noteTypes, null, 1, false, cnt);
		}

		$('#timeline_detail_layer1').width($('#timeline_detail').width());
		var layer1 = document.getElementById("timeline_detail_layer1");
		removeAllListeners(layer1, 'mousemove');
		removeAllListeners(layer1, 'click');
		addListener(layer1, 'mousemove', function(evt) {$timelineDetails.mouseMove(evt, height)}, false);
		addListener(layer1, 'click', function(evt) {$timelineDetails.mouseClick(evt, height)}, false);
		ctx.fillStyle = "black";
		ctx.fillText("PID "
				+ pid
				+ ", "
				+ $demographics.genderMap[$demographics.gender[$currentPatientDemographics]]
				+ ", "
				+ $demographics.raceMap[$demographics.race[$currentPatientDemographics]]
				+ ", "
				+ $demographics.ethnicityMap[$demographics.ethnicity[$currentPatientDemographics]]
				+ ", AGE "
				+ formatTime($demographics.recordStart[$currentPatientDemographics]
						+ $detailStartTime), DETAILS_LEFT_OFFSET, 10);
		
		ctx.fillText("1 pixel = " + $DETAILS_ZOOM_MINUTES + " minutes", ctx.canvas.width - 180, 15);
	}

	drawMapContents(clickedTime, ctx, map, codeNameMap, payloadSize, hasRight, cnt)
	{
		var startTime = $demographics.recordStart[$currentPatientDemographics];
		if (map != null && typeof map != 'undefined') {
			var keys = Object.keys(map);
			var sortedKeys = [];
			for (var x = 0; x < keys.length; x++) {
				sortedKeys.push(keys[x]);
			}
			sortedKeys.sort();
			keys = sortedKeys;
			for (var x = 0; x < keys.length; x++) {
				var values = map[keys[x]];
				var somethingDrawn = false;
				ctx.fillStyle = DETAILS_BLOCK_COLOR;
				for (var y = 0; y < values.length; y += payloadSize) {
					var left = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, values[y] - startTime, $DETAILS_ZOOM_MINUTES));
					var right = left;
					if (hasRight) {
						right = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, values[y + 1] - startTime, $DETAILS_ZOOM_MINUTES));
					}
					if (!this.isRowVisible(left, right, ctx)) {
						continue;
					}
					if (right - left < 2) {
						right = left + 2;
					}
					right = Math.min(ctx.canvas.width - DETAILS_LEFT_OFFSET, right);
					ctx.fillRect(left, cnt, right - left, 6);
					somethingDrawn = true;
				}
				if (somethingDrawn) {
					ctx.fillStyle = DETAILS_TEXT_COLOR;
					var shortText = shortenText(ctx, keys[x], DETAILS_TEXT_COLUMN_WIDTH - 20);
					ctx.fillText(shortText, DETAILS_LEFT_OFFSET, cnt + 7);
					var fullName = keys[x];
					if (codeNameMap != null) {
						fullName = codeNameMap[keys[x]];
					}
					$rowHeaders.push([ keys[x], fullName ]);
					cnt += 10;
				}
			}
		}
		return cnt;
	}

	convertCoord(clickedTime, eventTime, $DETAILS_ZOOM_MINUTES)
	{
		var canvas = $timelines.getCanvas();
		var ctx = canvas.getContext("2d");
		var pos = Math.max(-1, (eventTime - clickedTime) / $DETAILS_ZOOM_MINUTES);
		if (pos > ctx.canvas.width) {
			pos = ctx.canvas.width;
		}
		return pos;
	}

	isRowVisible(left, right, ctx)
	{
		return (!isNaN(left) && !isNaN(right) && ((left > DETAILS_TEXT_COLUMN_WIDTH - 1 && left < ctx.canvas.width - DETAILS_LEFT_OFFSET) || (left < ctx.canvas.width - DETAILS_LEFT_OFFSET && right > DETAILS_TEXT_COLUMN_WIDTH - 1)));
	}

	calculateMapRows(map, ctx, payloadSize, hasEnd, clickedTime, startTime)
	{
		var rowCnt = 0;
		if (map != null && typeof map != 'undefined') {
			var keys = Object.keys(map);
			for (var x = 0; x < keys.length; x++) {
				var values = map[keys[x]];
				for (var y = 0; y < values.length; y += payloadSize) {
					var left = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, values[y] - startTime, $DETAILS_ZOOM_MINUTES));
					var right = left;
					if (hasEnd) {
						right = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, values[y + 1] - startTime, $DETAILS_ZOOM_MINUTES));
					}
					if (this.isRowVisible(left, right, ctx)) {
						rowCnt++
						break;
					}
				}
			}
		}
		return rowCnt;
	}

	getAvailableWidth() {
		return this.getWidth() - DETAILS_TEXT_COLUMN_WIDTH - DETAILS_LEFT_OFFSET;
	}
	
	getMaxTimeLength(zoom) {
		return this.getAvailableWidth() * zoom;
	}
	
	getZoomToFit(timelineLength) {
		var result = timelineLength / this.getAvailableWidth();
		if (result < 1) {
			return 1;
		}
		return Math.ceil(result);
	}
	
	calculateRowCnt(pid, ctx, clickedTime, startTime, zoom)
	{
		var rowCnt = 0;
		for (var i = 0; i < $highlights.length; i++) {
			for (var x = 0; x < $highlights[i].length; x++) {
				if ($highlights[i][x][0] == pid) {
					var left = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, $highlights[i][x][1] * (60 * 24) - startTime, zoom));
					var right = Math.round(DETAILS_TEXT_COLUMN_WIDTH + this.convertCoord(clickedTime, $highlights[i][x][2] * (60 * 24) - startTime, zoom));
					if (this.isRowVisible(left, right, ctx)) {
						rowCnt++;
						break;
					}
				}
			}
		}

		rowCnt += this.calculateMapRows($dump.cpt, ctx, 2, true, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.icd9, ctx, 2, true, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.icd10, ctx, 2, true, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.departments, ctx, 2, true, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.rx, ctx, 4, true, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.visitTypes, ctx, 2, true, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.labs, ctx, 2, false, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.vitals, ctx, 2, false, clickedTime, startTime, zoom);
		rowCnt += this.calculateMapRows($dump.noteTypes, ctx, 1, false, clickedTime, startTime, zoom);

		return rowCnt;
	}
}