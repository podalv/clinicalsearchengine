const YEARS_LABEL = "years";
const MONTHS_LABEL = "months";
const DAYS_LABEL = "days";
const HOURS_LABEL = "hours";
const MINUTES_LABEL = "minutes";
const YEAR = 365 * 24 * 60;
const DAY = 24 * 60;
const MONTH = 30 * 24 * 60;
const HOUR = 60;
const TIMELINE_UNIT_COUNT = 30;
const TIMELINE_ROW_HEIGHT = 7;
const TIMELINE_ROW_SPACE = 1;

var Timelines = class {

	TIMELINE_HEIGHT() {
		return 400;
	}

	TIMELINE_ROW_HEIGHT() {
		return TIMELINE_ROW_HEIGHT;
	}
	
	TIMELINE_BAR_HEIGHT() {
		return TIMELINE_ROW_HEIGHT - TIMELINE_ROW_SPACE;
	}
	
	ID() {
		return "timelines";
	}
	
	clearCanvas() {
		var canvas = this.getCanvas();
		var ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	}
	
	getCanvas() {
		var result = document.getElementById(this.ID());
		if (result === undefined || result == null) {				
			result = document.createElement("canvas");
			result.id = this.ID();
			result.style.position = "relative";
			result.style.left = "0";
			result.style.top = "0";
			result.style.width = "100%";
			result.style.zIndex = "1";
			result.style.height = this.TIMELINE_HEIGHT();
		}
		return result;
	}
	
	getContext() {
		var result = this.getCanvas().getContext("2d");
		if (result.canvas.width != this.getWidth()) {
			result.canvas.width = this.getWidth();
		}
		if (result.canvas.height != this.TIMELINE_HEIGHT()) {
			result.canvas.height = this.TIMELINE_HEIGHT();
		}
		return result;
	}
	
	getTimeAxisLabel(timeUnit) {
		if (timeUnit == (YEAR)) {
			return YEARS_LABEL;
		} 
		if (timeUnit == (MONTH)) {
			return MONTHS_LABEL;
		}
		if (timeUnit == (DAY)) {
			return DAYS_LABEL;
		}
		return HOURS_LABEL;
	}
	
	getTimeAxisTimeUnitInMinutes() {
		if ($maxTimelineSize / HOUR <= TIMELINE_UNIT_COUNT) {
			return HOUR;
		}
		if ($maxTimelineSize / (DAY) <= TIMELINE_UNIT_COUNT) {
			return DAY;
		}
		if ($maxTimelineSize / (MONTH) <= TIMELINE_UNIT_COUNT) {
			return MONTH;
		}
		return YEAR; 
	}
	
	getMaxRowCnt() {
		return Math.floor(($timelines.getCanvasHeight() - 50) / TIMELINE_ROW_HEIGHT);
	}
	
	update() {
		$maxTimelineSize = 1;
		var maxPos = this.getMaxRowCnt();
		var cnt = 0;
		for (var x = $firstPatient; x < $demographics.sortedPids.length; x++) {
			if (this.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
				$maxTimelineSize = Math.max($maxTimelineSize, $demographics.recordEnd[$demographics.sortedPids[x]] - $demographics.recordStart[$demographics.sortedPids[x]]);
				cnt++;
			}
			if (cnt >= maxPos) {
				break;
			}
		}
		var max = $maxTimelineSize;
		if ($maxTimelineSize < 50) {
			$maxTimelineSize = 50;
		}

		$timelines.setWidth(QueryResultsDiv.width()-20);
		
		var ctx = $timelines.getContext();
		ctx.clearRect(0, 0, this.getWidth(), this.getHeight());
		
		var canvasWidth = $timelines.getWidth() - 20;
		var cnt = 20;
		var currentPatientIndex = 0;

		$("#timeline_detail").width(QueryResultsDiv.width()-20);
		$("#timeline_detail").height(DETAILS_HEIGHT);
		var detail = document.getElementById("timeline_detail");
		var c = detail.getContext("2d");
		c.canvas.width = $("#timeline_detail").width();
		c.canvas.height = $("#timeline_detail").height();
		
		this.drawXAxis();
		
		var displayedCnt = 0;
		for (x = $firstPatient; x < $demographics.sortedPids.length; x++) {
			if (displayedCnt >= maxPos) {
				break;
			}
			if (!this.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
				continue;
			}
			var l = (($demographics.recordEnd[$demographics.sortedPids[x]] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize);
			var ll = Math.round(l * canvasWidth);
			if (ll < 1) {
				ll = 1;
			}
			ctx.fillStyle = "#f4f4f4";
			ctx.fillRect(10, cnt, ll, this.TIMELINE_BAR_HEIGHT());
			ctx.fillStyle = "#dbdbdb";
		    for (var y = 0; y < $codes.length; y++) {
		    	if ($codes[y].patientId == $demographics.patientIds[$demographics.sortedPids[x]]) {
		    		if ($codes[y].icd9 != null && $codes[y].icd9 !== 'undefined') {
		    			if ($displayedFeatures[ICD9_ID]) {
		    				for (var z = 0; z < $codes[y].icd9.length; z+=2) {
		    					var left = Math.round(10 + (($codes[y].icd9[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = Math.round(10 + (($codes[y].icd9[z+1] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					if (right - left < 2) {
		    						right = left + 2;
		    					}
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
						if ($displayedFeatures[ICD10_ID]) {
		    				for (var z = 0; z < $codes[y].icd10.length; z+=2) {
		    					var left = Math.round(10 + (($codes[y].icd10[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = Math.round(10 + (($codes[y].icd10[z+1] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					if (right - left < 2) {
		    						right = left + 2;
		    					}
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
		    			if ($displayedFeatures[CPT_ID]) {
		    				for (var z = 0; z < $codes[y].cpt.length; z++) {
		    					var left = Math.round(10 + (($codes[y].cpt[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = left + 2;
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
		    			if ($displayedFeatures[DEPARTMENT_ID]) {
		    				for (var z = 0; z < $codes[y].departments.length; z++) {
		    					var left = Math.round(10 + (($codes[y].departments[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = left + 2;
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
		    			if ($displayedFeatures[VITALS_ID]) {
		    				for (var z = 0; z < $codes[y].vitals.length; z++) {
		    					var left = Math.round(10 + (($codes[y].vitals[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = left + 2;
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
		    			if ($displayedFeatures[RX_ID]) {
		    				for (var z = 0; z < $codes[y].rx.length; z+=2) {
		    					var left = Math.round(10 + (($codes[y].rx[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = Math.round(10 + (($codes[y].rx[z+1] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					if (right == left) {
		    						right = left + 2;
		    					}
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
		    			if ($displayedFeatures[NOTE_TYPE_ID]) {
		    				for (var z = 0; z < $codes[y].notes.length; z++) {
		    					var left = Math.round(10 + (($codes[y].notes[z] - $demographics.recordStart[$demographics.sortedPids[x]]) / $maxTimelineSize)*canvasWidth);
		    					var right = left + 2;
		    					ctx.fillRect(left, cnt, right - left, this.TIMELINE_BAR_HEIGHT());
		    				}
		    			}
		    		}
		    	}
		    }
		    displayedCnt++;
		    cnt = cnt + TIMELINE_ROW_HEIGHT;
		}
		this.addEvents();
	}
	
	redraw(x, y) {
		if (!WorkshopQuery.queryIsInProgress()) {
			WorkshopQuery.updatePatientCnt();
			$selectedDetailTime = -1;
			$clickedPatientPos = -1;
			$detailClickedTime = -1;
			$selectedPid = -1;
			$detailStartTime = 0;
			$timelineLayer.update(x, y);
			$timelineDetailsLayer.clearCanvas();
			WorkshopQuery.updateCodes();
		} 
	}
	
	updateClickedTime() {
		if ($demographics.recordEnd[$currentPatientDemographics] - $demographics.recordStart[$currentPatientDemographics] < $timelineDetails.getMaxTimeLength(DEFAULT_ZOOM)) {
			$detailStartTime = $timelineLayer.getClickedTimeInMinutes(0);
			var newSize = $timelineDetails.getZoomToFit($demographics.recordEnd[$currentPatientDemographics] - $demographics.recordStart[$currentPatientDemographics]);
			if (newSize == 1) {
				$DETAILS_ZOOM_MINUTES = newSize;
			}
		} 
	}
	
	previousFirstPatient() {
		if ($firstPatient > 0) {
			var maxRowCnt = $timelines.getMaxRowCnt();
			var firstPos = $firstPatient;
			for (var x = $firstPatient - 1;x >= 0; x--) {
				if (this.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
					firstPos = x;
					maxRowCnt--;
					if (maxRowCnt <= 0) {
						break;
					}
				}
			}
			$firstPatient = firstPos;
		}
	}

	nextFirstPatient() {
		var maxRowCnt = $timelines.getMaxRowCnt();
		var firstPos = $firstPatient;
		for (var x = $firstPatient + 1;x < $demographics.sortedPids.length; x++) {
			if (this.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
				firstPos = x;
				maxRowCnt--;
				if (maxRowCnt <= 0) {
					break;
				}
			}
		}
		if (maxRowCnt == 0) {
			$firstPatient = firstPos;
		}
	}

	addEvents() {
		var context = $timelineLayer.getContext();
		
		removeAllListeners($timelineLayer.getCanvas(), 'mousemove');
		addListener($timelineLayer.getCanvas(), 'mousemove', function(evt) {
  			var mousePos = getMousePos(timelines, evt);
  			$timelineLayer.update(mousePos.x, mousePos.y);
  		}, false);	
  		
		removeAllListeners($timelineLayer.getCanvas(), 'click');
		addListener($timelineLayer.getCanvas(), 'click', function(event) {
			if (WorkshopQuery.queryIsInProgress()) {
				return;
			}
  			var mousePos = getMousePos(timelines, event);
  			if (mousePos.y <= 20 && $firstPatient != 0) {
  				$timelines.previousFirstPatient();
  				$timelines.redraw(mousePos.x, mousePos.y);
  				return;
  			}
  			if (mousePos.y >= $timelines.getHeight() - 20 && $firstPatient + $timelines.getMaxRowCnt() < $demographics.sortedPids.length - 1) {
  				$timelines.nextFirstPatient();
  				$timelines.redraw(mousePos.x, mousePos.y);
  				return;
  			} 
  			var pid = $timelineLayer.getPatientId(mousePos.y);
  			if (pid == -1) {
  				pid = $selectedPid;
  				return;
  			}  else {
  				$detailStartTime = $timelineLayer.getClickedTimeInMinutes(mousePos.x);
  				$clickedPatientPos = $timelineLayer.getPatientPos(mousePos.y);
  			}
  			var startTime = 0;
  			var endTime = 0;
			for (var x = 0; x < $demographics.sortedPids.length; x++) {
				if ($demographics.patientIds[$demographics.sortedPids[x]] == pid) {
					startTime = $demographics.recordStart[$demographics.sortedPids[x]];
					endTime = $demographics.recordEnd[$demographics.sortedPids[x]];
					$currentPatientDemographics = $demographics.sortedPids[x];
					break;
				}
			}
			$timelines.updateClickedTime();
  			if (pid != $selectedPid) {
  				WorkshopQuery.dump(pid, $detailStartTime);
  			    $selectedDetailTime = -1;
  			} else {
  				WorkshopQuery.getCodeNames(pid, $detailStartTime);
  			}
  			$selectedPid = pid;
      		context.clearRect(0, 0, context.canvas.width, context.canvas.height);
      		$timelineLayer.highlightClickedPatient();
      		var c = document.getElementById("timeline_detail_layer1").getContext('2d');
      		c.clearRect(0, 0, c.canvas.width, c.canvas.height);
      		if ($selectedDetailTime != -1) {
      			c.fillStyle = "red";
      			var prevTimeInMinutes = $detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES);
      			c.fillRect(Math.floor((prevTimeInMinutes - $detailStartTime) / $DETAILS_ZOOM_MINUTES), 30, 1, c.canvas.height);
  				c.fillText(formatTime($detailClickedTime + ($selectedDetailTime * $DETAILS_ZOOM_MINUTES)), 10, 20);
      		}
  		}, false);
	}
	
	drawXAxis() {
		var ctx = this.getContext();
		ctx.fillStyle = "white";
		var canvasWidth = ctx.canvas.width - 20;
		ctx.fillRect(0, this.TIMELINE_HEIGHT() - 30, canvasWidth + 20, 30);
		ctx.fillStyle = "#464646";
		ctx.fillRect(10, this.TIMELINE_HEIGHT() - 15, ctx.canvas.width, 1);
		var timeUnit = this.getTimeAxisTimeUnitInMinutes();
		ctx.fillText("(" + this.getTimeAxisLabel(timeUnit) + ")", 10 + (canvasWidth / 2) - (ctx.measureText("(" + this.getTimeAxisLabel(timeUnit) + ")").width / 2), this.TIMELINE_HEIGHT()-18);
		var cumulativeTime = 0;
		var tickCount = $maxTimelineSize / timeUnit;
		for (var x = 0; x < tickCount; x++) {
			var left = Math.round(10 + (cumulativeTime / $maxTimelineSize) * canvasWidth);
			ctx.fillRect(left, this.TIMELINE_HEIGHT() - 15, 1, 3);
			cumulativeTime += timeUnit;
			ctx.fillText(x+"", left - (ctx.measureText(x+"").width / 2), this.TIMELINE_HEIGHT()-2);
			if (cumulativeTime > $maxTimelineSize) {
				break;
			}
		}
	}

	getElem() {
		return $("#".concat(this.ID()));
	}
	
	getWidth() {
		return this.getElem().width();
	}
	
	getHeight() {
		return this.getElem().height();
	}
	
	setWidth(width) {
		this.getElem().width(width);
	}
	
	setHeight(height) {
		this.getElem().height(height);
	}
	
	getCanvasHeight() {
		return this.TIMELINE_HEIGHT();
	}
				
	highlightAll() {
		var ctx = $timelines.getContext();
		var canvasWidth = ctx.canvas.width - 20;

		var maxPos = this.getMaxRowCnt();
		var cnt = 0;
		for (var y = $firstPatient; y < $demographics.sortedPids.length; y++) {
			if (cnt >= maxPos) {
				break;
			}
			for (var i = 0; i < $highlights.length; i++) {
				if ($highlightTypes[i] == HIGHLIGHT_TYPE_ENABLED || $highlightTypes[i] == HIGHLIGHT_TYPE_SHOW) {
					for (var x = 0; x < $highlights[i].length; x++) {
						if ($highlights[i][x][0] == $demographics.patientIds[$demographics.sortedPids[y]] && this.displayPid($demographics.patientIds[$demographics.sortedPids[y]])) {
							var yPos = 20 + ((cnt) * TIMELINE_ROW_HEIGHT);
							var left = Math.round(10 + ((($highlights[i][x][1]*(60*24)) - $demographics.recordStart[$demographics.sortedPids[y]]) / $maxTimelineSize)*canvasWidth);
							var right = Math.round(10 + ((($highlights[i][x][2]*(60*24)) - $demographics.recordStart[$demographics.sortedPids[y]]) / $maxTimelineSize)*canvasWidth);
							if (right - left < 2) {
								right = left + 2;
							}
							ctx.globalAlpha = 1;
							if (Math.abs($highlights[i][x][1]*(60*24) - $demographics.recordStart[$demographics.sortedPids[y]]) < 1 && Math.abs($highlights[i][x][2]*(60*24) - $demographics.recordEnd[$demographics.sortedPids[y]]) < 1) {
								ctx.globalAlpha = 0.4;
							}
							if (ctx.globalAlpha == 1 || (ctx.globalAlpha == 0.4 && $highlightTypes[i] == HIGHLIGHT_TYPE_ENABLED)) {
								ctx.fillStyle = $highlightColors[i];
								ctx.fillRect(left, yPos, right - left, this.TIMELINE_BAR_HEIGHT());
							}
							ctx.globalAlpha = 1;
						}
					}
				}
			}
			if (this.displayPid($demographics.patientIds[$demographics.sortedPids[y]])) {
				cnt++;
			}
		}
	}

	containsPid(highlightId, pid) {
		return $highlightPids[highlightId].hasOwnProperty(pid+"");
	}

	displayPid(pid) {
		var show = true;
		for (var y = 0; y < $highlights.length; y++) {
			if ($highlightTypes[y] == HIGHLIGHT_TYPE_HIDE) {
				if (this.containsPid(y, pid)) {
					return false;
				}
			}
			if ($highlightTypes[y] == HIGHLIGHT_TYPE_SHOW) {
				show = this.containsPid(y, pid);
			}
		}
		return show;
	}
	
	getCurrentlyDisplayedPatientIds() {
		var result = [];
		if ($demographics != null && typeof $demographics != 'undefined') {
			for (var x = $firstPatient; x < Math.min($firstPatient + $timelines.getMaxRowCnt(), $demographics.sortedPids.length); x++) {
				if (this.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
					result.push($demographics.patientIds[$demographics.sortedPids[x]]);
				}
			}
		} else {
			return null;
		}
		return result;
	}

}