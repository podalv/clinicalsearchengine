class Errors {

	static displayWarning(text) {
		toastr.remove();
		toastr.options.positionClass = "toast-top-full-width";
		var message = "";
		if (typeof text == 'object') {
			for (var x = 0; x < text.length; x++) {
				message = message.concat(String(text[x]));
				if (x != text.length - 1) {
					message = message.concat("<br />");
				}
			}
		} else {
			message = text;
		}
		if (message.trim() != "") {
			toastr.warning(message);
			return true;
		}
		return false;
	}

	static displayError(text) {
		toastr.remove();
			toastr.options.positionClass = "toast-top-full-width";
		var message = "";
		if (typeof text == 'object') {
			for (var x = 0; x < text.length; x++) {
				message = message.concat(String(text[x]));
				if (x != text.length - 1) {
					message = message.concat("<br />");
				}
			}
		} else {
			message = text;
		}
		if (message.trim() != "") {
			toastr.error(message);
			return true;		
		}	
		return false;
	}

	static displayShortError(text) {
		toastr.remove();
			toastr.options.positionClass = "toast-top-center";
		toastr.error(text);			
	}
	
	static displayQueryError(response) {
		if (response != null && typeof response != 'undefined') {
			if ((response.errorMessage !== null) && typeof(response.errorMessage) !== "undefined") {
				Errors.displayError(response.errorMessage);
			} else if (response.warningMessage !== null && typeof(response.warningMessage) !== "undefined" && response.warningMessage != "Not enough patients in the cohort to display statistics") {
				Errors.displayWarning(response.warningMessage);
			}
		}
	}


}