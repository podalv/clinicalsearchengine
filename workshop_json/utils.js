		function formatNumber(nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		function updatePopulationDescription(patientCnt, encounterCnt) {
			document.getElementById("global_patient_cnt").textContent = formatNumber(patientCnt);
			document.getElementById("global_encounter_cnt").textContent = formatNumber(encounterCnt);
		}

		(function ($, undefined) {
		    $.fn.getCursorPosition = function() {
		        var el = $(this).get(0);
		        var pos = 0;
		        if('selectionStart' in el) {
		            pos = el.selectionStart;
		        } else if('selection' in document) {
		            el.focus();
		            var Sel = document.selection.createRange();
		            var SelLength = document.selection.createRange().text.length;
		            Sel.moveStart('character', -el.value.length);
		            pos = Sel.text.length - SelLength;
		        }
		        return pos;
		    }
		})(jQuery);
				
    	function formatTime(timeInMinutes) {
    		years = 0;
    		days = 0;
    		hours = 0;
    		minutes = 0;
    		symbol = "";
    		if (timeInMinutes < 0) {
    			symbol = "-";
    		}
    		timeInMinutes = Math.abs(timeInMinutes);
    		if (timeInMinutes > (365*24*60)) {
    			years = Math.floor(timeInMinutes / (365*24*60));
    		}
    		timeInMinutes = timeInMinutes - (years * 365*24*60);
    		if (timeInMinutes > (24*60)) {
    			days = Math.floor(timeInMinutes / (24*60));
    		}    		
    		timeInMinutes = timeInMinutes - (days * 24*60);
    		if (timeInMinutes > (60)) {
    			hours = Math.floor(timeInMinutes / (60));
    		}    	
    		minutes = timeInMinutes - (hours * 60);
    		result = "";
    		if (years != 0) {
    			result = years + " y"; 
    		}
    		if (days != 0 || years != 0) {
    			if (result.length != 0) {
    				result = result.concat(", ");
    			}
    			result = result.concat(days + " d");
    		}
    		if (hours != 0 || years != 0 || days != 0) {
    			if (result.length != 0) {
    				result = result.concat(", ");
    			}
    			result = result.concat(hours + " h");
    		}
			if (result.length != 0) {
				result = result.concat(", ");
			}
			result = result.concat(Math.round(minutes) + " m");
			result = symbol.concat(result);
			return result;
    	}
    	
    	function wrapText(context, text, x, y, maxWidth, lineHeight) {
            var words = text.split(' ');
            var line = '';

            for(var n = 0; n < words.length; n++) {
              var testLine = line + words[n] + ' ';
              var metrics = context.measureText(testLine);
              var testWidth = metrics.width;
              if (testWidth > maxWidth && n > 0) {
                context.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
              }
              else {
                line = testLine;
              }
            }
            context.fillText(line, x, y);
        }
    	
    	function shortenStringNoCanvas(text, lengthInCharacters) {
    		if (text.length > lengthInCharacters) {
    			text = text.substring(0, lengthInCharacters - 3) + "...";
    		}
    		return text;
    	}
    	
    	function textTooLong(ctx, text, offset) {
    		return ctx.measureText(text).width > offset;
    	}
    	
    	function shortenText(ctx, text, offset) {
    		result = text;
    		if (textTooLong(ctx, text, offset)) {
    			dotLength = ctx.measureText("...");
    			while (textTooLong(ctx, result, offset)) {
    				result = result.substring(0, result.length - 1);
    			}
    			result = result.concat("...");
    		}
    		return result;
    	}
		
		function shortenString(canvas, text, maxLen) {
			for (x = 0; x < text.length; x++) {
				result = text.substring(0, text.length - x);
				if (canvas.measureText(result+"...").width <= maxLen) {
					return result+"...";
				}
			}
			return "...";
		}

    	function getMousePos(canvas, evt) {
    		var rect = canvas.getBoundingClientRect();
    		return {
      			x: evt.clientX - rect.left,
      			y: evt.clientY - rect.top
    		};
  		}
		
		function calculateTimeUnit(cohortData) {
			var fraction = Math.round((cohortData[cohortData.length - 1][0] / 365) / 5);
			if (fraction > 0 && (fraction*6*365) >= cohortData[cohortData.length - 1][0] / 365) {
				return [fraction, "years"];
			}
			fraction = Math.round((cohortData[cohortData.length - 1][0] / 30) / 5);
			if (fraction > 0 && (fraction*6*30) >= cohortData[cohortData.length - 1][0] / 30) {
				return [fraction, "months"];
			}
			fraction = Math.round((cohortData[cohortData.length - 1][0] / 7) / 5);
			if (fraction > 0 && (fraction*6*7) >= cohortData[cohortData.length - 1][0] / 7) {
				return [fraction, "weeks"];
			}
			return [Math.round(cohortData[cohortData.length - 1][0] / 5), "days"];
		}
	
		function clearDefaultText(){
			var c = document.getElementById("query_box");
			if (c.value == QUERY_BOX_DEFAULT_TEXT) {
				c.value = "";
			}
		}
		
		function setDefaultText() {
			var c = document.getElementById("query_box");
			if (c.value == "") {
				c.value = QUERY_BOX_DEFAULT_TEXT;
			}
		}
