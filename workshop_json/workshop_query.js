
$dump = null;
$codes = null;
$queryInProgress = false;
$displayedFeatures = [true, true, true, true, true, true, true, true, true];
const ICD9_ID = 0;
const CPT_ID = 1;
const LABS_ID = 2;
const VITALS_ID = 3;
const VISIT_TYPE_ID = 4;
const NOTE_TYPE_ID = 5;
const RX_ID = 6;
const ICD10_ID = 7;
const DEPARTMENT_ID = 8;

var WorkshopQuery = class {

	static clear() {
		$response = null;
		$demographics = null;
		$dump = null;
		$codes = null;
		Highlights.clear();
		$timelineDetailsLayer.clearCanvas();
		$timelineDetails.clearCanvas();
		$timelineLayer.clearCanvas();		
		$timelines.clearCanvas();
		document.getElementById("patient_cnt").textContent="ATLAS Explorer";
		document.getElementById("query_time_text").style.visibility = "hidden";
		Highlights.updateList();
	}
	
	static query() {
		if (this.queryBoxEmpty()) {
			Errors.displayShortError("There is nothing to query");
		} else {
			if (getText().trim().toLowerCase() == "clear") {
				WorkshopQuery.clear();
			} else {
				this.performQuery(true);
			}
		}
	}

	static getExistingQueryIndex(query) {
		for (var x = 0; x < $highlightQueries.length; x++) {
			if (query === $highlightQueries[x]) {
				return x;
			} 
		}
		return -1;
	}
	
	static extractActualQuery(parsedQuery) {
		var id = parsedQuery.indexOf("), ");
		if (id != -1) {
			return parsedQuery.substring(parsedQuery.indexOf("), ")+3, parsedQuery.length - 2);
		} else {
			return parsedQuery.substring(7, parsedQuery.length - 1);
		}
	}

	static performQuery(queryTimeIntervals) {
		var query = getText();
		var alias = query;
		var status = query;
		query = query;
		var request = {
		   	'query': query,
		   	'returnTimeIntervals': queryTimeIntervals,
		   	'searchablePids' : WorkshopQuery.getPatientIds(),
		   	'returnSurvivalData': false,
		   	'statisticsLimit': 0
		}
		try {
			if ($queryInProgress) {
				console.log("Request in progress...");
				return;
			}
			$queryInProgress = true;
			$("#query_button").removeClass("query_button");
			$("#query_button").addClass("query_progress_button");		
			$.ajax({
				url : $url + '/query',
				type: 'post',
				dataType: 'json',
				data: JSON.stringify(request),
				done: function(response){
					$queryInProgress = false;
					if ($response == null) {
						$response = response;
						Page.drawQueryPage(response);
					}
					if (response.consoleOutput.length != 0) {
						for (var x = 0; x < response.consoleOutput.length; x++) {
							console.log(response.consoleOutput[x]);
						}
					}
					if (WorkshopQuery.displayErrorMessage(response.errorMessage, response.warningMessage)) {
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
						return;
					}
					var queryId = WorkshopQuery.getExistingQueryIndex(WorkshopQuery.extractActualQuery(response.parsedQuery));
					if (queryId != -1) {
						$highlightAlias[queryId] = WorkshopQuery.extractActualQuery(response.parsedQuery);
						$highlightTypes[queryId] = HIGHLIGHT_TYPE_ENABLED;							
						Highlights.updateList();
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");						
					} else {
						alias = response.originalUnparsedQuery;
						$highlights.push(response.patientIds);
						WorkshopQuery.assignColor();
						$highlightAlias.push(alias);
						$highlightTypes.push(HIGHLIGHT_TYPE_ENABLED);
						$highlightQueries.push(WorkshopQuery.extractActualQuery($response.parsedQuery));
						var set = {};
						for (var x = 0; x < response.patientIds.length; x++) {
							set[response.patientIds[x][0]+""] = "a";
						}
						$highlightPids.push(set);
						if ($demographics != null && typeof $demographics != 'undefined') {
							WorkshopQuery.updateCodes();
						}
						Highlights.updateList();
					}
				},
				success: function(response){
					$queryInProgress = false;
					if ($response == null) {
						$response = response;
						Page.drawQueryPage(response);
					}
					if (response.consoleOutput.length != 0) {
						for (var x = 0; x < response.consoleOutput.length; x++) {
							console.log(response.consoleOutput[x]);
						}
					}
					if (WorkshopQuery.displayErrorMessage(response.errorMessage, response.warningMessage)) {
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
						return;
					}
					var queryId = WorkshopQuery.getExistingQueryIndex(WorkshopQuery.extractActualQuery(response.parsedQuery));
					if (queryId != -1) {
						$highlightAlias[queryId] = WorkshopQuery.extractActualQuery(response.parsedQuery);
						$highlightTypes[queryId] = HIGHLIGHT_TYPE_ENABLED;							
						Highlights.updateList();
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
					} else {
						alias = response.originalUnparsedQuery;
						$highlights.push(response.patientIds);
						WorkshopQuery.assignColor();
						$highlightAlias.push(alias);
						$highlightTypes.push(HIGHLIGHT_TYPE_ENABLED);
						$highlightQueries.push(WorkshopQuery.extractActualQuery($response.parsedQuery));
						var set = {};
						for (var x = 0; x < response.patientIds.length; x++) {
							set[response.patientIds[x][0]+""] = "a";
						}
						$highlightPids.push(set);
						if ($demographics != null && typeof $demographics != 'undefined') {
							WorkshopQuery.updateCodes();
						}
						Highlights.updateList();
					}
				}, 
				fail: function() {
					$queryInProgress = false;
					Errors.displayShortError("Error querying server");
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");
				}
			});
		} catch (err) {
			$queryInProgress = false;
			Errors.displayError(err.message);
			$("#query_button").removeClass("query_progress_button");
			$("#query_button").addClass("query_button");
		}
	}

	static assignColor() {
		for (var y = 0; y < HIGHLIGHT_COLORS.length; y++) {
			for (var x = 0; x < $highlightColors.length; x++) {
				var fnd = false;			
				if ($highlightColors[x] == HIGHLIGHT_COLORS[y]) {
					fnd = true;
					break;
				}
			}
			if (!fnd) {
				$highlightColors.push(HIGHLIGHT_COLORS[y]); 
				return;
			}
		}
		$highlightColors.push("black");
	}
	
	static displayErrorMessage(error, warning) {
		if (error !== null && typeof error !== "undefined") {
			return Errors.displayError(error);
		} else if (warning !== null && typeof warning !== "undefined" && warning != "Not enough patients in the cohort to display statistics") {
			return Errors.displayWarning(warning);
		}
		return false;
	}
	
	static replaceQueryText(clickedCode) {
		var request = {
				'selectedCode': clickedCode,
		    	'cursor': editor.getSelectionRange().start.column,
		    	'text': getCurrentLine() 
		}
		$.ajax({
        	url : $url + '/autosuggest_replace',
        	type: 'post',
        	dataType: 'json',
        	data: JSON.stringify(request),
        	success: function(response){
       			setCurrentLine(response.text);
       			$('.autosuggest_div').hide();
				enableUpDown();
				editor.selection.moveTo(editor.getSelectionRange().start.row, response.cursor);
        	}
		});	
	}
				
	static getPatientIds() {
		var result = null;
		var prevId = -1;
		if ($response != null) {
			result = [];
			for (var x = 0; x < $response.patientIds.length; x++) {
				if ($timelines.displayPid($response.patientIds[x][0]) && prevId != $response.patientIds[x][0]) {
					result.push($response.patientIds[x][0]);
				}
				prevId = $response.patientIds[x][0];
			}
		}
		return result;
	}

	static updateBasicTimelines() {
		if ($response != null && typeof $response != 'undefined') {
			var request = {
					'patientIds': this.getPatientIds()
			}
			try {
				if ($queryInProgress) {
					console.log("Request in progress...");
					return;
				}
				$queryInProgress = true;
				$.ajax({
					url : $url + '/demographics',
					type: 'post',
					dataType: 'json',
					data: JSON.stringify(request),
					done: function(response){
						$queryInProgress = false;
						$demographics = response;
						WorkshopQuery.updateCodes();
						WorkshopQuery.updatePatientCnt();
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
					},
					success: function(response){
						$queryInProgress = false;
						$demographics = response;
						WorkshopQuery.updateCodes();
						WorkshopQuery.updatePatientCnt();
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
					}, 
					fail: function() {
						$queryInProgress = false;
						Errors.displayShortError("Error querying server");
						$("#query_button").removeClass("query_progress_button");
						$("#query_button").addClass("query_button");
					}
				});
			} catch (err) {
				$queryInProgress = false;
				Errors.displayError(err.message);
				$("#query_button").removeClass("query_progress_button");
				$("#query_button").addClass("query_button");					
			}
		}
	}

	static getCodeRequestValue(value) {
		if (value != null && typeof value != 'undefined') {
			return Object.keys(value);
		} 
		return null;
	}
		
	static getCodeNames(patientId, clickedTime) {
		$("#query_button").removeClass("query_button");
		$("#query_button").addClass("query_progress_button");
		var request = {
	    	'icd9' : this.getCodeRequestValue($dump.icd9),
	    	'icd10' : this.getCodeRequestValue($dump.icd10),
	    	'cpt' : this.getCodeRequestValue($dump.cpt),
	    	'rxNorm' : this.getCodeRequestValue($dump.rx),
	    	'labs' : this.getCodeRequestValue($dump.labs),
	    	'atc' : this.getCodeRequestValue($dump.atc),
	    }
		try {
			if ($queryInProgress) {
				console.log("Request in progress...");
				return;
			}
			$queryInProgress = true;
			$.ajax({
				url : $url + '/dictionary',
				type: 'post',
				dataType: 'json',
				data: JSON.stringify(request),
				done: function(response){
					$queryInProgress = false;
					$codeNames = response;
					$timelineDetails.drawDetailedTimeline(patientId, clickedTime);
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");					
				},
				success: function(response){
					$queryInProgress = false;
					$codeNames = response;
					$timelineDetails.drawDetailedTimeline(patientId, clickedTime);
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");					
				}, 
				fail: function() {
					$queryInProgress = false;
					Errors.displayShortError("Error querying server");
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");					
				}
			});
		} catch (err) {
			$queryInProgress = false;
			Errors.displayError(err.message);
			$("#query_button").removeClass("query_progress_button");
			$("#query_button").addClass("query_button");					
		}		
	}
		
	static dump(patientId, clickedTime) {
		$("#query_button").removeClass("query_button");
		$("#query_button").addClass("query_progress_button");
		var request = {
	    	'patientId': patientId,
	    	'icd9' : true,
	    	'icd10' : true,
	    	'departments' : true,
	    	'cpt' : true,
	    	'rx' : true,
	    	'visitTypes' : true,
	    	'labs' : true,
	    	'noteTypes' : true,
	    	'vitals': true
	    }
		try {
			if ($queryInProgress) {
				console.log("Request in progress...");
				return;
			}
			$queryInProgress = true;
			$.ajax({
				url : $url + '/dump',
				type: 'post',
				dataType: 'json',
				data: JSON.stringify(request),
				done: function(response){
					$queryInProgress = false;
					$dump = response;
					WorkshopQuery.getCodeNames(patientId, clickedTime);
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");					
				},
				success: function(response){
					$queryInProgress = false;
					$dump = response;
					WorkshopQuery.getCodeNames(patientId, clickedTime);
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");
				}, 
				fail: function() {
					$queryInProgress = false;
					Errors.displayShortError("Error querying server");
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");					
				}
			});
		} catch (err) {
			$queryInProgress = false;
			Errors.displayError(err.message);
			$("#query_button").removeClass("query_progress_button");
			$("#query_button").addClass("query_button");					
		}
	}

	static queryIsInProgress() {
		return $queryInProgress;
	}
	
	static updateCodes() {
		var request = {
	    	'patientIds': $timelines.getCurrentlyDisplayedPatientIds()
	    }
		try {
			if ($queryInProgress) {
				console.log("Request in progress...");
				return;
			}
			$queryInProgress = true;
			$.ajax({
				url : $url + '/aggregate',
				type: 'post',
				dataType: 'json',
				data: JSON.stringify(request),
				done: function(response){
					$queryInProgress = false;
					$codes = response;
					$timelines.update();
					$timelines.highlightAll();
					WorkshopQuery.updatePatientCnt();
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");
				},
				success: function(response){
					$queryInProgress = false;
					$codes = response;
					$timelines.update();
					$timelines.highlightAll();
					WorkshopQuery.updatePatientCnt();
					$("#query_button").removeClass("query_progress_button");
					$("#query_button").addClass("query_button");
				}, 
				fail: function() {
					$queryInProgress = false;
					Errors.displayShortError("Error querying server");
				}
			});
		} catch (err) {
			$queryInProgress = false;
			Errors.displayError(err.message);
		}
	}
		
	static demographicsCompare (a,b) {
		return (a.recordEnd - a.recordStart) - (b.recordEnd - b.recordStart);
	}
		
	static getShowingPatientCnt() {
		var result = 0;
		for (var x = 0; x < $demographics.sortedPids.length; x++) {
			if ($timelines.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
				result++;
			}
		}
		return result;
	}
	
	static updatePatientCnt() {
		if ($response != null && typeof $response != 'undefined') {
			if ($demographics != null) {
				var cntBefore = 0;
				var cntAfter = 0;
				var maxRowCnt = $timelines.getMaxRowCnt();
				for (var x = 0; x < $demographics.sortedPids.length; x++) {
					if ($timelines.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
						if (x < $firstPatient) {
							cntBefore++;
						} else {
							cntAfter++;
							if (cntAfter >= maxRowCnt) {
								break;
							}
						}
					}
				}
				document.getElementById("query_time_text").textContent = "showing " + cntBefore + " - " + (cntBefore + cntAfter) + " range";
			}
			document.getElementById("patient_cnt").textContent = numeral(WorkshopQuery.getShowingPatientCnt()).format('0,0') + " patients";
		}
	}
				
	static queryBoxEmpty() {
		var queryText = getText();
		return (queryText.trim() == '' || queryText == QUERY_BOX_DEFAULT_TEXT);
	}

	static pids() {
		if ($demographics == null || typeof $demographics == 'undefined' || $demographics.sortedPids.length == 0) {
			Errors.displayError("Nothing to download. Specify patients by querying the main cohort query first");
			return;
		}
		var resultList = [];
		resultList.push("PATIENTS(");
		for (var x = 0; x < $demographics.sortedPids.length; x++) {
			if ($timelines.displayPid($demographics.patientIds[$demographics.sortedPids[x]])) {
				resultList.push($demographics.patientIds[$demographics.sortedPids[x]] + "\n");
				if (resultList[0] !== "PATIENTS(") {
					resultList[0] = resultList[0] + ","; 
				}
				resultList[0] = resultList[0] + $demographics.patientIds[$demographics.sortedPids[x]];
			}
		}
		resultList[0] = resultList[0] + ")\n";
		var blob = new Blob(resultList, {type: "text/plain;charset=utf-8"});
		saveAs(blob, "pids.txt");
	}
	
	static download() {
		if ($dump == null) {
			Errors.displayError("Nothing to download. Click patient's timeline to download currently displayed time window");
			return;
		}
		var result = [];
		var startTime = 0;
		var endTime = 0;
		for (var x = 0; x < $demographics.sortedPids.length; x++) {
			if ($demographics.patientIds[$demographics.sortedPids[x]] == $selectedPid) {
				startTime = $demographics.recordStart[$demographics.sortedPids[x]];
				endTime = $demographics.recordEnd[$demographics.sortedPids[x]];
				$currentPatientDemographics = $demographics.sortedPids[x];
				break;
			}
		}
		var canvas = $timelineDetails.getCanvas();
		var ctx = canvas.getContext("2d");

		for (var i = 0; i < $highlights.length; i++) {
			var highlightsDrawn = false;
			if ($highlightTypes[i] == HIGHLIGHT_TYPE_ENABLED || $highlightTypes[i] == HIGHLIGHT_TYPE_SHOW) {
				for (var x = 0; x < $highlights[i].length; x++) {
					if ($highlights[i][x][0] == $selectedPid) {
						var left = Math.round(DETAILS_TEXT_COLUMN_WIDTH + $timelineDetails.convertCoord($detailStartTime, $highlights[i][x][1] * (60 * 24) - startTime, $DETAILS_ZOOM_MINUTES));
						var right = Math.round(DETAILS_TEXT_COLUMN_WIDTH + $timelineDetails.convertCoord($detailStartTime, $highlights[i][x][2] * (60 * 24) - startTime, $DETAILS_ZOOM_MINUTES));
						if (!$timelineDetails.isRowVisible(left, right, ctx)) {
							continue;
						}
						result.push(["HIGHTLIGHT", WorkshopQuery.flattenString($highlightAlias[i]), WorkshopQuery.flattenString($highlightQueries[i]), $highlights[i][x][1], $highlights[i][x][2]]);
					}
				}
			}
		}

		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.visitTypes, null, 2, true, result, "VISIT TYPE", null);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.icd9, $codeNames.icd9, 2, true, result, "ICD9", $codeNames.icd9);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.icd10, $codeNames.icd10, 2, true, result, "ICD10", $codeNames.icd10);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.cpt, $codeNames.cpt, 2, true, result, "CPT", $codeNames.cpt);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.rx, $codeNames.rxNorm, 4, true, result, "RX", $codeNames.rxNorm);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.labs, $codeNames.labs, 2, false, result, "LABS", $codeNames.labs);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.vitals, null, 2, false, result, "VITALS", null);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.noteTypes, null, 1, false, result, "NOTE TYPES", null);
		WorkshopQuery.printMapContents($detailStartTime, ctx, $dump.departments, null, 1, false, result, "DEPARTMENTS", null);

		result.sort(this.outputCompare);
		WorkshopQuery.savePidFile("events_pid"+"_"+$demographics.patientIds[$demographics.sortedPids[x]]+".txt", result);
	}
	
	static flattenString(str) {
		return str.replace("\n", " ").replace("\t", " ");
	}
	
	static savePidFile(fileName, result) {
		var outputString;
		var array = [];
		for (var i = 0; i < result.length; i++) {
			var line = "";
			for (var x = 0; x < result[i].length; x++) {
				line += result[i][x] + "\t";
			}
			array.push(line+"\n");
		}
		var blob = new Blob(array, {type: "text/plain;charset=utf-8"});
		saveAs(blob, fileName);
	}
	
	static outputCompare (a,b) {
		return (a[2]) - (b[2]);
	}
			
	static printMapContents(clickedTime, ctx, map, codeNameMap, payloadSize, hasRight, result, type, codeNames)
	{
		var startTime = $demographics.recordStart[$currentPatientDemographics];
		if (map != null && typeof map != 'undefined') {
			var keys = Object.keys(map);
			for (var x = 0; x < keys.length; x++) {
				var values = map[keys[x]];
				var somethingDrawn = false;
				ctx.fillStyle = DETAILS_BLOCK_COLOR;
				for (var y = 0; y < values.length; y += payloadSize) {
					var left = Math.round(DETAILS_TEXT_COLUMN_WIDTH + $timelineDetails.convertCoord(clickedTime, values[y] - startTime, $DETAILS_ZOOM_MINUTES));
					var right = left;
					if (hasRight) {
						right = Math.round(DETAILS_TEXT_COLUMN_WIDTH + $timelineDetails.convertCoord(clickedTime, values[y + 1] - startTime, $DETAILS_ZOOM_MINUTES));
					}
					if (!$timelineDetails.isRowVisible(left, right, ctx)) {
						continue;
					}
					if (right - left < 2) {
						right = left + 2;
					}
					if (y != 0) {
						var same = true;						
						for (var prev = 0; prev < y; prev += payloadSize) {
							same = true;
							for (var i = 0; i < payloadSize; i++) {
								if (values[prev+i] != values[y+i]) {
									same = false;
									break;
								}
							}
							if (same) {
								break;
							}
						}
						if (same) {
							continue;
						}
					}
					var res = [];
					res.push(type);
					res.push(keys[x]);
					if (codeNames != null) {
						res.push(codeNames[keys[x]]);
					} else {
						res.push("");
					}					
					var cnt = 0;
					for (var i = y; i < y + payloadSize; i++) {
						if (cnt == 0) {
							res.push((values[i] / (60 * 24)));
							if (!hasRight) {
								res.push((values[i] / (60 * 24)));
							}
						} else if (cnt == 1 && hasRight) {
							res.push((values[i] / (60 * 24)));
						} else {
							res.push(values[i]);
						}
						cnt++;
					}
					result.push(res);
				}
			}
		}
	}

}