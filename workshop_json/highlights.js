$highlights = [];
$highlightQueries = [];
$highlightAlias = [];
$highlightPids = [];
$highlightTypes = [];
$highlightColors = [];

var Highlights = class {
	
	static updateList() {
		var list = document.getElementById('var_list');
		while (list.firstChild) {
			list.removeChild(list.firstChild);
		}
		for (var x = 0; x < $highlights.length; x++) {
			var element = document.createElement("li");
			element.innerHTML = "<a href=\"#\" class=\"var_list_item\" style=\"color: "+$highlightColors[x]+";\" onclick=\"Highlights.displayHighlightContent("+x+");return false;\">"+shortenStringNoCanvas($highlightAlias[x], 35)+"</a>";
			element.id = "var_" + x;
			list.appendChild(element);
		}
	}

	static clear() {
		$highlights = [];
		$highlightQueries = [];
		$highlightAlias = [];
		$highlightPids = [];
		$highlightTypes = [];
		$highlightColors = [];
	}
	
	static deleteHighlight(id) {
		$highlights.splice(id, 1);
		$highlightQueries.splice(id, 1);
		$highlightAlias.splice(id, 1);
		$highlightPids.splice(id, 1);
		$highlightTypes.splice(id, 1);
		$highlightColors.splice(id, 1);
	}
	
	static disable(id) {
		Highlights.closeItem(id);
		$highlightTypes[id] = HIGHLIGHT_TYPE_DISABLED;
		Highlights.displayHighlightContent(id);
		$timelines.redraw(0, 0);
	}

	static enable(id) {
		Highlights.closeItem(id);
		$highlightTypes[id] = HIGHLIGHT_TYPE_ENABLED;
		Highlights.displayHighlightContent(id);
		$timelines.redraw(0, 0);
	}

	static show(id) {
		Highlights.closeItem(id);
		$highlightTypes[id] = HIGHLIGHT_TYPE_SHOW;
		Highlights.displayHighlightContent(id);
		$timelines.redraw(0, 0);
	}

	static hide(id) {
		Highlights.closeItem(id);
		$highlightTypes[id] = HIGHLIGHT_TYPE_HIDE;
		Highlights.displayHighlightContent(id);
		$timelines.redraw(0, 0);
	}

	static delete(id) {
		Highlights.closeItem(id);
		Highlights.deleteHighlight(id);
		Highlights.updateList();
		$timelines.redraw(0, 0);
	}
	
	static closeItem(id) {
		var item = document.getElementById("var_"+ id);
		if (item.childElementCount > 1) {
			while (item.childElementCount > 1) {
				item.removeChild(item.childNodes[1]);
			}
		}
	}
	
	static displayHighlightContent(id) {
		var item = document.getElementById("var_"+ id);
		if (item.childElementCount == 1) {
			var description = document.createElement("div");
			if (shortenStringNoCanvas($highlightAlias[id], 35) !== $highlightAlias[id]) {
				description.id = "desc_" + id;
				description.innerHTML = $highlightAlias[id];
			}
			description.className = "variable_content";
			var e = document.createElement("div");
			e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.delete("+id+");return false;\">DELETE</a>";
			item.appendChild(e);
			if ($highlightTypes[id] != HIGHLIGHT_TYPE_DISABLED) {
				var e = document.createElement("div");
				e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.disable("+id+");return false;\">DISABLE</a>";
				item.appendChild(e);
			}
			if ($highlightTypes[id] == HIGHLIGHT_TYPE_DISABLED) {
				var e = document.createElement("div");
				e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.enable("+id+");return false;\">ENABLE</a>";
				item.appendChild(e);
			}
			if ($highlightTypes[id] == HIGHLIGHT_TYPE_HIDE) {
				var e = document.createElement("div");
				e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.enable("+id+");return false;\">UNHIDE PATIENTS</a>";
				item.appendChild(e);				
			}
			if ($highlightTypes[id] != HIGHLIGHT_TYPE_HIDE) {
				var e = document.createElement("div");
				e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.hide("+id+");return false;\">HIDE PATIENTS</a>";
				item.appendChild(e);				
			}
			if ($highlightTypes[id] != HIGHLIGHT_TYPE_SHOW) {
				var e = document.createElement("div");
				e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.show("+id+");return false;\">SHOW PATIENTS</a>";
				item.appendChild(e);				
			}
			if ($highlightTypes[id] == HIGHLIGHT_TYPE_SHOW) {
				var e = document.createElement("div");
				e.innerHTML = "<a href=\"#\" class=\"var_list_item\" class=\"insert_variable_button\" onclick=\"Highlights.enable("+id+");return false;\">SHOW ALL</a>";
				item.appendChild(e);				
			}
			item.appendChild(description);
		} else {
			Highlights.closeItem(id);
		}
	}
	
}