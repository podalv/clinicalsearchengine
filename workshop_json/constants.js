		const MAX_QUERY_LENGTH = 100000;
		const maleString = "male";
		const femaleString = "female";
		const asianString = "asian";
		const blackString = "black";
		const whiteString = "white";
		const ageRanges = [18, 44, 65];
		const ageLabels = ["0-18", "19-44", "45-65", "65+"];
		const encounterRanges = [10, 20, 30];
		const encounterLabels = ["1-10", "11-20", "21-30", "31+"];
		const durationRanges = [50, 99, 149];
		const durationLabels = ["1-50", "51-99", "100-149", "150+"];
		const QUERY_BOX_DEFAULT_TEXT = "START BY TYPING YOUR QUERY HERE...";	
		const HIGHLIGHT_COLORS = ['#5DA5DA', '#FAA43A', "#60BD68", "#F17CB0", "#B2912F", "#B276B2", "#DECF3F", "#F15854"];
		const DETAILS_HEIGHT = 200;
		const DETAILS_LEFT_OFFSET = 20;
		const DETAILS_RIGHT_OFFSET = 20;
		const DETAILS_TEXT_COLUMN_WIDTH = 100 + DETAILS_LEFT_OFFSET;
		
		const DEFAULT_ZOOM = 120;
		$DETAILS_ZOOM_MINUTES = DEFAULT_ZOOM;
		const ZOOM_SCROLL = 20;
		
		const HIGHLIGHT_TYPE_DISABLED = 0;
		const HIGHLIGHT_TYPE_ENABLED = 1;
		const HIGHLIGHT_TYPE_HIDE = 2;
		const HIGHLIGHT_TYPE_SHOW = 3;
		
		const DETAILS_BLOCK_COLOR = "#666666";
		const DETAILS_TEXT_COLOR = "#666666";
		const DETAILS_BACKGROUND_COLOR = "black";
		
		const TIMELINES_SELECTED_PATIENT_COLOR = "green";