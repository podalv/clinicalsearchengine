package com.podalv.extractor.optum;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import com.podalv.utils.file.FileUtils;

/** Used for consolidation
 *  Reads a single record at a time
 *
 * @author podalv
 *
 */
public class OptumMedsIterator implements SimpleIterator {

  private DataInputStream stream;
  private int             pid    = -1;
  private int             rx     = -1;
  private int             offset = -1;
  private int             year   = -1;

  public OptumMedsIterator(final InputStream stream) throws IOException {
    this.stream = new DataInputStream(new BufferedInputStream(stream));
    next();
  }

  @Override
  public int getPid() {
    return pid;
  }

  public int getRx() {
    return rx;
  }

  public int getOffset() {
    return offset;
  }

  public int getYear() {
    return year;
  }

  @Override
  public void next() throws IOException {
    try {
      pid = stream.readInt();
      rx = stream.readInt();
      offset = stream.readInt();
      year = stream.readShort();
    }
    catch (final EOFException exception) {
      FileUtils.close(stream);
      stream = null;
    }
  }

  @Override
  public boolean hasNext() {
    return stream != null;
  }

}
