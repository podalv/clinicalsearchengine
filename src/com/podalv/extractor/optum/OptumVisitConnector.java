package com.podalv.extractor.optum;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;

public class OptumVisitConnector {

  private final LinkedList<PatientRecord<VisitRecord>> queue      = new LinkedList<>();
  private final ArrayList<OptumProceduresIterator>     iteratorsP = new ArrayList<>();
  private final ArrayList<OptumVisitIterator>          iteratorsV = new ArrayList<>();
  private final ArrayList<OptumVisitMIterator>         iteratorsM = new ArrayList<>();
  private int                                          nextPid;

  public OptumVisitConnector(final File outputFolder) throws FileNotFoundException, IOException {
    final ArrayList<File> filesFd = OptumDownloader.getFiles(outputFolder, "fd");
    final ArrayList<File> filesM = OptumDownloader.getFiles(outputFolder, "m");
    final ArrayList<File> filesC = OptumDownloader.getFiles(outputFolder, "c");
    for (final File file : filesFd) {
      iteratorsP.add(new OptumProceduresIterator(new FileInputStream(file)));
    }
    for (final File file : filesM) {
      iteratorsM.add(new OptumVisitMIterator(file.getAbsolutePath(), new FileInputStream(file)));
    }
    for (final File file : filesC) {
      iteratorsV.add(new OptumVisitIterator(new FileInputStream(file)));
    }
    read();
  }

  public int getPid() {
    if (queue.size() != 0) {
      return queue.getFirst().getPatientId();
    }
    return -1;
  }

  public PatientRecord<VisitRecord> get() {
    final PatientRecord<VisitRecord> result = queue.getFirst();
    queue.removeFirst();
    read();
    return result;
  }

  private void read() {
    final int pid1 = OptumDownloader.nextPid(iteratorsP);
    final int pid2 = OptumDownloader.nextPid(iteratorsM);
    final int pid3 = OptumDownloader.nextPid(iteratorsV);
    final int prevPid = nextPid;
    nextPid = Math.min(pid1, Math.min(pid2, pid3));
    if (prevPid > nextPid) {
      System.out.println("PREV PID = " + prevPid + " / NEXT = " + nextPid);
    }
    try {
      if (nextPid < Integer.MAX_VALUE) {
        final PatientRecord<VisitRecord> record = new PatientRecord<>(nextPid);
        for (final OptumProceduresIterator i : iteratorsP) {
          if (i.getPid() == nextPid) {
            while (i.getPid() == nextPid) {
              record.add(new VisitRecord(1, i.getYear(), i.getOffset(), 0, "CPT", i.getProc(), "N/A"));
              if (!i.hasNext()) {
                break;
              }
              i.next();
            }
          }
        }
        for (final OptumVisitIterator i : iteratorsV) {
          if (i.getPid() == nextPid) {
            while (i.getPid() == nextPid) {
              for (int x = 0; x < i.getCodes().size(); x++) {
                String dept = OptumDownloader.getDepartmentValues().get(i.getDepartment());
                if (dept == null) {
                  dept = "N/A";
                }
                record.add(new VisitRecord(1, i.getYear(), i.getOffsetStart(), i.getOffsetEnd() - i.getOffsetStart(), i.getIcdFlag() == 9 ? "ICD9" : "ICD10", i.getCodes().get(x),
                    dept));
              }
              if (!i.hasNext()) {
                break;
              }
              i.next();
            }
          }
        }
        for (final OptumVisitMIterator i : iteratorsM) {
          if (i.getPid() == nextPid) {
            while (i.getPid() == nextPid) {
              for (int x = 0; x < i.getCodes().size(); x++) {
                if (!i.getCodes().get(x).startsWith("000.0")) {
                  String dept = OptumDownloader.getDepartmentValues().get(i.getDepartment());
                  if (dept == null) {
                    dept = "N/A";
                  }
                  record.add(new VisitRecord(1, i.getYear(), i.getOffset(), 0, i.getIcdFlag() == 9 ? "ICD9" : "ICD10", i.getCodes().get(x), dept));
                  record.add(new VisitRecord(1, i.getYear(), i.getOffset(), 0, "CPT", i.getProc(), "N/A"));
                }
              }
              if (!i.hasNext()) {
                break;
              }
              i.next();
            }
          }
        }
        queue.add(record);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
