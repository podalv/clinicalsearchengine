package com.podalv.extractor.optum;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.maps.primitive.map.ObjectKeyIntMapIterator;
import com.podalv.maps.primitive.map.ObjectKeyIntOpenHashMap;
import com.podalv.utils.file.FileUtils;

public class OptumQueryTool {

  private static HashMap<String, String> labsValues = new HashMap<>();
  AtomicInteger                          processed  = new AtomicInteger(0);

  static {
    labsValues.put("-", "-");
    labsValues.put("<", "BELOW ABSOLUTE LOW OFF INSTRUMENT SCALE");
    labsValues.put(">", "ABOVE ABSOLUTE HIGH OFF INSTRUMENT SCALE");
    labsValues.put("A", "ABNORMAL (APPLIES TO NON-NUMERIC RESULTS)");
    labsValues.put("AA", "VERY ABNORMAL (APPLIES TO NON-NUMERIC UNITS, ANALOGOUS TO PANIC LIMITS FOR NUMERIC UNITS)");
    labsValues.put("B", "BETTER USE WHEN DIRECTION NOT RELEVANT");
    labsValues.put("D", "SIGNIFICANT CHANGE DOWN");
    labsValues.put("H", "ABOVE HIGH NORMAL");
    labsValues.put("HH", "ABOVE UPPER PANIC LIMITS");
    labsValues.put("I", "INTERMEDIATE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("IN", "INDETERMINATE (ADDED FOR GENZYME)");
    labsValues.put("L", "BELOW LOW NORMAL");
    labsValues.put("LL", "BELOW LOWER PANIC LIMITS");
    labsValues.put("MS", "MODERATELY SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("N", "NORMAL (APPLIES TO NON-NUMERIC RESULTS)");
    labsValues.put("NG", "NEGATIVE (ADDED FOR GENZYME)");
    labsValues.put("OT", "OTHER (ADDED FOR GENZYME)");
    labsValues.put("PS", "POSITIVE (ADDED FOR GENZYME)");
    labsValues.put("R", "RESISTANT INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("S", "SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("SR", "SEE REPORT (ADDED FOR GENZYME)");
    labsValues.put("U", "SIGNIFICANT CHANGE UP");
    labsValues.put("UN", "UNKNOWN");
    labsValues.put("VS", "VERY SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("W", "WORSE USE WHEN DIRECTION NO RELEVANT");
    labsValues.put("VS", "VERY SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("W", "WORSE USE WHEN DIRECTION NO RELEVANT");
  }

  public static Iterator<String> getTableNames(final String prefix, final int startYear) {
    final HashSet<String> set = new HashSet<>();
    for (int x = startYear; x < 2017; x++) {
      for (int y = 1; y < 5; y++) {
        set.add(prefix + String.valueOf(x) + "q" + String.valueOf(y));
      }
    }
    return set.iterator();
  }

  static void queryLabs(final String labName) throws FileNotFoundException, IOException, SQLException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("lr", 2003);
    final HashMap<String, ObjectKeyIntOpenHashMap> unitsValues = new HashMap<>();
    final HashMap<String, ObjectKeyIntOpenHashMap> descValues = new HashMap<>();
    final HashMap<String, HashMap<String, ObjectKeyIntOpenHashMap>> rangeValueInterpretation = new HashMap<>();
    int emptyValueRows = 0;
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      db = Database.createInstance(settings);
      set = db.queryStreaming("SELECT RSLT_TXT, RSLT_UNIT_NM, TST_DESC, LOW_NRML, HI_NRML, ABNL_CD, RSLT_NBR FROM optumv7_dod." + name + " WHERE LOINC_CD = '" + labName + "'");
      while (set.next()) {
        String unit = set.getString(2);
        if (unit == null || unit.isEmpty() || set.wasNull()) {
          unit = "-";
        }
        String desc = set.getString(3);
        if (desc == null || desc.isEmpty() || set.wasNull()) {
          desc = "-";
        }
        String result = set.getString(1);
        if (result == null || result.isEmpty() || set.wasNull()) {
          result = null;
        }
        final double v = set.getDouble(7);
        if (set.wasNull() || v <= -1000) {
          emptyValueRows++;
          continue;
        }
        else {
          result = set.getString(7);
        }
        unit = unit.toLowerCase();
        desc = desc.toLowerCase();
        ObjectKeyIntOpenHashMap map = unitsValues.get(unit);
        if (map == null) {
          map = new ObjectKeyIntOpenHashMap();
          unitsValues.put(unit, map);
        }
        map.put(result, map.get(result) + 1);
        map = descValues.get(desc);
        if (map == null) {
          map = new ObjectKeyIntOpenHashMap();
          descValues.put(desc, map);
        }
        map.put(result, map.get(result) + 1);
        String rangeLow = set.getString(4);
        if (rangeLow == null || set.wasNull() || rangeLow.isEmpty()) {
          rangeLow = "-";
        }
        String rangeHi = set.getString(5);
        if (rangeHi == null || set.wasNull() || rangeHi.isEmpty()) {
          rangeHi = "-";
        }
        String interpretation = set.getString(6);
        if (interpretation == null || interpretation.isEmpty() || set.wasNull()) {
          interpretation = "-";
        }
        final String range = "[" + rangeLow + "," + rangeHi + "]";
        HashMap<String, ObjectKeyIntOpenHashMap> m = rangeValueInterpretation.get(range);
        if (m == null) {
          m = new HashMap<>();
          rangeValueInterpretation.put(range, m);
        }
        ObjectKeyIntOpenHashMap mm = m.get(result);
        if (mm == null) {
          mm = new ObjectKeyIntOpenHashMap();
          m.put(result, mm);
        }
        mm.put(interpretation, mm.get(interpretation) + 1);
      }
      set.close();
      db.close();
    }
    System.out.println("Empty value rows = " + emptyValueRows);
    Iterator<Entry<String, ObjectKeyIntOpenHashMap>> i = unitsValues.entrySet().iterator();
    while (i.hasNext()) {
      final Entry<String, ObjectKeyIntOpenHashMap> entry = i.next();
      final ObjectKeyIntMapIterator ii = entry.getValue().entries();
      double min = Double.MAX_VALUE;
      double max = Double.MIN_VALUE;
      int cnt = 0;
      while (ii.hasNext()) {
        ii.next();
        try {
          final double val = Double.parseDouble((String) ii.getKey());
          min = Math.min(val, min);
          max = Math.max(val, max);
          cnt++;
        }
        catch (final Exception e) {
          System.out.println("UNIT [" + entry.getKey() + "], RESULT [" + ii.getKey() + "] = " + ii.getValue());
        }
      }
      System.out.println("UNIT [" + entry.getKey() + "], RESULT [" + min + "-" + max + "] = " + cnt);
    }
    i = descValues.entrySet().iterator();
    while (i.hasNext()) {
      final Entry<String, ObjectKeyIntOpenHashMap> entry = i.next();
      final ObjectKeyIntMapIterator ii = entry.getValue().entries();
      double min = Double.MAX_VALUE;
      double max = Double.MIN_VALUE;
      int cnt = 0;
      while (ii.hasNext()) {
        ii.next();
        try {
          final double val = Double.parseDouble((String) ii.getKey());
          min = Math.min(val, min);
          max = Math.max(val, max);
          cnt++;
        }
        catch (final Exception e) {
          System.out.println("DESC [" + entry.getKey() + "], RESULT [" + ii.getKey() + "] = " + ii.getValue());
        }
      }
      System.out.println("DESC [" + entry.getKey() + "], RESULT [" + min + "-" + max + "] = " + cnt);
    }
    final Iterator<Entry<String, HashMap<String, ObjectKeyIntOpenHashMap>>> ii = rangeValueInterpretation.entrySet().iterator();
    while (ii.hasNext()) {
      final Entry<String, HashMap<String, ObjectKeyIntOpenHashMap>> entry = ii.next();
      final Iterator<Entry<String, ObjectKeyIntOpenHashMap>> iii = entry.getValue().entrySet().iterator();
      while (iii.hasNext()) {
        final Entry<String, ObjectKeyIntOpenHashMap> e = iii.next();
        final ObjectKeyIntMapIterator ix = e.getValue().entries();
        while (ix.hasNext()) {
          ix.next();
          System.out.println("RANGE [" + entry.getKey() + "], RESULT [" + e.getKey() + "], INTERPRETATION [" + labsValues.get(ix.getKey()) + "]=" + ix.getValue());
        }
      }
    }
  }

  private void getLab(final ConnectionSettings settings, final ObjectKeyIntOpenHashMap nameToRowCnt, final String loinc) throws FileNotFoundException, IOException, SQLException {
    final Iterator<String> names = getTableNames("lr", 2003);
    final Database db = Database.createInstance(settings);
    while (names.hasNext()) {
      final String name = names.next();
      final ResultSet set = db.query("SELECT COUNT(*) FROM optumv7_dod." + name + " WHERE LOINC_CD = '" + loinc + "'");
      if (set.next()) {
        synchronized (nameToRowCnt) {
          nameToRowCnt.put(loinc, nameToRowCnt.get(loinc) + set.getInt(1));
        }
      }
      set.close();
    }
    db.close();
    processed.incrementAndGet();
    if (processed.get() % 100 == 0) {
      System.out.println(processed.get());
    }
  }

  synchronized void labStats() throws FileNotFoundException, IOException, SQLException, InterruptedException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("lr", 2003);
    final Database db = Database.createInstance(settings);
    final HashSet<String> uniqueLoinc = new HashSet<>();
    while (names.hasNext()) {
      final String name = names.next();
      final ResultSet set = db.query("SELECT LOINC_CD FROM optumv7_dod." + name + " GROUP BY LOINC_CD");
      while (set.next()) {
        final String val = set.getString(1);
        if (val != null && !val.isEmpty() && !set.wasNull()) {
          uniqueLoinc.add(val);
        }
      }
      set.close();
    }
    Database.LOGGING_ENABLED = false;
    final ObjectKeyIntOpenHashMap nameToRowCnt = new ObjectKeyIntOpenHashMap();
    final Iterator<String> unique = uniqueLoinc.iterator();
    final ExecutorService executor = Executors.newFixedThreadPool(10);
    while (unique.hasNext()) {
      final String loinc = unique.next();
      executor.submit(new Runnable() {

        @Override
        public void run() {
          try {
            getLab(settings, nameToRowCnt, loinc);
          }
          catch (IOException | SQLException e) {
            e.printStackTrace();
          }
        }
      });
    }

    executor.shutdown();

    executor.awaitTermination(1000, TimeUnit.HOURS);

    final BufferedWriter writer = new BufferedWriter(new FileWriter("result.txt"));
    final ObjectKeyIntMapIterator i = nameToRowCnt.entries();
    while (i.hasNext()) {
      i.next();
      writer.write("'" + i.getKey() + "'\t" + i.getValue() + "\n");
    }
    writer.close();
    db.close();
  }

  static void extractLabCsv(final String lab) throws SQLException, IOException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("lr", 2003);
    final Database db = Database.createInstance(settings);
    final BufferedWriter writer = new BufferedWriter(new FileWriter(new File("/home/podalv/projects/labs/3094-0_train.csv")));
    writer.write("ANALYTSEQ\tPROC_CD\tRSLT\tRSLT_UNIT_NM\tSOURCE\tTST_DESC\tTST_NBR\n");
    while (names.hasNext()) {
      final String name = names.next();
      final ResultSet set = db.query("SELECT ANLYTSEQ, PROC_CD, RSLT_NBR, RSLT_TXT, RSLT_UNIT_NM, SOURCE, TST_DESC, TST_NBR FROM optumv7_dod." + name + " WHERE LOINC_CD = '" + lab
          + "' AND TST_NBR = 900343300");
      //+ "' AND RSLT_UNIT_NM <> 'mg/dl' AND RSLT_UNIT_NM <> ''");
      while (set.next()) {
        set.getDouble(3);
        String val = null;
        if (set.wasNull()) {
          val = set.getString(4);
          if (set.wasNull() || val == null || val.isEmpty()) {
            val = null;
          }
        }
        else {
          val = set.getString(3);
        }
        if (val != null) {
          writer.write(set.getString(1) + "\t" + set.getString(2) + "\t" + val + "\t" + set.getString(6) + "\t" + set.getString(7) + "\t" + set.getString(8) + "\t" + set.getString(
              5).toLowerCase() + "\n");
        }
      }
    }
    writer.close();
  }

  public static synchronized void labUnits(final String lab) throws SQLException, IOException, InterruptedException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("lr", 2003);
    final ObjectKeyIntOpenHashMap unitId = new ObjectKeyIntOpenHashMap();
    final HashMap<String, Double> min = new HashMap<>();
    final HashMap<String, Double> max = new HashMap<>();
    final HashMap<String, Double> sum = new HashMap<>();
    final ExecutorService executor = Executors.newCachedThreadPool();
    final AtomicInteger running = new AtomicInteger(0);
    while (names.hasNext()) {
      final String name = names.next();
      executor.submit(new Runnable() {

        @Override
        public void run() {
          Database db = null;
          try {
            running.incrementAndGet();
            db = Database.createInstance(settings);
            final ResultSet set = db.query("SELECT RSLT_UNIT_NM, RSLT_NBR FROM optumv7_dod." + name + " WHERE LOINC_CD = '" + lab + "'");
            while (set.next()) {
              final String unit = set.getString(1).toLowerCase();
              synchronized (unitId) {
                unitId.put(unit, unitId.get(unit) + 1);
              }
              final double val = set.getDouble(2);
              if (!set.wasNull() && (val > 0.0001)) {
                synchronized (min) {
                  if (min.containsKey(unit)) {
                    min.put(unit, Math.min(min.get(unit), val));
                  }
                  else {
                    min.put(unit, val);
                  }
                }
                synchronized (max) {
                  if (max.containsKey(unit)) {
                    max.put(unit, Math.max(max.get(unit), val));
                  }
                  else {
                    max.put(unit, val);
                  }
                }
                synchronized (sum) {
                  if (sum.containsKey(unit)) {
                    sum.put(unit, sum.get(unit) + val);
                  }
                  else {
                    sum.put(unit, val);
                  }
                }
              }
            }
            set.close();
            System.out.println(running.decrementAndGet() + " processes still running...");
          }
          catch (final SQLException e) {
            e.printStackTrace();
          }
          finally {
            if (db != null) {
              FileUtils.close(db);
            }
          }
        }
      });
    }

    executor.shutdown();

    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);

    final ObjectKeyIntMapIterator i = unitId.entries();
    while (i.hasNext()) {
      i.next();
      double average = 0;
      if (sum.containsKey(i.getKey()) && i.getValue() != 0) {
        average = sum.get(i.getKey()) / i.getValue();
      }
      System.out.println(i.getKey() + " = " + i.getValue() + " [" + min.get(i.getKey()) + ", " + average + ", " + max.get(i.getKey()));
    }
  }

  public static void labValues() throws SQLException, IOException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("lr", 2003);
    final Database db = Database.createInstance(settings);
    int cnt = 0;
    double sum = 0;
    double min = 1000000;
    double max = 0;
    while (names.hasNext()) {
      final String name = names.next();
      final ResultSet set = db.query("SELECT RSLT_NBR FROM optumv7_dod." + name + " WHERE LOINC_CD = '1920-8' AND RSLT_UNIT_NM = 'units'");
      while (set.next()) {
        for (int x = 1; x <= set.getMetaData().getColumnCount(); x++) {
          final double val = set.getDouble(1);
          if (val >= 0 && !set.wasNull()) {
            sum += set.getDouble(1);
            cnt++;
            min = Math.min(min, val);
            max = Math.max(max, val);
          }

        }
        //System.out.println(sb.toString());
      }
      set.close();
    }
    System.out.println(min + " / " + sum / cnt + " / " + max);

    db.close();
  }

  public static void labQuery() throws SQLException, IOException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("lr", 2003);
    final Database db = Database.createInstance(settings);
    while (names.hasNext()) {
      final String name = names.next();
      final ResultSet set = db.query("SELECT RSLT_NBR, RSLT_TXT FROM optumv7_dod." + name + " WHERE LOINC_CD = '2160-0' AND RSLT_UNIT_NM = ''");
      while (set.next()) {
        final StringBuilder sb = new StringBuilder();
        for (int x = 1; x <= set.getMetaData().getColumnCount(); x++) {
          String v = set.getString(x);
          if (set.wasNull()) {
            v = "-";
          }
          sb.append(v + ", ");
        }
        System.out.println(sb.toString());
      }
      set.close();
    }

    db.close();
  }

  /*  public static final String                      PROCEDURES          = "SELECT PATID, PROC_CD, FST_DT FROM optumv7_dod.";
  public static final String                      MEDS                = "SELECT PATID, NDC, Fill_Dt FROM optumv7_dod.";
  public static final String                      MEDS2               = "SELECT PATID, NDC, FST_DT FROM optumv7_dod.";
  public static final String                      LABS                = "SELECT PATID, LOINC_CD, FST_DT, RSLT_TXT, RSLT_NBR, ABNL_CD, HI_NRML, LOW_NRML, RSLT_UNIT_NM FROM optumv7_dod.";
  public static final String                      VISITS              = "SELECT PATID, ICD_FLAG, DIAG1, DIAG2, DIAG3, DIAG4, DIAG5, PROC1, PROC2, PROC3, PROC4, PROC5, ADMIT_DATE, DISCH_DATE, POS FROM optumv7_dod.";
  public static final String                      VISITS2             = "SELECT PATID, ICD_FLAG, FST_DT, POS, PROC_CD, DIAG1, DIAG2,DIAG3,DIAG4,DIAG5,DIAG6,DIAG7,DIAG8,DIAG9,DIAG10,DIAG11,DIAG12,DIAG13,DIAG14,DIAG15,DIAG16,DIAG17,DIAG18,DIAG19,DIAG20,DIAG21,DIAG22,DIAG23,DIAG24,DIAG25, PROC1, PROC2,PROC3,PROC4,PROC5,PROC6,PROC7,PROC8,PROC9,PROC10,PROC11,PROC12,PROC13,PROC14,PROC15,PROC16,PROC17,PROC18,PROC19,PROC20,PROC21,PROC22,PROC23,PROC24,PROC25 FROM optumv7_dod.";*/

  public static void queryCodes(final String code) throws SQLException, IOException {
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    final Iterator<String> names = getTableNames("c", 2004);
    final Database db = Database.createInstance(settings);
    while (names.hasNext()) {
      final String name = names.next();
      final ResultSet set = db.query("SELECT DIAG1, DIAG2, DIAG3, DIAG4, DIAG5 FROM optumv7_dod." + name + " WHERE DIAG1 like '%" + code + "%' OR DIAG2 like '%" + code
          + "%' OR DIAG3 like '%" + code + "%' OR DIAG4 like '%" + code + "%' OR DIAG5 like '%" + code + "%'");
      while (set.next()) {
        final StringBuilder sb = new StringBuilder();
        for (int x = 1; x <= set.getMetaData().getColumnCount(); x++) {
          String v = set.getString(x);
          if (set.wasNull()) {
            v = "-";
          }
          sb.append(v + ", ");
        }
        System.out.println(sb.toString());
      }
      set.close();
    }

    db.close();
  }

  public static void main(final String[] args) throws FileNotFoundException, IOException, SQLException, InterruptedException {
    //final OptumQueryTool tool = new OptumQueryTool();
    //queryCodes(".");
    //tool.labStats();
    //queryLabs(args[0]);
    //labValues();
    //labUnits("3097-3");
    //labQuery();
    //extractLabCsv("2823-3");
    //System.exit(0);
    final String patId = "33003304219"; //19355797
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File("/home/podalv/db2_settings.txt"));
    Iterator<String> names = getTableNames("fd", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      db = Database.createInstance(settings);
      Database.LOGGING_ENABLED = false;
      set = db.queryStreaming(OptumDownloader.PROCEDURES + name + " WHERE PATID = " + patId);
      while (set.next()) {
        System.out.println(set.getString(2));
      }
      set.close();
      db.close();
    }

    names = getTableNames("c", 2004);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      db = Database.createInstance(settings);
      Database.LOGGING_ENABLED = false;
      //SELECT PATID, ICD_FLAG, DIAG1, DIAG2, DIAG3, DIAG4, DIAG5, PROC1, PROC2, PROC3, PROC4, PROC5, ADMIT_DATE, DISCH_DATE, POS
      set = db.queryStreaming(OptumDownloader.VISITS + name + " WHERE PATID = " + patId);
      while (set.next()) {
        final StringBuilder line = new StringBuilder("FLAG=" + set.getString(2));
        for (int x = 3; x < 13; x++) {
          if (!set.getString(x).isEmpty()) {
            line.append("," + set.getString(x));
          }
        }
        System.out.println(line.toString());
      }
      set.close();
      db.close();
    }
    names = getTableNames("m", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      db = Database.createInstance(settings);
      Database.LOGGING_ENABLED = false;
      //SELECT PATID, ICD_FLAG, FST_DT, POS, PROC_CD, DIAG1, DIAG2,DIAG3,DIAG4,DIAG5,DIAG6,DIAG7,DIAG8,DIAG9,DIAG10,DIAG11,DIAG12,DIAG13,DIAG14,DIAG15,DIAG16,DIAG17,DIAG18,DIAG19,DIAG20,DIAG21,DIAG22,DIAG23,DIAG24,DIAG25, PROC1, PROC2,PROC3,PROC4,PROC5,PROC6,PROC7,PROC8,PROC9,PROC10,PROC11,PROC12,PROC13,PROC14,PROC15,PROC16,PROC17,PROC18,PROC19,PROC20,PROC21,PROC22,PROC23,PROC24,PROC25
      set = db.queryStreaming(OptumDownloader.VISITS2 + name + " WHERE PATID = " + patId);
      while (set.next()) {
        System.out.println(set.getString(5));
        final StringBuilder line = new StringBuilder("FLAG=" + set.getString(2));
        for (int x = 6; x < 56; x++) {
          if (!set.getString(x).isEmpty()) {
            line.append("," + set.getString(x));
          }
        }
        System.out.println(line.toString());
      }
      set.close();
      db.close();
    }
    names = getTableNames("lr", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      db = Database.createInstance(settings);
      Database.LOGGING_ENABLED = false;
      //PATID, LOINC_CD, FST_DT, RSLT_TXT, RSLT_NBR, ABNL_CD, HI_NRML, LOW_NRML
      set = db.queryStreaming(OptumDownloader.LABS + name + " WHERE PATID = " + patId);
      while (set.next()) {
        System.out.println(set.getString(2) + "," + set.getString(4) + "," + set.getString(3));
      }
      set.close();
      db.close();
    }
    names = getTableNames("r", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      db = Database.createInstance(settings);
      Database.LOGGING_ENABLED = false;
      //SELECT PATID, NDC, Fill_Dt
      set = db.queryStreaming(OptumDownloader.MEDS + name + " WHERE PATID = " + patId);
      while (set.next()) {
        System.out.println(set.getString(2));
      }
      set.close();
      db.close();
    }
  }
}