package com.podalv.extractor.optum;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.stride6.Commands;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.OhdsiDatabaseDownload;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.extractor.truven.TruvenExtractor;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.ObjectKeyIntMapIterator;
import com.podalv.maps.primitive.map.ObjectKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.index.StatisticsBuilderCompressed;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.text.TextUtils;

public class OptumDownloader {

  public static final String                      DEBUG_PID           = null;
  public static final String                      WHERE               = " WHERE PATID = " + DEBUG_PID + " ";
  public static final String                      DEMOGRAPHICS        = "select PATID, GDR_CD, YRDOB from optumv7_dod.mbr_detail " + (DEBUG_PID == null ? "" : WHERE)
      + "order by PATID";
  public static final String                      DEATH               = "SELECT PATID, YMDOD from optumv7_dod.dod " + (DEBUG_PID == null ? "" : WHERE) + "order by PATID";
  public static final String                      PROCEDURES          = "SELECT PATID, PROC_CD, FST_DT FROM optumv7_dod.";
  public static final String                      MEDS                = "SELECT PATID, NDC, Fill_Dt FROM optumv7_dod.";
  public static final String                      MEDS2               = "SELECT PATID, NDC, FST_DT FROM optumv7_dod.";
  public static final String                      LABS                = "SELECT PATID, LOINC_CD, FST_DT, RSLT_TXT, RSLT_NBR, ABNL_CD, HI_NRML, LOW_NRML, RSLT_UNIT_NM FROM optumv7_dod.";
  public static final String                      VISITS              = "SELECT PATID, ICD_FLAG, DIAG1, DIAG2, DIAG3, DIAG4, DIAG5, PROC1, PROC2, PROC3, PROC4, PROC5, ADMIT_DATE, DISCH_DATE, POS FROM optumv7_dod.";
  public static final String                      VISITS2             = "SELECT PATID, ICD_FLAG, FST_DT, POS, PROC_CD, DIAG1, DIAG2,DIAG3,DIAG4,DIAG5,DIAG6,DIAG7,DIAG8,DIAG9,DIAG10,DIAG11,DIAG12,DIAG13,DIAG14,DIAG15,DIAG16,DIAG17,DIAG18,DIAG19,DIAG20,DIAG21,DIAG22,DIAG23,DIAG24,DIAG25, PROC1, PROC2,PROC3,PROC4,PROC5,PROC6,PROC7,PROC8,PROC9,PROC10,PROC11,PROC12,PROC13,PROC14,PROC15,PROC16,PROC17,PROC18,PROC19,PROC20,PROC21,PROC22,PROC23,PROC24,PROC25 FROM optumv7_dod.";
  public static final String                      NDC_TO_RX_NORM      = "SELECT NDCNUM, RxCUI FROM optumv7_dod.NDCtoRxNormIngredient_OLD";
  private static final ObjectKeyIntOpenHashMap    patIdToPid          = new ObjectKeyIntOpenHashMap();
  private static int                              currentPid          = 1;
  private static final IntKeyObjectMap<Date>      pidToYearOfBirth    = new IntKeyObjectMap<>();
  private static HashMap<String, String>          labsValues          = new HashMap<>();
  private static HashMap<String, String>          departmentValues    = new HashMap<>();
  private static HashMap<String, String>          loinc               = new HashMap<>();
  private static HashSet<String>                  loincWithTextValues = new HashSet<>();
  private static HashSet<String>                  recordLoinc         = new HashSet<>();
  private static HashMap<String, HashSet<String>> loincToInvalidUoM   = new HashMap<>();

  static {
    final BufferedReader reader = new BufferedReader(new InputStreamReader(OptumDownloader.class.getResourceAsStream("loinc.txt")));
    String line;
    try {
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        loinc.put(data[0], data[1]);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    FileUtils.close(reader);
  }

  private static void processLoinc(final String loinc, final boolean recordTextValues, final String ... invalidUoM) {
    if (recordTextValues) {
      loincWithTextValues.add(loinc);
    }
    recordLoinc.add(loinc);
    if (invalidUoM != null) {
      final HashSet<String> s = new HashSet<>();
      for (final String uom : invalidUoM) {
        s.add(uom.toLowerCase());
      }
      loincToInvalidUoM.put(loinc, s);
    }
  }

  static {
    processLoinc("44306-9", true);
    processLoinc("45313-4", false);
    processLoinc("81625-6", false);
    processLoinc("8243-8", true);
    processLoinc("33060-5", false);
    processLoinc("3094-0", true, "meq/l");
    processLoinc("2823-3", true, "%", "mm/l", "percent", "ng/ml", "thous/mcl", "milliequivalent pe", "mg/dl", "thous/cu.mm", "g/dl", "not tested", "mmo/l");
    processLoinc("2160-0", true, "thous/cu.mm", "mg/coll", "qu", "meq/l", "gm/vol", "g/dl", "ma", "units", "gdl", "cells/mcl", "mmol/l", "percent", "not tested", "null",
        "ml/min/1.73m2", "gm/24 hr", "gm/24hrs", "%");
    processLoinc("2951-2", true, "mcg/dl", "iu/l", "percent", "mg/dl", "mm/l", "not tested", "/hpf", "seconds", " /hpf", "thous/cu.mm", "ml");
    processLoinc("2075-0", true, "percent", "mm/l", "not tested", "cells/mcl", "mg/dl", "%");
    processLoinc("2028-9", true, "/ul", "mm/l", "percent", "u/l", "uiu/ml", "%", "mg/dl");
    processLoinc("17861-6", true, "meq/l", "percent", "ng/dl", "pg/ml", "mg/ml", "not tested", "mmol/l");
    processLoinc("1751-7", true, "percent", "qu", "thou/ul", "not tested", "cells/cu.mm", "cells/mcl", " /hpf", "mcg t4/dl", "seconds", "mg/dl");
    processLoinc("1742-6", true, "zz", "percent", "meq/l", "cells/mcl", "fl", "%", "g/dl", "iu/ml");
    processLoinc("1920-8", true, "mg/dl", "fl", "meq/l", "not tested", "iu/ml", "intlunit/l", "cells/mcl", "thous/cu.mm", "ug/mg creatinine", "weeks", "mmol/l", "ug/d");

    labsValues.put("<", "BELOW ABSOLUTE LOW OFF INSTRUMENT SCALE");
    labsValues.put(">", "ABOVE ABSOLUTE HIGH OFF INSTRUMENT SCALE");
    labsValues.put("A", "ABNORMAL (APPLIES TO NON-NUMERIC RESULTS)");
    labsValues.put("AA", "VERY ABNORMAL (APPLIES TO NON-NUMERIC UNITS, ANALOGOUS TO PANIC LIMITS FOR NUMERIC UNITS)");
    labsValues.put("B", "BETTER USE WHEN DIRECTION NOT RELEVANT");
    labsValues.put("D", "SIGNIFICANT CHANGE DOWN");
    labsValues.put("H", "ABOVE HIGH NORMAL");
    labsValues.put("HH", "ABOVE UPPER PANIC LIMITS");
    labsValues.put("I", "INTERMEDIATE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("IN", "INDETERMINATE (ADDED FOR GENZYME)");
    labsValues.put("L", "BELOW LOW NORMAL");
    labsValues.put("LL", "BELOW LOWER PANIC LIMITS");
    labsValues.put("MS", "MODERATELY SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("N", "NORMAL (APPLIES TO NON-NUMERIC RESULTS)");
    labsValues.put("NG", "NEGATIVE (ADDED FOR GENZYME)");
    labsValues.put("OT", "OTHER (ADDED FOR GENZYME)");
    labsValues.put("PS", "POSITIVE (ADDED FOR GENZYME)");
    labsValues.put("R", "RESISTANT INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("S", "SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("SR", "SEE REPORT (ADDED FOR GENZYME)");
    labsValues.put("U", "SIGNIFICANT CHANGE UP");
    labsValues.put("UN", "UNKNOWN");
    labsValues.put("VS", "VERY SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("W", "WORSE USE WHEN DIRECTION NO RELEVANT");
    labsValues.put("VS", "VERY SUSCEPTIBLE INDICATES FOR MICROBIOLOGY SUSCEPTIBILITIES ONLY");
    labsValues.put("W", "WORSE USE WHEN DIRECTION NO RELEVANT");
  }

  public static HashMap<String, String> getLabsValues() {
    return labsValues;
  }

  public static HashMap<String, String> getDepartmentValues() {
    return departmentValues;
  }

  static {
    departmentValues.put("01", "PHARMACY");
    departmentValues.put("03", "SCHOOL");
    departmentValues.put("04", "HOMELESS SHELTER");
    departmentValues.put("11", "OFFICE");
    departmentValues.put("12", "HOME");
    departmentValues.put("13", "ASSISTED LIVING FACILITY");
    departmentValues.put("14", "GROUP HOME");
    departmentValues.put("15", "MOBILE UNIT");
    departmentValues.put("16", "TEMPORARY LODGING");
    departmentValues.put("17", "WALK-IN RETAIL HEALTH CLINIC");
    departmentValues.put("20", "URGENT CARE FACILITY");
    departmentValues.put("21", "INPATIENT HOSPITAL");
    departmentValues.put("22", "OUTPATIENT HOSPITAL");
    departmentValues.put("23", "EMERGENCY ROOM");
    departmentValues.put("24", "AMBULATORY SURGICAL CENTER");
    departmentValues.put("31", "SKILLED NURSING FACILITY");
    departmentValues.put("32", "NURSING FACILITY");
    departmentValues.put("33", "CUSTODIAL CARE FACILITY");
    departmentValues.put("34", "HOSPICE");
    departmentValues.put("41", "AMBULANCE - LAND");
    departmentValues.put("42", "AMBULANCE - AIR OR WATER");
    departmentValues.put("49", "INDEPENDENT CLINIC");
    departmentValues.put("50", "FEDERALLY QUALIFIED HEALTH CENTER");
    departmentValues.put("51", "INPATIENT PSYCHIATRIC FACILITY");
    departmentValues.put("52", "PSYCHIATRIC FACILITY");
    departmentValues.put("53", "COMMUNITY MENTAL HEALTH CENTER");
    departmentValues.put("54", "INTERMEDIATE CARE FACILITY FOR MENTALLY RETARDED");
    departmentValues.put("55", "RESIDENTIAL SUBSTANCE ABUSE TREATMENT FACILITY");
    departmentValues.put("56", "PSYCHIATRIC RESIDENTIAL TREATMENT FACILITY");
    departmentValues.put("57", "NON-RESIDENTIAL SUBSTANCE ABUSE TREATMENT FACILITY");
    departmentValues.put("60", "MASS IMMUNIZATION CENTER");
    departmentValues.put("61", "COMPREHENSIVE INPATIENT REHABILITATION FACILITY");
    departmentValues.put("62", "COMPREHENSIVE OUTPATIENT REHABILITATION FACILITY");
    departmentValues.put("65", "END-STAGE RENAL DISEASE TREATMENT FACILITY");
    departmentValues.put("71", "STATE OR LOCAL PUBLIC HEALTH CLINIC");
    departmentValues.put("81", "INDEPENDENT LABORATORY");
    departmentValues.put("99", "OTHER UNLISTED FACILITY");
  }

  private static int getPid(final String patID) {
    if (patIdToPid.containsKey(patID)) {
      return patIdToPid.get(patID);
    }
    return -1;
  }

  private static int getNextPid(final String patID) {
    if (patIdToPid.containsKey(patID)) {
      return patIdToPid.get(patID);
    }
    patIdToPid.put(patID, currentPid);
    return currentPid++;
  }

  private static void saveMaps(final File outputFolder) throws IOException {
    if (DEBUG_PID != null) {
      return;
    }
    System.out.println("SAVING MAPS...");
    final BufferedWriter pidMap = new BufferedWriter(new FileWriter(new File(outputFolder, "optumPidToPid.txt")));
    final ObjectKeyIntMapIterator iterator = patIdToPid.entries();
    final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    while (iterator.hasNext()) {
      iterator.next();
      final Date yob = pidToYearOfBirth.get(iterator.getValue());
      if (yob != null) {
        cal.setTime(yob);
        pidMap.write(iterator.getKey() + "\t" + iterator.getValue() + "\t" + cal.get(Calendar.YEAR) + "\n");
      }
    }
    pidMap.close();
    System.out.println("DONE");
  }

  private static void readMaps(final File outputFolder) throws IOException {
    if (patIdToPid.size() == 0 || pidToYearOfBirth.size() == 0) {
      System.out.println("READING MAPS...");
      final TextFileReader pidMap = new TextFileReader(new File(outputFolder, "optumPidToPid.txt"), Charset.forName("UTF-8"));
      patIdToPid.clear();
      pidToYearOfBirth.clear();
      final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      while (pidMap.hasNext()) {
        final String[] data = TextUtils.split(pidMap.next(), '\t');
        if (DEBUG_PID != null && !data[0].equals(DEBUG_PID)) {
          continue;
        }
        final int pid = Integer.parseInt(data[1]);
        patIdToPid.put(data[0], pid);
        cal.set(Integer.parseInt(data[2]), 0, 1);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        pidToYearOfBirth.put(pid, cal.getTime());
      }
      pidMap.close();
    }
  }

  private static HashMap<String, IntOpenHashSet> readNdcMap(final ConnectionSettings settings) throws SQLException {
    final HashMap<String, IntOpenHashSet> result = new HashMap<>();
    Database db = null;
    ResultSet set = null;
    try {
      db = Database.createInstance(settings);
      set = db.queryStreaming(NDC_TO_RX_NORM);
      while (set.next()) {
        IntOpenHashSet s = result.get(set.getString(1));
        if (s == null) {
          s = new IntOpenHashSet();
          result.put(set.getString(1), s);
        }
        s.add(set.getInt(2));
      }
    }
    finally {
      FileUtils.close(set);
      FileUtils.close(db);
    }
    return result;
  }

  private static IntKeyObjectMap<Date> extractDeaths(final ConnectionSettings settings) throws SQLException {
    final IntKeyObjectMap<Date> result = new IntKeyObjectMap<>();
    Database db = null;
    ResultSet set = null;
    try {
      db = Database.createInstance(settings);
      set = db.queryStreaming(DEATH);
      final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      while (set.next()) {
        final String ymDod = set.getString(2);
        cal.set(Integer.parseInt(ymDod.substring(0, 4)), Integer.parseInt(ymDod.substring(4)) - 1, 1);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        result.put(getNextPid(set.getString(1)), cal.getTime());
      }
    }
    finally {
      FileUtils.close(set);
      FileUtils.close(db);
    }
    return result;
  }

  public static Iterator<String> getTableNames(final String prefix, final int startYear) {
    final HashSet<String> set = new HashSet<>();
    for (int x = startYear; x < 2017; x++) {
      for (int y = 1; y < 5; y++) {
        set.add(prefix + String.valueOf(x) + "q" + String.valueOf(y));
      }
    }
    return set.iterator();
  }

  private static void extractProcedures(final File outputFolder, final ConnectionSettings settings) throws SQLException, IOException {
    final Iterator<String> names = getTableNames("fd", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      final Calendar cal = Calendar.getInstance();
      try {
        db = Database.createInstance(settings);
        set = db.queryStreaming(PROCEDURES + name + (DEBUG_PID == null ? "" : WHERE) + " ORDER BY PATID");
        final File output = new File(outputFolder, name);
        FileUtils.deleteWithException(output);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
        while (set.next()) {
          final int pid = getPid(set.getString(1));
          if (pid != -1) {
            final String proc = set.getString(2);
            if (proc != null && !proc.isEmpty()) {
              final Date dob = pidToYearOfBirth.get(pid);
              final Date currentDate = set.getDate(3);
              if (currentDate != null && dob != null) {
                final int offset = OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), set.getDate(3));
                out.writeInt(pid);
                out.writeUTF(proc);
                out.writeInt(offset);
                cal.setTime(currentDate);
                out.writeShort(cal.get(Calendar.YEAR));
              }
            }
          }
        }
        out.close();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  private static void extractMeds2(final File outputFolder, final ConnectionSettings settings, final HashMap<String, IntOpenHashSet> ndcToRx) throws SQLException, IOException {
    final Iterator<String> names = getTableNames("m", 2003);
    final Calendar cal = Calendar.getInstance();
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      try {
        db = Database.createInstance(settings);
        set = db.queryStreaming(MEDS2 + name + (DEBUG_PID == null ? "" : WHERE) + " ORDER BY PATID");
        final File output = new File(outputFolder, "m" + name);
        FileUtils.deleteWithException(output);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
        while (set.next()) {
          final int pid = getPid(set.getString(1));
          if (pid != -1) {
            final IntOpenHashSet s = ndcToRx.get(set.getString(2));
            if (s != null) {
              final Date dob = pidToYearOfBirth.get(pid);
              final Date currentDate = set.getDate(3);
              if (set.getDate(3) != null && dob != null && !set.wasNull()) {
                final int offset = OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), currentDate);
                final IntIterator ii = s.iterator();
                while (ii.hasNext()) {
                  out.writeInt(pid);
                  out.writeInt(ii.next());
                  out.writeInt(offset);
                  cal.setTime(currentDate);
                  out.writeShort(cal.get(Calendar.YEAR));
                }
              }
            }
          }
        }
        out.close();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  private static void extractMeds(final File outputFolder, final ConnectionSettings settings, final HashMap<String, IntOpenHashSet> ndcToRx) throws SQLException, IOException {
    final Iterator<String> names = getTableNames("r", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      try {
        db = Database.createInstance(settings);
        //SELECT PATID, NDC, Fill_Dt FROM optumv7_dod.
        set = db.queryStreaming(MEDS + name + (DEBUG_PID == null ? "" : WHERE) + " ORDER BY PATID");
        final File output = new File(outputFolder, name);
        FileUtils.deleteWithException(output);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
        final Calendar cal = Calendar.getInstance();
        while (set.next()) {
          final int pid = getPid(set.getString(1));
          if (pid != -1) {
            final IntOpenHashSet s = ndcToRx.get(set.getString(2));
            if (s != null) {
              final Date dob = pidToYearOfBirth.get(pid);
              final Date currentDate = set.getDate(3);
              if (currentDate != null && dob != null && !set.wasNull()) {
                final int offset = OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), currentDate);
                final IntIterator ii = s.iterator();
                while (ii.hasNext()) {
                  final int rx = ii.next();
                  out.writeInt(pid);
                  out.writeInt(rx);
                  out.writeInt(offset);
                  cal.setTime(currentDate);
                  out.writeShort(cal.get(Calendar.YEAR));
                }
              }
            }
          }
        }
        out.close();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  private static boolean recordTextValue(final String loincId) {
    return loincWithTextValues.contains(loincId);
  }

  private static boolean recordLab(final String loincId, final String unit) {
    return recordLoinc.contains(loincId) && (!loincToInvalidUoM.containsKey(loincId) || !loincToInvalidUoM.get(loincId).contains(unit.toLowerCase()));
  }

  private static void extractLabs(final File outputFolder, final ConnectionSettings settings) throws SQLException, IOException {
    final Iterator<String> names = getTableNames("lr", 2003);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      final Calendar cal = Calendar.getInstance();
      try {
        db = Database.createInstance(settings);
        set = db.queryStreaming(LABS + name + (DEBUG_PID == null ? "" : WHERE) + " ORDER BY PATID");
        //PATID, LOINC_CD, FST_DT, RSLT_TXT, RSLT_NBR, ABNL_CD, HI_NRML, LOW_NRML, RSLT_UNIT_NM
        final File output = new File(outputFolder, name);
        FileUtils.deleteWithException(output);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
        while (set.next()) {
          final int pid = getPid(set.getString(1));
          if (pid != -1) {
            final String loinc = set.getString(2);
            final String rsltUnitNm = set.getString(9);
            if (!recordLab(loinc, rsltUnitNm)) {
              final Date currentDate = set.getDate(3);
              final Date dob = pidToYearOfBirth.get(pid);
              if (currentDate != null && dob != null && !set.wasNull()) {
                out.writeInt(pid);
                out.writeUTF(loinc == null ? "" : loinc);
                final int offset = OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), currentDate);
                out.writeInt(offset);
                out.writeUTF(LabsRecord.UNDEFINED);
                out.writeDouble(Stride6DatabaseDownload.UNDEFINED);
                cal.setTime(currentDate);
                out.writeShort(cal.get(Calendar.YEAR));
              }
              continue;
            }
            if (loinc != null && !loinc.isEmpty()) {
              final Date dob = pidToYearOfBirth.get(pid);
              final Date currentDate = set.getDate(3);
              if (currentDate != null && dob != null && !set.wasNull()) {
                final int offset = OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), currentDate);
                String abnormal = set.getString(6);
                if (abnormal.isEmpty()) {
                  if (!set.getString(7).isEmpty() || !set.getString(8).isEmpty()) {
                    abnormal = LabsRecord.NORMAL;
                  }
                  else {
                    abnormal = LabsRecord.UNDEFINED;
                  }
                }
                try {
                  final String v = set.getString(4);
                  if (v == null || v.isEmpty() || set.wasNull()) {
                    out.writeInt(pid);
                    out.writeUTF(loinc);
                    out.writeInt(offset);
                    out.writeUTF(abnormal);
                    out.writeDouble(Stride6DatabaseDownload.UNDEFINED);
                    cal.setTime(currentDate);
                    out.writeShort(cal.get(Calendar.YEAR));
                  }
                  else {
                    final double val = Double.parseDouble(set.getString(4));
                    out.writeInt(pid);
                    out.writeUTF(loinc);
                    out.writeInt(offset);
                    out.writeUTF(abnormal);
                    out.writeDouble(val);
                    cal.setTime(currentDate);
                    out.writeShort(cal.get(Calendar.YEAR));
                  }
                }
                catch (final Exception e) {
                  if (recordTextValue(loinc)) {
                    out.writeInt(pid);
                    out.writeUTF(loinc);
                    out.writeInt(offset);
                    out.writeUTF(set.getString(4));
                    out.writeDouble(Stride6DatabaseDownload.UNDEFINED);
                    cal.setTime(currentDate);
                    out.writeShort(cal.get(Calendar.YEAR));
                  }
                  out.writeInt(pid);
                  out.writeUTF(loinc);
                  out.writeInt(offset);
                  out.writeUTF(abnormal);
                  out.writeDouble(Stride6DatabaseDownload.UNDEFINED);
                  cal.setTime(currentDate);
                  out.writeShort(cal.get(Calendar.YEAR));
                }
              }
            }
          }
        }
        out.close();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  private static void extractDemographics(final File outputFolder, final ConnectionSettings settings) throws SQLException, IOException {
    Database db = null;
    ResultSet set = null;
    final IntKeyObjectMap<Date> deaths = extractDeaths(settings);
    final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    String processedPid = "";
    try {
      db = Database.createInstance(settings);
      set = db.queryStreaming(DEMOGRAPHICS);
      final File output = new File(outputFolder, Stride6DatabaseDownload.DEMOGRAPHICS_FILE);
      FileUtils.deleteWithException(output);
      final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.DEMOGRAPHICS_STRUCTURE);
      while (set.next()) {
        final String pidStr = set.getString(1);
        if (pidStr.equals(processedPid)) {
          continue;
        }
        processedPid = pidStr;
        final int pid = getNextPid(pidStr);
        cal.set(set.getInt(3), 0, 1);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        pidToYearOfBirth.put(pid, cal.getTime());
        final Date death = deaths.get(pid);
        double deathOffset = 0;
        int deathYear = 0;
        if (death != null) {
          deathOffset = Common.minutesToDays(OhdsiDatabaseDownload.getOffset(death, cal.getTime()));
          cal.setTime(death);
          deathYear = cal.get(Calendar.YEAR);
        }
        writer.write(pid, //
            set.getString(2), //
            null, //
            null, //
            deathOffset, //
            deathYear);
        if (deathYear != 0) {
          System.out.println(pidStr + " / " + deathOffset + " / " + deathYear);
        }
      }
      writer.close();
    }
    finally {
      FileUtils.close(set);
      FileUtils.close(db);
      saveMaps(outputFolder);
    }
  }

  private static void extractVisits(final File outputFolder, final ConnectionSettings settings) throws SQLException, IOException {
    final Iterator<String> names = getTableNames("c", 2004);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      try {
        //SELECT PATID, ICD_FLAG, DIAG1, DIAG2, DIAG3, DIAG4, DIAG5, PROC1, PROC2, PROC3, PROC4, PROC5, ADMIT_DATE, DISCH_DATE, POS
        db = Database.createInstance(settings);
        set = db.queryStreaming(VISITS + name + (DEBUG_PID == null ? "" : WHERE) + " ORDER BY PATID");
        final File output = new File(outputFolder, name);
        FileUtils.deleteWithException(output);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
        final Calendar cal = Calendar.getInstance();
        while (set.next()) {
          final int pid = getPid(set.getString(1));
          if (pid != -1) {
            final String icdFlag = set.getString(2);
            final String pos = set.getString(15);
            final String department = pos;
            final Date admit = set.getDate(13);
            if (set.wasNull()) {
              continue;
            }
            Date discharge = set.getDate(14);
            if (set.wasNull()) {
              discharge = admit;
            }
            if (icdFlag != null && icdFlag.equals("9")) {
              final Date dob = pidToYearOfBirth.get(pid);
              if (dob != null) {
                out.writeInt(pid);
                out.writeInt(OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), admit));
                out.writeInt(OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), discharge));
                for (int x = 3; x < 8; x++) {
                  final String code = TruvenExtractor.convertIcd9Code(set.getString(x));
                  if (!set.wasNull() && !code.isEmpty() && !code.startsWith("000")) {
                    out.writeUTF(code);
                  }
                }
                for (int x = 8; x < 13; x++) {
                  String code = set.getString(x);
                  if (!code.startsWith("000")) {
                    code = TruvenExtractor.convertIcd9Code(code);
                    if (!set.wasNull() && !code.isEmpty()) {
                      out.writeUTF(code);
                    }
                  }
                }
                out.writeUTF("-1");
                out.writeUTF(department == null ? "OTHER" : department);
                cal.setTime(admit);
                out.writeShort(cal.get(Calendar.YEAR));
                out.writeByte(9);
              }
            }
            else if (icdFlag != null && icdFlag.equals("10")) {
              final Date dob = pidToYearOfBirth.get(pid);
              if (dob != null) {
                out.writeInt(pid);
                out.writeInt(OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), admit));
                out.writeInt(OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), discharge));
                for (int x = 3; x < 8; x++) {
                  final String code = TruvenExtractor.convertIcd10Code(set.getString(x));
                  if (!set.wasNull() && !code.isEmpty() && !code.startsWith("000")) {
                    out.writeUTF(code);
                  }
                }
                for (int x = 8; x < 13; x++) {
                  String code = set.getString(x);
                  if (!code.startsWith("000")) {
                    code = TruvenExtractor.convertIcd10Code(code);
                    if (!set.wasNull() && !code.isEmpty()) {
                      out.writeUTF(code);
                    }
                  }
                }
                out.writeUTF("-1");
                out.writeUTF(department == null ? "OTHER" : department);
                cal.setTime(admit);
                out.writeShort(cal.get(Calendar.YEAR));
                out.writeByte(10);
              }
            }
          }
        }
        out.close();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  private static void extractVisits2(final File outputFolder, final ConnectionSettings settings) throws SQLException, IOException {
    final Iterator<String> names = getTableNames("m", 2004);
    while (names.hasNext()) {
      final String name = names.next();
      Database db = null;
      ResultSet set = null;
      try {
        //SELECT PATID, ICD_FLAG, FST_DT, POS, PROC_CD, DIAG1, DIAG2,DIAG3,DIAG4,DIAG5,DIAG6,DIAG7,DIAG8,DIAG9,DIAG10,DIAG11,DIAG12,DIAG13,DIAG14,DIAG15,DIAG16,DIAG17,DIAG18,DIAG19,DIAG20,DIAG21,DIAG22,DIAG23,DIAG24,DIAG25, PROC1, PROC2,PROC3,PROC4,PROC5,PROC6,PROC7,PROC8,PROC9,PROC10,PROC11,PROC12,PROC13,PROC14,PROC15,PROC16,PROC17,PROC18,PROC19,PROC20,PROC21,PROC22,PROC23,PROC24,PROC25
        db = Database.createInstance(settings);
        set = db.queryStreaming(VISITS2 + name + (DEBUG_PID == null ? "" : WHERE) + " ORDER BY PATID");
        final File output = new File(outputFolder, name);
        FileUtils.deleteWithException(output);
        final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
        final Calendar cal = Calendar.getInstance();
        while (set.next()) {
          final int pid = getPid(set.getString(1));
          if (pid != -1) {
            final String icdFlag = set.getString(2);
            final String pos = set.getString(4);
            final String department = pos;
            final Date admit = set.getDate(3);
            if (set.wasNull()) {
              continue;
            }
            if (icdFlag != null && icdFlag.equals("9")) {
              final Date dob = pidToYearOfBirth.get(pid);
              if (dob != null) {
                out.writeInt(pid);
                out.writeUTF(set.getString(5).isEmpty() ? "" : set.getString(5));
                out.writeInt(OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), admit));
                for (int x = 6; x < 31; x++) {
                  final String code = TruvenExtractor.convertIcd9Code(set.getString(x));
                  if (!set.wasNull() && !code.isEmpty() && !code.startsWith("000")) {
                    out.writeUTF(code);
                  }
                }
                for (int x = 31; x < 56; x++) {
                  String code = set.getString(x);
                  if (code != null && !code.isEmpty() && !code.startsWith("000")) {
                    code = TruvenExtractor.convertIcd9Code(code);
                    if (!set.wasNull() && !code.isEmpty()) {
                      out.writeUTF(code);
                    }
                  }
                }
                out.writeUTF("-1");
                out.writeUTF(department == null ? "OTHER" : department);
                cal.setTime(admit);
                out.writeShort(cal.get(Calendar.YEAR));
                out.writeByte(9);
              }
            }
            else if (icdFlag != null && icdFlag.equals("10")) {
              final Date dob = pidToYearOfBirth.get(pid);
              if (dob != null) {
                out.writeInt(pid);
                out.writeUTF(set.getString(5).isEmpty() ? "" : set.getString(5));
                out.writeInt(OhdsiDatabaseDownload.getOffset(pidToYearOfBirth.get(pid), admit));
                for (int x = 6; x < 31; x++) {
                  final String code = TruvenExtractor.convertIcd10Code(set.getString(x));
                  if (!set.wasNull() && !code.isEmpty()) {
                    out.writeUTF(code);
                  }
                }
                for (int x = 31; x < 56; x++) {
                  String code = set.getString(x);
                  if (code != null && !code.isEmpty() && !code.startsWith("0000")) {
                    code = TruvenExtractor.convertIcd10Code(code);
                    if (!set.wasNull() && !code.isEmpty()) {
                      out.writeUTF(code);
                    }
                  }
                }
                out.writeUTF("-1");
                out.writeUTF(department == null ? "OTHER" : department);
                cal.setTime(admit);
                out.writeShort(cal.get(Calendar.YEAR));
                out.writeByte(10);
              }
            }
          }
        }
        out.close();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  static final ArrayList<File> getFiles(final File outputFolder, final String prefix) {
    final ArrayList<File> result = new ArrayList<>();
    final File[] files = outputFolder.listFiles();
    for (final File file : files) {
      final String name = file.getName();
      if (name.startsWith(prefix) && name.charAt(prefix.length()) == '2' && name.charAt(name.length() - 2) == 'q' && name.charAt(name.length() - 1) >= '1' && name.charAt(
          name.length() - 1) <= '4') {
        result.add(file);
      }
    }
    return result;
  }

  static int nextPid(final ArrayList<?> iterators) {
    int minPid = Integer.MAX_VALUE;
    for (int x = 0; x < iterators.size(); x++) {
      if (((SimpleIterator) iterators.get(x)).hasNext()) {
        if (((SimpleIterator) iterators.get(x)).getPid() < minPid) {
          minPid = ((SimpleIterator) iterators.get(x)).getPid();
        }
      }
    }
    return minPid;
  }

  private static synchronized void test(final File outputFolder) throws IOException, InterruptedException {
    try {
      final OptumVisitMIterator i = new OptumVisitMIterator("aaa", new FileInputStream(new File(outputFolder, "m2014q2")));
      final int prevPid = -1;
      while (i.hasNext()) {
        i.next();
        System.out.println(i.getPid());
        if (i.getPid() < prevPid) {
          System.out.println("Error " + i.getPid() + " / " + prevPid);
          break;
        }
      }
    }
    catch (final FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (final IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static void recordPatientData(final IndexCollection indices, final UmlsDictionary umls, final PatientBuilder patient, final int pid,
      final PatientRecord<VisitRecord> visits, final PatientRecord<MedRecord> meds, final PatientRecord<LabsRecord> labs) throws UnsupportedDataTypeException {
    patient.setId(pid);
    if (visits != null && visits.getPatientId() == pid) {
      Stride6Extractor.addIcd9HierarchyCodes(indices, visits, Stride6Extractor.generateIcd9Hierarchy(indices, umls, visits));
      Stride6Extractor.addIcd10HierarchyCodes(indices, visits, Stride6Extractor.generateIcd10Hierarchy(indices, umls, visits));
      patient.recordVisit(visits);
    }
    if (labs != null && labs.getPatientId() == pid) {
      patient.recordLabs(labs);
    }
    if (meds != null && meds.getPatientId() == pid) {
      patient.recordAtc(Stride6Extractor.generateAtcToRxNormMap(indices, umls, meds));
      patient.recordMeds(meds);
    }
  }

  private static PatientRecord<VisitRecord> copyVisit(final PatientRecord<VisitRecord> record) {
    final PatientRecord<VisitRecord> result = new PatientRecord<>(record.getPatientId());
    for (int x = 0; x < record.getRecords().size(); x++) {
      result.add(record.getRecords().get(x).getDeepCopy());
    }
    return result;
  }

  private static PatientRecord<LabsRecord> copyLabs(final PatientRecord<LabsRecord> record, final HashSet<String> uniqueLabNames) {
    final PatientRecord<LabsRecord> result = new PatientRecord<>(record.getPatientId());
    for (int x = 0; x < record.getRecords().size(); x++) {
      result.add(record.getRecords().get(x).getDeepCopy());
      uniqueLabNames.add(record.getRecords().get(x).getCode());
    }
    return result;
  }

  private static PatientRecord<MedRecord> copyMeds(final PatientRecord<MedRecord> record) {
    final PatientRecord<MedRecord> result = new PatientRecord<>(record.getPatientId());
    for (int x = 0; x < record.getRecords().size(); x++) {
      result.add(record.getRecords().get(x).getDeepCopy());
    }
    return result;
  }

  synchronized void extract(final File inputFolder, final File outputFolder, final int patCnt) throws IOException, InterruptedException, SQLException, ExecutionException {
    final ExecutorService executor = Executors.newFixedThreadPool(40);
    final Statistics statistics = new Statistics(new StatisticsBuilderCompressed(68000000));
    final IndexCollection indices = new IndexCollection(null);
    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(1000000000, 5, outputFolder);
    System.out.println("Starting extraction...");
    int processedCnt = 0;
    int emptyCnt = 0;
    final long startTime = System.currentTimeMillis();
    long reportTime = System.currentTimeMillis();
    long patientCnt = 0;
    int patientInReportCnt = 0;
    int maxEventSize = 0;
    final OptumDatabaseConnectors connectors = new OptumDatabaseConnectors(inputFolder);
    final LinkedList<Future<PatientBuilder>> futures = new LinkedList<>();
    final HashSet<String> uniqueLabNames = new HashSet<>();
    while (connectors.hasNext()) {
      connectors.next();
      final DemographicsRecord demo = connectors.getCurrentDemo().getDeepCopy();
      final int pid = demo.getPatientId();
      final PatientRecord<VisitRecord> visits = (connectors.getCurrentVisit() != null && connectors.getCurrentVisit().getPatientId() == pid) ? copyVisit(
          connectors.getCurrentVisit()) : null;
      final PatientRecord<LabsRecord> labs = (connectors.getCurrentLabs() != null && connectors.getCurrentLabs().getPatientId() == pid) ? copyLabs(connectors.getCurrentLabs(),
          uniqueLabNames) : null;
      final PatientRecord<MedRecord> meds = (connectors.getCurrentMeds() != null && connectors.getCurrentMeds().getPatientId() == pid) ? copyMeds(connectors.getCurrentMeds())
          : null;

      maxEventSize = Math.max(maxEventSize, (meds == null ? 0 : meds.getRecords().size()) + (visits == null ? 0 : visits.getRecords().size()) + (labs == null ? 0
          : labs.getRecords().size()));

      futures.add(executor.submit(new Callable<PatientBuilder>() {

        @Override
        public PatientBuilder call() throws Exception {
          final PatientBuilder patient = new PatientBuilder(indices);
          if (labs != null) {
            final Iterator<LabsRecord> i = labs.getRecords().iterator();
            while (i.hasNext()) {
              final LabsRecord record = i.next();
              if (record.getCode() == null || !loinc.containsKey(record.getCode()) || record.getCode().equals("2111-3") || record.getCode().equals("45194-8")) {
                record.setCode(null);
              }
              else {
                record.setTime(Common.daysToTime(Math.floor(Common.minutesToDays(record.getTime()))));
              }
            }
          }

          if (visits != null) {
            final Iterator<VisitRecord> i = visits.getRecords().iterator();
            while (i.hasNext()) {
              final VisitRecord record = i.next();
              if (record.getCode().startsWith("000.") || record.getCode().isEmpty() || record.getCode().equals("00000")) {
                record.setCode(null);
                record.setCodeId(-1);
              }
              else {
                record.setAge(Common.daysToTime(Math.floor(Common.minutesToDays(record.getAge()))));
                record.setDuration(Common.daysToTime(Math.floor(Common.minutesToDays(record.getDuration()))));
              }
            }
          }

          if (meds != null) {
            final Iterator<MedRecord> i = meds.getRecords().iterator();
            while (i.hasNext()) {
              final MedRecord record = i.next();
              record.setStart(Common.daysToTime(Math.floor(Common.minutesToDays(record.getStart()))));
              record.setEnd(Common.daysToTime(Math.floor(Common.minutesToDays(record.getEnd()))));
              record.setDuration(record.getEnd() - record.getStart());
            }
          }

          recordPatientData(indices, indices.getUmls(), patient, pid, visits, meds, labs);
          if (patient.isEmpty()) {
            indices.addEmptyDemographicRecord(patient.getId());
            return null;
          }
          else {
            indices.addDemographicRecord(demo);
            if (demo.getDeathInDays() != 0) {
              int deathYear = -1;
              if (demo.getDeathYear() > 0) {
                deathYear = demo.getDeathYear();
              }
              patient.recordTime(deathYear, Common.daysToTime(demo.getDeathInDays()), Common.daysToTime(demo.getDeathInDays()));
            }
            if (patient.getStartTime() < 0 || patient.getEndTime() < 0 || patient.getEndTime() > Stride6Extractor.MAX_END_TIME
                || patient.getStartTime() > Stride6Extractor.MAX_END_TIME) {
              patient.setStartEndTime(0, 0);
              return null;
            }
            if (labs != null && labs.getPatientId() == pid) {
              Stride6Extractor.recordLabsStatistics(indices, statistics, labs);
            }
            if (visits != null && visits.getPatientId() == pid) {
              Stride6Extractor.recordVisitStatistics(indices, statistics, indices.getUmls(), visits);
              Stride6Extractor.recordVisitTypeStats(indices, statistics, visits);
            }
            if (meds != null && meds.getPatientId() == pid) {
              Stride6Extractor.recordMedsStatistics(indices, statistics, indices.getUmls(), meds);
            }
          }
          patient.close();
          return patient;
        }
      }));

      if (futures.size() > 2000) {
        final Iterator<Future<PatientBuilder>> i = futures.iterator();
        while (i.hasNext()) {
          final Future<PatientBuilder> f = i.next();

          if (f.isDone()) {
            try {
              final PatientBuilder patient = f.get();
              if (patient != null) {
                statistics.addAge(Common.minutesToYears(patient.getStartTime()));
                statistics.recordPatientId(patient);
                patientCnt += generator.add(patient);
                if (Common.minutesToYears(patient.getEndTime() - patient.getStartTime()) > 60) {
                  System.out.println("TIMELINE >60 years for pid, start_time, end_time: " + patient.getId() + ", " + patient.getStartTime() + ", " + patient.getEndTime());
                }
                patientInReportCnt++;
              }
              else {
                emptyCnt++;
              }
            }
            catch (final Exception e) {
              System.out.println("Skipped patient...");
              e.printStackTrace();
            }
            processedCnt++;
            i.remove();
          }
        }
      }

      if (System.currentTimeMillis() - reportTime > 5000) {
        System.out.println("Futures             : " + futures.size());
        System.out.println("Memory used         : " + generator.getMemoryUsed());
        System.out.println("Max event size      : " + maxEventSize);
        System.out.println("Empty               : " + emptyCnt);
        if (patientInReportCnt != 0) {
          System.out.println("Average patient size: " + (patientCnt / patientInReportCnt));
        }
        System.out.println("Processed           : " + processedCnt + " patients, time per patient: " + new DecimalFormat("##.###").format((System.currentTimeMillis() - startTime)
            / (double) (processedCnt)) + " ms");
        reportTime = System.currentTimeMillis();
        patientInReportCnt = 0;
        maxEventSize = 0;
      }

      if (processedCnt > patCnt) {
        break;
      }
    }

    System.out.println("Waiting for shutdown...");

    System.out.println("SIZE = " + futures.size());
    executor.shutdown();

    System.out.println("SIZE = " + futures.size());
    executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.HOURS);

    System.out.println("SIZE = " + futures.size());

    while (futures.size() > 0) {
      final Iterator<Future<PatientBuilder>> i = futures.iterator();
      while (i.hasNext()) {
        final Future<PatientBuilder> f = i.next();

        if (f.isDone()) {
          final PatientBuilder patient = f.get();
          if (patient != null) {
            statistics.addAge(Common.minutesToYears(patient.getStartTime()));
            statistics.recordPatientId(patient);
            patientCnt += generator.add(patient);
            if (Common.minutesToYears(patient.getEndTime() - patient.getStartTime()) > 60) {
              System.out.println("TIMELINE >60 years for pid, start_time, end_time: " + patient.getId() + ", " + patient.getStartTime() + ", " + patient.getEndTime());
            }
            patientInReportCnt++;
          }
          else {
            emptyCnt++;
          }
          processedCnt++;
          i.remove();
        }
      }
    }

    final Iterator<String> i = uniqueLabNames.iterator();
    while (i.hasNext()) {
      final String code = i.next();
      if (loinc.get(code) != null) {
        indices.addLabsCommonName(code, loinc.get(code));
      }
    }

    System.out.println("Processed all patients...");

    System.out.println("Saving indices...");
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(outputFolder, Common.INDICES_FILE_NAME)));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();

    System.out.println("Saving statistics...");
    final BufferedOutputStream statisticsStream = new BufferedOutputStream(new FileOutputStream(new File(outputFolder, Common.STATISTICS_FILE_NAME)));
    statistics.save(new StreamOutput(statisticsStream));
    statisticsStream.close();

    System.out.println("Closing data generator...");
    generator.close();
    System.out.println("Generator closed");
    final BufferedWriter report = new BufferedWriter(new FileWriter(new File(outputFolder, "report.txt")));
    report.close();

    System.out.println("Processed patients: " + processedCnt);
    System.out.println("Unique ICD9 codes: " + statistics.getUniqueIcd9Counts());
    System.out.println("Unique CPT codes: " + statistics.getUniqueCptCounts());
    System.out.println("Unique RXNORM codes: " + statistics.getUniqueRxNormCounts());
    System.out.println("It took " + new DecimalFormat("##.###").format(((System.currentTimeMillis() - startTime) / (double) (1000 * 60))) + " minutes");
  }

  public static void execute(final String args, final File outputFolder, final ConnectionSettings settings, final int patCnt) throws SQLException, IOException,
      InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException, ExecutionException {
    if (args.indexOf('d') != -1) {
      extractDemographics(outputFolder, settings);
    }
    if (args.indexOf('p') != -1) {
      readMaps(outputFolder);
      extractProcedures(outputFolder, settings);
    }
    if (args.indexOf('m') != -1) {
      readMaps(outputFolder);
      final HashMap<String, IntOpenHashSet> ndcToRx = readNdcMap(settings);
      extractMeds(outputFolder, settings, ndcToRx);
    }
    if (args.indexOf('x') != -1) {
      readMaps(outputFolder);
      final HashMap<String, IntOpenHashSet> ndcToRx = readNdcMap(settings);
      extractMeds2(outputFolder, settings, ndcToRx);
    }
    if (args.indexOf('l') != -1) {
      readMaps(outputFolder);
      extractLabs(outputFolder, settings);
    }
    if (args.indexOf('v') != -1) {
      readMaps(outputFolder);
      extractVisits(outputFolder, settings);
    }
    if (args.indexOf('y') != -1) {
      readMaps(outputFolder);
      extractVisits2(outputFolder, settings);
    }
    if (args.indexOf('t') != -1) {
      test(outputFolder);
    }
    if (args.indexOf('e') != -1) {
      final OptumDownloader downloader = new OptumDownloader();
      final File out = new File(outputFolder, "result");
      out.mkdirs();
      downloader.extract(outputFolder, out, patCnt);
    }
  }

  public static void main(final String[] args) throws FileNotFoundException, SQLException, IOException, InterruptedException, ClassNotFoundException, InstantiationException,
      IllegalAccessException, ExecutionException {
    System.out.println("Usage: options output_folder connection_settings_file [EXTRACT PATIENTS COUNT]");
    System.out.println();
    System.out.println("options");
    System.out.println("d = demographics");
    System.out.println("m = prescriptions (r)");
    System.out.println("x = prescriptions (m)");
    System.out.println("l = labs");
    System.out.println("v = visits (c)");
    System.out.println("y = visits (m)");
    System.out.println("p = procedures");
    System.out.println("t = test");
    System.out.println("e = multithreaded extraction");
    System.out.println("DB_URL   jdbc:mysql://shahlab-db1.stanford.edu:3306");
    int patCnt = Integer.MAX_VALUE;
    if (args.length == 4) {
      patCnt = Integer.parseInt(args[3]);
    }
    if (DEBUG_PID != null) {
      Database.LOGGING_ENABLED = false;
    }
    execute(args[0], new File(args[1]), ConnectionSettings.createFromFile(new File(args[2])), patCnt);
    System.out.println("DONE");
  }
}