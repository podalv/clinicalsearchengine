package com.podalv.extractor.optum;

import java.io.IOException;

public interface SimpleIterator {

  public void next() throws IOException;

  public int getPid();

  public boolean hasNext();
}
