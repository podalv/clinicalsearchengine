package com.podalv.extractor.optum;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;

public class OptumLabsConnector {

  private final LinkedList<PatientRecord<LabsRecord>> queue     = new LinkedList<>();
  private ArrayList<File>                             files     = null;
  private final ArrayList<OptumLabsIterator>          iterators = new ArrayList<>();
  private int                                         nextPid;

  public OptumLabsConnector(final File outputFolder) throws FileNotFoundException, IOException {
    this.files = OptumDownloader.getFiles(outputFolder, "lr");
    for (final File file : files) {
      iterators.add(new OptumLabsIterator(new FileInputStream(file)));
    }
    read();
  }

  public int getPid() {
    if (queue.size() != 0) {
      return queue.getFirst().getPatientId();
    }
    return -1;
  }

  public PatientRecord<LabsRecord> get() {
    final PatientRecord<LabsRecord> result = queue.getFirst();
    queue.removeFirst();
    read();
    return result;
  }

  private void read() {
    nextPid = OptumDownloader.nextPid(iterators);
    try {
      if (nextPid < Integer.MAX_VALUE) {
        final PatientRecord<LabsRecord> record = new PatientRecord<>(nextPid);
        for (final OptumLabsIterator i : iterators) {
          if (i.getPid() == nextPid) {
            while (i.getPid() == nextPid) {
              String labValue = OptumDownloader.getLabsValues().get(i.getInterpretedValue());
              if (labValue == null || labValue.equals(LabsRecord.NORMAL)) {
                labValue = "N/A";
              }
              record.add(new LabsRecord(i.getLoinc(), i.getOffset(), i.getYear(), labValue, i.getValue()));
              if (!i.hasNext()) {
                break;
              }
              i.next();
            }
          }
        }
        queue.add(record);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
