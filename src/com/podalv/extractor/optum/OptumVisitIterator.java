package com.podalv.extractor.optum;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.podalv.utils.file.FileUtils;

/** Used for consolidation
 *  Reads a single record at a time
 *
 * @author podalv
 *
 */
public class OptumVisitIterator implements SimpleIterator {

  private DataInputStream         stream;
  private int                     pid         = -1;
  private String                  department  = null;
  private int                     offsetStart = -1;
  private int                     offsetEnd   = -1;
  private int                     year        = -1;
  private int                     icdFlag     = -1;
  private final ArrayList<String> codes       = new ArrayList<>();

  public OptumVisitIterator(final InputStream stream) throws IOException {
    this.stream = new DataInputStream(new BufferedInputStream(stream));
    next();
  }

  @Override
  public int getPid() {
    return pid;
  }

  public int getIcdFlag() {
    return icdFlag;
  }

  public ArrayList<String> getCodes() {
    return codes;
  }

  public int getOffsetStart() {
    return offsetStart;
  }

  public String getDepartment() {
    return department;
  }

  public int getOffsetEnd() {
    return offsetEnd;
  }

  public int getYear() {
    return year;
  }

  @Override
  public void next() throws IOException {
    try {
      codes.clear();
      final int prevPid = pid;
      pid = stream.readInt();
      if (prevPid > pid) {
        System.out.println("!!! " + prevPid + " / " + pid);
      }
      offsetStart = stream.readInt();
      offsetEnd = stream.readInt();
      while (true) {
        final String code = stream.readUTF();
        if (code.equals("-1")) {
          break;
        }
        codes.add(code);
      }
      department = stream.readUTF();
      year = stream.readShort();
      icdFlag = stream.readByte();
    }
    catch (final EOFException exception) {
      FileUtils.close(stream);
      stream = null;
    }
  }

  @Override
  public boolean hasNext() {
    return stream != null;
  }

}
