package com.podalv.extractor.optum;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;

public class OptumMedsConnector {

  private static final int                           MONTH     = 30 * 24 * 60;
  private final LinkedList<PatientRecord<MedRecord>> queue     = new LinkedList<>();
  private ArrayList<File>                            files     = null;
  private final ArrayList<OptumMedsIterator>         iterators = new ArrayList<>();
  private int                                        nextPid;

  public OptumMedsConnector(final File outputFolder) throws FileNotFoundException, IOException {
    this.files = OptumDownloader.getFiles(outputFolder, "mm");
    for (final File file : files) {
      iterators.add(new OptumMedsIterator(new FileInputStream(file)));
    }
    this.files = OptumDownloader.getFiles(outputFolder, "r");
    for (final File file : files) {
      iterators.add(new OptumMedsIterator(new FileInputStream(file)));
    }
    read();
  }

  public int getPid() {
    if (queue.size() != 0) {
      return queue.getFirst().getPatientId();
    }
    return -1;
  }

  public PatientRecord<MedRecord> get() {
    final PatientRecord<MedRecord> result = queue.getFirst();
    queue.removeFirst();
    read();
    return result;
  }

  private void read() {
    nextPid = OptumDownloader.nextPid(iterators);
    try {
      if (nextPid < Integer.MAX_VALUE) {
        final PatientRecord<MedRecord> record = new PatientRecord<>(nextPid);
        for (final OptumMedsIterator i : iterators) {
          if (i.getPid() == nextPid) {
            while (i.getPid() == nextPid) {
              record.add(new MedRecord(i.getOffset(), i.getOffset() + MONTH, i.getYear(), "N/A", i.getRx(), "N/A"));
              if (!i.hasNext()) {
                break;
              }
              i.next();
            }
          }
        }
        queue.add(record);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
