package com.podalv.extractor.optum;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.podalv.utils.file.FileUtils;

/** Used for consolidation
 *  Reads a single record at a time
 *
 * @author podalv
 *
 */
public class OptumVisitMIterator implements SimpleIterator {

  private DataInputStream         stream;
  private int                     pid        = -1;
  private String                  department = null;
  private String                  proc       = null;
  private int                     offset     = -1;
  private int                     year       = -1;
  private int                     icdFlag    = -1;
  private final ArrayList<String> codes      = new ArrayList<>();
  private final String            label;

  public OptumVisitMIterator(final String label, final InputStream stream) throws IOException {
    this.stream = new DataInputStream(new BufferedInputStream(stream));
    this.label = label;
    next();
  }

  @Override
  public int getPid() {
    return pid;
  }

  public String getLabel() {
    return label;
  }

  public ArrayList<String> getCodes() {
    return codes;
  }

  public String getProc() {
    return proc;
  }

  public int getOffset() {
    return offset;
  }

  public String getDepartment() {
    return department;
  }

  public int getIcdFlag() {
    return icdFlag;
  }

  public int getYear() {
    return year;
  }

  @Override
  public void next() throws IOException {
    try {
      codes.clear();
      final int prevPid = pid;
      pid = stream.readInt();
      if (prevPid > pid) {
        System.out.println("!!! " + prevPid + " / " + pid + " (" + label + ")");
      }
      proc = stream.readUTF();
      offset = stream.readInt();
      while (true) {
        final String code = stream.readUTF();
        if (code.equals("-1")) {
          break;
        }
        codes.add(code);
      }
      department = stream.readUTF();
      year = stream.readShort();
      icdFlag = stream.readByte();
    }
    catch (final EOFException exception) {
      FileUtils.close(stream);
      stream = null;
    }
    catch (final Exception e) {
      System.out.println(label);
      e.printStackTrace();
    }
  }

  @Override
  public boolean hasNext() {
    return stream != null;
  }

}
