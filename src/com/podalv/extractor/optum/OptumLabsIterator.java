package com.podalv.extractor.optum;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import com.podalv.utils.file.FileUtils;

/** Used for consolidation
 *  Reads a single record at a time
 *
 * @author podalv
 *
 */
public class OptumLabsIterator implements SimpleIterator {

  private DataInputStream stream;
  private int             pid      = -1;
  private String          loinc    = null;
  private int             offset   = -1;
  private String          abnormal = null;
  private int             year     = -1;
  private double          value    = -10000;

  public OptumLabsIterator(final InputStream stream) throws IOException {
    this.stream = new DataInputStream(new BufferedInputStream(stream));
    next();
  }

  @Override
  public int getPid() {
    return pid;
  }

  public String getLoinc() {
    return loinc;
  }

  public int getOffset() {
    return offset;
  }

  public String getInterpretedValue() {
    return abnormal;
  }

  public double getValue() {
    return value;
  }

  public int getYear() {
    return year;
  }

  @Override
  public void next() throws IOException {
    try {
      pid = stream.readInt();
      loinc = stream.readUTF();
      offset = stream.readInt();
      abnormal = stream.readUTF();
      value = stream.readDouble();
      year = stream.readShort();
    }
    catch (final EOFException exception) {
      FileUtils.close(stream);
      stream = null;
    }
  }

  @Override
  public boolean hasNext() {
    return stream != null;
  }

}
