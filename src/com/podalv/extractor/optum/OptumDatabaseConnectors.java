package com.podalv.extractor.optum;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.DepartmentRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.NoteRecord;
import com.podalv.extractor.datastructures.records.ObservationRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitDxRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.Commands;
import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;
import com.podalv.extractor.stride6.extractors.DemographicsExtractor;

public class OptumDatabaseConnectors implements DatabaseConnectorsInterface {

  private final OptumLabsConnector    labs;
  private final OptumVisitConnector   visits;
  private final OptumMedsConnector    meds;
  private final DemographicsExtractor demo;
  private DemographicsRecord          currentDemo;
  private PatientRecord<LabsRecord>   currentLabs;
  private PatientRecord<MedRecord>    currentMeds;
  private PatientRecord<VisitRecord>  currentVisits;

  public OptumDatabaseConnectors(final File folder) throws FileNotFoundException, IOException {
    labs = new OptumLabsConnector(folder);
    visits = new OptumVisitConnector(folder);
    meds = new OptumMedsConnector(folder);
    demo = new DemographicsExtractor(new BinaryFileExtractionSource(new File(folder, "demographics.bin"), new File(folder, "demographics.bin_dict"),
        Commands.DEMOGRAPHICS_STRUCTURE));
  }

  @Override
  public Extractor<NoteRecord> getNotesExtractor() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PatientRecord<MedRecord> getCurrentMeds() {
    return currentMeds;
  }

  @Override
  public boolean hasNext() {
    return demo.hasNext();
  }

  @Override
  public DemographicsRecord getCurrentDemo() {
    if (currentDemo.getGender().equals("M")) {
      currentDemo.setGender("MALE");
    }
    else if (currentDemo.getGender().equals("F")) {
      currentDemo.setGender("FEMALE");
    }
    else {
      currentDemo.setGender("OTHER");
    }
    return currentDemo;
  }

  @Override
  public PatientRecord<LabsRecord> getCurrentLabs() {
    return currentLabs;
  }

  @Override
  public PatientRecord<VitalsRecord> getCurrentVitals() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PatientRecord<DepartmentRecord> getCurrentDepartments() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PatientRecord<TermRecord> getCurrentTerms() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PatientRecord<VisitRecord> getCurrentVisit() {
    return currentVisits;
  }

  @Override
  public PatientRecord<ObservationRecord> getCurrentObservation() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void next() throws SQLException, IOException {
    final int prevId = currentDemo == null ? -1 : currentDemo.getPatientId();
    while (prevId == (currentDemo == null ? -1 : currentDemo.getPatientId())) {
      currentDemo = demo.next();
    }
    if (currentLabs == null || currentLabs.getPatientId() < currentDemo.getPatientId()) {
      if (labs.getPid() == -1) {
        currentLabs = null;
      }
      else {
        if (labs.getPid() < currentDemo.getPatientId()) {
          throw new RuntimeException("PIDs out of sync LAB=" + labs.getPid() + ", DEMO=" + currentDemo.getPatientId());
        }
        if (labs.getPid() == currentDemo.getPatientId()) {
          currentLabs = labs.get();
        }
      }
    }
    if (currentMeds == null || currentMeds.getPatientId() < currentDemo.getPatientId()) {
      if (meds.getPid() < 0) {
        currentMeds = null;
      }
      else if (meds.getPid() < currentDemo.getPatientId()) {
        throw new RuntimeException("PIDs out of sync MED=" + meds.getPid() + ", DEMO=" + currentDemo.getPatientId());
      }
      if (meds.getPid() == currentDemo.getPatientId()) {
        currentMeds = meds.get();
      }
    }
    if (currentVisits == null || currentVisits.getPatientId() < currentDemo.getPatientId()) {
      if (visits.getPid() < 0) {
        currentVisits = null;
      }
      else {
        if (visits.getPid() < currentDemo.getPatientId()) {
          throw new RuntimeException("PIDs out of sync VISIT=" + visits.getPid() + ", DEMO=" + currentDemo.getPatientId());
        }
        if (visits.getPid() == currentDemo.getPatientId()) {
          currentVisits = visits.get();
        }
      }
    }
  }

  @Override
  public Extractor<VisitDxRecord> getDxExtractor() {
    // TODO Auto-generated method stub
    return null;
  }

}
