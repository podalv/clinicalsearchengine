package com.podalv.extractor.umls;

/** A pair of two strings a code and text. Used in UMLS extractor
 *
 * @author podalv
 *
 */
public class CodeTextPair {

  private final String code;
  private final String text;

  public CodeTextPair(final String code, final String text) {
    this.code = code;
    this.text = text;
  }

  public String getCode() {
    return code;
  }

  public String getText() {
    return text;
  }

  @Override
  public String toString() {
    return code + "=" + text;
  }
}
