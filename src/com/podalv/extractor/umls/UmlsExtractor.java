package com.podalv.extractor.umls;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.maps.string.StringKeyObjectIterator;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.maps.string.StringKeyStringIterator;
import com.podalv.maps.string.StringKeyStringMap;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

/** Extracts the CODE -> STRING pairs from UMLS MRCONSO table
 *
 * @author podalv
 *
 */
public class UmlsExtractor {

  private static final UmlsExtractor                        INSTANCE                  = new UmlsExtractor();
  public static final String[]                              CPT_ONTOLOGY              = new String[] {"CPT", "HCPT", "HCPCS"};
  public static final String                                RXNORM_ONTOLOGY           = "RXNORM";
  private static final int                                  CODE_COLUMN_ID            = 0;
  private static final int                                  TEXT_COLUMN_ID            = 1;
  private static final int                                  ONTOLOGY_COLUMN_ID        = 2;
  public static final String                                MRCONSO_QUERY             = "SELECT * FROM umls2015ab.MRCONSO WHERE LAT=\"ENG\"";
  private final StringKeyObjectMap<ArrayList<CodeTextPair>> ontologyToCodeTextPairMap = new StringKeyObjectMap<>();

  private UmlsExtractor() {
    readData();
  }

  private void addData(final String[] row) {
    ArrayList<CodeTextPair> list = ontologyToCodeTextPairMap.get(row[ONTOLOGY_COLUMN_ID]);
    if (list == null) {
      list = new ArrayList<>();
      ontologyToCodeTextPairMap.put(row[ONTOLOGY_COLUMN_ID], list);
    }
    list.add(new CodeTextPair(row[CODE_COLUMN_ID], row[TEXT_COLUMN_ID]));
  }

  private void readData() {
    final ClassLoader classLoader = getClass().getClassLoader();
    final BufferedReader reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("umls.txt")));
    try {
      String line;
      while ((line = reader.readLine()) != null) {
        addData(TextUtils.split(line, '\t'));
      }
    }
    catch (final IOException exception) {
      exception.printStackTrace();
    }
    finally {
      FileUtils.close(reader);
    }
  }

  public static final ArrayList<CodeTextPair> get(final String ... ontology) {
    if (ontology.length == 1) {
      return INSTANCE.ontologyToCodeTextPairMap.get(ontology[0]);
    }
    else {
      final ArrayList<CodeTextPair> result = new ArrayList<>();
      for (final String element : ontology) {
        result.addAll(INSTANCE.ontologyToCodeTextPairMap.get(element));
      }
      if (result.size() != 0) {
        return result;
      }
    }
    return null;
  }

  public static void extract(final File file, final ConnectionSettings settings, final String ... sabToExtract) throws SQLException, IOException {
    final Database db = Database.createInstance(settings);
    final ResultSet set = db.queryStreaming(MRCONSO_QUERY);
    final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
    final StringKeyObjectMap<StringKeyStringMap> sabToCodeToStr = new StringKeyObjectMap<>();
    final HashSet<String> validSab = new HashSet<>();
    for (final String element : sabToExtract) {
      validSab.add(element.toUpperCase());
    }
    while (set.next()) {
      final String sab = set.getString(12).toUpperCase();
      final String code = set.getString(14);
      final String str = set.getString(15);
      if (validSab.size() == 0 || validSab.contains(sab)) {
        if (sabToCodeToStr.containsKey(sab) && sabToCodeToStr.get(sab).containsKey(code)) {
          if (set.getString(7).equals("Y")) {
            StringKeyStringMap m = sabToCodeToStr.get(sab);
            if (m == null) {
              m = new StringKeyStringMap();
              sabToCodeToStr.put(sab, m);
            }
            m.put(code, str + "\t" + sab);
          }
        }
        else {
          StringKeyStringMap m = sabToCodeToStr.get(sab);
          if (m == null) {
            m = new StringKeyStringMap();
            sabToCodeToStr.put(sab, m);
          }
          m.put(code, str + "\t" + sab);
        }
      }
    }
    final StringKeyObjectIterator<StringKeyStringMap> iterator = sabToCodeToStr.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final StringKeyStringIterator i = iterator.getValue().entries();
      while (i.hasNext()) {
        i.next();
        writer.write(i.getKey() + "\t" + i.getValue() + "\n");
      }
    }
    set.close();
    db.close();
    writer.close();
  }

  public static void extractCuiString(final File file, final ConnectionSettings settings) throws SQLException, IOException {
    final Database db = Database.createInstance(settings);
    final ResultSet set = db.queryStreaming(MRCONSO_QUERY);
    final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
    final StringKeyStringMap codeToStr = new StringKeyStringMap();
    while (set.next()) {
      final String cui = set.getString(1);
      final String str = set.getString(15);
      if (codeToStr.containsKey(cui)) {
        if (set.getString(7).equals("Y")) {
          codeToStr.put(cui, str);
        }
      }
      else {
        codeToStr.put(cui, str);
      }
    }
    final StringKeyStringIterator iterator = codeToStr.entries();
    while (iterator.hasNext()) {
      iterator.next();
      writer.write(iterator.getKey() + "\t" + iterator.getValue() + "\n");
    }
    set.close();
    db.close();
    writer.close();
  }

  public static void main(final String[] args) throws SQLException, IOException {
    System.out.println("Usage: output file connection_settings_file");
    final ConnectionSettings settings = ConnectionSettings.createFromFile(new File(args[1]));
    extract(new File(new File(args[0]), "umls.txt"), settings, "RXNORM", "HCPT", "CPT", "ICD9CM");
    extractCuiString(new File(new File(args[0]), "cui.txt"), settings);
  }
}
