package com.podalv.extractor.hierarchies.atc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.input.Input;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.search.index.IndexCollection;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

/** Encapsulates the ATC hierarchy from OHDSI. This exists because UMLS hierarchy had an error
 *
 * @author podalv
 *
 */
public class AtcHierarchy implements PersistentObject {

  private static final AtcHierarchy             INSTANCE       = new AtcHierarchy();
  private HashMap<String, String>               codeToText     = new HashMap<String, String>();
  private HashMap<String, String[]>             childToParents = new HashMap<String, String[]>();
  private final IntKeyObjectMap<IntOpenHashSet> codeToParentId = new IntKeyObjectMap<IntOpenHashSet>();

  private AtcHierarchy() {
    BufferedReader reader = null;
    final ClassLoader classLoader = getClass().getClassLoader();
    try {
      reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("atc.dict")));
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        codeToText.put(data[0].toUpperCase(), data[1]);
      }
      reader.close();

      reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("atc.hier")));
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        childToParents.put(data[0], Arrays.copyOfRange(data, 1, data.length));
      }
    }
    catch (final IOException e) {
      codeToText = null;
      childToParents = null;
    }
    finally {
      FileUtils.close(reader);
    }
  }

  public Iterator<String> getCodes() {
    return codeToText.keySet().iterator();
  }

  public String getText(final String code) {
    return codeToText.get(code.toUpperCase());
  }

  public String[] getHierarchy(final String code) {
    return childToParents.get(code);
  }

  private void initIds(final IndexCollection indices) {
    if (codeToParentId.size() == 0) {
      synchronized (codeToParentId) {
        if (codeToParentId.size() == 0) {
          final Iterator<Entry<String, String[]>> i = childToParents.entrySet().iterator();
          while (i.hasNext()) {
            final Entry<String, String[]> entry = i.next();
            final IntOpenHashSet s = new IntOpenHashSet();
            for (final String ss : entry.getValue()) {
              s.add(indices.addAtcCode(ss));
            }
            codeToParentId.put(indices.addAtcCode(entry.getKey()), s);
          }
        }
      }
    }
  }

  public IntOpenHashSet getHierarchy(final IndexCollection indices, final int codeId) {
    initIds(indices);
    final IntOpenHashSet set = codeToParentId.get(codeId);
    return set == null ? new IntOpenHashSet() : set;
  }

  public static AtcHierarchy create() {
    return INSTANCE;
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void save(final Output output) throws IOException {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }

}