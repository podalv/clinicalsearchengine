package com.podalv.extractor.hierarchies;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.podalv.utils.text.TextUtils;

/** Allows overriding existing code and adding new codes with the ones specified here
 *
 * @author podalv
 *
 */
public class CustomCodeDictionary {

  private final HashMap<String, HashMap<String, String>> codeTypeToCodeToName = new HashMap<String, HashMap<String, String>>();

  private CustomCodeDictionary() {
    // static access only
  }

  public static CustomCodeDictionary createEmpty() {
    return new CustomCodeDictionary();
  }

  public synchronized void addCode(final String codeType, final String code, final String text) {
    HashMap<String, String> codeToName = codeTypeToCodeToName.get(codeType.toUpperCase());
    if (codeToName == null) {
      codeToName = new HashMap<String, String>();
      codeTypeToCodeToName.put(codeType.toUpperCase(), codeToName);
    }
    codeToName.put(code.toUpperCase(), text);
  }

  public static CustomCodeDictionary load(final InputStream stream) throws IOException {
    final CustomCodeDictionary result = new CustomCodeDictionary();
    final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    String line;
    while ((line = reader.readLine()) != null) {
      if (!line.trim().isEmpty()) {
        final String[] data = TextUtils.split(line, '\t');
        result.addCode(data[0], data[1], data[2]);
      }
    }
    reader.close();
    return result;
  }

}
