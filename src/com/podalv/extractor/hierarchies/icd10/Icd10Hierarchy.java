package com.podalv.extractor.hierarchies.icd10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.index.IndexCollection;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

public class Icd10Hierarchy {

  private static final Icd10Hierarchy   INSTANCE            = new Icd10Hierarchy();
  private HashMap<String, String>       codeToText          = new HashMap<>();
  private HashMap<String, String[]>     childToParents      = new HashMap<>();
  private final HashMap<Integer, int[]> childIdToParentsIds = new HashMap<>();
  private final HashSet<String>         topLevelParents     = new HashSet<>();

  private Icd10Hierarchy() {
    final ClassLoader classLoader = getClass().getClassLoader();
    BufferedReader reader = null;
    try {
      reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("icd10.dict")));
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        codeToText.put(Common.filterString(data[0]).toUpperCase(), Common.filterString(data[1]));
      }
      reader.close();

      reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("icd10.hier")));
      while ((line = reader.readLine()) != null) {
        final String[] data = Common.filterString(TextUtils.split(line.trim(), '\t'));
        final String child = Common.filterString(data[0]).toUpperCase();
        childToParents.put(child, Arrays.copyOfRange(data, 1, data.length));
      }
      calculateTopLevelParents();
    }
    catch (final IOException e) {
      childToParents = null;
      codeToText = null;
    }
    finally {
      FileUtils.close(reader);
    }
  }

  public Iterator<String> getCodes() {
    return codeToText.keySet().iterator();
  }

  public HashSet<String> getParents() {
    final HashSet<String> result = new HashSet<>();
    final Iterator<Entry<String, String[]>> i = childToParents.entrySet().iterator();
    while (i.hasNext()) {
      final Entry<String, String[]> entry = i.next();
      for (final String v : entry.getValue()) {
        result.add(v);
      }
    }
    return result;
  }

  public HashSet<String> getTopLevelParents() {
    return topLevelParents;
  }

  private void calculateTopLevelParents() {
    final Iterator<String> i = codeToText.keySet().iterator();
    while (i.hasNext()) {
      final String code = i.next();
      if (code.indexOf('-') != -1) {
        topLevelParents.add(code);
      }
    }
  }

  public void set(final String icd10Code, final String icd10Text) {
    codeToText.put(icd10Code, icd10Text);
  }

  public String getText(final String code) {
    return codeToText.get(code.toUpperCase());
  }

  public String[] getHierarchy(final String code) {
    return childToParents.get(code);
  }

  public int[] getHierarchy(final int codeId, final IndexCollection indices) {
    if (childIdToParentsIds.size() == 0) {
      calculateIds(indices);
    }
    return childIdToParentsIds.get(codeId);
  }

  public void calculateIds(final IndexCollection indices) {
    final Iterator<Entry<String, String[]>> i = childToParents.entrySet().iterator();
    while (i.hasNext()) {
      final Entry<String, String[]> entry = i.next();
      final IntArrayList ii = new IntArrayList();
      for (int x = 0; x < entry.getValue().length; x++) {
        if (!entry.getValue()[x].isEmpty()) {
          ii.add(indices.addIcd10Code(Common.filterString(entry.getValue()[x])));
        }
      }
      childIdToParentsIds.put(indices.addIcd10Code(Common.filterString(entry.getKey())), ii.toArray());
    }
  }

  public static Icd10Hierarchy create() {
    return INSTANCE;
  }

}