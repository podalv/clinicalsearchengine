package com.podalv.extractor.stride6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

public class SnomedDictionary {

  private final HashMap<String, String> snomedCodeToText = new HashMap<>();
  private final HashMap<String, String> snomedTextToCode = new HashMap<>();
  private static final AtomicInteger    snomedId         = new AtomicInteger(0);

  public SnomedDictionary(final int startCode) {
    snomedId.set(startCode);
  }

  public boolean containsCode(final String code) {
    return code == null ? false : snomedCodeToText.containsKey(code);
  }

  public String add(final String text) {
    if (!snomedCodeToText.containsValue(text)) {
      final int val = snomedId.incrementAndGet();
      put(val + "", text);
      return val + "";
    }
    return snomedTextToCode.get(text);
  }

  public void put(final String code, final String text) {
    snomedCodeToText.put(code, text);
    snomedTextToCode.put(text, code);
  }

  public void writeSnomedDictionary(final File file) throws IOException {
    final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
    final Iterator<Entry<String, String>> iterator = snomedCodeToText.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, String> entry = iterator.next();
      writer.write(entry.getKey() + "\t" + entry.getValue() + "\n");
    }
    writer.close();
  }

}
