package com.podalv.extractor.stride6;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;

/** Contains all the SQL commands used to query the Stride6
 *
 *
 * @author podalv
 *
 */
public class Commands {

  public static final DATA_TYPE[] DEMOGRAPHICS_STRUCTURE            = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.STRING, DATA_TYPE.STRING, DATA_TYPE.STRING, DATA_TYPE.DOUBLE,
      DATA_TYPE.SHORT};
  public static final DATA_TYPE[] OBSERVATION_STRUCTURE             = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.STRING, DATA_TYPE.DOUBLE, DATA_TYPE.SHORT, DATA_TYPE.STRING};
  public static final DATA_TYPE[] VITALS_STRUCTURE                  = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.STRING, DATA_TYPE.DOUBLE,
      DATA_TYPE.SHORT};
  public static final DATA_TYPE[] VITALS_VISITS_STRUCTURE           = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.SHORT, DATA_TYPE.SHORT, DATA_TYPE.DOUBLE,
      DATA_TYPE.SHORT, DATA_TYPE.DOUBLE, DATA_TYPE.STRING, DATA_TYPE.SHORT, DATA_TYPE.DOUBLE, DATA_TYPE.DOUBLE};
  public static final DATA_TYPE[] TERMS_STRUCTURE                   = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.INT, DATA_TYPE.INT, DATA_TYPE.BYTE};
  public static final DATA_TYPE[] VISITS_STRUCTURE                  = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.INT, DATA_TYPE.SHORT, DATA_TYPE.DOUBLE, DATA_TYPE.STRING,
      DATA_TYPE.DOUBLE, DATA_TYPE.STRING, DATA_TYPE.STRING, DATA_TYPE.STRING};
  public static final DATA_TYPE[] MEDS_STRUCTURE                    = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.SHORT, DATA_TYPE.STRING, DATA_TYPE.INT,
      DATA_TYPE.DOUBLE, DATA_TYPE.STRING};
  public static final DATA_TYPE[] DEPARTMENT_STRUCTURE              = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.SHORT, DATA_TYPE.STRING};
  public static final DATA_TYPE[] LABS_SHC_STRUCTURE                = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.STRING, DATA_TYPE.DOUBLE, DATA_TYPE.DOUBLE,
      DATA_TYPE.DOUBLE, DATA_TYPE.STRING};
  public static final DATA_TYPE[] LABS_LPCH_STRUCTURE               = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.STRING, DATA_TYPE.DOUBLE, DATA_TYPE.STRING};
  public static final DATA_TYPE[] LABS_COMPONENT_STRUCTURE          = new DATA_TYPE[] {DATA_TYPE.STRING, DATA_TYPE.STRING};
  public static final DATA_TYPE[] LABS_EXTENDED_STRUCTURE           = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.DOUBLE, DATA_TYPE.SHORT, DATA_TYPE.STRING, DATA_TYPE.STRING,
      DATA_TYPE.DOUBLE};
  public static final DATA_TYPE[] VISIT_DX_STRUCTURE                = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.INT, DATA_TYPE.STRING};
  public static final DATA_TYPE[] NOTES_STRUCTURE                   = new DATA_TYPE[] {DATA_TYPE.INT, DATA_TYPE.STRING, DATA_TYPE.DOUBLE, DATA_TYPE.SHORT};

  public static final int         LABS_COMPONENT_BASE_NAME_COLUMN   = 1;
  public static final int         LABS_COMPONENT_COMMON_NAME_COLUMN = 2;

  public static final int         STRIDE_6_PATIENT_CNT              = 2011221;

  public static final int         MAX_TERM_MENTIONS_INCREMENT       = 3000;

  public static final String      STRIDE_6_TERM_PATIENT_ID          = "patient_id";
  private static final String     WHERE_STATEMENT                   = "$WHERE$";
  public static final String      TERMINOLOGY_STATEMENT             = "$TERMINOLOGY$";
  public static final String      DEMOGRAPHICS                      = "SELECT patient_id, gender, race, ethnicity, age_at_death_in_days, -1 FROM stride6.demographics ORDER BY patient_id";
  public static final String      VISITS                            = "SELECT visit_id, patient_id, visit_year, age_at_visit_in_days, src_visit, duration, code, sab, SOURCE_CODE FROM stride6.visit_master UNION SELECT visit_id, patient_id, visit_year, age_at_visit_in_days, src_visit, duration, code, sab, SOURCE_CODE FROM stride6.visit_master_billing order by patient_id";
  public static final String      PATIENT_COUNT                     = "SELECT MAX(patient_id) FROM stride6.demographics";
  public static final String      TERMS                             = "SELECT " + STRIDE_6_TERM_PATIENT_ID
      + ", stride6.term_mentions.* FROM stride6.notes JOIN (stride6.term_mentions) ON (stride6.term_mentions.nid = stride6.notes.note_id) $WHERE$ order by "
      + STRIDE_6_TERM_PATIENT_ID;
  public static final String      TERM_DICTIONARY                   = "SELECT tid, str FROM " + Commands.TERMINOLOGY_STATEMENT + ".str2tid group by tid";
  public static final String      TERM_DICTIONARY_TABLE             = "SELECT terminology FROM stride6._metadata";
  public static final String      MEDS                              = "SELECT stride6.pharmacy_order.patient_id, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.start_time_year, stride6.pharmacy_order.order_status, ingredient.rxcui, stride6.pharmacy_order.age_at_start_time_in_days, stride6.pharmacy_order.route FROM stride6.pharmacy_order JOIN (stride6.ingredient) ON (stride6.pharmacy_order.ingr_set_id = stride6.ingredient.ingr_set_id) order by patient_id";
  public static final String      VITALS                            = "SELECT patient_id, flo_meas_id, age_at_vital_entered_in_days, detail_display, meas_value, -1 FROM stride6.vitals order by patient_id";
  public static final String      VITALS_VISITS                     = "SELECT patient_id, age_at_contact_in_days, bp_systolic, bp_diastolic, temperature, pulse, weight_in_lbs, height, respirations, bmi, bsa FROM stride6.visit WHERE bp_systolic <> 0 OR bp_diastolic <> 0 OR temperature <> 0 OR pulse <> 0 OR weight_in_lbs <> 0 OR height <> \"\" OR respirations <> 0 OR bmi <> 0 OR bsa <> 0 order by patient_id";
  public static final String      LABS_SHC                          = "SELECT patient_id, age_at_taken_time_in_days, base_name, ord_num_value, reference_low, reference_high, ref_normal_vals FROM stride6.lab_results_shc order by patient_id";
  public static final String      LABS_LPCH                         = "SELECT patient_id, age_at_effective_time_in_days, component_code, labvalue, reference_range FROM stride6.lab_results_lpch order by patient_id";
  public static final String      LABS_SHC_TEST                     = "SELECT base_name, reference_low, reference_high, reference_unit FROM stride6.lab_results_shc";
  public static final String      LABS_LPCH_TEST                    = "SELECT component_code, reference_range, units FROM stride6.lab_results_lpch";
  public static final String      TERM_TO_CONCEPT_MAPS              = "SELECT * FROM term_concept_map.term_to_concept_values JOIN (term_concept_map.term_to_concept_maps) ON (term_concept_map.term_to_concept_maps.mapid = term_concept_map.term_to_concept_values.mapid) WHERE experimental = \"N\"";
  public static final String      LABS_COMPONENT                    = "SELECT base_name, common_name FROM stride6.component WHERE base_name <> \"\" GROUP by base_name";
  public static final String      VISIT_DX                          = "SELECT visit_id, dx_id, primary_dx_yn FROM stride6.visit_dx";
  public static final String      NOTES                             = "SELECT note_id, doc_description, age_at_note_date_in_days, note_year FROM stride6.notes";

  private Commands() {
    // Static access of variables only
  }

  public static String getTerminologyQuery(final String query, final String terminologyVersion) {
    return query.replace(TERMINOLOGY_STATEMENT, terminologyVersion);
  }

  public static String getTermsQuery(final String query, final int currentPatient, final String codeName) {
    return query.replace(WHERE_STATEMENT, " WHERE " + codeName + " >= " + currentPatient + " AND " + codeName + " <= " + (currentPatient + Commands.MAX_TERM_MENTIONS_INCREMENT));
  }

}
