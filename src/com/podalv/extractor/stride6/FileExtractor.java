package com.podalv.extractor.stride6;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/** Encapsulates a files that can be used for extraction as Demographics, ICD9, CPT...
 *
 * @author podalv
 *
 */
public class FileExtractor {

  private InputStream demographics;
  private InputStream demographics_dict;
  private InputStream icd9;
  private InputStream icd9_dict;
  private InputStream cpt;
  private InputStream cpt_dict;
  private InputStream observation;
  private InputStream observation_dict;
  private InputStream visits;
  private InputStream visits_dict;
  private InputStream terms;
  private InputStream terms_dict;
  private InputStream department;
  private InputStream department_dict;
  private InputStream meds        = null;
  private InputStream meds_dict   = null;
  private InputStream vitals      = null;
  private InputStream vitals_dict = null;
  private InputStream vitalsVisits;
  private InputStream vitalsVisits_dict;
  private InputStream visitDx;
  private InputStream visitDx_dict;
  private InputStream notes;
  private InputStream notes_dict;
  private InputStream shcLabs;
  private InputStream shcLabs_dict;
  private InputStream lpchLabs;
  private InputStream lpchLabs_dict;
  private InputStream extendedLabs;
  private InputStream extendedLabs_dict;
  private final int   patientCnt;

  FileExtractor(final int patientCnt) {
    this.patientCnt = patientCnt;
  }

  public int getPatientCnt() {
    return patientCnt;
  }

  public InputStream getDemographics_dict() {
    return demographics_dict;
  }

  public InputStream getExtendedLabs_dict() {
    return extendedLabs_dict;
  }

  public InputStream getCpt_dict() {
    return cpt_dict;
  }

  public InputStream getIcd9_dict() {
    return icd9_dict;
  }

  public InputStream getLpchLabs_dict() {
    return lpchLabs_dict;
  }

  public InputStream getMeds_dict() {
    return meds_dict;
  }

  public InputStream getNotes_dict() {
    return notes_dict;
  }

  public InputStream getShcLabs_dict() {
    return shcLabs_dict;
  }

  public InputStream getObservation() {
    return observation;
  }

  public InputStream getObservation_dict() {
    return observation_dict;
  }

  public InputStream getTerms_dict() {
    return terms_dict;
  }

  public InputStream getDepartment() {
    return department;
  }

  public InputStream getDepartment_dict() {
    return department_dict;
  }

  public InputStream getVisitDx_dict() {
    return visitDx_dict;
  }

  public InputStream getVisits_dict() {
    return visits_dict;
  }

  public InputStream getVitals_dict() {
    return vitals_dict;
  }

  public InputStream getVitalsVisits_dict() {
    return vitalsVisits_dict;
  }

  public void setDemographics(final File file) throws IOException {
    if (file.exists()) {
      this.demographics = new BufferedInputStream(new FileInputStream(file));
      this.demographics_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setDepartment(final File file) throws IOException {
    if (file.exists()) {
      this.department = new BufferedInputStream(new FileInputStream(file));
      this.department_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public InputStream getDemographics() {
    return this.demographics;
  }

  public void setIcd9(final File file) throws IOException {
    if (file.exists()) {
      this.icd9 = new BufferedInputStream(new FileInputStream(file));
      this.icd9_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setObservation(final File file) throws IOException {
    if (file.exists()) {
      this.observation = new BufferedInputStream(new FileInputStream(file));
      this.observation_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public InputStream getIcd9() {
    return this.icd9;
  }

  public InputStream getVitals() {
    return vitals;
  }

  public InputStream getVisitDx() {
    return visitDx;
  }

  public InputStream getLpchLabs() {
    return lpchLabs;
  }

  public InputStream getExtendedLabs() {
    return extendedLabs;
  }

  public InputStream getNotes() {
    return notes;
  }

  public InputStream getShcLabs() {
    return shcLabs;
  }

  public void setCpt(final File file) throws IOException {
    if (file.exists()) {
      this.cpt = new BufferedInputStream(new FileInputStream(file));
      this.cpt_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setMeds(final File file) throws IOException {
    if (file.exists()) {
      this.meds = new BufferedInputStream(new FileInputStream(file));
      this.meds_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setVitals(final File file) throws IOException {
    if (file.exists()) {
      this.vitals = new BufferedInputStream(new FileInputStream(file));
      this.vitals_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setExtendedLabs(final File file) throws IOException {
    if (file.exists()) {
      this.extendedLabs = new BufferedInputStream(new FileInputStream(file));
      this.extendedLabs_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setVitalsVisits(final File file) throws IOException {
    if (file.exists()) {
      this.vitalsVisits = new BufferedInputStream(new FileInputStream(file));
      this.vitalsVisits_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setLpchLabs(final File file) throws IOException {
    if (file.exists()) {
      this.lpchLabs = new BufferedInputStream(new FileInputStream(file));
      this.lpchLabs_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setShcLabs(final File file) throws IOException {
    if (file.exists()) {
      this.shcLabs = new BufferedInputStream(new FileInputStream(file));
      this.shcLabs_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setVisitDx(final File file) throws IOException {
    if (file.exists()) {
      this.visitDx = new BufferedInputStream(new FileInputStream(file));
      this.visitDx_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public void setNotes(final File file) throws IOException {
    if (file.exists()) {
      this.notes = new BufferedInputStream(new FileInputStream(file));
      this.notes_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public InputStream getCpt() {
    return this.cpt;
  }

  public InputStream getVitalsVisits() {
    return vitalsVisits;
  }

  public void setMeds(final InputStream meds) {
    this.meds = meds;
  }

  public InputStream getMeds() {
    return meds;
  }

  public void setVisits(final File file) throws IOException {
    if (file.exists()) {
      this.visits = new BufferedInputStream(new FileInputStream(file));
      this.visits_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public InputStream getVisits() {
    return this.visits;
  }

  public void setTerms(final File file) throws IOException {
    if (file.exists()) {
      this.terms = new BufferedInputStream(new FileInputStream(file));
      this.terms_dict = new BufferedInputStream(new FileInputStream(Common.getDictFile(file)));
    }
  }

  public InputStream getTerms() {
    return this.terms;
  }

}