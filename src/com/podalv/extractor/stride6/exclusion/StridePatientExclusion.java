package com.podalv.extractor.stride6.exclusion;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;

/** Excludes patients with negative ages, invalid years, etc.
 *
 * @author podalv
 *
 */
public class StridePatientExclusion implements PatientExclusion {

  @Override
  public boolean exclude(final DatabaseConnectorsInterface connectors) {
    final int pid = connectors.getCurrentDemo().getPatientId();
    int maxAge = 0;
    int minYear = Integer.MAX_VALUE;
    int maxYear = 0;
    boolean isEmpty = true;
    if (connectors.getCurrentDepartments() != null && connectors.getCurrentDepartments().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentDepartments().getRecords().size(); x++) {
        if ((connectors.getCurrentDepartments().getRecords().get(x).getDepartment() != null && !connectors.getCurrentDepartments().getRecords().get(x).getDepartment().isEmpty())
            && (connectors.getCurrentDepartments().getRecords().get(x).getYear() == 0 || connectors.getCurrentDepartments().getRecords().get(x).getAge() == null
                || connectors.getCurrentDepartments().getRecords().get(x).getAge() < 1)) {
          return true;
        }
        else if (connectors.getCurrentDepartments().getRecords().get(x).getDepartment() != null && !connectors.getCurrentDepartments().getRecords().get(
            x).getDepartment().isEmpty()) {
          maxAge = Math.max(maxAge, connectors.getCurrentDepartments().getRecords().get(x).getAge());
          if (connectors.getCurrentDepartments().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentDepartments().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentDepartments().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (connectors.getCurrentLabs() != null && connectors.getCurrentLabs().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentLabs().getRecords().size(); x++) {
        if ((connectors.getCurrentLabs().getRecords().get(x).getCode() != null && !connectors.getCurrentLabs().getRecords().get(x).getCode().trim().isEmpty())
            && (connectors.getCurrentLabs().getRecords().get(x).getYear() == 0 || (connectors.getCurrentLabs().getRecords().get(x).getTime() < 1))) {
          return true;
        }
        else if ((connectors.getCurrentLabs().getRecords().get(x).getCode() != null && !connectors.getCurrentLabs().getRecords().get(x).getCode().trim().isEmpty())) {
          maxAge = Math.max(maxAge, connectors.getCurrentLabs().getRecords().get(x).getTime());
          if (connectors.getCurrentLabs().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentLabs().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentLabs().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (connectors.getCurrentMeds() != null && connectors.getCurrentMeds().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentMeds().getRecords().size(); x++) {
        if ((connectors.getCurrentMeds().getRecords().get(x).getRxCui() != null && connectors.getCurrentMeds().getRecords().get(x).getRxCui() >= 0)
            && (connectors.getCurrentMeds().getRecords().get(x).getYear() == 0 || (connectors.getCurrentMeds().getRecords().get(x).getStart() < 1))) {
          return true;
        }
        else if (connectors.getCurrentMeds().getRecords().get(x).getRxCui() != null && connectors.getCurrentMeds().getRecords().get(x).getRxCui() >= 0) {
          maxAge = Math.max(maxAge, connectors.getCurrentMeds().getRecords().get(x).getStart());
          if (connectors.getCurrentMeds().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentMeds().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentMeds().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (connectors.getCurrentObservation() != null && connectors.getCurrentObservation().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentObservation().getRecords().size(); x++) {
        if ((connectors.getCurrentObservation().getRecords().get(x).getCode() != null && !connectors.getCurrentObservation().getRecords().get(x).getCode().isEmpty())
            && (connectors.getCurrentObservation().getRecords().get(x).getYear() == 0 || (connectors.getCurrentObservation().getRecords().get(x).getAge() < 1))) {
          return true;
        }
        else if (connectors.getCurrentObservation().getRecords().get(x).getCode() != null && !connectors.getCurrentObservation().getRecords().get(x).getCode().isEmpty()) {
          maxAge = Math.max(maxAge, connectors.getCurrentObservation().getRecords().get(x).getAge());
          if (connectors.getCurrentObservation().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentObservation().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentObservation().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (connectors.getCurrentTerms() != null && connectors.getCurrentTerms().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentTerms().getRecords().size(); x++) {
        if ((connectors.getCurrentTerms().getRecords().get(x).getTermId() >= 0) && (connectors.getCurrentTerms().getRecords().get(x).getYear() == 0
            || (connectors.getCurrentTerms().getRecords().get(x).getAge() < 1))) {
          return true;
        }
        else if (connectors.getCurrentTerms().getRecords().get(x).getTermId() >= 0) {
          maxAge = Math.max(maxAge, connectors.getCurrentTerms().getRecords().get(x).getAge());
          if (connectors.getCurrentTerms().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentTerms().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentTerms().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (connectors.getCurrentVisit() != null && connectors.getCurrentVisit().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentVisit().getRecords().size(); x++) {
        if (((connectors.getCurrentVisit().getRecords().get(x).getCode() != null && !connectors.getCurrentVisit().getRecords().get(x).getCode().isEmpty())
            || (connectors.getCurrentVisit().getRecords().get(x).getSrc_visit() != null && !connectors.getCurrentVisit().getRecords().get(x).getSrc_visit().isEmpty()
                && !connectors.getCurrentVisit().getRecords().get(x).getSrc_visit().equals("EMPTY"))) && (connectors.getCurrentVisit().getRecords().get(x).getYear() == 0
                    || connectors.getCurrentVisit().getRecords().get(x).getAge() == null || (connectors.getCurrentVisit().getRecords().get(x).getAge() < 1))) {
          return true;
        }
        else if (connectors.getCurrentVisit().getRecords().get(x).getCode() != null && !connectors.getCurrentVisit().getRecords().get(x).getCode().isEmpty()) {
          maxAge = Math.max(maxAge, connectors.getCurrentVisit().getRecords().get(x).getAge());
          if (connectors.getCurrentVisit().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentVisit().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentVisit().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (connectors.getCurrentVitals() != null && connectors.getCurrentVitals().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentVitals().getRecords().size(); x++) {
        if ((connectors.getCurrentVitals().getRecords().get(x).getDescription() != null && !connectors.getCurrentVitals().getRecords().get(x).getDescription().isEmpty())
            && (connectors.getCurrentVitals().getRecords().get(x).getYear() == 0 || (connectors.getCurrentVitals().getRecords().get(x).getAge() < 1))) {
          return true;
        }
        else if (connectors.getCurrentVitals().getRecords().get(x).getDescription() != null && !connectors.getCurrentVitals().getRecords().get(x).getDescription().isEmpty()) {
          maxAge = Math.max(maxAge, connectors.getCurrentVitals().getRecords().get(x).getAge());
          if (connectors.getCurrentVitals().getRecords().get(x).getYear() >= 0) {
            minYear = Math.min(minYear, connectors.getCurrentVitals().getRecords().get(x).getYear());
            maxYear = Math.max(maxYear, connectors.getCurrentVitals().getRecords().get(x).getYear());
          }
          isEmpty = false;
        }
      }
    }
    if (Common.minutesToYears(maxAge) > 130 || ((maxYear - minYear) > 130)) {
      return true;
    }
    return isEmpty;
  }

}
