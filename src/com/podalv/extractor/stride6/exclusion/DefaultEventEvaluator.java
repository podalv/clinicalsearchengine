package com.podalv.extractor.stride6.exclusion;

import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;

/** Default evaluator does not modify the data
 *
 * @author podalv
 *
 */
public class DefaultEventEvaluator implements InvalidEventEvaluator {

  @Override
  public void evaluate(final DatabaseConnectorsInterface connectors) {
    // does nothing
  }

}
