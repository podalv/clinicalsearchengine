package com.podalv.extractor.stride6.exclusion;

import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;

public class Stride7EventEvaluator implements InvalidEventEvaluator {

  private static int THREE_MONTHS = 3 * 30 * 24 * 60;
  private static int HIGH_AGE     = 120 * 365 * 24 * 60;
  private static int INVALID_YEAR = 1950;

  private boolean isChild(final DatabaseConnectorsInterface connectors) {
    final int pid = connectors.getCurrentDemo().getPatientId();
    if (connectors.getCurrentDepartments() != null && connectors.getCurrentDepartments().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentDepartments().getRecords().size(); x++) {
        if ((connectors.getCurrentDepartments().getRecords().get(x).getDepartment() != null && !connectors.getCurrentDepartments().getRecords().get(x).getDepartment().isEmpty())
            && (connectors.getCurrentDepartments().getRecords().get(x).getYear() >= INVALID_YEAR && (connectors.getCurrentDepartments().getRecords().get(x).getAge() != null
                && connectors.getCurrentDepartments().getRecords().get(x).getAge() > 0))) {
          if (connectors.getCurrentDepartments().getRecords().get(x).getAge() != null && connectors.getCurrentDepartments().getRecords().get(x).getAge() <= THREE_MONTHS) {
            return true;
          }
        }
      }
    }
    if (connectors.getCurrentLabs() != null && connectors.getCurrentLabs().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentLabs().getRecords().size(); x++) {
        if ((connectors.getCurrentLabs().getRecords().get(x).getCode() != null && !connectors.getCurrentLabs().getRecords().get(x).getCode().trim().isEmpty())
            && (connectors.getCurrentLabs().getRecords().get(x).getYear() >= INVALID_YEAR && connectors.getCurrentLabs().getRecords().get(x).getTime() > 0)) {
          if (connectors.getCurrentLabs().getRecords().get(x).getTime() <= THREE_MONTHS) {
            return true;
          }
        }
      }
    }
    if (connectors.getCurrentMeds() != null && connectors.getCurrentMeds().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentMeds().getRecords().size(); x++) {
        if ((connectors.getCurrentMeds().getRecords().get(x).getRxCui() != null && connectors.getCurrentMeds().getRecords().get(x).getRxCui() >= 0)
            && (connectors.getCurrentMeds().getRecords().get(x).getYear() >= INVALID_YEAR && connectors.getCurrentMeds().getRecords().get(x).getStart() > 0)) {
          if (connectors.getCurrentMeds().getRecords().get(x).getStart() <= THREE_MONTHS) {
            return true;
          }
        }
      }
    }
    if (connectors.getCurrentObservation() != null && connectors.getCurrentObservation().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentObservation().getRecords().size(); x++) {
        if ((connectors.getCurrentObservation().getRecords().get(x).getCode() != null && !connectors.getCurrentObservation().getRecords().get(x).getCode().isEmpty())
            && (connectors.getCurrentObservation().getRecords().get(x).getYear() >= INVALID_YEAR && connectors.getCurrentObservation().getRecords().get(x).getAge() > 0)) {
          if (connectors.getCurrentObservation().getRecords().get(x).getAge() <= THREE_MONTHS) {
            return true;
          }
        }
      }
    }
    if (connectors.getCurrentTerms() != null && connectors.getCurrentTerms().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentTerms().getRecords().size(); x++) {
        if ((connectors.getCurrentTerms().getRecords().get(x).getTermId() >= 0) && (connectors.getCurrentTerms().getRecords().get(x).getYear() >= INVALID_YEAR
            && connectors.getCurrentTerms().getRecords().get(x).getAge() > 0)) {
          if (connectors.getCurrentTerms().getRecords().get(x).getAge() <= THREE_MONTHS) {
            return true;
          }
        }
      }
    }
    if (connectors.getCurrentVisit() != null && connectors.getCurrentVisit().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentVisit().getRecords().size(); x++) {
        if (((connectors.getCurrentVisit().getRecords().get(x).getCode() != null && !connectors.getCurrentVisit().getRecords().get(x).getCode().isEmpty())
            || (connectors.getCurrentVisit().getRecords().get(x).getSrc_visit() != null && !connectors.getCurrentVisit().getRecords().get(x).getSrc_visit().isEmpty()
                && !connectors.getCurrentVisit().getRecords().get(x).getSrc_visit().equals("EMPTY"))) && (connectors.getCurrentVisit().getRecords().get(x).getAge() != null
                    && connectors.getCurrentVisit().getRecords().get(x).getAge() > 0 && connectors.getCurrentVisit().getRecords().get(x).getYear() >= INVALID_YEAR)) {
          if (connectors.getCurrentVisit().getRecords().get(x).getAge() <= THREE_MONTHS) {
            return true;
          }
        }
      }
    }
    if (connectors.getCurrentVitals() != null && connectors.getCurrentVitals().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentVitals().getRecords().size(); x++) {
        if ((connectors.getCurrentVitals().getRecords().get(x).getDescription() != null && !connectors.getCurrentVitals().getRecords().get(x).getDescription().isEmpty())
            && (connectors.getCurrentVitals().getRecords().get(x).getAge() != null && connectors.getCurrentVitals().getRecords().get(x).getAge() > 0
                && connectors.getCurrentVitals().getRecords().get(x).getYear() >= INVALID_YEAR)) {
          if (connectors.getCurrentVitals().getRecords().get(x).getAge() <= THREE_MONTHS) {
            return true;
          }

        }
      }
    }
    return false;
  }

  private boolean notEmpty(final String value) {
    return value != null && !value.isEmpty();
  }

  @Override
  public void evaluate(final DatabaseConnectorsInterface connectors) {
    final int pid = connectors.getCurrentDemo().getPatientId();
    final boolean isChild = isChild(connectors);
    if (connectors.getCurrentDepartments() != null && connectors.getCurrentDepartments().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentDepartments().getRecords().size(); x++) {
        if (notEmpty(connectors.getCurrentDepartments().getRecords().get(x).getDepartment()) && (connectors.getCurrentDepartments().getRecords().get(x).getAge() != null
            && connectors.getCurrentDepartments().getRecords().get(x).getAge() > HIGH_AGE || connectors.getCurrentDepartments().getRecords().get(x).getAge() == null
            || connectors.getCurrentDepartments().getRecords().get(x).getAge() < 0 || ((connectors.getCurrentDepartments().getRecords().get(x).getAge() == 0 && !isChild)
                || connectors.getCurrentDepartments().getRecords().get(x).getYear() < INVALID_YEAR))) {
          connectors.getCurrentDepartments().getRecords().get(x).invalidate();
        }
      }
    }
    if (connectors.getCurrentLabs() != null && connectors.getCurrentLabs().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentLabs().getRecords().size(); x++) {
        if (notEmpty(connectors.getCurrentLabs().getRecords().get(x).getCode()) && (connectors.getCurrentLabs().getRecords().get(x).getTime() > HIGH_AGE
            || connectors.getCurrentLabs().getRecords().get(x).getTime() < 0 || (!isChild && connectors.getCurrentLabs().getRecords().get(x).getTime() == 0)
            || connectors.getCurrentLabs().getRecords().get(x).getYear() < INVALID_YEAR)) {
          connectors.getCurrentLabs().getRecords().get(x).invalidate();
        }
      }
    }
    if (connectors.getCurrentMeds() != null && connectors.getCurrentMeds().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentMeds().getRecords().size(); x++) {
        if ((connectors.getCurrentMeds().getRecords().get(x).getRxCui() != null && connectors.getCurrentMeds().getRecords().get(x).getRxCui() >= 0)
            && (connectors.getCurrentMeds().getRecords().get(x).getStart() > HIGH_AGE || connectors.getCurrentMeds().getRecords().get(x).getStart() < 0 || (!isChild
                && connectors.getCurrentMeds().getRecords().get(x).getStart() == 0) || connectors.getCurrentMeds().getRecords().get(x).getYear() < INVALID_YEAR)) {
          connectors.getCurrentMeds().getRecords().get(x).invalidate();
        }
      }
    }
    if (connectors.getCurrentObservation() != null && connectors.getCurrentObservation().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentObservation().getRecords().size(); x++) {
        if (notEmpty(connectors.getCurrentObservation().getRecords().get(x).getCode()) && (connectors.getCurrentObservation().getRecords().get(x).getAge() > HIGH_AGE
            || connectors.getCurrentObservation().getRecords().get(x).getAge() < 0 || (connectors.getCurrentObservation().getRecords().get(x).getAge() == 0 && !isChild)
            || connectors.getCurrentObservation().getRecords().get(x).getYear() < INVALID_YEAR)) {
          connectors.getCurrentObservation().getRecords().get(x).invalidate();
        }
      }
    }
    if (connectors.getCurrentTerms() != null && connectors.getCurrentTerms().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentTerms().getRecords().size(); x++) {
        if ((connectors.getCurrentTerms().getRecords().get(x).getTermId() >= 0) && (connectors.getCurrentTerms().getRecords().get(x).getAge() > HIGH_AGE
            || connectors.getCurrentTerms().getRecords().get(x).getAge() < 0 || (connectors.getCurrentTerms().getRecords().get(x).getAge() == 0 && !isChild)
            || connectors.getCurrentTerms().getRecords().get(x).getYear() < INVALID_YEAR)) {
          connectors.getCurrentTerms().getRecords().get(x).invalidate();
        }
      }
    }
    if (connectors.getCurrentVisit() != null && connectors.getCurrentVisit().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentVisit().getRecords().size(); x++) {
        if (((notEmpty(connectors.getCurrentVisit().getRecords().get(x).getCode()) || (notEmpty(connectors.getCurrentVisit().getRecords().get(x).getSrc_visit())
            && !connectors.getCurrentVisit().getRecords().get(x).getSrc_visit().equals("EMPTY"))) && ((connectors.getCurrentVisit().getRecords().get(x).getAge() != null
                && connectors.getCurrentVisit().getRecords().get(x).getAge() > HIGH_AGE || connectors.getCurrentVisit().getRecords().get(x).getAge() == null
                || connectors.getCurrentVisit().getRecords().get(x).getAge() < 0 || (connectors.getCurrentVisit().getRecords().get(x).getAge() == 0 && !isChild)
                || connectors.getCurrentVisit().getRecords().get(x).getYear() < INVALID_YEAR)))) {
          connectors.getCurrentVisit().getRecords().get(x).invalidate();
        }
      }
    }
    if (connectors.getCurrentVitals() != null && connectors.getCurrentVitals().getPatientId() == pid) {
      for (int x = 0; x < connectors.getCurrentVitals().getRecords().size(); x++) {
        if ((notEmpty(connectors.getCurrentVitals().getRecords().get(x).getDescription()) && (connectors.getCurrentVitals().getRecords().get(x).getAge() > HIGH_AGE
            || connectors.getCurrentVitals().getRecords().get(x).getAge() < 0 || (connectors.getCurrentVitals().getRecords().get(x).getAge() == 0 && !isChild)
            || connectors.getCurrentVitals().getRecords().get(x).getYear() < INVALID_YEAR))) {
          connectors.getCurrentVitals().getRecords().get(x).invalidate();
        }
      }
    }
  }
}
