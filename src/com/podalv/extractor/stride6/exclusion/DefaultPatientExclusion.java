package com.podalv.extractor.stride6.exclusion;

import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;

/** Does not exclude any patients
 *
 * @author podalv
 *
 */
public class DefaultPatientExclusion implements PatientExclusion {

  @Override
  public boolean exclude(final DatabaseConnectorsInterface connectors) {
    return false;
  }
}
