package com.podalv.extractor.stride6.exclusion;

import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;

/** Part of Stride6Extractor, used to pass different exclusion schemas for different data sources
 *  Allows evaluating all the data for the patient and flagging patients that should be excluded
 *
 *
 * @author podalv
 *
 */
public interface PatientExclusion {

  /** Evaluates all the data about a patient
   *
   * @param connectors
   * @return
   */
  public boolean exclude(final DatabaseConnectorsInterface connectors);
}
