package com.podalv.extractor.stride6.exclusion;

import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;

/** Evaluates all the events for the patient and flags invalid events
 *
 * @author podalv
 *
 */
public interface InvalidEventEvaluator {

  public void evaluate(final DatabaseConnectorsInterface connectors);
}
