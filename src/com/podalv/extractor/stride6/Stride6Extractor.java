package com.podalv.extractor.stride6;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource;
import com.podalv.extractor.datastructures.CommonWordsFilter;
import com.podalv.extractor.datastructures.ExtractionOptions;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.DepartmentRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.NoteRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitDxRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.datastructures.DatabaseConnectors;
import com.podalv.extractor.stride6.datastructures.DatabaseConnectorsInterface;
import com.podalv.extractor.stride6.exclusion.DefaultEventEvaluator;
import com.podalv.extractor.stride6.exclusion.DefaultPatientExclusion;
import com.podalv.extractor.stride6.exclusion.InvalidEventEvaluator;
import com.podalv.extractor.stride6.exclusion.PatientExclusion;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.index.StatisticsBuilderCompressed;
import com.podalv.search.datastructures.index.StringEnumIndexBuilder;
import com.podalv.search.datastructures.index.StringIntegerFastIndex;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.server.AtlasVersion;
import com.podalv.utils.Logging;
import com.podalv.utils.arrays.ComparableIntArray;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.text.TextUtils;

public class Stride6Extractor {

  public static final int                       MAX_END_TIME           = 120 * 365 * 24 * 60;
  public static int                             SHARD_SIZE             = 500000000;
  public static int                             SHARD_COUNT            = 5;
  private final DatabaseConnectorsInterface     connectors;
  private IndexCollection                       indices                = null;
  private final Statistics                      statistics             = new Statistics(new StatisticsBuilderCompressed(70000000));
  private PersistentObjectDatabaseGenerator     generator;
  private static UmlsDictionary                 umls                   = UmlsDictionary.create();
  private final IntKeyObjectMap<IntOpenHashSet> visitIdToVisitDx       = new IntKeyObjectMap<>();
  private final IntKeyIntOpenHashMap            noteIdToDocDescription = new IntKeyIntOpenHashMap();
  private final StringIntegerFastIndex          noteTypeMap            = new StringIntegerFastIndex();
  private final IntKeyIntOpenHashMap            noteIdToYear           = new IntKeyIntOpenHashMap();
  private final IntKeyObjectMap<Double>         noteIdToAge            = new IntKeyObjectMap<>();
  private static IntOpenHashSet                 ignorePatientIds       = new IntOpenHashSet();
  private final PatientExclusion                exclusionEvaluator;
  private final InvalidEventEvaluator           eventEvaluator;

  Stride6Extractor(final DatabaseConnectorsInterface connectors, final PatientExclusion exclusionEvaluator, final InvalidEventEvaluator eventEvaluator) throws SQLException {
    this.connectors = connectors;
    this.exclusionEvaluator = exclusionEvaluator;
    this.eventEvaluator = eventEvaluator;
  }

  private PatientBuilder generatePatient(final DemographicsRecord demographics) throws IOException {
    return new PatientBuilder(indices);
  }

  public static IntKeyObjectMap<IntOpenHashSet> generateIcd9Hierarchy(final IndexCollection indices, final UmlsDictionary umls, final PatientRecord<? extends VisitRecord> icd9) {
    final IntKeyObjectMap<IntOpenHashSet> result = new IntKeyObjectMap<>();
    for (final VisitRecord record : icd9.getRecords()) {
      if (record.isIcd9Code() && (record.getCode() != null || record.getCodeId() != -1)) {
        if (record.getCode() != null) {
          final String[] hierarchy = umls.getIcd9Parents(record.getCode());
          if (hierarchy != null) {
            final int childId = record.getCode() == null ? record.getCodeId() : indices.addIcd9Code(record.getCode());
            IntOpenHashSet s = result.get(childId);
            if (s == null) {
              s = new IntOpenHashSet();
              result.put(childId, s);
            }
            for (final String hierarchyItem : hierarchy) {
              s.add(indices.addIcd9Code(hierarchyItem));
            }
          }
        }
        else if (record.getCodeId() >= 0) {
          final int[] hierarchy = umls.getIcd9Parents(record.getCodeId(), indices);
          if (hierarchy != null) {
            final int childId = record.getCode() == null ? record.getCodeId() : indices.addIcd9Code(record.getCode());
            IntOpenHashSet s = result.get(childId);
            if (s == null) {
              s = new IntOpenHashSet();
              result.put(childId, s);
            }
            for (final int hierarchyItem : hierarchy) {
              s.add(hierarchyItem);
            }
          }
        }
      }
    }
    return result;
  }

  public static IntKeyObjectMap<IntOpenHashSet> generateIcd10Hierarchy(final IndexCollection indices, final UmlsDictionary umls, final PatientRecord<? extends VisitRecord> icd10) {
    final IntKeyObjectMap<IntOpenHashSet> result = new IntKeyObjectMap<>();
    for (final VisitRecord record : icd10.getRecords()) {
      if (record.isIcd10Code() && (record.getCode() != null || record.getCodeId() != -1)) {
        if (record.getCode() != null) {
          final String[] hierarchy = umls.getIcd10Parents(record.getCode());
          if (hierarchy != null) {
            final int childId = record.getCode() == null ? record.getCodeId() : indices.addIcd10Code(record.getCode());
            IntOpenHashSet s = result.get(childId);
            if (s == null) {
              s = new IntOpenHashSet();
              result.put(childId, s);
            }
            for (final String hierarchyItem : hierarchy) {
              s.add(indices.addIcd10Code(hierarchyItem));
            }
          }
        }
        else if (record.getCodeId() >= 0) {
          final int[] hierarchy = umls.getIcd10Parents(record.getCodeId(), indices);
          if (hierarchy != null) {
            final int childId = record.getCode() == null ? record.getCodeId() : indices.addIcd10Code(record.getCode());
            IntOpenHashSet s = result.get(childId);
            if (s == null) {
              s = new IntOpenHashSet();
              result.put(childId, s);
            }
            for (final int hierarchyItem : hierarchy) {
              s.add(hierarchyItem);
            }
          }
        }
      }
    }
    return result;
  }

  public static IntKeyObjectMap<IntOpenHashSet> generateAtcToRxNormMap(final IndexCollection indices, final UmlsDictionary umls, final PatientRecord<MedRecord> meds) {
    final IntKeyObjectMap<IntOpenHashSet> result = new IntKeyObjectMap<>();
    for (final MedRecord record : meds.getRecords()) {
      final HashSet<String> atc = umls.getAtcHierarchy(record.getRxCui());
      final Iterator<String> i = atc.iterator();
      while (i.hasNext()) {
        final int atcId = indices.addAtcCode(i.next());
        IntOpenHashSet set = result.get(atcId);
        if (set == null) {
          set = new IntOpenHashSet();
          result.put(atcId, set);
        }
        set.add(record.getRxCui());
      }
    }
    return result;
  }

  public static IntKeyObjectMap<IntOpenHashSet> generateAtcToRxNormMapCached(final IndexCollection indices, final UmlsDictionary umls, final PatientRecord<MedRecord> meds) {
    final IntKeyObjectMap<IntOpenHashSet> result = new IntKeyObjectMap<>();
    final IntOpenHashSet uniqueRx = new IntOpenHashSet();
    for (final MedRecord record : meds.getRecords()) {
      uniqueRx.add(record.getRxCui());
    }
    final IntIterator ii = uniqueRx.iterator();
    while (ii.hasNext()) {
      final int rx = ii.next();
      final IntOpenHashSet atc = umls.getAtcHierarchy(indices, rx);
      final IntIterator i = atc.iterator();
      while (i.hasNext()) {
        final int atcId = i.next();
        IntOpenHashSet set = result.get(atcId);
        if (set == null) {
          set = new IntOpenHashSet();
          result.put(atcId, set);
        }
        set.add(rx);
      }
    }
    return result;
  }

  public static Object[] recordVisitStatistics(final IndexCollection indices, final Statistics statistics, final UmlsDictionary umls,
      final PatientRecord<? extends VisitRecord> record) {
    if (record != null) {
      if (statistics != null) {
        final IntOpenHashSet uniqueIcd9Codes = new IntOpenHashSet();
        final IntOpenHashSet uniqueIcd10Codes = new IntOpenHashSet();
        final IntOpenHashSet uniqueCptCodes = new IntOpenHashSet();
        final IntOpenHashSet uniqueSnomedCodes = new IntOpenHashSet();
        for (int x = 0; x < record.getRecords().size(); x++) {
          if (record.getRecords().get(x).getCode() != null || record.getRecords().get(x).getCodeId() != -1) {
            if (record.getRecords().get(x).isIcd9Code()) {
              uniqueIcd9Codes.add(record.getRecords().get(x).getCode() == null ? record.getRecords().get(x).getCodeId()
                  : indices.addIcd9Code(record.getRecords().get(x).getCode()));
            }
            if (record.getRecords().get(x).isIcd10Code()) {
              uniqueIcd10Codes.add(record.getRecords().get(x).getCode() == null ? record.getRecords().get(x).getCodeId()
                  : indices.addIcd10Code(record.getRecords().get(x).getCode()));
            }
            if (record.getRecords().get(x).isCptCode()) {
              uniqueCptCodes.add(record.getRecords().get(x).getCode() == null ? record.getRecords().get(x).getCodeId() : indices.addCptCode(record.getRecords().get(x).getCode()));
            }
            else if (record.getRecords().get(x).getSab() != null && record.getRecords().get(x).getSab().equals("SNOMED")) {
              uniqueSnomedCodes.add(record.getRecords().get(x).getCode() == null ? record.getRecords().get(x).getCodeId()
                  : indices.addSnomedCode(record.getRecords().get(x).getCode()));
            }
          }
        }
        IntIterator iterator = uniqueIcd9Codes.iterator();
        while (iterator.hasNext()) {
          statistics.addIcd9(iterator.next(), record.getPatientId());
        }
        iterator = uniqueCptCodes.iterator();
        while (iterator.hasNext()) {
          statistics.addCpt(iterator.next(), record.getPatientId());
        }
        iterator = uniqueIcd10Codes.iterator();
        while (iterator.hasNext()) {
          statistics.addIcd10(iterator.next(), record.getPatientId());
        }
        iterator = uniqueSnomedCodes.iterator();
        while (iterator.hasNext()) {
          statistics.addSnomed(iterator.next(), record.getPatientId());
        }
      }
      final IntKeyObjectMap<IntOpenHashSet> map = generateIcd9Hierarchy(indices, umls, record);
      if (statistics != null) {
        final IntKeyObjectIterator<IntOpenHashSet> icd9Hierarchy = map.entries();
        while (icd9Hierarchy.hasNext()) {
          icd9Hierarchy.next();
          statistics.addIcd9(icd9Hierarchy.getKey(), record.getPatientId());
        }
      }
      final IntKeyObjectMap<IntOpenHashSet> map2 = generateIcd10Hierarchy(indices, umls, record);
      if (statistics != null) {
        final IntKeyObjectIterator<IntOpenHashSet> icd10Hierarchy = map2.entries();
        while (icd10Hierarchy.hasNext()) {
          icd10Hierarchy.next();
          statistics.addIcd10(icd10Hierarchy.getKey(), record.getPatientId());
        }
      }
      return new Object[] {map, map2};
    }
    return null;
  }

  private void recordVitalsStatistics(final PatientRecord<VitalsRecord> record) {
    if (record != null) {
      final IntOpenHashSet uniqueVitalsCodes = new IntOpenHashSet();
      for (int x = 0; x < record.getRecords().size(); x++) {
        uniqueVitalsCodes.add(indices.addVitals(record.getRecords().get(x).getDescription()));
      }
      final IntIterator iterator = uniqueVitalsCodes.iterator();
      while (iterator.hasNext()) {
        statistics.addVitals(iterator.next(), record.getPatientId());
      }
    }
  }

  private void recordDepartmentStatistics(final PatientRecord<DepartmentRecord> record) {
    if (record != null) {
      final IntOpenHashSet uniqueDeptCodes = new IntOpenHashSet();
      for (int x = 0; x < record.getRecords().size(); x++) {
        uniqueDeptCodes.add(indices.addDepartmentCode(record.getRecords().get(x).getDepartment()));
      }
      final IntIterator iterator = uniqueDeptCodes.iterator();
      while (iterator.hasNext()) {
        statistics.addDepartment(iterator.next(), record.getPatientId());
      }
    }
  }

  public static void recordLabsStatistics(final IndexCollection indices, final Statistics statistics, final PatientRecord<LabsRecord> record) {
    if (record != null) {
      final IntOpenHashSet uniqueLabsCodes = new IntOpenHashSet();
      final IntKeyObjectMap<IntOpenHashSet> labIdToValues = new IntKeyObjectMap<>();
      for (int x = 0; x < record.getRecords().size(); x++) {
        if (record.getRecords().get(x) == null || record.getRecords().get(x).getCode() == null) {
          continue;
        }
        final String code = record.getRecords().get(x).getCode().trim();
        if (code != null && !code.isEmpty()) {
          final int labCode = indices.addLabs(record.getRecords().get(x).getCode());
          uniqueLabsCodes.add(labCode);
          if (record.getRecords().get(x).hasCalculatedValue()) {
            IntOpenHashSet labValues = labIdToValues.get(labCode);
            if (labValues == null) {
              labValues = new IntOpenHashSet();
              labIdToValues.put(labCode, labValues);
            }
            labValues.add(indices.getLabValueId(record.getRecords().get(x).getCalculatedValue()));
          }
        }
      }
      final IntIterator iterator = uniqueLabsCodes.iterator();
      while (iterator.hasNext()) {
        final int labId = iterator.next();
        statistics.addLabs(labId, record.getPatientId());
        if (labIdToValues.get(labId) != null) {
          final IntIterator values = labIdToValues.get(labId).iterator();
          while (values.hasNext()) {
            statistics.addLabValue(labId, values.next());
          }
        }
      }
    }
  }

  public static void recordMedsStatistics(final IndexCollection indices, final Statistics statistics, final UmlsDictionary umls, final PatientRecord<MedRecord> record) {
    if (record != null) {
      final IntOpenHashSet uniqueCodes = new IntOpenHashSet();
      final HashSet<ComparableIntArray> set = new HashSet<>();
      for (final MedRecord rec : record.getRecords()) {
        uniqueCodes.add(rec.getRxCui());
        final int[] array = new int[] {rec.getRxCui(), indices.addDrugRouteId(rec.getRoute()), indices.addDrugStatusId(rec.getStatus())};
        set.add(new ComparableIntArray(array));
      }
      statistics.addRxNormStats(set.iterator());
      final IntIterator iterator = uniqueCodes.iterator();
      while (iterator.hasNext()) {
        statistics.addRxNorm(iterator.next(), record.getPatientId());
      }
      final IntKeyObjectIterator<IntOpenHashSet> atcMap = generateAtcToRxNormMap(indices, umls, record).entries();
      while (atcMap.hasNext()) {
        atcMap.next();
        statistics.addAtc(atcMap.getKey(), record.getPatientId());
      }
    }
  }

  private void recordTidStats(final IntKeyIntOpenHashMap tidCnt, final PatientRecord<TermRecord> terms) {
    final IntOpenHashSet uniqueTids = new IntOpenHashSet();
    final IntOpenHashSet uniqueNegatedTids = new IntOpenHashSet();
    final IntOpenHashSet uniqueFamilyHistoryTids = new IntOpenHashSet();
    final IntOpenHashSet uniqueNoteTypes = new IntOpenHashSet();
    for (int x = 0; x < terms.getRecords().size(); x++) {
      if (terms.getRecords().get(x).getNegated() == 0 && terms.getRecords().get(x).getFamilyHistory() == 0) {
        uniqueTids.add(terms.getRecords().get(x).getTermId());
      }
      else if (terms.getRecords().get(x).getNegated() != 0 && terms.getRecords().get(x).getFamilyHistory() != 0) {
        uniqueFamilyHistoryTids.add(terms.getRecords().get(x).getTermId());
        uniqueNegatedTids.add(terms.getRecords().get(x).getTermId());
      }
      else if (terms.getRecords().get(x).getNegated() != 0) {
        uniqueNegatedTids.add(terms.getRecords().get(x).getTermId());
      }
      else if (terms.getRecords().get(x).getFamilyHistory() != 0) {
        uniqueFamilyHistoryTids.add(terms.getRecords().get(x).getTermId());
      }
      uniqueNoteTypes.add(terms.getRecords().get(x).getNoteTypeId());
    }

    IntIterator i = uniqueTids.iterator();
    while (i.hasNext()) {
      final int tid = i.next();
      tidCnt.put(tid, tidCnt.get(tid) + 1);
      statistics.addTid(tid, terms.getPatientId());
    }

    i = uniqueNegatedTids.iterator();
    while (i.hasNext()) {
      final int tid = i.next();
      tidCnt.put(tid, tidCnt.get(tid) + 1);
      statistics.addTidNegated(tid, terms.getPatientId());
    }

    i = uniqueFamilyHistoryTids.iterator();
    while (i.hasNext()) {
      final int tid = i.next();
      tidCnt.put(tid, tidCnt.get(tid) + 1);
      statistics.addTidFamilyHistory(tid, terms.getPatientId());
    }

    i = uniqueNoteTypes.iterator();
    while (i.hasNext()) {
      final int noteTypeId = i.next();
      statistics.addNoteType(noteTypeId, terms.getPatientId());
    }

  }

  public static void recordVisitTypeStats(final IndexCollection indices, final Statistics statistics, final PatientRecord<VisitRecord> record) {
    final IntOpenHashSet uniqueVisitTypes = new IntOpenHashSet();
    for (int x = 0; x < record.getRecords().size(); x++) {
      if (record.getRecords().get(x).getSrc_visit() != null) {
        uniqueVisitTypes.add(indices.getVisitType(record.getRecords().get(x).getSrc_visit()));
      }
    }
    final IntIterator i = uniqueVisitTypes.iterator();
    while (i.hasNext()) {
      statistics.addVisitType(i.next(), record.getPatientId());
    }
  }

  private void extractLabNames(final File labComponentFile) {
    if (labComponentFile.exists()) {
      BinaryFileExtractionSource source = null;
      FileInputStream stream = null;
      try {
        stream = new FileInputStream(labComponentFile);
        source = new BinaryFileExtractionSource(stream, new FileInputStream(Common.getDictFile(labComponentFile)), Commands.LABS_COMPONENT_STRUCTURE);
        while (source.next()) {
          final String baseName = source.getString(Commands.LABS_COMPONENT_BASE_NAME_COLUMN);
          final String commonName = source.getString(Commands.LABS_COMPONENT_COMMON_NAME_COLUMN);
          indices.addLabsCommonName(baseName.toUpperCase(), commonName);
        }
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
      finally {
        FileUtils.close(stream);
      }
    }
  }

  private void readVisitDxFile(final Extractor<VisitDxRecord> extractor) throws IOException {
    if (extractor != null) {
      while (extractor.hasNext()) {
        final VisitDxRecord record = extractor.next();
        final int visitId = record.getVisitId();
        final int sourceId = record.getVisitDxId();
        final String primary = record.getPrimary();
        if (primary != null && primary.equalsIgnoreCase("Y")) {
          IntOpenHashSet s = visitIdToVisitDx.get(visitId);
          if (s == null) {
            s = new IntOpenHashSet();
            visitIdToVisitDx.put(visitId, s);
          }
          s.add(sourceId);
        }
      }
      extractor.close();
    }
  }

  private void readNotesFile(final Extractor<NoteRecord> extractor) throws IOException {
    if (extractor != null) {
      while (extractor.hasNext()) {
        final NoteRecord record = extractor.next();
        final int noteId = record.getNoteId();
        String noteType = record.getNoteType();
        if (noteType == null || noteType.trim().isEmpty()) {
          noteType = StringEnumIndexBuilder.EMPTY;
        }
        final int descriptionId = noteTypeMap.add(noteType.toLowerCase());
        noteIdToDocDescription.put(noteId, descriptionId);
        noteIdToAge.put(noteId, record.getAge());
        noteIdToYear.put(noteId, record.getYear());
      }
      extractor.close();
    }
  }

  private void setPrimaryIcd9Code(final PatientRecord<VisitRecord> icd9Codes) {
    for (int x = 0; x < icd9Codes.getRecords().size(); x++) {
      final IntOpenHashSet primaryVisitDxCodes = visitIdToVisitDx.get(icd9Codes.getRecords().get(x).getVisitId());
      if (primaryVisitDxCodes != null && primaryVisitDxCodes.contains(icd9Codes.getRecords().get(x).getSourceCode())) {
        icd9Codes.getRecords().get(x).setPrimary(true);
      }
      if (icd9Codes.getRecords().get(x).getSab() != null && (icd9Codes.getRecords().get(x).getSab().equals(AtlasDatabaseDownload.CODE_TYPE_ICD9_PRIMARY))) {
        icd9Codes.getRecords().get(x).setPrimary(true);
      }
    }
  }

  public static void addIcd9HierarchyCodes(final IndexCollection indices, final PatientRecord<VisitRecord> icd9Codes, final IntKeyObjectMap<IntOpenHashSet> childToParents) {
    if (childToParents.size() > 0) {
      final ArrayList<VisitRecord> newVisits = new ArrayList<>();
      for (int x = 0; x < icd9Codes.getRecords().size(); x++) {
        if (icd9Codes.getRecords().get(x).isIcd9Code() && (icd9Codes.getRecords().get(x).getCode() != null || icd9Codes.getRecords().get(x).getCodeId() != -1)) {
          int codeId = icd9Codes.getRecords().get(x).getCodeId();
          if (codeId == -1 && icd9Codes.getRecords().get(x).getCode() != null) {
            codeId = indices.getIcd9(icd9Codes.getRecords().get(x).getCode());
          }
          if (codeId != -1) {
            final IntOpenHashSet parents = childToParents.get(codeId);
            if (parents != null) {
              final IntIterator parentIterator = parents.iterator();
              while (parentIterator.hasNext()) {
                final int parentId = parentIterator.next();
                if (parentId != codeId) {
                  newVisits.add(icd9Codes.getRecords().get(x).copy(indices.getIcd9(parentId), parentId).setGeneratedByHierarchy(true));
                }
              }
            }
          }
        }
      }
      if (newVisits.size() > 0) {
        icd9Codes.getRecords().addAll(newVisits);
        Collections.sort(icd9Codes.getRecords());
      }
    }
  }

  public static void addIcd10HierarchyCodes(final IndexCollection indices, final PatientRecord<VisitRecord> icd10Codes, final IntKeyObjectMap<IntOpenHashSet> childToParents) {
    if (childToParents.size() > 0) {
      final ArrayList<VisitRecord> newVisits = new ArrayList<>();
      for (int x = 0; x < icd10Codes.getRecords().size(); x++) {
        if (icd10Codes.getRecords().get(x).isIcd10Code() && (icd10Codes.getRecords().get(x).getCode() != null || icd10Codes.getRecords().get(x).getCodeId() != -1)) {
          int codeId = icd10Codes.getRecords().get(x).getCodeId();
          if (codeId == -1 && icd10Codes.getRecords().get(x).getCode() != null) {
            codeId = indices.getIcd10(icd10Codes.getRecords().get(x).getCode());
          }
          if (codeId != -1) {
            final IntOpenHashSet parents = childToParents.get(codeId);
            if (parents != null) {
              final IntIterator parentIterator = parents.iterator();
              while (parentIterator.hasNext()) {
                final int parentId = parentIterator.next();
                if (parentId != codeId) {
                  newVisits.add(icd10Codes.getRecords().get(x).copy(indices.getIcd10(parentId), parentId).setGeneratedByHierarchy(true));
                }
              }
            }
          }
        }
      }
      if (newVisits.size() > 0) {
        icd10Codes.getRecords().addAll(newVisits);
        Collections.sort(icd10Codes.getRecords());
      }
    }
  }

  private void setPrimaryIcd10Code(final PatientRecord<VisitRecord> icd10Codes) {
    for (int x = 0; x < icd10Codes.getRecords().size(); x++) {
      if (icd10Codes.getRecords().get(x).getSab() != null && (icd10Codes.getRecords().get(x).getSab().equals(AtlasDatabaseDownload.CODE_TYPE_ICD10_PRIMARY))) {
        icd10Codes.getRecords().get(x).setPrimary(true);
      }
    }
  }

  private void setDocDescription(final PatientRecord<TermRecord> terms) {
    if (terms != null) {
      for (final TermRecord record : terms.getRecords()) {
        if (noteIdToDocDescription.containsKey(record.getNoteId())) {
          record.setNoteTypeId(noteIdToDocDescription.get(record.getNoteId()));
        }
        if (noteIdToAge.containsKey(record.getNoteId())) {
          record.setAge(Common.daysToTime(noteIdToAge.get(record.getNoteId())));
        }
        if (noteIdToYear.containsKey(record.getNoteId())) {
          record.setYear(noteIdToYear.get(record.getNoteId()));
        }
      }
    }
  }

  private static void readTermDictionary(final File file, final IndexCollection indices) throws IOException {
    final TextFileReader reader = FileUtils.readFileIterator(file, Charset.forName("UTF-8"));
    while (reader.hasNext()) {
      final String[] data = TextUtils.split(reader.next(), '\t');
      if (data.length == 2) {
        indices.addTid(Integer.parseInt(data[0]), data[1]);
      }
    }
    reader.close();
  }

  private static void readSnomedDictionary(final File file, final IndexCollection indices) throws IOException {
    if (file.exists()) {
      final BufferedReader reader = new BufferedReader(new FileReader(file));
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        indices.addSnomedText(data[0], data[1]);
      }
      reader.close();
    }
  }

  void extract(final File inputFolder, final File outputFolder, final int maxPatientCnt) throws IOException, InterruptedException {
    final HashSet<String> uniqueLabNames = new HashSet<>();
    umls.extractCustomCodeDictionary(new File(inputFolder, Stride6DatabaseDownload.CODE_DICTIONARY_FILE));
    FileUtils.copyFile(new File(inputFolder, Stride6DatabaseDownload.CODE_DICTIONARY_FILE), new File(outputFolder, Stride6DatabaseDownload.CODE_DICTIONARY_FILE));
    noteTypeMap.clear();

    indices = new IndexCollection(null);
    File dictFile = new File(outputFolder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE);
    if (!dictFile.exists()) {
      dictFile = new File(inputFolder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE);
    }
    System.out.println("Reading term dictionary");
    try {
      readTermDictionary(dictFile, indices);
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    generator = new PersistentObjectDatabaseGenerator(SHARD_SIZE, SHARD_COUNT, outputFolder);
    System.out.println("Reading VisitDxFile");
    readVisitDxFile(connectors.getDxExtractor());
    System.out.println("Reading Notes");
    readNotesFile(connectors.getNotesExtractor());
    System.out.println("Reading SNOMED dictionary");
    readSnomedDictionary(new File(inputFolder, Stride6DatabaseDownload.SNOMED_DICTIONARY_FILE), indices);
    System.out.println("Starting extraction...");
    int processedCnt = 0;
    int patientCntInReport = 0;
    int patientSizeInReport = 0;
    int processedPatientsInReport = 0;
    int rejectedPatients = 0;
    int rejectedPatientsInReport = 0;
    int emptyPatients = 0;
    int emptyPatientsInReport = 0;
    final IntKeyIntOpenHashMap tidCnt = new IntKeyIntOpenHashMap();
    final long startTime = System.currentTimeMillis();
    long displayRefreshTime = System.currentTimeMillis();
    int labPatientCnt = 0;
    while (connectors.hasNext()) {
      try {
        connectors.next();
      }
      catch (final Exception e) {
        e.printStackTrace();
        break;
      }
      if (processedCnt >= maxPatientCnt) {
        break;
      }

      processedCnt++;

      if ((System.currentTimeMillis() - displayRefreshTime) > 15000 && patientCntInReport > 0 && processedPatientsInReport > 0) {
        displayRefreshTime = System.currentTimeMillis();
        System.out.println("Processed           : " + processedCnt + " patients, time per patient: " + new DecimalFormat("##.###").format((System.currentTimeMillis() - startTime)
            / (double) (processedCnt)) + " ms");
        System.out.println("Memory used         : " + generator.getMemoryUsed());
        System.out.println("Average patient size: " + patientSizeInReport / patientCntInReport);
        System.out.println("Rejected / empty / processed: " + rejectedPatientsInReport + " / " + emptyPatientsInReport + " / " + processedPatientsInReport);
        patientCntInReport = 0;
        patientSizeInReport = 0;
        rejectedPatientsInReport = 0;
        processedPatientsInReport = 0;
        emptyPatientsInReport = 0;
      }
      processedPatientsInReport++;
      final PatientBuilder patient = generatePatient(connectors.getCurrentDemo());
      if (ignorePatientIds.contains(patient.getId())) {
        continue;
      }
      setDocDescription(connectors.getCurrentTerms());
      final boolean patientShouldBeRemoved = exclusionEvaluator.exclude(connectors);
      if (!patientShouldBeRemoved) {
        eventEvaluator.evaluate(connectors);
        recordPatientData(patient);
        if (patient.isEmpty()) {
          indices.addEmptyDemographicRecord(patient.getId());
          emptyPatients++;
          emptyPatientsInReport++;
        }
        else {
          indices.addDemographicRecord(connectors.getCurrentDemo());
          if (connectors.getCurrentDemo().getDeathInDays() != 0) {
            int deathYear = -1;
            if (connectors.getCurrentDemo().getDeathYear() > 0) {
              deathYear = connectors.getCurrentDemo().getDeathYear();
            }
            patient.recordTime(deathYear, Common.daysToTime(connectors.getCurrentDemo().getDeathInDays()), Common.daysToTime(connectors.getCurrentDemo().getDeathInDays()));
          }
          if (connectors.getCurrentLabs() != null && connectors.getCurrentLabs().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordLabsStatistics(indices, statistics, connectors.getCurrentLabs());
            for (int x = 0; x < connectors.getCurrentLabs().getRecords().size(); x++) {
              uniqueLabNames.add(connectors.getCurrentLabs().getRecords().get(x).getCode());
            }
          }
          if (connectors.getCurrentVitals() != null && connectors.getCurrentVitals().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordVitalsStatistics(connectors.getCurrentVitals());
          }
          if (connectors.getCurrentDepartments() != null && connectors.getCurrentDepartments().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordDepartmentStatistics(connectors.getCurrentDepartments());
          }
          if (connectors.getCurrentVisit() != null && connectors.getCurrentVisit().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordVisitStatistics(indices, statistics, umls, connectors.getCurrentVisit());
            recordVisitTypeStats(indices, statistics, connectors.getCurrentVisit());
          }
          if (connectors.getCurrentObservation() != null && connectors.getCurrentObservation().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordVisitStatistics(indices, statistics, umls, connectors.getCurrentObservation());
          }
          if (connectors.getCurrentMeds() != null && connectors.getCurrentMeds().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordMedsStatistics(indices, statistics, umls, connectors.getCurrentMeds());
          }
          if (connectors.getCurrentTerms() != null && connectors.getCurrentTerms().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
            recordTidStats(tidCnt, connectors.getCurrentTerms());
          }
          if (patient.getStartTime() < 0 || patient.getEndTime() < 0 || patient.getEndTime() > MAX_END_TIME || patient.getStartTime() > MAX_END_TIME) {
            patient.setStartEndTime(0, 0);
          }
          statistics.addAge(Common.minutesToYears(patient.getStartTime()));
          patient.close();
          statistics.recordPatientId(patient);
          patientSizeInReport += generator.add(patient);
          patientCntInReport++;
          if (patient.getUniqueLabIds() != null && patient.getUniqueLabIds().length != 0) {
            labPatientCnt++;
          }
          if (Common.minutesToYears(patient.getEndTime() - patient.getStartTime()) > 60) {
            System.out.println("TIMELINE >60 years for pid, start_time, end_time: " + patient.getId() + ", " + patient.getStartTime() + ", " + patient.getEndTime());
          }
        }
      }
      else {
        indices.addEmptyDemographicRecord(patient.getId());
        rejectedPatients++;
        rejectedPatientsInReport++;
      }
    }

    System.out.println("Processed all patients...");

    extractLabNames(new File(inputFolder, "labs_component.bin"));

    System.out.println("Setting the DocDescriptionType map...");
    indices.setDocDescription(noteTypeMap);

    System.out.println("Saving indices...");
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(outputFolder, Common.INDICES_FILE_NAME)));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();

    System.out.println("Saving statistics...");
    final BufferedOutputStream statisticsStream = new BufferedOutputStream(new FileOutputStream(new File(outputFolder, Common.STATISTICS_FILE_NAME)));
    statistics.save(new StreamOutput(statisticsStream));
    statisticsStream.close();

    System.out.println("Closing data generator...");
    generator.close();
    System.out.println("Generator closed");
    final BufferedWriter report = new BufferedWriter(new FileWriter(new File(outputFolder, "report.txt")));
    report.close();

    System.out.println("Processed patients: " + processedCnt);
    System.out.println("Rejected patients : " + rejectedPatients);
    System.out.println("Empty patients : " + emptyPatients);
    System.out.println("Unique ICD9 codes: " + statistics.getUniqueIcd9Counts());
    System.out.println("Unique CPT codes: " + statistics.getUniqueCptCounts());
    System.out.println("Unique LAB names: " + uniqueLabNames.size());
    System.out.println("Unique RXNORM codes: " + statistics.getUniqueRxNormCounts());
    System.out.println("Patients with LABS: " + labPatientCnt);
    System.out.println("It took " + new DecimalFormat("##.###").format(((System.currentTimeMillis() - startTime) / (double) (1000 * 60))) + " minutes");
  }

  private void recordPatientData(final PatientBuilder patient) throws UnsupportedDataTypeException {
    patient.setId(connectors.getCurrentDemo().getPatientId());
    if (connectors.getCurrentVisit() != null && connectors.getCurrentVisit().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
      setPrimaryIcd9Code(connectors.getCurrentVisit());
      setPrimaryIcd10Code(connectors.getCurrentVisit());
      addIcd9HierarchyCodes(indices, connectors.getCurrentVisit(), generateIcd9Hierarchy(indices, umls, connectors.getCurrentVisit()));
      addIcd10HierarchyCodes(indices, connectors.getCurrentVisit(), generateIcd10Hierarchy(indices, umls, connectors.getCurrentVisit()));
      patient.recordVisit(connectors.getCurrentVisit());
    }
    if (connectors.getCurrentObservation() != null && connectors.getCurrentObservation().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
      patient.recordObservation(connectors.getCurrentObservation());
    }
    if (connectors.getCurrentDepartments() != null && connectors.getCurrentDepartments().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
      patient.recordDepartment(connectors.getCurrentDepartments());
    }
    if (connectors.getCurrentLabs() != null && connectors.getCurrentLabs().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
      patient.recordLabs(connectors.getCurrentLabs());
    }
    if (connectors.getCurrentMeds() != null && connectors.getCurrentMeds().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
      patient.recordAtc(generateAtcToRxNormMap(indices, umls, connectors.getCurrentMeds()));
      patient.recordMeds(connectors.getCurrentMeds());
    }
    if (connectors.getCurrentVitals() != null && connectors.getCurrentVitals().getPatientId() == connectors.getCurrentDemo().getPatientId()) {
      patient.recordVitals(connectors.getCurrentVitals());
    }
    if (connectors.getCurrentTerms() != null && connectors.getCurrentTerms().getPatientId() == connectors.getCurrentDemo().getPatientId() && connectors.getCurrentTerms() != null
        && connectors.getCurrentTerms().getRecords().size() != 0) {
      patient.recordTerms(connectors.getCurrentTerms());
    }
  }

  static DatabaseConnectors getDatabaseConnectorFromFolder(final int maxPatientCnt, final File inputFolder, final ExtractionOptions options) throws IOException, SQLException {
    final FileExtractor files = new FileExtractor(maxPatientCnt == Integer.MAX_VALUE ? Commands.STRIDE_6_PATIENT_CNT : maxPatientCnt);
    files.setVisits(new File(inputFolder, Stride6DatabaseDownload.VISITS_FILE));
    if (options.extractTerms()) {
      files.setTerms(new File(inputFolder, Stride6DatabaseDownload.TERMS_FILE));
    }
    files.setDemographics(new File(inputFolder, Stride6DatabaseDownload.DEMOGRAPHICS_FILE));
    files.setMeds(new File(inputFolder, Stride6DatabaseDownload.MEDS_FILE));
    files.setVitals(new File(inputFolder, Stride6DatabaseDownload.VITALS_FILE));
    files.setVitalsVisits(new File(inputFolder, Stride6DatabaseDownload.VITALS_VISITS_FILE));
    files.setDepartment(new File(inputFolder, Stride6DatabaseDownload.DEPARTMENT_FILE));
    if (new File(inputFolder, Stride6DatabaseDownload.LABS_FILE).exists()) {
      files.setExtendedLabs(new File(inputFolder, Stride6DatabaseDownload.LABS_FILE));
    }
    else {
      files.setShcLabs(new File(inputFolder, Stride6DatabaseDownload.LABS_SHC_FILE));
      files.setLpchLabs(new File(inputFolder, Stride6DatabaseDownload.LABS_LPCH_FILE));
    }
    files.setVisitDx(new File(inputFolder, Stride6DatabaseDownload.VISIT_DX_FILE));
    files.setNotes(new File(inputFolder, Stride6DatabaseDownload.NOTES_FILE));
    if (new File(inputFolder, Stride6DatabaseDownload.OBSERVATION_FILE).exists()) {
      files.setObservation(new File(inputFolder, Stride6DatabaseDownload.OBSERVATION_FILE));
    }
    return new DatabaseConnectors(files, options);
  }

  public static void execute(final String[] args) throws IOException, SQLException, InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException {
    System.out.println("Usage: options input_folder output_folder exclusion_class_name event_eval_class_name [PATIENT_CNT | LIST_OF_PATIENTS_TO_IGNORE]");
    System.out.println();
    System.out.println("options");
    System.out.println(ExtractionOptions.getDescriptions());
    for (int x = 0; x < args.length; x++) {
      System.out.println(x + " = '" + args[x] + "'");
    }
    PatientExclusion evaluator = new DefaultPatientExclusion();
    try {
      final Class<?> clazz = Class.forName(args[3]);
      evaluator = (PatientExclusion) clazz.newInstance();
      System.out.println("Using '" + evaluator.getClass().getName() + "' as the PatientExclusionEvaluator");
    }
    catch (final Exception e) {
      Logging.error("Could not find PatientExclusionEvaluator class '" + args[3] + "'. Using default one...");
    }
    InvalidEventEvaluator eventEvaluator = new DefaultEventEvaluator();
    try {
      final Class<?> clazz = Class.forName(args[4]);
      eventEvaluator = (InvalidEventEvaluator) clazz.newInstance();
      System.out.println("Using '" + eventEvaluator.getClass().getName() + "' as the InvalidEventEvaluator");
    }
    catch (final Exception e) {
      Logging.error("Could not find InvalidEventEvaluator class '" + args[3] + "'. Using default one...");
    }
    final ExtractionOptions options = new ExtractionOptions(args[0]);
    int maxPatientCnt = Integer.MAX_VALUE;
    if (args.length >= 6) {
      try {
        maxPatientCnt = Integer.parseInt(args[5]);
      }
      catch (final Exception e) {
        try {
          final ArrayList<String> patientIds = FileUtils.readFileArrayList(new File(args[5]), Charset.forName("UTF-8"));
          for (int x = 0; x < patientIds.size(); x++) {
            ignorePatientIds.add(Integer.parseInt(patientIds.get(x)));
          }
        }
        catch (final Exception ex) {
          //
        }
      }
    }
    System.out.println("Extracting " + maxPatientCnt + " patients");
    System.out.println("Ignoring " + ignorePatientIds.size() + " patients");
    final File inputFolder = new File(args[1]);
    try {
      if (!CommonWordsFilter.isInitialized()) {
        if (new File(inputFolder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE).exists()) {
          CommonWordsFilter.init(new FileInputStream(new File(inputFolder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE)));
        }
        else {
          CommonWordsFilter.enable(false);
        }
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    final Stride6Extractor extractor = new Stride6Extractor(getDatabaseConnectorFromFolder(maxPatientCnt, inputFolder, options), evaluator, eventEvaluator);
    extractor.extract(inputFolder, new File(args[2]), maxPatientCnt);
  }

  public static void execute(final DatabaseConnectorsInterface connectors, final File inputFolder, final File outputFolder, final PatientExclusion evaluator,
      final InvalidEventEvaluator eventEvaluator) throws IOException, SQLException, InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException {
    System.out.println("Usage: options input_folder output_folder exclusion_class_name event_eval_class_name [PATIENT_CNT | LIST_OF_PATIENTS_TO_IGNORE]");
    System.out.println();
    System.out.println("options");
    System.out.println(ExtractionOptions.getDescriptions());
    final Stride6Extractor extractor = new Stride6Extractor(connectors, evaluator, eventEvaluator);
    extractor.extract(inputFolder, outputFolder, Integer.MAX_VALUE);
  }

  public static void main(final String[] args) throws IOException, SQLException, InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException {
    System.out.println("Version = " + AtlasVersion.getVersion());
    System.out.println("Usage: settings_file");
    System.out.println("Settings file format:");
    System.out.println("INPUT FOLDER=location that contains binary files generated by the database download");
    System.out.println("OUTPUT FOLDER=location into which to generate the output files");
    System.out.println("PATIENT EXCLUSION=class name of the patient exclusion evaluator");
    System.out.println("EVENT EVALUATION=class name of the patient event evaluation");

    final HashMap<String, String> properties = FileUtils.readPropertiesFile(new File(args[0]), Charset.forName("UTF-8"), "=");
    execute(new String[] {"dit", properties.get("INPUT FOLDER"), properties.get("OUTPUT FOLDER"), properties.get("PATIENT EXCLUSION"), properties.get("EVENT EVALUATION"),
        properties.get("PATIENT COUNT")});
  }

}