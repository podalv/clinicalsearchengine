package com.podalv.extractor.stride6.extractors;

import java.io.IOException;
import java.util.ArrayList;

import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.VisitDxRecord;

public class JsonVisitDxExtractor implements Extractor<VisitDxRecord> {

  private final ArrayList<VisitDxRecord> records;
  private int                            pos = 0;

  public JsonVisitDxExtractor(final ArrayList<VisitDxRecord> records) {
    this.records = records;
  }

  @Override
  public void close() throws IOException {
    // There is no closeable object. This method does not need to do anything
  }

  @Override
  public boolean hasNext() {
    return records.size() > 0 && pos < records.size();
  }

  @Override
  public VisitDxRecord next() {
    return records.get(pos++);
  }
}
