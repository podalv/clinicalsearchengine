package com.podalv.extractor.stride6.extractors;

import java.io.IOException;
import java.util.ArrayList;

import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.NoteRecord;

public class JsonNotesExtractor implements Extractor<NoteRecord> {

  private final ArrayList<NoteRecord> records;
  private int                         pos = 0;

  public JsonNotesExtractor(final ArrayList<NoteRecord> records) {
    this.records = records;
  }

  @Override
  public void close() throws IOException {
    // There is no closeable object. This method does not need to do anything
  }

  @Override
  public boolean hasNext() {
    return records.size() > 0 && pos < records.size();
  }

  @Override
  public NoteRecord next() {
    return records.get(pos++);
  }
}
