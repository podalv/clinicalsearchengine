package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.ObservationRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.stride6.Common;

/** An iterator of observation data by patient
*
* @author podalv
*
*/
public class ObservationExtractor implements Extractor<PatientRecord<ObservationRecord>> {

  private static final int                 PATIENT_ID_COLUMN = 1;
  private static final int                 CODE_COLUMN       = 2;
  private static final int                 AGE_COLUMN        = 3;
  private static final int                 YEAR_COLUMN       = 4;
  private static final int                 SAB_COLUMN        = 5;
  private PatientRecord<ObservationRecord> prevRecord        = null;
  private PatientRecord<ObservationRecord> currRecord        = null;
  protected final ExtractionSource         set;

  public ObservationExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<ObservationRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<ObservationRecord>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<ObservationRecord>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(PATIENT_ID_COLUMN);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        final String code = set.getString(CODE_COLUMN);
        if (currentPatientId == patientId) {
          if (code != null && !code.isEmpty()) {
            getMatchingRecord(currentPatientId).add(
                new ObservationRecord(set.getString(CODE_COLUMN), Common.daysToTime(set.getDouble(AGE_COLUMN)), set.getShort(YEAR_COLUMN), set.getString(SAB_COLUMN)));
            if (currRecord != null) {
              break;
            }
          }
        }
        else {
          if (code != null && !code.isEmpty()) {
            getMatchingRecord(patientId).add(
                new ObservationRecord(set.getString(CODE_COLUMN), Common.daysToTime(set.getDouble(AGE_COLUMN)), set.getShort(YEAR_COLUMN), set.getString(SAB_COLUMN)));
          }
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<ObservationRecord> next() {
    final PatientRecord<ObservationRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}