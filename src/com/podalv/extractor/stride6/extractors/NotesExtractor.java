package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.NoteRecord;

public class NotesExtractor implements Extractor<NoteRecord> {

  private static final int         NOTE_ID_COLUMN     = 1;
  private static final int         DOC_NOTE_ID_COLUMN = 2;
  private static final int         AGE_COLUMN         = 3;
  private static final int         YEAR_COLUMN        = 4;
  private NoteRecord               currentRecord;
  protected final ExtractionSource set;

  public NotesExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private void readNext() {
    try {
      if (!set.isAfterLast()) {
        if (set.next()) {
          currentRecord = new NoteRecord(set.getInt(NOTE_ID_COLUMN), set.getString(DOC_NOTE_ID_COLUMN), set.getDouble(AGE_COLUMN), set.getShort(YEAR_COLUMN));
          return;
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    currentRecord = null;
  }

  @Override
  public boolean hasNext() {
    try {
      return currentRecord != null && !set.isAfterLast();
    }
    catch (final Exception e) {
      return false;
    }
  }

  @Override
  public NoteRecord next() {
    final NoteRecord result = currentRecord;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}