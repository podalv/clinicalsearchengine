package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.Common;

/** Vitals from stride6.visits table
 *
 * @author podalv
 *
 */
public class VitalsVisitsExtractor implements Extractor<PatientRecord<VitalsRecord>> {

  public static final int             VITALS_VISITS_PATIENT_ID = 1;
  public static final int             VITALS_VISITS_AGE        = 2;
  public static final int             VITALS_VISITS_BP_S       = 3;
  public static final int             VITALS_VISITS_BP_D       = 4;
  public static final int             VITALS_VISITS_TEMP       = 5;
  public static final int             VITALS_VISITS_PULSE      = 6;
  public static final int             VITALS_VISITS_WEIGHT     = 7;
  public static final int             VITALS_VISITS_HEIGHT     = 8;
  public static final int             VITALS_VISITS_RESP       = 9;
  public static final int             VITALS_VISITS_BMI        = 10;
  public static final int             VITALS_VISITS_BSA        = 11;
  private PatientRecord<VitalsRecord> prevRecord               = null;
  private PatientRecord<VitalsRecord> currRecord               = null;
  protected final ExtractionSource    set;

  public VitalsVisitsExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<VitalsRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<VitalsRecord>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<VitalsRecord>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(VITALS_VISITS_PATIENT_ID);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        if (currentPatientId == patientId) {
          recordValues(getMatchingRecord(currentPatientId));
          if (currRecord != null) {
            break;
          }
        }
        else {
          recordValues(getMatchingRecord(patientId));
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  private void recordValues(final PatientRecord<VitalsRecord> recordToAdd) throws Exception {
    if (set.getShort(VITALS_VISITS_BP_S) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_BP_S, set.getShort(VITALS_VISITS_BP_S)));
    }
    if (set.getShort(VITALS_VISITS_BP_D) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_BP_D, set.getShort(VITALS_VISITS_BP_D)));
    }
    if (set.getDouble(VITALS_VISITS_TEMP) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_TEMPERATURE, set.getDouble(VITALS_VISITS_TEMP)));
    }
    if (set.getShort(VITALS_VISITS_PULSE) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_PULSE, set.getShort(VITALS_VISITS_PULSE)));
    }
    if (set.getDouble(VITALS_VISITS_WEIGHT) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_WEIGHT, set.getDouble(VITALS_VISITS_WEIGHT)));
    }
    if (set.getString(VITALS_VISITS_HEIGHT) != null && set.getString(VITALS_VISITS_HEIGHT).length() != 0) {
      final double height = Common.parseHeightToInches(set.getString(VITALS_VISITS_HEIGHT));
      if (height > 0) {
        recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_HEIGHT, height));
      }
    }
    if (set.getShort(VITALS_VISITS_RESP) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_RESP, set.getShort(VITALS_VISITS_RESP)));
    }
    if (set.getDouble(VITALS_VISITS_BMI) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_BMI, set.getDouble(VITALS_VISITS_BMI)));
    }
    if (set.getDouble(VITALS_VISITS_BSA) != 0) {
      recordToAdd.add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_VISITS_AGE)), Common.VITALS_BSA, set.getDouble(VITALS_VISITS_BSA)));
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<VitalsRecord> next() {
    final PatientRecord<VitalsRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}