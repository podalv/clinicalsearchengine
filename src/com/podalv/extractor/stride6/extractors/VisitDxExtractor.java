package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.VisitDxRecord;

public class VisitDxExtractor implements Extractor<VisitDxRecord> {

  private static final int         VISIT_ID_COLUMN    = 1;
  private static final int         VISIT_DX_ID_COLUMN = 2;
  private static final int         PRIMARY_Y_N_COLUMN = 3;
  private VisitDxRecord            currentRecord      = null;
  protected final ExtractionSource set;

  public VisitDxExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private void readNext() {
    try {
      if (!set.isAfterLast()) {
        set.next();
        currentRecord = new VisitDxRecord(set.getInt(VISIT_ID_COLUMN), set.getInt(VISIT_DX_ID_COLUMN), set.getString(PRIMARY_Y_N_COLUMN));
        return;
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    currentRecord = null;
  }

  @Override
  public boolean hasNext() {
    try {
      return (currentRecord != null && !set.isAfterLast());
    }
    catch (final Exception e) {
      return false;
    }
  }

  @Override
  public VisitDxRecord next() {
    final VisitDxRecord result = currentRecord;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    set.close();
  }

}