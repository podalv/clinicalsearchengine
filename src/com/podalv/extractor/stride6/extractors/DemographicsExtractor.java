package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.DemographicsRecord;

/** Extracts demographics using the Stride6 schema
 *
 * @author podalv
 *
 */
public class DemographicsExtractor implements Extractor<DemographicsRecord> {

  public static final int          PATIENT_ID_COLUMN = 1;
  public static final int          GENDER_COLUMN     = 2;
  public static final int          RACE_COLUMN       = 3;
  public static final int          ETHNICITY_COLUMN  = 4;
  public static final int          DEATH_COLUMN      = 5;
  public static final int          DEATH_YEAR_COLUMN = 6;
  private DemographicsRecord       record            = null;
  private boolean                  beforeStart       = true;
  protected final ExtractionSource set;

  public DemographicsExtractor(final ExtractionSource set) {
    this.set = set;
  }

  @Override
  public boolean hasNext() {
    if (beforeStart) {
      extractNext();
    }
    return record != null;
  }

  @Override
  public DemographicsRecord next() {
    if (beforeStart) {
      extractNext();
    }
    final DemographicsRecord result = record;
    extractNext();
    return result;
  }

  private void extractNext() {
    beforeStart = false;
    try {
      if (set.next()) {
        record = new DemographicsRecord(set.getInt(PATIENT_ID_COLUMN), set.getString(GENDER_COLUMN), set.getString(RACE_COLUMN), set.getString(ETHNICITY_COLUMN), set.getDouble(
            DEATH_COLUMN), set.getShort(DEATH_YEAR_COLUMN));
      }
      else {
        if (set instanceof ResultSet) {
          Database.closeQuery((ResultSet) set);
        }
        record = null;
      }
    }
    catch (final EOFException ee) {
      record = null;
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      record = null;
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }
}
