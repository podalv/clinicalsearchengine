package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.stride6.Common;
import com.podalv.search.datastructures.index.StringEnumIndexBuilder;

/** An iterator of visit data by patient
 *
 * @author podalv
 *
 */
public class VisitExtractor implements Extractor<PatientRecord<VisitRecord>> {

  public static final int            VISIT_ID_COLUMN    = 1;
  public static final int            PATIENT_ID_COLUMN  = 2;
  public static final int            YEAR_COLUMN        = 3;
  public static final int            AGE_COLUMN         = 4;
  public static final int            SRC_VISIT_COLUMN   = 5;
  public static final int            DURATION_COLUMN    = 6;
  public static final int            CODE_COLUMN        = 7;
  public static final int            SAB_COLUMN         = 8;
  public static final int            SOURCE_CODE_COLUMN = 9;
  private PatientRecord<VisitRecord> prevRecord         = null;
  private PatientRecord<VisitRecord> currRecord         = null;
  protected final ExtractionSource   set;

  public VisitExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<VisitRecord> addMatchingRecord(final int currentPatientId, final VisitRecord record) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      prevRecord.add(record);
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      currRecord.add(record);
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<>(currentPatientId);
      currRecord.add(record);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(PATIENT_ID_COLUMN);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        int duration = Common.daysToTime(set.getDouble(DURATION_COLUMN));
        if (duration < 0) {
          duration = 0;
        }
        String visitType = set.getString(SRC_VISIT_COLUMN);
        if (visitType == null || visitType.trim().isEmpty()) {
          visitType = StringEnumIndexBuilder.EMPTY;
        }
        final double age = set.getDouble(AGE_COLUMN);
        if (currentPatientId == patientId) {
          final VisitRecord rec = new VisitRecord(set.getInt(VISIT_ID_COLUMN), set.getShort(YEAR_COLUMN), age == Double.NEGATIVE_INFINITY ? null : Common.daysToTime(age), duration,
              set.getString(SAB_COLUMN), set.getString(CODE_COLUMN), visitType);
          final String srcCode = set.getString(SOURCE_CODE_COLUMN);
          if (set.getString(SAB_COLUMN) != null && set.getString(SAB_COLUMN).equals("DX_ID") && srcCode != null && !srcCode.isEmpty() && !srcCode.equals("null")) {
            rec.setSourceCode(Integer.parseInt(srcCode));
          }
          if (srcCode != null && !srcCode.isEmpty() && !srcCode.equals("null") && srcCode.equals(Common.PRIMARY_CODE)) {
            rec.setPrimary(true);
          }
          addMatchingRecord(currentPatientId, rec);
          if (currRecord != null) {
            break;
          }
        }
        else {
          final VisitRecord rec = new VisitRecord(set.getInt(VISIT_ID_COLUMN), set.getShort(YEAR_COLUMN), age == Double.NEGATIVE_INFINITY ? null : Common.daysToTime(age), duration,
              set.getString(SAB_COLUMN), set.getString(CODE_COLUMN), visitType);
          final String srcCode = set.getString(SOURCE_CODE_COLUMN);
          if (set.getString(SAB_COLUMN) != null && set.getString(SAB_COLUMN).equals("DX_ID") && srcCode != null && !srcCode.isEmpty() && !srcCode.equals("null")) {
            rec.setSourceCode(Integer.parseInt(srcCode));
          }
          if (srcCode != null && !srcCode.isEmpty() && !srcCode.equals("null") && srcCode.equals(Common.PRIMARY_CODE)) {
            rec.setPrimary(true);
          }
          addMatchingRecord(patientId, rec);
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<VisitRecord> next() {
    final PatientRecord<VisitRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}