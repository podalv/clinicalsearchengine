package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.Common;

public class VitalsExtractor implements Extractor<PatientRecord<VitalsRecord>> {

  public static final int             VITALS_PATIENT_ID  = 1;
  public static final int             VITALS_MEAS_ID     = 2;
  public static final int             VITALS_AGE         = 3;
  public static final int             VITALS_DESCRIPTION = 4;
  public static final int             VITALS_VALUE       = 5;
  public static final int             VITALS_YEAR        = 6;
  private PatientRecord<VitalsRecord> prevRecord         = null;
  private PatientRecord<VitalsRecord> currRecord         = null;
  protected final ExtractionSource    set;

  public VitalsExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<VitalsRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<VitalsRecord>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<VitalsRecord>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(VITALS_PATIENT_ID);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        final String description = set.getString(VITALS_DESCRIPTION);
        if (currentPatientId == patientId) {
          if (set.getDouble(VITALS_VALUE) > 0 && description != null) {
            getMatchingRecord(currentPatientId).add(
                new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_AGE)), description, set.getDouble(VITALS_VALUE), set.getShort(VITALS_YEAR)));
            if (currRecord != null) {
              break;
            }
          }
        }
        else {
          if (set.getDouble(VITALS_VALUE) > 0 && description != null) {
            getMatchingRecord(patientId).add(new VitalsRecord(Common.daysToTime(set.getDouble(VITALS_AGE)), description, set.getDouble(VITALS_VALUE), set.getShort(VITALS_YEAR)));
          }
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<VitalsRecord> next() {
    final PatientRecord<VitalsRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}