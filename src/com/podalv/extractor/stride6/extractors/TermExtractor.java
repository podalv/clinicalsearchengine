package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.CommonWordsFilter;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.stride6.Common;

/** An iterator of terms data by patient
 *
 * @author podalv
 *
 */
public class TermExtractor implements Extractor<PatientRecord<TermRecord>> {

  public static final int           PATIENT_ID_COLUMN = 1;
  public static final int           NID_COLUMN        = 2;
  public static final int           TID_COLUMN        = 3;
  public static final int           NEGATED_FH_COLUMN = 4;
  public static final int           NEGATED_COLUMN    = 4;
  public static final int           FH_COLUMN         = 5;
  private PatientRecord<TermRecord> prevRecord        = null;
  private PatientRecord<TermRecord> currRecord        = null;
  protected final ExtractionSource  set;

  public TermExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<TermRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(PATIENT_ID_COLUMN);
        final int tid = set.getInt(TID_COLUMN);
        final int nid = set.getInt(NID_COLUMN);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        if (currentPatientId == patientId) {
          if (!CommonWordsFilter.isCommonWord(tid)) {
            final byte[] negatedFh = Common.decodeNegatedFamilyHistory(set.getByte(NEGATED_FH_COLUMN));
            getMatchingRecord(currentPatientId).add(new TermRecord(tid, nid, negatedFh[0], negatedFh[1], -1));
            if (currRecord != null) {
              break;
            }
          }
        }
        else {
          if (!CommonWordsFilter.isCommonWord(tid)) {
            final byte[] negatedFh = Common.decodeNegatedFamilyHistory(set.getByte(NEGATED_FH_COLUMN));
            getMatchingRecord(patientId).add(new TermRecord(tid, nid, negatedFh[0], negatedFh[1], -1));
          }
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<TermRecord> next() {
    final PatientRecord<TermRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}
