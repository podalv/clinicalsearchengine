package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.stride6.Common;

public class MedsExtractor implements Extractor<PatientRecord<MedRecord>> {

  public static final int          PATIENT_ID_COLUMN   = 1;
  public static final int          AGE_COLUMN          = 2;
  public static final int          YEAR_COLUMN         = 3;
  public static final int          ORDER_STATUS_COLUMN = 4;
  public static final int          RX_CUI_COLUMN       = 5;
  private static final int         END_AGE_COLUMN      = 6;
  private static final int         ROUTE_COLUMN        = 7;

  private PatientRecord<MedRecord> prevRecord          = null;
  private PatientRecord<MedRecord> currRecord          = null;
  protected final ExtractionSource set;

  public MedsExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<MedRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<MedRecord>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<MedRecord>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      stride6Extraction();
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  private void stride6Extraction() throws Exception {
    int currentPatientId = -1;
    while (set.next()) {
      final int patientId = set.getInt(PATIENT_ID_COLUMN);
      if (currentPatientId == -1) {
        currentPatientId = patientId;
      }
      int end = Common.daysToTime(set.getDouble(END_AGE_COLUMN));
      final int start = Common.daysToTime(set.getDouble(AGE_COLUMN));
      if (end < start) {
        end = start;
      }
      if (currentPatientId == patientId) {
        if (set.getInt(RX_CUI_COLUMN) > 0) {
          getMatchingRecord(currentPatientId).add(
              new MedRecord(start, end, set.getShort(YEAR_COLUMN), set.getString(ORDER_STATUS_COLUMN), set.getInt(RX_CUI_COLUMN), set.getString(ROUTE_COLUMN)));
        }
        if (currRecord != null) {
          break;
        }
      }
      else {
        if (set.getInt(RX_CUI_COLUMN) > 0) {
          getMatchingRecord(patientId).add(
              new MedRecord(start, end, set.getShort(YEAR_COLUMN), set.getString(ORDER_STATUS_COLUMN), set.getInt(RX_CUI_COLUMN), set.getString(ROUTE_COLUMN)));
        }
        if (currRecord != null) {
          break;
        }
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<MedRecord> next() {
    final PatientRecord<MedRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}