package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.stride6.Common;

public class ShcLabsExtractor implements Extractor<PatientRecord<LabsRecord>> {

  private PatientRecord<LabsRecord> prevRecord = null;
  private PatientRecord<LabsRecord> currRecord = null;
  protected final ExtractionSource  set;

  public ShcLabsExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<LabsRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<LabsRecord>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<LabsRecord>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(1);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        if (currentPatientId == patientId) {
          getMatchingRecord(currentPatientId).add(
              new LabsRecord(set.getString(3), Common.daysToTime(set.getDouble(2)), set.getDouble(4), set.getDouble(5), set.getDouble(6), set.getString(7)));
          if (currRecord != null) {
            break;
          }
        }
        else {
          getMatchingRecord(patientId).add(
              new LabsRecord(set.getString(3), Common.daysToTime(set.getDouble(2)), set.getDouble(4), set.getDouble(5), set.getDouble(6), set.getString(7)));
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<LabsRecord> next() {
    final PatientRecord<LabsRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }
}