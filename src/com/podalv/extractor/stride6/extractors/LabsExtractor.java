package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;

/** Extractor of OHDSI labs
 *
 * @author podalv
 *
 */
public class LabsExtractor implements Extractor<PatientRecord<LabsRecord>> {

  private PatientRecord<LabsRecord> prevRecord = null;
  private PatientRecord<LabsRecord> currRecord = null;
  protected final ExtractionSource  set;

  public LabsExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<LabsRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(1);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        final String value = set.getString(5);
        if (currentPatientId == patientId) {
          if ((value != null && !value.isEmpty()) || (set.getDouble(6) > Stride6DatabaseDownload.UNDEFINED)) {
            if (set.getDouble(6) > Stride6DatabaseDownload.UNDEFINED) {
              getMatchingRecord(currentPatientId).add(new LabsRecord(set.getString(4), Common.daysToTime(set.getDouble(2)), set.getShort(3), value, set.getDouble(6)));
            }
            else {
              getMatchingRecord(currentPatientId).add(new LabsRecord(set.getString(4), Common.daysToTime(set.getDouble(2)), set.getShort(3), value));
            }
            if (currRecord != null) {
              break;
            }
          }
        }
        else {
          if (set.getDouble(6) > Stride6DatabaseDownload.UNDEFINED) {
            getMatchingRecord(patientId).add(new LabsRecord(set.getString(4), Common.daysToTime(set.getDouble(2)), set.getShort(3), value, set.getDouble(6)));
          }
          else {
            getMatchingRecord(patientId).add(new LabsRecord(set.getString(4), Common.daysToTime(set.getDouble(2)), set.getShort(3), value));
          }
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<LabsRecord> next() {
    final PatientRecord<LabsRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }
}