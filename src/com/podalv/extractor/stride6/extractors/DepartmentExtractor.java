package com.podalv.extractor.stride6.extractors;

import java.io.EOFException;
import java.io.IOException;
import java.sql.ResultSet;

import com.podalv.db.Database;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.DepartmentRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.stride6.Common;

public class DepartmentExtractor implements Extractor<PatientRecord<DepartmentRecord>> {

  private static final int                PATIENT_ID_COLUMN = 1;
  private static final int                CODE_COLUMN       = 4;
  private static final int                AGE_COLUMN        = 2;
  private static final int                YEAR_COLUMN       = 3;
  private PatientRecord<DepartmentRecord> prevRecord        = null;
  private PatientRecord<DepartmentRecord> currRecord        = null;
  protected final ExtractionSource        set;

  public DepartmentExtractor(final ExtractionSource set) {
    this.set = set;
    readNext();
  }

  private PatientRecord<DepartmentRecord> getMatchingRecord(final int currentPatientId) {
    if (prevRecord == null) {
      prevRecord = new PatientRecord<>(currentPatientId);
    }
    if (prevRecord.getPatientId() == currentPatientId) {
      return prevRecord;
    }
    else if (currRecord != null && currRecord.getPatientId() == currentPatientId) {
      return currRecord;
    }
    else {
      currRecord = new PatientRecord<>(currentPatientId);
      return currRecord;
    }
  }

  private void readNext() {
    try {
      if (set.isAfterLast() && prevRecord == null) {
        return;
      }
      int currentPatientId = -1;
      while (set.next()) {
        final int patientId = set.getInt(PATIENT_ID_COLUMN);
        if (currentPatientId == -1) {
          currentPatientId = patientId;
        }
        final String code = set.getString(CODE_COLUMN);
        final double age = set.getDouble(AGE_COLUMN);
        if (currentPatientId == patientId) {
          if (code != null && !code.isEmpty()) {
            getMatchingRecord(currentPatientId).add(new DepartmentRecord(set.getString(CODE_COLUMN), age == Double.NEGATIVE_INFINITY ? null
                : Common.daysToTime(set.getDouble(AGE_COLUMN)), set.getShort(YEAR_COLUMN)));
            if (currRecord != null) {
              break;
            }
          }
        }
        else {
          if (code != null && !code.isEmpty()) {
            getMatchingRecord(patientId).add(new DepartmentRecord(set.getString(CODE_COLUMN), age == Double.NEGATIVE_INFINITY ? null : Common.daysToTime(set.getDouble(AGE_COLUMN)),
                set.getShort(YEAR_COLUMN)));
          }
          if (currRecord != null) {
            break;
          }
        }
      }
    }
    catch (final EOFException ee) {
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      if (set instanceof ResultSet) {
        Database.closeQuery((ResultSet) set);
      }
    }
  }

  @Override
  public boolean hasNext() {
    return prevRecord != null;
  }

  @Override
  public PatientRecord<DepartmentRecord> next() {
    final PatientRecord<DepartmentRecord> result = prevRecord;
    prevRecord = currRecord;
    currRecord = null;
    readNext();
    return result;
  }

  @Override
  public void close() throws IOException {
    try {
      set.close();
    }
    catch (final Exception e) {
      throw new IOException("Error closing connection");
    }
  }

}