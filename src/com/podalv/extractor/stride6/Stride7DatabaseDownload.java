package com.podalv.extractor.stride6;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.function.Function;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.datastructures.BinaryFileExtractionSource;
import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;
import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.datastructures.ExtractionSource;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.stride6.datastructures.DepartmentSource;
import com.podalv.extractor.stride6.datastructures.LabSource;
import com.podalv.extractor.stride6.datastructures.LabSourceCulture;
import com.podalv.extractor.stride6.datastructures.MedicationSource;
import com.podalv.extractor.stride6.datastructures.Stride7Source;
import com.podalv.extractor.stride6.datastructures.VisitSourceAdmitDe;
import com.podalv.extractor.stride6.datastructures.VisitSourceDxPatEnc;
import com.podalv.extractor.stride6.datastructures.VisitSourceHl7;
import com.podalv.extractor.stride6.datastructures.VisitSourcePxCpt;
import com.podalv.extractor.stride6.datastructures.VisitSourcePxIcdHspDe;
import com.podalv.extractor.stride6.datastructures.VisitType;
import com.podalv.extractor.stride6.extractors.DemographicsExtractor;
import com.podalv.extractor.stride6.extractors.TermExtractor;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.map.IntKeyOpenHashMap;
import com.podalv.maps.primitive.map.LongKeyOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.IntKeyStringMap;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

public class Stride7DatabaseDownload {

  public static int          MAX_PATIENT_CNT                           = 30000000;

  public static final String PID_QUERY                                 = " ";
  public static final String AND_PID_QUERY                             = "  ";

  public static final String TERMINOLOGY_TABLE                         = "terminology4";

  public static final String CULTURE_DICTIONARY_OK_FLAG                = "OK";

  public static final String TERM_DICTIONARY                           = "SELECT tid, str FROM " + TERMINOLOGY_TABLE + ".str2tid group by tid";
  public static final String TERMS                                     = "SELECT nid, tid, negated, familyHistory FROM stride7.term_mentions";

  public static final String DEMOGRAPHICS_7                            = "SELECT patient_id, gender, race, ethnicity, FLOOR(age_at_death_in_days), YEAR(death_date) FROM stride7.demographics "
      + PID_QUERY + "ORDER BY patient_id";
  public static final String VITALS_VISITS_7                           = "SELECT patient_id, FLOOR(age_at_contact_in_days), bp_systolic, bp_diastolic, temperature, pulse, weight_in_lbs, height, respirations, bmi, bsa, YEAR(contact_date) FROM stride7.LPCH_visit_de WHERE (bp_systolic <> 0 OR bp_diastolic <> 0 OR temperature <> 0 OR pulse <> 0 OR weight_in_lbs <> 0 OR height <> \"\" OR respirations <> 0 OR bmi <> 0 OR bsa <> 0)"
      + AND_PID_QUERY
      + " UNION SELECT patient_id, age_at_contact_in_days, bp_systolic, bp_diastolic, temperature, pulse, weight_in_lbs, height, respirations, bmi, bsa, YEAR(contact_date) FROM stride7.SHC_visit_de WHERE (bp_systolic <> 0 OR bp_diastolic <> 0 OR temperature <> 0 OR pulse <> 0 OR weight_in_lbs <> 0 OR height <> \"\" OR respirations <> 0 OR bmi <> 0 OR bsa <> 0) "
      + AND_PID_QUERY + "order by patient_id";

  public static final String MED_ID_LPCH_EPIC                          = "SELECT medication_id, rxcui FROM user_podalv.dictionary_medications_LPCH";
  public static final String MED_STATUS_LPCH                           = "SELECT order_status_c, name FROM stride7_dictionaries.lpch_zc_order_status  where name <> ''";
  public static final String MED_ID_SHC_EPIC                           = "SELECT medication_id, rxcui FROM user_podalv.dictionary_medications_SHC";
  public static final String MED_STATUS_SHC                            = "SELECT order_status_c, name FROM stride7_dictionaries.shc_zc_order_status where name <> ''";
  public static final String MED_ID_HL7                                = "SELECT medication_id, rxcui FROM user_podalv.dictionary_medications_hl7";
  public static final String MED_CLASS_SHC                             = "SELECT order_class_c, name FROM stride7_dictionaries.shc_zc_order_class";
  public static final String MED_CLASS_LPCH                            = "SELECT order_class_c, name FROM stride7_dictionaries.lpch_zc_order_class";

  public static final String MED_LPCH_DE                               = "SELECT patient_id, medication_id, FLOOR(age_at_order_in_days), YEAR(order_time), order_status_c, med_route_c, med_id, FLOOR(age_at_start_in_days), FLOOR(age_at_end_in_days), FLOOR(age_at_discon_in_days), ordering_mode_c, order_class_c FROM stride7.LPCH_med_de "
      + PID_QUERY + "order by patient_id";
  public static final String MED_LPCH_HL7                              = "SELECT patient_id, hl7_medication_id, FLOOR(age_at_start_in_days), YEAR(start_time), order_status, route_id, med_id, FLOOR(age_at_start_in_days), FLOOR(age_at_end_in_days), FLOOR(age_at_end_in_days), -1, -1 FROM stride7.LPCH_med_hl7_de "
      + PID_QUERY + "order by patient_id";
  public static final String MED_SHC_DE                                = "SELECT patient_id, medication_id, FLOOR(age_at_order_in_days), YEAR(order_time), order_status_c, med_route_c, med_id, FLOOR(age_at_start_in_days), FLOOR(age_at_end_in_days), FLOOR(age_at_discon_in_days), ordering_mode_c, order_class_c FROM stride7.SHC_med_de "
      + PID_QUERY + "order by patient_id";
  public static final String MED_SHC_HL7                               = "SELECT patient_id, hl7_medication_id, FLOOR(age_at_start_in_days), YEAR(start_time), order_status, route_id, med_id, FLOOR(age_at_start_in_days), FLOOR(age_at_end_in_days), FLOOR(age_at_end_in_days), -1, -1 FROM stride7.SHC_med_hl7_de "
      + PID_QUERY + "order by patient_id";
  // Both LPCH and SHC routes are mapped from SHC
  public static final String MED_ROUTE_LPCH                            = "SELECT med_route_c, accom_reason_c FROM stride7_dictionaries.shc_zc_admin_route";
  public static final String MED_ROUTE_SHC                             = "SELECT med_route_c, accom_reason_c FROM stride7_dictionaries.shc_zc_admin_route";

  public static final String MED_ADMIN_SHC                             = "SELECT patient_id,med_id,FLOOR(age_at_taken_in_days) FROM stride7.SHC_med_admin_de";
  public static final String MED_ADMIN_LPCH                            = "SELECT patient_id,med_id,FLOOR(age_at_taken_in_days) FROM stride7.LPCH_med_admin_de";

  public static final String DICTIONARY_DIAGNOSES_LPCH                 = "SELECT dx_id, current_icd9_list FROM stride7.dictionary_diagnoses_LPCH";
  public static final String DICTIONARY_DIAGNOSES_SHC                  = "SELECT dx_id, current_icd9_list FROM stride7.dictionary_diagnoses_SHC";

  public static final String DICTIONARY_LOINC                          = "SELECT LOINC_CODE, long_common_name FROM stride7.dictionary_loinc";

  public static final String DICTIONARY_DIAGNOSES_LPCH_ICD10           = "SELECT dx_id, current_icd10_list FROM stride7.dictionary_diagnoses_LPCH";
  public static final String DICTIONARY_DIAGNOSES_SHC_ICD10            = "SELECT dx_id, current_icd10_list FROM stride7.dictionary_diagnoses_SHC";

  public static final String DICTIONARY_DIAGNOSES_LPCH_PRIMARY_CODE_9  = "SELECT dx_id, ref_bill_code FROM stride7.dictionary_diagnoses_LPCH where ref_bill_code_set_c = 1";
  public static final String DICTIONARY_DIAGNOSES_SHC_PRIMARY_CODE_9   = "SELECT dx_id, ref_bill_code FROM stride7.dictionary_diagnoses_SHC where ref_bill_code_set_c = 1";

  public static final String DICTIONARY_DIAGNOSES_LPCH_PRIMARY_CODE_10 = "SELECT dx_id, ref_bill_code FROM stride7.dictionary_diagnoses_LPCH where ref_bill_code_set_c = 2";
  public static final String DICTIONARY_DIAGNOSES_SHC_PRIMARY_CODE_10  = "SELECT dx_id, ref_bill_code FROM stride7.dictionary_diagnoses_SHC where ref_bill_code_set_c = 2";

  public static final String DICTIONARY_DEPARTMENT_LPCH                = "SELECT department_id, department_name FROM stride7.dictionary_department_LPCH";
  public static final String DICTIONARY_DEPARTMENT_SHC                 = "SELECT department_id, department_name FROM stride7.dictionary_department_SHC";

  public static final String DICTIONARY_VISIT_TYPE_LPCH                = "SELECT disp_enc_type_c,name FROM stride7_dictionaries.lpch_zc_disp_enc_type";
  public static final String DICTIONARY_VISIT_TYPE_SHC                 = "SELECT disp_enc_type_c,name FROM stride7_dictionaries.shc_zc_disp_enc_type";

  public static final String SHC_VISIT_DE                              = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.SHC_visit_de "
      + PID_QUERY + "order by patient_id";
  public static final String LPCH_VISIT_DE                             = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.LPCH_visit_de "
      + PID_QUERY + "order by patient_id";

  public static final String SHC_VISIT_DE_VISIT_TYPE                   = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), enc_type_c, FLOOR(age_at_hosp_admsn_in_days), FLOOR(age_at_hosp_disch_in_days) FROM stride7.SHC_visit_de "
      + PID_QUERY + "order by patient_id";
  public static final String LPCH_VISIT_DE_VISIT_TYPE                  = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), enc_type_c, FLOOR(age_at_hosp_admsn_in_days), FLOOR(age_at_hosp_disch_in_days) FROM stride7.LPCH_visit_de "
      + PID_QUERY + "order by patient_id";

  public static final String SHC_VISIT_APPT_DE                         = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.SHC_visit_appt_de "
      + PID_QUERY + "order by patient_id";
  public static final String LPCH_VISIT_APPT_DE                        = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.LPCH_visit_appt_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_DX_ADMIT_DE                          = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.LPCH_dx_admit_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_DX_ADMIT_DE                           = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.SHC_dx_admit_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_DX_EXTINJURY_DE                      = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date) FROM stride7.LPCH_dx_extinjury_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_DX_EXTINJURY_DE                       = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date) FROM stride7.SHC_dx_extinjury_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_DX_HL7_DE                            = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(visit_duration), code, visit_type, dx_px_type_text, YEAR(contact_date) FROM stride7.LPCH_dx_hl7_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_DX_HL7_DE                             = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(visit_duration), code, visit_type, dx_px_type_text, YEAR(contact_date) FROM stride7.SHC_dx_hl7_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_DX_HSP_ACCT_DE                       = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.LPCH_dx_hsp_acct_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_DX_HSP_ACCT_DE                        = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.SHC_dx_hsp_acct_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_DX_PAT_ENC_DE                        = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date), primary_dx_yn FROM stride7.LPCH_dx_pat_enc_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_DX_PAT_ENC_DE                         = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date), primary_dx_yn FROM stride7.SHC_dx_pat_enc_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_DX_PROB_LIST_DE                      = "SELECT patient_id, FLOOR(age_at_noted_in_days), FLOOR(age_at_noted_in_days), dx_id, YEAR(noted_date), principal_pl_yn FROM stride7.LPCH_dx_prob_list_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_DX_PROB_LIST_DE                       = "SELECT patient_id, FLOOR(age_at_noted_in_days), FLOOR(age_at_noted_in_days), dx_id, YEAR(noted_date), principal_pl_yn FROM stride7.SHC_dx_prob_list_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_DE                            = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.LPCH_px_cpt_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_DE                             = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.SHC_px_cpt_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_HSP_DE                        = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.LPCH_px_cpt_hsp_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_HSP_DE                         = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.SHC_px_cpt_hsp_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE                       = "SELECT patient_id, FLOOR(age_at_service_in_days), YEAR(contact_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.LPCH_px_cpt_prov_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE                        = "SELECT patient_id, FLOOR(age_at_service_in_days), YEAR(contact_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.SHC_px_cpt_prov_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE_ICD1                  = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), primary_dx_id, YEAR(contact_date), 'Y' FROM stride7.LPCH_px_cpt_prov_de where primary_dx_id != 0 "
      + AND_PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE_ICD1                   = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), primary_dx_id, YEAR(contact_date), 'Y' FROM stride7.SHC_px_cpt_prov_de where primary_dx_id != 0 "
      + AND_PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE_ICD2                  = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_two_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_two_id != 0 "
      + AND_PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE_ICD2                   = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_two_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_two_id != 0 "
      + AND_PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE_ICD3                  = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_three_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_three_id != 0 "
      + AND_PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE_ICD3                   = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_three_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_three_id != 0 "
      + AND_PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE_ICD4                  = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_four_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_four_id != 0 "
      + AND_PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE_ICD4                   = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_four_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_four_id != 0 "
      + AND_PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE_ICD5                  = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_five_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_five_id != 0 "
      + AND_PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE_ICD5                   = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_five_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_five_id != 0 "
      + AND_PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_CPT_PROV_DE_ICD6                  = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_six_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_six_id != 0 "
      + AND_PID_QUERY + "order by patient_id";
  public static final String SHC_PX_CPT_PROV_DE_ICD6                   = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_six_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_six_id != 0 "
      + AND_PID_QUERY + "order by patient_id";

  public static final String LPCH_PX_ICD_DE                            = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.LPCH_px_icd_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_PX_ICD_DE                             = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.SHC_px_icd_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_LAB_DE                               = "SELECT patient_id, FLOOR(age_at_lab_in_days), YEAR(lab_time), component_text, labvalue, LOINC_CODE FROM stride7.LPCH_lab_de "
      + PID_QUERY + "order by patient_id";

  public static final String LPCH_LAB_EPIC_DE                          = "SELECT patient_id, FLOOR(age_at_taken_in_days), YEAR(taken_time), lab_name, ord_value, LOINC_CODE FROM stride7.LPCH_lab_epic_de "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_LAB_EPIC_DE                           = "SELECT patient_id, FLOOR(age_at_taken_in_days), YEAR(taken_time), lab_name, ord_value, LOINC_CODE FROM stride7.SHC_lab_epic_de "
      + PID_QUERY + "order by patient_id";

  public static final String SHC_LAB_CULTURE                           = "SELECT stride7.SHC_lab_epic_comment_de.patient_id, FLOOR(stride7.SHC_lab_epic_de.age_at_taken_in_days), YEAR(stride7.SHC_lab_epic_de.taken_time), stride7.SHC_lab_epic_de.group_lab_name, stride7.SHC_lab_epic_comment_de.cmt_results FROM stride7.SHC_lab_epic_comment_de join stride7.SHC_lab_epic_de on (stride7.SHC_lab_epic_comment_de.order_proc_id = stride7.SHC_lab_epic_de.order_proc_id) where stride7.SHC_lab_epic_de.group_lab_name like \"%culture%\" order by patient_id";

  public static final String CULTURE_DICTIONARY                        = "SELECT cmt_result, mapped_name, flag from user_podalv.lab_cultures_map";

  public static final String CULTURE_NAME_DICTIONARY                   = "SELECT group_lab_name, mapped FROM user_podalv.lab_cultures_lab_names_map";

  public static final String LPCH_PX_ICD_HSP_DE                        = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), ref_bill_code, ref_bill_code_set_c FROM stride7.LPCH_px_icd_hsp_de LEFT OUTER join stride7_dictionaries.lpch_cl_icd_px on (stride7.LPCH_px_icd_hsp_de.icd_px_id = stride7_dictionaries.lpch_cl_icd_px.icd_px_id) "
      + PID_QUERY + "order by patient_id";
  public static final String SHC_PX_ICD_HSP_DE                         = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), ref_bill_code, ref_bill_code_set_c FROM stride7.SHC_px_icd_hsp_de LEFT OUTER join stride7_dictionaries.shc_cl_icd_px on (stride7.SHC_px_icd_hsp_de.icd_px_id = stride7_dictionaries.shc_cl_icd_px.icd_px_id) "
      + PID_QUERY + "order by patient_id";

  public static final String NOTES_CLINICAL                            = "SELECT note_id, note_type, age_at_note_in_days, YEAR(note_date), patient_id FROM stride7.note_clinical_meta_de";
  public static final String NOTES_RADIOLOGY                           = "SELECT note_id, note_type, age_at_note_in_days, YEAR(note_date), patient_id FROM stride7.note_radiology_meta_de";
  public static final String NOTES_PATHOLOGY                           = "SELECT note_id, note_type, age_at_note_in_days, YEAR(note_date), patient_id FROM stride7.note_pathology_meta_de";

  public static int[] extractHistoricalClasses(final ResultSet set) throws SQLException {
    final IntOpenHashSet result = new IntOpenHashSet();
    while (set.next()) {
      if (set.getString(2).toLowerCase().startsWith("historical") || set.getString(2).equalsIgnoreCase("patient supplied")) {
        result.add(set.getInt(1));
      }
    }
    set.close();
    return result.toArray();
  }

  public static void execute(final String[] args, final ConnectionSettings settings) throws SQLException {
    Database db = null;
    ResultSet set = null;
    if (args[0].indexOf('d') != -1) {
      try {
        db = Database.createInstance(settings);
        set = db.queryStreaming(DEMOGRAPHICS_7);
        extractDemographics(set, new File(new File(args[1]), Stride6DatabaseDownload.DEMOGRAPHICS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting demographics");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('v') != -1) {
      try {
        final BinaryFileWriter writer = new BinaryFileWriter(new File(new File(args[1]), Stride6DatabaseDownload.VISIT_DX_FILE), Commands.VISIT_DX_STRUCTURE);
        writer.write(1, 1, "Y");
        writer.close();

        final IntKeyObjectMap<HashSet<String>> lpchDxMap = extractDxDictionary(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_LPCH));
        final IntKeyObjectMap<HashSet<String>> shcDxMap = extractDxDictionary(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_SHC));
        final IntKeyObjectMap<HashSet<String>> lpchDxIcd10Map = extractDxDictionary(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_LPCH_ICD10));
        final IntKeyObjectMap<HashSet<String>> shcDxIcd10Map = extractDxDictionary(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_SHC_ICD10));
        final IntKeyStringMap lpchPrimaryCode9Map = extractDxPrimaryCode(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_LPCH_PRIMARY_CODE_9));
        final IntKeyStringMap lpchVisitTypeMap = extractDxPrimaryCode(Database.createInstance(settings).query(DICTIONARY_VISIT_TYPE_LPCH));
        final IntKeyStringMap shcVisitTypeMap = extractDxPrimaryCode(Database.createInstance(settings).query(DICTIONARY_VISIT_TYPE_SHC));

        final IntKeyStringMap shcPrimaryCode9Map = extractDxPrimaryCode(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_SHC_PRIMARY_CODE_9));
        final IntKeyStringMap lpchPrimaryCode10Map = extractDxPrimaryCode(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_LPCH_PRIMARY_CODE_10));
        final IntKeyStringMap shcPrimaryCode10Map = extractDxPrimaryCode(Database.createInstance(settings).query(DICTIONARY_DIAGNOSES_SHC_PRIMARY_CODE_10));
        final Stride7Source[] sources = new Stride7Source[36];
        sources[0] = new VisitSourceAdmitDe(Database.createInstance(settings).queryStreaming(LPCH_DX_ADMIT_DE), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[1] = new VisitSourceAdmitDe(Database.createInstance(settings).queryStreaming(SHC_DX_ADMIT_DE), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map, shcPrimaryCode10Map);
        sources[2] = new VisitSourceAdmitDe(Database.createInstance(settings).queryStreaming(LPCH_DX_EXTINJURY_DE), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[3] = new VisitSourceAdmitDe(Database.createInstance(settings).queryStreaming(SHC_DX_EXTINJURY_DE), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[4] = new VisitSourceHl7(Database.createInstance(settings).queryStreaming(LPCH_DX_HL7_DE), 1);
        sources[5] = new VisitSourceHl7(Database.createInstance(settings).queryStreaming(SHC_DX_HL7_DE), 1);
        sources[6] = new VisitSourceAdmitDe(Database.createInstance(settings).queryStreaming(LPCH_DX_HSP_ACCT_DE), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[7] = new VisitSourceAdmitDe(Database.createInstance(settings).queryStreaming(SHC_DX_HSP_ACCT_DE), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[8] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_DX_PAT_ENC_DE), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[9] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_DX_PAT_ENC_DE), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[10] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_DX_PROB_LIST_DE), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[11] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_DX_PROB_LIST_DE), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[12] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_DE), 1);
        sources[13] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_DE), 1);
        sources[14] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_HSP_DE), 1);
        sources[15] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_HSP_DE), 1);
        sources[16] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE), 1);
        sources[17] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE), 1);
        sources[18] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(LPCH_PX_ICD_DE), 1);
        sources[19] = new VisitSourcePxCpt(Database.createInstance(settings).queryStreaming(SHC_PX_ICD_DE), 1);

        sources[20] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE_ICD1), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[21] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE_ICD2), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[22] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE_ICD3), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[23] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE_ICD4), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[24] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE_ICD5), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);
        sources[25] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(LPCH_PX_CPT_PROV_DE_ICD6), 1, lpchDxMap, lpchDxIcd10Map, lpchPrimaryCode9Map,
            lpchPrimaryCode10Map);

        sources[26] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE_ICD1), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[27] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE_ICD2), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[28] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE_ICD3), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[29] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE_ICD4), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[30] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE_ICD5), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);
        sources[31] = new VisitSourceDxPatEnc(Database.createInstance(settings).queryStreaming(SHC_PX_CPT_PROV_DE_ICD6), 1, shcDxMap, shcDxIcd10Map, shcPrimaryCode9Map,
            shcPrimaryCode10Map);

        sources[32] = new VisitSourcePxIcdHspDe(Database.createInstance(settings).queryStreaming(LPCH_PX_ICD_HSP_DE), 1);
        sources[33] = new VisitSourcePxIcdHspDe(Database.createInstance(settings).queryStreaming(SHC_PX_ICD_HSP_DE), 1);

        sources[34] = new VisitType(Database.createInstance(settings).queryStreaming(SHC_VISIT_DE_VISIT_TYPE), 1, shcVisitTypeMap);
        sources[35] = new VisitType(Database.createInstance(settings).queryStreaming(LPCH_VISIT_DE_VISIT_TYPE), 1, lpchVisitTypeMap);

        extractVisits(sources, new File(new File(args[1]), Stride6DatabaseDownload.VISITS_FILE), Commands.VISITS_STRUCTURE);

        for (int x = 0; x < sources.length; x++) {
          System.out.println("VISITS[" + x + "] " + sources[x].getRejectedCnt() + " / " + sources[x].getProcessedCnt());
        }
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting visits");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('l') != -1) {
      final HashMap<String, String> cultureMap = extractCulturesDictionary(Database.createInstance(settings).query(CULTURE_DICTIONARY));
      final HashMap<String, String> cultureNameMap = extractLoincDictionary(Database.createInstance(settings).query(CULTURE_NAME_DICTIONARY));
      final HashMap<String, HashSet<String>> map = new HashMap<>();
      try {
        calculateDistinctTexts(Database.createInstance(settings).queryStreaming("SELECT ord_value, LOINC_CODE, lab_name FROM stride7.LPCH_lab_epic_de"), map);
        calculateDistinctTexts(Database.createInstance(settings).queryStreaming("SELECT ord_value, LOINC_CODE, lab_name FROM stride7.SHC_lab_epic_de"), map);
        calculateDistinctTexts(Database.createInstance(settings).queryStreaming("SELECT meas_value, LOINC_CODE, gp2_disp_name FROM stride7.SHC_lab_outside_de"), map);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }

      final HashSet<String> ignoreLabTextValues = new HashSet<>();
      final Iterator<Entry<String, HashSet<String>>> iterator = map.entrySet().iterator();
      while (iterator.hasNext()) {
        final Entry<String, HashSet<String>> entry = iterator.next();
        if (entry.getValue().size() > 20) {
          ignoreLabTextValues.add(entry.getKey());
          System.out.println("Ignoring text values of lab '" + entry.getKey() + "' " + entry.getValue().size());
        }
      }

      try {
        final BinaryFileWriter labNamesOut = new BinaryFileWriter(new File(new File(args[1]), Stride6DatabaseDownload.LABS_COMPONENT_FILE), Commands.LABS_COMPONENT_STRUCTURE);
        final Stride7Source[] sources = new Stride7Source[4];
        final HashMap<String, String> uniqueLabNames = new HashMap<>();
        final HashMap<String, String> loincCodeToText = extractLoincDictionary(Database.createInstance(settings).query(DICTIONARY_LOINC));

        sources[0] = new LabSource(Database.createInstance(settings).queryStreaming(LPCH_LAB_DE), 1, uniqueLabNames, ignoreLabTextValues);
        sources[1] = new LabSource(Database.createInstance(settings).queryStreaming(LPCH_LAB_EPIC_DE), 1, uniqueLabNames, ignoreLabTextValues);
        sources[2] = new LabSource(Database.createInstance(settings).queryStreaming(SHC_LAB_EPIC_DE), 1, uniqueLabNames, ignoreLabTextValues);
        sources[3] = new LabSourceCulture(Database.createInstance(settings).queryStreaming(SHC_LAB_CULTURE), 1, getLabNameTransformation(cultureNameMap), getLabValueTransformation(
            cultureMap));

        extractVisits(sources, new File(new File(args[1]), Stride6DatabaseDownload.LABS_FILE), Commands.LABS_EXTENDED_STRUCTURE);
        for (int x = 0; x < sources.length; x++) {
          System.out.println("LABS[" + x + "] " + sources[x].getRejectedCnt() + " / " + sources[x].getProcessedCnt());
        }
        Iterator<String> i = uniqueLabNames.values().iterator();
        while (i.hasNext()) {
          final String labName = i.next();
          if (labName != null) {
            labNamesOut.write(labName, labName);
          }
        }

        i = loincCodeToText.keySet().iterator();
        while (i.hasNext()) {
          final String loincCode = i.next();
          labNamesOut.write(loincCode, loincCodeToText.get(loincCode));
        }
        labNamesOut.close();
        System.out.println("Number of unique labs = " + uniqueLabNames.size());
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting labs");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('r') != -1) {
      try {
        db = Database.createInstance(settings);
        set = db.query(VITALS_VISITS_7);
        extractVitalsVisits(set, new File(new File(args[1]), Stride6DatabaseDownload.VITALS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting vitals_visits");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('n') != -1) {
      try {
        try {
          final IntKeyIntOpenHashMap nidToTid = new IntKeyIntOpenHashMap();
          db = Database.createInstance(settings);
          final BinaryFileWriter writer = new BinaryFileWriter(new File(new File(args[1]), Stride6DatabaseDownload.NOTES_FILE), Commands.NOTES_STRUCTURE);
          extractNotes(db.query(NOTES_CLINICAL), writer, nidToTid);
          extractNotes(db.query(NOTES_PATHOLOGY), writer, nidToTid);
          extractNotes(db.query(NOTES_RADIOLOGY), writer, nidToTid);
          writer.close();
          extractTerms(settings, new File(args[1]), nidToTid);
          extractDictionary(settings, new File(args[1]));
        }
        catch (final Exception e) {
          System.out.println("!!! Error extracting notes");
          e.printStackTrace();
        }
        finally {
          FileUtils.close(set);
          FileUtils.close(db);
        }
      }
      catch (final Exception e) {
        //
      }
    }
    if (args[0].indexOf('u') != -1) {
      try {
        extractDictionary(settings, new File(args[1]));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting term dictionary");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('m') != -1) {
      try {
        db = Database.createInstance(settings);
        final LongKeyOpenHashMap lpchMedIds = extractMedDictionary(db.query(MED_ID_LPCH_EPIC));
        final IntKeyOpenHashMap lpchStatus = extractMedStatus(db.query(MED_STATUS_LPCH));
        final IntKeyOpenHashMap lpchRoute = extractMedStatus(db.query(MED_ROUTE_LPCH));
        final LongKeyOpenHashMap shcMedIds = extractMedDictionary(db.query(MED_ID_SHC_EPIC));
        final IntKeyOpenHashMap shcStatus = extractMedStatus(db.query(MED_STATUS_SHC));
        final IntKeyOpenHashMap shcRoute = extractMedStatus(db.query(MED_ROUTE_SHC));
        final LongKeyOpenHashMap hl7MedIds = extractMedDictionary(db.query(MED_ID_HL7));
        final IntKeyObjectMap<IntKeyObjectMap<ArrayList<Double>>> admin = getAdminData(db.query(MED_ADMIN_LPCH));

        final int[] historicalClassesLpch = extractHistoricalClasses(db.query(MED_CLASS_LPCH));
        final int[] historicalClassesShc = extractHistoricalClasses(db.query(MED_CLASS_SHC));

        final Stride7Source[] sources = new Stride7Source[4];
        sources[0] = new MedicationSource(Database.createInstance(settings).queryStreaming(MED_LPCH_DE), 1, lpchMedIds, lpchRoute, lpchStatus, admin);
        ((MedicationSource) sources[0]).setStatusIdColumn(5).setHistoricalClasses(historicalClassesLpch);
        sources[1] = new MedicationSource(Database.createInstance(settings).queryStreaming(MED_LPCH_HL7), 1, hl7MedIds, lpchRoute, lpchStatus, admin);
        ((MedicationSource) sources[1]).setStatusStringColumn(5);
        final IntKeyObjectMap<IntKeyObjectMap<ArrayList<Double>>> adminShc = getAdminData(db.queryStreaming(MED_ADMIN_SHC));
        sources[2] = new MedicationSource(Database.createInstance(settings).queryStreaming(MED_SHC_HL7), 1, hl7MedIds, shcRoute, shcStatus, adminShc);
        ((MedicationSource) sources[2]).setStatusStringColumn(5);
        sources[3] = new MedicationSource(Database.createInstance(settings).queryStreaming(MED_SHC_DE), 1, shcMedIds, shcRoute, shcStatus, adminShc);
        ((MedicationSource) sources[3]).setStatusIdColumn(5).setHistoricalClasses(historicalClassesShc);
        extractVisits(sources, new File(new File(args[1]), Stride6DatabaseDownload.MEDS_FILE), Commands.MEDS_STRUCTURE);

        for (int x = 0; x < sources.length; x++) {
          System.out.println("MEDS[" + x + "] " + sources[x].getRejectedCnt() + " / " + sources[x].getProcessedCnt());
          sources[x].close();
        }

      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting meds");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('x') != -1) {
      try {
        db = Database.createInstance(settings);
        final LongKeyOpenHashMap lpchDepartments = extractDepartmentDictionary(db.query(DICTIONARY_DEPARTMENT_LPCH));
        final LongKeyOpenHashMap shcDepartments = extractDepartmentDictionary(db.query(DICTIONARY_DEPARTMENT_SHC));

        final Stride7Source[] sources = new Stride7Source[4];
        sources[0] = new DepartmentSource(Database.createInstance(settings).queryStreaming(SHC_VISIT_APPT_DE), shcDepartments);
        sources[1] = new DepartmentSource(Database.createInstance(settings).queryStreaming(SHC_VISIT_DE), shcDepartments);
        sources[2] = new DepartmentSource(Database.createInstance(settings).queryStreaming(LPCH_VISIT_APPT_DE), lpchDepartments);
        sources[3] = new DepartmentSource(Database.createInstance(settings).queryStreaming(LPCH_VISIT_DE), lpchDepartments);
        extractVisits(sources, new File(new File(args[1]), Stride6DatabaseDownload.DEPARTMENT_FILE), Commands.DEPARTMENT_STRUCTURE);

        for (int x = 0; x < sources.length; x++) {
          System.out.println("DEPARTMENTS[" + x + "] " + sources[x].getRejectedCnt() + " / " + sources[x].getProcessedCnt());
          sources[x].close();
        }

      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting departments");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  private static Function<String, String> getLabNameTransformation(final HashMap<String, String> cultureNameMap) {
    final Function<String, String> labNameTransformation = new Function<String, String>() {

      @Override
      public String apply(final String t) {
        if (cultureNameMap.containsKey(t)) {
          return "CULTURE [" + cultureNameMap.get(t) + "]";
        }
        else {
          return t;
        }
      }
    };
    return labNameTransformation;
  }

  private static Function<String, String[]> getLabValueTransformation(final HashMap<String, String> cultureMap) {
    final Function<String, String[]> labValueTransformation = new Function<String, String[]>() {

      @Override
      public String[] apply(String t) {
        if (t != null) {
          t = t.toLowerCase().trim();
          if (t.indexOf('[') != -1) {
            t = t.toLowerCase().trim().replace("  ", " ").replace("  ", " ").replace("strep.", "streprococcus").replaceAll("strep ", "streptococcus").replace("c. diff",
                "clostridium diff").replace("c.diff", "clostridium diff").replaceAll("e. coli", "escherichia coli").replace("s. pneumonia", "streptococcus pneumoniae").replace(
                    "corynebac.", "corynebacterium").replace("viridans gp", "viridans group").replace("porphyromonas gp.", "porphyromonas group").replace("streptococcus bovis gp",
                        "streptococcus bovis group");
            final String[] cultures = TextUtils.getTextBetweenChars(t, '[', ']');
            for (int x = 0; x < cultures.length; x++) {
              cultures[x] = cultures[x].trim();
              cultures[x] = cultureMap.containsKey(cultures[x]) ? cultureMap.get(cultures[x]) : cultures[x];
            }
            return cultures;
          }
        }
        return new String[] {"negative"};
      }
    };
    return labValueTransformation;
  }

  private static void collateTerms(final File outputFolder, final IntArrayList writerIds) throws Exception {
    for (int x = 0; x < 100; x++) {
      final File file = new File(outputFolder, "notes_" + x);
      final File dict = Common.getDictFile(new File(outputFolder, "notes_" + x));
      if (!file.exists() || !dict.exists()) {
        continue;
      }
      System.out.println("Collating " + x);
      final ExtractionSource set = new BinaryFileExtractionSource(file, dict, Commands.TERMS_STRUCTURE);
      final IntKeyObjectMap<ArrayList<int[]>> pidToContents = new IntKeyObjectMap<>();
      while (set.next()) {
        final int patientId = set.getInt(TermExtractor.PATIENT_ID_COLUMN);
        final int tid = set.getInt(TermExtractor.TID_COLUMN);
        final int nid = set.getInt(TermExtractor.NID_COLUMN);
        final int negatedFh = set.getByte(TermExtractor.NEGATED_FH_COLUMN);
        ArrayList<int[]> content = pidToContents.get(patientId);
        if (content == null) {
          content = new ArrayList<>();
          pidToContents.put(patientId, content);
        }
        content.add(new int[] {nid, tid, negatedFh});
      }
      set.close();
      final IntArrayList sortedPids = new IntArrayList(pidToContents.keySet());
      sortedPids.sort();
      FileUtils.deleteWithException(new File(outputFolder, "notes_" + x));
      FileUtils.deleteWithException(Common.getDictFile(new File(outputFolder, "notes_" + x)));
      System.out.println("Writing " + x);
      final BinaryFileWriter writer = new BinaryFileWriter(new File(outputFolder, "notes_" + x), Commands.TERMS_STRUCTURE);
      for (int y = 0; y < sortedPids.size(); y++) {
        final ArrayList<int[]> contents = pidToContents.get(sortedPids.get(y));
        for (int z = 0; z < contents.size(); z++) {
          writer.write(sortedPids.get(y), contents.get(z)[0], contents.get(z)[1], contents.get(z)[2]);
        }
      }
      writer.close();
    }
    final ArrayList<TermExtractor> extractors = new ArrayList<>();
    final ArrayList<PatientRecord<TermRecord>> records = new ArrayList<>();
    for (int x = 0; x < 100; x++) {
      final File file = new File(outputFolder, "notes_" + x);
      final File dict = Common.getDictFile(new File(outputFolder, "notes_" + x));
      if (!file.exists() || !dict.exists()) {
        continue;
      }
      extractors.add(new TermExtractor(new BinaryFileExtractionSource(file, dict, Commands.TERMS_STRUCTURE)));
      records.add(extractors.get(extractors.size() - 1).next());
    }
    final BinaryFileWriter writer = new BinaryFileWriter(new File(outputFolder, Stride6DatabaseDownload.TERMS_FILE), Commands.TERMS_STRUCTURE);
    while (true) {
      int minPatientId = Integer.MAX_VALUE;
      int minPatientPos = -1;
      for (int x = 0; x < records.size(); x++) {
        if (records.get(x) != null && records.get(x).getPatientId() < minPatientId) {
          minPatientId = records.get(x).getPatientId();
          minPatientPos = x;
        }
      }
      if (minPatientPos == -1) {
        break;
      }
      for (final TermRecord rec : records.get(minPatientPos).getRecords()) {
        writer.write(records.get(minPatientPos).getPatientId(), rec.getNoteId(), rec.getTermId(), Common.encodeNegatedFamilyHistory(rec.getNegated(), rec.getFamilyHistory()));
      }
      if (extractors.get(minPatientPos).hasNext()) {
        records.set(minPatientPos, extractors.get(minPatientPos).next());
      }
      else {
        records.set(minPatientPos, null);
        extractors.get(minPatientPos).close();
      }
    }
    writer.close();
  }

  private static IntKeyObjectMap<IntKeyObjectMap<ArrayList<Double>>> getAdminData(final ResultSet set) throws SQLException {
    final IntKeyObjectMap<IntKeyObjectMap<ArrayList<Double>>> result = new IntKeyObjectMap<>();
    while (set.next()) {
      IntKeyObjectMap<ArrayList<Double>> patient = result.get(set.getInt(1));
      if (patient == null) {
        patient = new IntKeyObjectMap<>();
        result.put(set.getInt(1), patient);
      }
      ArrayList<Double> times = patient.get(set.getInt(2));
      if (times == null) {
        times = new ArrayList<>();
        patient.put(set.getInt(2), times);
      }
      times.add(set.getDouble(3));
    }
    set.close();
    return result;
  }

  private static void extractTerms(final ConnectionSettings settings, final File outputFolder, final IntKeyIntOpenHashMap nidToPid) throws Exception {
    for (int x = 0; x < 100; x++) {
      FileUtils.deleteWithException(new File(outputFolder, "notes_" + x));
    }
    final IntArrayList writerIds = new IntArrayList();
    int roundRobinPosition = 0;
    final ArrayList<BinaryFileWriter> writers = new ArrayList<>();
    for (int x = 0; x < 100; x++) {
      writers.add(new BinaryFileWriter(new File(outputFolder, "notes_" + x), Commands.TERMS_STRUCTURE));
    }
    final Database db = Database.createInstance(settings);
    final ResultSet set = db.queryStreaming(TERMS);
    while (set.next()) {
      final int nid = set.getInt(1);
      if (set.wasNull()) {
        continue;
      }
      if (!nidToPid.containsKey(nid)) {
        System.out.println("There is no pid for nid " + nid);
        continue;
      }
      final int pid = nidToPid.get(nid);
      if (set.wasNull()) {
        continue;
      }
      BinaryFileWriter writer = null;
      if (writerIds.size() < pid) {
        for (int x = writerIds.size() - 1; x <= pid; x++) {
          writerIds.add(-1);
        }
        if (roundRobinPosition > writers.size() - 1) {
          roundRobinPosition = 0;
        }
        writerIds.add(roundRobinPosition++);
      }
      final int writerId = writerIds.get(pid);
      if (writerId == -1) {
        if (roundRobinPosition > writers.size() - 1) {
          roundRobinPosition = 0;
        }
        writerIds.set(pid, roundRobinPosition++);
      }
      writer = writers.get(writerIds.get(pid));
      final int tid = set.getInt(2);
      if (set.wasNull()) {
        continue;
      }
      if (!set.wasNull()) {
        writer.write(pid, //
            nid, //
            tid, //
            Common.encodeNegatedFamilyHistory(set.getInt(3), set.getInt(4)));
      }
    }
    set.close();
    db.close();
    for (final BinaryFileWriter writer : writers) {
      writer.close();
    }
    System.out.println("Collating terms...");
    collateTerms(outputFolder, writerIds);
  }

  private static void extractDictionary(final ConnectionSettings settings, final File folder) throws SQLException, IOException {
    System.out.println("Extracting dictionary...");

    final ResultSet set = Database.createInstance(settings).query(TERM_DICTIONARY);
    Stride6DatabaseDownload.extractTermDictionary(set, new File(folder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE));
    set.close();

  }

  private static IntKeyObjectMap<HashSet<String>> extractDxDictionary(final ResultSet set) throws SQLException {
    final IntKeyObjectMap<HashSet<String>> result = new IntKeyObjectMap<>();
    while (set.next()) {
      final int dxId = set.getInt(1);
      HashSet<String> s = result.get(dxId);
      if (s == null) {
        s = new HashSet<>();
        result.put(dxId, s);
      }
      final String[] codes = TextUtils.split(set.getString(2), ',');
      for (final String code : codes) {
        s.add(code.trim());
      }
    }
    set.close();
    return result;
  }

  private static HashMap<String, String> extractLoincDictionary(final ResultSet set) throws SQLException {
    final HashMap<String, String> result = new HashMap<>();
    while (set.next()) {
      final String loinc = set.getString(1).toLowerCase();
      final String text = set.getString(2);
      result.put(loinc, text);
    }
    set.close();
    return result;
  }

  private static HashMap<String, String> extractCulturesDictionary(final ResultSet set) throws SQLException {
    final HashMap<String, String> result = new HashMap<>();
    while (set.next()) {
      final String origText = set.getString(1);
      String resultText = set.getString(2);
      if (!set.getString(3).equalsIgnoreCase(CULTURE_DICTIONARY_OK_FLAG)) {
        resultText = null;
      }
      result.put(origText.toLowerCase(), resultText);
    }
    set.close();
    return result;
  }

  private static IntKeyStringMap extractDxPrimaryCode(final ResultSet set) throws SQLException {
    final IntKeyStringMap result = new IntKeyStringMap();
    while (set.next()) {
      result.put(set.getInt(1), set.getString(2));
    }
    set.close();
    return result;
  }

  private static void extractNotes(final ResultSet set, final BinaryFileWriter writer, final IntKeyIntOpenHashMap map) throws IOException, SQLException {
    while (set.next()) {
      final int nid = set.getInt(1);
      final int pid = set.getInt(5);
      writer.write(nid, //
          set.getString(2), //
          Math.floor(set.getDouble(3)), //
          set.getShort(4));
      map.put(nid, pid);
    }
    set.close();
  }

  private static boolean extractPatient(final Stride7Source[] sources, final BinaryFileWriter writer, final int patientId) throws SQLException, IOException {
    boolean recordsToExtract = false;
    for (final Stride7Source src : sources) {
      final int pid = src.getPatientId();
      if (pid == patientId) {
        src.write(writer);
      }
      if (pid != Integer.MAX_VALUE) {
        recordsToExtract = true;
      }
    }
    return recordsToExtract;
  }

  private static void extractVisits(final Stride7Source[] sources, final File output, final DATA_TYPE[] structure) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    System.out.println("Extracting...");
    final BinaryFileWriter writer = new BinaryFileWriter(output, structure);
    for (int x = 0; x < Integer.MAX_VALUE; x++) {
      if (!extractPatient(sources, writer, x)) {
        break;
      }
    }
    for (final Stride7Source source : sources) {
      source.close();
    }
    writer.close();
  }

  private static void calculateDistinctTexts(final ResultSet set, final HashMap<String, HashSet<String>> map) throws SQLException {
    while (set.next()) {
      final String value = set.getString(1);
      try {
        set.getDouble(1);
      }
      catch (final Exception e) {
        String loinc = set.getString(2);
        if (set.wasNull()) {
          loinc = set.getString(3);
        }
        HashSet<String> s = map.get(loinc);
        if (s == null) {
          s = new HashSet<>();
          map.put(loinc, s);
        }
        s.add(value);
      }
    }
  }

  private static LongKeyOpenHashMap extractMedDictionary(final ResultSet set) throws IOException, SQLException {
    final LongKeyOpenHashMap result = new LongKeyOpenHashMap();
    while (set.next()) {
      final long medId = set.getLong(1);
      final int rxCui = set.getInt(2);
      IntOpenHashSet s = (IntOpenHashSet) result.get(medId);
      if (s == null) {
        s = new IntOpenHashSet();
        result.put(medId, s);
      }
      s.add(rxCui);
    }
    return result;
  }

  private static LongKeyOpenHashMap extractDepartmentDictionary(final ResultSet set) throws IOException, SQLException {
    final LongKeyOpenHashMap result = new LongKeyOpenHashMap();
    while (set.next()) {
      final long deptId = set.getLong(1);
      final String departmentName = set.getString(2);
      result.put(deptId, departmentName);
    }
    return result;
  }

  private static IntKeyOpenHashMap extractMedStatus(final ResultSet set) throws IOException, SQLException {
    final IntKeyOpenHashMap result = new IntKeyOpenHashMap();
    while (set.next()) {
      final int statusId = set.getInt(1);
      final String name = set.getString(2);
      result.put(statusId, name);
    }
    return result;
  }

  private static void extractVitalsVisits(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.VITALS_STRUCTURE);
    while (set.next()) {
      if (set.getDouble(3) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_BP_S, //
            set.getDouble(3), //
            set.getInt(12));
      }
      if (set.getDouble(4) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_BP_D, //
            set.getDouble(4), //
            set.getInt(12));
      }
      if (set.getDouble(5) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_TEMPERATURE, //
            set.getDouble(5), //
            set.getInt(12));
      }
      if (set.getDouble(6) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_PULSE, //
            set.getDouble(6), //
            set.getInt(12));
      }
      if (set.getString(7) != null && !set.getString(7).isEmpty() && set.getDouble(7) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_WEIGHT, //
            set.getDouble(7), //
            set.getInt(12));
      }
      if (set.getString(8) != null && !set.getString(8).isEmpty()) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_HEIGHT, //
            Common.parseHeightToInchesSilent(set.getString(8)), //
            set.getInt(12));
      }
      if (set.getString(9) != null && !set.getString(9).isEmpty() && set.getDouble(9) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_RESP, //
            set.getDouble(9), //
            set.getInt(12));
      }
      if (set.getString(10) != null && !set.getString(10).isEmpty() && set.getDouble(10) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_BMI, //
            set.getDouble(10), //
            set.getInt(12));
      }
      if (set.getString(11) != null && !set.getString(11).isEmpty() && set.getDouble(11) != 0) {
        writer.write(set.getInt(1), //
            1, //
            set.getDouble(2), //
            Common.VITALS_BSA, //
            set.getDouble(11), //
            set.getInt(12));
      }
    }

    writer.close();
  }

  static void extractDemographics(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.DEMOGRAPHICS_STRUCTURE);
    while (set.next()) {
      double death = set.getDouble(DemographicsExtractor.DEATH_COLUMN);
      if (death != 0) {
        death = Math.ceil(set.getDouble(DemographicsExtractor.DEATH_COLUMN) + 0.001);
      }
      writer.write(set.getInt(DemographicsExtractor.PATIENT_ID_COLUMN), //
          set.getString(DemographicsExtractor.GENDER_COLUMN), //
          set.getString(DemographicsExtractor.RACE_COLUMN), //
          set.getString(DemographicsExtractor.ETHNICITY_COLUMN), //
          death, //
          set.getInt(DemographicsExtractor.DEATH_YEAR_COLUMN));
    }
    writer.close();
  }

  public static void main(final String[] args) throws IOException, SQLException {
    System.out.println("Usage: options output_folder connection_settings_file");
    System.out.println();
    System.out.println("options");
    System.out.println("d = demographics");
    System.out.println("r = vitals");
    System.out.println("m = prescriptions");
    System.out.println("v = visits");
    System.out.println("l = labs");
    System.out.println("x = departments");
    System.out.println("n = notes");
    System.out.println("u = extract dictionary");
    System.out.println("DB_URL   jdbc:mysql://shahlab-db1.stanford.edu:3306");
    execute(args, ConnectionSettings.createFromFile(new File(args[2])));
    System.out.println("DONE");
  }

}