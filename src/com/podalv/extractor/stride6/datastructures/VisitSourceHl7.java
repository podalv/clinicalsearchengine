package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileWriter;

public class VisitSourceHl7 extends Stride7Source {

  public VisitSourceHl7(final ResultSet set, final int patientIdColumn) {
    super(set, patientIdColumn);
  }

  private String getPrimaryIndicator(final String type) {
    return (type != null && type.startsWith("Primary")) ? "ICD9_DX" : "ICD9";
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //patient_id, age_at_contact_in_days, visit_duration, code, visit_type, dx_px_type_text, YEAR(contact_date)
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      if (set.getString(4) != null && set.getString(4).startsWith("N")) {
        throw new UnsupportedOperationException(set.getInt(patientIdColumn) + " / ");
      }
      addProcessed();
      writer.write(1, //
          set.getInt(patientIdColumn), //
          set.getInt(7), //
          set.getDouble(2), //
          set.getString(5), //
          set.getDouble(3), //
          set.getString(4), //
          getPrimaryIndicator(set.getString(6)), //
          null);
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }
}
