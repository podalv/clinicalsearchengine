package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;

public class LabSourceCulture extends Stride7Source {

  private final Function<String, String[]> valueTransformation;
  private final Function<String, String>   labNameTransformation;

  public LabSourceCulture(final ResultSet set, final int patientIdColumn, final Function<String, String> labNameTransformation,
      final Function<String, String[]> valueTransformation) {
    super(set, patientIdColumn);
    this.valueTransformation = valueTransformation;
    this.labNameTransformation = labNameTransformation;
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      final String[] value = valueTransformation.apply(set.getString(5));
      if (value != null) {
        for (final String culture : value) {
          if (culture != null)
            writer.write(set.getInt(patientIdColumn), //
                set.getDouble(2), //
                set.getInt(3), //
                labNameTransformation.apply(set.getString(4).toLowerCase()), //
                culture, //
                Stride6DatabaseDownload.UNDEFINED);
        }
      }
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }

  }

}
