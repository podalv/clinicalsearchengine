package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.maps.primitive.map.LongKeyOpenHashMap;

public class DepartmentSource extends Stride7Source {

  private final LongKeyOpenHashMap departmentIdToNameMap;

  public DepartmentSource(final ResultSet set, final LongKeyOpenHashMap departmentIdToNameMap) {
    super(set, 1);
    this.departmentIdToNameMap = departmentIdToNameMap;
  }

  @Override
  public void write(final BinaryFileWriter writer) throws SQLException, IOException {
    final int pid = getPatientId();
    if (pid == Integer.MAX_VALUE) {
      return;
    }
    while (pid == getPatientId()) {
      final long deptId = set.getLong(2);
      final String department = (String) departmentIdToNameMap.get(deptId);
      if (department != null) {
        double offset = set.getDouble(3);
        if (set.wasNull()) {
          offset = Double.NEGATIVE_INFINITY;
        }
        writer.write(set.getInt(1), //
            offset, //
            set.getInt(4), //
            department);
        addProcessed();
      }
      else {
        addRejected();
      }
      if (!set.next() || pid != set.getInt(1)) {
        break;
      }
    }
  }

  @Override
  public int getPatientId() throws SQLException {
    if (set.isBeforeFirst()) {
      set.next();
    }
    if (set.isAfterLast()) {
      return Integer.MAX_VALUE;
    }
    return set.getInt(1);
  }

}