package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileWriter;

public class VisitSourcePxIcdHspDe extends Stride7Source {

  public VisitSourcePxIcdHspDe(final ResultSet set, final int patientIdColumn) {
    super(set, patientIdColumn);
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //patient_id, age_at_proc_in_days, YEAR(proc_date), ref_bill_code, ref_bill_code_set_c
    //patient_id, age_at_contact_in_days, YEAR(contact_date), code, sab, visit_type
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      addProcessed();
      if (set.getInt(5) == 2) {
        final String code = set.getString(4);
        if (!set.wasNull()) {
          writer.write(1, //
              set.getInt(patientIdColumn), //
              set.getInt(3), //
              set.getDouble(2), //
              "UNKNOWN", //
              0, //
              code, //
              "ICD10", //
              null);
        }
      }
      else {
        writer.write(1, //
            set.getInt(patientIdColumn), //
            set.getInt(3), //
            set.getDouble(2), //
            "UNKNOWN", //
            0, //
            set.getString(4), //
            "ICD9", //
            null);
      }
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }
}
