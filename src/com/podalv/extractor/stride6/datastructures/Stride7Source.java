package com.podalv.extractor.stride6.datastructures;

import java.io.Closeable;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;
import com.podalv.utils.file.FileUtils;

public abstract class Stride7Source implements Closeable {

  ResultSet    set;
  final int    patientIdColumn;
  private long processedCnt = 0;
  private long rejectedCnt  = 0;

  public Stride7Source(final ResultSet set, final int patientIdColumn) {
    this.set = set;
    this.patientIdColumn = patientIdColumn;
  }

  void write(final BinaryFileWriter writer, final ResultSet set, final String transformedLabName, final String text, final double value) throws IOException, SQLException {
    writer.write(set.getInt(patientIdColumn), //
        set.getDouble(2), //
        set.getInt(3), //
        transformedLabName, //
        text, //
        value);
  }

  void writeNoValue(final BinaryFileWriter writer, final ResultSet set, final String transformedLabName) throws IOException, SQLException {
    write(writer, set, transformedLabName, LabsRecord.NO_VALUE, Stride6DatabaseDownload.UNDEFINED);
  }

  void writeOnlyValue(final BinaryFileWriter writer, final ResultSet set, final String transformedLabName, final double value) throws IOException, SQLException {
    write(writer, set, transformedLabName, LabsRecord.UNDEFINED, value);
  }

  void writeOnlyText(final BinaryFileWriter writer, final ResultSet set, final String transformedLabName, final String text) throws IOException, SQLException {
    write(writer, set, transformedLabName, text, Stride6DatabaseDownload.UNDEFINED);
  }

  void addProcessed() {
    processedCnt++;
  }

  void addRejected() {
    rejectedCnt++;
  }

  public long getProcessedCnt() {
    return processedCnt;
  }

  public long getRejectedCnt() {
    return rejectedCnt;
  }

  public abstract void write(BinaryFileWriter writer) throws IOException, SQLException;

  @Override
  public void close() throws IOException {
    FileUtils.close(set);
  }

  public int getPatientId() throws SQLException {
    boolean hasNext = true;
    if (set.isBeforeFirst()) {
      hasNext = set.next();
    }
    else if (set.isAfterLast()) {
      return Integer.MAX_VALUE;
    }
    if (hasNext) {
      return set.getInt(patientIdColumn);
    }
    return Integer.MAX_VALUE;
  }

}
