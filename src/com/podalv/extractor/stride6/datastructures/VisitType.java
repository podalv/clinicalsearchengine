package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.maps.string.IntKeyStringMap;

public class VisitType extends Stride7Source {

  private final IntKeyStringMap visitTypeMap;

  public VisitType(final ResultSet set, final int patientIdColumn, final IntKeyStringMap visitTypeMap) {
    super(set, patientIdColumn);
    this.visitTypeMap = visitTypeMap;
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //SELECT patient_id, age_at_contact_in_days, YEAR(contact_date), stride7_dictionaries.shc_zc_disp_enc_type.name, age_at_hosp_admsn_in_days, age_at_hosp_admsn_in_days

    //patient_id, age_at_contact_in_days, visit_duration, code, visit_type, dx_px_type_text, YEAR(contact_date)
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      addProcessed();
      final double admDate = set.getDouble(5);
      final double dischDate = set.getDouble(6);
      final double age = set.getDouble(2);
      String visitType = visitTypeMap.get(set.getInt(4));
      if (age > 0 && visitType != null) {
        writer.write(1, //
            set.getInt(patientIdColumn), //
            set.getInt(3), //
            age, //
            visitType, //
            0d, //
            null, //
            "ICD9", //
            null);
      }
      if (admDate != 0 && dischDate != 0) {
        visitType = "Inpatient";
        writer.write(1, //
            set.getInt(patientIdColumn), //
            set.getInt(3), //
            admDate, //
            visitType, //
            dischDate - admDate, //
            null, //
            "ICD9", //
            null);
      }
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }
}
