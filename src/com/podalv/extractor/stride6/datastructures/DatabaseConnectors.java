package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource;
import com.podalv.extractor.datastructures.ExtractionOptions;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.DepartmentRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.ObservationRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.Commands;
import com.podalv.extractor.stride6.FileExtractor;
import com.podalv.extractor.stride6.extractors.DemographicsExtractor;
import com.podalv.extractor.stride6.extractors.DepartmentExtractor;
import com.podalv.extractor.stride6.extractors.LabsExtractor;
import com.podalv.extractor.stride6.extractors.LpchLabsExtractor;
import com.podalv.extractor.stride6.extractors.MedsExtractor;
import com.podalv.extractor.stride6.extractors.NotesExtractor;
import com.podalv.extractor.stride6.extractors.ObservationExtractor;
import com.podalv.extractor.stride6.extractors.ShcLabsExtractor;
import com.podalv.extractor.stride6.extractors.TermExtractor;
import com.podalv.extractor.stride6.extractors.VisitDxExtractor;
import com.podalv.extractor.stride6.extractors.VisitExtractor;
import com.podalv.extractor.stride6.extractors.VitalsExtractor;
import com.podalv.extractor.stride6.extractors.VitalsVisitsExtractor;

/** Encapsulates all the database connectors. These are configurable and can be plugged into any schema provided that it can
 *  serve same data views as Stride6
 *
 * @author podalv
 *
 */
public class DatabaseConnectors implements DatabaseConnectorsInterface {

  private final DemographicsExtractor      demographics;
  private final VisitExtractor             visits;
  private TermExtractor                    terms;
  private final MedsExtractor              med;
  private VitalsExtractor                  vitals;
  private VitalsVisitsExtractor            vitalsVisits;
  private LpchLabsExtractor                lpchLabs;
  private ShcLabsExtractor                 shcLabs;
  private LabsExtractor                    extendedLabs;
  private final VisitDxExtractor           visitDxExtractor;
  private final NotesExtractor             notesExtractor;
  private ObservationExtractor             observationExtractor;
  private DepartmentExtractor              departmentExtractor;
  private final ExtractionOptions          options;
  private PatientRecord<VisitRecord>       currentVisit        = null;
  private PatientRecord<TermRecord>        currentTerms        = null;
  private PatientRecord<MedRecord>         currentMeds         = null;
  private PatientRecord<VitalsRecord>      mergedVitals        = null;
  private PatientRecord<VitalsRecord>      currentVitals       = null;
  private PatientRecord<DepartmentRecord>  currentDepartments  = null;
  private PatientRecord<VitalsRecord>      currentVitalsVisits = null;
  private PatientRecord<LabsRecord>        currentShcLabs      = null;
  private PatientRecord<LabsRecord>        currentLpchLabs     = null;
  private PatientRecord<LabsRecord>        currentExtendedLabs = null;
  private PatientRecord<ObservationRecord> currentObservation  = null;
  private PatientRecord<LabsRecord>        currentLabs         = null;
  private DemographicsRecord               currentDemo         = null;

  public DatabaseConnectors(final FileExtractor files, final ExtractionOptions options) throws SQLException, IOException {
    this.options = options;
    demographics = new DemographicsExtractor(new BinaryFileExtractionSource(files.getDemographics(), files.getDemographics_dict(), Commands.DEMOGRAPHICS_STRUCTURE));
    visits = new VisitExtractor(new BinaryFileExtractionSource(files.getVisits(), files.getVisits_dict(), Commands.VISITS_STRUCTURE));
    if (options.extractTerms()) {
      terms = new TermExtractor(new BinaryFileExtractionSource(files.getTerms(), files.getTerms_dict(), Commands.TERMS_STRUCTURE));
    }
    med = new MedsExtractor(new BinaryFileExtractionSource(files.getMeds(), files.getMeds_dict(), Commands.MEDS_STRUCTURE));
    if (files.getVitals() != null) {
      vitals = new VitalsExtractor(new BinaryFileExtractionSource(files.getVitals(), files.getVitals_dict(), Commands.VITALS_STRUCTURE));
    }
    if (files.getVitalsVisits() != null) {
      vitalsVisits = new VitalsVisitsExtractor(new BinaryFileExtractionSource(files.getVitalsVisits(), files.getVitalsVisits_dict(), Commands.VITALS_VISITS_STRUCTURE));
    }
    if (files.getObservation() != null) {
      observationExtractor = new ObservationExtractor(new BinaryFileExtractionSource(files.getObservation(), files.getObservation_dict(), Commands.OBSERVATION_STRUCTURE));
    }
    if (files.getDepartment() != null) {
      departmentExtractor = new DepartmentExtractor(new BinaryFileExtractionSource(files.getDepartment(), files.getDepartment_dict(), Commands.DEPARTMENT_STRUCTURE));
    }
    if (files.getExtendedLabs() != null) {
      extendedLabs = new LabsExtractor(new BinaryFileExtractionSource(files.getExtendedLabs(), files.getExtendedLabs_dict(), Commands.LABS_EXTENDED_STRUCTURE));
    }
    else {
      shcLabs = new ShcLabsExtractor(new BinaryFileExtractionSource(files.getShcLabs(), files.getShcLabs_dict(), Commands.LABS_SHC_STRUCTURE));
      lpchLabs = new LpchLabsExtractor(new BinaryFileExtractionSource(files.getLpchLabs(), files.getLpchLabs_dict(), Commands.LABS_LPCH_STRUCTURE));
    }
    visitDxExtractor = new VisitDxExtractor(new BinaryFileExtractionSource(files.getVisitDx(), files.getVisitDx_dict(), Commands.VISIT_DX_STRUCTURE));
    notesExtractor = new NotesExtractor(new BinaryFileExtractionSource(files.getNotes(), files.getNotes_dict(), Commands.NOTES_STRUCTURE));
  }

  @Override
  public NotesExtractor getNotesExtractor() {
    return notesExtractor;
  }

  @Override
  public PatientRecord<MedRecord> getCurrentMeds() {
    return currentMeds;
  }

  @Override
  public boolean hasNext() {
    return demographics.hasNext();
  }

  @Override
  public DemographicsRecord getCurrentDemo() {
    return currentDemo;
  }

  @Override
  public PatientRecord<LabsRecord> getCurrentLabs() {
    return (currentLabs != null && currentDemo.getPatientId() == currentLabs.getPatientId()) ? currentLabs : null;
  }

  @Override
  public PatientRecord<VitalsRecord> getCurrentVitals() {
    return (mergedVitals != null && currentDemo.getPatientId() == mergedVitals.getPatientId()) ? mergedVitals : null;
  }

  @Override
  public PatientRecord<TermRecord> getCurrentTerms() {
    return (currentTerms != null && currentDemo.getPatientId() == currentTerms.getPatientId()) ? currentTerms : null;
  }

  @Override
  public PatientRecord<VisitRecord> getCurrentVisit() {
    return (currentVisit != null && currentDemo.getPatientId() == currentVisit.getPatientId()) ? currentVisit : null;
  }

  @Override
  public PatientRecord<ObservationRecord> getCurrentObservation() {
    return (currentObservation != null && currentDemo.getPatientId() == currentObservation.getPatientId()) ? currentObservation : null;
  }

  private void mergeSortVitals() {
    mergedVitals = new PatientRecord<VitalsRecord>(currentDemo.getPatientId());
    if (currentVitals != null && currentVitals.getPatientId() == currentDemo.getPatientId()) {
      mergedVitals.addAll(currentVitals.getRecords());
    }
    if (currentVitalsVisits != null && currentVitalsVisits.getPatientId() == currentDemo.getPatientId()) {
      mergedVitals.addAll(currentVitalsVisits.getRecords());
    }
    Collections.sort(mergedVitals.getRecords());
  }

  private void mergeSortLabs() {
    currentLabs = new PatientRecord<LabsRecord>(currentDemo.getPatientId());
    if (currentShcLabs != null && currentShcLabs.getPatientId() == currentDemo.getPatientId()) {
      currentLabs.addAll(currentShcLabs.getRecords());
    }
    if (currentLpchLabs != null && currentLpchLabs.getPatientId() == currentDemo.getPatientId()) {
      currentLabs.addAll(currentLpchLabs.getRecords());
    }
    if (currentExtendedLabs != null && currentExtendedLabs.getPatientId() == currentDemo.getPatientId()) {
      currentLabs.addAll(currentExtendedLabs.getRecords());
    }

    Collections.sort(currentLabs.getRecords());
  }

  @Override
  public void next() throws SQLException, IOException {
    currentDemo = demographics.next();
    if (currentVitals == null || currentVitals.getPatientId() < currentDemo.getPatientId()) {
      currentVitals = getVitals(currentDemo.getPatientId());
    }
    if (currentVitalsVisits == null || currentVitalsVisits.getPatientId() < currentDemo.getPatientId()) {
      currentVitalsVisits = getVitalsVisits(currentDemo.getPatientId());
    }
    if (currentDepartments == null || currentDepartments.getPatientId() < currentDemo.getPatientId()) {
      currentDepartments = getDepartment(currentDemo.getPatientId());
    }
    if (currentObservation == null || currentObservation.getPatientId() < currentDemo.getPatientId()) {
      currentObservation = getObservation(currentDemo.getPatientId());
    }
    if (currentShcLabs == null || currentShcLabs.getPatientId() < currentDemo.getPatientId()) {
      currentShcLabs = getShcLabs(currentDemo.getPatientId());
    }
    if (currentLpchLabs == null || currentLpchLabs.getPatientId() < currentDemo.getPatientId()) {
      currentLpchLabs = getLpchLabs(currentDemo.getPatientId());
    }
    if (currentExtendedLabs == null || currentExtendedLabs.getPatientId() < currentDemo.getPatientId()) {
      currentExtendedLabs = getExtendedLabs(currentDemo.getPatientId());
    }
    if (options.extractIcd9() && (currentVisit == null || currentVisit.getPatientId() < currentDemo.getPatientId())) {
      currentVisit = getVisit(currentDemo.getPatientId());
    }
    if (options.extractTerms() && (currentTerms == null || currentTerms.getPatientId() < currentDemo.getPatientId())) {
      currentTerms = getTerms(currentDemo.getPatientId());
    }
    if (options.extractIcd9() && (currentMeds == null || currentMeds.getPatientId() < currentDemo.getPatientId())) {
      currentMeds = getMed(currentDemo.getPatientId());
    }
    mergeSortVitals();
    mergeSortLabs();
  }

  @Override
  public VisitDxExtractor getDxExtractor() {
    return visitDxExtractor;
  }

  private PatientRecord<VisitRecord> getVisit(final int patientId) {
    while (visits.hasNext()) {
      final PatientRecord<VisitRecord> visit = visits.next();
      if (visit.getPatientId() >= patientId) {
        return visit;
      }
    }
    return null;
  }

  private PatientRecord<VitalsRecord> getVitals(final int patientId) {
    while (vitals != null && vitals.hasNext()) {
      final PatientRecord<VitalsRecord> vital = vitals.next();
      if (vital.getPatientId() >= patientId) {
        return vital;
      }
    }
    return null;
  }

  private PatientRecord<DepartmentRecord> getDepartment(final int patientId) {
    while (departmentExtractor != null && departmentExtractor.hasNext()) {
      final PatientRecord<DepartmentRecord> dept = departmentExtractor.next();
      if (dept.getPatientId() >= patientId) {
        return dept;
      }
    }
    return null;
  }

  private PatientRecord<VitalsRecord> getVitalsVisits(final int patientId) {
    while (vitalsVisits != null && vitalsVisits.hasNext()) {
      final PatientRecord<VitalsRecord> vital = vitalsVisits.next();
      if (vital.getPatientId() >= patientId) {
        return vital;
      }
    }
    return null;
  }

  private PatientRecord<ObservationRecord> getObservation(final int patientId) {
    while (observationExtractor != null && observationExtractor.hasNext()) {
      final PatientRecord<ObservationRecord> vital = observationExtractor.next();
      if (vital.getPatientId() >= patientId) {
        return vital;
      }
    }
    return null;
  }

  private PatientRecord<LabsRecord> getShcLabs(final int patientId) {
    while (shcLabs != null && shcLabs.hasNext()) {
      final PatientRecord<LabsRecord> labs = shcLabs.next();
      if (labs.getPatientId() >= patientId) {
        return labs;
      }
    }
    return null;
  }

  private PatientRecord<LabsRecord> getLpchLabs(final int patientId) {
    while (lpchLabs != null && lpchLabs.hasNext()) {
      final PatientRecord<LabsRecord> labs = lpchLabs.next();
      if (labs.getPatientId() >= patientId) {
        return labs;
      }
    }
    return null;
  }

  private PatientRecord<LabsRecord> getExtendedLabs(final int patientId) {
    while (extendedLabs != null && extendedLabs.hasNext()) {
      final PatientRecord<LabsRecord> labs = extendedLabs.next();
      if (labs.getPatientId() >= patientId) {
        return labs;
      }
    }
    return null;
  }

  private PatientRecord<TermRecord> getTerms(final int patientId) throws SQLException, IOException {
    if (terms != null) {
      if (!terms.hasNext()) {
        terms.close();
        //initTermExtractor();
      }
      while (terms.hasNext()) {
        final PatientRecord<TermRecord> term = terms.next();
        if (term.getPatientId() >= patientId) {
          return term;
        }
      }
    }
    return null;
  }

  private PatientRecord<MedRecord> getMed(final int patientId) {
    while (med.hasNext()) {
      final PatientRecord<MedRecord> i = med.next();
      if (i.getPatientId() >= patientId) {
        return i;
      }
    }
    return null;
  }

  @Override
  public PatientRecord<DepartmentRecord> getCurrentDepartments() {
    return currentDepartments;
  }

}