package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.IntKeyStringMap;

public class VisitSourceAdmitDe extends Stride7Source {

  private final IntKeyObjectMap<HashSet<String>> dxToIcd9Code;
  private final IntKeyObjectMap<HashSet<String>> dxToIcd10Code;
  private final IntKeyStringMap                  dxToPrimaryCode9;
  private final IntKeyStringMap                  dxToPrimaryCode10;

  public VisitSourceAdmitDe(final ResultSet set, final int patientIdColumn, final IntKeyObjectMap<HashSet<String>> dxToIcd9Code,
      final IntKeyObjectMap<HashSet<String>> dxToIcd10Code, final IntKeyStringMap dxToPrimaryCode9, final IntKeyStringMap dxToPrimaryCode10) {
    super(set, patientIdColumn);
    this.dxToIcd9Code = dxToIcd9Code;
    this.dxToIcd10Code = dxToIcd10Code;
    this.dxToPrimaryCode9 = dxToPrimaryCode9;
    this.dxToPrimaryCode10 = dxToPrimaryCode10;
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //patient_id, age_at_admit_in_days, age_at_disch_in_days, dx_id, YEAR(admit_date_time)
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      addProcessed();
      final int dxId = set.getInt(4);
      if (!set.wasNull()) {
        String primaryCode = dxToPrimaryCode9.get(dxId);
        final HashSet<String> icd9Codes = dxToIcd9Code.get(dxId);
        final HashSet<String> icd10Codes = dxToIcd10Code.get(dxId);
        if (primaryCode != null && (icd9Codes == null || !icd9Codes.contains(primaryCode))) {
          writeSingleLine(writer, primaryCode, "ICD9");
        }
        primaryCode = dxToPrimaryCode10.get(dxId);
        if (primaryCode != null && (icd10Codes == null || !icd10Codes.contains(primaryCode))) {
          writeSingleLine(writer, primaryCode, "ICD10");
        }
        writeLine(writer, icd9Codes, "ICD9");
        writeLine(writer, dxToIcd10Code.get(dxId), "ICD10");
      }
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }

  private void writeLine(final BinaryFileWriter writer, final HashSet<String> codes, final String sab) throws IOException, SQLException {
    if (codes != null) {
      for (final String code : codes) {
        writeSingleLine(writer, code, sab);
      }
    }
  }

  private void writeSingleLine(final BinaryFileWriter writer, final String code, final String sab) throws IOException, SQLException {
    if (sab.equals("ICD9") && code.startsWith("N")) {
      throw new UnsupportedOperationException(set.getInt(patientIdColumn) + " / ");
    }
    double age = set.getDouble(2);
    if (set.wasNull()) {
      age = Double.NEGATIVE_INFINITY;
    }
    writer.write(1, //
        set.getInt(patientIdColumn), //
        set.getInt(5), //
        age, //
        "UNKNOWN", //
        set.getDouble(3) - set.getDouble(2), //
        code, //
        sab, //
        String.valueOf(-1));
  }

}
