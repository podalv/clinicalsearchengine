package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.Extractor;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.DepartmentRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.NoteRecord;
import com.podalv.extractor.datastructures.records.ObservationRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitDxRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;

public interface DatabaseConnectorsInterface {

  public Extractor<NoteRecord> getNotesExtractor();

  public PatientRecord<MedRecord> getCurrentMeds();

  public boolean hasNext();

  public DemographicsRecord getCurrentDemo();

  public PatientRecord<LabsRecord> getCurrentLabs();

  public PatientRecord<VitalsRecord> getCurrentVitals();

  public PatientRecord<DepartmentRecord> getCurrentDepartments();

  public PatientRecord<TermRecord> getCurrentTerms();

  public PatientRecord<VisitRecord> getCurrentVisit();

  public PatientRecord<ObservationRecord> getCurrentObservation();

  public void next() throws SQLException, IOException;

  public Extractor<VisitDxRecord> getDxExtractor();

}
