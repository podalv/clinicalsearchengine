package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.stride6.Common;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.IntKeyStringMap;

public class VisitSourceDxPatEnc extends Stride7Source {

  private final IntKeyObjectMap<HashSet<String>> dxToIcd9Code;
  private final IntKeyObjectMap<HashSet<String>> dxToIcd10Code;
  private final IntKeyStringMap                  dxToPrimaryCode9Map;
  private final IntKeyStringMap                  dxToPrimaryCode10Map;

  public VisitSourceDxPatEnc(final ResultSet set, final int patientIdColumn, final IntKeyObjectMap<HashSet<String>> dxToCode, final IntKeyObjectMap<HashSet<String>> dxToIcd10Code,
      final IntKeyStringMap dxToPrimaryCode9, final IntKeyStringMap dxToPrimaryCode10) {
    super(set, patientIdColumn);
    this.dxToIcd9Code = dxToCode;
    this.dxToIcd10Code = dxToIcd10Code;
    this.dxToPrimaryCode9Map = dxToPrimaryCode9;
    this.dxToPrimaryCode10Map = dxToPrimaryCode10;
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //patient_id, age_at_contact_in_days, age_at_contact_in_days, dx_id, YEAR(contact_date), primary_dx_yn
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      addProcessed();
      final int dxId = set.getInt(4);
      if (!set.wasNull()) {
        final HashSet<String> icd9Codes = dxToIcd9Code.get(dxId);
        final HashSet<String> icd10Codes = dxToIcd10Code.get(dxId);
        String primaryCode = dxToPrimaryCode9Map.get(dxId);
        final String primaryText = set.getString(6);
        final boolean isPrimary = primaryText != null && primaryText.equals("Y");

        if (isPrimary && primaryCode != null) {
          writeSingleLine(primaryCode, writer, isPrimary, "ICD9");
        }
        else {
          primaryCode = null;
        }

        writeCodes(icd9Codes, writer, isPrimary, "ICD9", primaryCode);
        primaryCode = dxToPrimaryCode10Map.get(dxId);
        if (isPrimary && primaryCode != null) {
          writeSingleLine(primaryCode, writer, isPrimary, "ICD10");
        }
        else {
          primaryCode = null;
        }

        writeCodes(icd10Codes, writer, isPrimary, "ICD10", primaryCode);
      }

      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }

  private void writeCodes(final HashSet<String> codes, final BinaryFileWriter writer, final boolean isPrimary, final String sab, final String primaryCodeWritten)
      throws IOException, SQLException {
    if (codes != null) {
      final Iterator<String> i = codes.iterator();
      while (i.hasNext()) {
        final String code = i.next();
        if (primaryCodeWritten == null || !code.equals(primaryCodeWritten)) {
          if (sab.equals("ICD9") && code.startsWith("N")) {
            throw new UnsupportedOperationException(set.getInt(patientIdColumn) + " / ");
          }
          writeSingleLine(code, writer, isPrimary, sab);
        }
      }
    }
  }

  private void writeSingleLine(final String code, final BinaryFileWriter writer, final boolean isPrimary, final String sab) throws IOException, SQLException {
    if (sab.equals("ICD9") && code.startsWith("N")) {
      throw new UnsupportedOperationException(set.getInt(patientIdColumn) + " / ");
    }
    double age = set.getDouble(2);
    if (set.wasNull()) {
      age = Double.NEGATIVE_INFINITY;
    }
    writer.write(1, //
        set.getInt(patientIdColumn), //
        set.getInt(5), //
        age, //
        "UNKNOWN", //
        set.getDouble(3) - set.getDouble(2), //
        code, //
        sab, //
        isPrimary ? Common.PRIMARY_CODE : null);
  }

}