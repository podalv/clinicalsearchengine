package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.extractor.datastructures.BinaryFileWriter;

public class VisitSourcePxCpt extends Stride7Source {

  public VisitSourcePxCpt(final ResultSet set, final int patientIdColumn) {
    super(set, patientIdColumn);
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //patient_id, age_at_contact_in_days, YEAR(contact_date), code, sab, visit_type
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {
      final String code = set.getString(6);
      final String sab = set.getString(5);
      if (sab != null && code != null) {
        addProcessed();
        writer.write(1, //
            set.getInt(patientIdColumn), //
            set.getInt(3), //
            set.getDouble(2), //
            set.getString(6), //
            0, //
            set.getString(4), //
            set.getString(5).equals("ICD9CM") ? "ICD9" : "CPT", //
            null);
      }
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }
}