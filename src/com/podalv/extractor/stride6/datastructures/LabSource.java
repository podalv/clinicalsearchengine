package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;

public class LabSource extends Stride7Source {

  private final HashMap<String, String> transformedLabNames;
  private final HashSet<String>         ignoreLabTextValues;

  public LabSource(final ResultSet set, final int patientIdColumn, final HashMap<String, String> transformedLabNames, final HashSet<String> ignoreLabTextValues) {
    super(set, patientIdColumn);
    this.transformedLabNames = transformedLabNames;
    this.ignoreLabTextValues = ignoreLabTextValues;
  }

  public static boolean isLabNameValid(final String labName) {
    return !labName.equals("Date") && !labName.equals("Type") && !labName.equals("Date test completed") && //
        !labName.equals("Date of test(s)") && !labName.equals("ALT Date") && !labName.equals("LMP Start date") && //
        !labName.equals("DATE RECEIVED") && !labName.equals("DATE VIRAL LOAD COLLECTED") && !labName.equals("Source");
  }

  public static double getOrdValue(String value) {
    try {
      value = value.trim();
      if (value.startsWith("<") || value.startsWith(">")) {
        value = value.substring(1);
      }
      if (value.endsWith("%")) {
        value = value.substring(0, value.length() - 1);
      }
      return Double.parseDouble(value);
    }
    catch (final Exception e) {
      return Stride6DatabaseDownload.UNDEFINED;
    }
  }

  @Override
  public void write(final BinaryFileWriter writer) throws IOException, SQLException {
    //patient_id, age_at_lab_in_days, YEAR(lab_time), component_text, labvalue, LOINC_CODE
    final int patientId = getPatientId();
    while (patientId == getPatientId()) {

      double labValue = Stride6DatabaseDownload.UNDEFINED;
      String labValueAsText = set.getString(5);
      labValue = getOrdValue(set.getString(5));
      if (set.wasNull() || (labValueAsText != null && labValueAsText.isEmpty())) {
        labValue = Stride6DatabaseDownload.UNDEFINED;
        labValueAsText = null;
      }
      final String loinc = set.getString(6);
      final String labName = set.getString(4);
      final String transformedLabName = transformLabName(loinc, labName);

      if (transformedLabName != null) {
        if ((labValueAsText == null || labValueAsText.trim().isEmpty() || ignoreLabTextValues.contains(transformedLabName)) && labValue <= Stride6DatabaseDownload.UNDEFINED) {
          writeNoValue(writer, set, transformedLabName);
        }
        else if (labValue > Stride6DatabaseDownload.UNDEFINED) {
          writeOnlyValue(writer, set, transformedLabName, labValue);
        }
        else {
          writeOnlyText(writer, set, transformedLabName, labValueAsText);
        }
        addProcessed();
      }
      if (!set.next() || patientId != getPatientId()) {
        break;
      }
    }
  }

  private String transformLabName(final String loinc, final String labName) {
    String transformedLabName = transformedLabNames.get(labName);
    if (loinc != null) {
      transformedLabName = loinc;
    }
    else {
      if (transformedLabName == null && !(transformedLabNames.containsKey(labName))) {
        transformedLabName = labName;
        if (transformedLabName == null) {
          return null;
        }
        if (transformedLabName.endsWith(" (Outside)")) {
          transformedLabName = transformedLabName.substring(0, transformedLabName.length() - " (Outside)".length()).trim();
        }
        if (!isLabNameValid(transformedLabName)) {
          transformedLabNames.put(labName, null);
        }
        else {
          transformedLabNames.put(labName, transformedLabName);
        }
      }
    }
    return transformedLabName;
  }
}