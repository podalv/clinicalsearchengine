package com.podalv.extractor.stride6.datastructures;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.map.IntKeyOpenHashMap;
import com.podalv.maps.primitive.map.LongKeyOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectMap;

public class MedicationSource extends Stride7Source {

  public static final String                                        STATUS_CANCELED         = "Canceled";
  public static final String                                        STATUS_DENIED           = "Denied Approval";
  public static final int                                           ORDERING_MODE_INPATIENT = 2;
  private final IntKeyOpenHashMap                                   routeIdToRoute;
  private final IntKeyOpenHashMap                                   statusIdToStatus;
  private final LongKeyOpenHashMap                                  medicationIdToRxCui;
  private int                                                       statusIdColumn          = -1;
  private int                                                       statusStringColumn      = -1;
  private final IntKeyObjectMap<IntKeyObjectMap<ArrayList<Double>>> admin;
  private long                                                      deniedStatusId          = -1;
  private long                                                      canceledStatusId        = -1;
  private int[]                                                     historicalClasses       = new int[0];

  public MedicationSource(final ResultSet set, final int patientColumnId, final LongKeyOpenHashMap rxCuiMap, final IntKeyOpenHashMap routeMap, final IntKeyOpenHashMap statusMap,
      final IntKeyObjectMap<IntKeyObjectMap<ArrayList<Double>>> admin) {
    super(set, patientColumnId);
    this.routeIdToRoute = routeMap;
    this.statusIdToStatus = statusMap;
    this.medicationIdToRxCui = rxCuiMap;
    this.admin = admin;
    getDeniedCanceledStatusIds();
  }

  private void getDeniedCanceledStatusIds() {
    final IntKeyMapIterator iterator = statusIdToStatus.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().equals(STATUS_CANCELED)) {
        canceledStatusId = iterator.getKey();
      }
      if (iterator.getValue().equals(STATUS_DENIED)) {
        deniedStatusId = iterator.getKey();
      }
    }
    if (deniedStatusId == -1 || canceledStatusId == -1) {
      throw new UnsupportedOperationException(
          "'Canceled' or 'Denied Approval' status not found in the dictionary. Code depends on this status to filter-out canceled medications. Cannot proceed");
    }
  }

  public MedicationSource setHistoricalClasses(final int[] historicalClasses) {
    this.historicalClasses = Arrays.copyOf(historicalClasses, historicalClasses.length);
    return this;
  }

  public MedicationSource setStatusIdColumn(final int columnId) {
    this.statusIdColumn = columnId;
    return this;
  }

  public MedicationSource setStatusStringColumn(final int columnId) {
    this.statusStringColumn = columnId;
    return this;
  }

  private ArrayList<Double> getTimes(final int patientId, final int medId) {
    if (admin.containsKey(patientId)) {
      final IntKeyObjectMap<ArrayList<Double>> s = admin.get(patientId);
      if (s.containsKey(medId)) {
        return s.get(medId);
      }
    }
    return new ArrayList<>();
  }

  @Override
  public void write(final BinaryFileWriter writer) throws SQLException, IOException {
    final int pid = getPatientId();
    if (pid == Integer.MAX_VALUE) {
      return;
    }
    while (pid == getPatientId()) {
      final int medId = set.getInt(2);
      if (!set.wasNull()) {
        final int routeId = set.getInt(6);
        final int visitId = set.getInt(7);
        final int classId = set.getInt(12);
        final IntOpenHashSet rxCui = (IntOpenHashSet) medicationIdToRxCui.get(medId);
        boolean isStatusValid = true;
        if (rxCui != null) {
          String status = "N/A";
          if (statusIdColumn != -1) {
            final int statusId = set.getInt(5);
            if (statusId == canceledStatusId || statusId == deniedStatusId) {
              // do not extract canceled
              isStatusValid = false;
            }
            status = (String) statusIdToStatus.get(statusId);
          }
          else if (statusStringColumn != -1) {
            status = set.getString(5);
            if (status != null && (status.equals(STATUS_CANCELED) || status.equals(STATUS_DENIED))) {
              // do not extract canceled
              isStatusValid = false;
            }
          }
          if (isStatusValid) {
            double offset = set.getDouble(3);
            final double start = set.getDouble(8);
            if (start > 0) {
              offset = start;
            }
            double end = set.getDouble(10);
            if (end <= 0) {
              end = set.getDouble(9);
            }
            if (end <= 0) {
              end = offset;
            }
            final int orderingMode = set.getInt(11);
            final ArrayList<Double> times = getTimes(pid, visitId);
            Collections.sort(times);
            if (times.size() != 0) {
              end = -1;
            }

            boolean isHistorical = false;
            if (historicalClasses.length != 0) {
              for (final int h : historicalClasses) {
                if (h == classId) {
                  isHistorical = true;
                  break;
                }
              }
            }

            if (times.size() != 0) {
              status = "inpatient administered";
            }
            else if (orderingMode == ORDERING_MODE_INPATIENT && times.size() == 0) {
              status = "inpatient prescribed";
            }
            else {
              status = isHistorical ? "historical" : orderingMode == -1 ? "unknown (hl7)" : "outpatient";
            }

            if (times.size() == 0) {
              times.add(offset);
            }
            double prevTime = -1;
            final Iterator<Double> ti = times.iterator();
            while (ti.hasNext()) {
              final double time = ti.next();
              if (Double.compare(time, prevTime) == 0) {
                continue;
              }
              prevTime = time;
              final IntIterator i = rxCui.iterator();
              while (i.hasNext()) {
                final int rx = i.next();
                writer.write(set.getInt(1), //
                    time, //
                    set.getInt(4), //
                    status, //
                    rx, //
                    end == -1 ? time : end, //
                    routeIdToRoute.get(routeId));
              }
            }
            addProcessed();
          }
        }
        else {
          addRejected();
        }
      }
      if (!set.next() || pid != set.getInt(1)) {
        break;
      }
    }
  }

  @Override
  public int getPatientId() throws SQLException {
    if (set.isBeforeFirst()) {
      set.next();
    }
    if (set.isAfterLast()) {
      return Integer.MAX_VALUE;
    }
    return set.getInt(1);
  }

}