package com.podalv.extractor.stride6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.stride6.extractors.TermExtractor;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.utils.file.FileUtils;

/** Downloads the ATLAS schema into binary files
 *
 * @author podalv
 *
 */
public class AtlasDatabaseDownload {

  public static final String          CODE_TYPE_CPT           = "CPT";
  public static final String          CODE_TYPE_LABS          = "LAB";
  public static final String          CODE_TYPE_VITALS        = "VITAL";
  public static final String          CODE_TYPE_ICD9          = "ICD9";
  public static final String          CODE_TYPE_ICD10         = "ICD10";
  public static final String          CODE_TYPE_ICD9_PRIMARY  = "ICD9_DX";
  public static final String          CODE_TYPE_ICD10_PRIMARY = "ICD10_DX";
  public static final String          CODE_TYPE_RX            = "RX";
  public static final String          CODE_TYPE_SNOMED        = "SNOMED";
  public static final String          DATABASE                = "$DATABASE";
  private static final String         WHERE                   = "$WHERE$";
  public static final String[]        CODE_DICTIONARY_COLUMNS = new String[] {"code_type", "code", "text"};
  public static final String[]        DEMOGRAPHICS_COLUMNS    = new String[] {"pid", "gender", "race", "ethnicity", "dob", "dod", "dod_year"};
  public static final String[]        ENCOUNTERS_COLUMNS      = new String[] {"pid", "code_type", "code", "visit_type", "department", "primary_diagnosis", "start_time",
      "end_time"};
  public static final String[]        PRESCRIPTIONS_COLUMNS   = new String[] {"pid", "code", "status", "route", "start_time", "end_time"};
  public static final String[]        MEASUREMENTS_COLUMNS    = new String[] {"pid", "code_type", "code", "date", "value", "computed_value"};
  public static final String[]        TERM_DICTIONARY_COLUMNS = new String[] {"tid", "text"};
  public static final String[]        NOTES_COLUMNS           = new String[] {"pid", "nid", "date", "note_type"};
  public static final String[]        TERM_MENTIONS_COLUMNS   = new String[] {"pid", "nid", "tid", "negated", "family_history"};
  public static final String          CODE_DICTIONARY         = "SELECT code_type, code, text FROM " + DATABASE + ".code_dictionary";
  public static final String          DEMOGRAPHICS            = "SELECT pid, gender, race, ethnicity, dob, dod, YEAR(dod) FROM " + DATABASE + ".demographics ORDER BY pid";
  public static final String          ENCOUNTERS              = "SELECT pid, code_type, code, visit_type, department, primary_diagnosis, start_time, end_time FROM " + DATABASE
      + ".encounters  ORDER BY pid";
  public static final String          PRESCRIPTIONS           = "SELECT pid, code, status, route, start_time, end_time FROM " + DATABASE + ".prescriptions  ORDER BY pid";
  public static final String          MEASUREMENTS            = "SELECT pid, code_type, code, date, value, computed_value FROM " + DATABASE + ".measurements  ORDER BY pid";
  public static final String          TERM_DICTIONARY         = "SELECT tid, text FROM " + DATABASE + ".term_dictionary";
  public static final String          NOTES                   = "SELECT pid, nid, date, note_type FROM " + DATABASE + ".notes  ORDER BY pid";
  public static final String          TERM_MENTIONS           = "SELECT pid, nid, tid, negated, family_history FROM " + DATABASE + ".term_mentions " + WHERE + " ORDER BY pid";
  public static final String          MAX_PATIENT_ID          = "SELECT max(pid) FROM " + DATABASE + ".demographics";
  private final String                databaseName;
  private final ConnectionSettings    settings;
  private final File                  outputFolder;
  private final IntKeyObjectMap<Date> pidToDob                = new IntKeyObjectMap<>();

  public AtlasDatabaseDownload(final ConnectionSettings connection, final String databaseName, final File outputFolder) {
    this.databaseName = databaseName;
    this.outputFolder = outputFolder;
    this.settings = connection;
  }

  private double getOffset(final int personId, final Date date) {
    return Common.minutesToDays((int) ((date.getTime() - pidToDob.get(personId).getTime()) / 60000));
  }

  private int getMaxPatientCnt() throws SQLException, IOException {
    final Database db = Database.createInstance(settings);
    final ResultSet set = query(MAX_PATIENT_ID);
    set.next();
    final int maxPatientId = set.getInt(1);
    db.close();
    return maxPatientId;
  }

  private void extractCodeDictionary(final ResultSet set, final File output) throws SQLException, IOException {
    final BufferedWriter writer = new BufferedWriter(new FileWriter(output));
    while (set.next()) {
      writer.write(Common.filterString(set.getString(1)) + "\t" + Common.filterString(set.getString(2)) + "\t" + Common.filterString(set.getString(3)) + "\n");
    }
    writer.close();
  }

  private Integer getYear(final Date date) {
    if (date != null) {
      final Calendar start = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      start.setTime(date);
      return start.get(Calendar.YEAR);
    }
    return -1;
  }

  private String getSab(final String codeType, final boolean isPrimary) {
    if (codeType.equals(CODE_TYPE_ICD9)) {
      return isPrimary ? CODE_TYPE_ICD9_PRIMARY : CODE_TYPE_ICD9;
    }
    else if (codeType.equals(CODE_TYPE_ICD10)) {
      return isPrimary ? CODE_TYPE_ICD10_PRIMARY : CODE_TYPE_ICD10;
    }
    else if (codeType.equals(CODE_TYPE_CPT)) {
      return CODE_TYPE_CPT;
    }
    else {
      throw new UnsupportedOperationException("Code type '" + codeType + "' is not supported for visits. Use ICD9, ICD10 or CPT");
    }
  }

  private void extractEncounters(final ResultSet set, final File visitFile, final File visitDxFile) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(visitFile, Commands.VISITS_STRUCTURE);
    final BinaryFileWriter writerDx = new BinaryFileWriter(visitDxFile, Commands.VISIT_DX_STRUCTURE);
    while (set.next()) {
      final int pid = set.getInt(1);
      int visitId = -1;
      if (set.getInt(6) == 1) {
        visitId = 1;
      }
      // SAB = DX_ID if it is a primary diagnosis
      //       ICD??? if it is ICD9 code
      //       CPT for CPT codes
      writer.write(visitId, // visit id
          pid, // pid
          getYear(set.getTimestamp(7)), // year
          getOffset(pid, set.getTimestamp(7)), // age
          set.getString(4) == null ? "EMPTY" : set.getString(4), // src visit
          getOffset(pid, set.getTimestamp(8)) - getOffset(pid, set.getTimestamp(7)), // duration
          set.getString(3), // code
          getSab(set.getString(2), set.getInt(6) == 1), // sab
          "" // source code

      );
    }
    writerDx.write(1, 1, "Y");
    writer.close();
    writerDx.close();
  }

  private void extractPrescriptions(final ResultSet set, final File output) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.MEDS_STRUCTURE);
    while (set.next()) {
      final int pid = set.getInt(1);
      writer.write(pid, // pid
          getOffset(pid, set.getTimestamp(5)), // age
          getYear(set.getTimestamp(5)), // year
          set.getString(3), // status
          set.getInt(2), // RX CUI
          getOffset(pid, set.getTimestamp(6)), // end date
          set.getString(4) // route
      );
    }
    writer.close();
  }

  private void extractMeasurements(final ResultSet set, final File outputLabs, final File outputVitals) throws SQLException, IOException {
    final BinaryFileWriter writerLabs = new BinaryFileWriter(outputLabs, Commands.LABS_EXTENDED_STRUCTURE);
    final BinaryFileWriter writerVitals = new BinaryFileWriter(outputVitals, Commands.VITALS_STRUCTURE);
    while (set.next()) {
      final int pid = set.getInt(1);
      //pid, code_type, code, date, value, computed_value
      final String codeType = set.getString(2);
      if (codeType.equals(CODE_TYPE_VITALS)) {
        writerVitals.write(set.getInt(1), //
            1, //
            set.getTimestamp(4) == null ? -1 : getOffset(pid, set.getTimestamp(4)), //
            set.getString(3) == null ? "" : set.getString(3), //
            set.getDouble(5), //
            getYear(set.getTimestamp(4)));
      }
      else if (codeType.equals(CODE_TYPE_LABS)) {
        String textValue = set.getString(6);
        if (set.wasNull() || textValue == null || textValue.isEmpty()) {
          textValue = null;
        }
        double numericValue = set.getDouble(5);
        if (set.wasNull() && textValue == null) {
          textValue = LabsRecord.NO_VALUE;
          numericValue = Stride6DatabaseDownload.UNDEFINED;
        }
        writerLabs.write(set.getInt(1), //
            set.getTimestamp(4) == null ? -1 : getOffset(pid, set.getTimestamp(4)), //
            getYear(set.getTimestamp(4)), //
            set.getString(3) == null ? "" : set.getString(3), //
            textValue, //
            numericValue);
      }
    }
    writerLabs.close();
    writerVitals.close();
  }

  private void extractTermDictionary(final ResultSet set, final File output) throws SQLException, IOException {
    final BufferedWriter writer = new BufferedWriter(new FileWriter(output));
    while (set.next()) {
      writer.write(set.getInt(1) + "\t" + set.getString(2) + "\n");
    }
    writer.close();
  }

  private void extractNotes(final ResultSet set, final File output) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.NOTES_STRUCTURE);
    while (set.next()) {
      final String noteType = set.getString(4);
      double age = -1;
      if (set.getTimestamp(3) != null) {
        age = getOffset(set.getInt(1), set.getTimestamp(3));
      }
      writer.write(set.getInt(2), // nid
          noteType == null ? "" : noteType, // note type
          age, // age
          getYear(set.getTimestamp(3)) // year
      );
    }
    writer.close();
  }

  private void extractTermMentions(final File output) throws SQLException, IOException {
    int currentPatient = 0;
    final int maxPatientId = getMaxPatientCnt();
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.TERMS_STRUCTURE);
    while (currentPatient < maxPatientId) {
      final ResultSet set = query(Commands.getTermsQuery(transformDatabaseName(TERM_MENTIONS), currentPatient, "pid"));
      while (set.next()) {
        writer.write(set.getInt(1), //
            set.getInt(2), //
            set.getInt(3), //
            Common.encodeNegatedFamilyHistory(set.getInt(TermExtractor.NEGATED_COLUMN), set.getInt(TermExtractor.FH_COLUMN)));
      }
      currentPatient += (Commands.MAX_TERM_MENTIONS_INCREMENT + 1);
      set.close();
    }
    writer.close();
  }

  private void extractDemographics(final ResultSet set, final File output) throws SQLException, IOException {
    final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.DEMOGRAPHICS_STRUCTURE);
    while (set.next()) {
      final Date dob = set.getTimestamp(5);
      if (dob == null) {
        continue;
      }
      cal.setTime(dob);
      final int pid = set.getInt(1);
      pidToDob.put(pid, cal.getTime());
      double dod = 0;
      if (set.getTimestamp(6) != null) {
        dod = getOffset(pid, set.getTimestamp(6));
      }
      writer.write(pid, //
          set.getString(2), //
          set.getString(3), //
          set.getString(4), //
          dod, //
          set.getInt(7));
    }
    writer.close();
  }

  private String transformDatabaseName(final String query) {
    return query.replace(DATABASE, databaseName);
  }

  private ResultSet query(final String queryName) throws SQLException {
    return Database.createInstance(settings).query(transformDatabaseName(queryName));
  }

  private File getFile(final String fileName) {
    return new File(outputFolder, fileName);
  }

  public void extract() throws SQLException, IOException {
    extractCodeDictionary(query(CODE_DICTIONARY), getFile(Stride6DatabaseDownload.CODE_DICTIONARY_FILE));
    extractDemographics(query(DEMOGRAPHICS), getFile(Stride6DatabaseDownload.DEMOGRAPHICS_FILE));
    extractEncounters(query(ENCOUNTERS), getFile(Stride6DatabaseDownload.VISITS_FILE), getFile(Stride6DatabaseDownload.VISIT_DX_FILE));
    extractPrescriptions(query(PRESCRIPTIONS), getFile(Stride6DatabaseDownload.MEDS_FILE));
    extractMeasurements(query(MEASUREMENTS), getFile(Stride6DatabaseDownload.LABS_FILE), getFile(Stride6DatabaseDownload.VITALS_FILE));
    extractTermDictionary(query(TERM_DICTIONARY), getFile(Stride6DatabaseDownload.TERM_DICTIONARY_FILE));
    extractNotes(query(NOTES), getFile(Stride6DatabaseDownload.NOTES_FILE));
    extractTermMentions(getFile(Stride6DatabaseDownload.TERMS_FILE));
    extractCodeDictionary(query(CODE_DICTIONARY), getFile(Stride6DatabaseDownload.CODE_DICTIONARY_FILE));
  }

  public static void main(final String[] args) throws SQLException, IOException {
    System.out.println("Usage: settings_file");
    System.out.println("Settings file format:");
    System.out.println("DB URL=jdbc:mysql://LOCATION:PORT");
    System.out.println("LOGIN=database login credentials");
    System.out.println("PASSWORD=database login credentials");
    System.out.println("DATABASE NAME=database name containing the ATLAS schema");
    System.out.println("OUTPUT FOLDER=location into which to generate the output files");
    final HashMap<String, String> properties = FileUtils.readPropertiesFile(new File(args[0]), Charset.forName("UTF-8"), "=");
    final AtlasDatabaseDownload download = new AtlasDatabaseDownload(ConnectionSettings.createFromUrlNamePassword(properties.get("DB URL"), properties.get("LOGIN"), properties.get(
        "PASSWORD")), properties.get("DATABASE NAME"), new File(properties.get("OUTPUT FOLDER")));
    download.extract();
  }
}
