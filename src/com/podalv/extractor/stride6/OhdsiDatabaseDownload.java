package com.podalv.extractor.stride6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.db.datastructures.LoginCredentials;
import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.IntKeyStringMap;
import com.podalv.maps.string.StringKeyIntMap;
import com.podalv.search.datastructures.index.StringEnumIndexBuilder;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.text.TextUtils;

/** Downloads the data from the database into the same binary format as Stride6DatabaseDownload
 *
 * @author podalv
 *
 */
public class OhdsiDatabaseDownload {

  private static final int            NO_MATCHING_CONCEPT_ID                         = 0;
  private static long                 MAX_ROW_CNT                                    = Long.MAX_VALUE;
  static final double                 MEDICATION_DURATION_EXTENSION_IN_DAYS          = 30;
  public static final String          VOCABULARY_ICD9                                = "ICD9CM";
  private static final String         VOCABULARY_ICD9_PROC                           = "ICD9Proc";
  public static final String          VOCABULARY_ATC                                 = "ATC";
  public static final String          VOCABULARY_SNOMED                              = "SNOMED";
  public static final String          VOCABULARY_RXNORM                              = "RxNorm";
  public static final String          VOCABULARY_CPT                                 = "CPT4";
  private static final String         VOCABULARY_HCPCS                               = "HCPCS";
  public static final int             MEASUREMENT_TYPE_ID_VITALS                     = 44818701;
  private static final int            ROW_PROGRESS_CNT                               = 100000;
  public static final String          MAX_PATIENT_ID                                 = "SELECT max(person_id) FROM " + AtlasDatabaseDownload.DATABASE + ".person";
  public static final String          ATLAS_TERM_DICTIONARY                          = "SELECT term_id, term_name FROM " + AtlasDatabaseDownload.DATABASE
      + ".atlas_term_dictionary";
  public static final String          RELATIONSHIPS_CONTAINS                         = "SELECT concept_id_1, concept_id_2 FROM " + AtlasDatabaseDownload.DATABASE
      + ".concept_relationship where relationship_id = \"Contains\" AND invalid_reason = \"\"";
  public static final String          RELATIONSHIPS_ATC_LEAF                         = "SELECT concept_id_1, concept_id_2 FROM " + AtlasDatabaseDownload.DATABASE
      + ".concept_relationship where relationship_id = \"ATC - RxNorm\" AND invalid_reason = \"\"";
  public static final String          HIERARCHIES                                    = "SELECT ancestor_concept_id, descendant_concept_id FROM " + AtlasDatabaseDownload.DATABASE
      + ".concept_ancestor";
  public static final String          SYNONYMS                                       = "SELECT concept_id, language_concept_id FROM " + AtlasDatabaseDownload.DATABASE
      + ".concept_synonym";
  public static final String          CONCEPT_QUERY                                  = "SELECT concept_id, concept_name FROM " + AtlasDatabaseDownload.DATABASE + ".concept";
  public static final String          CONCEPT_CODE_QUERY                             = "SELECT concept_id, concept_code, vocabulary_id FROM " + AtlasDatabaseDownload.DATABASE
      + ".concept";
  public static final String          DEATH_QUERY                                    = "SELECT person_id, death_date FROM " + AtlasDatabaseDownload.DATABASE + ".death";
  public static final String          PERSON_QUERY                                   = "SELECT person_id, gender_concept_id, year_of_birth, month_of_birth, day_of_birth, time_of_birth, race_concept_id, ethnicity_concept_id FROM "
      + AtlasDatabaseDownload.DATABASE + ".person ORDER BY person_id";
  public static final String          NOTE_QUERY                                     = "SELECT person_id, note_id, note_type_concept_id, note_date, note_time FROM "
      + AtlasDatabaseDownload.DATABASE + ".note";
  public static final String          CONDITION_OCCURRENCE                           = "SELECT visit_occurrence.person_id, visit_occurrence.visit_concept_id, condition_occurrence.condition_concept_id, condition_occurrence.condition_type_concept_id, condition_occurrence.condition_start_date, condition_occurrence.condition_occurrence_start_time, condition_occurrence.condition_end_date, condition_occurrence.condition_occurrence_end_time FROM "
      + AtlasDatabaseDownload.DATABASE + ".condition_occurrence JOIN (" + AtlasDatabaseDownload.DATABASE
      + ".visit_occurrence) ON (visit_occurrence.visit_occurrence_id = condition_occurrence.visit_occurrence_id) ORDER by person_id";
  public static final String          OBSERVATION_QUERY                              = "SELECT person_id, observation_concept_id, observation_date, observation_time FROM "
      + AtlasDatabaseDownload.DATABASE + ".observation order by person_id";
  public static final String          PROCEDURE_OCCURRENCE                           = "SELECT visit_occurrence.person_id, visit_occurrence.visit_concept_id, procedure_occurrence.procedure_concept_id, procedure_occurrence.procedure_date, procedure_occurrence.procedure_time FROM "
      + AtlasDatabaseDownload.DATABASE + ".procedure_occurrence JOIN (" + AtlasDatabaseDownload.DATABASE
      + ".visit_occurrence) ON (procedure_occurrence.visit_occurrence_id = visit_occurrence.visit_occurrence_id) ORDER BY person_id";
  public static final String          ATLAS_TERM_MENTIONS                            = "SELECT person_id, atlas_term_mentions.note_id, term_id, negated, familyHistory FROM "
      + AtlasDatabaseDownload.DATABASE + ".atlas_term_mentions JOIN (" + AtlasDatabaseDownload.DATABASE
      + ".note) ON (note.note_id = atlas_term_mentions.note_id) $WHERE$ ORDER by person_id";
  public static final String          DRUG_EXPOSURE                                  = "SELECT person_id, drug_concept_id, drug_type_concept_id, route_concept_id, drug_exposure_start_date, drug_exposure_start_time, drug_exposure_end_date, drug_exposure_end_time FROM "
      + AtlasDatabaseDownload.DATABASE + ".drug_exposure order by person_id";
  public static final String          LABS                                           = "SELECT person_id, measurement_concept_id, measurement_date, measurement_time, value_as_number, value_as_concept_id, measurement_type_concept_id FROM "
      + AtlasDatabaseDownload.DATABASE + ".measurement WHERE measurement_concept_id <> 0 ORDER by person_id";
  public static final String          CONCEPT_DICTIONARY                             = "SELECT concept_id, concept_name, vocabulary_id, concept_code FROM "
      + AtlasDatabaseDownload.DATABASE + ".concept";
  private static final int            OBSERVATION_PERSON_ID                          = 1;
  private static final int            OBSERVATION_CONCEPT_ID                         = 2;
  private static final int            OBSERVATION_DATE                               = 3;
  private static final int            OBSERVATION_TIME                               = 4;
  private static final int            ATLAS_TERM_DICTIONARY_TID                      = 1;
  private static final int            ATLAS_TERM_DICTIONARY_STR                      = 2;
  private static final int            SYNONYMS_CONCEPT_ID                            = 1;
  private static final int            SYNONYMS_LANGUAGE_CONCEPT_ID                   = 2;
  private static final int            LABS_PERSON_ID                                 = 1;
  private static final int            LABS_MEASUREMENT_ID                            = 2;
  private static final int            LABS_DATE                                      = 3;
  private static final int            LABS_TIME                                      = 4;
  private static final int            LABS_VALUE                                     = 5;
  private static final int            LABS_VALUE_AS_CONCEPT                          = 6;
  private static final int            LABS_TYPE                                      = 7;
  private static final int            CONCEPT_CONCEPT_ID                             = 1;
  private static final int            CONCEPT_CONCEPT_NAME                           = 2;
  private static final int            PERSON_PERSON_ID                               = 1;
  private static final int            PERSON_GENDER                                  = 2;
  private static final int            PERSON_YEAR                                    = 3;
  private static final int            PERSON_MONTH                                   = 4;
  private static final int            PERSON_DAY                                     = 5;
  private static final int            PERSON_TIME                                    = 6;
  private static final int            PERSON_RACE                                    = 7;
  private static final int            PERSON_ETHNICITY                               = 8;
  private static final int            NOTE_PERSON_ID                                 = 1;
  private static final int            NOTE_NOTE_ID                                   = 2;
  private static final int            NOTE_TYPE                                      = 3;
  private static final int            NOTE_DATE                                      = 4;
  private static final int            NOTE_TIME                                      = 5;
  private static final int            CONDITION_OCCURRENCE_PERSON_ID                 = 1;
  private static final int            CONDITION_OCCURRENCE_VISIT_CONCEPT_ID          = 2;
  private static final int            CONDITION_OCCURRENCE_CONDITION_CONCEPT_ID      = 3;
  private static final int            CONDITION_OCCURRENCE_CONDITION_CONCEPT_TYPE_ID = 4;
  private static final int            CONDITION_OCCURRENCE_START_DATE                = 5;
  private static final int            CONDITION_OCCURRENCE_START_TIME                = 6;
  private static final int            CONDITION_OCCURRENCE_END_DATE                  = 7;
  private static final int            CONDITION_OCCURRENCE_END_TIME                  = 8;
  private static final int            PROCEDURE_PERSON_ID                            = 1;
  private static final int            PROCEDURE_VISIT_CONCEPT_ID                     = 2;
  private static final int            PROCEDURE_CONDITION_CONCEPT_ID                 = 3;
  private static final int            PROCEDURE_DATE                                 = 4;
  private static final int            PROCEDURE_TIME                                 = 5;
  private static final int            ATLAS_TERM_MENTIONS_PERSON_ID                  = 1;
  private static final int            ATLAS_TERM_MENTIONS_NOTE_ID                    = 2;
  private static final int            ATLAS_TERM_MENTIONS_TERM_ID                    = 3;
  private static final int            ATLAS_TERM_MENTIONS_NEGATED                    = 4;
  private static final int            ATLAS_TERM_MENTIONS_FAMILY_HISTORY             = 5;
  private static final int            DRUG_EXPOSURE_PERSON_ID                        = 1;
  private static final int            DRUG_EXPOSURE_DRUG_CONCEPT_ID                  = 2;
  private static final int            DRUG_EXPOSURE_DRUG_TYPE                        = 3;
  private static final int            DRUG_EXPOSURE_ROUTE                            = 4;
  private static final int            DRUG_EXPOSURE_START_DATE                       = 5;
  private static final int            DRUG_EXPOSURE_START_TIME                       = 6;
  private static final int            DRUG_EXPOSURE_END_DATE                         = 7;
  private static final int            DRUG_EXPOSURE_END_TIME                         = 8;
  private static final int            CONCEPT_RX_NORM_CONCEPT_ID                     = 1;
  private static final int            CONCEPT_RX_NORM_CODE                           = 2;
  private static final int            CONCEPT_RX_NORM_VOCABULARY_ID                  = 3;

  private final ConnectionSettings    settings;
  private final String                databaseName;
  private final IntKeyStringMap       conceptToName                                  = new IntKeyStringMap();
  private final SnomedDictionary      snomedDictionary                               = new SnomedDictionary(0);
  private final IntKeyIntOpenHashMap  conceptIdToSynonymLanguageId                   = new IntKeyIntOpenHashMap();
  private final StringKeyIntMap       conceptNameToSynonymId                         = new StringKeyIntMap();
  private final IntKeyObjectMap<Date> personIdToDob                                  = new IntKeyObjectMap<>();
  private final IntKeyStringMap       conceptIdToCode                                = new IntKeyStringMap();
  private final IntOpenHashSet        rxNormConceptIds                               = new IntOpenHashSet();
  private final IntOpenHashSet        cptConceptIds                                  = new IntOpenHashSet();
  private final IntOpenHashSet        icd9ConceptIds                                 = new IntOpenHashSet();
  private static final IntOpenHashSet primaryCodeConceptIds                          = new IntOpenHashSet();
  private final StringKeyIntMap       errorMessages                                  = new StringKeyIntMap();
  private final AtomicInteger         uniqueVisitId                                  = new AtomicInteger(1);
  private final IntOpenHashSet        removedPatients                                = new IntOpenHashSet();
  private final IntKeyStringMap       atcConceptIdToCode                             = new IntKeyStringMap();
  private int                         removedMeds                                    = 0;
  private int                         totalMeds                                      = 0;
  private int                         removedVisits                                  = 0;
  private int                         totalVisits                                    = 0;

  static {
    primaryCodeConceptIds.add(44786627);
    primaryCodeConceptIds.add(38000183);
    primaryCodeConceptIds.add(38000184);
    primaryCodeConceptIds.add(38000199);
    primaryCodeConceptIds.add(38000200);
    primaryCodeConceptIds.add(38000215);
    primaryCodeConceptIds.add(38000230);
    primaryCodeConceptIds.add(42894222);
    primaryCodeConceptIds.add(44786628);
  }

  public static IntIterator getPrimaryCodeConceptIds() {
    return primaryCodeConceptIds.iterator();
  }

  public OhdsiDatabaseDownload(final ConnectionSettings settings, final String dbName) {
    this.settings = settings;
    this.databaseName = dbName;
  }

  private String getConceptName(final int conceptId, final boolean useSynonyms) {
    final int synonym = useSynonyms ? conceptIdToSynonymLanguageId.containsKey(conceptId) ? conceptIdToSynonymLanguageId.get(conceptId) : conceptId : conceptId;
    return conceptToName.get(synonym);
  }

  private String transformDatabaseName(final String query) {
    return query.replace(AtlasDatabaseDownload.DATABASE, databaseName);
  }

  private static OhdsiDatabaseDownload readCredentialsFile(final String file) {
    OhdsiDatabaseDownload result = null;
    String login = null;
    String password = null;
    String url = null;
    String database = null;
    try {
      final TextFileReader reader = FileUtils.readFileIterator(new File(file), Charset.forName("UTF-8"));
      while (reader.hasNext()) {
        final String line = reader.next();
        if (line.toLowerCase().startsWith("login")) {
          login = line.substring(line.indexOf(':') + 1).trim();
        }
        if (line.toLowerCase().startsWith("password")) {
          password = line.substring(line.indexOf(':') + 1).trim();
        }
        if (line.toLowerCase().startsWith("url")) {
          url = line.substring(line.indexOf(':') + 1).trim();
        }
        if (line.toLowerCase().startsWith("database")) {
          database = line.substring(line.indexOf(':') + 1).trim();
        }
      }
      reader.close();
    }
    catch (final Exception e) {
      System.err.println("Could not open the credentials file");
    }
    if (login != null && password != null && url != null && database != null) {
      result = new OhdsiDatabaseDownload(new ConnectionSettings(url, new LoginCredentials(login, password)), database);
    }
    return result;
  }

  private static int printStatus(int cnt) {
    if (cnt++ % ROW_PROGRESS_CNT == 0) {
      if (cnt > ROW_PROGRESS_CNT) {
        System.out.println("Read " + (cnt / ROW_PROGRESS_CNT) + " 000 k rows");
      }
    }
    return cnt;
  }

  private void readConcepts(final ResultSet set) throws SQLException {
    int cnt = 0;
    while (set.next()) {
      cnt = printStatus(cnt);
      final String name = set.getString(CONCEPT_CONCEPT_NAME);
      final int conceptId = set.getInt(CONCEPT_CONCEPT_ID);
      final String lowerCasedName = name.toLowerCase();
      if (conceptNameToSynonymId.containsKey(lowerCasedName)) {
        if (conceptNameToSynonymId.get(lowerCasedName) != conceptIdToSynonymLanguageId.get(conceptId)) {
          System.out.println("Duplicate concept names for '" + conceptId + ", " + name + "'");
        }
      }
      else {
        if (conceptIdToSynonymLanguageId.containsKey(conceptId)) {
          conceptNameToSynonymId.put(lowerCasedName, conceptIdToSynonymLanguageId.get(conceptId));
        }
      }
      conceptToName.put(conceptId, name);
    }
  }

  private void readSynonyms(final ResultSet set) throws SQLException {
    int cnt = 0;
    while (set.next()) {
      cnt = printStatus(cnt);
      final int conceptId = set.getInt(SYNONYMS_CONCEPT_ID);
      final int languageConceptId = set.getInt(SYNONYMS_LANGUAGE_CONCEPT_ID);
      final int existingLanguageConceptId = conceptIdToSynonymLanguageId.get(conceptId);
      if (existingLanguageConceptId != 0 && existingLanguageConceptId != languageConceptId) {
        System.out.println("!!! Error multiple synonym definitions for '" + conceptId + " => " + languageConceptId + "'");
      }
      conceptIdToSynonymLanguageId.put(conceptId, languageConceptId);
    }
  }

  private void readRxNorm(final ResultSet set) throws SQLException {
    int cnt = 0;
    while (set.next()) {
      cnt = printStatus(cnt);
      final int conceptId = set.getInt(CONCEPT_RX_NORM_CONCEPT_ID);
      conceptIdToCode.put(conceptId, set.getString(CONCEPT_RX_NORM_CODE));
      if (set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_RXNORM)) {
        rxNormConceptIds.add(conceptId);
      }
      else if (set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_CPT) || set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_HCPCS)) {
        cptConceptIds.add(conceptId);
      }
      else if (set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_ICD9) || set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_ICD9_PROC)) {
        icd9ConceptIds.add(conceptId);
      }
      else if (set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_ATC)) {
        atcConceptIdToCode.put(conceptId, set.getString(CONCEPT_RX_NORM_CODE));
      }
      else if (set.getString(CONCEPT_RX_NORM_VOCABULARY_ID).equals(VOCABULARY_SNOMED)) {
        snomedDictionary.put(set.getString(CONCEPT_RX_NORM_CODE), conceptToName.get(conceptId));
      }
    }
    // This is a workaround so that the No matching concept can be automatically mapped in RXNorm

    conceptIdToCode.put(NO_MATCHING_CONCEPT_ID, NO_MATCHING_CONCEPT_ID + "");
  }

  private IntKeyObjectMap<Date> readDeath(final ResultSet set) throws SQLException {
    final IntKeyObjectMap<Date> result = new IntKeyObjectMap<>();
    int cnt = 0;
    while (set.next()) {
      cnt = printStatus(cnt);
      result.put(set.getInt(CONCEPT_CONCEPT_ID), set.getDate(CONCEPT_CONCEPT_NAME));
    }
    return result;
  }

  private int getOffset(final int personId, final Date date) {
    return (int) Math.round(((date.getTime() - personIdToDob.get(personId).getTime()) / (double) 60000));
  }

  static double calculateDays(final int dateOffset) {
    final double result = (dateOffset) / (double) (60 * 24);
    return (result <= 0.01 && result >= 0) ? 0.01 : result;
  }

  public static int getOffset(final Date date1, final Date date2) {
    if (date1.after(date2)) {
      return (int) Math.round(((date1.getTime() - date2.getTime()) / (double) 60000));
    }
    return (int) Math.round(((date2.getTime() - date1.getTime()) / (double) 60000));
  }

  static int getYear(final Date date) {
    final Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return cal.get(Calendar.YEAR);
  }

  private Date getDate(final ResultSet set) throws SQLException {
    final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    final String time = set.getString(PERSON_TIME);
    int hour = 0;
    int minute = 0;
    int second = 0;
    if (time != null) {
      try {
        final String[] timeSeparated = TextUtils.split(time, ':');
        if (timeSeparated.length == 3) {
          hour = Integer.parseInt(timeSeparated[0]);
          minute = Integer.parseInt(timeSeparated[1]);
          second = Integer.parseInt(timeSeparated[2]);
        }
      }
      catch (final Exception e) {
        Integer val = errorMessages.get("Error parsing time");
        if (val == null) {
          val = Integer.valueOf(0);
        }
        errorMessages.put("Error parsing time", val + 1);
      }
    }
    calendar.set(set.getInt(PERSON_YEAR), set.getInt(PERSON_MONTH) - 1, set.getInt(PERSON_DAY), hour, minute, second);
    return calendar.getTime();
  }

  private Date getDate(final Date date, final String time) throws SQLException {
    if (date == null) {
      return null;
    }
    final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    int hour = 0;
    int minute = 0;
    int second = 0;
    if (time != null) {
      try {
        final String[] timeSeparated = TextUtils.split(time, ':');
        if (timeSeparated.length == 3) {
          hour = Integer.parseInt(timeSeparated[0]);
          minute = Integer.parseInt(timeSeparated[1]);
          second = Integer.parseInt(timeSeparated[2]);
        }
      }
      catch (final Exception e) {
        Integer val = errorMessages.get("Error parsing time");
        if (val == null) {
          val = Integer.valueOf(0);
        }
        errorMessages.put("Error parsing time", val + 1);
      }
    }
    final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    cal.setTime(date);
    calendar.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), hour, minute, second);
    return calendar.getTime();
  }

  private void readPerson(final ResultSet set, final IntKeyObjectMap<Date> deathMap, final File file) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(file, Commands.DEMOGRAPHICS_STRUCTURE);
    int cnt = 0;
    final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    while (set.next()) {
      cnt = printStatus(cnt);
      final Date dob = getDate(set);
      cal.setTime(dob);
      final int personId = set.getInt(PERSON_PERSON_ID);
      personIdToDob.put(personId, cal.getTime());
      final int dod = deathMap.get(personId) == null ? 0 : getOffset(deathMap.get(personId), dob);
      int val = set.getInt(PERSON_GENDER);
      final String genderString = val == 0 ? StringEnumIndexBuilder.UNMAPPED : getConceptName(val, false);
      val = set.getInt(PERSON_ETHNICITY);
      final String ethnicityString = val == 0 ? StringEnumIndexBuilder.UNMAPPED : getConceptName(val, false);
      val = set.getInt(PERSON_RACE);
      final String raceString = val == 0 ? StringEnumIndexBuilder.UNMAPPED : getConceptName(set.getInt(PERSON_RACE), false);
      writer.write(personId, //
          genderString, //
          raceString, //
          ethnicityString, //
          dod > 0 ? calculateDays(dod) : 0, //
          dod > 0 ? getYear(deathMap.get(personId)) : -1);
    }
    writer.close();
  }

  private void readVisits(final ResultSet occurrenceSet, final ResultSet procedureSet, final File output, final File visit_dx) throws SQLException, IOException {
    int cnt = 0;
    final BinaryFileWriter out = new BinaryFileWriter(output, Commands.VISITS_STRUCTURE);
    final BinaryFileWriter out_dx = new BinaryFileWriter(visit_dx, Commands.VISIT_DX_STRUCTURE);
    boolean getNextOccurrence = true;
    boolean getNextProcedure = true;
    while (true) {
      if (cnt >= MAX_ROW_CNT) {
        break;
      }
      if (getNextOccurrence && !occurrenceSet.isAfterLast()) {
        occurrenceSet.next();
      }
      if (getNextProcedure && !procedureSet.isAfterLast()) {
        procedureSet.next();
      }
      getNextOccurrence = false;
      getNextProcedure = false;
      cnt = printStatus(cnt);
      if (procedureSet.isAfterLast() && occurrenceSet.isAfterLast()) {
        break;
      }
      if (!procedureSet.isAfterLast() && !occurrenceSet.isAfterLast() && procedureSet.getInt(PROCEDURE_PERSON_ID) == occurrenceSet.getInt(CONDITION_OCCURRENCE_PERSON_ID)) {
        writeConditionOccurrence(occurrenceSet, out, out_dx);
        writeProcedureOccurrence(procedureSet, out, out_dx);
        getNextOccurrence = true;
        getNextProcedure = true;
      }
      else if (!procedureSet.isAfterLast()) {
        if (occurrenceSet.isAfterLast() || occurrenceSet.getInt(CONDITION_OCCURRENCE_PERSON_ID) > procedureSet.getInt(PROCEDURE_PERSON_ID)) {
          writeProcedureOccurrence(procedureSet, out, out_dx);
          getNextProcedure = true;
        }
        else if (!occurrenceSet.isAfterLast() && occurrenceSet.getInt(CONDITION_OCCURRENCE_PERSON_ID) < procedureSet.getInt(PROCEDURE_PERSON_ID)) {
          writeConditionOccurrence(occurrenceSet, out, out_dx);
          getNextOccurrence = true;
        }
      }
      else if (!occurrenceSet.isAfterLast()) {
        if (procedureSet.isAfterLast() || occurrenceSet.getInt(CONDITION_OCCURRENCE_PERSON_ID) < procedureSet.getInt(PROCEDURE_PERSON_ID)) {
          writeConditionOccurrence(occurrenceSet, out, out_dx);
          getNextOccurrence = true;
        }
        else if (!procedureSet.isAfterLast() && occurrenceSet.getInt(CONDITION_OCCURRENCE_PERSON_ID) > procedureSet.getInt(PROCEDURE_PERSON_ID)) {
          writeProcedureOccurrence(procedureSet, out, out_dx);
          getNextProcedure = true;
        }
      }
    }
    out.close();
    out_dx.close();
  }

  private void readMeds(final ResultSet drugSet, final File output) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.MEDS_STRUCTURE);
    int cnt = 0;
    while (drugSet.next()) {
      totalMeds++;
      cnt = printStatus(cnt);
      if (cnt >= MAX_ROW_CNT) {
        break;
      }
      final int personId = drugSet.getInt(DRUG_EXPOSURE_PERSON_ID);
      final int conceptId = drugSet.getInt(DRUG_EXPOSURE_DRUG_CONCEPT_ID);
      if (!rxNormConceptIds.contains(conceptId) && conceptId != 0) {
        System.out.println("Discarding non-RxNorm medication with conceptId '" + conceptId + "'");
        removedPatients.add(personId);
        removedMeds++;
        continue;
      }
      final Date startDate = getDate(drugSet.getDate(DRUG_EXPOSURE_START_DATE), drugSet.getString(DRUG_EXPOSURE_START_TIME));
      Date endDate = getDate(drugSet.getDate(DRUG_EXPOSURE_END_DATE), drugSet.getString(DRUG_EXPOSURE_END_TIME));
      if (endDate == null) {
        endDate = startDate;
      }
      final Calendar start = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      start.setTime(startDate);
      final double startOffset = calculateDays(getOffset(personId, startDate));
      int rxNorm = 0;
      if (rxNormConceptIds.contains(conceptId)) {
        rxNorm = Integer.parseInt(conceptIdToCode.get(conceptId));
      }
      else {
        removedPatients.add(personId);
        removedMeds++;
      }

      writer.write(personId, //
          startOffset, //
          start.get(Calendar.YEAR), //
          getConceptName(drugSet.getInt(DRUG_EXPOSURE_DRUG_TYPE), false), //
          rxNorm, //
          // adding 30 days to the end of the meds exposure so that we have the same durations as OHDSI tools
          calculateDays(getOffset(personId, endDate)) + MEDICATION_DURATION_EXTENSION_IN_DAYS, //
          getConceptName(drugSet.getInt(DRUG_EXPOSURE_ROUTE), false));
    }
    writer.close();
  }

  private void readLabs(final ResultSet labSet, final File output, final File vitals, final File labNames) throws SQLException, IOException {
    final HashSet<String> uniqueLabNames = new HashSet<>();
    final BinaryFileWriter out = new BinaryFileWriter(output, Commands.LABS_EXTENDED_STRUCTURE);
    final BinaryFileWriter vitalsOut = new BinaryFileWriter(vitals, Commands.VITALS_STRUCTURE);
    final BinaryFileWriter labNamesOut = new BinaryFileWriter(labNames, Commands.LABS_COMPONENT_STRUCTURE);
    int cnt = 0;
    final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    while (labSet.next()) {
      cnt = printStatus(cnt);
      if (cnt >= MAX_ROW_CNT) {
        break;
      }
      final int measurementType = labSet.getInt(LABS_TYPE);
      if (measurementType == MEASUREMENT_TYPE_ID_VITALS) {
        writeVitals(labSet, vitalsOut, calendar);
      }
      else {
        writeLabs(labSet, out, calendar, uniqueLabNames);
      }
    }
    final Iterator<String> iterator = uniqueLabNames.iterator();
    while (iterator.hasNext()) {
      final String name = iterator.next();
      labNamesOut.write(name, //
          name);
    }
    out.close();
    vitalsOut.close();
    labNamesOut.close();
  }

  private void writeVitals(final ResultSet labSet, final BinaryFileWriter out, final Calendar calendar) throws SQLException, IOException {
    final int personId = labSet.getInt(LABS_PERSON_ID);
    final Date date = getDate(labSet.getDate(LABS_DATE), labSet.getString(LABS_TIME));
    final double age = calculateDays(getOffset(personId, date));
    final int measurementId = labSet.getInt(LABS_MEASUREMENT_ID);
    calendar.setTime(date);
    final double value = labSet.getDouble(LABS_VALUE);
    if (value < 10000000 && value > Stride6DatabaseDownload.UNDEFINED) {
      out.write(personId, //
          measurementId, //
          age, //
          getConceptName(measurementId, false), //
          labSet.getDouble(LABS_VALUE), //
          calendar.get(Calendar.YEAR));
    }
  }

  private void writeLabs(final ResultSet labSet, final BinaryFileWriter out, final Calendar calendar, final HashSet<String> uniqueLabNames) throws SQLException, IOException {
    final int personId = labSet.getInt(LABS_PERSON_ID);
    final Date date = getDate(labSet.getDate(LABS_DATE), labSet.getString(LABS_TIME));
    final double age = calculateDays(getOffset(personId, date));
    calendar.setTime(date);
    final String labName = getConceptName(labSet.getInt(LABS_MEASUREMENT_ID), false);
    final double value = labSet.getDouble(LABS_VALUE);
    if (value < 10000000 && value > Stride6DatabaseDownload.UNDEFINED) {
      out.write(personId, //
          age, //
          calendar.get(Calendar.YEAR), //
          labName, //
          getConceptName(labSet.getInt(LABS_VALUE_AS_CONCEPT), false), //
          labSet.getDouble(LABS_VALUE));
    }
    uniqueLabNames.add(labName);
  }

  private void writeProcedureOccurrence(final ResultSet occurrenceSet, final BinaryFileWriter out, final BinaryFileWriter visit_dx) throws SQLException, IOException {
    totalVisits++;
    final int personId = occurrenceSet.getInt(PROCEDURE_PERSON_ID);
    final int conceptId = occurrenceSet.getInt(PROCEDURE_CONDITION_CONCEPT_ID);
    final Date startDate = getDate(occurrenceSet.getDate(PROCEDURE_DATE), occurrenceSet.getString(PROCEDURE_TIME));
    final Calendar start = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    start.setTime(startDate);
    final short year = (short) start.get(Calendar.YEAR);
    final double startOffset = calculateDays(getOffset(personId, startDate));
    final double end = 0;
    final String visitType = getConceptName(occurrenceSet.getInt(PROCEDURE_VISIT_CONCEPT_ID), false);
    final String code = conceptIdToCode.get(conceptId);
    if (cptConceptIds.contains(conceptId)) {
      writeProcedureConditionRecord(out, visit_dx, personId, year, startOffset, 0, visitType, code, "CPT", false);
    }
    else if (icd9ConceptIds.contains(conceptId)) {
      writeProcedureConditionRecord(out, visit_dx, personId, year, startOffset, end, visitType, code, "DX_ID", false);
    }
    else if (snomedDictionary.containsCode(conceptIdToCode.get(conceptId))) {
      writeProcedureConditionRecord(out, visit_dx, personId, year, startOffset, end, visitType, code, "SNOMED", false);
    }
    else {
      removedPatients.add(personId);
      removedVisits++;
      if (conceptId != 0) {
        System.out.println("Discarding visit with non-ICD9, non-CPT, non-SNOMED concept Id = '" + conceptId + "'");
      }
    }
  }

  private void writeProcedureConditionRecord(final BinaryFileWriter out, final BinaryFileWriter visit_dx, final int personId, final short year, final double start,
      final double end, final String visitType, final String code, final String ontology, final boolean primary) throws IOException {
    final int conditionOccurrenceId = uniqueVisitId.incrementAndGet();
    out.write(conditionOccurrenceId, //
        personId, //
        year, //
        start, //
        visitType, //
        end, //
        code, //
        ontology, //
        conditionOccurrenceId + "");
    if (primary) {
      visit_dx.write(conditionOccurrenceId, //
          conditionOccurrenceId, //
          "Y");
    }
  }

  private void writeConditionOccurrence(final ResultSet occurrenceSet, final BinaryFileWriter out, final BinaryFileWriter visit_dx) throws SQLException, IOException {
    totalVisits++;
    final int personId = occurrenceSet.getInt(CONDITION_OCCURRENCE_PERSON_ID);
    final Date startDate = getDate(occurrenceSet.getDate(CONDITION_OCCURRENCE_START_DATE), occurrenceSet.getString(CONDITION_OCCURRENCE_START_TIME));
    final Date endDate = getDate(occurrenceSet.getDate(CONDITION_OCCURRENCE_END_DATE), occurrenceSet.getString(CONDITION_OCCURRENCE_END_TIME));
    final Calendar start = Calendar.getInstance(TimeZone.getTimeZone("PST"));
    start.setTime(startDate);
    final short year = (short) start.get(Calendar.YEAR);
    final double startOffset = calculateDays(getOffset(personId, startDate));
    final String visitType = getConceptName(occurrenceSet.getInt(CONDITION_OCCURRENCE_VISIT_CONCEPT_ID), false);

    start.setTime(endDate);
    // adding 30 days to the end of the meds exposure so that we have the same durations as OHDSI tools
    final double end = (calculateDays(getOffset(personId, endDate)) - startOffset);
    final int conceptId = occurrenceSet.getInt(CONDITION_OCCURRENCE_CONDITION_CONCEPT_ID);
    final String code = conceptIdToCode.get(conceptId);
    final boolean primary = primaryCodeConceptIds.contains(occurrenceSet.getInt(CONDITION_OCCURRENCE_CONDITION_CONCEPT_TYPE_ID));
    if (cptConceptIds.contains(conceptId)) {
      writeProcedureConditionRecord(out, visit_dx, personId, year, startOffset, end, visitType, code, "CPT", primary);
    }
    else if (icd9ConceptIds.contains(conceptId)) {
      writeProcedureConditionRecord(out, visit_dx, personId, year, startOffset, end, visitType, code, "DX_ID", primary);
    }
    else if (snomedDictionary.containsCode(conceptIdToCode.get(conceptId))) {
      writeProcedureConditionRecord(out, visit_dx, personId, year, startOffset, end, visitType, code, "SNOMED", primary);
    }
    else {
      removedPatients.add(personId);
      removedVisits++;
      if (conceptId != 0) {
        System.out.println("Discarding visit with non-ICD9 and non-CPT concept Id = '" + conceptId + "'");
      }
    }
  }

  private void readTidDictionary(final File output) throws SQLException, IOException {
    final BufferedWriter writer = new BufferedWriter(new FileWriter(output));
    final ResultSet termDict = resultSet(ATLAS_TERM_DICTIONARY);
    while (termDict.next()) {
      writer.write(termDict.getInt(ATLAS_TERM_DICTIONARY_TID) + "\t" + termDict.getString(ATLAS_TERM_DICTIONARY_STR) + "\n");
    }
    writer.close();
  }

  private void readTermMentions(final File output) throws SQLException, IOException {
    int currentPatient = 0;
    final int maxPatientId = getMaxPatientCnt();
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.TERMS_STRUCTURE);
    int cnt = 0;
    while (currentPatient < maxPatientId) {
      final ResultSet set = resultSet(Commands.getTermsQuery(transformDatabaseName(ATLAS_TERM_MENTIONS), currentPatient, "person_id"));
      if (cnt >= MAX_ROW_CNT) {
        break;
      }
      while (set.next()) {
        cnt = printStatus(cnt);
        if (cnt >= MAX_ROW_CNT) {
          break;
        }
        final int personId = set.getInt(ATLAS_TERM_MENTIONS_PERSON_ID);
        writer.write(personId, //
            set.getInt(ATLAS_TERM_MENTIONS_NOTE_ID), //
            set.getInt(ATLAS_TERM_MENTIONS_TERM_ID), //
            Common.encodeNegatedFamilyHistory(set.getInt(ATLAS_TERM_MENTIONS_NEGATED), set.getInt(ATLAS_TERM_MENTIONS_FAMILY_HISTORY)));
      }
      currentPatient += (Commands.MAX_TERM_MENTIONS_INCREMENT + 1);
      set.close();
    }
    writer.close();
  }

  private int getMaxPatientCnt() throws SQLException, IOException {
    final Database db = Database.createInstance(settings);
    final ResultSet set = resultSet(MAX_PATIENT_ID);
    set.next();
    final int maxPatientId = set.getInt(1);
    db.close();
    return maxPatientId;
  }

  private void readNote(final ResultSet set, final File output) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.NOTES_STRUCTURE);
    int cnt = 0;
    while (set.next()) {
      cnt = printStatus(cnt);
      if (cnt >= MAX_ROW_CNT) {
        break;
      }
      final Date startDate = getDate(set.getDate(NOTE_DATE), set.getString(NOTE_TIME));
      final Calendar start = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      start.setTime(startDate);
      final short year = (short) start.get(Calendar.YEAR);
      final int personId = set.getInt(NOTE_PERSON_ID);
      final double startOffset = calculateDays(getOffset(personId, startDate));
      final int noteId = set.getInt(NOTE_NOTE_ID);
      final int conceptId = set.getInt(NOTE_TYPE);
      writer.write(noteId, //
          set.wasNull() ? "" : getConceptName(conceptId, false), //
          startOffset, //
          year);
    }
    writer.close();
  }

  private void readObservations(final ResultSet set, final File output) throws SQLException, IOException {
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.OBSERVATION_STRUCTURE);
    int cnt = 0;
    while (set.next()) {
      cnt = printStatus(cnt);
      if (cnt >= MAX_ROW_CNT) {
        break;
      }
      final Date startDate = getDate(set.getDate(OBSERVATION_DATE), set.getString(OBSERVATION_TIME));
      final Calendar start = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      start.setTime(startDate);
      final short year = (short) start.get(Calendar.YEAR);
      final int personId = set.getInt(OBSERVATION_PERSON_ID);
      final double startOffset = calculateDays(getOffset(personId, startDate));
      int conceptId = set.getInt(OBSERVATION_CONCEPT_ID);
      String sab = null;
      if (icd9ConceptIds.contains(conceptId)) {
        sab = "ICD";
      }
      else if (cptConceptIds.contains(conceptId)) {
        sab = "CPT";
      }
      else if (snomedDictionary.containsCode(conceptIdToCode.get(conceptId))) {
        sab = "SNOMED";
      }
      else {
        sab = "";
        conceptId = 0;
      }
      writer.write(personId, //
          set.wasNull() ? "" : conceptIdToCode.get(conceptId), //
          startOffset, //
          year, //
          sab);
    }
    writer.close();
  }

  private ResultSet resultSet(final String query) throws SQLException {
    return Database.createInstance(settings).query(transformDatabaseName(query));
  }

  private ResultSet resultSetStream(final String query) throws SQLException {
    return Database.createInstance(settings).queryStreaming(transformDatabaseName(query));
  }

  public void process(final File outputFolder) throws SQLException, IOException {
    readRxNorm(resultSet(CONCEPT_CODE_QUERY));
    readSynonyms(resultSet(SYNONYMS));
    readConcepts(resultSet(CONCEPT_QUERY));
    snomedDictionary.writeSnomedDictionary(new File(outputFolder, Stride6DatabaseDownload.SNOMED_DICTIONARY_FILE));
    readPerson(resultSet(PERSON_QUERY), readDeath(resultSet(DEATH_QUERY)), new File(outputFolder, Stride6DatabaseDownload.DEMOGRAPHICS_FILE));
    readMeds(resultSetStream(DRUG_EXPOSURE), new File(outputFolder, Stride6DatabaseDownload.MEDS_FILE));
    readNote(resultSet(NOTE_QUERY), new File(outputFolder, Stride6DatabaseDownload.NOTES_FILE));
    readVisits(resultSetStream(CONDITION_OCCURRENCE), resultSetStream(PROCEDURE_OCCURRENCE), new File(outputFolder, Stride6DatabaseDownload.VISITS_FILE), new File(outputFolder,
        Stride6DatabaseDownload.VISIT_DX_FILE));
    try {
      readTidDictionary(new File(outputFolder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE));
      readTermMentions(new File(outputFolder, Stride6DatabaseDownload.TERMS_FILE));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    readLabs(resultSetStream(LABS), new File(outputFolder, Stride6DatabaseDownload.LABS_FILE), new File(outputFolder, Stride6DatabaseDownload.VITALS_FILE), new File(outputFolder,
        Stride6DatabaseDownload.LABS_COMPONENT_FILE));
    readObservations(resultSet(OBSERVATION_QUERY), new File(outputFolder, Stride6DatabaseDownload.OBSERVATION_FILE));
    FileUtils.writeFile(new File(outputFolder, "removed_patients.txt"), removedPatients.toString("\n"));
  }

  public static void main(final String[] args) throws SQLException, IOException {
    System.out.println("Usage: database_credentials_file output_folder [EXTRACT_MAX_ROW_NR]");
    System.out.println();
    System.out.println("database_credentials_file contents:");
    System.out.println("url: database url goes here (example: jdbc:mysql://podalv.domain.edu:3306");
    System.out.println("login: login goes here");
    System.out.println("password: password goes here");
    System.out.println("database: database name containing the OHDSI schema goes here");
    if (args.length > 2) {
      MAX_ROW_CNT = Long.parseLong(args[2]);
    }
    System.out.println("Reading maximum " + MAX_ROW_CNT + " rows");
    final OhdsiDatabaseDownload instance = readCredentialsFile(args[0]);
    if (instance == null) {
      System.err.println("Credentials file does not contain url, database name, login and password information");
      System.exit(1);
    }
    instance.process(new File(args[1]));
    System.out.println(instance.removedPatients.size() + " patients had invalid data (unpammed concept id or data from unsupported ontology)");
    System.out.println("Removed meds / total meds     : " + instance.removedMeds + " / " + instance.totalMeds);
    System.out.println("Removed visits / total visits : " + instance.removedVisits + " / " + instance.totalVisits);
  }
}
