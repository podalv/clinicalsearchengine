package com.podalv.extractor.stride6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;

/** Given a pid, pulls out all the data for a patient from Stride7
 *
 * @author podalv
 *
 */
public class Stride7SqlQueryTool {

  private static HashMap<String, HashSet<String>> medIdLpchEpic  = new HashMap<>();
  private static HashMap<String, HashSet<String>> medIdShcEpic   = new HashMap<>();
  private static HashMap<String, String>          medStatusShc   = new HashMap<>();
  private static HashMap<String, String>          medStatusLpch  = new HashMap<>();
  private static HashMap<String, HashSet<String>> medIdHl7       = new HashMap<>();
  private static HashMap<String, String>          medRoute       = new HashMap<>();
  private static HashMap<String, String>          departmentLpch = new HashMap<>();
  private static HashMap<String, String>          departmentShc  = new HashMap<>();
  private static HashMap<String, String>          visitTypeLpch  = new HashMap<>();
  private static HashMap<String, String>          visitTypeShc   = new HashMap<>();
  private static HashMap<String, String>          dxIdLpch       = new HashMap<>();
  private static HashMap<String, String>          dxIdShc        = new HashMap<>();
  private static HashMap<String, String>          resultFlagLpch = new HashMap<>();
  private static HashMap<String, String>          resultFlagShc  = new HashMap<>();

  static class MappedColumn {

    private final HashMap<String, ?> map;
    private final int                columnId;

    public MappedColumn(final int columnId, final HashMap<String, ?> map) {
      this.columnId = columnId;
      this.map = map;
    }

    public int getColumnId() {
      return columnId;
    }

    @SuppressWarnings("unchecked")
    public Iterator<String> getColumnValue(final String key) {
      HashSet<String> result;
      if (map.get(key) instanceof String) {
        result = new HashSet<>();
        result.add((String) map.get(key));
      }
      else {
        result = (HashSet<String>) map.get(key);
      }

      if (result == null) {
        result = new HashSet<>();
      }
      return result.iterator();
    }
  }

  private static void queryDictionary(final ResultSet set, final HashMap<String, String> map) throws SQLException {
    while (set.next()) {
      map.put(set.getString(1), set.getString(2));
    }
    set.close();
  }

  private static void queryDictionaryMulti(final ResultSet set, final HashMap<String, HashSet<String>> map) throws SQLException {
    while (set.next()) {
      HashSet<String> s = map.get(set.getString(1));
      if (s == null) {
        s = new HashSet<>();
        map.put(set.getString(1), s);
      }
      s.add(set.getString(2));
    }
    set.close();
  }

  private static void queryDictionaries(final Database db) throws SQLException {
    Database.LOGGING_ENABLED = false;
    queryDictionary(db.query(Stride7DatabaseDownload.MED_STATUS_LPCH), medStatusLpch);
    queryDictionary(db.query(Stride7DatabaseDownload.MED_STATUS_SHC), medStatusShc);
    queryDictionaryMulti(db.query(Stride7DatabaseDownload.MED_ID_LPCH_EPIC), medIdLpchEpic);
    queryDictionaryMulti(db.query(Stride7DatabaseDownload.MED_ID_SHC_EPIC), medIdShcEpic);
    queryDictionaryMulti(db.query(Stride7DatabaseDownload.MED_ID_HL7), medIdHl7);
    queryDictionary(db.query(Stride7DatabaseDownload.MED_ROUTE_LPCH), medRoute);
    queryDictionary(db.query(Stride7DatabaseDownload.DICTIONARY_DEPARTMENT_LPCH), departmentLpch);
    queryDictionary(db.query(Stride7DatabaseDownload.DICTIONARY_DEPARTMENT_SHC), departmentShc);
    queryDictionary(db.query(Stride7DatabaseDownload.DICTIONARY_VISIT_TYPE_LPCH), visitTypeLpch);
    queryDictionary(db.query(Stride7DatabaseDownload.DICTIONARY_VISIT_TYPE_SHC), visitTypeShc);
    queryDictionary(db.query("SELECT dx_id, rtrim(concat(ref_bill_code, \" \", current_icd9_list, \" \", current_icd10_list)) FROM stride7.dictionary_diagnoses_LPCH"), dxIdLpch);
    queryDictionary(db.query("SELECT dx_id, rtrim(concat(ref_bill_code, \" \", current_icd9_list, \" \", current_icd10_list)) FROM stride7.dictionary_diagnoses_SHC"), dxIdShc);
  }

  private static void printResultSet(final ResultSet set, final String table, final MappedColumn[] columnMap, final String ... columns) throws SQLException {
    final StringBuilder line = new StringBuilder("TABLE   = " + table + "\nCOLUMNS = ");
    for (int x = 0; x < columns.length; x++) {
      line.append(columns[x]);
      if (x != columns.length - 1) {
        line.append("|");
      }
    }
    boolean added = false;
    while (set.next()) {
      added = true;
      line.append("\n");
      for (int x = 0; x < columns.length; x++) {
        String val = set.getString(x + 1);
        if (set.wasNull()) {
          val = "null";
        }
        else {
          if (columnMap != null) {
            for (final MappedColumn col : columnMap) {
              if (col.getColumnId() == (x + 1)) {
                final Iterator<String> i = col.getColumnValue(val);
                val = "";
                while (i.hasNext()) {
                  val += " " + i.next();
                }
                val = val.trim();
              }
            }
          }
        }
        line.append(val);
        if (x != columns.length - 1) {
          line.append("|");
        }
      }
    }
    if (added) {
      System.out.println(line.toString());
      System.out.println("====================================================");
    }
    set.close();
  }

  public static void main(final String[] args) throws FileNotFoundException, SQLException, IOException {
    System.out.println("Usage: database_settings_file PID");
    final Database db = Database.create(ConnectionSettings.createFromFile(new File(args[0])));
    final String pid = args[1];
    queryDictionaries(db);

    String query = "SELECT patient_id, gender, race, ethnicity, FLOOR(age_at_death_in_days) FROM stride7.demographics WHERE patient_id = " + pid;
    printResultSet(db.query(query), "stride7.demographics", null, "pid", "gender", "race", "ethnicity", "death");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), bp_systolic, bp_diastolic, temperature, pulse, weight_in_lbs, height, respirations, bmi, bsa, YEAR(contact_date) FROM stride7.LPCH_visit_de WHERE (bp_systolic <> 0 OR bp_diastolic <> 0 OR temperature <> 0 OR pulse <> 0 OR weight_in_lbs <> 0 OR height <> \"\" OR respirations <> 0 OR bmi <> 0 OR bsa <> 0) AND patient_id = "
        + pid
        + " UNION SELECT patient_id, age_at_contact_in_days, bp_systolic, bp_diastolic, temperature, pulse, weight_in_lbs, height, respirations, bmi, bsa, YEAR(contact_date) FROM stride7.SHC_visit_de WHERE (bp_systolic <> 0 OR bp_diastolic <> 0 OR temperature <> 0 OR pulse <> 0 OR weight_in_lbs <> 0 OR height <> \"\" OR respirations <> 0 OR bmi <> 0 OR bsa <> 0) AND patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_visit_de+stride7.SHC_visit_de", null, "pid", "age", "bp_sys", "bp_dias", "temp", "pulse", "weight", "height", "resp", "bmi",
        "bsa", "year");

    query = "SELECT patient_id, medication_id, FLOOR(age_at_order_in_days), YEAR(order_time), order_status_c, med_route_c FROM stride7.LPCH_med_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_med_de", new MappedColumn[] {new MappedColumn(2, medIdLpchEpic), new MappedColumn(5, medStatusLpch), new MappedColumn(6,
        medRoute)}, "pid", "rxNorm", "age", "year", "status", "route");

    query = "SELECT patient_id, hl7_medication_id, FLOOR(age_at_start_in_days), YEAR(start_time), order_status, route_id FROM stride7.LPCH_med_hl7_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_med_hl7_de", new MappedColumn[] {new MappedColumn(2, medIdHl7), new MappedColumn(5, medStatusLpch), new MappedColumn(6,
        medRoute)}, "pid", "rxNorm", "age", "year", "status", "route");

    query = "SELECT patient_id, hl7_medication_id, FLOOR(age_at_start_in_days), YEAR(start_time), order_status, route_id FROM stride7.SHC_med_hl7_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_med_hl7_de", new MappedColumn[] {new MappedColumn(2, medIdHl7), new MappedColumn(5, medStatusShc), new MappedColumn(6, medRoute)},
        "pid", "rxNorm", "age", "year", "status", "route");

    query = "SELECT patient_id, medication_id, FLOOR(age_at_order_in_days), YEAR(order_time), order_status_c, med_route_c FROM stride7.SHC_med_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_med_de", new MappedColumn[] {new MappedColumn(2, medIdShcEpic), new MappedColumn(5, medStatusShc), new MappedColumn(6, medRoute)},
        "pid", "rxNorm", "age", "year", "status", "route");

    query = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.SHC_visit_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_visit_de", new MappedColumn[] {new MappedColumn(2, departmentShc)}, "pid", "department", "age", "year");

    query = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.LPCH_visit_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_visit_de", new MappedColumn[] {new MappedColumn(2, departmentLpch)}, "pid", "department", "age", "year");

    query = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.SHC_visit_appt_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_visit_appt_de", new MappedColumn[] {new MappedColumn(2, departmentShc)}, "pid", "department", "age", "year");

    query = "SELECT patient_id, department_id, FLOOR(age_at_contact_in_days), YEAR(contact_date) FROM stride7.LPCH_visit_appt_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_visit_appt_de", new MappedColumn[] {new MappedColumn(2, departmentLpch)}, "pid", "department", "age", "year");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), enc_type_c, FLOOR(age_at_hosp_admsn_in_days), FLOOR(age_at_hosp_disch_in_days) FROM stride7.SHC_visit_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_visit_de", new MappedColumn[] {new MappedColumn(4, visitTypeShc)}, "pid", "age", "year", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), enc_type_c, FLOOR(age_at_hosp_admsn_in_days), FLOOR(age_at_hosp_disch_in_days) FROM stride7.LPCH_visit_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_visit_de", new MappedColumn[] {new MappedColumn(4, visitTypeLpch)}, "pid", "age", "year", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.LPCH_dx_admit_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_dx_admit_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year");

    query = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.SHC_dx_admit_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_dx_admit_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date) FROM stride7.LPCH_dx_extinjury_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_dx_extinjury_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date) FROM stride7.SHC_dx_extinjury_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_dx_extinjury_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(visit_duration), code, visit_type, dx_px_type_text, YEAR(contact_date) FROM stride7.LPCH_dx_hl7_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_dx_hl7_de", null, "pid", "start", "duration", "code", "visit_type", "dx_px_type_text", "year");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(visit_duration), code, visit_type, dx_px_type_text, YEAR(contact_date) FROM stride7.SHC_dx_hl7_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_dx_hl7_de", null, "pid", "start", "duration", "code", "visit_type", "dx_px_type_text", "year");

    query = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.LPCH_dx_hsp_acct_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_dx_hsp_acct_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year");

    query = "SELECT patient_id, FLOOR(age_at_admit_in_days), FLOOR(age_at_disch_in_days), dx_id, YEAR(admit_date_time) FROM stride7.SHC_dx_hsp_acct_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_dx_hsp_acct_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date), primary_dx_yn FROM stride7.LPCH_dx_pat_enc_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_dx_pat_enc_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), FLOOR(age_at_contact_in_days), dx_id, YEAR(contact_date), primary_dx_yn FROM stride7.SHC_dx_pat_enc_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_dx_pat_enc_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_noted_in_days), FLOOR(age_at_noted_in_days), dx_id, YEAR(noted_date), principal_pl_yn FROM stride7.LPCH_dx_prob_list_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_dx_prob_list_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "principal_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_noted_in_days), FLOOR(age_at_noted_in_days), dx_id, YEAR(noted_date), principal_pl_yn FROM stride7.SHC_dx_prob_list_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_dx_prob_list_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "principal_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.LPCH_px_cpt_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.SHC_px_cpt_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.LPCH_px_cpt_hsp_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_hsp_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.SHC_px_cpt_hsp_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_hsp_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), YEAR(contact_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.LPCH_px_cpt_prov_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), YEAR(contact_date), cpt_code, \"CPT\", \"UNKNOWN\" FROM stride7.SHC_px_cpt_prov_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), primary_dx_id, YEAR(contact_date), 'Y' FROM stride7.LPCH_px_cpt_prov_de where primary_dx_id != 0 AND patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), primary_dx_id, YEAR(contact_date), 'Y' FROM stride7.SHC_px_cpt_prov_de where primary_dx_id != 0 AND patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_two_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_two_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_two_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_two_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_three_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_three_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_three_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_three_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_four_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_four_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_four_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_four_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_five_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_five_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_five_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_five_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_six_id, YEAR(contact_date), 'N' FROM stride7.LPCH_px_cpt_prov_de where dx_six_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdLpch)}, "pid", "start", "end", "ICD9/ICD10", "year",
        "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_service_in_days), FLOOR(age_at_service_in_days), dx_six_id, YEAR(contact_date), 'N' FROM stride7.SHC_px_cpt_prov_de where dx_six_id != 0 and patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_cpt_prov_de", new MappedColumn[] {new MappedColumn(4, dxIdShc)}, "pid", "start", "end", "ICD9/ICD10", "year", "primary_dx_yn");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.LPCH_px_icd_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_icd_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_contact_in_days), YEAR(contact_date), code, sab, visit_type FROM stride7.SHC_px_icd_de where patient_id = " + pid;
    printResultSet(db.query(query), "stride7.SHC_px_icd_de", null, "pid", "age", "year", "code", "sab", "visit_type");

    query = "SELECT patient_id, FLOOR(age_at_lab_in_days), YEAR(lab_time), component_text, labvalue, abnormal, reference_range, LOINC_CODE FROM stride7.LPCH_lab_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_lab_de", null, "pid", "age", "year", "component_text", "labvalue", "abnormal", "reference_range", "LOINC_CODE");

    query = "SELECT patient_id, FLOOR(age_at_recorded_time_in_days), YEAR(recorded_time), gp2_disp_name, meas_value, '', '', LOINC_CODE FROM stride7.SHC_lab_outside_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "SHC_lab_outside_de", null, "pid", "age", "year", "gp2_disp_name", "meas_value", "abnormal", "reference_range", "LOINC_CODE");

    query = "SELECT patient_id, FLOOR(age_at_taken_in_days), YEAR(taken_time), lab_name, ord_value, reference_low, reference_high, result_in_range_yn, result_flag_c, LOINC_CODE FROM stride7.LPCH_lab_epic_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_lab_epic_de", new MappedColumn[] {new MappedColumn(9, resultFlagLpch)}, "pid", "age", "year", "lab_text", "ord_value", "ref_low",
        "ref_high", "in_range_yn", "result_flag", "LOINC_CODE");

    query = "SELECT patient_id, FLOOR(age_at_taken_in_days), YEAR(taken_time), lab_name, ord_value, reference_low, reference_high, result_in_range_yn, result_flag_c, LOINC_CODE FROM stride7.SHC_lab_epic_de where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_lab_epic_de", new MappedColumn[] {new MappedColumn(9, resultFlagShc)}, "pid", "age", "year", "lab_text", "ord_value", "ref_low",
        "ref_high", "in_range_yn", "result_flag", "LOINC_CODE");

    query = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), ref_bill_code, ref_bill_code_set_c FROM stride7.LPCH_px_icd_hsp_de LEFT OUTER join stride7_dictionaries.lpch_cl_icd_px on (stride7.LPCH_px_icd_hsp_de.icd_px_id = stride7_dictionaries.lpch_cl_icd_px.icd_px_id) where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.LPCH_px_icd_hsp_de", null, "pid", "age", "year", "ref_bill_code", "ref_bill_code_set_c");

    query = "SELECT patient_id, FLOOR(age_at_proc_in_days), YEAR(proc_date), ref_bill_code, ref_bill_code_set_c FROM stride7.SHC_px_icd_hsp_de LEFT OUTER join stride7_dictionaries.shc_cl_icd_px on (stride7.SHC_px_icd_hsp_de.icd_px_id = stride7_dictionaries.shc_cl_icd_px.icd_px_id) where patient_id = "
        + pid;
    printResultSet(db.query(query), "stride7.SHC_px_icd_hsp_de", null, "pid", "age", "year", "ref_bill_code", "ref_bill_code_set_c");

    db.close();
  }
}
