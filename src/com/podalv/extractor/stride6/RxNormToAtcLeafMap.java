package com.podalv.extractor.stride6;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Iterator;

import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.search.index.IndexCollection;
import com.podalv.utils.text.TextUtils;

/** Reads the rxnorm_to_atc.text file and given a RxNorm, returns a list of Atc leaf nodes
 *
 * @author podalv
 *
 */
public class RxNormToAtcLeafMap {

  private final IntKeyObjectMap<HashSet<String>> rxNormToAtcLeaves   = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<IntOpenHashSet>  rxNormToAtcLeavesId = new IntKeyObjectMap<>();

  private void init(final IndexCollection indices) {
    if (rxNormToAtcLeaves.size() == 0) {
      synchronized (this) {
        if (rxNormToAtcLeaves.size() == 0) {
          readMap();
        }
      }
    }
    if (rxNormToAtcLeavesId.size() == 0) {
      synchronized (this) {
        if (indices != null && rxNormToAtcLeavesId.size() == 0) {
          calculateIds(indices);
        }
      }
    }
  }

  private void calculateIds(final IndexCollection indices) {
    final IntKeyObjectIterator<HashSet<String>> i = rxNormToAtcLeaves.entries();
    while (i.hasNext()) {
      i.next();
      final IntOpenHashSet arr = new IntOpenHashSet();
      final Iterator<String> s = i.getValue().iterator();
      while (s.hasNext()) {
        arr.add(indices.addAtcCode(s.next()));
      }
      rxNormToAtcLeavesId.put(i.getKey(), arr);
    }
  }

  public IntOpenHashSet getAtcLeaves(final IndexCollection indices, final int rxNorm) {
    init(indices);
    final IntOpenHashSet result = rxNormToAtcLeavesId.get(rxNorm);
    return result == null ? new IntOpenHashSet() : result;
  }

  public HashSet<String> getAtcLeaves(final int rxNorm) {
    init(null);
    final HashSet<String> result = rxNormToAtcLeaves.get(rxNorm);
    return result == null ? new HashSet<>() : result;
  }

  private void readMap() {
    try {
      final ClassLoader classLoader = getClass().getClassLoader();
      final BufferedReader reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("rxnorm_to_atc.text"), Charset.forName("UTF-8")));
      String line;
      while ((line = reader.readLine()) != null) {
        //F0|getRelatedByType|I0|1000000|F1|getRxProperty|I1|5487|property_category|CODES|property_name|ATC|property_value|C03AA03
        final String[] data = TextUtils.split(line, '|');
        if (data.length == 14) {
          final int rxNorm = Integer.parseInt(data[3]);
          HashSet<String> set = rxNormToAtcLeaves.get(rxNorm);
          if (set == null) {
            set = new HashSet<>();
            rxNormToAtcLeaves.put(rxNorm, set);
          }
          set.add(Common.filterString(data[13]));
        }
      }
      reader.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

}