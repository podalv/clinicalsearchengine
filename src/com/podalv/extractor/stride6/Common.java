package com.podalv.extractor.stride6;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

import com.podalv.objectdb.datastructures.OffHeapIntArrayList.COMPRESSION_TYPE;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.search.datastructures.ObjectWithPayload;
import com.podalv.search.language.evaluation.iterators.CompressedPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.utils.datastructures.SortPair;
import com.podalv.utils.text.TextUtils;

public class Common {

  public static final int     UNSIGNED_BYTE_MAX_VALUE      = Byte.MAX_VALUE * 2;
  public static final int     UNSIGNED_SHORT_MAX_VALUE     = Short.MAX_VALUE * 2;
  public static final int     BYTE_SIZE_IN_BYTES           = 1;
  public static final int     SHORT_SIZE_IN_BYTES          = 2;
  public static final int     INT_SIZE_IN_BYTES            = 4;
  static final byte           POSITIVE_MENTION             = 0;
  static final byte           NEGATED_MENTION              = 1;
  static final byte           FH_MENTION                   = 2;
  static final byte           NEGATED_FH_MENTION           = 3;
  public static final String  OHDSI_DATE_FORMAT            = "yyyy-MM-dd";
  public static final String  PRIMARY_CODE                 = "PRIMARY";
  public static final String  INVALID_CODE                 = "INVALID";
  public static final String  NOT_AVAILABLE                = "N/A";
  public static final int     MISSING_VALUE                = -1;
  public static final String  VITALS_TEMPERATURE           = "Temperature";
  public static final String  VITALS_PULSE                 = "Heart Rate (Pulse)";
  public static final String  VITALS_MAP                   = "Mean arterial pressure (MAP)";
  public static final String  VITALS_RESP                  = "Respiration rate";
  public static final String  VITALS_BP_S                  = "Blood pressure systolic (BP)";
  public static final String  VITALS_BP_D                  = "Blood pressure diastolic (BP)";
  public static final String  VITALS_WEIGHT                = "Weight (lbs)";
  public static final String  VITALS_HEIGHT                = "Height (inch)";
  public static final String  VITALS_BMI                   = "Body mass index (BMI)";
  public static final String  VITALS_BSA                   = "Body surface area (BSA)";
  public static final String  INDICES_FILE_NAME            = "indices";
  public static final String  STATISTICS_FILE_NAME         = "statistics";
  public static final String  STATISTICS_REDUCED_FILE_NAME = "statistics.reduced";
  public static final int     UNDEFINED                    = -1;
  private static final char   INCH_SIGN                    = '"';
  private static final char   FOOT_SIGN                    = '\'';
  private static final byte[] POSITIVE                     = new byte[] {0, 0};
  private static final byte[] NEGATED                      = new byte[] {1, 0};
  private static final byte[] FH                           = new byte[] {0, 1};
  private static final byte[] NEGATED_FH                   = new byte[] {1, 1};
  static final int            CURRENT_YEAR                 = Calendar.getInstance().get(Calendar.YEAR);
  public static boolean       FILTERING_ENABLED            = true;

  public static String timeInDaysToString(final double value) {
    final double converted = (Math.round((value * 10000)) / (double) 10000);
    if (Math.abs(Math.ceil(converted) - converted) < 0.000001) {
      return ((int) Math.ceil(converted)) + "";
    }
    return Double.toString(converted);
  }

  private Common() {
    // Static access only
  }

  public static String filterString(final String source) {
    if (FILTERING_ENABLED) {
      return source == null ? "null" : source.replace('"', '\'').replace('$', '_').replace('\t', ' ').replace('\n', ' ').replace('\r', ' ').replace('#', '_').trim();
    }
    return source;
  }

  public static String[] filterString(final String ... source) {
    if (FILTERING_ENABLED) {
      for (int x = 0; x < source.length; x++) {
        source[x] = filterString(source[x]);
      }
      return source;
    }
    return source;
  }

  public static byte[] decodeNegatedFamilyHistory(final int value) {
    if (value == POSITIVE_MENTION) {
      return POSITIVE;
    }
    else if (value == NEGATED_MENTION) {
      return NEGATED;
    }
    else if (value == FH_MENTION) {
      return FH;
    }
    else if (value == NEGATED_FH_MENTION) {
      return NEGATED_FH;
    }
    return null;
  }

  static byte encodeNegatedFamilyHistory(final int negated, final int familyHistory) {
    if (negated == 0 && familyHistory == 0) {
      return POSITIVE_MENTION;
    }
    else if (negated == 0 && familyHistory == 1) {
      return FH_MENTION;
    }
    else if (negated == 1 && familyHistory == 0) {
      return NEGATED_MENTION;
    }
    else if (negated == 1 && familyHistory == 1) {
      return NEGATED_FH_MENTION;
    }
    throw new UnsupportedOperationException("Negated and/or family history indicators have values out of range [0-1] NEG/FH = [" + negated + "/" + familyHistory + "]");
  }

  public static File getDictFile(final File file) throws IOException {
    return new File(file.getCanonicalPath() + "_dict");
  }

  private static final double parseHeightToInches(final String heightInFeetInches, final char footSign, final char inchSign) {
    double result;
    try {
      final int realInchPos = heightInFeetInches.indexOf(inchSign);
      final int inchPos = realInchPos == -1 ? heightInFeetInches.length() - 2 : realInchPos;
      result = Integer.parseInt(heightInFeetInches.substring(0, heightInFeetInches.indexOf(footSign)).trim()) * 12;
      if (realInchPos != -1) {
        try {
          result += Double.parseDouble(heightInFeetInches.substring(heightInFeetInches.indexOf(footSign) + 1, inchPos).trim());
        }
        catch (final Exception e) {
          result = -1;
        }
      }
    }
    catch (final StringIndexOutOfBoundsException ex) {
      result = -1;
    }
    catch (final NumberFormatException e) {
      result = -1;
    }

    return result;
  }

  public static COMPRESSION_TYPE calculateCompressionType(final int value) {
    if (value < 256) {
      return COMPRESSION_TYPE.BYTE;
    }
    else if (value < (256 * 256)) {
      return COMPRESSION_TYPE.SHORT;
    }
    return COMPRESSION_TYPE.INT;
  }

  public static int compressionTypeToSizeInBytes(final COMPRESSION_TYPE type) {
    if (type == COMPRESSION_TYPE.BYTE) {
      return 1;
    }
    else if (type == COMPRESSION_TYPE.SHORT) {
      return 2;
    }
    return 4;
  }

  public static final double parseHeightToInchesSilent(final String heightInFeetInches) {
    final double result = parseHeightToInches(heightInFeetInches, FOOT_SIGN, INCH_SIGN);
    final double result2 = parseHeightToInches(heightInFeetInches, FOOT_SIGN, '*');
    return Math.max(result, result2);
  }

  public static final double parseHeightToInches(final String heightInFeetInches) {
    double result = parseHeightToInches(heightInFeetInches, FOOT_SIGN, INCH_SIGN);
    final double result2 = parseHeightToInches(heightInFeetInches, FOOT_SIGN, '*');
    result = Math.max(result, result2);
    if (result < 0) {
      System.out.println("Cannot parse height '" + heightInFeetInches + "'");
    }
    return result;
  }

  public static final String languageNodeTimeArgumentToText(final int value) {
    return value == Integer.MAX_VALUE ? "MAX" : value == Integer.MIN_VALUE ? "MIN" : String.valueOf(value);
  }

  public static long parseInt(final String time) {
    try {
      if (time.length() <= 9) {
        if (time.equals("MIN")) {
          return Integer.MIN_VALUE;
        }
        if (time.equals("MAX")) {
          return Integer.MAX_VALUE;
        }
        return Integer.parseInt(time);
      }
      else {
        final long parsedTimeAsLong = Long.parseLong(time);
        if (parsedTimeAsLong >= Integer.MAX_VALUE) {
          return Integer.MAX_VALUE;
        }
        if (parsedTimeAsLong < 0) {
          return Integer.MIN_VALUE;
        }
        return parsedTimeAsLong;
      }
    }
    catch (final Exception e) {
      throw new IllegalArgumentException("Expecting number, found '" + time + "'");
    }
  }

  public static final int timeValueToTimeInMinutes(final String time, final String timeModifier) {
    final long parsedTime = parseInt(time);
    if (parsedTime >= Integer.MAX_VALUE) {
      return Integer.MAX_VALUE;
    }
    if (parsedTime <= Integer.MIN_VALUE) {
      return Integer.MIN_VALUE;
    }
    if (timeModifier != null) {
      if (timeModifier.startsWith("W")) {
        if (parsedTime >= 0) {
          return (int) Math.min(Integer.MAX_VALUE, parsedTime * 7 * 24 * 60);
        }
        else {
          return (int) Math.max(Integer.MIN_VALUE, parsedTime * 7 * 24 * 60);
        }
      }
      if (timeModifier.startsWith("D")) {
        if (parsedTime >= 0) {
          return (int) Math.min(Integer.MAX_VALUE, parsedTime * 24 * 60);
        }
        else {
          return (int) Math.max(Integer.MIN_VALUE, parsedTime * 24 * 60);
        }
      }
      if (timeModifier.startsWith("H")) {
        if (parsedTime >= 0) {
          return (int) Math.min(Integer.MAX_VALUE, parsedTime * 60);
        }
        else {
          return (int) Math.max(Integer.MIN_VALUE, parsedTime * 60);
        }
      }
      if (timeModifier.startsWith("MO")) {
        if (parsedTime >= 0) {
          return (int) Math.min(Integer.MAX_VALUE, parsedTime * 30 * 24 * 60);
        }
        else {
          return (int) Math.max(Integer.MIN_VALUE, parsedTime * 30 * 24 * 60);
        }
      }
      if (timeModifier.startsWith("Y")) {
        if (parsedTime >= 0) {
          return (int) Math.min(Integer.MAX_VALUE, parsedTime * 365 * 24 * 60);
        }
        else {
          return (int) Math.max(Integer.MIN_VALUE, parsedTime * 365 * 24 * 60);
        }
      }
    }
    return (int) parsedTime;
  }

  public static final int minutesToYears(final int age) {
    return (age == Integer.MAX_VALUE) ? Integer.MAX_VALUE : (age == Integer.MIN_VALUE) ? 0 : age / (24 * 60 * 365);
  }

  public static final double minutesToDays(final int age) {
    return (age == Integer.MAX_VALUE) ? Integer.MAX_VALUE : (age < 0) ? 0 : age / (double) (24 * 60);
  }

  public static final int yearsToTime(final int age) {
    return (age == Integer.MAX_VALUE) ? Integer.MAX_VALUE : (age == Integer.MIN_VALUE) ? 0 : age * (24 * 60 * 365);
  }

  public static final int daysToTime(final double age) {
    return (age == Integer.MAX_VALUE) ? Integer.MAX_VALUE : (age == Integer.MIN_VALUE) ? 0 : (int) Math.round(age * 24 * 60);
  }

  static double convertCelsiusToFahrenheit(final double celsius) {
    return (celsius * (1.8)) + 32;
  }

  public static PayloadIterator compressIterator(final ObjectWithPayload patient, final PayloadIterator iterator) {
    return iterator instanceof CompressedPayloadIterator ? iterator : new CompressedPayloadIterator(patient, iterator);
  }

  public static PersistentIntArrayList compressAgeRangesIntArrayList(final HashSet<SortPair<Integer, Integer>> set) {
    final ArrayList<SortPair<Integer, Integer>> array = new ArrayList<>();
    final Iterator<SortPair<Integer, Integer>> i = set.iterator();
    while (i.hasNext()) {
      final SortPair<Integer, Integer> value = i.next();
      array.add(value);
    }
    Collections.sort(array);
    final PersistentIntArrayList result = new PersistentIntArrayList();
    int lastStart = -1;
    int lastEnd = -1;
    for (int x = 0; x < array.size(); x++) {
      if (lastStart == -1) {
        lastStart = array.get(x).getComparable().intValue();
        lastEnd = array.get(x).getObject().intValue();
      }
      if (lastStart != array.get(x).getComparable().intValue()) {
        result.add(lastStart);
        result.add(lastEnd);
        lastStart = array.get(x).getComparable().intValue();
        lastEnd = array.get(x).getObject().intValue();
      }
      if (lastStart == array.get(x).getComparable().intValue() && lastEnd < array.get(x).getObject().intValue()) {
        lastEnd = array.get(x).getObject().intValue();
      }
    }
    if (lastStart != -1) {
      result.add(lastStart);
      result.add(lastEnd);
    }
    return result;
  }

  public static void addAgeRange(final HashSet<SortPair<Integer, Integer>> ageRanges, final int start, final int end) {
    ageRanges.add(new SortPair<>(start, end));
  }

  private static double adjustValues(final double value, final boolean adjustDown) {
    if (Double.compare(value, Double.MAX_VALUE) != 0 && Double.compare(value, Double.MIN_VALUE) != 0) {
      if (adjustDown) {
        return value - 0.001;
      }
      else {
        return value + 0.001;
      }
    }
    return value;
  }

  private static double[] adjustGreaterOrEqual(final double[] values) {
    values[0] = adjustValues(values[0], true);
    values[1] = adjustValues(values[1], false);
    return values;
  }

  public static double[] parseDoubleValueRange(String range) {
    if (range != null && range.endsWith("%")) {
      range = range.substring(0, range.length() - 1);
    }
    try {
      if (range != null && !range.isEmpty()) {
        int pos = range.indexOf('>');
        if (pos != -1) {
          final int equalPos = range.indexOf("=", pos);
          return (equalPos == -1) ? new double[] {Double.parseDouble(range.substring(pos + 1).trim()), Double.MAX_VALUE}
              : adjustGreaterOrEqual(new double[] {Double.parseDouble(range.substring(equalPos + 1).trim()), Double.MAX_VALUE});
        }
        else {
          pos = range.indexOf('<');
          if (pos != -1) {
            final int equalPos = range.indexOf("=", pos);
            return equalPos == -1 ? new double[] {Double.MIN_VALUE + 1, Double.parseDouble(range.substring(pos + 1).trim())}
                : adjustGreaterOrEqual(new double[] {Double.MIN_VALUE, Double.parseDouble(range.substring(equalPos + 1).trim())});
          }
          else {
            final String[] ranges = (range.startsWith("-")) ? TextUtils.split(range.substring(1), '-') : TextUtils.split(range, '-');
            if (ranges.length == 2) {
              return adjustGreaterOrEqual(new double[] {range.startsWith("-") ? -1 * Double.parseDouble(ranges[0].trim()) : Double.parseDouble(ranges[0].trim()),
                  Double.parseDouble(ranges[1].trim())});
            }
          }
        }
      }
    }
    catch (final NumberFormatException e) {
      return null;
    }
    return null;
  }

}
