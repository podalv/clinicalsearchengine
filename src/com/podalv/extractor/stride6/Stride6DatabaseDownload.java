package com.podalv.extractor.stride6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.datastructures.BinaryFileWriter;
import com.podalv.extractor.stride6.extractors.DemographicsExtractor;
import com.podalv.extractor.stride6.extractors.MedsExtractor;
import com.podalv.extractor.stride6.extractors.TermExtractor;
import com.podalv.extractor.stride6.extractors.VisitExtractor;
import com.podalv.extractor.stride6.extractors.VitalsExtractor;
import com.podalv.extractor.stride6.extractors.VitalsVisitsExtractor;
import com.podalv.maps.string.StringHashSet;
import com.podalv.maps.string.StringKeyObjectIterator;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

/** Downloads data from the database into a binary format
 *
 * @author podalv
 *
 */
public class Stride6DatabaseDownload {

  public static final int    UNDEFINED              = Integer.MIN_VALUE;
  private static final int   INVALID_YEAR           = 2020;
  static final String        VISIT_DX_FILE          = "visit_dx.bin";
  static final String        LABS_COMPONENT_FILE    = "labs_component.bin";
  static final String        LABS_LPCH_FILE         = "labs_lpch.bin";
  static final String        LABS_SHC_FILE          = "labs_shc.bin";
  public static final String LABS_FILE              = "labs.bin";
  public static final String MEDS_FILE              = "meds.bin";
  public static final String DEPARTMENT_FILE        = "departments.bin";
  static final String        VITALS_VISITS_FILE     = "vitals_visits.bin";
  static final String        VITALS_FILE            = "vitals.bin";
  public static final String VISITS_FILE            = "visits.bin";
  static final String        NOTES_FILE             = "notes.bin";
  public static final String CODE_DICTIONARY_FILE   = "code_dictionary.bin";
  public static final String DEMOGRAPHICS_FILE      = "demographics.bin";
  static final String        OBSERVATION_FILE       = "observation.bin";
  static final String        SNOMED_DICTIONARY_FILE = "snomed_dictionary.bin";
  public static final String TERM_DICTIONARY_FILE   = "term_dictionary.bin";
  public static String       TERMS_FILE             = "terms.bin";

  static void extractDemographics(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.DEMOGRAPHICS_STRUCTURE);
    while (set.next()) {
      double death = set.getDouble(DemographicsExtractor.DEATH_COLUMN);
      if (death != 0) {
        death = Math.ceil(set.getDouble(DemographicsExtractor.DEATH_COLUMN) + 0.001);
      }
      writer.write(set.getInt(DemographicsExtractor.PATIENT_ID_COLUMN), //
          set.getString(DemographicsExtractor.GENDER_COLUMN), //
          set.getString(DemographicsExtractor.RACE_COLUMN), //
          set.getString(DemographicsExtractor.ETHNICITY_COLUMN), //
          death, -1);
    }
    writer.close();
  }

  static void extractVisits(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.VISITS_STRUCTURE);
    while (set.next()) {
      final int year = set.getInt(VisitExtractor.YEAR_COLUMN);
      if (year != INVALID_YEAR || Common.CURRENT_YEAR >= INVALID_YEAR) {
        writer.write(set.getInt(VisitExtractor.VISIT_ID_COLUMN), //
            set.getInt(VisitExtractor.PATIENT_ID_COLUMN), //
            year, //
            set.getDouble(VisitExtractor.AGE_COLUMN), //
            set.getString(VisitExtractor.SRC_VISIT_COLUMN), //
            set.getDouble(VisitExtractor.DURATION_COLUMN), //
            set.getString(VisitExtractor.CODE_COLUMN), //
            set.getString(VisitExtractor.SAB_COLUMN), //
            set.getString(VisitExtractor.SOURCE_CODE_COLUMN));
      }
    }
    writer.close();
  }

  private static void extractVisitsDx(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.VISIT_DX_STRUCTURE);
    while (set.next()) {
      writer.write(set.getInt(1), //
          set.getInt(2), //
          set.getString(3));
    }
    writer.close();
  }

  static void extractNotes(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.NOTES_STRUCTURE);
    while (set.next()) {
      writer.write(set.getInt(1), //
          set.getString(2), //
          set.getDouble(3), //
          set.getShort(4));
    }
    writer.close();
  }

  static void extractVitals(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.VITALS_STRUCTURE);
    while (set.next()) {
      final int patientId = set.getInt(VitalsExtractor.VITALS_PATIENT_ID);
      int code = -1;
      try {
        code = set.getInt(VitalsExtractor.VITALS_MEAS_ID);
      }
      catch (final Exception e) {
        // invalid code... just ignore
      }
      if (code == -1) {
        continue;
      }
      String val = set.getString(VitalsExtractor.VITALS_DESCRIPTION);
      double value = set.getDouble(VitalsExtractor.VITALS_VALUE);
      if (val != null && val.indexOf("Celsius") != -1) {
        value = Common.convertCelsiusToFahrenheit(value);
        val = "Temperature";
        code = 30401115;
      }
      writer.write(patientId, //
          code, //
          set.getDouble(VitalsExtractor.VITALS_AGE), //
          val, //
          value, //
          set.getInt(VitalsExtractor.VITALS_YEAR));
    }
    writer.close();
  }

  private static void extractVitalsVisits(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.VITALS_VISITS_STRUCTURE);
    while (set.next()) {
      writer.write(set.getInt(VitalsVisitsExtractor.VITALS_VISITS_PATIENT_ID), //
          set.getDouble(VitalsVisitsExtractor.VITALS_VISITS_AGE), //
          set.getShort(VitalsVisitsExtractor.VITALS_VISITS_BP_S), //
          set.getShort(VitalsVisitsExtractor.VITALS_VISITS_BP_D), //
          set.getDouble(VitalsVisitsExtractor.VITALS_VISITS_TEMP), //
          set.getShort(VitalsVisitsExtractor.VITALS_VISITS_PULSE), //
          set.getDouble(VitalsVisitsExtractor.VITALS_VISITS_WEIGHT), //
          set.getString(VitalsVisitsExtractor.VITALS_VISITS_HEIGHT), //
          set.getShort(VitalsVisitsExtractor.VITALS_VISITS_RESP), //
          set.getDouble(VitalsVisitsExtractor.VITALS_VISITS_BMI), //
          set.getDouble(VitalsVisitsExtractor.VITALS_VISITS_BSA));
    }
    writer.close();
  }

  private static void extractShcLabs(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.LABS_SHC_STRUCTURE);
    while (set.next()) {
      try {
        final int patientId = set.getInt(1);
        final double age = set.getDouble(2);
        String code = set.getString(3);
        code = code == null ? "" : code;
        double val1 = set.getDouble(4);
        if (set.wasNull() || val1 > 1000000) {
          val1 = Stride6DatabaseDownload.UNDEFINED;
        }
        double val2;// = set.getDouble(5);
        double val3;// = set.getDouble(6);
        String str = set.getString(7);
        final double[] ranges = Common.parseDoubleValueRange(str);
        if (ranges != null) {
          val2 = ranges[0];
          val3 = ranges[1];
          str = null;
        }
        else {
          try {
            val2 = set.getDouble(5);
            val3 = set.getDouble(6);
          }
          catch (final Exception e) {
            val2 = -1;
            val3 = -1;
          }
        }
        str = str == null ? "" : str;
        writer.write(patientId, //
            age, //
            code, //
            val1, //
            val2, //
            val3, //
            str);
      }
      catch (final Exception e) {
        //
      }
    }
    writer.close();
  }

  private static void extractLpchLabs(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.LABS_LPCH_STRUCTURE);
    while (set.next()) {
      try {
        final int patientId = set.getInt(1);
        final double age = set.getDouble(2);
        String str1 = set.getString(3);
        str1 = str1 == null ? "" : str1;
        double val1 = set.getDouble(4);
        if (set.wasNull() || val1 > 10000000) {
          val1 = Stride6DatabaseDownload.UNDEFINED;
        }
        String str2 = set.getString(5);
        str2 = str2 == null ? "" : str2;
        writer.write(patientId, //
            age, //
            str1, //
            val1, //
            str2);
      }
      catch (final Exception e) {
        //
      }
    }
    writer.close();
  }

  public static double getDuration(final String status) {
    return ((status != null && !status.isEmpty()) && (status.toLowerCase().startsWith("discon") || status.toLowerCase().startsWith("cancel") || status.toLowerCase().startsWith(
        "delete") || status.toLowerCase().startsWith("suspe") || status.toLowerCase().startsWith("retrac") || status.toLowerCase().startsWith("denie"))) ? 0
            : OhdsiDatabaseDownload.MEDICATION_DURATION_EXTENSION_IN_DAYS;
  }

  static void extractMeds(final ResultSet set, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.MEDS_STRUCTURE);
    while (set.next()) {
      final String status = set.getString(MedsExtractor.ORDER_STATUS_COLUMN);
      writer.write(set.getInt(MedsExtractor.PATIENT_ID_COLUMN), //
          set.getDouble(MedsExtractor.AGE_COLUMN), //
          set.getShort(MedsExtractor.YEAR_COLUMN), //
          status, //
          set.getInt(MedsExtractor.RX_CUI_COLUMN), //
          set.getDouble(MedsExtractor.AGE_COLUMN) + getDuration(status), //
          set.getString(7));
    }
    writer.close();
  }

  private static void addRefValue(final String key, final String value, final StringKeyObjectMap<StringHashSet> map) {
    if (key != null && !key.isEmpty() && value != null && !value.isEmpty()) {
      StringHashSet set = map.get(key);
      if (set == null) {
        set = new StringHashSet();
        map.put(key, set);
      }
      set.add(value);
    }
  }

  public static void testLabReferenceValues(final ConnectionSettings settings) throws IOException, SQLException {
    final Database db = Database.createInstance(settings);
    final ResultSet shc = db.queryStreaming(Commands.LABS_SHC_TEST);
    final StringKeyObjectMap<StringHashSet> shcBaseNameToUnits = new StringKeyObjectMap<>();
    final StringKeyObjectMap<StringHashSet> shcBaseNameToLow = new StringKeyObjectMap<>();
    final StringKeyObjectMap<StringHashSet> shcBaseNameToHigh = new StringKeyObjectMap<>();

    final StringKeyObjectMap<StringHashSet> lpchBaseNameToUnits = new StringKeyObjectMap<>();
    final StringKeyObjectMap<StringHashSet> lpchBaseNameToLow = new StringKeyObjectMap<>();
    final StringKeyObjectMap<StringHashSet> lpchBaseNameToHigh = new StringKeyObjectMap<>();

    //base_name, reference_low, reference_high, reference_unit
    while (shc.next()) {
      final String baseName = shc.getString(1);
      final String low = shc.getString(2);
      final String high = shc.getString(3);
      final String unit = shc.getString(4).toLowerCase();
      addRefValue(baseName, low, shcBaseNameToLow);
      addRefValue(baseName, high, shcBaseNameToHigh);
      addRefValue(baseName, unit, shcBaseNameToUnits);
    }

    shc.close();

    final ResultSet lpch = db.queryStreaming(Commands.LABS_LPCH_TEST);
    while (lpch.next()) {
      final String baseName = lpch.getString(1);
      final String range = lpch.getString(2);
      final String unit = lpch.getString(3).toLowerCase();
      addRefValue(baseName, unit, lpchBaseNameToUnits);
      if (range != null && !range.isEmpty()) {
        int pos = range.indexOf('>');
        if (pos != -1) {
          addRefValue(baseName, range.substring(pos + 1).trim(), lpchBaseNameToLow);
        }
        else {
          pos = range.indexOf('<');
          if (pos != -1) {
            addRefValue(baseName, range.substring(pos + 1).trim(), lpchBaseNameToHigh);
          }
          else {
            final String[] ranges = TextUtils.split(range, '-');
            if (ranges.length == 2) {
              addRefValue(baseName, ranges[1].trim(), lpchBaseNameToHigh);
              addRefValue(baseName, ranges[0].trim(), lpchBaseNameToLow);
            }
          }
        }
      }
    }
    lpch.close();

    StringKeyObjectIterator<StringHashSet> iterator = shcBaseNameToUnits.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().size() > 1) {
        System.out.println("SHC units for '" + iterator.getKey() + "' = " + iterator.getValue().toString("\t"));
      }
    }

    iterator = lpchBaseNameToUnits.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().size() > 1) {
        System.out.println("LPCH units for '" + iterator.getKey() + "' = " + iterator.getValue().toString("\t"));
      }
    }

    iterator = shcBaseNameToUnits.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final StringHashSet lpUnits = lpchBaseNameToUnits.get(iterator.getKey());
      if (lpUnits != null) {
        final StringHashSet merge = new StringHashSet(lpUnits.toArray());
        merge.addAll(iterator.getValue().toArray());
        if (merge.size() > 1) {
          System.out.println("SHC/LPCH units for '" + iterator.getKey() + "' = " + merge.toString("\t"));
        }
      }
    }

    iterator = shcBaseNameToLow.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().size() > 1) {
        System.out.println("SHC low for '" + iterator.getKey() + "' = " + iterator.getValue().toString("\t"));
      }
    }

    iterator = shcBaseNameToHigh.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().size() > 1) {
        System.out.println("SHC high for '" + iterator.getKey() + "' = " + iterator.getValue().toString("\t"));
      }
    }

    iterator = lpchBaseNameToLow.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().size() > 1) {
        System.out.println("LPCH low for '" + iterator.getKey() + "' = " + iterator.getValue().toString("\t"));
      }
    }

    iterator = lpchBaseNameToHigh.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (iterator.getValue().size() > 1) {
        System.out.println("LPCH high for '" + iterator.getKey() + "' = " + iterator.getValue().toString("\t"));
      }
    }

    iterator = lpchBaseNameToLow.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final StringHashSet shcVal = shcBaseNameToLow.get(iterator.getKey());
      if (shcVal != null) {
        final StringHashSet merge = new StringHashSet(iterator.getValue().toArray());
        merge.addAll(shcVal.toArray());
        if (merge.size() > 1) {
          System.out.println("SHC/LPCH low for '" + iterator.getKey() + "' = " + merge.toString("\t"));
        }
      }
    }

    iterator = lpchBaseNameToHigh.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final StringHashSet shcVal = shcBaseNameToHigh.get(iterator.getKey());
      if (shcVal != null) {
        final StringHashSet merge = new StringHashSet(iterator.getValue().toArray());
        merge.addAll(shcVal.toArray());
        if (merge.size() > 1) {
          System.out.println("SHC/LPCH high for '" + iterator.getKey() + "' = " + merge.toString("\t"));
        }
      }
    }

    System.out.println("FINISHED !!!");
  }

  private static void extractTerms(final ConnectionSettings settings, final File output) throws IOException, SQLException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.TERMS_STRUCTURE);
    Database db = Database.createInstance(settings);
    ResultSet set = db.query(Commands.PATIENT_COUNT);
    set.next();
    final int maxPatientId = set.getInt(1);
    set.close();
    db.close();
    int currentPatient = 0;
    final long time = System.currentTimeMillis();
    while (currentPatient < maxPatientId) {
      db = Database.createInstance(settings);
      set = db.query(Commands.getTermsQuery(Commands.TERMS, currentPatient, "patient_id"));
      final int prevPatient = currentPatient;
      while (set.next()) {
        currentPatient = set.getInt(TermExtractor.PATIENT_ID_COLUMN);
        writer.write(currentPatient, //
            set.getInt(TermExtractor.NID_COLUMN), //
            set.getInt(TermExtractor.TID_COLUMN), //
            Common.encodeNegatedFamilyHistory(set.getInt(TermExtractor.NEGATED_COLUMN), set.getInt(TermExtractor.FH_COLUMN)));
      }
      currentPatient = prevPatient + Commands.MAX_TERM_MENTIONS_INCREMENT + 1;
      set.close();
      db.close();
      final double patientTime = ((System.currentTimeMillis() - time) / (currentPatient / (double) maxPatientId));
      System.out.println("Time remaining: " + new DecimalFormat("##.###").format((patientTime * (maxPatientId - currentPatient)) / (1000 * 60 * 60)) + " hours");
      if (currentPatient > maxPatientId) {
        break;
      }
    }
    writer.close();
  }

  static void extractTermDictionary(final ResultSet set, final File output) throws SQLException, IOException {
    final BufferedWriter writer = new BufferedWriter(new FileWriter(output));
    while (set.next()) {
      writer.write(set.getInt(1) + "\t" + set.getString(2) + "\n");
    }
    writer.close();
  }

  private static void extractComponent(final ResultSet set, final File output) throws SQLException, IOException {
    FileUtils.deleteWithException(output);
    final BinaryFileWriter writer = new BinaryFileWriter(output, Commands.LABS_COMPONENT_STRUCTURE);
    while (set.next()) {
      writer.write(set.getString(Commands.LABS_COMPONENT_BASE_NAME_COLUMN), //
          set.getString(Commands.LABS_COMPONENT_COMMON_NAME_COLUMN));
    }
    writer.close();
  }

  private static void extractDictionary(final ConnectionSettings settings, final File folder) throws SQLException, IOException {
    System.out.println("Extracting dictionary...");

    final ResultSet s = Database.createInstance(settings).query(Commands.TERM_DICTIONARY_TABLE);
    s.next();

    final ResultSet set = Database.createInstance(settings).query(Commands.getTerminologyQuery(Commands.TERM_DICTIONARY, s.getString(1)));
    s.close();
    extractTermDictionary(set, new File(folder, TERM_DICTIONARY_FILE));
    set.close();
  }

  public static void execute(final String[] args, final ConnectionSettings connection) {
    Database db = null;
    ResultSet set = null;
    if (args[0].indexOf('d') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.query(Commands.DEMOGRAPHICS);
        extractDemographics(set, new File(new File(args[1]), DEMOGRAPHICS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting demographics");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('p') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.query(Commands.VISIT_DX);
        extractVisitsDx(set, new File(new File(args[1]), VISIT_DX_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting visit_dx");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('n') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.query(Commands.NOTES);
        extractNotes(set, new File(new File(args[1]), NOTES_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting notes");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('v') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.query(Commands.VISITS);
        extractVisits(set, new File(new File(args[1]), VISITS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting visits");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('t') != -1) {
      try {
        extractTerms(connection, new File(new File(args[1]), TERMS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting terms");
        e.printStackTrace();
      }
    }
    if (args[0].indexOf('s') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.query(Commands.VITALS);
        extractVitals(set, new File(new File(args[1]), VITALS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting vitals");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('r') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.query(Commands.VITALS_VISITS);
        extractVitalsVisits(set, new File(new File(args[1]), VITALS_VISITS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting vitals_visits");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('m') != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.queryStreaming(Commands.MEDS);
        extractMeds(set, new File(new File(args[1]), MEDS_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting visits");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf('u') != -1 || args[0].indexOf('t') != -1) {
      try {
        extractDictionary(connection, new File(args[1]));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting UMLS");
      }
    }
    if (args[0].indexOf("x") != -1) {
      try {
        testLabReferenceValues(connection);
      }
      catch (final Exception e) {
        System.out.println("!!! Error testing lab reference values");
      }
    }
    if (args[0].indexOf("l") != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.queryStreaming(Commands.LABS_SHC);
        extractShcLabs(set, new File(new File(args[1]), LABS_SHC_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting SHC labs");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
      try {
        db = Database.createInstance(connection);
        set = db.queryStreaming(Commands.LABS_LPCH);
        extractLpchLabs(set, new File(new File(args[1]), LABS_LPCH_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting LPCH labs");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
      try {
        db = Database.createInstance(connection);
        set = db.queryStreaming(Commands.LABS_COMPONENT);
        extractComponent(set, new File(new File(args[1]), LABS_COMPONENT_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting lab components");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
    if (args[0].indexOf("c") != -1) {
      try {
        db = Database.createInstance(connection);
        set = db.queryStreaming(Commands.LABS_COMPONENT);
        extractComponent(set, new File(new File(args[1]), LABS_COMPONENT_FILE));
      }
      catch (final Exception e) {
        System.out.println("!!! Error extracting lab components");
        e.printStackTrace();
      }
      finally {
        FileUtils.close(set);
        FileUtils.close(db);
      }
    }
  }

  public static void main(final String[] args) throws IOException, SQLException {
    System.out.println("Usage: options output_folder connection_settings_file");
    System.out.println();
    System.out.println("options");
    System.out.println("d = stride6.demographics");
    System.out.println("u = term dictionary");
    System.out.println("t = stride6.term_mentions");
    System.out.println("v = stride6.visit_master");
    System.out.println("m = stride6.order_med");
    System.out.println("s = stride6_staging.vitals");
    System.out.println("n = stride6.notes");
    System.out.println("r = stride6.visits (vitals)");
    System.out.println("p = stride6.visit_dx (primary diagnosis data)");
    System.out.println("x = test reference values for labs");
    System.out.println("l = stride6.lab_results_shc, stride6.lab_results_lpch, stride6.component");
    System.out.println("c = stride6.component");
    System.out.println("DB_URL   jdbc:mysql://xxx.xxx:3306");
    execute(args, ConnectionSettings.createFromFile(new File(args[2])));
  }

}