package com.podalv.extractor.truven;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;

import com.podalv.utils.text.TextUtils;

/** Reads the Truven D .csv files
*
* @author podalv
*
*/
public class TruvenDExtractor implements TruvenFileReader, Closeable {

  private final BufferedReader reader;
  private int                  pidColId;
  private int                  startDateColId;
  private int                  endDateColId;
  private int                  ageColId;
  private int                  ndcColId;
  private int                  sexColId;
  private String[]             currentLine = null;

  public static TruvenDExtractor createEmpty() {
    TruvenDExtractor result = null;
    try {
      result = new TruvenDExtractor(null);
    }
    catch (final Exception e) {
      // Irrelevant we are creating an empty instance
    }
    return result;
  }

  public TruvenDExtractor(final BufferedReader reader) throws IOException {
    this.reader = reader;
    readHeader();
  }

  private void readHeader() throws IOException {
    final String[] data = TextUtils.split(reader.readLine(), ',');
    for (int x = 0; x < data.length; x++) {
      if (data[x].equals(TruvenConst.SEX_COL)) {
        sexColId = x;
      }
      if (data[x].equals(TruvenConst.PATID_COL)) {
        pidColId = x;
      }
      if (data[x].equals(TruvenConst.SVCDATE_COL)) {
        startDateColId = x;
      }
      if (data[x].equals(TruvenConst.DAYS_SUPPLY_COL)) {
        endDateColId = x;
      }
      if (data[x].equals(TruvenConst.AGE_COL)) {
        ageColId = x;
      }
      if (data[x].equals(TruvenConst.NDC_COL)) {
        ndcColId = x;
      }
    }
  }

  @Override
  public boolean next() throws IOException {
    final String line = reader.readLine();
    if (line == null) {
      currentLine = null;
      return false;
    }
    else {
      currentLine = TextUtils.split(line, ',');
      return true;
    }
  }

  @Override
  public String getPid() {
    if (currentLine != null) {
      return currentLine[pidColId];
    }
    return null;
  }

  public String getSex() {
    if (currentLine != null) {
      return currentLine[sexColId];
    }
    return null;
  }

  public String getAge() {
    if (currentLine != null) {
      return currentLine[ageColId];
    }
    return null;
  }

  public String getStartDate() {
    if (currentLine != null) {
      return currentLine[startDateColId];
    }
    return null;
  }

  public String getEndDate() {
    if (currentLine != null) {
      return currentLine[endDateColId];
    }
    return null;
  }

  public String getNdc() {
    if (currentLine != null) {
      return currentLine[ndcColId];
    }
    return null;
  }

  @Override
  public void close() throws IOException {
    reader.close();
  }

}
