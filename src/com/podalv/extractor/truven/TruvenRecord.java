package com.podalv.extractor.truven;

public abstract class TruvenRecord implements Comparable<TruvenRecord> {

  public abstract short getStart();

  public abstract short getAge();

  @Override
  public int compareTo(final TruvenRecord o) {
    return Short.compare(getStart(), o.getStart());
  }

}
