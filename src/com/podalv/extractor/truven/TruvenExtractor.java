package com.podalv.extractor.truven;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.antlr.v4.runtime.misc.Pair;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.input.StreamInput;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.list.LongArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.PersistentObjectDatabaseGenerator;
import com.podalv.output.StreamOutput;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.index.StatisticsBuilderCompressed;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.StatisticsAgeGenerator;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.utils.datastructures.LongMutable;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.memory.MemoryStatistics;
import com.podalv.utils.text.TextUtils;

/** Extracts the Truven MarketScan data from .csv files
 *
 * @author podalv
 *
 */
public class TruvenExtractor {

  private final HashMap<Long, Integer>          pidToInt          = new HashMap<>();
  private final HashMap<Integer, Long>          intToPid          = new HashMap<>();
  private final HashMap<String, Integer>        icd9ToId          = new HashMap<>();
  private final HashMap<String, Integer>        cptToId           = new HashMap<>();
  private final IntOpenHashSet                  inpatient         = new IntOpenHashSet();
  private final IntOpenHashSet                  outpatient        = new IntOpenHashSet();
  private final IntOpenHashSet                  other             = new IntOpenHashSet();
  private final ArrayList<IntOpenHashSet>       cptToPatientCnt   = new ArrayList<>();
  private final ArrayList<IntOpenHashSet>       icd9ToPatientCnt  = new ArrayList<>();
  private final IntKeyObjectMap<IntOpenHashSet> rxToPatientCnt    = new IntKeyObjectMap<>();
  private final ArrayList<String>               cptIdToCpt        = new ArrayList<>();
  private final ArrayList<String>               icd9IdToIcd9      = new ArrayList<>();
  private final IntOpenHashSet                  males             = new IntOpenHashSet();
  private int                                   currentPid        = 1;
  private int                                   icd9Id            = 0;
  private int                                   cptId             = 0;
  private final NdcToRxIngredient               ndcToRxIngredient = NdcToRxIngredient.create();

  public synchronized void addMale(final int pid) {
    males.add(pid);
  }

  public int getIcd9Id(final String code) {
    Integer id = icd9ToId.get(code);
    if (id == null) {
      synchronized (icd9ToId) {
        id = icd9ToId.get(code);
        if (id == null) {
          id = icd9Id++;
          icd9ToId.put(code, id);
        }
      }
    }
    return id;
  }

  public int getCptId(final String code) {
    Integer id = cptToId.get(code);
    if (id == null) {
      synchronized (cptToId) {
        id = cptToId.get(code);
        if (id == null) {
          id = cptId++;
          cptToId.put(code, id);
        }
      }
    }
    return id;
  }

  protected int getPidInt(final long original) {
    Integer result = pidToInt.get(original);
    if (result == null) {
      synchronized (pidToInt) {
        result = pidToInt.get(original);
        if (result == null) {
          result = currentPid;
          pidToInt.put(original, currentPid++);
        }
      }
    }
    return result.intValue();
  }

  private static void savePatientCntMap(final ArrayList<IntOpenHashSet> map, final File outputFolder, final String id) throws IOException {
    final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(outputFolder, id))));
    for (int x = 0; x < map.size(); x++) {
      if (map.get(x) != null && map.get(x).size() != 0) {
        out.writeInt(x);
        out.writeInt(map.get(x).size());
        final IntIterator ii = map.get(x).iterator();
        while (ii.hasNext()) {
          final int val = ii.next();
          out.writeInt(val);
        }
      }
    }
    out.close();
  }

  private static void savePatientCntMap(final IntKeyObjectMap<IntOpenHashSet> map, final File outputFolder, final String id) throws IOException {
    final DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(outputFolder, id))));
    final BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputFolder, id + ".txt")));
    final IntKeyObjectIterator<IntOpenHashSet> i = map.entries();
    while (i.hasNext()) {
      i.next();
      final int x = i.getKey();
      if (map.get(x) != null && map.get(x).size() != 0) {
        out.writeInt(x);
        writer.write(i.getKey() + "\t" + i.getValue().size() + "\n");
        out.writeInt(i.getValue().size());
        final IntIterator ii = i.getValue().iterator();
        while (ii.hasNext()) {
          out.writeInt(ii.next());
        }
      }
    }
    writer.close();
    out.close();
  }

  protected void extractI(final String id, final TruvenOutput iOutput, final BufferedReader ... readers) throws IOException, ParseException, InterruptedException {
    for (int x = 0; x < readers.length; x++) {
      initIcd9Map();
      initCptMap();
      final BufferedReader reader = readers[x];
      extractSingle(id, iOutput, reader);
      savePatientCntMap(icd9ToPatientCnt, iOutput.getOutputFolder(), id + "." + x + "." + "icd9_stats");
      savePatientCntMap(cptToPatientCnt, iOutput.getOutputFolder(), id + "." + x + "." + "cpt_stats");
      icd9ToPatientCnt.clear();
      cptToPatientCnt.clear();
    }
    iOutput.close();
    System.out.println(id + " DONE !!!");
  }

  protected void extractD(final String id, final TruvenOutput iOutput, final BufferedReader ... readers) throws IOException, ParseException, InterruptedException {
    for (int x = 0; x < readers.length; x++) {
      initRxMap();
      final BufferedReader reader = readers[x];
      extractSingleD(id, iOutput, reader);
      savePatientCntMap(rxToPatientCnt, iOutput.getOutputFolder(), id + "." + x + "." + "rx_stats");
      rxToPatientCnt.clear();
    }
    iOutput.close();
    System.out.println(id + " DONE !!!");
  }

  private void initIcd9Map() {
    initMap(icd9ToPatientCnt, 15000);
  }

  private void initCptMap() {
    initMap(cptToPatientCnt, 25000);
  }

  private void initRxMap() {
    rxToPatientCnt.clear();
  }

  private void initMap(final ArrayList<IntOpenHashSet> map, final int size) {
    for (int x = 0; x < size; x++) {
      map.add(new IntOpenHashSet());
    }
  }

  private void recordCode(final ArrayList<IntOpenHashSet> map, final int id, final int patientId) {
    synchronized (map.get(id)) {
      map.get(id).add(patientId);
    }
  }

  private void recordCode(final IntKeyObjectMap<IntOpenHashSet> map, final int id, final int patientId) {
    IntOpenHashSet set = map.get(id);
    if (set == null) {
      synchronized (map) {
        set = map.get(id);
        if (set == null) {
          set = new IntOpenHashSet();
          map.put(id, set);
        }
        set.add(patientId);
      }
    }
    else {
      synchronized (map) {
        map.get(id).add(patientId);
      }
    }
  }

  private void extractSingle(final String id, final TruvenOutput iOutput, final BufferedReader reader) throws IOException {
    final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    final TruvenIExtractor extractor = new TruvenIExtractor(reader);
    long incorrectRecordCnt = 0;
    long correctRecordCnt = 0;
    while (extractor.next()) {
      if (incorrectRecordCnt > 0 && correctRecordCnt > 0 && (incorrectRecordCnt + correctRecordCnt) % 500000 == 0) {
        System.out.println(id + " " + (incorrectRecordCnt + correctRecordCnt));
        System.out.println("M/ICD9/CPT/ID=" + males.size() + "/" + icd9ToId.size() + "/" + cptToId.size() + "/" + pidToInt.size() + " / FREE MEM = "
            + MemoryStatistics.getAvailableMemory());
      }
      try {
        final long pid = Long.parseLong(extractor.getPid().substring(0, extractor.getPid().indexOf('.')));
        final int newPid = getPidInt(pid);
        final int age = Integer.parseInt(extractor.getAge().substring(0, extractor.getAge().indexOf('.')));
        final int time1 = TruvenConst.dateToDays(format.parse(extractor.getStartDate() + " 00:00:00.000").getTime());
        final int time2 = TruvenConst.dateToDays(format.parse(extractor.getEndDate() + " 00:00:00.000").getTime());
        if (time1 > Short.MAX_VALUE || time2 > Short.MAX_VALUE) {
          System.out.println("Unparseable date '" + extractor.getStartDate() + "' / '" + extractor.getEndDate() + "'");
          continue;
        }
        if (time1 < 0 || time2 < 0) {
          System.out.println("Unparseable date '" + extractor.getStartDate() + "' / '" + extractor.getEndDate() + "'");
          continue;
        }
        if (time2 < time1) {
          System.out.println("Unparseable date '" + extractor.getStartDate() + "' / '" + extractor.getEndDate() + "'");
          continue;
        }
        if (extractor.getSex().equals(TruvenConst.MALE)) {
          males.add(newPid);
        }
        final DataOutputStream out = iOutput.getWriter(newPid);
        out.writeInt(newPid);
        out.writeShort(time1);
        out.writeShort(time2);
        out.writeShort(age);
        final Iterator<String> dxIterator = extractor.getDx();
        while (dxIterator.hasNext()) {
          final String val = dxIterator.next();
          if (!val.isEmpty()) {
            final int icd9Code = getIcd9Id(val);
            recordCode(icd9ToPatientCnt, icd9Code, newPid);
            out.writeShort(icd9Code);
          }
        }
        out.writeShort(-1);
        final Iterator<String> procIterator = extractor.getProc();
        while (procIterator.hasNext()) {
          final String val = procIterator.next();
          if (!val.isEmpty()) {
            final int cptCode = getCptId(val);
            recordCode(cptToPatientCnt, cptCode, newPid);
            out.writeShort(cptCode);
          }
        }
        out.writeShort(-1);
        correctRecordCnt++;
      }
      catch (final Exception e) {
        incorrectRecordCnt++;
      }
    }
    extractor.close();
    System.out.println(id + " DONE " + incorrectRecordCnt + " / " + correctRecordCnt);
  }

  private void extractSingleD(final String id, final TruvenOutput iOutput, final BufferedReader reader) throws IOException {
    final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    final TruvenDExtractor extractor = new TruvenDExtractor(reader);
    long incorrectRecordCnt = 0;
    long correctRecordCnt = 0;
    while (extractor.next()) {
      if (incorrectRecordCnt > 0 && correctRecordCnt > 0 && (incorrectRecordCnt + correctRecordCnt) % 500000 == 0) {
        System.out.println("M/ICD9/CPT/ID=" + males.size() + "/" + icd9ToId.size() + "/" + cptToId.size() + "/" + pidToInt.size() + " / FREE MEM = "
            + MemoryStatistics.getAvailableMemory());
      }
      try {
        final long pid = Long.parseLong(extractor.getPid().substring(0, extractor.getPid().indexOf('.')));
        final int newPid = getPidInt(pid);
        final int age = Integer.parseInt(extractor.getAge().substring(0, extractor.getAge().indexOf('.')));
        final int time1 = TruvenConst.dateToDays(format.parse(extractor.getStartDate() + " 00:00:00.000").getTime());
        final int time2 = (int) (time1 + Math.abs(Double.parseDouble(extractor.getEndDate())));
        if (time1 > Short.MAX_VALUE || time2 > Short.MAX_VALUE) {
          System.out.println("#1 Unparseable date '" + extractor.getStartDate() + "' / '" + extractor.getEndDate() + "'");
        }
        if (time1 < 0 || time2 < 0) {
          System.out.println("#2 Unparseable date '" + extractor.getStartDate() + "' / '" + extractor.getEndDate() + "'");
        }
        if (time2 < time1) {
          System.out.println("#3 Unparseable date '" + extractor.getStartDate() + "' / '" + extractor.getEndDate() + "'");
        }
        if (extractor.getSex().equals(TruvenConst.MALE)) {
          males.add(newPid);
        }
        final String ndc = extractor.getNdc();
        if (ndc != null) {
          final IntArrayList list = ndcToRxIngredient.getRx(ndc);
          if (list != null) {
            for (int x = 0; x < list.size(); x++) {
              final DataOutputStream out = iOutput.getWriter(newPid);
              out.writeInt(newPid);
              out.writeShort(time1);
              out.writeShort(time2);
              out.writeShort(age);
              final int rxCode = list.get(x);
              recordCode(rxToPatientCnt, rxCode, newPid);
              out.writeInt(rxCode);
            }
          }
        }
        correctRecordCnt++;
      }
      catch (final Exception e) {
        incorrectRecordCnt++;
      }
    }
    extractor.close();
    System.out.println(id + " DONE " + incorrectRecordCnt + " / " + correctRecordCnt);
  }

  private static BufferedReader[] getIReaders(final File folder, final String startName) throws FileNotFoundException {
    final ArrayList<BufferedReader> result = new ArrayList<>();
    final File[] files = folder.listFiles();
    if (files == null) {
      throw new FileNotFoundException("Folder contains no files");
    }
    for (final File file : files) {
      if (file.getName().startsWith(startName)) {
        result.add(new BufferedReader(new FileReader(file)));
      }
    }
    return result.toArray(new BufferedReader[result.size()]);
  }

  private void sortFiles(final File folder) throws IOException, InterruptedException {
    readMaps(folder);
    final UmlsDictionary umls = UmlsDictionary.create();
    final StatisticsAgeGenerator ageGenerator = new StatisticsAgeGenerator();
    final Statistics statistics = new Statistics(new StatisticsBuilderCompressed(150000000));
    final IndexCollection indices = IndexCollection.createWithoutIcd9Hierarchy();
    for (int x = 0; x < icd9IdToIcd9.size(); x++) {
      if (icd9IdToIcd9.get(x) != null) {
        indices.setIcd9Code(x, icd9IdToIcd9.get(x));
      }
    }
    for (int x = 0; x < cptIdToCpt.size(); x++) {
      if (cptIdToCpt.get(x) != null) {
        indices.setCptCode(x, cptIdToCpt.get(x));
      }
    }
    indices.setUmls(umls);
    indices.addAllIcd9HierarchyParents();

    umls.calculateIcd9Hierarchy(indices);

    final PersistentObjectDatabaseGenerator generator = new PersistentObjectDatabaseGenerator(1000000000, 2, folder);

    final ArrayList<OffHeapMemory> mem = new ArrayList<>();
    final ArrayList<String> types = new ArrayList<>();
    final File[] files = folder.listFiles();
    final AtomicInteger tossedPatients = new AtomicInteger(0);
    for (int i = 0; i < 10; i++) {
      final HashMap<Integer, Pair<IntKeyObjectMap<IntArrayList>, IntKeyObjectMap<LongArrayList>>> pidToPositions = new HashMap<>();
      int memId = 0;
      int minPid = Integer.MAX_VALUE;
      int maxPid = 0;
      for (int y = 0; y < mem.size(); y++) {
        mem.get(y).free();
      }
      mem.clear();
      for (final File file : files) {
        try {
          final String end = file.getName().substring(file.getName().indexOf('.') + 1);
          if (file.getName().indexOf('.') == -1) {
            continue;
          }
          final int shard = Integer.parseInt(end);
          if (shard != i) {
            continue;
          }
          if (memId >= mem.size()) {
            mem.add(OffHeapMemory.allocateMemory((file.length())));
            types.add(null);
          }
          if (file.getName().startsWith("o")) {
            types.set(memId, "OUTPATIENT");
          }
          else if (file.getName().startsWith("i")) {
            types.set(memId, "INPATIENT");
          }
          else {
            types.set(memId, "OTHER");
          }
          System.out.println("Sorting file " + file.getAbsolutePath());
          if (file.getName().startsWith("d")) {
            final ShardResult res = readDShardToMemory(mem.get(memId), new TruvenDReader(new FileInputStream(file)));
            pidToPositions.put(memId, new Pair<>(res.getPidToPosInt(), res.getPidToPosLong()));
            minPid = Math.min(minPid, res.getMin());
            maxPid = Math.max(maxPid, res.getMax());
          }
          else {
            final ShardResult res = readShardToMemory(mem.get(memId), new TruvenReader(new FileInputStream(file)));
            pidToPositions.put(memId, new Pair<>(res.getPidToPosInt(), res.getPidToPosLong()));
            minPid = Math.min(minPid, res.getMin());
            maxPid = Math.max(maxPid, res.getMax());
          }
          memId++;
        }
        catch (final NumberFormatException ex) {
          // wrong file
        }
        catch (final Exception e) {
          e.printStackTrace();
        }
      }
      System.out.println("Outputing data...");
      final ExecutorService service = Executors.newFixedThreadPool(5);
      final Semaphore sem = new Semaphore(200);
      long t = System.currentTimeMillis();
      final AtomicInteger processedPatients = new AtomicInteger(0);
      final long startTime = System.currentTimeMillis();
      for (int x = minPid; x <= maxPid; x++) {
        processedPatients.incrementAndGet();
        if (x > 0 && (x - minPid) % 10000 == 0) {
          System.out.println("Processing patient " + x + " " + (((10000 * 1000) / (double) (System.currentTimeMillis() - t))) + "/" + (((processedPatients.get() * 1000)
              / (double) (System.currentTimeMillis() - startTime))) + ", TOSSED " + tossedPatients.get());
          System.out.println(MemoryStatistics.printMemoryStatistics());
          t = System.currentTimeMillis();
        }
        final int fX = x;
        sem.acquire(1);
        service.execute(new Runnable() {

          @Override
          public void run() {
            try {
              if (!processSinglePatient(ageGenerator, umls, statistics, indices, generator, mem, types, pidToPositions, fX)) {
                tossedPatients.incrementAndGet();
              }
            }
            catch (IOException | InterruptedException e) {
              e.printStackTrace();
            }
            sem.release(1);
          }
        });
      }
      service.shutdown();
      service.awaitTermination(1000, TimeUnit.HOURS);
    }
    for (int x = 0; x < mem.size(); x++) {
      mem.get(x).free();
    }

    System.out.println("Tossed patients = " + tossedPatients.get());

    System.out.println("Closing data generator...");
    generator.close();
    System.out.println("Generator closed");

    System.out.println("Saving statistics...");
    System.out.println("Age generator size = " + ageGenerator.size());
    final BufferedOutputStream statisticsStream = new BufferedOutputStream(new FileOutputStream(new File(folder, Common.STATISTICS_FILE_NAME)));
    recordStatisticsData(indices, umls, statistics, folder);
    System.out.println("Age generator size = " + ageGenerator.size());
    statistics.setAgeGenerator(ageGenerator);
    statistics.save(new StreamOutput(statisticsStream));
    statisticsStream.close();

    System.out.println("Saving indices...");
    final BufferedOutputStream indexCollection = new BufferedOutputStream(new FileOutputStream(new File(folder, Common.INDICES_FILE_NAME)));
    indices.save(new StreamOutput(indexCollection));
    indexCollection.close();
  }

  private void recordStatisticsData(final IndexCollection indices, final UmlsDictionary umls, final Statistics statistics, final File inputFolder) throws IOException {
    int[] v = outpatient.toArray();
    Arrays.sort(v);
    statistics.recordCompleteVisitType(indices.getVisitType("OUTPATIENT"), v);
    v = inpatient.toArray();
    Arrays.sort(v);
    statistics.recordCompleteVisitType(indices.getVisitType("INPATIENT"), v);
    v = other.toArray();
    Arrays.sort(v);
    statistics.recordCompleteVisitType(indices.getVisitType("OTHER"), v);

    System.out.println("Consolidating RX " + MemoryStatistics.getAvailableMemory());
    File file = new File(inputFolder, "consolidated.rx");
    DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
    try {
      while (true) {
        final int key = dis.readInt();
        final int size = dis.readInt();
        final int[] val = new int[size];
        for (int x = 0; x < size; x++) {
          val[x] = dis.readInt();
        }
        Arrays.sort(val);
        statistics.recordCompleteRxCode(key, val);
      }
    }
    catch (final EOFException e) {
      // ignore, normal
    }
    dis.close();

    System.out.println("Consolidating ICD9 " + MemoryStatistics.getAvailableMemory());
    file = new File(inputFolder, "consolidated.icd9");
    dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
    final IntOpenHashSet keys = new IntOpenHashSet();
    final IntKeyObjectMap<IntOpenHashSet> icd9ToPids = new IntKeyObjectMap<>();
    try {
      while (true) {
        final int key = dis.readInt();
        keys.add(key);
        final int size = dis.readInt();
        final int[] val = new int[size];
        for (int x = 0; x < size; x++) {
          val[x] = dis.readInt();
        }
        icd9ToPids.put(key, new IntOpenHashSet(val));
      }
    }
    catch (final EOFException e) {
      // ignore, normal
    }
    dis.close();

    System.out.println("Calculating ICD9 hierarchy " + MemoryStatistics.getAvailableMemory());
    final IntIterator i = keys.iterator();
    while (i.hasNext()) {
      final int keyId = i.next();
      final int[] hierarchy = umls.getIcd9Parents(keyId, indices);
      if (hierarchy != null) {
        for (int x = 0; x < hierarchy.length; x++) {
          IntOpenHashSet set = icd9ToPids.get(hierarchy[x]);
          if (set == null) {
            set = new IntOpenHashSet();
            icd9ToPids.put(hierarchy[x], set);
          }
          set.addAll(icd9ToPids.get(keyId));
        }
      }
    }

    System.out.println("Saving ICD9 " + MemoryStatistics.getAvailableMemory());
    final IntKeyObjectIterator<IntOpenHashSet> ii = icd9ToPids.entries();
    while (ii.hasNext()) {
      ii.next();
      final int[] vals = ii.getValue().toArray();
      Arrays.sort(vals);
      statistics.recordCompleteIcd9Code(ii.getKey(), vals);
      ii.getValue().clear();
      ii.remove();
    }

    System.out.println("Consolidating CPT " + MemoryStatistics.getAvailableMemory());
    file = new File(inputFolder, "consolidated.cpt");
    dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
    try {
      while (true) {
        final int key = dis.readInt();
        final int size = dis.readInt();
        final int[] val = new int[size];
        for (int x = 0; x < size; x++) {
          val[x] = dis.readInt();
        }
        Arrays.sort(val);
        statistics.recordCompleteCptCode(key, val);
      }
    }
    catch (final EOFException e) {
      // ignore, normal
    }
    dis.close();
    System.out.println("Consolidated " + MemoryStatistics.getAvailableMemory());
  }

  private void recordVisitType(final String type, final int pid) {
    if (type.equals("OUTPATIENT")) {
      synchronized (outpatient) {
        outpatient.add(pid);
      }
    }
    else if (type.equals("INPATIENT")) {
      synchronized (inpatient) {
        inpatient.add(pid);
      }
    }
    else {
      synchronized (other) {
        other.add(pid);
      }
    }
  }

  private boolean processSinglePatient(final StatisticsAgeGenerator ages, final UmlsDictionary umls, final Statistics statistics, final IndexCollection indices,
      final PersistentObjectDatabaseGenerator generator, final ArrayList<OffHeapMemory> mem, final ArrayList<String> types,
      final HashMap<Integer, Pair<IntKeyObjectMap<IntArrayList>, IntKeyObjectMap<LongArrayList>>> pidToPositions, final int x) throws IOException, InterruptedException {
    final ArrayList<TruvenRecord> singlePatient = new ArrayList<>();
    for (int y = 0; y < mem.size(); y++) {
      final LongArrayList positions = pidToPositions.get(y).b.get(x);
      if (positions != null) {
        for (int z = 0; z < positions.size(); z++) {
          final TruvenRecord rec = TruvenDRecord.deserialize(mem.get(y), positions.get(z));
          if (rec instanceof TruvenIRecord) {
            ((TruvenIRecord) rec).setVisitType(types.get(y));
            recordVisitType(types.get(y), x);
          }
          singlePatient.add(rec);
        }
        positions.clear();
      }
      final IntArrayList pos = pidToPositions.get(y).a.get(x);
      if (pos != null) {
        for (int z = 0; z < pos.size(); z++) {
          final TruvenRecord rec = TruvenDRecord.deserialize(mem.get(y), pos.get(z));
          if (rec instanceof TruvenIRecord) {
            ((TruvenIRecord) rec).setVisitType(types.get(y));
            recordVisitType(types.get(y), x);
          }
          singlePatient.add(rec);
        }
        pos.clear();
      }
    }
    if (singlePatient.size() != 0) {
      final int newPid = x;
      Collections.sort(singlePatient);
      @SuppressWarnings("resource")
      final PatientBuilder patient = new PatientBuilder(indices);
      patient.setId(newPid);
      final PatientRecord<VisitRecord> rec = convert(newPid, singlePatient);
      final PatientRecord<MedRecord> meds = convertMeds(newPid, singlePatient);
      Stride6Extractor.addIcd9HierarchyCodes(indices, rec, Stride6Extractor.generateIcd9Hierarchy(indices, umls, rec));
      patient.recordVisit(rec);
      patient.recordMeds(meds);
      if (patient.getStartTime() != Integer.MAX_VALUE) {
        synchronized (ages) {
          ages.addRecord(patient.getId(), patient.getStartTime(), patient.getEndTime());
        }
        patient.recordAtc(Stride6Extractor.generateAtcToRxNormMapCached(indices, umls, meds));
        //Stride6Extractor.recordVisitTypeStats(indices, statistics, rec);
        //Stride6Extractor.recordMedsStatistics(indices, statistics, umls, meds);
        statistics.addAge(Common.minutesToYears(patient.getStartTime()));
        final DemographicsRecord demo = new DemographicsRecord(newPid, males.contains(x) ? TruvenConst.MALE_STR : TruvenConst.FEMALE_STR, "UNKNOWN", "UNKNOWN", 0, -1);
        indices.addDemographicRecord(demo);
        patient.close();
        statistics.recordPatientId(patient);
        generator.add(patient);
        return true;
      }
      else {
        //indices.addEmptyDemographicRecord(x);
      }
    }
    else {
      //indices.addEmptyDemographicRecord(x);
    }
    return false;
  }

  private static int getYear(final int dayOffset) {
    return 2007 + (dayOffset / 365);
  }

  public static String convertIcd9Code(final String code) {
    if (code.indexOf('.') == -1) {
      if (code.length() > 4 && (code.charAt(0) == 'E')) {
        return code.substring(0, 4) + '.' + code.substring(4);
      }
      else if (code.length() > 3) {
        return code.substring(0, 3) + '.' + code.substring(3);
      }
    }
    return code;
  }

  public static String convertIcd10Code(final String code) {
    if (code.indexOf('.') == -1) {
      if (code.length() > 3) {
        return code.substring(0, 3) + '.' + code.substring(3);
      }
    }
    return code;
  }

  private void readPidToOriginalIdMap(final File folder) throws IOException {
    System.out.println("Reading pidMap...");
    intToPid.clear();
    final TextFileReader reader = new TextFileReader(new File(folder, "pidToId.txt"), Charset.forName("UTF-8"));
    while (reader.hasNext()) {
      final String[] data = TextUtils.split(reader.next(), '\t');
      intToPid.put(Integer.parseInt(data[1]), Long.parseLong(data[0]));
    }
    reader.close();
    System.out.println("DONE");
  }

  public void generateAtcStatistics(final File folder) throws IOException {
    final Statistics stats = new Statistics();
    StreamInput input = new StreamInput(new BufferedInputStream(new FileInputStream(new File(folder, "statistics"))));
    stats.load(input, 0);
    input.close();
    final UmlsDictionary umls = UmlsDictionary.create();
    input = new StreamInput(new BufferedInputStream(new FileInputStream(new File(folder, "indices"))));
    final IndexCollection indices = new IndexCollection(umls);
    indices.load(input, 0);
    input.close();

    final IntKeyObjectMap<IntOpenHashSet> atcToPids = new IntKeyObjectMap<>();
    final IntIterator i = stats.getUniquerxNormCodes();
    while (i.hasNext()) {
      final int rxNorm = i.next();
      final IntOpenHashSet parents = umls.getAtcHierarchy(indices, rxNorm);
      final IntIterator ii = parents.iterator();
      while (ii.hasNext()) {
        final int atcParent = ii.next();
        IntOpenHashSet s = atcToPids.get(atcParent);
        if (s == null) {
          s = new IntOpenHashSet();
          atcToPids.put(atcParent, s);
        }
        s.addAll(stats.getRxNormPatients(rxNorm));
      }
    }
    final IntKeyObjectIterator<IntOpenHashSet> ii = atcToPids.entries();
    while (ii.hasNext()) {
      ii.next();
      final int[] values = ii.getValue().toArray();
      Arrays.sort(values);
      stats.recordCompleteAtcCode(ii.getKey(), values);
    }
    final StreamOutput out = new StreamOutput(new BufferedOutputStream(new FileOutputStream(new File(folder, "statistics"))));
    stats.save(out);
    out.close();
  }

  public void readPidMap(final File folder) throws IOException {
    System.out.println("Reading pidMap...");
    pidToInt.clear();
    final TextFileReader reader = new TextFileReader(new File(folder, "pidToId.txt"), Charset.forName("UTF-8"));
    while (reader.hasNext()) {
      final String[] data = TextUtils.split(reader.next(), '\t');
      pidToInt.put(Long.parseLong(data[0]), Integer.parseInt(data[1]));
    }
    reader.close();
    System.out.println("DONE");
  }

  private void readMaps(final File folder) throws IOException {
    System.out.println("Reading maps...");
    System.out.println("Reading males...");
    males.clear();
    TextFileReader reader = new TextFileReader(new File(folder, "males.txt"), Charset.forName("UTF-8"));
    while (reader.hasNext()) {
      males.add(Integer.parseInt(reader.next()));
    }
    reader.close();
    System.out.println("Reading ICD9...");
    reader = new TextFileReader(new File(folder, "icd9.txt"), Charset.forName("UTF-8"));
    while (reader.hasNext()) {
      final String[] line = TextUtils.split(reader.next(), '\t');
      final int l1 = Integer.parseInt(line[1]);
      for (int x = icd9IdToIcd9.size(); x <= l1; x++) {
        icd9IdToIcd9.add(null);
      }
      icd9IdToIcd9.set(l1, convertIcd9Code(line[0]));
    }
    reader.close();
    System.out.println("Reading CPT...");
    reader = new TextFileReader(new File(folder, "cpt.txt"), Charset.forName("UTF-8"));
    while (reader.hasNext()) {
      final String[] line = TextUtils.split(reader.next(), '\t');
      final int l1 = Integer.parseInt(line[1]);
      for (int x = cptIdToCpt.size(); x <= l1; x++) {
        cptIdToCpt.add(null);
      }
      cptIdToCpt.set(l1, line[0]);
    }
    reader.close();
    System.out.println("DONE");
  }

  private PatientRecord<VisitRecord> convert(final int pid, final ArrayList<TruvenRecord> singlePatient) {
    final PatientRecord<VisitRecord> result = new PatientRecord<>(pid);
    final int firstDate = (singlePatient.get(0).getAge() * 365 * 24 * 60);

    for (final TruvenRecord event : singlePatient) {
      boolean first = true;
      if (event instanceof TruvenIRecord) {
        final TruvenIRecord e = (TruvenIRecord) event;
        for (final short icd9Id : e.getIcd9()) {
          final VisitRecord rec = new VisitRecord(1, getYear(event.getStart()), firstDate + (event.getStart() * 24 * 60), (e.getEnd() - event.getStart()) * 24 * 60, "ICD", icd9Id,
              e.getVisitType());
          rec.setPrimary(first);
          result.add(rec);
          first = false;
        }
        for (final short cptId : e.getCpt()) {
          if (cptId > cptIdToCpt.size()) {
            System.out.println("PID = " + pid + " / " + e.getVisitType());
          }
          final VisitRecord rec = new VisitRecord(1, getYear(event.getStart()), firstDate + (event.getStart() * 24 * 60), (e.getEnd() - event.getStart()) * 24 * 60, "CPT", cptId,
              e.getVisitType());
          result.add(rec);
        }
      }
    }
    return result;
  }

  public PatientRecord<MedRecord> convertMeds(final int pid, final ArrayList<TruvenRecord> singlePatient) {
    final PatientRecord<MedRecord> result = new PatientRecord<>(pid);
    final int firstDate = (singlePatient.get(0).getAge() * 365 * 24 * 60);

    for (final TruvenRecord event : singlePatient) {
      if (event instanceof TruvenDRecord) {
        result.add(new MedRecord(firstDate + (event.getStart() * 24 * 60), firstDate + (event.getStart() * 24 * 60), getYear(event.getStart()), "discontinued",
            ((TruvenDRecord) event).getRx(), "other"));
      }
    }
    return result;
  }

  private static ShardResult readShardToMemory(final OffHeapMemory mem, final TruvenReader reader) throws IOException {
    final IntKeyObjectMap<LongArrayList> pidToPositions = new IntKeyObjectMap<>();
    final IntKeyObjectMap<IntArrayList> pidToPositionsInt = new IntKeyObjectMap<>();
    final LongMutable pos = new LongMutable(0);
    int minPid = Integer.MAX_VALUE;
    int maxPid = 0;
    while (true) {
      TruvenIRecord record = null;
      try {
        record = reader.read();
      }
      catch (final Exception e) {
        break;
      }
      if (record.getEnd() < record.getStart() || record.getEnd() < 0 || record.getStart() < 0 || record.getStart() > 5000) {
        System.out.println("Wrong start / end date: " + record.getPid() + " / " + record.getStart() + " / " + record.getEnd());
        continue;
      }
      minPid = Math.min(minPid, record.getPid());
      maxPid = Math.max(maxPid, record.getPid());
      if (pos.get() > Integer.MAX_VALUE) {
        LongArrayList lst = pidToPositions.get(record.getPid());
        if (lst == null) {
          lst = new LongArrayList();
          pidToPositions.put(record.getPid(), lst);
        }
        lst.add(pos.get());
      }
      else {
        IntArrayList lst = pidToPositionsInt.get(record.getPid());
        if (lst == null) {
          lst = new IntArrayList();
          pidToPositionsInt.put(record.getPid(), lst);
        }
        lst.add((int) pos.get());
      }
      if (mem != null) {
        record.serialize(mem, pos);
      }
    }
    reader.close();
    return new ShardResult(minPid, maxPid, pidToPositions, pidToPositionsInt);
  }

  private static ShardResult readDShardToMemory(final OffHeapMemory mem, final TruvenDReader reader) throws IOException {
    final IntKeyObjectMap<LongArrayList> pidToPositions = new IntKeyObjectMap<>();
    final IntKeyObjectMap<IntArrayList> pidToPositionsInt = new IntKeyObjectMap<>();
    final LongMutable pos = new LongMutable(0);
    int minPid = Integer.MAX_VALUE;
    int maxPid = 0;
    while (true) {
      TruvenDRecord record = null;
      try {
        record = reader.read();
      }
      catch (final Exception e) {
        break;
      }
      if (record.getEnd() < record.getStart() || record.getEnd() < 0 || record.getStart() < 0 || record.getStart() > 5000) {
        System.out.println("Wrong start / end date: " + record.getPid() + " / " + record.getStart() + " / " + record.getEnd());
        continue;
      }
      minPid = Math.min(minPid, record.getPid());
      maxPid = Math.max(maxPid, record.getPid());
      if (pos.get() > Integer.MAX_VALUE) {
        LongArrayList lst = pidToPositions.get(record.getPid());
        if (lst == null) {
          lst = new LongArrayList();
          pidToPositions.put(record.getPid(), lst);
        }
        lst.add(pos.get());
      }
      else {
        IntArrayList lst = pidToPositionsInt.get(record.getPid());
        if (lst == null) {
          lst = new IntArrayList();
          pidToPositionsInt.put(record.getPid(), lst);
        }
        lst.add((int) pos.get());
      }
      if (mem != null) {
        record.serialize(mem, pos);
      }
    }
    reader.close();
    final IntKeyObjectIterator<LongArrayList> i = pidToPositions.entries();
    while (i.hasNext()) {
      i.next();
      i.getValue().trimToSize();
    }
    final IntKeyObjectIterator<IntArrayList> ii = pidToPositionsInt.entries();
    while (ii.hasNext()) {
      ii.next();
      ii.getValue().trimToSize();
    }
    return new ShardResult(minPid, maxPid, pidToPositions, pidToPositionsInt);
  }

  private void dumpTables(final File folder, final boolean dumpCodes) throws IOException {
    System.out.println("Dumping males...");
    BufferedWriter writer = new BufferedWriter(new FileWriter(new File(folder, "males.txt")));
    final IntIterator i = males.iterator();
    while (i.hasNext()) {
      writer.write(i.next() + "\n");
    }
    writer.close();
    System.out.println("Dumping pids...");
    writer = new BufferedWriter(new FileWriter(new File(folder, "pidToId.txt")));
    final Iterator<Entry<Long, Integer>> pidIterator = pidToInt.entrySet().iterator();
    while (pidIterator.hasNext()) {
      final Entry<Long, Integer> entry = pidIterator.next();
      writer.write(entry.getKey() + "\t" + entry.getValue() + "\n");
    }
    writer.close();
    if (dumpCodes) {
      System.out.println("Dumping icd9...");
      writer = new BufferedWriter(new FileWriter(new File(folder, "icd9.txt")));
      Iterator<Entry<String, Integer>> icd9Iterator = icd9ToId.entrySet().iterator();
      while (icd9Iterator.hasNext()) {
        final Entry<String, Integer> entry = icd9Iterator.next();
        writer.write(entry.getKey() + "\t" + entry.getValue() + "\n");
      }
      writer.close();
      System.out.println("Dumping cpt...");
      writer = new BufferedWriter(new FileWriter(new File(folder, "cpt.txt")));
      icd9Iterator = cptToId.entrySet().iterator();
      while (icd9Iterator.hasNext()) {
        final Entry<String, Integer> entry = icd9Iterator.next();
        writer.write(entry.getKey() + "\t" + entry.getValue() + "\n");
      }
      writer.close();
    }
  }

  private static void consolidateStatistics(final File folder) throws IOException {
    System.out.println("Consolidating RXNORM...");
    final File[] files = folder.listFiles();
    if (files == null) {
      throw new IOException("Folder contains no files");
    }
    final IntKeyObjectMap<IntOpenHashSet> stats = new IntKeyObjectMap<>();
    for (final File file : files) {
      if (file.getName().endsWith(".rx_stats")) {
        final DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
        try {
          while (true) {
            final int key = in.readInt();
            final int size = in.readInt();
            IntOpenHashSet set = stats.get(key);
            if (set == null) {
              set = new IntOpenHashSet();
              stats.put(key, set);
            }
            for (int x = 0; x < size; x++) {
              set.add(in.readInt());
            }
          }
        }
        catch (final Exception e) {
          e.printStackTrace();
        }
        FileUtils.close(in);
      }
    }
    savePatientCntMap(stats, folder, "consolidated.rx");
    System.out.println("Consolidating ICD9...");
    stats.clear();
    for (final File file : files) {
      if (file.getName().endsWith(".icd9_stats")) {
        final DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
        try {
          while (true) {
            final int key = in.readInt();
            final int size = in.readInt();
            IntOpenHashSet set = stats.get(key);
            if (set == null) {
              set = new IntOpenHashSet();
              stats.put(key, set);
            }
            for (int x = 0; x < size; x++) {
              set.add(in.readInt());
            }
          }
        }
        catch (final Exception e) {
          e.printStackTrace();
        }
        FileUtils.close(in);
      }
    }
    savePatientCntMap(stats, folder, "consolidated.icd9");
    System.out.println("Consolidating CPT...");
    stats.clear();
    for (final File file : files) {
      if (file.getName().endsWith(".cpt_stats")) {
        final DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
        try {
          while (true) {
            final int key = in.readInt();
            final int size = in.readInt();
            IntOpenHashSet set = stats.get(key);
            if (set == null) {
              set = new IntOpenHashSet();
              stats.put(key, set);
            }
            for (int x = 0; x < size; x++) {
              set.add(in.readInt());
            }
          }
        }
        catch (final Exception e) {
          e.printStackTrace();
        }
        FileUtils.close(in);
      }
    }
    savePatientCntMap(stats, folder, "consolidated.cpt");
  }

  private BufferedReader[] mergeReaders(final BufferedReader[] reader1, final BufferedReader[] reader2) {
    final BufferedReader[] result = new BufferedReader[reader1.length + reader2.length];
    int pos = 0;
    for (final BufferedReader r : reader1) {
      result[pos++] = r;
    }
    for (final BufferedReader r : reader2) {
      result[pos++] = r;
    }
    return result;
  }

  public void generateFiles(final String type, final File input, final File output) throws IOException, ParseException, InterruptedException {
    //readMaps(output);
    //readPidMap(output);
    final TruvenExtractor extractor = new TruvenExtractor();
    if (type.indexOf('i') != -1) {
      extractor.extractI("i.csv", TruvenOutput.createFromFolder(Integer.MAX_VALUE, output, "i"), mergeReaders(getIReaders(input, "ccaei"), getIReaders(input, "mdcri")));
    }
    if (type.indexOf('d') != -1) {
      extractor.extractD("d.csv", TruvenOutput.createFromFolder(Integer.MAX_VALUE, output, "d"), mergeReaders(getIReaders(input, "ccaed"), getIReaders(input, "mdcrd")));
    }
    if (type.indexOf('o') != -1) {
      extractor.extractI("o.csv", TruvenOutput.createFromFolder(Integer.MAX_VALUE, output, "o"), mergeReaders(getIReaders(input, "ccaeo"), getIReaders(input, "mdcro")));
    }
    if (type.indexOf('s') != -1) {
      extractor.extractI("s.csv", TruvenOutput.createFromFolder(Integer.MAX_VALUE, output, "s"), mergeReaders(getIReaders(input, "ccaes"), getIReaders(input, "mdcrs")));
    }
    extractor.dumpTables(output, true);
  }

  public static void generateMedicationFiles(final File input, final File output) throws IOException, ParseException, InterruptedException {
    final TruvenExtractor extractor = new TruvenExtractor();
    extractor.readPidToOriginalIdMap(output);
    extractor.readMaps(output);
    extractor.extractD("d.csv", TruvenOutput.createFromFolder(Integer.MAX_VALUE, output, "d"), getIReaders(input, "ccaed"));
    extractor.dumpTables(output, false);
  }

  public static void main(final String[] args) throws IOException, ParseException, InterruptedException {
    Common.FILTERING_ENABLED = false;
    System.out.println("Usage: action inputFolder outputFolder");
    System.out.println();
    System.out.println("action = 1 : phase 1 (generate compressed, bucketed output files)");
    System.out.println("         2 : phase 2 (consolidate patient count statistics)");
    System.out.println("         3 : phase 3 (generate ATLAS files)");
    System.out.println("         4 : phase 4 (loads statistics file, generates ATC hierarchy from each RxNorm code and stores new statistics)");
    System.out.println();
    System.out.println("Correct workflow is: 1 => 2 => 3 => 4");
    if (args[0].indexOf('1') != -1) {
      final long time = System.currentTimeMillis();
      final TruvenExtractor extractor = new TruvenExtractor();
      extractor.generateFiles("oisd", new File(args[1]), new File(args[2]));
      System.out.println("Phase 1 took " + ((System.currentTimeMillis() - time) / 60000) + " minutes");
    }
    if (args[0].indexOf('3') != -1) {
      final long time = System.currentTimeMillis();
      final TruvenExtractor extractor = new TruvenExtractor();
      extractor.sortFiles(new File(args[2]));
      System.out.println("Phase 3 took " + ((System.currentTimeMillis() - time) / (double) 60000) + " minutes");
    }
    if (args[0].indexOf('2') != -1) {
      final long time = System.currentTimeMillis();
      consolidateStatistics(new File(args[2]));
      System.out.println("Phase 2 took " + ((System.currentTimeMillis() - time) / 60000) + " minutes");
    }
    if (args[0].indexOf('4') != -1) {
      final long time = System.currentTimeMillis();
      final TruvenExtractor extractor = new TruvenExtractor();
      extractor.generateAtcStatistics(new File(args[2]));
      System.out.println("Phase 4 took " + ((System.currentTimeMillis() - time) / 60000) + " minutes");
    }
  }
}
