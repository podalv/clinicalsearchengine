package com.podalv.extractor.truven;

import java.io.DataOutputStream;
import java.io.IOException;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.utils.datastructures.LongMutable;

public class TruvenDRecord extends TruvenRecord {

  private int    pid;
  private short  start;
  private short  end;
  private int    sex;
  private short  age;
  private int    rx;
  private String visitType = null;

  public void setVisitType(final String visitType) {
    this.visitType = visitType;
  }

  public String getVisitType() {
    return visitType;
  }

  public static TruvenRecord deserialize(final OffHeapMemory mem, long pos) {
    Object result = null;
    final short start = mem.getShort(pos);
    pos += 2;
    final short end = mem.getShort(pos);
    pos += 2;
    final short age = mem.getShort(pos);
    pos += 2;
    final byte type = mem.getByte(pos++);
    if (type == -1) {
      result = new TruvenDRecord();
      ((TruvenDRecord) result).setStart(start);
      ((TruvenDRecord) result).setEnd(end);
      ((TruvenDRecord) result).setAge(age);
      final int rx = mem.getInt(pos);
      ((TruvenDRecord) result).setRx(rx);
    }
    else {
      result = new TruvenIRecord();
      ((TruvenIRecord) result).setStart(start);
      ((TruvenIRecord) result).setEnd(end);
      ((TruvenIRecord) result).setAge(age);
      byte len = type;
      for (int x = 0; x < len; x++) {
        ((TruvenIRecord) result).addIcd9(mem.getShort(pos));
        pos += 2;
      }
      len = mem.getByte(pos++);
      for (int x = 0; x < len; x++) {
        ((TruvenIRecord) result).addCpt(mem.getShort(pos));
        pos += 2;
      }
    }
    return (TruvenRecord) result;
  }

  public void serialize(final OffHeapMemory mem, final LongMutable pos) {
    mem.putShort(pos.get(), start);
    pos.addAndGet(2);
    mem.putShort(pos.get(), end);
    pos.addAndGet(2);
    mem.putShort(pos.get(), age);
    pos.addAndGet(2);
    mem.putByte(pos.get(), (byte) -1);
    pos.addAndGet(1);
    mem.putInt(pos.get(), rx);
    pos.addAndGet(4);
  }

  public void serialize(final DataOutputStream stream) throws IOException {
    stream.writeInt(pid);
    stream.writeShort(start);
    stream.writeShort(end);
    stream.writeShort(age);
    stream.writeByte((byte) -1);
    stream.writeInt(rx);
  }

  public void setAge(final short age) {
    this.age = age;
  }

  public void setEnd(final short end) {
    this.end = end;
  }

  public void setRx(final int rx) {
    this.rx = rx;
  }

  public void setPid(final int pid) {
    this.pid = pid;
  }

  public void setSex(final int sex) {
    this.sex = sex;
  }

  public void setStart(final short start) {
    this.start = start;
  }

  @Override
  public short getAge() {
    return age;
  }

  public short getEnd() {
    return end;
  }

  public int getRx() {
    return rx;
  }

  public int getPid() {
    return pid;
  }

  public int getSex() {
    return sex;
  }

  @Override
  public short getStart() {
    return start;
  }

  @Override
  public int compareTo(final TruvenRecord arg0) {
    return Short.compare(getStart(), arg0.getStart());
  }

  @Override
  public String toString() {
    return "PID=" + pid + ",START=" + start + ",END=" + end + ",SEX=" + sex + ",AGE=" + age + ",RX=" + rx;
  }

}