package com.podalv.extractor.truven;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TruvenConst {

  public static final String MALE            = "1";
  public static final String FEMALE          = "2";

  public static final String MALE_STR        = "MALE";
  public static final String FEMALE_STR      = "FEMALE";

  public static final String SEX_COL         = "SEX";
  public static final String PATID_COL       = "ENROLID";
  public static final String ADMDATE_COL     = "ADMDATE";
  public static final String NDC_COL         = "NDCNUM";
  public static final String DAYS_SUPPLY_COL = "DAYSUPP";
  public static final String DISDATE_COL     = "DISDATE";
  public static final String SVCDATE_COL     = "SVCDATE";
  public static final String AGE_COL         = "AGE";
  public static final String DX_COL          = "DX";
  public static final String PROC_COL        = "PROC";
  public static long         REFERENCE_DATE  = -1;

  static {
    try {
      REFERENCE_DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse("2007-01-01 00:00:00.000").getTime();
    }
    catch (final ParseException e) {
      e.printStackTrace();
    }
  }

  public static final int dateToDays(final long time) {
    return (int) ((time - REFERENCE_DATE) / (1000 * 60 * 60 * 24));
  }

}
