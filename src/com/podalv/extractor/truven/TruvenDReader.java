package com.podalv.extractor.truven;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/** Reads the generated i.1 i.2, etc. files
*
* @author podalv
*
*/
public class TruvenDReader implements Closeable {

  private final DataInputStream stream;

  public TruvenDReader(final InputStream s) {
    this.stream = new DataInputStream(new BufferedInputStream(s));
  }

  public TruvenDRecord read() throws IOException {
    final TruvenDRecord record = new TruvenDRecord();
    record.setPid(stream.readInt());
    final short time1 = stream.readShort();
    final short time2 = stream.readShort();

    record.setStart(time1);
    record.setEnd(time2);

    record.setAge(stream.readShort());
    record.setRx(stream.readInt());
    return record;
  }

  @Override
  public void close() throws IOException {
    stream.close();
  }

}
