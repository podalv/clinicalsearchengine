package com.podalv.extractor.truven;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.utils.text.TextUtils;

public class NdcToRxIngredient {

  private final HashMap<String, IntArrayList> ndcToRx = new HashMap<String, IntArrayList>();

  private NdcToRxIngredient() {
    // static access only
  }

  public static NdcToRxIngredient create() {
    final NdcToRxIngredient result = new NdcToRxIngredient();
    try {
      final BufferedReader reader = new BufferedReader(new InputStreamReader(NdxToRxNorm.class.getResourceAsStream("ndcToRx.txt")));
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '\t');
        IntArrayList list = result.ndcToRx.get(data[0]);
        if (list == null) {
          list = new IntArrayList();
          result.ndcToRx.put(data[0], list);
        }
        for (int x = 1; x < data.length; x++) {
          list.add(Integer.parseInt(data[x]));
        }
      }
      reader.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  public IntArrayList getRx(final String ndc) {
    return ndcToRx.get(ndc);
  }
}
