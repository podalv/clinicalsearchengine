package com.podalv.extractor.truven;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/** Shards data by hash code
 *
 * @author podalv
 *
 */
public class TruvenOutput implements Closeable {

  private final DataOutputStream[] writers;
  private final File               outputFolder;
  public static final int          divider = 150000000 / 10;

  public static TruvenOutput createFromFolder(final int maxPatientId, final File folder, final String filePrefix) throws FileNotFoundException {
    final DataOutputStream[] writers = new DataOutputStream[10];
    for (int x = 0; x < 10; x++) {
      writers[x] = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(folder, filePrefix + "." + x))));
    }
    return new TruvenOutput(maxPatientId, folder, writers);
  }

  public File getOutputFolder() {
    return outputFolder;
  }

  public TruvenOutput(final int maxPatientId, final File outputFolder, final DataOutputStream ... writers) {
    this.outputFolder = outputFolder;
    this.writers = writers;
  }

  private int getPos(final long pid) {
    return (int) Math.floor(pid / (double) divider);
  }

  public DataOutputStream getWriter(final long pid) {
    return writers[getPos(pid)];
  }

  @Override
  public void close() throws IOException {
    for (int x = 0; x < writers.length; x++) {
      writers[x].close();
    }
  }
}
