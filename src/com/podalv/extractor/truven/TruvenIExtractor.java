package com.podalv.extractor.truven;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.utils.text.TextUtils;

/** Reads the Truven I .csv files
 *
 * @author podalv
 *
 */
public class TruvenIExtractor implements TruvenFileReader, Closeable {

  private final BufferedReader reader;
  private int                  pidColId;
  private int                  startDateColId;
  private int                  endDateColId;
  private int                  ageColId;
  private int                  sexColId;
  private String[]             currentLine = null;
  private final IntArrayList   dxColId     = new IntArrayList();
  private final IntArrayList   procColId   = new IntArrayList();

  public static TruvenIExtractor createEmpty() {
    TruvenIExtractor result = null;
    try {
      result = new TruvenIExtractor(null);
    }
    catch (final Exception e) {
      // Irrelevant we are creating an empty instance
    }
    return result;
  }

  public TruvenIExtractor(final BufferedReader reader) throws IOException {
    this.reader = reader;
    readHeader();
  }

  private void readHeader() throws IOException {
    final String[] data = TextUtils.split(reader.readLine(), ',');
    int admDate = -1;
    int disDate = -1;
    int svcDate = -1;
    for (int x = 0; x < data.length; x++) {
      if (data[x].equals(TruvenConst.SEX_COL)) {
        sexColId = x;
      }
      if (data[x].equals(TruvenConst.PATID_COL)) {
        pidColId = x;
      }
      if (data[x].equals(TruvenConst.ADMDATE_COL)) {
        admDate = x;
      }
      if (data[x].equals(TruvenConst.SVCDATE_COL)) {
        svcDate = x;
      }
      if (data[x].equals(TruvenConst.DISDATE_COL)) {
        disDate = x;
      }
      if (data[x].equals(TruvenConst.AGE_COL)) {
        ageColId = x;
      }
      if (data[x].startsWith(TruvenConst.DX_COL)) {
        try {
          Integer.parseInt(data[x].substring(TruvenConst.DX_COL.length()));
          dxColId.add(x);
        }
        catch (final Exception e) {
          // not a DXNUM column, just skip it
        }
      }
      if (data[x].startsWith(TruvenConst.PROC_COL)) {
        try {
          Integer.parseInt(data[x].substring(TruvenConst.PROC_COL.length()));
          procColId.add(x);
        }
        catch (final Exception e) {
          // not a DXNUM column, just skip it
        }
      }
    }
    if (svcDate != -1 && admDate == -1) {
      startDateColId = svcDate;
      endDateColId = svcDate;
    }
    else if (admDate != -1 && disDate != -1) {
      startDateColId = admDate;
      endDateColId = disDate;
    }
  }

  @Override
  public boolean next() throws IOException {
    final String line = reader.readLine();
    if (line == null) {
      currentLine = null;
      return false;
    }
    else {
      currentLine = TextUtils.split(line, ',');
      return true;
    }
  }

  @Override
  public String getPid() {
    if (currentLine != null) {
      return currentLine[pidColId];
    }
    return null;
  }

  public String getSex() {
    if (currentLine != null) {
      return currentLine[sexColId];
    }
    return null;
  }

  public String getAge() {
    if (currentLine != null) {
      return currentLine[ageColId];
    }
    return null;
  }

  public String getStartDate() {
    if (currentLine != null) {
      return currentLine[startDateColId];
    }
    return null;
  }

  public String getEndDate() {
    if (currentLine != null) {
      return currentLine[endDateColId];
    }
    return null;
  }

  public Iterator<String> getDx() {
    final Iterator<String> result = new Iterator<String>() {

      int curPos = 0;

      @Override
      public boolean hasNext() {
        return curPos < dxColId.size();
      }

      @Override
      public String next() {
        if (currentLine != null) {
          return currentLine[dxColId.get(curPos++)];
        }
        curPos++;
        return null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
    return result;
  }

  public Iterator<String> getProc() {
    final Iterator<String> result = new Iterator<String>() {

      int curPos = 0;

      @Override
      public boolean hasNext() {
        return curPos < procColId.size();
      }

      @Override
      public String next() {
        if (currentLine != null) {
          return currentLine[procColId.get(curPos++)];
        }
        curPos++;
        return null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
    return result;
  }

  @Override
  public void close() throws IOException {
    reader.close();
  }

}
