package com.podalv.extractor.truven.test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.podalv.extractor.truven.TruvenExtractor;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

public class TruvenTestHarness {

  private final static File folder = new File(".", "extractionTestData");

  public static TruvenSourceFileGenerator getHarness() throws IOException {
    return new TruvenSourceFileGenerator(folder);
  }

  public static void clearData() {
    final File[] files = folder.listFiles();
    if (files != null) {
      for (final File file : files) {
        FileUtils.deleteWithMessage(file);
      }
    }
  }

  public static IntKeyIntOpenHashMap readNewPidToOldPidMap() throws IOException {
    final IntKeyIntOpenHashMap result = new IntKeyIntOpenHashMap();
    final ArrayList<String> arr = FileUtils.readFileArrayList(new File(folder, "pidToId.txt"), Charset.forName("UTF-8"));
    for (int x = 0; x < arr.size(); x++) {
      final String[] data = TextUtils.split(arr.get(x), '\t');
      result.put(Integer.parseInt(data[1]), Integer.parseInt(data[0]));
    }
    return result;
  }

  public static void generateAtlas(final File inputFolder, final File outputFolder) {
    try {
      TruvenExtractor.main(new String[] {"1", inputFolder.getAbsolutePath(), outputFolder.getAbsolutePath()});
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    try {
      TruvenExtractor.main(new String[] {"2", inputFolder.getAbsolutePath(), outputFolder.getAbsolutePath()});
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    try {
      TruvenExtractor.main(new String[] {"3", inputFolder.getAbsolutePath(), outputFolder.getAbsolutePath()});
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    try {
      TruvenExtractor.main(new String[] {"4", inputFolder.getAbsolutePath(), outputFolder.getAbsolutePath()});
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  public static ClinicalSearchEngine getEngine(final TruvenSourceFileGenerator generator) throws InstantiationException, IllegalAccessException, IOException, InterruptedException {
    generator.close();
    generateAtlas(generator.getFolder(), generator.getFolder());
    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, generator.getFolder(), generator.getFolder());
  }
}
