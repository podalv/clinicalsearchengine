package com.podalv.extractor.truven;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.list.LongArrayList;
import com.podalv.maps.string.IntKeyObjectMap;

public class ShardResult {

  private final int                            min;
  private final int                            max;
  private final IntKeyObjectMap<LongArrayList> pidToPosLong;
  private final IntKeyObjectMap<IntArrayList>  pidToPosInt;

  public ShardResult(final int min, final int max, final IntKeyObjectMap<LongArrayList> pidToPosLong, final IntKeyObjectMap<IntArrayList> pidToPosInt) {
    this.max = max;
    this.min = min;
    this.pidToPosInt = pidToPosInt;
    this.pidToPosLong = pidToPosLong;
  }

  public int getMax() {
    return max;
  }

  public int getMin() {
    return min;
  }

  public IntKeyObjectMap<IntArrayList> getPidToPosInt() {
    return pidToPosInt;
  }

  public IntKeyObjectMap<LongArrayList> getPidToPosLong() {
    return pidToPosLong;
  }
}
