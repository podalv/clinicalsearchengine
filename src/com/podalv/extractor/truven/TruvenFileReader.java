package com.podalv.extractor.truven;

import java.io.IOException;

public interface TruvenFileReader {

  public boolean next() throws IOException;

  public String getPid();

}
