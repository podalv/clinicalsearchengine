package com.podalv.extractor.truven;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;

import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.text.TextUtils;

public class NdxToRxNorm {

  private final HashMap<String, HashSet<String>> ndcToRxNorm = new HashMap<String, HashSet<String>>();

  public HashSet<String> getRxNorm(final String ndc) {
    return ndcToRxNorm.get(ndc);
  }

  public static String normalizeNdc(final String ndc) {
    final String[] data = TextUtils.split(ndc.replace('*', '0'), '-');
    if (data.length == 3) {
      if (data[0].length() == 6) {
        if (data[1].length() == 4 && data[2].length() == 2) {
          return data[0].substring(1) + data[1] + data[2];
        }
        else if (data[1].length() == 4 && data[2].length() == 1) {
          return data[0].substring(1) + data[1] + "0" + data[2];
        }
        else if (data[1].length() == 3 && data[2].length() == 2) {
          return data[0].substring(1) + "0" + data[1] + data[2];
        }
        else if (data[1].length() == 3 && data[2].length() == 1) {
          return data[0].substring(1) + "0" + data[1] + "0" + data[2];
        }
      }
      else if (data[0].length() == 5) {
        if (data[1].length() == 4 && data[2].length() == 2) {
          return data[0] + data[1] + data[2];
        }
        else if (data[1].length() == 4 && data[2].length() == 1) {
          return data[0] + data[1] + "0" + data[2];
        }
        else if (data[1].length() == 3 && data[2].length() == 2) {
          return data[0] + "0" + data[1] + data[2];
        }
      }
      else if (data[0].length() == 4) {
        return "0" + data[0] + data[1] + data[2];
      }
    }
    else if (data[0].length() == 11) {
      return data[0];
    }
    else if (data[0].length() == 12 && data[0].charAt(0) == '0') {
      return data[0].substring(1);
    }
    return null;
  }

  public static NdxToRxNorm create() {
    final NdxToRxNorm result = new NdxToRxNorm();
    try {
      final BufferedReader reader = new BufferedReader(new InputStreamReader(NdxToRxNorm.class.getResourceAsStream("NDC_RXNORM.RRF")));
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] data = TextUtils.split(line, '|');
        final String rx = data[0];
        final String ndc = normalizeNdc(data[10]);
        if (ndc == null) {
          System.out.println("could not normalize '" + data[10] + "'");
        }
        HashSet<String> set = result.ndcToRxNorm.get(ndc);
        if (set == null) {
          set = new HashSet<String>();
          result.ndcToRxNorm.put(ndc, set);
        }
        set.add(rx);
      }
      reader.close();
    }
    catch (final IOException e) {
      result.ndcToRxNorm.clear();
    }

    return result;
  }

  public static void main(final String[] args) throws IOException {
    System.out.println("Usage: redbook.csv");
    final TextFileReader reader = new TextFileReader(new File(args[0]), Charset.forName("UTF-8"));
    reader.next();
    final NdxToRxNorm ndcToRxMap = NdxToRxNorm.create();
    int hit = 0;
    int miss = 0;
    while (reader.hasNext()) {
      final String line = reader.next();
      final String ndc = line.substring(0, line.indexOf(','));
      if (ndcToRxMap.getRxNorm(ndc) == null) {
        //System.out.println(line);
        miss++;
      }
      else {
        hit++;
      }
    }
    reader.close();
    System.out.println(hit + " / " + miss);
  }
}
