package com.podalv.extractor.truven;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/** Reads the generated i.1 i.2, etc. files
 *
 * @author podalv
 *
 */
public class TruvenReader implements Closeable {

  private final DataInputStream stream;

  public TruvenReader(final InputStream s) {
    this.stream = new DataInputStream(new BufferedInputStream(s));
  }

  public TruvenIRecord read() throws IOException {
    final TruvenIRecord record = new TruvenIRecord();
    record.setPid(stream.readInt());
    final short time1 = stream.readShort();
    final short time2 = stream.readShort();

    record.setStart(time1);
    record.setEnd(time2);

    record.setAge(stream.readShort());
    while (true) {
      final short curVal = stream.readShort();
      if (curVal == -1) {
        break;
      }
      else {
        record.addIcd9(curVal);
      }
    }
    while (true) {
      final short curVal = stream.readShort();
      if (curVal == -1) {
        break;
      }
      else {
        record.addCpt(curVal);
      }
    }
    return record;
  }

  @Override
  public void close() throws IOException {
    stream.close();
  }

}
