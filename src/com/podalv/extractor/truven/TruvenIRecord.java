package com.podalv.extractor.truven;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.utils.datastructures.LongMutable;

public class TruvenIRecord extends TruvenRecord {

  private int                     pid;
  private short                   start;
  private short                   end;
  private int                     sex;
  private short                   age;
  private String                  visitType = null;
  private final LinkedList<Short> icd9      = new LinkedList<>();
  private final LinkedList<Short> cpt       = new LinkedList<>();

  public void setVisitType(final String visitType) {
    this.visitType = visitType;
  }

  public String getVisitType() {
    return visitType;
  }

  public void serialize(final OffHeapMemory mem, final LongMutable pos) {
    mem.putShort(pos.get(), start);
    pos.addAndGet(2);
    mem.putShort(pos.get(), end);
    pos.addAndGet(2);
    mem.putShort(pos.get(), age);
    pos.addAndGet(2);
    mem.putByte(pos.get(), (byte) icd9.size());
    pos.incrementAndGet();
    for (final short code : icd9) {
      mem.putShort(pos.get(), code);
      pos.addAndGet(2);
    }
    mem.putByte(pos.get(), (byte) cpt.size());
    pos.incrementAndGet();
    for (final short code : cpt) {
      mem.putShort(pos.get(), code);
      pos.addAndGet(2);
    }
  }

  public void serialize(final DataOutputStream stream) throws IOException {
    stream.writeInt(pid);
    stream.writeShort(start);
    stream.writeShort(end);
    stream.writeShort(age);
    stream.writeByte(icd9.size());
    for (final short code : icd9) {
      stream.writeShort(code);
    }
    stream.writeByte(cpt.size());
    for (final short code : cpt) {
      stream.writeShort(code);
    }
  }

  public void setAge(final short age) {
    this.age = age;
  }

  public void addCpt(final short code) {
    this.cpt.add(code);
  }

  public void setEnd(final short end) {
    this.end = end;
  }

  public void addIcd9(final short code) {
    this.icd9.add(code);
  }

  public void setPid(final int pid) {
    this.pid = pid;
  }

  public void setSex(final int sex) {
    this.sex = sex;
  }

  public void setStart(final short start) {
    this.start = start;
  }

  @Override
  public short getAge() {
    return age;
  }

  public LinkedList<Short> getCpt() {
    return cpt;
  }

  public short getEnd() {
    return end;
  }

  public LinkedList<Short> getIcd9() {
    return icd9;
  }

  public int getPid() {
    return pid;
  }

  public int getSex() {
    return sex;
  }

  @Override
  public short getStart() {
    return start;
  }

  @Override
  public int compareTo(final TruvenRecord arg0) {
    return Short.compare(getStart(), arg0.getStart());
  }

  @Override
  public String toString() {
    return "PID=" + pid;
  }

}