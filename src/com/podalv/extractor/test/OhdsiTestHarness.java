package com.podalv.extractor.test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.stride6.AtlasDatabaseDownload;
import com.podalv.extractor.stride6.OhdsiDatabaseDownload;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.extractor.stride6.exclusion.DefaultEventEvaluator;
import com.podalv.extractor.stride6.exclusion.StridePatientExclusion;
import com.podalv.extractor.test.datastructures.OhdsiTestDataSource;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.server.ClinicalSearchEngine;

/** Allows running tests throughout the whole pipeline.
 *
 * @author podalv
 *
 */
public class OhdsiTestHarness {

  public static ClinicalSearchEngine engine(final OhdsiTestDataSource source, final File outputFolder, final File rootFolder, final int patientCnt) throws SQLException,
      IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    final OhdsiDatabaseDownload downloader = new OhdsiDatabaseDownload(ConnectionSettings.createFromConnection(source.generate()), AtlasDatabaseDownload.DATABASE);
    downloader.process(outputFolder);
    Stride6Extractor.execute(new String[] {"dit", outputFolder.getAbsolutePath(), outputFolder.getAbsolutePath(), StridePatientExclusion.class.getName(),
        DefaultEventEvaluator.class.getName(), patientCnt + ""});
    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, outputFolder, rootFolder);
  }
}