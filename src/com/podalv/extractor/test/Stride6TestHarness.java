package com.podalv.extractor.test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.stride6.AtlasDatabaseDownload;
import com.podalv.extractor.stride6.OhdsiDatabaseDownload;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.extractor.stride6.exclusion.DefaultEventEvaluator;
import com.podalv.extractor.stride6.exclusion.StridePatientExclusion;
import com.podalv.extractor.test.datastructures.AtlasTestDataSource;
import com.podalv.extractor.test.datastructures.Stride6TestDataSource;
import com.podalv.extractor.test.datastructures.Stride6ToAtlasDataSourceConvertor;
import com.podalv.extractor.test.datastructures.Stride6ToOhdsiDataSourceConvertor;
import com.podalv.objectdb.datastructures.DatabaseSettings;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.PatientExport;
import com.podalv.search.server.PatientExportTestInstance;
import com.podalv.search.server.QueryCache;

public class Stride6TestHarness {

  private static void performSourceSpecificDownload(final Stride6TestDataSource source, final File outputFolder) throws SQLException, IOException {
    if (source instanceof Stride6ToOhdsiDataSourceConvertor) {
      final OhdsiDatabaseDownload download = new OhdsiDatabaseDownload(ConnectionSettings.createFromConnection(source.generate()), AtlasDatabaseDownload.DATABASE);
      download.process(outputFolder);
    }
    else if (source instanceof Stride6ToAtlasDataSourceConvertor) {
      final AtlasDatabaseDownload download = new AtlasDatabaseDownload(ConnectionSettings.createFromConnection(source.generate()), AtlasDatabaseDownload.DATABASE, outputFolder);
      download.extract();
    }
    else {
      Stride6DatabaseDownload.execute(new String[] {"dutvmsnrpxlc", outputFolder.getAbsolutePath()}, ConnectionSettings.createFromConnection(source.generate()));
    }
  }

  public static ClinicalSearchEngine engine(final Stride6TestDataSource source, final File outputFolder, final File rootFolder) throws SQLException, IOException,
      InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    performSourceSpecificDownload(source, outputFolder);
    Stride6Extractor.execute(new String[] {"dit", outputFolder.getAbsolutePath(), outputFolder.getAbsolutePath(), StridePatientExclusion.class.getName(),
        DefaultEventEvaluator.class.getName(), Integer.MAX_VALUE + ""});
    PatientExport.setInstance(new PatientExportTestInstance(null));
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    QueryCache.disable();
    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, outputFolder, rootFolder);
  }

  public static ClinicalSearchEngine engine(final AtlasTestDataSource source, final File outputFolder, final File rootFolder) throws SQLException, IOException,
      InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    final AtlasDatabaseDownload download = new AtlasDatabaseDownload(ConnectionSettings.createFromConnection(source.generate()), AtlasDatabaseDownload.DATABASE, outputFolder);
    download.extract();
    Stride6Extractor.execute(new String[] {"dit", outputFolder.getAbsolutePath(), outputFolder.getAbsolutePath(), StridePatientExclusion.class.getName(),
        DefaultEventEvaluator.class.getName(), Integer.MAX_VALUE + ""});
    PatientExport.setInstance(new PatientExportTestInstance(null));
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    QueryCache.disable();
    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, outputFolder, rootFolder);
  }

  public static ClinicalSearchEngine engine(final SERVER_TYPE type, final Stride6TestDataSource source, final File outputFolder, final File rootFolder) throws SQLException,
      IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    performSourceSpecificDownload(source, outputFolder);
    Stride6Extractor.execute(new String[] {"dit", outputFolder.getAbsolutePath(), outputFolder.getAbsolutePath(), StridePatientExclusion.class.getName(),
        DefaultEventEvaluator.class.getName(), Integer.MAX_VALUE + ""});
    PatientExport.setInstance(new PatientExportTestInstance(null));
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    QueryCache.disable();
    return new ClinicalSearchEngine(type, outputFolder, rootFolder);
  }

  public static void extract(final Stride6TestDataSource source, final File rootFolder, final DatabaseSettings settings) throws SQLException, IOException, InterruptedException,
      InstantiationException, IllegalAccessException, ClassNotFoundException {
    performSourceSpecificDownload(source, settings.getInputFolder());
    Stride6Extractor.execute(new String[] {"dit", settings.getInputFolder().getAbsolutePath(), settings.getInputFolder().getAbsolutePath(), Integer.MAX_VALUE + ""});
  }

  public static ClinicalSearchEngine startWithoutExtraction(final SERVER_TYPE type, final Stride6TestDataSource source, final File rootFolder, final DatabaseSettings settings)
      throws SQLException, IOException, InterruptedException, InstantiationException, IllegalAccessException {
    PatientExport.setInstance(new PatientExportTestInstance(null));
    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(".", "export")));
    QueryCache.disable();
    return new ClinicalSearchEngine(type, rootFolder, settings);
  }

  public static ClinicalSearchEngine engine(final SERVER_TYPE type, final Stride6TestDataSource source, final File rootFolder, final DatabaseSettings settings) throws SQLException,
      IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    extract(source, rootFolder, settings);
    return startWithoutExtraction(type, source, rootFolder, settings);
  }

}
