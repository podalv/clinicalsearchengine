package com.podalv.extractor.test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.stride6.Stride6Extractor;
import com.podalv.extractor.stride6.Stride7DatabaseDownload;
import com.podalv.extractor.stride6.exclusion.DefaultPatientExclusion;
import com.podalv.extractor.stride6.exclusion.Stride7EventEvaluator;
import com.podalv.extractor.test.datastructures.Stride7TestDataSource;
import com.podalv.objectdb.datastructures.DatabaseSettings;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.server.ClinicalSearchEngine;
import com.podalv.search.server.QueryCache;

public class Stride7TestHarness {

  public static ClinicalSearchEngine engine(final Stride7TestDataSource source, final File outputFolder, final File rootFolder, final int patientCnt) throws SQLException,
      IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    Stride7DatabaseDownload.execute(new String[] {"drmvlxutn", outputFolder.getAbsolutePath()}, ConnectionSettings.createFromConnection(source.generate()));
    Stride6Extractor.execute(new String[] {"dit", outputFolder.getAbsolutePath(), outputFolder.getAbsolutePath(), DefaultPatientExclusion.class.getName(),
        Stride7EventEvaluator.class.getName(), patientCnt + ""});
    QueryCache.disable();
    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, outputFolder, rootFolder, DatabaseSettings.create(PatientBuilder.class, outputFolder).withOffHeapSize(
        200000000).clearDiskCache(false));
  }

  public static ClinicalSearchEngine engineWithExclusion(final Stride7TestDataSource source, final File outputFolder, final File rootFolder, final int patientCnt)
      throws SQLException, IOException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    Stride7DatabaseDownload.execute(new String[] {"drmvlxutn", outputFolder.getAbsolutePath()}, ConnectionSettings.createFromConnection(source.generate()));
    Stride6Extractor.execute(new String[] {"dit", outputFolder.getAbsolutePath(), outputFolder.getAbsolutePath(), DefaultPatientExclusion.class.getName(),
        Stride7EventEvaluator.class.getName(), patientCnt + ""});
    QueryCache.disable();
    return new ClinicalSearchEngine(SERVER_TYPE.INDEPENDENT, outputFolder, rootFolder, DatabaseSettings.create(PatientBuilder.class, outputFolder).withOffHeapSize(
        200000000).clearDiskCache(false));
  }

}
