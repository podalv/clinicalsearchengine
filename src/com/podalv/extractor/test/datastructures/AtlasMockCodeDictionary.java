package com.podalv.extractor.test.datastructures;

public class AtlasMockCodeDictionary extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"code_type", "code", "text"};
  private String               codeType     = "";
  private String               code         = "";
  private String               text         = "";

  public static AtlasMockCodeDictionary create(final String codeType, final String code, final String text) {
    return new AtlasMockCodeDictionary(codeType, code, text);
  }

  private AtlasMockCodeDictionary(final String codeType, final String code, final String text) {
    this.codeType = codeType;
    this.code = code;
    this.text = text;
  }

  @Override
  public long comparable() {
    return 0;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {codeType, code, text};
  }

  @Override
  public Stride6MockRecord copy() {
    return new AtlasMockCodeDictionary(codeType, code, text);
  }

}