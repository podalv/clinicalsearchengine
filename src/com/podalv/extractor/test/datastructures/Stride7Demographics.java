package com.podalv.extractor.test.datastructures;

public class Stride7Demographics extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "gender", "race", "ethnicity", "CEILING(age_at_death_in_days)", "YEAR(death_date)"};
  private final int            id;
  private String               race         = "";
  private String               gender       = "";
  private String               ethnicity    = "";
  private Integer              deathYear    = null;
  private Double               death        = null;

  public static Stride7Demographics create(final int id) {
    return new Stride7Demographics(id);
  }

  public Stride7Demographics race(final String race) {
    this.race = race;
    return this;
  }

  public Stride7Demographics gender(final String gender) {
    this.gender = gender;
    return this;
  }

  public Stride7Demographics setDeathYear(final Integer deathYear) {
    this.deathYear = deathYear;
    return this;
  }

  public Stride7Demographics ethnicity(final String ethnicity) {
    this.ethnicity = ethnicity;
    return this;
  }

  public Stride7Demographics death(final Double death) {
    this.death = death;
    return this;
  }

  public Integer getDeathYear() {
    return deathYear;
  }

  public int getId() {
    return id;
  }

  public String getEthnicity() {
    return ethnicity;
  }

  public Double getDeath() {
    return death;
  }

  public String getGender() {
    return gender;
  }

  public String getRace() {
    return race;
  }

  private Stride7Demographics(final int id) {
    this.id = id;
  }

  @Override
  public long comparable() {
    return id;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", gender, race, ethnicity, death == null ? null : Math.floor(death) + "", deathYear == null ? "0" : deathYear + ""};
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7Demographics result = new Stride7Demographics(id);
    result.death = death;
    result.ethnicity = ethnicity;
    result.gender = gender;
    result.race = race;
    return result;
  }

}