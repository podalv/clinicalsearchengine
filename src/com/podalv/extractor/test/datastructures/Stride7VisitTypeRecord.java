package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7VisitTypeRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "CEILING(age_at_contact_in_days)", "YEAR(contact_date)", "enc_type_c",
      "CEILING(age_at_hosp_admsn_in_days)", "CEILING(age_at_hosp_admsn_in_days)"};
  private final int            patientId;
  private Double               age          = null;
  private Integer              year         = null;
  private Integer              encType      = null;

  private Double               ageHospAdm   = null;
  private Double               ageHospDisch = null;

  public static Stride7VisitTypeRecord create(final int patientId) {
    return new Stride7VisitTypeRecord(patientId);
  }

  public Stride7VisitTypeRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7VisitTypeRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7VisitTypeRecord encType(final Integer encType) {
    this.encType = encType;
    return this;
  }

  public Stride7VisitTypeRecord hospAdm(final Double start, final Double end) {
    this.ageHospAdm = start;
    this.ageHospDisch = end;
    return this;
  }

  private Stride7VisitTypeRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", encType == null ? null : encType + "", ageHospAdm == null
        ? null : Math.floor(ageHospAdm) + "", ageHospDisch == null ? null : Math.floor(ageHospDisch) + ""};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7VisitTypeRecord result = new Stride7VisitTypeRecord(patientId);
    result.age(age);
    result.year(year);
    result.encType(encType);
    result.hospAdm(ageHospAdm, ageHospDisch);
    return result;
  }

}