package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7VisitWithSabRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "CEILING(age_at_contact_in_days)", "YEAR(contact_date)", "code", "sab", "visit_type"};
  private final int            patientId;
  private Double               age          = null;
  private Integer              year         = null;
  private String               code         = null;
  private String               sab          = null;
  private String               visitType    = null;

  public static Stride7VisitWithSabRecord create(final int patientId) {
    return new Stride7VisitWithSabRecord(patientId);
  }

  public Stride7VisitWithSabRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7VisitWithSabRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7VisitWithSabRecord code(final String code) {
    this.code = code;
    return this;
  }

  public Stride7VisitWithSabRecord sab(final String sab) {
    this.sab = sab;
    return this;
  }

  public Stride7VisitWithSabRecord visitType(final String visitType) {
    this.visitType = visitType;
    return this;
  }

  private Stride7VisitWithSabRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", code, sab, visitType};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7VisitWithSabRecord result = new Stride7VisitWithSabRecord(patientId);
    result.age(age);
    result.year(year);
    result.code(code);
    result.sab(sab);
    result.visitType(visitType);
    return result;
  }

}