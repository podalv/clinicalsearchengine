package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockVitalsRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "flo_meas_id", "age_at_vital_entered_in_days", "detail_display", "meas_value", "-1"};

  private final int            patientId;
  private Integer              floMeasId;
  private Double               age          = 0d;
  private String               detailDisplay;
  private Double               measValue;

  public static MockVitalsRecord create(final int patientId) {
    return new MockVitalsRecord(patientId);
  }

  private MockVitalsRecord(final int patientId) {
    this.patientId = patientId;
  }

  public Double getAge() {
    return age;
  }

  public String getDetailDisplay() {
    return detailDisplay;
  }

  public Integer getFloMeasId() {
    return floMeasId;
  }

  public Double getMeasValue() {
    return measValue;
  }

  public int getPatientId() {
    return patientId;
  }

  public MockVitalsRecord floMeasId(final Integer id) {
    this.floMeasId = id;
    return this;
  }

  public MockVitalsRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockVitalsRecord display(final String detailDisplay) {
    this.detailDisplay = detailDisplay;
    return this;
  }

  public MockVitalsRecord value(final Double value) {
    this.measValue = value;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", floMeasId == null ? null : floMeasId + "", age == null ? null : age + "", detailDisplay, measValue == null ? null : measValue + "", "-1"};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockVitalsRecord result = new MockVitalsRecord(patientId);
    result.age = age;
    result.detailDisplay = detailDisplay;
    result.floMeasId = floMeasId;
    result.measValue = measValue;
    return result;
  }
}
