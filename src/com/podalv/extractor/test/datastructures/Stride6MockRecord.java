package com.podalv.extractor.test.datastructures;

public abstract class Stride6MockRecord implements Comparable<Stride6MockRecord> {

  public abstract String[] toTableRow();

  public abstract long comparable();

  public abstract Stride6MockRecord copy();

  @Override
  public int compareTo(final Stride6MockRecord o) {
    return Long.compare(comparable(), o.comparable());
  }
}
