package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockTermRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "nid", "tid", "negated", "familyHistory"};

  private final int            patientId;
  private Integer              nid          = 0;
  private Integer              tid          = 0;
  private boolean              negated;
  private boolean              familyHistory;

  public static MockTermRecord create(final int patientId) {
    return new MockTermRecord(patientId);
  }

  public MockTermRecord negated() {
    this.negated = true;
    return this;
  }

  public int getPatientId() {
    return patientId;
  }

  public Integer getNid() {
    return nid;
  }

  public Integer getTid() {
    return tid;
  }

  public boolean getNegated() {
    return negated;
  }

  public boolean getFamilyHistory() {
    return familyHistory;
  }

  public MockTermRecord familyHistory() {
    this.familyHistory = true;
    return this;
  }

  public MockTermRecord noteId(final Integer nid) {
    this.nid = nid;
    return this;
  }

  public MockTermRecord termId(final Integer tid) {
    this.tid = tid;
    return this;
  }

  private MockTermRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", nid == null ? null : nid + "", tid == null ? null : tid + "", negated ? "1" : "0", familyHistory ? "1" : "0"};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(nid)).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockTermRecord result = new MockTermRecord(patientId);
    result.familyHistory = familyHistory;
    result.negated = negated;
    result.nid = nid;
    result.tid = tid;
    return result;
  }
}
