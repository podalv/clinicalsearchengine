package com.podalv.extractor.test.datastructures;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.podalv.extractor.stride6.AtlasDatabaseDownload;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;

/** Converts Stride6TestDataSource to Atlas Connection
*
* @author podalv
*
*/
public class Stride6ToAtlasDataSourceConvertor extends Stride6TestDataSource {

  private final AtlasTestDataSource source = new AtlasTestDataSource();

  private Date getDob() {
    final Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, 1950);
    cal.set(Calendar.MONTH, 0);
    cal.set(Calendar.DAY_OF_YEAR, 1);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    return cal.getTime();
  }

  private String formatDate(final Date date) {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
  }

  private void convertEncounters() {
    for (int x = 0; x < visits.size(); x++) {
      final MockVisitRecord rec = (MockVisitRecord) visits.get(x);
      final AtlasMockEncounters visit = AtlasMockEncounters.create(rec.getPatientId());
      double age = 0;
      if (rec.getAge() != null) {
        age = rec.getAge();
      }
      visit.start(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(age)));
      double duration = 0;
      if (rec.getDuration() != null) {
        duration = rec.getDuration();
      }
      visit.end(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(age + duration)));
      if (rec.getSab() != null && rec.getSab().equals("CPT")) {
        visit.codeType("CPT");
      }
      else if (rec.getSab() != null) {
        visit.codeType("ICD9");
      }
      visit.code(rec.getCode());
      visit.dept("");
      visit.visitType(rec.getSrcVisit());
      if (rec.getSourceCode() != null) {
        for (int y = 0; y < visitDx.size(); y++) {
          final MockVisitDxRecord recDx = (MockVisitDxRecord) visitDx.get(y);
          if (recDx.getDxId().toString().equals(rec.getSourceCode()) && recDx.isPrimary()) {
            visit.primary();
          }
        }
      }
      source.addEncounter(visit);
    }
  }

  private void convertDemographics() {
    for (int x = 0; x < demographics.size(); x++) {
      try {
        final MockDemographicsRecord rec = (MockDemographicsRecord) demographics.get(x);
        final AtlasMockDemographics demo = AtlasMockDemographics.create(rec.getId());
        demo.ethnicity(rec.getEthnicity()).gender(rec.getGender()).race(rec.getRace());
        demo.birth(formatDate(getDob()));
        if (rec.getDeath() != null) {
          demo.death(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getDeath())));
        }
        else {
          demo.death(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(0)));
        }
        source.addDemographics(demo);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertPrescriptions() {
    for (int x = 0; x < rx.size(); x++) {
      try {
        final MockRxRecord rec = (MockRxRecord) rx.get(x);
        final AtlasMockPrescriptions medRecord = AtlasMockPrescriptions.create(rec.getPatientId());
        medRecord.codeType(AtlasDatabaseDownload.CODE_TYPE_RX);
        medRecord.code(rec.getRxCui() == null ? null : rec.getRxCui().toString());
        medRecord.route(rec.getRoute());
        medRecord.status(rec.getStatus());
        medRecord.startTime(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge())));
        medRecord.endTime(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge())));
        source.addPrescription(medRecord);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertNotes() {
    for (int x = 0; x < notes.size(); x++) {
      try {
        final MockNoteRecord rec = (MockNoteRecord) notes.get(x);
        int pid = -1;
        for (int y = 0; y < terms.size(); y++) {
          final MockTermRecord termRec = (MockTermRecord) terms.get(y);
          if (termRec.getNid().equals(rec.getNoteId())) {
            pid = termRec.getPatientId();
            break;
          }
        }
        if (pid == -1) {
          continue;
        }
        final AtlasMockNotes noteRecord = AtlasMockNotes.create(pid, rec.getNoteId());
        if (rec.getAge() == null) {
          continue;
        }
        noteRecord.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge())));
        noteRecord.noteType(rec.getDocDescription());
        source.addNote(noteRecord);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertTermMentions() {
    for (int x = 0; x < terms.size(); x++) {
      try {
        final MockTermRecord rec = (MockTermRecord) terms.get(x);
        final AtlasMockTermMentions termRecord = AtlasMockTermMentions.create(rec.getPatientId(), rec.getNid());
        termRecord.tid(rec.getTid());
        termRecord.negated(rec.getNegated() ? 1 : 0);
        termRecord.familyHistory(rec.getFamilyHistory() ? 1 : 0);
        source.addTermMention(termRecord);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertTermDictionary() {
    for (int x = 0; x < termDictionary.size(); x++) {
      try {
        source.addTermDictionary(termDictionary.get(x));
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertMeasurements() {
    for (int x = 0; x < labsLpch.size(); x++) {
      final MockLabsLpchRecord rec = (MockLabsLpchRecord) labsLpch.get(x);
      try {
        final AtlasMockMeasurements measRecord = AtlasMockMeasurements.create(rec.getPatientId());
        measRecord.codeType(AtlasDatabaseDownload.CODE_TYPE_LABS);
        measRecord.code(rec.getCode());
        if (rec.getAge() == null) {
          continue;
        }
        measRecord.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge())));
        final double[] range = Common.parseDoubleValueRange(rec.getRange());
        measRecord.value(rec.getValue());
        source.addMeasurement(measRecord);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
    for (int x = 0; x < labsShc.size(); x++) {
      final MockLabsShcRecord rec = (MockLabsShcRecord) labsShc.get(x);
      try {
        final AtlasMockMeasurements measRecord = AtlasMockMeasurements.create(rec.getPatientId());
        measRecord.codeType(AtlasDatabaseDownload.CODE_TYPE_LABS);
        measRecord.code(rec.getBaseName());
        measRecord.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge())));
        final double[] range = new double[2];
        range[0] = rec.getLow() == null || rec.getLow().trim().isEmpty() ? Stride6DatabaseDownload.UNDEFINED : Double.parseDouble(rec.getLow());
        range[1] = rec.getHigh() == null || rec.getHigh().trim().isEmpty() ? Stride6DatabaseDownload.UNDEFINED : Double.parseDouble(rec.getHigh());
        measRecord.computedValue(rec.getNormalVal());
        measRecord.value(rec.getValue());
        source.addMeasurement(measRecord);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
    for (int x = 0; x < vitals.size(); x++) {
      final MockVitalsRecord rec = (MockVitalsRecord) vitals.get(x);
      final AtlasMockMeasurements measRecord = AtlasMockMeasurements.create(rec.getPatientId());
      measRecord.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
      measRecord.code(rec.getDetailDisplay());
      if (rec.getAge() == null) {
        measRecord.date(null);
      }
      else {
        measRecord.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge())));
      }
      measRecord.computedValue(null);
      measRecord.value(rec.getMeasValue());
      source.addMeasurement(measRecord);
    }
    for (int x = 0; x < vitalsVisits.size(); x++) {
      final MockVitalsVisitsRecord rec = (MockVitalsVisitsRecord) vitalsVisits.get(x);
      final AtlasMockMeasurements measRecord = AtlasMockMeasurements.create(rec.getPatientId());
      measRecord.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
      if (rec.getBmi() != null && rec.getBmi() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_BMI);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value(rec.getBmi());
        source.addMeasurement(meas);
      }
      if (rec.getBsa() != null && rec.getBsa() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_BSA);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value(rec.getBsa());
        source.addMeasurement(meas);
      }
      if (rec.getDiastolic() != null && rec.getDiastolic() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_BP_D);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value((double) rec.getDiastolic());
        source.addMeasurement(meas);
      }
      if (rec.getPulse() != null && rec.getPulse() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_PULSE);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value((double) rec.getPulse());
        source.addMeasurement(meas);
      }
      if (rec.getResp() != null && rec.getResp() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_RESP);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value((double) rec.getResp());
        source.addMeasurement(meas);
      }
      if (rec.getSystolic() != null && rec.getSystolic() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_BP_S);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value((double) rec.getSystolic());
        source.addMeasurement(meas);
      }
      if (rec.getTemp() != null && rec.getTemp() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_TEMPERATURE);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value(rec.getTemp());
        source.addMeasurement(meas);
      }
      if (rec.getWeight() != null && rec.getWeight() > 0) {
        final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
        meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
        meas.code(Common.VITALS_WEIGHT);
        meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
        meas.value(rec.getWeight());
        source.addMeasurement(meas);
      }
      if (rec.getHeight() != null) {
        final double inches = Common.parseHeightToInchesSilent(rec.getHeight());
        if (inches > 0) {
          final AtlasMockMeasurements meas = AtlasMockMeasurements.create(rec.getPatientId());
          meas.date(formatDate(Stride6ToOhdsiDataSourceConvertor.daysToDate(rec.getAge() == null ? 0 : rec.getAge())));
          meas.code(Common.VITALS_HEIGHT);
          meas.codeType(AtlasDatabaseDownload.CODE_TYPE_VITALS);
          meas.value(inches);
          source.addMeasurement(meas);
        }
      }
    }
  }

  @Override
  public Connection generate() {
    convertDemographics();
    convertEncounters();
    convertPrescriptions();
    convertMeasurements();
    convertNotes();
    convertTermMentions();
    convertTermDictionary();
    return source.generate();
  }

}