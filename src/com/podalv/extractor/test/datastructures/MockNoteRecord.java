package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockNoteRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"note_id", "doc_description", "age_at_note_date_in_days", "note_year"};

  private final Integer        noteId;
  private String               docDescription;
  private Double               age;
  private Integer              year;

  public static MockNoteRecord create(final Integer noteId) {
    return new MockNoteRecord(noteId);
  }

  public Double getAge() {
    return age;
  }

  public Integer getNoteId() {
    return noteId;
  }

  public Integer getYear() {
    return year;
  }

  public String getDocDescription() {
    return docDescription;
  }

  private MockNoteRecord(final Integer noteId) {
    this.noteId = noteId;
  }

  public MockNoteRecord docDescription(final String description) {
    this.docDescription = description;
    return this;
  }

  public MockNoteRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockNoteRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(noteId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public String[] toTableRow() {
    return new String[] {noteId == null ? null : noteId + "", docDescription, age == null ? null : age + "", year == null ? null : year + ""};
  }

  @Override
  public Stride6MockRecord copy() {
    final MockNoteRecord result = new MockNoteRecord(noteId);
    result.age = age;
    result.docDescription = docDescription;
    result.year = year;
    return result;
  }
}