package com.podalv.extractor.test.datastructures;

//pid, nid, date, note_type
public class AtlasMockNotes extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"pid", "nid", "date", "note_type"};

  private final Integer        noteId;
  private final Integer        pid;
  private String               date;
  private String               noteType;

  public static AtlasMockNotes create(final Integer pid, final Integer noteId) {
    return new AtlasMockNotes(pid, noteId);
  }

  public Integer getNoteId() {
    return noteId;
  }

  private AtlasMockNotes(final Integer pid, final Integer noteId) {
    this.pid = pid;
    this.noteId = noteId;
  }

  public AtlasMockNotes noteType(final String noteType) {
    this.noteType = noteType;
    return this;
  }

  public AtlasMockNotes date(final String date) {
    this.date = date;
    return this;
  }

  @Override
  public long comparable() {
    return pid;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {pid == null ? null : pid + "", noteId == null ? null : noteId + "", date, noteType};
  }

  @Override
  public Stride6MockRecord copy() {
    final AtlasMockNotes result = new AtlasMockNotes(pid, noteId);
    result.date = date;
    result.noteType = noteType;
    return result;
  }
}