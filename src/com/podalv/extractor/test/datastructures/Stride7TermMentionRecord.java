package com.podalv.extractor.test.datastructures;

public class Stride7TermMentionRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES  = new String[] {"nid", "tid", "negated", "family_history"};
  private Integer              noteId        = null;
  private Integer              termId        = null;
  private Integer              negated       = null;
  private Integer              familyHistory = null;

  public static Stride7TermMentionRecord create() {
    return new Stride7TermMentionRecord();
  }

  public Stride7TermMentionRecord termId(final Integer termId) {
    this.termId = termId;
    return this;
  }

  public Stride7TermMentionRecord negated(final Integer negated) {
    this.negated = negated;
    return this;
  }

  public Stride7TermMentionRecord familyHistory(final Integer familyHistory) {
    this.familyHistory = familyHistory;
    return this;
  }

  public Stride7TermMentionRecord noteId(final Integer noteId) {
    this.noteId = noteId;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {noteId == null ? null : noteId + "", termId == null ? null : termId + "", negated == null ? null : negated + "", familyHistory == null ? null
        : familyHistory + ""};
  }

  @Override
  public long comparable() {
    return 1;
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7TermMentionRecord result = new Stride7TermMentionRecord();
    result.noteId(noteId);
    result.termId(termId);
    result.negated(negated);
    result.familyHistory(familyHistory);
    return result;
  }

}