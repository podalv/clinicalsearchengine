package com.podalv.extractor.test.datastructures;

public class AtlasMockEncounters extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"pid", "code_type", "code", "visit_type", "department", "primary_diagnosis", "start_time", "end_time"};
  private final int            id;
  private String               codeType     = "";
  private String               code         = "";
  private String               visitType    = "";
  private String               department   = null;
  private int                  primary      = 0;
  private String               start        = null;
  private String               end          = null;

  public static AtlasMockEncounters create(final int id) {
    return new AtlasMockEncounters(id);
  }

  private AtlasMockEncounters(final int id) {
    this.id = id;
  }

  public AtlasMockEncounters codeType(final String codeType) {
    this.codeType = codeType;
    return this;
  }

  public AtlasMockEncounters code(final String code) {
    this.code = code;
    return this;
  }

  public AtlasMockEncounters visitType(final String visitType) {
    this.visitType = visitType;
    return this;
  }

  public AtlasMockEncounters dept(final String dept) {
    this.department = dept;
    return this;
  }

  public AtlasMockEncounters start(final String start) {
    this.start = start;
    return this;
  }

  public AtlasMockEncounters end(final String end) {
    this.end = end;
    return this;
  }

  public AtlasMockEncounters primary() {
    this.primary = 1;
    return this;
  }

  public String getCode() {
    return code;
  }

  public String getCodeType() {
    return codeType;
  }

  public String getDepartment() {
    return department;
  }

  public String getEnd() {
    return end;
  }

  public int getId() {
    return id;
  }

  public int getPrimary() {
    return primary;
  }

  public String getStart() {
    return start;
  }

  public String getVisitType() {
    return visitType;
  }

  @Override
  public long comparable() {
    return id;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", codeType, code, visitType, department, primary + "", start, end};
  }

  @Override
  public Stride6MockRecord copy() {
    final AtlasMockEncounters result = new AtlasMockEncounters(id);
    result.code = code;
    result.codeType = codeType;
    result.department = department;
    result.end = end;
    result.primary = primary;
    result.start = start;
    result.visitType = visitType;
    return result;
  }
}