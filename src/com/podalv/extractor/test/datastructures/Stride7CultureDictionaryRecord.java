package com.podalv.extractor.test.datastructures;

public class Stride7CultureDictionaryRecord extends Stride6MockRecord {

  public static String[] COLUMN_NAMES = new String[] {"cmt_result", "mapped_name", "flag"};

  private final String   cmtResult;
  private final String   mappedName;
  private final String   flag;

  public Stride7CultureDictionaryRecord(final String cmtResult, final String mappedName, final String flag) {
    this.cmtResult = cmtResult;
    this.mappedName = mappedName;
    this.flag = flag;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {cmtResult, mappedName, flag};
  }

  @Override
  public long comparable() {
    return 0;
  }

  @Override
  public Stride6MockRecord copy() {
    return new Stride7CultureDictionaryRecord(cmtResult, mappedName, flag);
  }

}
