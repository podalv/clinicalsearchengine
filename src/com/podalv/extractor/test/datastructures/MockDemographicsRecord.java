package com.podalv.extractor.test.datastructures;

/** A single patient's demographics record
 *
 * @author podalv
 *
 */
public class MockDemographicsRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "gender", "race", "ethnicity", "age_at_death_in_days"};
  private final int            id;
  private String               race         = "";
  private String               gender       = "";
  private String               ethnicity    = "";
  private Double               death        = null;

  public static MockDemographicsRecord create(final int id) {
    return new MockDemographicsRecord(id);
  }

  public MockDemographicsRecord race(final String race) {
    this.race = race;
    return this;
  }

  public MockDemographicsRecord gender(final String gender) {
    this.gender = gender;
    return this;
  }

  public MockDemographicsRecord ethnicity(final String ethnicity) {
    this.ethnicity = ethnicity;
    return this;
  }

  public MockDemographicsRecord death(final Double death) {
    this.death = death;
    return this;
  }

  public int getId() {
    return id;
  }

  public String getEthnicity() {
    return ethnicity;
  }

  public Double getDeath() {
    return death;
  }

  public String getGender() {
    return gender;
  }

  public String getRace() {
    return race;
  }

  private MockDemographicsRecord(final int id) {
    this.id = id;
  }

  @Override
  public long comparable() {
    return id;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", gender, race, ethnicity, death == null ? null : death + ""};
  }

  @Override
  public Stride6MockRecord copy() {
    final MockDemographicsRecord result = new MockDemographicsRecord(id);
    result.death = death;
    result.ethnicity = ethnicity;
    result.gender = gender;
    result.race = race;
    return result;
  }

}