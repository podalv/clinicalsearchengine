package com.podalv.extractor.test.datastructures;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestCondition;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestDemographics;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestDrugs;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestMeasurement;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestNotes;
import com.podalv.extractor.test.datastructures.tables.OhdsiTestProcedures;

/** Converts Stride6TestDataSource to Ohdsi Connection
 *
 * @author podalv
 *
 */
public class Stride6ToOhdsiDataSourceConvertor extends Stride6TestDataSource {

  private final OhdsiTestDataSource source = new OhdsiTestDataSource();
  private static final int[]        DOB    = new int[] {1950, 01, 01};

  public static int[] daysToYearMonthDay(final double date) {
    final Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, 1950);
    cal.set(Calendar.MONTH, 0);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.MINUTE, Common.daysToTime(date));
    return new int[] {cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH)};
  }

  public static String[] daysToDayTime(final double date) {
    final Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, 1950);
    cal.set(Calendar.MONTH, 0);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.MINUTE, Common.daysToTime(date));
    return new String[] {new SimpleDateFormat(Common.OHDSI_DATE_FORMAT).format(cal.getTime()), new SimpleDateFormat("HH:mm:ss").format(cal.getTime())};
  }

  public static Date daysToDate(final double date) {
    final Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, 1950);
    cal.set(Calendar.MONTH, 0);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.MINUTE, Common.daysToTime(date));
    return cal.getTime();
  }

  private void convertDemographics() {
    for (int x = 0; x < demographics.size(); x++) {
      try {
        final MockDemographicsRecord rec = (MockDemographicsRecord) demographics.get(x);
        final OhdsiTestDemographics demo = OhdsiTestDemographics.add(rec.getId());
        demo.ethnicity(rec.getEthnicity()).gender(rec.getGender()).race(rec.getRace()).dob(DOB[0], DOB[1], DOB[2]);
        if (rec.getDeath() != null) {
          demo.death(daysToYearMonthDay(rec.getDeath())[0], daysToYearMonthDay(rec.getDeath())[1], daysToYearMonthDay(rec.getDeath())[2]);
        }
        source.add(demo);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertDrugs() {
    for (int x = 0; x < rx.size(); x++) {
      try {
        final MockRxRecord rec = (MockRxRecord) rx.get(x);
        final OhdsiTestDrugs drugs = OhdsiTestDrugs.add(rec.getPatientId());
        drugs.drugConceptId(source.addRxNorm(rec.getRxCui() + "", "aaa"));
        drugs.drugTypeConceptId(source.addConcept(rec.getStatus()));
        drugs.routeConceptId(source.addConcept(rec.getRoute()));
        drugs.startTime(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
        drugs.endTime(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
        source.add(drugs);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertNotes() {
    final ArrayList<OhdsiTestNotes> resultNotes = new ArrayList<OhdsiTestNotes>();
    for (int x = 0; x < terms.size(); x++) {
      try {
        final MockTermRecord rec = (MockTermRecord) terms.get(x);
        OhdsiTestNotes note = null;
        for (int y = 0; y < resultNotes.size(); y++) {
          if (resultNotes.get(y).getPatientId() == rec.getPatientId() && resultNotes.get(y).getNoteId() == rec.getNid()) {
            note = resultNotes.get(y);
            break;
          }
        }
        if (note == null) {
          note = OhdsiTestNotes.add(rec.getPatientId());
          for (int y = 0; y < notes.size(); y++) {
            final MockNoteRecord n = (MockNoteRecord) notes.get(y);
            if (n.getNoteId().intValue() == rec.getNid().intValue()) {
              note.date(daysToDayTime(n.getAge())[0], daysToDayTime(n.getAge())[1]);
              note.noteType(source.addConcept(n.getDocDescription()));
              note.noteId(n.getNoteId());
              break;
            }
          }
        }
        String term = null;
        for (int y = 0; y < termDictionary.size(); y++) {
          final MockTermDictionaryRecord dict = (MockTermDictionaryRecord) termDictionary.get(y);
          if (dict.getTid() == rec.getTid()) {
            term = dict.getStr();
            break;
          }
        }
        if (rec.getFamilyHistory()) {
          note.familyHistoryTerms(new String[] {term});
        }
        if (rec.getNegated()) {
          note.negatedTerms(new String[] {term});
        }
        if (!rec.getFamilyHistory() && !rec.getNegated()) {
          note.positiveTerms(new String[] {term});
        }
        source.add(note);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertVisits() {
    for (int x = 0; x < visits.size(); x++) {
      final MockVisitRecord rec = (MockVisitRecord) visits.get(x);
      try {
        if (rec.getSab().equals(MockVisitRecord.SAB_CPT)) {
          final OhdsiTestProcedures proc = OhdsiTestProcedures.add(rec.getPatientId());
          proc.procedureConceptId(source.addCpt(rec.getCode(), "aaa"));
          proc.visitConceptId(source.addConcept(rec.getSrcVisit()));
          proc.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          source.add(proc);
        }
        else {
          final OhdsiTestCondition cond = OhdsiTestCondition.add(rec.getPatientId());
          if (rec.getSab().equals(MockVisitRecord.SAB_ICD)) {
            cond.conditionConceptId(source.addIcd9(rec.getCode(), "aaa"));
          }
          else {
            cond.conditionConceptId(source.addSnomed(rec.getCode(), "aaa"));
          }
          cond.visitConceptId(source.addConcept(rec.getSrcVisit()));
          cond.startTime(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          cond.endTime(daysToDayTime(rec.getAge() + rec.getDuration())[0], daysToDayTime(rec.getAge() + rec.getDuration())[1]);
          for (int y = 0; y < visitDx.size(); y++) {
            final MockVisitDxRecord dx = (MockVisitDxRecord) visitDx.get(y);
            if (dx.getVisitId().intValue() == rec.getVisitId()) {
              cond.primaryDiagnosis();
            }
          }
          source.add(cond);
        }
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertLabs() {
    for (int x = 0; x < labsShc.size(); x++) {
      try {
        final MockLabsShcRecord rec = (MockLabsShcRecord) labsShc.get(x);
        final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
        meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
        meas.measurementConceptId(source.addConcept(rec.getBaseName()));
        meas.vitalsValue(rec.getValue());
        source.add(meas);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
    for (int x = 0; x < labsLpch.size(); x++) {
      try {
        final MockLabsLpchRecord rec = (MockLabsLpchRecord) labsLpch.get(x);
        final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
        meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
        meas.measurementConceptId(source.addConcept(rec.getCode()));
        meas.vitalsValue(rec.getValue());
        source.add(meas);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void convertVitals() {
    for (int x = 0; x < vitals.size(); x++) {
      try {
        final MockVitalsRecord rec = (MockVitalsRecord) vitals.get(x);
        final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
        meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
        meas.vitalsValue(rec.getMeasValue().doubleValue());
        meas.measurementConceptId(source.addConcept(rec.getDetailDisplay()));
        source.add(meas);
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
    for (int x = 0; x < vitalsVisits.size(); x++) {
      try {
        final MockVitalsVisitsRecord rec = (MockVitalsVisitsRecord) vitalsVisits.get(x);
        if (rec.getBmi() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getBmi());
          meas.measurementConceptId(source.addConcept(Common.VITALS_BMI));
          source.add(meas);
        }
        if (rec.getBsa() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getBsa());
          meas.measurementConceptId(source.addConcept(Common.VITALS_BSA));
          source.add(meas);
        }
        if (rec.getDiastolic() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getDiastolic());
          meas.measurementConceptId(source.addConcept(Common.VITALS_BP_D));
          source.add(meas);
        }
        if (rec.getPulse() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getPulse());
          meas.measurementConceptId(source.addConcept(Common.VITALS_PULSE));
          source.add(meas);
        }
        if (rec.getResp() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getResp());
          meas.measurementConceptId(source.addConcept(Common.VITALS_RESP));
          source.add(meas);
        }
        if (rec.getSystolic() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getSystolic());
          meas.measurementConceptId(source.addConcept(Common.VITALS_BP_S));
          source.add(meas);
        }
        if (rec.getTemp() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getTemp());
          meas.measurementConceptId(source.addConcept(Common.VITALS_TEMPERATURE));
          source.add(meas);
        }
        if (rec.getWeight() > 0) {
          final OhdsiTestMeasurement meas = OhdsiTestMeasurement.add(rec.getPatientId());
          meas.date(daysToDayTime(rec.getAge())[0], daysToDayTime(rec.getAge())[1]);
          meas.vitalsValue(rec.getWeight());
          meas.measurementConceptId(source.addConcept(Common.VITALS_WEIGHT));
          source.add(meas);
        }
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public Connection generate() {
    convertDemographics();
    convertDrugs();
    convertNotes();
    convertVisits();
    convertLabs();
    convertVitals();
    return source.generate();
  }

}
