package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockRxRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "age_at_start_time_in_days", "start_time_year", "order_status", "rxcui", "age_at_start_time_in_days",
      "route"                               };

  private final int            patientId;
  private Double               age;
  private Integer              year;
  private String               status;
  private Integer              rxCui;
  private String               route;

  public static MockRxRecord create(final int patientId) {
    return new MockRxRecord(patientId);
  }

  private MockRxRecord(final int patientId) {
    this.patientId = patientId;
  }

  public int getPatientId() {
    return patientId;
  }

  public Integer getRxCui() {
    return rxCui;
  }

  public Double getAge() {
    return age;
  }

  public String getRoute() {
    return route;
  }

  public String getStatus() {
    return status;
  }

  public Integer getYear() {
    return year;
  }

  public MockRxRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockRxRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public MockRxRecord status(final String status) {
    this.status = status;
    return this;
  }

  public MockRxRecord route(final String route) {
    this.route = route;
    return this;
  }

  public MockRxRecord rxCui(final Integer rxCui) {
    this.rxCui = rxCui;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : age + "", year == null ? null : year + "", status, rxCui == null ? null : rxCui + "", age == null ? null : age + "",
        route};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockRxRecord result = new MockRxRecord(patientId);
    result.age = age;
    result.route = route;
    result.rxCui = rxCui;
    result.status = status;
    result.year = year;
    return result;
  }
}
