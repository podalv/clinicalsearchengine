package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7DepartmentRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "department_id", "age_at_contact_in_days", "YEAR(contact_date)"};
  private final int            id;
  private Integer              departmentId = null;
  private Double               age          = null;
  private Integer              year         = null;

  public static Stride7DepartmentRecord create(final int patientId) {
    return new Stride7DepartmentRecord(patientId);
  }

  private Stride7DepartmentRecord(final int patientId) {
    this.id = patientId;
  }

  public Stride7DepartmentRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Integer getYear() {
    return year;
  }

  public int getId() {
    return id;
  }

  public Stride7DepartmentRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Double getAge() {
    return age;
  }

  public Integer getDepartmentId() {
    return departmentId;
  }

  public Stride7DepartmentRecord departmentId(final Integer departmentId) {
    this.departmentId = departmentId;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", departmentId == null ? null : departmentId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + ""};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(id * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7DepartmentRecord result = new Stride7DepartmentRecord(id);
    result.year(year);
    result.age(age);
    result.departmentId(departmentId);
    return result;
  }

}
