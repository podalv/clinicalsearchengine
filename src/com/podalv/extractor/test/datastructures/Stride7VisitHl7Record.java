package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7VisitHl7Record extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "CEILING(age_at_contact_in_days)", "CEILING(visit_duration)", "code", "visit_type", "dx_px_type_text",
      "YEAR(contact_date)"};
  private final int            patientId;
  private Double               ageStart     = null;
  private Double               duration     = null;
  private Integer              year         = null;
  private String               code         = null;
  private String               visitType    = null;
  private String               primaryType  = null;

  public static Stride7VisitHl7Record create(final int patientId) {
    return new Stride7VisitHl7Record(patientId);
  }

  public Stride7VisitHl7Record age(final Double ageStart, final Double ageEnd) {
    this.ageStart = ageStart;
    if (ageEnd != null && ageStart != null) {
      this.duration = ageEnd - ageStart;
    }
    else {
      this.duration = 0d;
    }
    return this;
  }

  public Stride7VisitHl7Record year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7VisitHl7Record code(final String code) {
    this.code = code;
    return this;
  }

  public Stride7VisitHl7Record visitType(final String visitType) {
    this.visitType = visitType;
    return this;
  }

  public Stride7VisitHl7Record primaryType(final String primaryType) {
    this.primaryType = primaryType;
    return this;
  }

  private Stride7VisitHl7Record(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", ageStart == null ? null : Math.floor(ageStart) + "", duration == null ? null : Math.floor(duration) + "", code, visitType, primaryType,
        year == null ? null : year + ""};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(ageStart == null ? 0 : (int) (ageStart * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7VisitHl7Record result = new Stride7VisitHl7Record(patientId);
    result.age(ageStart, duration);
    result.year(year);
    result.code(code);
    result.visitType(visitType);
    result.primaryType(primaryType);
    return result;
  }

}