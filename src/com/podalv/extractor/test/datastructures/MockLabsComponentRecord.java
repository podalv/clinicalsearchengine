package com.podalv.extractor.test.datastructures;

public class MockLabsComponentRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"base_name", "common_name"};

  private final String         baseName;
  private final String         componentName;

  public static MockLabsComponentRecord create(final String baseName, final String componentName) {
    return new MockLabsComponentRecord(baseName, componentName);
  }

  public MockLabsComponentRecord(final String baseName, final String componentName) {
    this.baseName = baseName;
    this.componentName = componentName;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {baseName, componentName};
  }

  @Override
  public long comparable() {
    return 0;
  }

  @Override
  public Stride6MockRecord copy() {
    return new MockLabsComponentRecord(baseName, componentName);
  }
}
