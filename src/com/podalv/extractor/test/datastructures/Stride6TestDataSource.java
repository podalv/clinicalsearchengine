package com.podalv.extractor.test.datastructures;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;

import com.podalv.db.test.MockConnection;
import com.podalv.db.test.MockDataTable;
import com.podalv.extractor.stride6.Commands;

/** Contains MockDataTables that form Stride6 extraction source
 *
 * @author podalv
 *
 */
public class Stride6TestDataSource {

  final ArrayList<Stride6MockRecord> demographics   = new ArrayList<>();
  final ArrayList<Stride6MockRecord> visits         = new ArrayList<>();
  final ArrayList<Stride6MockRecord> rx             = new ArrayList<>();
  final ArrayList<Stride6MockRecord> visitDx        = new ArrayList<>();
  final ArrayList<Stride6MockRecord> notes          = new ArrayList<>();
  final ArrayList<Stride6MockRecord> termDictionary = new ArrayList<>();
  final ArrayList<Stride6MockRecord> terms          = new ArrayList<>();
  final ArrayList<Stride6MockRecord> labsShc        = new ArrayList<>();
  final ArrayList<Stride6MockRecord> labsLpch       = new ArrayList<>();
  final ArrayList<Stride6MockRecord> labsComponent  = new ArrayList<>();
  final ArrayList<Stride6MockRecord> vitals         = new ArrayList<>();
  final ArrayList<Stride6MockRecord> vitalsVisits   = new ArrayList<>();

  public void addDemographics(final MockDemographicsRecord record) {
    demographics.add(record);
  }

  public void addVitals(final MockVitalsRecord record) {
    vitals.add(record);
  }

  public void addVitals(final MockVitalsVisitsRecord record) {
    vitalsVisits.add(record);
  }

  public void addNote(final MockNoteRecord record) {
    notes.add(record);
  }

  public void addTermMention(final MockTermRecord record) {
    terms.add(record);
  }

  public void addTermDictionaryRecord(final MockTermDictionaryRecord record) {
    termDictionary.add(record);
  }

  public void addLabsComponent(final MockLabsComponentRecord record) {
    labsComponent.add(record);
  }

  public void addVisit(final MockVisitRecord record) {
    visits.add(record);
  }

  public void addLabsShc(final MockLabsShcRecord record) {
    labsShc.add(record);
  }

  public void addLabsLpch(final MockLabsLpchRecord record) {
    labsLpch.add(record);
  }

  public void addVisitDx(final MockVisitDxRecord record) {
    visitDx.add(record);
  }

  public void addRx(final MockRxRecord record) {
    rx.add(record);
  }

  public int getPatientCnt() {
    return demographics.size();
  }

  static MockDataTable getTable(final String[] columns, final ArrayList<Stride6MockRecord> records) {
    final MockDataTable result = new MockDataTable(MockRxRecord.COLUMN_NAMES);
    Collections.sort(records);
    for (final Stride6MockRecord record : records) {
      result.addRow(record.toTableRow());
    }
    return result;
  }

  private MockDataTable getTerminologyVersion() {
    final MockDataTable result = new MockDataTable("result");
    result.addRow("str2tid");
    return result;
  }

  private MockDataTable getPatientCount() {
    final MockDataTable result = new MockDataTable("result");
    result.addRow(demographics.size() + "");
    return result;
  }

  private ArrayList<Stride6MockRecord> copy(final ArrayList<Stride6MockRecord> src) {
    final ArrayList<Stride6MockRecord> result = new ArrayList<>();
    for (final Stride6MockRecord rec : src) {
      result.add(rec.copy());
    }
    return result;
  }

  private void sortAll() {
    Collections.sort(demographics);
    Collections.sort(labsComponent);
    Collections.sort(labsLpch);
    Collections.sort(labsShc);
    Collections.sort(notes);
    Collections.sort(rx);
    Collections.sort(termDictionary);
    Collections.sort(terms);
    Collections.sort(visitDx);
    Collections.sort(visits);
    Collections.sort(vitals);
    Collections.sort(vitalsVisits);
  }

  public Stride6TestDataSource toOhdsi() {
    sortAll();
    final Stride6ToOhdsiDataSourceConvertor result = new Stride6ToOhdsiDataSourceConvertor();
    result.demographics.addAll(copy(demographics));
    result.labsComponent.addAll(copy(labsComponent));
    result.labsLpch.addAll(copy(labsLpch));
    result.labsShc.addAll(copy(labsShc));
    result.notes.addAll(copy(notes));
    result.rx.addAll(copy(rx));
    result.termDictionary.addAll(copy(termDictionary));
    result.terms.addAll(copy(terms));
    result.visitDx.addAll(copy(visitDx));
    result.visits.addAll(copy(visits));
    result.vitals.addAll(copy(vitals));
    result.vitalsVisits.addAll(copy(vitalsVisits));
    return result;
  }

  public Stride6TestDataSource toAtlasSchema() {
    sortAll();
    final Stride6ToAtlasDataSourceConvertor result = new Stride6ToAtlasDataSourceConvertor();
    result.demographics.addAll(copy(demographics));
    result.labsComponent.addAll(copy(labsComponent));
    result.labsLpch.addAll(copy(labsLpch));
    result.labsShc.addAll(copy(labsShc));
    result.notes.addAll(copy(notes));
    result.rx.addAll(copy(rx));
    result.termDictionary.addAll(copy(termDictionary));
    result.terms.addAll(copy(terms));
    result.visitDx.addAll(copy(visitDx));
    result.visits.addAll(copy(visits));
    result.vitals.addAll(copy(vitals));
    result.vitalsVisits.addAll(copy(vitalsVisits));
    return result;
  }

  public Connection generate() {
    sortAll();
    final MockConnection result = new MockConnection();

    result.addResultSet(Commands.DEMOGRAPHICS, getTable(MockDemographicsRecord.COLUMN_NAMES, demographics));
    result.addResultSet(Commands.PATIENT_COUNT, getPatientCount());
    result.addResultSet(Commands.TERM_DICTIONARY_TABLE, getTerminologyVersion());
    result.addResultSet(Commands.VISITS, getTable(MockVisitRecord.COLUMN_NAMES, visits));
    result.addResultSet(Commands.MEDS, getTable(MockRxRecord.COLUMN_NAMES, rx));
    result.addResultSet(Commands.VISIT_DX, getTable(MockVisitDxRecord.COLUMN_NAMES, visitDx));
    result.addResultSet(Commands.NOTES, getTable(MockNoteRecord.COLUMN_NAMES, notes));
    result.addResultSet(Commands.LABS_SHC, getTable(MockLabsShcRecord.COLUMN_NAMES, labsShc));
    result.addResultSet(Commands.LABS_LPCH, getTable(MockLabsLpchRecord.COLUMN_NAMES, labsLpch));
    result.addResultSet(Commands.LABS_COMPONENT, getTable(MockLabsComponentRecord.COLUMN_NAMES, labsComponent));
    result.addResultSet(Commands.getTerminologyQuery(Commands.TERM_DICTIONARY, "str2tid"), getTable(MockTermDictionaryRecord.COLUMN_NAMES, termDictionary));
    result.addResultSet(Commands.getTermsQuery(Commands.TERMS, 0, Commands.STRIDE_6_TERM_PATIENT_ID), getTable(MockTermRecord.COLUMN_NAMES, terms));
    result.addResultSet(Commands.VITALS, getTable(MockVitalsRecord.COLUMN_NAMES, vitals));
    result.addResultSet(Commands.VITALS_VISITS, getTable(MockVitalsVisitsRecord.COLUMN_NAMES, vitalsVisits));
    return result;
  }
}
