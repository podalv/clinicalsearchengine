package com.podalv.extractor.test.datastructures;

public class AtlasMockTermMentions extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"pid", "nid", "tid", "negated", "family_history"};

  private final Integer        noteId;
  private final Integer        pid;
  private Integer              tid;
  private Integer              negated;
  private Integer              familyHistory;

  public static AtlasMockTermMentions create(final Integer pid, final Integer noteId) {
    return new AtlasMockTermMentions(pid, noteId);
  }

  public Integer getNoteId() {
    return noteId;
  }

  private AtlasMockTermMentions(final Integer pid, final Integer noteId) {
    this.pid = pid;
    this.noteId = noteId;
  }

  public AtlasMockTermMentions tid(final Integer tid) {
    this.tid = tid;
    return this;
  }

  public AtlasMockTermMentions negated(final Integer val) {
    this.negated = val;
    return this;
  }

  public AtlasMockTermMentions familyHistory(final Integer val) {
    this.familyHistory = val;
    return this;
  }

  @Override
  public long comparable() {
    return pid;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {pid == null ? null : pid + "", noteId == null ? null : noteId + "", tid == null ? null : tid + "", negated == null ? null : negated + "",
        familyHistory == null ? null : familyHistory + ""};
  }

  @Override
  public Stride6MockRecord copy() {
    final AtlasMockTermMentions result = new AtlasMockTermMentions(pid, noteId);
    result.familyHistory = familyHistory;
    result.negated = negated;
    result.tid = tid;
    return result;
  }
}