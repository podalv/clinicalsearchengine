package com.podalv.extractor.test.datastructures;

public class Stride7VitalsRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "CEILING(age_at_contact_in_days)", "bp_systolic", "bp_diastolic", "temperature", "pulse", "weight_in_lbs",
      "height", "respirations", "bmi", "bsa", "YEAR(contact_date)"};

  private final int            patientId;
  private Double               age          = 0d;
  private Integer              systolic     = 0;
  private Integer              diastolic    = 0;
  private Double               temp         = 0d;
  private Integer              pulse        = 0;
  private Double               weight       = 0d;
  private String               height       = "";
  private Integer              resp         = 0;
  private Double               bmi          = 0d;
  private Double               bsa          = 0d;
  private Integer              year         = 0;

  public static Stride7VitalsRecord create(final int patientId) {
    return new Stride7VitalsRecord(patientId);
  }

  public static Stride7VitalsRecord createNulls(final int patientId) {
    final Stride7VitalsRecord result = new Stride7VitalsRecord(patientId);
    result.age = null;
    result.systolic = null;
    result.diastolic = null;
    result.temp = null;
    result.pulse = null;
    result.weight = null;
    result.height = null;
    result.resp = null;
    result.bmi = null;
    result.bsa = null;
    result.year = null;
    return result;
  }

  private Stride7VitalsRecord(final int patientId) {
    this.patientId = patientId;
  }

  public Stride7VitalsRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Integer getYear() {
    return year;
  }

  public Stride7VitalsRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7VitalsRecord bp(final Integer systolic, final Integer diastolic) {
    this.systolic = systolic;
    this.diastolic = diastolic;
    return this;
  }

  public Stride7VitalsRecord temp(final Double temp) {
    this.temp = temp;
    return this;
  }

  public Stride7VitalsRecord pulse(final Integer pulse) {
    this.pulse = pulse;
    return this;
  }

  public Stride7VitalsRecord height(final String height) {
    this.height = height;
    return this;
  }

  public Stride7VitalsRecord weight(final Double weight) {
    this.weight = weight;
    return this;
  }

  public Stride7VitalsRecord resp(final Integer resp) {
    this.resp = resp;
    return this;
  }

  public Stride7VitalsRecord bmi(final Double bmi) {
    this.bmi = bmi;
    return this;
  }

  public Stride7VitalsRecord bsa(final Double bsa) {
    this.bsa = bsa;
    return this;
  }

  public Double getBmi() {
    return bmi;
  }

  public Double getBsa() {
    return bsa;
  }

  public Integer getDiastolic() {
    return diastolic;
  }

  public String getHeight() {
    return height;
  }

  public Integer getPulse() {
    return pulse;
  }

  public Integer getResp() {
    return resp;
  }

  public Integer getSystolic() {
    return systolic;
  }

  public Double getTemp() {
    return temp;
  }

  public Double getWeight() {
    return weight;
  }

  public int getPatientId() {
    return patientId;
  }

  public Double getAge() {
    return age;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", systolic == null ? null : systolic + "", diastolic == null ? null : diastolic + "", temp == null
        ? null
        : temp + "", pulse == null ? null : pulse + "", weight == null ? null : weight + "", height, resp == null ? null : resp + "", bmi == null ? null : bmi + "", bsa == null
            ? null
            : bsa + "", year == null ? null : year + ""};
  }

  @Override
  public long comparable() {
    return patientId;
  }

  @Override
  public Stride7VitalsRecord copy() {
    final Stride7VitalsRecord result = new Stride7VitalsRecord(patientId);
    result.age = age;
    result.bmi = bmi;
    result.bsa = bsa;
    result.diastolic = diastolic;
    result.height = height;
    result.pulse = pulse;
    result.resp = resp;
    result.systolic = systolic;
    result.temp = temp;
    result.weight = weight;
    result.year = year;
    return result;
  }
}
