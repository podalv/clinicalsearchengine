package com.podalv.extractor.test.datastructures;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import com.podalv.db.test.MockConnection;
import com.podalv.db.test.MockDataTable;
import com.podalv.extractor.stride6.Commands;
import com.podalv.extractor.stride6.OhdsiDatabaseDownload;
import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.extractor.test.datastructures.tables.TestConstruct;
import com.podalv.extractor.test.datastructures.tables.TestTable;
import com.podalv.extractor.test.datastructures.tables.TestTableCollection;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.string.StringKeyIntMap;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;
import com.podalv.utils.datastructures.SortPairSingle;

/** An aggregation of all of patient's tables in one place
 *
 * @author podalv
 *
 */
public class OhdsiTestDataSource {

  private final StringKeyIntMap                                                    icd9ToConceptId                  = new StringKeyIntMap();
  private final StringKeyIntMap                                                    rxNormToConceptId                = new StringKeyIntMap();
  private final StringKeyIntMap                                                    cptToConceptId                   = new StringKeyIntMap();
  private final StringKeyIntMap                                                    atcToConceptId                   = new StringKeyIntMap();
  private final StringKeyIntMap                                                    snomedToConceptId                = new StringKeyIntMap();
  private final TestIndexCollection                                                dictionary                       = new TestIndexCollection();
  private final MockDataTable                                                      maxPatientId                     = new MockDataTable("max");
  private final MockDataTable                                                      person                           = new MockDataTable("person_id", "gender_concept_id",
                                                                                                                        "year_of_birth", "month_of_birth", "day_of_birth",
                                                                                                                        "time_of_birth", "race_concept_id", "ethnicity_concept_id");
  private final MockDataTable                                                      death                            = new MockDataTable("person_id", "death_date");
  private final MockDataTable                                                      procedure_occurrence_join_visits = new MockDataTable("person_id", "visit_concept_id",
                                                                                                                        "procedure_concept_id", "procedure_date", "procedure_time");
  private final MockDataTable                                                      note                             = new MockDataTable("note_id", "note_type_concept_id");
  private final MockDataTable                                                      observation                      = new MockDataTable("person_id", "observation_concept_id",
                                                                                                                        "observation_date", "observation_time");
  private final MockDataTable                                                      atlas_term_mentions_join_notes   = new MockDataTable("person_id", "note_id", "term_id",
                                                                                                                        "negated", "family_history", "note_date", "note_time");

  private final MockDataTable                                                      condition_occurrence_join_visits = new MockDataTable("person_id", "visit_concept_id",
                                                                                                                        "condition_concept_id", "condition_type_concept_id",
                                                                                                                        "condition_start_date", "condition_occurrence_start_time",
                                                                                                                        "condition_end_date", "condition_occurrence_end_time");
  private final MockDataTable                                                      measurement                      = new MockDataTable("person_id", "measurement_concept_id",
                                                                                                                        "measurement_date", "measurement_time", "value_as_number",
                                                                                                                        "value_as_concept_id", "measurement_type_concept_id");
  private final MockDataTable                                                      atlas_term_dictionary            = new MockDataTable("term_id", "term_name");
  private final MockDataTable                                                      concept_ancestor                 = new MockDataTable("ancestor_concept_id",
                                                                                                                        "descendant_concept_id");
  private final MockDataTable                                                      concept_relationship_contains    = new MockDataTable("concept_id_1", "concept_id_2");
  private final MockDataTable                                                      concept_relationship_atc         = new MockDataTable("concept_id_1", "concept_id_2");
  private final MockDataTable                                                      concept_synonym                  = new MockDataTable("concept_id", "concept_synonym_name",
                                                                                                                        "language_concept_id");
  private final MockDataTable                                                      drug_exposure                    = new MockDataTable("person_id", "drug_concept_id",
                                                                                                                        "drug_type_concept_id", "route_concept_id",
                                                                                                                        "drug_exposure_start_date", "drug_exposure_start_time",
                                                                                                                        "drug_exposure_end_date", "drug_exposure_end_time");
  private final MockDataTable                                                      concept                          = new MockDataTable("concept_id", "concept_name");
  private final MockDataTable                                                      conceptIdToCodeShortDictionary   = new MockDataTable("concept_id", "concept_code", "vocabulary");
  private final MockDataTable                                                      conceptCodeDictionary            = new MockDataTable("concept_id", "concept_name",
                                                                                                                        "vocabulary_id", "concept_code");

  private final HashMap<OHDSI_TABLE, ArrayList<SortPairSingle<Integer, String[]>>> tableToContents                  = new HashMap<OHDSI_TABLE, ArrayList<SortPairSingle<Integer, String[]>>>();

  public void add(final TestConstruct construct) {
    final TestTableCollection collection = construct.toTable(dictionary);
    final Iterator<TestTable> iterator = collection.iterate();
    while (iterator.hasNext()) {
      final TestTable table = iterator.next();
      ArrayList<SortPairSingle<Integer, String[]>> tables = tableToContents.get(table.getTable());
      if (tables == null) {
        tables = new ArrayList<SortPairSingle<Integer, String[]>>();
        tableToContents.put(table.getTable(), tables);
      }
      tables.addAll(table.getTableRows());
    }
  }

  private void generateDictionary() {
    generateConceptDictionary();
    generateAtlasConceptDictionary();
    generateTermDictionary();
    generateSynonymDictionary();
    generateHierarchyDictionary();
    generateRelationshipDictionary();
  }

  public int addConcept(final String text) {
    return dictionary.getConcept(text == null ? "EMPTY" : text);
  }

  private void generateRelationshipDictionary() {
    IntIterator iterator = dictionary.getRelationshipAtcKeys();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      concept_relationship_atc.addRow(id + "", dictionary.getRelationshipAtc(id) + "");
    }
    iterator = dictionary.getRelationshipContainsKeys();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      concept_relationship_contains.addRow(id + "", dictionary.getRelationshipContains(id) + "");
    }
  }

  private void generateSynonymDictionary() {
    final IntIterator iterator = dictionary.getSynonymKeyIds();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      concept_synonym.addRow(id + "", dictionary.getSynonym(id) + "");
    }
  }

  private void generateHierarchyDictionary() {
    final IntIterator iterator = dictionary.getHierarchyParentIds();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      concept_ancestor.addRow(id + "", dictionary.getChild(id) + "");
    }
  }

  private void generateConceptDictionary() {
    final IntIterator iterator = dictionary.getConceptIds();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      concept.addRow(id + "", dictionary.getConcept(id));
    }
  }

  private void generateAtlasConceptDictionary() {
    final IntIterator iterator = dictionary.getAtlasConceptIds();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      concept.addRow(id + "", dictionary.getAtlasConcept(id));
    }
  }

  private void generateTermDictionary() {
    final IntIterator iterator = dictionary.getAtlasTermDictionaryIds();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      atlas_term_dictionary.addRow(id + "", dictionary.getTerm(id));
    }
  }

  private void copyTableRows(final OHDSI_TABLE table, final MockDataTable result) {
    final ArrayList<SortPairSingle<Integer, String[]>> list = tableToContents.get(table);
    if (list != null) {
      Collections.sort(list);
      for (final SortPairSingle<Integer, String[]> row : list) {
        result.addRow(row.getObject());
      }
    }
  }

  public int addRxNorm(final String code, final String text) {
    return addCodeDictionary(code, OhdsiDatabaseDownload.VOCABULARY_RXNORM, text);
  }

  public int addIcd9(final String code, final String text) {
    return addCodeDictionary(code, OhdsiDatabaseDownload.VOCABULARY_ICD9, text);
  }

  public int addSnomed(final String code, final String text) {
    return addCodeDictionary(code, OhdsiDatabaseDownload.VOCABULARY_SNOMED, text);
  }

  public int addAtc(final String code, final String text) {
    return addCodeDictionary(code, OhdsiDatabaseDownload.VOCABULARY_ATC, text);
  }

  public int addCpt(final String code, final String text) {
    return addCodeDictionary(code, OhdsiDatabaseDownload.VOCABULARY_CPT, text);
  }

  private synchronized int addCodeDictionary(final String code, final String vocabulary, final String text) {
    StringKeyIntMap map = null;
    int conceptId = -1;
    if (vocabulary.equals(OhdsiDatabaseDownload.VOCABULARY_ATC)) {
      map = atcToConceptId;
    }
    else if (vocabulary.equals(OhdsiDatabaseDownload.VOCABULARY_RXNORM)) {
      map = rxNormToConceptId;
    }
    else if (vocabulary.equals(OhdsiDatabaseDownload.VOCABULARY_CPT)) {
      map = cptToConceptId;
    }
    else if (vocabulary.equals(OhdsiDatabaseDownload.VOCABULARY_ICD9)) {
      map = icd9ToConceptId;
    }
    else if (vocabulary.equals(OhdsiDatabaseDownload.VOCABULARY_SNOMED)) {
      map = snomedToConceptId;
    }

    if (map == null) {
      throw new UnsupportedOperationException("The vocabulary '" + vocabulary + "' is not supported");
    }

    if (map.containsKey(code)) {
      conceptId = map.get(code);
    }
    else {
      conceptId = dictionary.getNextConceptId();
      conceptIdToCodeShortDictionary.addRow(new String[] {conceptId + "", code, vocabulary});
      conceptCodeDictionary.addRow(new String[] {conceptId + "", text, vocabulary, code});
    }
    return conceptId;
  }

  private void getMaxPatientId() {
    final ArrayList<SortPairSingle<Integer, String[]>> list = tableToContents.get(OHDSI_TABLE.PERSON);
    Collections.sort(list);
    maxPatientId.addRow(new String[] {list.get(list.size() - 1).getComparable().intValue() + ""});
  }

  public Connection generate() {
    final MockConnection result = new MockConnection();
    generateDictionary();
    getMaxPatientId();
    copyTableRows(OHDSI_TABLE.DEATH, death);
    copyTableRows(OHDSI_TABLE.PERSON, person);
    copyTableRows(OHDSI_TABLE.MEASUREMENT, measurement);
    copyTableRows(OHDSI_TABLE.PROCEDURE_OCCURRENCE, procedure_occurrence_join_visits);
    copyTableRows(OHDSI_TABLE.NOTE, note);
    copyTableRows(OHDSI_TABLE.ATLAS_TERM_MENTIONS, atlas_term_mentions_join_notes);
    copyTableRows(OHDSI_TABLE.CONDITION_OCCURRENCE, condition_occurrence_join_visits);
    copyTableRows(OHDSI_TABLE.DRUG_EXPOSURE, drug_exposure);
    copyTableRows(OHDSI_TABLE.OBSERVATION, observation);
    result.addResultSet(OhdsiDatabaseDownload.ATLAS_TERM_DICTIONARY, atlas_term_dictionary);
    result.addResultSet(Commands.getTermsQuery(OhdsiDatabaseDownload.ATLAS_TERM_MENTIONS, 0, "person_id"), atlas_term_mentions_join_notes);
    result.addResultSet(OhdsiDatabaseDownload.RELATIONSHIPS_CONTAINS, concept_relationship_contains);
    result.addResultSet(OhdsiDatabaseDownload.RELATIONSHIPS_ATC_LEAF, concept_relationship_atc);
    result.addResultSet(OhdsiDatabaseDownload.HIERARCHIES, concept_ancestor);
    result.addResultSet(OhdsiDatabaseDownload.SYNONYMS, concept_synonym);
    result.addResultSet(OhdsiDatabaseDownload.CONCEPT_QUERY, concept);
    result.addResultSet(OhdsiDatabaseDownload.DEATH_QUERY, death);
    result.addResultSet(OhdsiDatabaseDownload.PERSON_QUERY, person);
    result.addResultSet(OhdsiDatabaseDownload.NOTE_QUERY, note);
    result.addResultSet(OhdsiDatabaseDownload.OBSERVATION_QUERY, observation);
    result.addResultSet(OhdsiDatabaseDownload.PROCEDURE_OCCURRENCE, procedure_occurrence_join_visits);
    result.addResultSet(OhdsiDatabaseDownload.LABS, measurement);
    result.addResultSet(OhdsiDatabaseDownload.DRUG_EXPOSURE, drug_exposure);
    result.addResultSet(OhdsiDatabaseDownload.CONDITION_OCCURRENCE, condition_occurrence_join_visits);
    result.addResultSet(OhdsiDatabaseDownload.CONCEPT_CODE_QUERY, conceptIdToCodeShortDictionary);
    result.addResultSet(OhdsiDatabaseDownload.CONCEPT_DICTIONARY, conceptCodeDictionary);
    result.addResultSet(OhdsiDatabaseDownload.MAX_PATIENT_ID, maxPatientId);
    return result;
  }

}