package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7LabDeRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES  = new String[] {"patient_id", "FLOOR(age_at_lab_in_days)", "YEAR(lab_time)", "component_text", "labvalue", "LOINC_CODE"};
  private final int            patientId;
  private Double               age           = null;
  private Integer              year          = null;
  private String               componentText = null;
  private Double               labValue      = null;
  private String               loincCode     = null;

  public static Stride7LabDeRecord create(final int patientId) {
    return new Stride7LabDeRecord(patientId);
  }

  public Stride7LabDeRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7LabDeRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7LabDeRecord componentText(final String componentText) {
    this.componentText = componentText;
    return this;
  }

  public Stride7LabDeRecord value(final Double labValue) {
    this.labValue = labValue;
    return this;
  }

  public Stride7LabDeRecord loinc(final String loinc) {
    this.loincCode = loinc;
    return this;
  }

  private Stride7LabDeRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", componentText, labValue == null ? null : labValue + "",
        loincCode};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7LabDeRecord result = new Stride7LabDeRecord(patientId);
    result.age(age);
    result.year(year);
    result.componentText(componentText);
    result.value(labValue);
    result.loinc(loincCode);
    return result;
  }

}