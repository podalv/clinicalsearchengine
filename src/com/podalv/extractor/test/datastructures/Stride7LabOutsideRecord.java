package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7LabOutsideRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES  = new String[] {"patient_id", "FLOOR(age_at_recorded_time_in_days)", "YEAR(recorded_time)", "gp2_disp_name", "meas_value", "", "",
      "LOIN_CODE"};
  private final int            patientId;
  private Double               age           = null;
  private Integer              year          = null;
  private String               componentText = null;
  private String               value         = null;
  private String               loincCode     = null;

  public static Stride7LabOutsideRecord create(final int patientId) {
    return new Stride7LabOutsideRecord(patientId);
  }

  public Stride7LabOutsideRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7LabOutsideRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7LabOutsideRecord name(final String name) {
    this.componentText = name;
    return this;
  }

  public Stride7LabOutsideRecord value(final String labValue) {
    this.value = labValue;
    return this;
  }

  public Stride7LabOutsideRecord loinc(final String loinc) {
    this.loincCode = loinc;
    return this;
  }

  private Stride7LabOutsideRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", componentText, value, "", "", loincCode};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7LabOutsideRecord result = new Stride7LabOutsideRecord(patientId);
    result.age(age);
    result.year(year);
    result.name(componentText);
    result.value(value);
    result.loinc(loincCode);
    return result;
  }

}