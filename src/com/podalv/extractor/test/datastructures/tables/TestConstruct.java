package com.podalv.extractor.test.datastructures.tables;

import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;

/** Each aspect of patient (construct) is constructed separately and will implement this interface
 *
 * @author podalv
 *
 */
public interface TestConstruct {

  public TestTableCollection toTable(TestIndexCollection dictionary);
}
