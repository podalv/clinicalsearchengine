package com.podalv.extractor.test.datastructures.tables;

import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;

/** drug_exposure
 *
 * @author podalv
 *
 */
public class OhdsiTestDrugs implements TestConstruct {

  private final int patientId;
  private int       drugConceptId     = OhdsiTestDemographics.UNDEFINED;
  private int       drugTypeConceptId = OhdsiTestDemographics.UNDEFINED;
  private int       routeConceptId    = OhdsiTestDemographics.UNDEFINED;
  private String    startDate         = null;
  private String    startTime         = null;
  private String    endDate           = null;
  private String    endTime           = null;

  private OhdsiTestDrugs(final int patientId) {
    this.patientId = patientId;
  }

  public static OhdsiTestDrugs add(final int patientId) {
    return new OhdsiTestDrugs(patientId);
  }

  public OhdsiTestDrugs routeConceptId(final int routeConceptId) {
    this.routeConceptId = routeConceptId;
    return this;
  }

  public OhdsiTestDrugs drugConceptId(final int drugConceptId) {
    this.drugConceptId = drugConceptId;
    return this;
  }

  public OhdsiTestDrugs drugTypeConceptId(final int drugTypeConceptId) {
    this.drugTypeConceptId = drugTypeConceptId;
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestDrugs startTime(final String date, final String time) {
    this.startDate = date;
    this.startTime = time;
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestDrugs endTime(final String date, final String time) {
    this.endDate = date;
    this.endTime = time;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection dictionary) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.DRUG_EXPOSURE, patientId, new String[] {patientId + "", drugConceptId + "", drugTypeConceptId + "", routeConceptId + "", startDate,
        startTime, endDate, endTime}));
    return result;
  }

}