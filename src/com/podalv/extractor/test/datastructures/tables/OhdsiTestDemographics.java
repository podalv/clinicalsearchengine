package com.podalv.extractor.test.datastructures.tables;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;
import com.podalv.utils.text.Format;

/** person table + death table
 *
 * @author podalv
 *
 */
public class OhdsiTestDemographics implements TestConstruct {

  public static final int UNDEFINED    = Integer.MIN_VALUE;
  private final int       patientId;
  private int             yearOfBirth  = UNDEFINED;
  private int             monthOfBirth = UNDEFINED;
  private int             dayOfBirth   = UNDEFINED;
  private int             yearOfDeath  = UNDEFINED;
  private int             monthOfDeath = UNDEFINED;
  private int             dayOfDeath   = UNDEFINED;
  private String          timeOfBirth  = null;
  private String          race         = null;
  private String          ethnicity    = null;
  private String          gender       = null;

  private OhdsiTestDemographics(final int patientId) {
    this.patientId = patientId;
  }

  public static OhdsiTestDemographics add(final int patientId) {
    return new OhdsiTestDemographics(patientId);
  }

  public OhdsiTestDemographics dob(final int year, final int month, final int day) {
    this.yearOfBirth = year;
    this.monthOfBirth = month;
    this.dayOfBirth = day;
    return this;
  }

  public OhdsiTestDemographics death(final int year, final int month, final int day) {
    this.yearOfDeath = year;
    this.monthOfDeath = month;
    this.dayOfDeath = day;
    return this;
  }

  public OhdsiTestDemographics timeOfBirth(final String time) {
    this.timeOfBirth = time;
    return this;
  }

  public OhdsiTestDemographics race(final String race) {
    this.race = race;
    return this;
  }

  public OhdsiTestDemographics ethnicity(final String eth) {
    this.ethnicity = eth;
    return this;
  }

  public OhdsiTestDemographics gender(final String gender) {
    this.gender = gender;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection context) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.PERSON, patientId, new String[] {patientId + "", context.getConcept(gender) + "", yearOfBirth + "", monthOfBirth + "", dayOfBirth + "",
        timeOfBirth, context.getConcept(race) + "", context.getConcept(ethnicity) + ""}));
    if (yearOfDeath != UNDEFINED) {
      result.add(new TestTable(OHDSI_TABLE.DEATH, patientId, new String[] {patientId + "", Format.format(yearOfDeath, monthOfDeath, dayOfDeath, Common.OHDSI_DATE_FORMAT)}));
    }
    return result;
  }

}