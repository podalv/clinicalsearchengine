package com.podalv.extractor.test.datastructures.tables;

import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;

/** observation table
 *
 * @author podalv
 *
 */
public class OhdsiTestObservation implements TestConstruct {

  private final int patientId;
  private int       conceptId;
  private String    date = null;
  private String    time = null;

  private OhdsiTestObservation(final int patientId) {
    this.patientId = patientId;
  }

  public static OhdsiTestObservation add(final int patientId) {
    return new OhdsiTestObservation(patientId);
  }

  public OhdsiTestObservation concept(final int conceptId) {
    this.conceptId = conceptId;
    return this;
  }

  public OhdsiTestObservation date(final String date, final String time) {
    this.date = date;
    this.time = time;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection dictionary) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.OBSERVATION, patientId, new String[] {patientId + "", conceptId + "", date, time}));
    return result;
  }

}
