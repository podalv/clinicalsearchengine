package com.podalv.extractor.test.datastructures.tables;

import java.util.ArrayList;

import com.podalv.search.datastructures.Enums;
import com.podalv.utils.datastructures.SortPairSingle;

/** Contains OHDSI_TABLE table type and the rows for the table
 *
 * @author podalv
 *
 */
public class TestTable {

  private final Enums.OHDSI_TABLE                            table;
  private final ArrayList<SortPairSingle<Integer, String[]>> tableRows;
  private int                                                patientId = OhdsiTestDemographics.UNDEFINED;

  public TestTable(final Enums.OHDSI_TABLE table, final int personId, final ArrayList<SortPairSingle<Integer, String[]>> tableRows) {
    this.table = table;
    this.tableRows = tableRows;
    this.patientId = personId;
  }

  public TestTable(final Enums.OHDSI_TABLE table, final int personId, final String[] tableRow) {
    this.table = table;
    this.patientId = personId;
    tableRows = new ArrayList<SortPairSingle<Integer, String[]>>();
    tableRows.add(new SortPairSingle<Integer, String[]>(personId, tableRow));
  }

  public TestTable(final Enums.OHDSI_TABLE table, final int personId) {
    this.table = table;
    this.patientId = personId;
    tableRows = new ArrayList<SortPairSingle<Integer, String[]>>();
  }

  public void add(final String[] row) {
    tableRows.add(new SortPairSingle<Integer, String[]>(patientId, row));
  }

  public Enums.OHDSI_TABLE getTable() {
    return table;
  }

  public ArrayList<SortPairSingle<Integer, String[]>> getTableRows() {
    return tableRows;
  }
}
