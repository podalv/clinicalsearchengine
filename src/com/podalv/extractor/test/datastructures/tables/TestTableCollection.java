package com.podalv.extractor.test.datastructures.tables;

import java.util.Iterator;
import java.util.LinkedList;

/** A collection of test tables of different or same types
 *
 * @author podalv
 *
 */
public class TestTableCollection {

  private final LinkedList<TestTable> tableTypeToData = new LinkedList<TestTable>();

  public void add(final TestTable table) {
    tableTypeToData.add(table);
  }

  public Iterator<TestTable> iterate() {
    return tableTypeToData.iterator();
  }
}
