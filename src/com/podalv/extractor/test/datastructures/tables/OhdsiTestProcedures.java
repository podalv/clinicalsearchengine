package com.podalv.extractor.test.datastructures.tables;

import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;

/** procedure_occurrence table
*
* @author podalv
*
*/
public class OhdsiTestProcedures implements TestConstruct {

  private final int patientId;
  private int       procedureConceptId = OhdsiTestDemographics.UNDEFINED;
  private int       visitConceptId     = OhdsiTestDemographics.UNDEFINED;
  private String    date               = null;
  private String    time               = null;

  private OhdsiTestProcedures(final int patientId) {
    this.patientId = patientId;
  }

  public static OhdsiTestProcedures add(final int patientId) {
    return new OhdsiTestProcedures(patientId);
  }

  public OhdsiTestProcedures procedureConceptId(final int procedureConceptId) {
    this.procedureConceptId = procedureConceptId;
    return this;
  }

  public OhdsiTestProcedures visitConceptId(final int visitConceptId) {
    this.visitConceptId = visitConceptId;
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestProcedures date(final String date, final String time) {
    this.date = date;
    this.time = time;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection dictionary) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.PROCEDURE_OCCURRENCE, patientId, new String[] {patientId + "", visitConceptId + "", procedureConceptId + "", date, time}));
    return result;
  }

}
