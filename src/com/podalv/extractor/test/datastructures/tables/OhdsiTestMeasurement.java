package com.podalv.extractor.test.datastructures.tables;

import org.junit.Assert;

import com.podalv.extractor.stride6.OhdsiDatabaseDownload;
import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;
import com.podalv.utils.text.Format;

/** measurement OHDSI table
 *
 * @author podalv
 *
 */
public class OhdsiTestMeasurement implements TestConstruct {

  private final int patientId;
  private int       measurementTypeConceptId = OhdsiTestDemographics.UNDEFINED;
  private int       measurementConceptId     = OhdsiTestDemographics.UNDEFINED;
  private int       valueAsConceptId         = OhdsiTestDemographics.UNDEFINED;
  private double    value                    = OhdsiTestDemographics.UNDEFINED;
  private String    date                     = null;
  private String    time                     = null;

  private OhdsiTestMeasurement(final int patientId) {
    this.patientId = patientId;
  }

  public static OhdsiTestMeasurement add(final int patientId) {
    return new OhdsiTestMeasurement(patientId);
  }

  public OhdsiTestMeasurement measurementTypeConceptId(final int measurementTypeConceptId) {
    this.measurementTypeConceptId = measurementTypeConceptId;
    return this;
  }

  public OhdsiTestMeasurement measurementConceptId(final int measurementConceptId) {
    this.measurementConceptId = measurementConceptId;
    return this;
  }

  public OhdsiTestMeasurement labValue(final int valueAsConceptId) {
    //Measurement cannot be defined as vitals already
    Assert.assertEquals(value, OhdsiTestDemographics.UNDEFINED, 0.01);
    this.valueAsConceptId = valueAsConceptId;
    return this;
  }

  public OhdsiTestMeasurement vitalsValue(final double value) {
    //Measurement cannot be defined as Lab value already
    Assert.assertEquals(valueAsConceptId, OhdsiTestDemographics.UNDEFINED);
    this.measurementTypeConceptId = OhdsiDatabaseDownload.MEASUREMENT_TYPE_ID_VITALS;
    this.value = value;
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestMeasurement date(final String date, final String time) {
    this.date = date;
    this.time = time;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection dictionary) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.MEASUREMENT, patientId, new String[] {patientId + "", measurementConceptId + "", date, time, Format.format(value, 5),
        valueAsConceptId + "", measurementTypeConceptId + ""}));
    return result;
  }
}
