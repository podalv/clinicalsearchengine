package com.podalv.extractor.test.datastructures.tables;

import java.util.Iterator;

import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.maps.string.StringHashSet;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;

/** note, atlas_term_mentions, atlas_term_dictionary
 *
 * @author podalv
 *
 */
public class OhdsiTestNotes implements TestConstruct {

  private final int           patientId;
  private int                 noteId             = OhdsiTestDemographics.UNDEFINED;
  private int                 noteTypeConceptId  = OhdsiTestDemographics.UNDEFINED;
  private final StringHashSet positiveTerms      = new StringHashSet();
  private final StringHashSet negatedTerms       = new StringHashSet();
  private final StringHashSet familyHistoryTerms = new StringHashSet();
  private String              date               = null;
  private String              time               = null;

  private OhdsiTestNotes(final int patientId) {
    this.patientId = patientId;
  }

  public static OhdsiTestNotes add(final int patientId) {
    return new OhdsiTestNotes(patientId);
  }

  public int getNoteId() {
    return noteId;
  }

  public int getPatientId() {
    return patientId;
  }

  public OhdsiTestNotes noteId(final int noteId) {
    this.noteId = noteId;
    return this;
  }

  public OhdsiTestNotes noteType(final int typeConceptId) {
    this.noteTypeConceptId = typeConceptId;
    return this;
  }

  public OhdsiTestNotes positiveTerms(final String[] terms) {
    positiveTerms.addAll(terms);
    return this;
  }

  public OhdsiTestNotes negatedTerms(final String[] terms) {
    negatedTerms.addAll(terms);
    return this;
  }

  public OhdsiTestNotes familyHistoryTerms(final String[] terms) {
    familyHistoryTerms.addAll(terms);
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestNotes date(final String date, final String time) {
    this.date = date;
    this.time = time;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection dictionary) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.NOTE, patientId, new String[] {patientId + "", noteId + "", noteTypeConceptId + "", date, time}));
    final TestTable notes = new TestTable(OHDSI_TABLE.ATLAS_TERM_MENTIONS, patientId);
    final StringHashSet added = new StringHashSet();
    addNotes(dictionary, notes, positiveTerms.iterator(), added);
    addNotes(dictionary, notes, negatedTerms.iterator(), added);
    addNotes(dictionary, notes, familyHistoryTerms.iterator(), added);
    result.add(notes);
    return result;
  }

  private void addNotes(final TestIndexCollection dictionary, final TestTable notes, final Iterator<String> iterator, final StringHashSet added) {
    while (iterator.hasNext()) {
      final String text = iterator.next();
      if (!added.contains(text)) {
        notes.add(new String[] {patientId + "", noteId + "", dictionary.getTerm(text) + "", negatedTerms.contains(text) ? "1" : "0", familyHistoryTerms.contains(text) ? "1" : "0"});
      }
      added.add(text);
    }
  }

}
