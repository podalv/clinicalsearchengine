package com.podalv.extractor.test.datastructures.tables;

import com.podalv.extractor.stride6.OhdsiDatabaseDownload;
import com.podalv.extractor.test.datastructures.indices.TestIndexCollection;
import com.podalv.search.datastructures.Enums.OHDSI_TABLE;

/** condition_occurrence, atlas_condition_diagnosis
 *
 * @author podalv
 *
 */
public class OhdsiTestCondition implements TestConstruct {

  private final int patientId;
  private int       conditionConceptId     = OhdsiTestDemographics.UNDEFINED;
  private int       visitConceptId         = OhdsiTestDemographics.UNDEFINED;
  private int       conditionTypeConceptId = OhdsiTestDemographics.UNDEFINED;
  private String    startDate              = null;
  private String    endDate                = null;
  private String    startTime              = null;
  private String    endTime                = null;

  private OhdsiTestCondition(final int patientId) {
    this.patientId = patientId;
  }

  public OhdsiTestCondition primaryDiagnosis() {
    conditionTypeConceptId = OhdsiDatabaseDownload.getPrimaryCodeConceptIds().next();
    return this;
  }

  public static OhdsiTestCondition add(final int patientId) {
    return new OhdsiTestCondition(patientId);
  }

  public OhdsiTestCondition visitConceptId(final int visitConceptId) {
    this.visitConceptId = visitConceptId;
    return this;
  }

  public OhdsiTestCondition conditionConceptId(final int conditionConceptId) {
    this.conditionConceptId = conditionConceptId;
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestCondition startTime(final String date, final String time) {
    this.startDate = date;
    this.startTime = time;
    return this;
  }

  /**
  *
  * @param date format yyyy-MM-dd
  * @param time HH:mm:ss
  * @return
  */
  public OhdsiTestCondition endTime(final String date, final String time) {
    this.endDate = date;
    this.endTime = time;
    return this;
  }

  @Override
  public TestTableCollection toTable(final TestIndexCollection dictionary) {
    final TestTableCollection result = new TestTableCollection();
    result.add(new TestTable(OHDSI_TABLE.CONDITION_OCCURRENCE, patientId, new String[] {patientId + "", visitConceptId + "", conditionConceptId + "", conditionTypeConceptId + "",
        startDate, startTime, endDate, endTime}));
    return result;
  }
}