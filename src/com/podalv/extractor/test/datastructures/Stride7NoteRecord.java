package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7NoteRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"note_id", "note_type", "age_at_note_in_days", "YEAR(note_date)", "patient_id"};
  private final int            patientId;
  private Integer              noteId       = null;
  private String               noteType     = null;
  private Double               age          = null;
  private Integer              year         = null;

  public static Stride7NoteRecord create(final int patientId) {
    return new Stride7NoteRecord(patientId);
  }

  public Stride7NoteRecord(final int patientId) {
    this.patientId = patientId;
  }

  public Stride7NoteRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7NoteRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7NoteRecord noteType(final String type) {
    this.noteType = type;
    return this;
  }

  public Stride7NoteRecord noteId(final Integer noteId) {
    this.noteId = noteId;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {noteId == null ? null : noteId + "", noteType, age == null ? null : Math.floor(age) + "", year == null ? null : year + "", patientId + ""};
  }

  @Override
  public long comparable() {
    if (noteId == null) {
      return 0;
    }
    return BigInteger.valueOf(noteId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7NoteRecord result = new Stride7NoteRecord(patientId);
    result.age(age);
    result.year(year);
    result.noteId(noteId);
    result.noteType(noteType);
    return result;
  }

}