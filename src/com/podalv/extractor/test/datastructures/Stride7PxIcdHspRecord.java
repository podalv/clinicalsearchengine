package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7PxIcdHspRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES    = new String[] {"patient_id", "CEILING(age_at_proc_in_days)", "YEAR(proc_date)", "ref_bill_code", "ref_bill_code_set_c"};
  private final int            patientId;
  private Double               age             = null;
  private Integer              year            = null;
  private String               code            = null;
  private Integer              refBillCodeType = null;

  public static Stride7PxIcdHspRecord create(final int patientId) {
    return new Stride7PxIcdHspRecord(patientId);
  }

  public Stride7PxIcdHspRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7PxIcdHspRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  /**
   *
   * @param code
   * @param refBillCode 1 = ICD9, 2 = ICD10
   * @return
   */
  public Stride7PxIcdHspRecord code(final String code, final Integer refBillCode) {
    this.code = code;
    this.refBillCodeType = refBillCode;
    return this;
  }

  private Stride7PxIcdHspRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", code, refBillCodeType == null ? null : refBillCodeType + "",};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7PxIcdHspRecord result = new Stride7PxIcdHspRecord(patientId);
    result.age(age);
    result.year(year);
    result.code(code, refBillCodeType);
    return result;
  }

}