package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7VisitRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "CEILING(age_at_admit_in_days)", "CEILING(age_at_disch_in_days)", "dx_id", "YEAR(admit_date_time)"};
  private final int            patientId;
  private Double               ageStart     = null;
  private Double               ageEnd       = null;
  private Integer              year         = null;
  private Integer              dxId         = null;

  public static Stride7VisitRecord create(final int patientId) {
    return new Stride7VisitRecord(patientId);
  }

  public Stride7VisitRecord age(final Double ageStart, final Double ageEnd) {
    this.ageStart = ageStart;
    this.ageEnd = ageEnd;
    return this;
  }

  public Stride7VisitRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7VisitRecord dxId(final Integer dxId) {
    this.dxId = dxId;
    return this;
  }

  private Stride7VisitRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", ageStart == null ? null : Math.floor(ageStart) + "", ageEnd == null ? null : Math.floor(ageEnd) + "", dxId == null ? null : dxId + "",
        year == null ? null : year + ""};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(ageStart == null ? 0 : (int) (ageStart * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7VisitRecord result = new Stride7VisitRecord(patientId);
    result.age(ageStart, ageEnd);
    result.year(year);
    result.dxId(dxId);
    return result;
  }

}