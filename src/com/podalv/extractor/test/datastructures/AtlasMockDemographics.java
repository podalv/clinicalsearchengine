package com.podalv.extractor.test.datastructures;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import com.podalv.db.test.MockResultSet;

public class AtlasMockDemographics extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"pid", "gender", "race", "ethnicity", "dob", "dod", "YEAR(dod)"};
  private final int            id;
  private String               race         = "";
  private String               gender       = "";
  private String               ethnicity    = "";
  private String               death        = null;
  private String               birth        = null;

  public static AtlasMockDemographics create(final int id) {
    return new AtlasMockDemographics(id);
  }

  public AtlasMockDemographics race(final String race) {
    this.race = race;
    return this;
  }

  public AtlasMockDemographics gender(final String gender) {
    this.gender = gender;
    return this;
  }

  public AtlasMockDemographics ethnicity(final String ethnicity) {
    this.ethnicity = ethnicity;
    return this;
  }

  public AtlasMockDemographics death(final String death) {
    this.death = death;
    return this;
  }

  public AtlasMockDemographics birth(final String birth) {
    this.birth = birth;
    return this;
  }

  public int getId() {
    return id;
  }

  public String getEthnicity() {
    return ethnicity;
  }

  public String getDeath() {
    return death;
  }

  public String getBirth() {
    return birth;
  }

  public String getGender() {
    return gender;
  }

  public String getRace() {
    return race;
  }

  private AtlasMockDemographics(final int id) {
    this.id = id;
  }

  @Override
  public long comparable() {
    return id;
  }

  public static String getYear(final String date) {
    try {
      final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("PST"));
      cal.setTimeInMillis(new SimpleDateFormat(MockResultSet.DATE_PATTERN).parse(date).getTime());
      return cal.get(Calendar.YEAR) + "";
    }
    catch (final Exception e) {
      return "0";
    }
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", gender, race, ethnicity, birth, death == null ? null : death, death == null ? "0" : getYear(death)};
  }

  @Override
  public Stride6MockRecord copy() {
    final AtlasMockDemographics result = new AtlasMockDemographics(id);
    result.death = death;
    result.ethnicity = ethnicity;
    result.gender = gender;
    result.race = race;
    result.birth = birth;
    return result;
  }
}