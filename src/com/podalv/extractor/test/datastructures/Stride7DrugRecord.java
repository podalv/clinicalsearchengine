package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7DrugRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "medication_id", "FLOOR(age_at_order_in_days)", "YEAR(order_time)", "order_status_c", "med_route_c",
      "med_id", "FLOOR(age_at_start_in_days)", "FLOOR(age_at_end_in_days)", "FLOOR(age_at_discon_in_days)", "ordering_mode_c", "order_class_c"};
  private final int            patientId;
  private Double               age          = null;
  private Double               ageStart     = null;
  private Double               ageEnd       = null;
  private Double               ageDiscon    = null;
  private Integer              year         = null;
  private Integer              medId        = null;
  private Integer              route        = null;
  private Integer              status       = null;
  private Integer              medicationId = null;
  private Integer              orderingMode = null;
  private Integer              orderClass   = null;

  public static Stride7DrugRecord create(final int patientId) {
    return new Stride7DrugRecord(patientId);
  }

  public Stride7DrugRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7DrugRecord ageStart(final Double ageStart) {
    this.ageStart = ageStart;
    return this;
  }

  public Stride7DrugRecord ageEnd(final Double ageEnd) {
    this.ageEnd = ageEnd;
    return this;
  }

  public Stride7DrugRecord ageDiscon(final Double ageDiscon) {
    this.ageDiscon = ageDiscon;
    return this;
  }

  public Stride7DrugRecord medId(final Integer medId) {
    this.medId = medId;
    return this;
  }

  public Stride7DrugRecord orderClassId(final Integer orderClassId) {
    this.orderClass = orderClassId;
    return this;
  }

  public Stride7DrugRecord orderingMode(final Integer orderingMode) {
    this.orderingMode = orderingMode;
    return this;
  }

  public Stride7DrugRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7DrugRecord route(final Integer route) {
    this.route = route;
    return this;
  }

  public Stride7DrugRecord status(final Integer status) {
    this.status = status;
    return this;
  }

  public Stride7DrugRecord medicationId(final Integer medicationId) {
    this.medicationId = medicationId;
    return this;
  }

  private Stride7DrugRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", medicationId == null ? null : medicationId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", status == null
        ? null
        : status + "", route == null ? null : route + "", medId == null ? null : medId + "", ageStart == null ? null : Math.floor(ageStart) + "", ageEnd == null ? null
            : Math.floor(ageEnd) + "", ageDiscon == null ? null : Math.floor(ageDiscon) + "", orderingMode == null ? null : orderingMode + "", orderClass == null ? "-1"
                : orderClass + ""};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7DrugRecord result = new Stride7DrugRecord(patientId);
    result.age(age);
    result.year(year);
    result.status(status);
    result.route(route);
    result.medicationId(medicationId);
    result.medId(medId);
    result.ageStart(ageStart);
    result.ageEnd(ageEnd);
    result.ageDiscon(ageDiscon);
    result.orderingMode(orderingMode);
    result.orderClassId(orderClass);
    return result;
  }

}