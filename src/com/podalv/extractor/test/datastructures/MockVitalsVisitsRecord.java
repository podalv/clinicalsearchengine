package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockVitalsVisitsRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "age_at_contact_in_days", "bp_systolic", "bp_diastolic", "temperature", "pulse", "weight_in_lbs",
      "height", "respirations", "bmi", "bsa"};

  private final int            patientId;
  private Double               age          = 0d;
  private Integer              systolic     = 0;
  private Integer              diastolic    = 0;
  private Double               temp         = 0d;
  private Integer              pulse        = 0;
  private Double               weight       = 0d;
  private String               height       = "";
  private Integer              resp         = 0;
  private Double               bmi          = 0d;
  private Double               bsa          = 0d;

  public static MockVitalsVisitsRecord create(final int patientId) {
    return new MockVitalsVisitsRecord(patientId);
  }

  public static MockVitalsVisitsRecord createNulls(final int patientId) {
    final MockVitalsVisitsRecord result = new MockVitalsVisitsRecord(patientId);
    result.age = null;
    result.systolic = null;
    result.diastolic = null;
    result.temp = null;
    result.pulse = null;
    result.weight = null;
    result.height = null;
    result.resp = null;
    result.bmi = null;
    result.bsa = null;
    return result;
  }

  private MockVitalsVisitsRecord(final int patientId) {
    this.patientId = patientId;
  }

  public MockVitalsVisitsRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockVitalsVisitsRecord bp(final Integer systolic, final Integer diastolic) {
    this.systolic = systolic;
    this.diastolic = diastolic;
    return this;
  }

  public MockVitalsVisitsRecord temp(final Double temp) {
    this.temp = temp;
    return this;
  }

  public MockVitalsVisitsRecord pulse(final Integer pulse) {
    this.pulse = pulse;
    return this;
  }

  public MockVitalsVisitsRecord height(final String height) {
    this.height = height;
    return this;
  }

  public MockVitalsVisitsRecord weight(final Double weight) {
    this.weight = weight;
    return this;
  }

  public MockVitalsVisitsRecord resp(final Integer resp) {
    this.resp = resp;
    return this;
  }

  public MockVitalsVisitsRecord bmi(final Double bmi) {
    this.bmi = bmi;
    return this;
  }

  public MockVitalsVisitsRecord bsa(final Double bsa) {
    this.bsa = bsa;
    return this;
  }

  public Double getBmi() {
    return bmi;
  }

  public Double getBsa() {
    return bsa;
  }

  public Integer getDiastolic() {
    return diastolic;
  }

  public String getHeight() {
    return height;
  }

  public Integer getPulse() {
    return pulse;
  }

  public Integer getResp() {
    return resp;
  }

  public Integer getSystolic() {
    return systolic;
  }

  public Double getTemp() {
    return temp;
  }

  public Double getWeight() {
    return weight;
  }

  public int getPatientId() {
    return patientId;
  }

  public Double getAge() {
    return age;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : age + "", systolic == null ? null : systolic + "", diastolic == null ? null : diastolic + "",
        temp == null ? null : temp + "", pulse == null ? null : pulse + "", weight == null ? null : weight + "", height, resp == null ? null : resp + "",
        bmi == null ? null : bmi + "", bsa == null ? null : bsa + ""};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockVitalsVisitsRecord result = new MockVitalsVisitsRecord(patientId);
    result.age = age;
    result.bmi = bmi;
    result.bsa = bsa;
    result.diastolic = diastolic;
    result.height = height;
    result.pulse = pulse;
    result.resp = resp;
    result.systolic = systolic;
    result.temp = temp;
    result.weight = weight;
    return result;
  }
}
