package com.podalv.extractor.test.datastructures;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;

import com.podalv.db.test.MockConnection;
import com.podalv.db.test.MockDataTable;
import com.podalv.extractor.stride6.AtlasDatabaseDownload;
import com.podalv.extractor.stride6.Commands;

public class AtlasTestDataSource {

  final ArrayList<Stride6MockRecord> codeDictionary = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> demographics   = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> encounters     = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> prescriptions  = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> measurements   = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> termDictonary  = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> notes          = new ArrayList<Stride6MockRecord>();
  final ArrayList<Stride6MockRecord> termMentions   = new ArrayList<Stride6MockRecord>();

  public void addCodeDictionary(final Stride6MockRecord record) {
    codeDictionary.add(record);
  }

  public void addDemographics(final Stride6MockRecord record) {
    demographics.add(record);
  }

  public void addEncounter(final Stride6MockRecord record) {
    encounters.add(record);
  }

  public void addPrescription(final Stride6MockRecord record) {
    prescriptions.add(record);
  }

  public void addMeasurement(final Stride6MockRecord record) {
    measurements.add(record);
  }

  public void addTermDictionary(final Stride6MockRecord record) {
    termDictonary.add(record);
  }

  public void addNote(final Stride6MockRecord record) {
    notes.add(record);
  }

  public void addTermMention(final Stride6MockRecord record) {
    termMentions.add(record);
  }

  private MockDataTable getTable(final String[] columns, final ArrayList<Stride6MockRecord> records) {
    final MockDataTable result = new MockDataTable(MockRxRecord.COLUMN_NAMES);
    Collections.sort(records);
    for (final Stride6MockRecord record : records) {
      result.addRow(record.toTableRow());
    }
    return result;
  }

  private ArrayList<Stride6MockRecord> getMaxPatientId() {
    final ArrayList<Stride6MockRecord> result = new ArrayList<Stride6MockRecord>();
    result.add(new Stride6MockRecord() {

      @Override
      public String[] toTableRow() {
        return new String[] {demographics.size() + ""};
      }

      @Override
      public Stride6MockRecord copy() {
        return null;
      }

      @Override
      public long comparable() {
        return 0;
      }
    });
    return result;
  }

  public Connection generate() {
    final MockConnection result = new MockConnection();

    result.addResultSet(AtlasDatabaseDownload.DEMOGRAPHICS, getTable(AtlasDatabaseDownload.DEMOGRAPHICS_COLUMNS, demographics));
    result.addResultSet(AtlasDatabaseDownload.CODE_DICTIONARY, getTable(AtlasDatabaseDownload.CODE_DICTIONARY_COLUMNS, codeDictionary));
    result.addResultSet(AtlasDatabaseDownload.ENCOUNTERS, getTable(AtlasDatabaseDownload.ENCOUNTERS_COLUMNS, encounters));
    result.addResultSet(AtlasDatabaseDownload.MAX_PATIENT_ID, getTable(new String[] {"count"}, getMaxPatientId()));
    result.addResultSet(AtlasDatabaseDownload.MEASUREMENTS, getTable(AtlasDatabaseDownload.MEASUREMENTS_COLUMNS, measurements));
    result.addResultSet(AtlasDatabaseDownload.PRESCRIPTIONS, getTable(AtlasDatabaseDownload.PRESCRIPTIONS_COLUMNS, prescriptions));
    result.addResultSet(AtlasDatabaseDownload.TERM_DICTIONARY, getTable(AtlasDatabaseDownload.TERM_DICTIONARY_COLUMNS, termDictonary));
    result.addResultSet(Commands.getTermsQuery(AtlasDatabaseDownload.TERM_MENTIONS, 0, "pid"), getTable(AtlasDatabaseDownload.TERM_MENTIONS_COLUMNS, termMentions));
    result.addResultSet(AtlasDatabaseDownload.NOTES, getTable(AtlasDatabaseDownload.NOTES_COLUMNS, notes));
    return result;
  }

}
