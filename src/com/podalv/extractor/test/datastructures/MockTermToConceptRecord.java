package com.podalv.extractor.test.datastructures;

/** A single TermToConceptRecord
 *
 * @author podalv
 *
 */
public class MockTermToConceptRecord extends Stride6MockRecord {

  public static String[] COLUMN_NAMES = new String[] {"mapid", "code", "text", "mapid", "mapname", "ownerid", "experimental", "version", "querytext"};

  private final String   cuiCode;
  private final String   text;
  private String         experimental = "N";
  private String         queryText    = "";

  public static MockTermToConceptRecord create(final String cui, final String text) {
    return new MockTermToConceptRecord(cui, text);
  }

  public MockTermToConceptRecord experimental() {
    this.experimental = "Y";
    return this;
  }

  public MockTermToConceptRecord queryText(final String queryText) {
    this.queryText = queryText;
    return this;
  }

  private MockTermToConceptRecord(final String cui, final String text) {
    this.cuiCode = cui;
    this.text = text;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {"1", cuiCode, text, "1", "", "", experimental, "1.1", queryText};
  }

  @Override
  public long comparable() {
    return 1;
  }

  @Override
  public Stride6MockRecord copy() {
    final MockTermToConceptRecord result = new MockTermToConceptRecord(cuiCode, text);
    result.experimental = experimental;
    result.queryText = queryText;
    return result;
  }
}