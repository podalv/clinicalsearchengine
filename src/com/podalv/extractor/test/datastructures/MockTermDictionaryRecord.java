package com.podalv.extractor.test.datastructures;

public class MockTermDictionaryRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"tid", "str"};

  private final int            tid;
  private final String         str;

  public static MockTermDictionaryRecord create(final int tid, final String str) {
    return new MockTermDictionaryRecord(tid, str);
  }

  private MockTermDictionaryRecord(final int tid, final String str) {
    this.tid = tid;
    this.str = str;
  }

  public int getTid() {
    return tid;
  }

  public String getStr() {
    return str;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {tid + "", str};
  }

  @Override
  public long comparable() {
    return tid;
  }

  @Override
  public Stride6MockRecord copy() {
    return new MockTermDictionaryRecord(tid, str);
  }
}
