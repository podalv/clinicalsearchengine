package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockVisitRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"visit_id", "patient_id", "visit_year", "age_at_visit_in_days", "src_visit", "duration", "code", "sab", "SOURCE_CODE"};

  public static final String   SAB_CPT      = "CPT";
  public static final String   SAB_ICD      = "ICD";
  public static final String   SAB_SNOMED   = "SNOMED";

  public static enum VISIT_TYPE {
    SNOMED, CPT, ICD9
  };
  private int       visitId = -1;
  private final int patientId;
  private Integer   visitYear;
  private Double    age;
  private String    srcVisit;
  private Double    duration;
  private String    code;
  private String    sab;
  private String    sourceCode;

  public static MockVisitRecord create(final VISIT_TYPE type, final int patientId) {
    final MockVisitRecord record = new MockVisitRecord(patientId);
    if (type == VISIT_TYPE.SNOMED) {
      record.sab = "SNOMED";
    }
    else if (type == VISIT_TYPE.CPT) {
      record.sab = "CPT";
    }
    else {
      record.sab = "ICD";
    }
    return record;
  }

  public Double getAge() {
    return age;
  }

  public String getCode() {
    return code;
  }

  public Double getDuration() {
    return duration;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getSab() {
    return sab;
  }

  public String getSourceCode() {
    return sourceCode;
  }

  public String getSrcVisit() {
    return srcVisit;
  }

  public int getVisitId() {
    return visitId;
  }

  public Integer getVisitYear() {
    return visitYear;
  }

  public MockVisitRecord year(final Integer year) {
    this.visitYear = year;
    return this;
  }

  public MockVisitRecord year(final int year) {
    this.visitYear = year;
    return this;
  }

  public MockVisitRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockVisitRecord duration(final Double duration) {
    this.duration = duration;
    return this;
  }

  public MockVisitRecord age(final double age) {
    this.age = age;
    return this;
  }

  public MockVisitRecord duration(final double duration) {
    this.duration = duration;
    return this;
  }

  public MockVisitRecord srcVisit(final String srcVisit) {
    this.srcVisit = srcVisit;
    return this;
  }

  public MockVisitRecord code(final String code) {
    this.code = code;
    return this;
  }

  public MockVisitRecord sab(final String sab) {
    this.sab = sab;
    return this;
  }

  public MockVisitRecord sourceCode(final String sourceCode) {
    this.sourceCode = sourceCode;
    this.sab = "DX_ID";
    return this;
  }

  public MockVisitRecord visitId(final int visitId) {
    this.visitId = visitId;
    return this;
  }

  private MockVisitRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {visitId + "", patientId + "", visitYear == null ? null : visitYear + "", age == null ? null : age + "", srcVisit, duration == null ? null : duration + "",
        code, sab, sourceCode};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockVisitRecord result = new MockVisitRecord(patientId);
    result.age = age;
    result.code = code;
    result.duration = duration;
    result.sab = sab;
    result.sourceCode = sourceCode;
    result.srcVisit = srcVisit;
    result.visitId = visitId;
    result.visitYear = visitYear;
    return result;
  }
}
