package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockLabsShcRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "age_at_taken_time_in_days", "base_name", "ord_num_value", "reference_low", "reference_high",
      "ref_normal_vals"                     };

  private final int            patientId;
  private Double               age;
  private String               baseName;
  private Double               value;
  private String               low;
  private String               high;
  private String               normalVal;

  public static MockLabsShcRecord create(final int patientId) {
    return new MockLabsShcRecord(patientId);
  }

  public Double getAge() {
    return age;
  }

  public String getBaseName() {
    return baseName;
  }

  public String getHigh() {
    return high;
  }

  public String getLow() {
    return low;
  }

  public String getNormalVal() {
    return normalVal;
  }

  public int getPatientId() {
    return patientId;
  }

  public Double getValue() {
    return value;
  }

  private MockLabsShcRecord(final int patientId) {
    this.patientId = patientId;
  }

  public MockLabsShcRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockLabsShcRecord name(final String baseName) {
    this.baseName = baseName;
    return this;
  }

  public MockLabsShcRecord value(final Double value, final String low, final String high, final String normalVal) {
    this.value = value;
    this.low = low;
    this.high = high;
    this.normalVal = normalVal;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : age + "", baseName, value == null ? null : value + "", low, high, normalVal};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockLabsShcRecord result = new MockLabsShcRecord(patientId);
    result.age = age;
    result.baseName = baseName;
    result.high = high;
    result.low = low;
    result.normalVal = normalVal;
    result.value = value;
    return result;
  }
}
