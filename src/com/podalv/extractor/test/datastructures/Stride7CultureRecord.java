package com.podalv.extractor.test.datastructures;

public class Stride7CultureRecord extends Stride6MockRecord {

  public static String[] COLUMN_NAMES = new String[] {"stride7.SHC_lab_epic_comment_de.patient_id", "FLOOR(stride7.SHC_lab_epic_de.age_at_taken_in_days)",
      "YEAR(stride7.SHC_lab_epic_de.taken_time)", "stride7.SHC_lab_epic_de.group_lab_name, stride7.SHC_lab_epic_comment_de.cmt_results"};

  private final int      patientId;
  private Double         age          = null;
  private Integer        year         = null;
  private String         labName      = null;
  private String         value        = null;

  public static Stride7CultureRecord create(final int patientId) {
    return new Stride7CultureRecord(patientId);
  }

  private Stride7CultureRecord(final int patientId) {
    this.patientId = patientId;
  }

  public Stride7CultureRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7CultureRecord labName(final String labName) {
    this.labName = labName;
    return this;
  }

  public Stride7CultureRecord value(final String value) {
    this.value = value;
    return this;
  }

  public Stride7CultureRecord age(final Double age) {
    this.age = age;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year.toString(), labName, value};
  }

  @Override
  public long comparable() {
    return patientId;
  }

  @Override
  public Stride6MockRecord copy() {
    return Stride7CultureRecord.create(patientId).age(age).year(year).labName(labName).value(value);
  }

}
