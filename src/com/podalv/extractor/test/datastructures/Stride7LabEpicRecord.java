package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7LabEpicRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "FLOOR(age_at_taken_in_days)", "YEAR(taken_time)", "lab_name", "ord_value", "LOINC_CODE"};
  private final int            patientId;
  private Double               age          = null;
  private Integer              year         = null;
  private String               labName      = null;
  private String               ordValue     = null;
  private String               loincCode    = null;

  public static Stride7LabEpicRecord create(final int patientId) {
    return new Stride7LabEpicRecord(patientId);
  }

  public Stride7LabEpicRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7LabEpicRecord year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7LabEpicRecord labName(final String labName) {
    this.labName = labName;
    return this;
  }

  public Stride7LabEpicRecord value(final String labValue) {
    this.ordValue = labValue;
    return this;
  }

  public Stride7LabEpicRecord loinc(final String loinc) {
    this.loincCode = loinc;
    return this;
  }

  private Stride7LabEpicRecord(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", labName, ordValue, loincCode};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7LabEpicRecord result = new Stride7LabEpicRecord(patientId);
    result.age(age);
    result.year(year);
    result.labName(labName);
    result.value(ordValue);
    result.loinc(loincCode);
    return result;
  }

}