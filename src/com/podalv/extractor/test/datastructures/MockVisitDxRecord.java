package com.podalv.extractor.test.datastructures;

public class MockVisitDxRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"visit_id", "dx_id", "primary_dx_yn"};

  private static final String  PRIMARY      = "Y";
  private Integer              visitId      = 0;
  private final Integer        dxId;
  private String               primaryDxYn;

  public static MockVisitDxRecord create(final Integer dxId) {
    return new MockVisitDxRecord(dxId);
  }

  public MockVisitDxRecord visitId(final Integer visitId) {
    this.visitId = visitId;
    return this;
  }

  public boolean isPrimary() {
    return this.primaryDxYn.equalsIgnoreCase(PRIMARY);
  }

  public Integer getVisitId() {
    return visitId;
  }

  public Integer getDxId() {
    return dxId;
  }

  public MockVisitDxRecord primary() {
    this.primaryDxYn = PRIMARY;
    return this;
  }

  public MockVisitDxRecord primary(final String primaryDxYn) {
    this.primaryDxYn = primaryDxYn;
    return this;
  }

  private MockVisitDxRecord(final Integer dxId) {
    this.dxId = dxId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {visitId == null ? null : visitId + "", dxId == null ? null : dxId + "", primaryDxYn};
  }

  @Override
  public long comparable() {
    return visitId;
  }

  @Override
  public Stride6MockRecord copy() {
    final MockVisitDxRecord result = new MockVisitDxRecord(dxId);
    result.primaryDxYn = primaryDxYn;
    result.visitId = visitId;
    return result;
  }
}
