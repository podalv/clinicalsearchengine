package com.podalv.extractor.test.datastructures;

public class AtlasMockMeasurements extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES  = new String[] {"pid", "code_type", "code", "date", "value", "computed_value"};
  private final int            id;
  private String               codeType      = "";
  private String               code          = "";
  private String               date          = "";
  private Double               value         = null;
  private String               computedValue = null;

  public static AtlasMockMeasurements create(final int id) {
    return new AtlasMockMeasurements(id);
  }

  private AtlasMockMeasurements(final int id) {
    this.id = id;
  }

  public AtlasMockMeasurements codeType(final String codeType) {
    this.codeType = codeType;
    return this;
  }

  public AtlasMockMeasurements code(final String code) {
    this.code = code;
    return this;
  }

  public AtlasMockMeasurements date(final String date) {
    this.date = date;
    return this;
  }

  public AtlasMockMeasurements value(final Double value) {
    this.value = value;
    return this;
  }

  public AtlasMockMeasurements computedValue(final String computedValue) {
    this.computedValue = computedValue;
    return this;
  }

  @Override
  public long comparable() {
    return id;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", codeType, code, date, value == null ? null : value + "", computedValue};
  }

  @Override
  public Stride6MockRecord copy() {
    final AtlasMockMeasurements result = new AtlasMockMeasurements(id);
    result.code = code;
    result.codeType = codeType;
    result.date = date;
    result.value = value;
    result.computedValue = computedValue;
    return result;
  }
}