package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class MockLabsLpchRecord extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "age_at_effective_time_in_days", "component_code", "labvalue", "reference_range"};

  private final int            patientId;
  private Double               age;
  private String               code;
  private Double               value;
  private String               range;

  public static MockLabsLpchRecord create(final int patientId) {
    return new MockLabsLpchRecord(patientId);
  }

  public Double getAge() {
    return age;
  }

  public String getCode() {
    return code;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getRange() {
    return range;
  }

  public Double getValue() {
    return value;
  }

  private MockLabsLpchRecord(final int patientId) {
    this.patientId = patientId;
  }

  public MockLabsLpchRecord age(final Double age) {
    this.age = age;
    return this;
  }

  public MockLabsLpchRecord code(final String code) {
    this.code = code;
    return this;
  }

  public MockLabsLpchRecord value(final Double value) {
    this.value = value;
    return this;
  }

  public MockLabsLpchRecord range(final String range) {
    this.range = range;
    return this;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", age == null ? null : age + "", code, value == null ? null : value + "", range};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final MockLabsLpchRecord result = new MockLabsLpchRecord(patientId);
    result.age = age;
    result.code = code;
    result.range = range;
    result.value = value;
    return result;
  }

}