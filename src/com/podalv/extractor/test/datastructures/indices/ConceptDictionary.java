package com.podalv.extractor.test.datastructures.indices;

import java.util.concurrent.atomic.AtomicInteger;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.string.StringKeyIntReversedMap;

/** Stores concepts and allows adding new ones
 *
 * @author podalv
 *
 */
public class ConceptDictionary {

  private final StringKeyIntReversedMap concepts = new StringKeyIntReversedMap();

  protected ConceptDictionary() {
    // only package access
  }

  public IntIterator getIds() {
    return concepts.getIntStrMap().keySet().iterator();
  }

  public String get(final int conceptId) {
    return concepts.get(conceptId).toString();
  }

  public synchronized int get(final AtomicInteger conceptCounter, final String text) {
    if (concepts.get(text) != null) {
      return concepts.get(text).intValue();
    }
    concepts.put(conceptCounter.incrementAndGet(), text);
    return conceptCounter.get();
  }
}
