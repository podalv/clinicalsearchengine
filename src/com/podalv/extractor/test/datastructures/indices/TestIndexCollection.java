package com.podalv.extractor.test.datastructures.indices;

import java.util.concurrent.atomic.AtomicInteger;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;

/** Encapsulates all the indices for the OHDSI schema (concept, atlas_concept...)
 *
 * @author podalv
 *
 */
public class TestIndexCollection {

  private final AtomicInteger        currentConceptId                     = new AtomicInteger();
  private final ConceptDictionary    concepts                             = new ConceptDictionary();
  private final ConceptDictionary    atlasTermDictionary                  = new ConceptDictionary();
  private final ConceptDictionary    atlasConcepts                        = new ConceptDictionary();
  private final IntKeyIntOpenHashMap conceptToLanguageConcept             = new IntKeyIntOpenHashMap();
  private final IntKeyIntOpenHashMap conceptHierarchyParentChildConceptId = new IntKeyIntOpenHashMap();
  private final IntKeyIntOpenHashMap conceptRelationshipContains          = new IntKeyIntOpenHashMap();
  private final IntKeyIntOpenHashMap conceptRelationshipAtc               = new IntKeyIntOpenHashMap();

  public int getNextConceptId() {
    return currentConceptId.incrementAndGet();
  }

  public void addParent(final int childId, final int parentId) {
    conceptHierarchyParentChildConceptId.put(parentId, childId);
  }

  public void addRelationshipContains(final int conceptId1, final int conceptId2) {
    conceptRelationshipContains.put(conceptId1, conceptId2);
  }

  public void addRelationshipAtc(final int conceptId1, final int conceptId2) {
    conceptRelationshipAtc.put(conceptId1, conceptId2);
  }

  public IntIterator getRelationshipContainsKeys() {
    return conceptRelationshipContains.keySet().iterator();
  }

  public IntIterator getRelationshipAtcKeys() {
    return conceptRelationshipAtc.keySet().iterator();
  }

  public int getRelationshipContains(final int conceptId1) {
    return conceptRelationshipContains.containsKey(conceptId1) ? conceptRelationshipContains.get(conceptId1) : -1;
  }

  public int getRelationshipAtc(final int conceptId1) {
    return conceptRelationshipAtc.containsKey(conceptId1) ? conceptRelationshipAtc.get(conceptId1) : -1;
  }

  public void addSynonym(final int conceptId, final int languageConceptId) {
    conceptToLanguageConcept.put(conceptId, languageConceptId);
  }

  public IntIterator getSynonymKeyIds() {
    return conceptToLanguageConcept.keySet().iterator();
  }

  public IntIterator getHierarchyParentIds() {
    return conceptHierarchyParentChildConceptId.keySet().iterator();
  }

  public int getChild(final int parentConceptId) {
    return conceptHierarchyParentChildConceptId.containsKey(parentConceptId) ? conceptHierarchyParentChildConceptId.get(parentConceptId) : -1;
  }

  public int getSynonym(final int conceptId) {
    return conceptToLanguageConcept.containsKey(conceptId) ? conceptToLanguageConcept.get(conceptId) : -1;
  }

  public IntIterator getConceptIds() {
    return concepts.getIds();
  }

  public IntIterator getAtlasConceptIds() {
    return atlasConcepts.getIds();
  }

  public IntIterator getAtlasTermDictionaryIds() {
    return atlasTermDictionary.getIds();
  }

  public int getConcept(final String text) {
    return concepts.get(currentConceptId, text);
  }

  public int getTerm(final String text) {
    return atlasTermDictionary.get(currentConceptId, text);
  }

  public int getAtlasConcept(final String text) {
    return atlasConcepts.get(currentConceptId, text);
  }

  public String getConcept(final int conceptId) {
    return concepts.get(conceptId);
  }

  public String getTerm(final int termId) {
    return atlasTermDictionary.get(termId);
  }

  public String getAtlasConcept(final int conceptId) {
    return atlasConcepts.get(conceptId);
  }
}
