package com.podalv.extractor.test.datastructures;

public class AtlasMockPrescriptions extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"pid", "code", "status", "route", "start_time", "end_time"};
  private final int            id;
  private String               codeType     = "";
  private String               code         = "";
  private String               route        = "";
  private String               status       = null;
  private String               startTime    = null;
  private String               endTime      = null;

  public static AtlasMockPrescriptions create(final int id) {
    return new AtlasMockPrescriptions(id);
  }

  private AtlasMockPrescriptions(final int id) {
    this.id = id;
  }

  public AtlasMockPrescriptions codeType(final String codeType) {
    this.codeType = codeType;
    return this;
  }

  public AtlasMockPrescriptions code(final String code) {
    this.code = code;
    return this;
  }

  public AtlasMockPrescriptions route(final String route) {
    this.route = route;
    return this;
  }

  public AtlasMockPrescriptions status(final String status) {
    this.status = status;
    return this;
  }

  public AtlasMockPrescriptions startTime(final String start) {
    this.startTime = start;
    return this;
  }

  public AtlasMockPrescriptions endTime(final String end) {
    this.endTime = end;
    return this;
  }

  @Override
  public long comparable() {
    return id;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {id + "", code, status, route, startTime, endTime};
  }

  @Override
  public Stride6MockRecord copy() {
    final AtlasMockPrescriptions result = new AtlasMockPrescriptions(id);
    result.code = code;
    result.codeType = codeType;
    result.status = status;
    result.route = route;
    result.startTime = startTime;
    result.endTime = endTime;
    return result;
  }
}