package com.podalv.extractor.test.datastructures;

import java.math.BigInteger;

public class Stride7DrugHl7Record extends Stride6MockRecord {

  public static final String[] COLUMN_NAMES = new String[] {"patient_id", "medication_id", "FLOOR(age_at_order_in_days)", "YEAR(order_time)", "order_status_c", "med_route_c",
      "medId", "FLOOR(age_at_start_in_days)", "FLOOR(age_at_end_in_days)", "FLOOR(age_at_end_in_days)", "-1", "-1"};
  private final int            patientId;
  private Double               age          = null;
  private Double               ageEnd       = null;
  private Integer              year         = null;
  private Integer              route        = null;
  private String               status       = null;
  private Integer              medicationId = null;
  private Integer              medId        = null;

  public static Stride7DrugHl7Record create(final int patientId) {
    return new Stride7DrugHl7Record(patientId);
  }

  public Stride7DrugHl7Record age(final Double age) {
    this.age = age;
    return this;
  }

  public Stride7DrugHl7Record ageEnd(final Double ageEnd) {
    this.ageEnd = ageEnd;
    return this;
  }

  public Stride7DrugHl7Record year(final Integer year) {
    this.year = year;
    return this;
  }

  public Stride7DrugHl7Record medId(final Integer medId) {
    this.medId = medId;
    return this;
  }

  public Stride7DrugHl7Record route(final Integer route) {
    this.route = route;
    return this;
  }

  public Stride7DrugHl7Record status(final String status) {
    this.status = status;
    return this;
  }

  public Stride7DrugHl7Record medicationId(final Integer medicationId) {
    this.medicationId = medicationId;
    return this;
  }

  private Stride7DrugHl7Record(final int patientId) {
    this.patientId = patientId;
  }

  @Override
  public String[] toTableRow() {
    return new String[] {patientId + "", medicationId == null ? null : medicationId + "", age == null ? null : Math.floor(age) + "", year == null ? null : year + "", status,
        route == null ? null : route + "", medId == null ? null : medId + "", age == null ? null : Math.floor(age) + "", ageEnd == null ? null : Math.floor(ageEnd) + "",
        ageEnd == null ? null : Math.floor(ageEnd) + "", "-1", "-1"};
  }

  @Override
  public long comparable() {
    return BigInteger.valueOf(patientId * 100000l).add(BigInteger.valueOf(age == null ? 0 : (int) (age * 100l))).longValue();
  }

  @Override
  public Stride6MockRecord copy() {
    final Stride7DrugHl7Record result = new Stride7DrugHl7Record(patientId);
    result.age(age);
    result.ageEnd(ageEnd);
    result.year(year);
    result.status(status);
    result.route(route);
    result.medicationId(medicationId);
    result.medId(medId);
    return result;
  }

}