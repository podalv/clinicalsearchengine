package com.podalv.extractor.test.datastructures;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.db.test.MockConnection;
import com.podalv.db.test.MockDataTable;
import com.podalv.extractor.stride6.Commands;
import com.podalv.extractor.stride6.Stride7DatabaseDownload;
import com.podalv.extractor.stride6.datastructures.MedicationSource;
import com.podalv.search.datastructures.index.StringIntegerFastIndex;

public class Stride7TestDataSource {

  private static String[]                    DEPARTMENT_MAP_COLUMNS          = new String[] {"department_id", "department_name"};
  private static String[]                    DX_ID_ICD9_MAP_COLUMNS          = new String[] {"dx_id", "current_icd9_list"};
  private static String[]                    DX_ID_ICD10_MAP_COLUMNS         = new String[] {"dx_id", "current_icd10_list"};
  private static String[]                    DX_ID_ICD10_PRIMARY_MAP_COLUMNS = new String[] {"dx_id", "ref_bill_code"};
  private static String[]                    VISIT_TYPE_COLUMNS              = new String[] {"disp_enc_type_c", "name"};
  private static String[]                    MED_ID_COLUMNS                  = new String[] {"medication_id", "rxcui"};
  private static String[]                    CULTURE_NAME_DICTIONARY_COLUMNS = new String[] {"group_lab_name", "mapped"};
  private static String[]                    MED_STATUS_COLUMNS              = new String[] {"order_status_c", "name"};
  private static String[]                    MED_CLASS_COLUMNS               = new String[] {"order_class_c", "name"};
  private static String[]                    MED_ROUTE_COLUMNS               = new String[] {"med_route_c", "accom_reason_c"};
  private static String[]                    LOINC_COLUMNS                   = new String[] {"LOINC_CODE", "long_common_name"};
  private static String[]                    TERM_DICTIONARY_COLUMNS         = new String[] {"tid", "str"};
  private static String[]                    ADMIN_COLUMNS                   = new String[] {"patient_id", "med_id", "age_at_taken_in_days"};

  private final ArrayList<Stride6MockRecord> demographics                    = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> vitals                          = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> departmentLpch                  = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> departmentShc                   = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> departmentLpch2                 = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> departmentShc2                  = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitTypeLpch                   = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitTypeShc                    = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitAdmitLpch                  = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitAdmitShc                   = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitExtInjuryLpch              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitExtInjuryShc               = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxHl7Lpch                  = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxHl7Shc                   = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxHspAcctLpch              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxHspAcctShc               = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxPatEncLpch               = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxPatEncShc                = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxProbListLpch             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitDxProbListShc              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptDeLpch                = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptDeShc                 = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptHspLpch               = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptHspShc                = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc               = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxIcdHspLpch               = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxIcdHspShc                = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> culturesShc                     = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> cultureDictionary               = new ArrayList<>();
  private final HashMap<String, String>      cultureNameDictionary           = new HashMap<>();

  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch1             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch2             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch3             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch4             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch5             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvLpch6             = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc1              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc2              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc3              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc4              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc5              = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxCptProvShc6              = new ArrayList<>();

  private final ArrayList<Stride6MockRecord> medLpch                         = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> medShc                          = new ArrayList<>();

  private final ArrayList<Stride6MockRecord> medHl7Lpch                      = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> medHl7Shc                       = new ArrayList<>();

  private final ArrayList<Stride6MockRecord> visitPxIcdDeShc                 = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> visitPxIcdDeLpch                = new ArrayList<>();

  private final ArrayList<Stride6MockRecord> notesMaster                     = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> termMentions                    = new ArrayList<>();

  private final StringIntegerFastIndex       departmentMapLpch               = new StringIntegerFastIndex();
  private final StringIntegerFastIndex       departmentMapShc                = new StringIntegerFastIndex();
  private final StringIntegerFastIndex       visitTypeMapShc                 = new StringIntegerFastIndex();
  private final StringIntegerFastIndex       visitTypeMapLpch                = new StringIntegerFastIndex();

  private final StringIntegerFastIndex       termDictionary                  = new StringIntegerFastIndex();

  private final ArrayList<Double>            adminLpch                       = new ArrayList<>();
  private final ArrayList<Double>            adminShc                        = new ArrayList<>();

  private final ArrayList<String>            loincTexts                      = new ArrayList<>();

  private final ArrayList<String[]>          dxLpch                          = new ArrayList<>();
  private final ArrayList<String[]>          dxShc                           = new ArrayList<>();

  private final ArrayList<Integer>           medicationIdToRxNormLpch        = new ArrayList<>();
  private final ArrayList<Integer>           medicationIdToRxNormShc         = new ArrayList<>();
  private final ArrayList<Integer>           medicationIdToRxNormHl7         = new ArrayList<>();

  private final ArrayList<String>            medStatusLpch                   = new ArrayList<>();
  private final ArrayList<String>            medStatusShc                    = new ArrayList<>();

  private final ArrayList<String>            medClassLpch                    = new ArrayList<>();
  private final ArrayList<String>            medClassShc                     = new ArrayList<>();

  private final ArrayList<String>            medRouteShc                     = new ArrayList<>();

  private final ArrayList<Stride6MockRecord> labsDeLpch                      = new ArrayList<>();

  private final ArrayList<Stride6MockRecord> labsEpicLpch                    = new ArrayList<>();
  private final ArrayList<Stride6MockRecord> labsEpicShc                     = new ArrayList<>();

  public Stride7TestDataSource() {
    medStatusLpch.add("null");
    medStatusShc.add("null");
    medStatusLpch.add(MedicationSource.STATUS_CANCELED);
    medStatusLpch.add(MedicationSource.STATUS_DENIED);
    medStatusShc.add(MedicationSource.STATUS_CANCELED);
    medStatusShc.add(MedicationSource.STATUS_DENIED);
  }

  public void addDemographics(final Stride7Demographics record) {
    demographics.add(record);
    Collections.sort(demographics);
  }

  public void addVitals(final Stride7VitalsRecord vital) {
    vitals.add(vital);
    Collections.sort(vitals);
  }

  public void addLabDe(final Stride7LabDeRecord labDe) {
    labsDeLpch.add(labDe);
    Collections.sort(labsDeLpch);
  }

  public void addLabEpicLpch(final Stride7LabEpicRecord lab) {
    labsEpicLpch.add(lab);
    Collections.sort(labsEpicLpch);
  }

  public void addLabEpicShc(final Stride7LabEpicRecord lab) {
    labsEpicShc.add(lab);
    Collections.sort(labsEpicShc);
  }

  public void addNotes(final Stride7NoteRecord note) {
    notesMaster.add(note);
    Collections.sort(notesMaster);
  }

  public void addAdminLpch(final int pid, final int medId, final double time) {
    adminLpch.add((double) pid);
    adminLpch.add((double) medId);
    adminLpch.add(time);
  }

  public void addAdminShc(final int pid, final int medId, final double time) {
    adminShc.add((double) pid);
    adminShc.add((double) medId);
    adminShc.add(time);
  }

  public synchronized int addMedicationIdLpch(final int rxCui) {
    for (int x = 0; x < medicationIdToRxNormLpch.size(); x++) {
      if (medicationIdToRxNormLpch.get(x) == rxCui) {
        return x;
      }
    }
    medicationIdToRxNormLpch.add(rxCui);
    return medicationIdToRxNormLpch.size() - 1;
  }

  public void addMedLpch(final Stride7DrugRecord record) {
    medLpch.add(record);
    Collections.sort(medLpch);
  }

  public void addCultureLabName(final String groupLabName, final String transformedName) {
    cultureNameDictionary.put(groupLabName.toLowerCase(), transformedName);
  }

  public void addMedShc(final Stride7DrugRecord record) {
    medShc.add(record);
    Collections.sort(medShc);
  }

  public void addTermMention(final Stride7TermMentionRecord record) {
    termMentions.add(record);
    Collections.sort(termMentions);
  }

  public void addMedHl7Lpch(final Stride7DrugHl7Record record) {
    medHl7Lpch.add(record);
    Collections.sort(medHl7Lpch);
  }

  public void addMedHl7Shc(final Stride7DrugHl7Record record) {
    medHl7Shc.add(record);
    Collections.sort(medHl7Shc);
  }

  public synchronized int addMedicationStatusShc(final String status) {
    for (int x = 0; x < medStatusShc.size(); x++) {
      if (medStatusShc.get(x).equals(status)) {
        return x;
      }
    }
    medStatusShc.add(status);
    return medStatusShc.size() - 1;
  }

  public synchronized int addMedicationClassShc(final String medClass) {
    for (int x = 0; x < medClassShc.size(); x++) {
      if (medClassShc.get(x).equals(medClass)) {
        return x;
      }
    }
    medClassShc.add(medClass);
    return medClassShc.size() - 1;
  }

  public synchronized int addMedicationClassLpch(final String medClass) {
    for (int x = 0; x < medClassLpch.size(); x++) {
      if (medClassLpch.get(x).equals(medClass)) {
        return x;
      }
    }
    medClassLpch.add(medClass);
    return medClassLpch.size() - 1;
  }

  public synchronized int addMedicationRouteShc(final String route) {
    for (int x = 0; x < medRouteShc.size(); x++) {
      if (medRouteShc.get(x).equals(route)) {
        return x;
      }
    }
    medRouteShc.add(route);
    return medRouteShc.size() - 1;
  }

  /** A workaround for the Stride7 issue is that both routes for LPCH and SHC are stored in SHC
   *
   * @param route
   * @return
   */
  public synchronized int addMedicationRouteLpch(final String route) {
    for (int x = 0; x < medRouteShc.size(); x++) {
      if (medRouteShc.get(x).equals(route)) {
        return x;
      }
    }
    medRouteShc.add(route);
    return medRouteShc.size() - 1;
  }

  public synchronized int addMedicationStatusLpch(final String status) {
    for (int x = 0; x < medStatusLpch.size(); x++) {
      if (medStatusLpch.get(x).equals(status)) {
        return x;
      }
    }
    medStatusLpch.add(status);
    return medStatusLpch.size() - 1;
  }

  public synchronized int addMedicationIdShc(final int rxCui) {
    for (int x = 0; x < medicationIdToRxNormShc.size(); x++) {
      if (medicationIdToRxNormShc.get(x) == rxCui) {
        return x;
      }
    }
    medicationIdToRxNormShc.add(rxCui);
    return medicationIdToRxNormShc.size() - 1;
  }

  public synchronized int addMedicationIdHl7(final int rxCui) {
    for (int x = 0; x < medicationIdToRxNormHl7.size(); x++) {
      if (medicationIdToRxNormHl7.get(x) == rxCui) {
        return x;
      }
    }
    medicationIdToRxNormHl7.add(rxCui);
    return medicationIdToRxNormHl7.size() - 1;
  }

  public void addVisitTypeLpch(final Stride7VisitTypeRecord rec) {
    visitTypeLpch.add(rec);
    Collections.sort(visitTypeLpch);
  }

  public void addVisitTypeShc(final Stride7VisitTypeRecord rec) {
    visitTypeShc.add(rec);
    Collections.sort(visitTypeShc);
  }

  public void addDepartmentLpch(final Stride7DepartmentRecord rec) {
    departmentLpch.add(rec);
    Collections.sort(departmentLpch);
  }

  public void addDepartmentShc(final Stride7DepartmentRecord rec) {
    departmentShc.add(rec);
    Collections.sort(departmentShc);
  }

  public void addVisitPxHspLpch(final Stride7VisitWithSabRecord rec) {
    visitPxCptHspLpch.add(rec);
    Collections.sort(visitPxCptHspLpch);
  }

  public void addVisitPxHspShc(final Stride7VisitWithSabRecord rec) {
    visitPxCptHspShc.add(rec);
    Collections.sort(visitPxCptHspShc);
  }

  public void addVisitPxIcdDeLpch(final Stride7VisitWithSabRecord rec) {
    visitPxIcdDeLpch.add(rec);
    Collections.sort(visitPxIcdDeLpch);
  }

  public void addVisitPxIcdDeShc(final Stride7VisitWithSabRecord rec) {
    visitPxIcdDeShc.add(rec);
    Collections.sort(visitPxIcdDeShc);
  }

  public void addVisitPxCptDeLpch(final Stride7VisitWithSabRecord rec) {
    visitPxCptDeLpch.add(rec);
    Collections.sort(visitPxCptDeLpch);
  }

  public void addVisitPxCptDeShc(final Stride7VisitWithSabRecord rec) {
    visitPxCptDeShc.add(rec);
    Collections.sort(visitPxCptDeShc);
  }

  public void addVisitPxCptProvLpch(final Stride7VisitWithSabRecord rec) {
    visitPxCptProvLpch.add(rec);
    Collections.sort(visitPxCptProvLpch);
  }

  public void addVisitPxCptProvShc(final Stride7VisitWithSabRecord rec) {
    visitPxCptProvShc.add(rec);
    Collections.sort(visitPxCptProvShc);
  }

  public void addVisitHl7Lpch(final Stride7VisitHl7Record rec) {
    visitDxHl7Lpch.add(rec);
    Collections.sort(visitDxHl7Lpch);
  }

  public void addCultureDictionary(final Stride7CultureDictionaryRecord rec) {
    cultureDictionary.add(rec);
    Collections.sort(cultureDictionary);
  }

  public void addCulturesShc(final Stride7CultureRecord rec) {
    culturesShc.add(rec);
    Collections.sort(culturesShc);
  }

  public void addVisitHl7Shc(final Stride7VisitHl7Record rec) {
    visitDxHl7Shc.add(rec);
    Collections.sort(visitDxHl7Shc);
  }

  public void addVisitPxIcdHspLpch(final Stride7VisitHl7Record rec) {
    visitPxIcdHspLpch.add(rec);
    Collections.sort(visitPxIcdHspLpch);
  }

  public void addVisitPxIcdHspShc(final Stride7VisitHl7Record rec) {
    visitPxIcdHspShc.add(rec);
    Collections.sort(visitPxIcdHspShc);
  }

  public void addHospAcctLpch(final Stride7VisitRecord rec) {
    visitDxHspAcctLpch.add(rec);
    Collections.sort(visitDxHspAcctLpch);
  }

  public void addHospAcctShc(final Stride7VisitRecord rec) {
    visitDxHspAcctShc.add(rec);
    Collections.sort(visitDxHspAcctShc);
  }

  public void addDxProbListLpch(final Stride7VisitPrimaryRecord rec) {
    visitDxProbListLpch.add(rec);
    Collections.sort(visitDxProbListLpch);
  }

  public void addDxProbListShc(final Stride7VisitPrimaryRecord rec) {
    visitDxProbListShc.add(rec);
    Collections.sort(visitDxProbListShc);
  }

  public void addVisitAdmitLpch(final Stride7VisitRecord rec) {
    visitAdmitLpch.add(rec);
    Collections.sort(visitAdmitLpch);
  }

  public void addVisitAdmitShc(final Stride7VisitRecord rec) {
    visitAdmitShc.add(rec);
    Collections.sort(visitAdmitShc);
  }

  public void addDxPatEncLpch(final Stride7VisitPrimaryRecord rec) {
    visitDxPatEncLpch.add(rec);
    Collections.sort(visitDxPatEncLpch);
  }

  public void addDxPatEncShc(final Stride7VisitPrimaryRecord rec) {
    visitDxPatEncShc.add(rec);
    Collections.sort(visitDxPatEncShc);
  }

  public void addVisitPxCptProvShc(final Stride7VisitPrimaryRecord rec, final int number) {
    ArrayList<Stride6MockRecord> map = null;
    switch (number) {
    case 1:
      map = visitPxCptProvShc1;
      break;
    case 2:
      map = visitPxCptProvShc2;
      break;
    case 3:
      map = visitPxCptProvShc3;
      break;
    case 4:
      map = visitPxCptProvShc4;
      break;
    case 5:
      map = visitPxCptProvShc5;
      break;
    case 6:
      map = visitPxCptProvShc6;
      break;
    }
    if (map == null) {
      throw new RuntimeException("Argument " + number + " is not supported");
    }
    map.add(rec);
    Collections.sort(map);
  }

  public void addVisitPxCptProvLpch(final Stride7VisitPrimaryRecord rec, final int number) {
    ArrayList<Stride6MockRecord> map = null;
    switch (number) {
    case 1:
      map = visitPxCptProvLpch1;
      break;
    case 2:
      map = visitPxCptProvLpch2;
      break;
    case 3:
      map = visitPxCptProvLpch3;
      break;
    case 4:
      map = visitPxCptProvLpch4;
      break;
    case 5:
      map = visitPxCptProvLpch5;
      break;
    case 6:
      map = visitPxCptProvLpch6;
      break;
    }
    if (map == null) {
      throw new RuntimeException("Argument " + number + " is not supported");
    }
    map.add(rec);
    Collections.sort(map);
  }

  public void addVisitExtInjuryLpch(final Stride7VisitRecord rec) {
    visitExtInjuryLpch.add(rec);
    Collections.sort(visitExtInjuryLpch);
  }

  public void addVisitExtInjuryShc(final Stride7VisitRecord rec) {
    visitExtInjuryShc.add(rec);
    Collections.sort(visitExtInjuryShc);
  }

  public void addDepartmentApptLpch(final Stride7DepartmentRecord rec) {
    departmentLpch2.add(rec);
    Collections.sort(departmentLpch2);
  }

  public void addDepartmentApptShc(final Stride7DepartmentRecord rec) {
    departmentShc2.add(rec);
    Collections.sort(departmentShc2);
  }

  public int getDepartmentIdLpch(final String departmentName) {
    return departmentMapLpch.add(departmentName);
  }

  public int getDepartmentIdShc(final String departmentName) {
    return departmentMapShc.add(departmentName);
  }

  public int getVisitTypeIdLpch(final String visitName) {
    return visitTypeMapLpch.add(visitName);
  }

  public int getVisitTypeIdShc(final String visitName) {
    return visitTypeMapShc.add(visitName);
  }

  public int getTermId(final String term) {
    return termDictionary.add(term);
  }

  public synchronized String addLoincText(final String loincText) {
    for (int x = 0; x < loincTexts.size(); x++) {
      if (loincTexts.get(x).equalsIgnoreCase(loincText)) {
        return x + "";
      }
    }
    loincTexts.add(loincText);
    return (loincTexts.size() - 1) + "";
  }

  public synchronized int addDxIdLpch(final String icd9Code, final String icd10Code, final String refBillCodeIcd9, final String refBillCodeIcd10) {
    if (icd9Code == null || icd10Code == null || (refBillCodeIcd9 == null && refBillCodeIcd10 == null)) {
      throw new UnsupportedOperationException("All three fields must be defined");
    }
    for (int x = 0; x < dxLpch.size(); x++) {
      if (dxLpch.get(x)[0].equals(icd9Code) || dxLpch.get(x)[1].equals(icd10Code)) {
        return x;
      }
    }
    dxLpch.add(new String[] {icd9Code, icd10Code, refBillCodeIcd9, refBillCodeIcd10});
    return dxLpch.size() - 1;
  }

  public synchronized int addDxIdShc(final String icd9Code, final String icd10Code, final String refBillCodeIcd9, final String refBillCodeIcd10) {
    if (icd9Code == null || icd10Code == null || (refBillCodeIcd9 == null && refBillCodeIcd10 == null)) {
      throw new UnsupportedOperationException("All three fields must be defined");
    }
    for (int x = 0; x < dxShc.size(); x++) {
      if (dxShc.get(x)[0].equals(icd9Code) || dxShc.get(x)[1].equals(icd10Code)) {
        return x;
      }
    }
    dxShc.add(new String[] {icd9Code, icd10Code, refBillCodeIcd9, refBillCodeIcd10});
    return dxShc.size() - 1;
  }

  private MockDataTable generateDictionary(final StringIntegerFastIndex map, final String ... columns) {
    final MockDataTable table = new MockDataTable(columns);
    final String[] keys = map.keySet();
    for (final String key : keys) {
      table.addRow(map.get(key) + "", key);
    }
    return table;
  }

  private MockDataTable generateDictionary(final HashMap<String, String> map, final String ... columns) {
    final MockDataTable table = new MockDataTable(columns);
    final Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, String> entry = iterator.next();
      table.addRow(entry.getKey(), entry.getValue());
    }
    return table;
  }

  private MockDataTable generateAdmin(final ArrayList<Double> admin, final String ... columns) {
    final MockDataTable table = new MockDataTable(columns);
    for (int x = 0; x < admin.size(); x += 3) {
      table.addRow((int) Math.floor(admin.get(x)) + "", (int) Math.floor(admin.get(x + 1)) + "", admin.get(x + 2) + "");
    }
    return table;
  }

  private MockDataTable generateDictionaryDiagnoses(final ArrayList<String[]> map, final int index, final String[] columns) {
    final MockDataTable result = new MockDataTable(columns);
    for (int x = 0; x < map.size(); x++) {
      if (map.get(x)[index] != null) {
        result.addRow(x + "", map.get(x)[index]);
      }
    }
    return result;
  }

  private MockDataTable generateMedicationDictionary(final ArrayList<?> list, final String ... columns) {
    final MockDataTable table = new MockDataTable(columns);
    for (int x = 0; x < list.size(); x++) {
      table.addRow(String.valueOf(x), String.valueOf(list.get(x)));
    }
    return table;
  }

  public Connection generate() {
    final MockConnection result = new MockConnection();

    result.addResultSet(Stride7DatabaseDownload.DEMOGRAPHICS_7, Stride6TestDataSource.getTable(Stride7Demographics.COLUMN_NAMES, demographics));
    result.addResultSet(Stride7DatabaseDownload.VITALS_VISITS_7, Stride6TestDataSource.getTable(Stride7VitalsRecord.COLUMN_NAMES, vitals));

    result.addResultSet(Stride7DatabaseDownload.LPCH_VISIT_DE, Stride6TestDataSource.getTable(Stride7DepartmentRecord.COLUMN_NAMES, departmentLpch));
    result.addResultSet(Stride7DatabaseDownload.SHC_VISIT_DE, Stride6TestDataSource.getTable(Stride7DepartmentRecord.COLUMN_NAMES, departmentShc));

    result.addResultSet(Stride7DatabaseDownload.LPCH_VISIT_DE_VISIT_TYPE, Stride6TestDataSource.getTable(Stride7VisitTypeRecord.COLUMN_NAMES, visitTypeLpch));
    result.addResultSet(Stride7DatabaseDownload.SHC_VISIT_DE_VISIT_TYPE, Stride6TestDataSource.getTable(Stride7VisitTypeRecord.COLUMN_NAMES, visitTypeShc));

    result.addResultSet(Stride7DatabaseDownload.SHC_VISIT_APPT_DE, Stride6TestDataSource.getTable(Stride7DepartmentRecord.COLUMN_NAMES, departmentShc2));
    result.addResultSet(Stride7DatabaseDownload.LPCH_VISIT_APPT_DE, Stride6TestDataSource.getTable(Stride7DepartmentRecord.COLUMN_NAMES, departmentLpch2));

    result.addResultSet(Stride7DatabaseDownload.SHC_DX_ADMIT_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitAdmitShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_DX_ADMIT_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitAdmitLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_DX_EXTINJURY_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitExtInjuryShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_DX_EXTINJURY_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitExtInjuryLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_DX_HL7_DE, Stride6TestDataSource.getTable(Stride7VisitHl7Record.COLUMN_NAMES, visitDxHl7Shc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_DX_HL7_DE, Stride6TestDataSource.getTable(Stride7VisitHl7Record.COLUMN_NAMES, visitDxHl7Lpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_DX_HSP_ACCT_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitDxHspAcctShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_DX_HSP_ACCT_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitDxHspAcctLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_DX_PAT_ENC_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitDxPatEncShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_DX_PAT_ENC_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitDxPatEncLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_DX_PROB_LIST_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitDxProbListShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_DX_PROB_LIST_DE, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitDxProbListLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxCptDeShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxCptDeLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_HSP_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxCptHspShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_HSP_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxCptHspLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxCptProvShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxCptProvLpch));

    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE_ICD1, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvLpch1));
    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE_ICD1, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvShc1));

    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE_ICD2, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvLpch2));
    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE_ICD2, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvShc2));

    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE_ICD3, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvLpch3));
    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE_ICD3, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvShc3));

    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE_ICD4, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvLpch4));
    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE_ICD4, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvShc4));

    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE_ICD5, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvLpch5));
    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE_ICD5, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvShc5));

    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_CPT_PROV_DE_ICD6, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvLpch6));
    result.addResultSet(Stride7DatabaseDownload.SHC_PX_CPT_PROV_DE_ICD6, Stride6TestDataSource.getTable(Stride7VisitRecord.COLUMN_NAMES, visitPxCptProvShc6));

    result.addResultSet(Stride7DatabaseDownload.SHC_PX_ICD_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxIcdDeShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_ICD_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxIcdDeLpch));

    result.addResultSet(Stride7DatabaseDownload.SHC_PX_ICD_HSP_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxIcdHspShc));
    result.addResultSet(Stride7DatabaseDownload.LPCH_PX_ICD_HSP_DE, Stride6TestDataSource.getTable(Stride7VisitWithSabRecord.COLUMN_NAMES, visitPxIcdHspLpch));

    result.addResultSet(Stride7DatabaseDownload.MED_LPCH_DE, Stride6TestDataSource.getTable(Stride7DrugRecord.COLUMN_NAMES, medLpch));
    result.addResultSet(Stride7DatabaseDownload.MED_SHC_DE, Stride6TestDataSource.getTable(Stride7DrugRecord.COLUMN_NAMES, medShc));

    result.addResultSet(Stride7DatabaseDownload.MED_LPCH_HL7, Stride6TestDataSource.getTable(Stride7DrugRecord.COLUMN_NAMES, medHl7Lpch));
    result.addResultSet(Stride7DatabaseDownload.MED_SHC_HL7, Stride6TestDataSource.getTable(Stride7DrugRecord.COLUMN_NAMES, medHl7Shc));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DEPARTMENT_LPCH, generateDictionary(departmentMapLpch, DEPARTMENT_MAP_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DEPARTMENT_SHC, generateDictionary(departmentMapShc, DEPARTMENT_MAP_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_LPCH, generateDictionaryDiagnoses(dxLpch, 0, DX_ID_ICD9_MAP_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_SHC, generateDictionaryDiagnoses(dxShc, 0, DX_ID_ICD9_MAP_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_LPCH_ICD10, generateDictionaryDiagnoses(dxLpch, 1, DX_ID_ICD10_MAP_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_SHC_ICD10, generateDictionaryDiagnoses(dxShc, 1, DX_ID_ICD10_MAP_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_LPCH_PRIMARY_CODE_9, generateDictionaryDiagnoses(dxLpch, 2, DX_ID_ICD10_PRIMARY_MAP_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_SHC_PRIMARY_CODE_9, generateDictionaryDiagnoses(dxShc, 2, DX_ID_ICD10_PRIMARY_MAP_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_LPCH_PRIMARY_CODE_10, generateDictionaryDiagnoses(dxLpch, 3, DX_ID_ICD10_PRIMARY_MAP_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_DIAGNOSES_SHC_PRIMARY_CODE_10, generateDictionaryDiagnoses(dxShc, 3, DX_ID_ICD10_PRIMARY_MAP_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_VISIT_TYPE_LPCH, generateDictionary(visitTypeMapLpch, VISIT_TYPE_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_VISIT_TYPE_SHC, generateDictionary(visitTypeMapShc, VISIT_TYPE_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.MED_ID_LPCH_EPIC, generateMedicationDictionary(medicationIdToRxNormLpch, MED_ID_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.MED_ID_SHC_EPIC, generateMedicationDictionary(medicationIdToRxNormShc, MED_ID_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.MED_ID_HL7, generateMedicationDictionary(medicationIdToRxNormHl7, MED_ID_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.MED_STATUS_LPCH, generateMedicationDictionary(medStatusLpch, MED_STATUS_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.MED_STATUS_SHC, generateMedicationDictionary(medStatusShc, MED_STATUS_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.MED_CLASS_LPCH, generateMedicationDictionary(medClassLpch, MED_CLASS_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.MED_CLASS_SHC, generateMedicationDictionary(medClassShc, MED_CLASS_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.MED_ROUTE_LPCH, generateMedicationDictionary(medRouteShc, MED_ROUTE_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.MED_ROUTE_SHC, generateMedicationDictionary(medRouteShc, MED_ROUTE_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.DICTIONARY_LOINC, generateMedicationDictionary(loincTexts, LOINC_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.TERM_DICTIONARY, generateDictionary(termDictionary, TERM_DICTIONARY_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.CULTURE_DICTIONARY, Stride6TestDataSource.getTable(Stride7CultureDictionaryRecord.COLUMN_NAMES, cultureDictionary));
    result.addResultSet(Stride7DatabaseDownload.SHC_LAB_CULTURE, Stride6TestDataSource.getTable(Stride7CultureRecord.COLUMN_NAMES, culturesShc));
    result.addResultSet(Stride7DatabaseDownload.CULTURE_NAME_DICTIONARY, generateDictionary(cultureNameDictionary, CULTURE_NAME_DICTIONARY_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.MED_ADMIN_LPCH, generateAdmin(adminLpch, ADMIN_COLUMNS));
    result.addResultSet(Stride7DatabaseDownload.MED_ADMIN_SHC, generateAdmin(adminShc, ADMIN_COLUMNS));

    result.addResultSet(Stride7DatabaseDownload.NOTES_CLINICAL, Stride6TestDataSource.getTable(Stride7NoteRecord.COLUMN_NAMES, notesMaster));

    result.addResultSet(Stride7DatabaseDownload.NOTES_PATHOLOGY, new MockDataTable(Stride7NoteRecord.COLUMN_NAMES));

    result.addResultSet(Stride7DatabaseDownload.NOTES_RADIOLOGY, new MockDataTable(Stride7NoteRecord.COLUMN_NAMES));

    result.addResultSet(Commands.getTermsQuery(Stride7DatabaseDownload.TERMS, 0, "patient_id"), Stride6TestDataSource.getTable(Stride7TermMentionRecord.COLUMN_NAMES,
        termMentions));

    result.addResultSet(Stride7DatabaseDownload.LPCH_LAB_DE, Stride6TestDataSource.getTable(Stride7LabDeRecord.COLUMN_NAMES, labsDeLpch));

    result.addResultSet(Stride7DatabaseDownload.LPCH_LAB_EPIC_DE, Stride6TestDataSource.getTable(Stride7LabEpicRecord.COLUMN_NAMES, labsEpicLpch));
    result.addResultSet(Stride7DatabaseDownload.SHC_LAB_EPIC_DE, Stride6TestDataSource.getTable(Stride7LabEpicRecord.COLUMN_NAMES, labsEpicShc));

    return result;
  }

}
