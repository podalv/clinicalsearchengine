package com.podalv.extractor.datastructures;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.utils.file.FileUtils;

public class DatabaseExtractionSource implements ExtractionSource {

  private final ResultSet set;

  public DatabaseExtractionSource(final ResultSet set) {
    this.set = set;
  }

  @Override
  public String getString(final int columnNr) throws SQLException {
    return set.getString(columnNr);
  }

  @Override
  public int getInt(final int columnNr) throws SQLException {
    return set.getInt(columnNr);
  }

  @Override
  public long getLong(final int columnNr) throws SQLException {
    return set.getLong(columnNr);
  }

  @Override
  public double getDouble(final int columnNr) throws SQLException {
    return set.getDouble(columnNr);
  }

  @Override
  public boolean next() throws SQLException {
    return set.next();
  }

  @Override
  public boolean isAfterLast() throws SQLException {
    return set.isAfterLast();
  }

  @Override
  public void close() {
    FileUtils.close(set);
  }

  @Override
  public int getByte(final int columnNr) throws Exception {
    return set.getByte(columnNr);
  }

  @Override
  public int getShort(final int columnNr) throws Exception {
    return set.getShort(columnNr);
  }

}
