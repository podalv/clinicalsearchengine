package com.podalv.extractor.datastructures;

import java.io.Closeable;

public interface Extractor<S> extends Closeable {

  public boolean hasNext();

  public S next();
}
