package com.podalv.extractor.datastructures;

/** A single extraction option that contains:
 *  char (which character triggers this option from the Extractor parameters)
 *  String description
 *  boolean actual parsed value
 *
 * @author podalv
 *
 */
class ExtractionOption {

  private final char       trigger;
  private final String     description;
  private boolean          value;
  private final IDENTIFIER identifier;

  static enum IDENTIFIER {
    TERMS, CODES, UMLS
  };

  ExtractionOption(final char trigger, final IDENTIFIER identifier, final String description) {
    this.trigger = trigger;
    this.description = description;
    this.identifier = identifier;
  }

  public IDENTIFIER getIdentifier() {
    return identifier;
  }

  public String getDescription() {
    return description;
  }

  public char getTrigger() {
    return trigger;
  }

  public void setValue(final boolean value) {
    this.value = value;
  }

  public boolean getValue() {
    return value;
  }
}
