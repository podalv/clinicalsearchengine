package com.podalv.extractor.datastructures;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import com.podalv.maps.primitive.list.LongArrayList;
import com.podalv.utils.arrays.ArrayUtils;
import com.podalv.utils.file.FileUtils;

public class BinaryFileExtractionSource implements ExtractionSource {

  private final DataInputStream   source;
  private final DATA_TYPE[]       types;
  private boolean                 afterLast    = false;
  private final ArrayList<Object> row          = new ArrayList<Object>();
  private long                    currentRowId = 0;
  private long[]                  nextTypeChange;
  private int[]                   currentType;
  private String[][]              dictionary;
  private LongArrayList[]         typeChange;

  public static enum DATA_TYPE {
    DOUBLE, STRING, INT, SHORT, BYTE, LONG
  };

  public BinaryFileExtractionSource(final File data, final File dictionary, final DATA_TYPE ... types) throws IOException {
    this.types = types;
    for (int x = 0; x < types.length; x++) {
      row.add(null);
    }
    if (data == null || dictionary == null) {
      afterLast = true;
      this.source = null;
    }
    else {
      this.source = new DataInputStream(new BufferedInputStream(new FileInputStream(data)));
      readDictionary(dictionary);
    }
  }

  public BinaryFileExtractionSource(final InputStream stream, final InputStream dictionaryStream, final DATA_TYPE ... types) throws IOException {
    this.types = types;
    for (int x = 0; x < types.length; x++) {
      row.add(null);
    }
    if (stream == null || dictionaryStream == null) {
      afterLast = true;
      this.source = null;
    }
    else {
      this.source = new DataInputStream(new BufferedInputStream(stream));
      readDictionary(dictionaryStream);
    }
  }

  private void readDictionary(final File file) throws IOException {
    BufferedInputStream stream = null;
    try {
      stream = new BufferedInputStream(new FileInputStream(file));
      readDictionary(stream);
    }
    finally {
      FileUtils.close(stream);
    }
  }

  private void readDictionary(final InputStream dictionaryStream) throws IOException {
    final DataInputStream s = new DataInputStream(new BufferedInputStream(dictionaryStream));
    int len = s.readInt();
    dictionary = new String[len][];
    typeChange = new LongArrayList[len];
    nextTypeChange = new long[len];
    Arrays.fill(nextTypeChange, -1);
    currentType = new int[len];
    final int actualLength = s.readInt();
    for (int x = 0; x < actualLength; x++) {
      final int id = s.readInt();
      typeChange[id] = new LongArrayList();
      len = s.readInt();
      for (int y = 0; y < len; y++) {
        typeChange[id].add(s.readLong());
      }
      len = s.readInt();
      dictionary[id] = new String[len + 1];
      dictionary[id][0] = s.readUTF();
      for (int y = 0; y < len; y++) {
        dictionary[id][y + 1] = s.readUTF();
      }
    }
    for (int x = 0; x < typeChange.length; x++) {
      if (typeChange[x] != null && typeChange[x].size() != 0) {
        nextTypeChange[x] = typeChange[x].get(0);
      }
    }
  }

  private String readString(final int column) throws IOException {
    if (nextTypeChange[column] == currentRowId) {
      currentType[column]++;
      for (int x = 0; x < typeChange[column].size(); x++) {
        if (typeChange[column].get(x) > nextTypeChange[column]) {
          nextTypeChange[column] = typeChange[column].get(x);
          break;
        }
      }
    }
    int index = -1;
    if (currentType[column] == 0) {
      index = ArrayUtils.convertByteArrayToUnsignedByte(source.readByte());
    }
    else if (currentType[column] == 1) {
      final byte[] v = new byte[2];
      source.readFully(v);
      index = ArrayUtils.convertByteArrayToUnsignedShort(v, 0);
    }
    else if (currentType[column] == 2) {
      final byte[] v = new byte[3];
      source.readFully(v);
      index = ArrayUtils.convertByteArrayToTriByte(v, 0);
    }
    else {
      index = source.readInt();
    }
    return index == 0 ? null : dictionary[column][index];
  }

  private Object readNextValue(final int column) throws IOException {
    if (types[column] == DATA_TYPE.DOUBLE) {
      return Double.valueOf(source.readDouble());
    }
    if (types[column] == DATA_TYPE.STRING) {
      return readString(column);
    }
    if (types[column] == DATA_TYPE.INT) {
      return source.readInt();
    }
    if (types[column] == DATA_TYPE.LONG) {
      return source.readLong();
    }
    if (types[column] == DATA_TYPE.SHORT) {
      return source.readShort();
    }
    if (types[column] == DATA_TYPE.BYTE) {
      return source.readByte();
    }
    throw new IOException("Invalid read type");
  }

  private boolean readCurrentRow() {
    if (!afterLast) {
      try {
        for (int x = 0; x < types.length; x++) {
          row.set(x, readNextValue(x));
        }
        currentRowId++;
        return true;
      }
      catch (final EOFException ee) {
        afterLast = true;
        // No need to report the end, it is a common event
      }
      catch (final NullPointerException eee) {
        System.out.println("No data found");
      }
      catch (final Exception e) {
        e.printStackTrace();
        afterLast = true;
        return false;
      }
    }
    return false;
  }

  @Override
  public String getString(final int columnNr) throws Exception {
    if (types[columnNr - 1] == DATA_TYPE.STRING) {
      return (String) row.get(columnNr - 1);
    }
    else if (types[columnNr - 1] == DATA_TYPE.BYTE) {
      return ((Byte) row.get(columnNr - 1)).byteValue() + "";
    }
    else if (types[columnNr - 1] == DATA_TYPE.INT) {
      return ((Integer) row.get(columnNr - 1)).intValue() + "";
    }
    else if (types[columnNr - 1] == DATA_TYPE.LONG) {
      return ((Long) row.get(columnNr - 1)).longValue() + "";
    }
    else if (types[columnNr - 1] == DATA_TYPE.SHORT) {
      return ((Short) row.get(columnNr - 1)).shortValue() + "";
    }
    else if (types[columnNr - 1] == DATA_TYPE.DOUBLE) {
      return ((Double) row.get(columnNr - 1)).doubleValue() + "";
    }
    throw new UnsupportedOperationException("Cannot convert " + types[columnNr - 1] + " to String");
  }

  @Override
  public int getInt(final int columnNr) throws Exception {
    return (int) row.get(columnNr - 1);
  }

  @Override
  public long getLong(final int columnNr) throws Exception {
    return (long) row.get(columnNr - 1);
  }

  @Override
  public double getDouble(final int columnNr) throws Exception {
    return (double) row.get(columnNr - 1);
  }

  @Override
  public boolean next() throws Exception {
    return readCurrentRow();
  }

  @Override
  public boolean isAfterLast() throws Exception {
    return afterLast;
  }

  @Override
  public void close() {
    FileUtils.close(source);
  }

  @Override
  public int getByte(final int columnNr) throws Exception {
    return (byte) row.get(columnNr - 1);
  }

  @Override
  public int getShort(final int columnNr) throws Exception {
    return (short) row.get(columnNr - 1);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder();
    for (final Object obj : row) {
      if (obj != null) {
        result.append(obj.toString() + ", ");
      }
      else {
        result.append("NULL, ");
      }
    }
    return result.toString();
  }
}
