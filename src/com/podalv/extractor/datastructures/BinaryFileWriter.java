package com.podalv.extractor.datastructures;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;
import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.LongArrayList;
import com.podalv.maps.string.StringKeyIntReversedMap;
import com.podalv.maps.string.Utf8String;
import com.podalv.utils.arrays.ArrayUtils;

/** Encapsulates the database download binary file
 *  Binary file consists of a dictionary (String to ID) and the binary output stream
 *
 *
 * @author podalv
 *
 */
public class BinaryFileWriter implements Closeable {

  private DataOutputStream          stream           = null;
  private DataOutputStream          dictionaryStream = null;
  private final File                outputFile;
  private StringKeyIntReversedMap[] dictionary;
  private LongArrayList[]           typeChange;
  private int[]                     currentId;
  private final DATA_TYPE[]         structure;
  private long                      currentRowId     = 0;

  public BinaryFileWriter(final File file, final DATA_TYPE[] structure) {
    initDictionary(structure);
    this.structure = structure;
    this.outputFile = file;
  }

  public BinaryFileWriter(final OutputStream output, final OutputStream dictionary, final DATA_TYPE[] structure) {
    initDictionary(structure);
    this.structure = structure;
    this.outputFile = null;
    stream = new DataOutputStream(output);
    dictionaryStream = new DataOutputStream(dictionary);
  }

  public DATA_TYPE[] getStructure() {
    return structure;
  }

  private int getLastStringId(final DATA_TYPE[] structure) {
    int lastStringId = -1;
    for (int x = 0; x < structure.length; x++) {
      if (structure[x] == DATA_TYPE.STRING) {
        lastStringId = x;
      }
    }
    return lastStringId;
  }

  private void initDictionary(final DATA_TYPE[] structure) {
    final int lastId = getLastStringId(structure) + 1;
    dictionary = new StringKeyIntReversedMap[lastId];
    typeChange = new LongArrayList[lastId];
    currentId = new int[lastId];
    for (int x = 0; x < lastId; x++) {
      if (structure[x] == DATA_TYPE.STRING) {
        dictionary[x] = new StringKeyIntReversedMap();
        typeChange[x] = new LongArrayList();
        currentId[x] = 1;
      }
    }
  }

  private void initFile() throws FileNotFoundException {
    if (stream == null) {
      stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile)));
    }
  }

  private int getStringIndex(final int index, final String str) {
    if (dictionary[index].containsKey(str)) {
      return dictionary[index].get(str);
    }
    final int newId = currentId[index]++;
    dictionary[index].put(str, newId);
    return newId;
  }

  private void writeString(final int index, final String value) throws IOException {
    final int stringIndex = value == null ? 0 : getStringIndex(index, value);
    if ((stringIndex > 255 && typeChange[index].size() == 0) || (stringIndex > 65535 && typeChange[index].size() == 1)) {
      typeChange[index].add(currentRowId);
    }
    if (typeChange[index].size() == 0) {
      stream.writeByte(ArrayUtils.convertByteArrayToUnsignedByte((byte) stringIndex));
    }
    else if (typeChange[index].size() == 1) {
      stream.write(ArrayUtils.convertUnsignedShortToByteArray(stringIndex));
    }
    else if (typeChange[index].size() == 2) {
      stream.write(ArrayUtils.convertTriByteToByteArray(stringIndex));
    }
    else {
      stream.writeInt(stringIndex);
    }
  }

  private long getValue(final Object obj) {
    if (obj instanceof Integer) {
      return ((Integer) obj).intValue();
    }
    else if (obj instanceof Short) {
      return ((Short) obj).shortValue();
    }
    else if (obj instanceof Byte) {
      return ((Byte) obj).byteValue();
    }
    else if (obj instanceof Long) {
      return ((Long) obj).longValue();
    }
    else if (obj instanceof String) {
      return Integer.parseInt((String) obj);
    }
    throw new UnsupportedOperationException("Integer data type conversion is not supported for " + obj.getClass());
  }

  private double getDoubleValue(final Object obj) {
    if (obj instanceof Double) {
      return ((Double) obj).doubleValue();
    }
    else if (obj instanceof Float) {
      return ((Float) obj).floatValue();
    }
    else if (obj instanceof Integer) {
      return ((Integer) obj).intValue();
    }
    else if (obj instanceof Short) {
      return ((Short) obj).shortValue();
    }
    throw new UnsupportedOperationException("Double/Float data type conversion is not supported for " + obj.getClass());
  }

  private long getLongValue(final Object obj) {
    if (obj instanceof Integer) {
      return ((Integer) obj).intValue();
    }
    else if (obj instanceof Long) {
      return ((Long) obj).longValue();
    }
    throw new UnsupportedOperationException("Double/Float data type conversion is not supported for " + obj.getClass());
  }

  public void write(final Object ... values) throws IOException {
    initFile();
    //final StringBuilder line = new StringBuilder();
    for (int x = 0; x < structure.length; x++) {
      //line.append(values[x] + ", ");
      if (structure[x] == DATA_TYPE.BYTE) {
        stream.writeByte((int) getValue(values[x]));
      }
      else if (structure[x] == DATA_TYPE.DOUBLE) {
        stream.writeDouble(getDoubleValue(values[x]));
      }
      else if (structure[x] == DATA_TYPE.LONG) {
        stream.writeLong(getLongValue(values[x]));
      }
      else if (structure[x] == DATA_TYPE.INT) {
        stream.writeInt((int) getValue(values[x]));
      }
      else if (structure[x] == DATA_TYPE.SHORT) {
        stream.writeShort((int) getValue(values[x]));
      }
      else if (structure[x] == DATA_TYPE.STRING) {
        writeString(x, (String) values[x]);
      }
      else {
        throw new UnsupportedOperationException("Unsupported data type");
      }
    }
    //System.out.println(line.toString());
    currentRowId++;
  }

  private void writeDictionary() throws FileNotFoundException, IOException {
    if (this.dictionaryStream == null) {
      dictionaryStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(Common.getDictFile(outputFile))));
    }
    dictionaryStream.writeInt(dictionary.length);
    int actualLength = 0;
    for (int x = 0; x < dictionary.length; x++) {
      if (dictionary[x] != null) {
        actualLength++;
      }
    }
    dictionaryStream.writeInt(actualLength);
    for (int x = 0; x < dictionary.length; x++) {
      if (dictionary[x] != null) {
        dictionaryStream.writeInt(x);
        dictionaryStream.writeInt(typeChange[x].size());
        for (int y = 0; y < typeChange[x].size(); y++) {
          dictionaryStream.writeLong(typeChange[x].get(y));
        }
        dictionaryStream.writeInt(dictionary[x].size());
        dictionaryStream.writeUTF("");
        for (int y = 1; y < dictionary[x].size() + 1; y++) {
          final Utf8String utf = dictionary[x].get(y);
          dictionaryStream.writeUTF(utf == null ? "" : utf.toString());
        }
      }
    }
    dictionaryStream.close();
  }

  @Override
  public void close() throws IOException {
    writeDictionary();
    if (stream != null) {
      stream.close();
    }
  }

}