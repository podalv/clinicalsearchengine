package com.podalv.extractor.datastructures.records;

import com.podalv.utils.text.TextUtils;

public class ObservationRecord extends VisitRecord {

  public ObservationRecord(final String code, final int age, final int year, final String sab) {
    super(-1, year, age, 0, sab, code, "");
  }

  @Override
  public Integer getAge() {
    return age;
  }

  @Override
  public String getSab() {
    return sab;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public Integer getYear() {
    return year;
  }

  @Override
  public int hashCode() {
    return age;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof VisitRecord) {
      final VisitRecord o = (VisitRecord) obj;
      return TextUtils.compareStrings(code, o.getCode()) && //
          Integer.compare(age, o.getAge()) == 0 && //
          Integer.compare(year, o.getYear()) == 0 && //
          TextUtils.compareStrings(sab, o.getSab()) && isValid() == o.isValid();
    }
    return false;
  }

}
