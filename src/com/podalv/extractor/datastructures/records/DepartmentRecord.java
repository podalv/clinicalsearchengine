package com.podalv.extractor.datastructures.records;

public class DepartmentRecord extends EventRecord<DepartmentRecord> implements Comparable<DepartmentRecord> {

  private final String  department;
  private final Integer age;
  private final int     year;

  public DepartmentRecord(final String department, final Integer age, final int year) {
    this.department = department;
    this.age = age;
    this.year = year;
  }

  public Integer getAge() {
    return age;
  }

  public String getDepartment() {
    return department;
  }

  public Integer getYear() {
    return year;
  }

  @Override
  public int hashCode() {
    return department.hashCode();
  }

  @Override
  public DepartmentRecord getDeepCopy() {
    final DepartmentRecord result = new DepartmentRecord(department, age, year);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  private boolean agesEqual(final Integer age1, final Integer age2) {
    return (age1 == null && age2 == null) || (age1 != null && age2 != null && age1.equals(age2));
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof DepartmentRecord) {
      final DepartmentRecord dept = (DepartmentRecord) obj;
      return (dept.department.equals(department) && (agesEqual(dept.age, age)) && (dept.year == year) && dept.isValid() == isValid());
    }
    return false;
  }

  @Override
  public int compareTo(final DepartmentRecord o) {
    if (!isValid()) {
      if (!o.isValid()) {
        return 0;
      }
      return -1;
    }
    if (!o.isValid()) {
      return 1;
    }
    if (age == null || o.age == null) {
      return 0;
    }
    final int result = Integer.compare(age, o.age);
    return result == 0 ? department.compareTo(o.department) : result;
  }
}