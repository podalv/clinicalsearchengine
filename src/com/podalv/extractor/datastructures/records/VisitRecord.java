package com.podalv.extractor.datastructures.records;

import com.podalv.extractor.stride6.AtlasDatabaseDownload;
import com.podalv.utils.text.TextUtils;

public class VisitRecord extends EventRecord<VisitRecord> implements Comparable<VisitRecord> {

  final Integer        year;
  Integer              age;
  private final String src_visit;
  private int          duration;
  String               code;
  private int          codeId;
  final String         sab;
  private int          sourceCode;
  private final int    visitId;
  private boolean      primary              = false;
  private boolean      generatedByHierarchy = false;

  public VisitRecord(final int visitId, final Integer year, final Integer age, final int duration, final String sab, final String code, final String src_visit) {
    this.visitId = visitId;
    this.year = year;
    this.age = age;
    this.duration = duration;
    this.sab = sab;
    this.code = code;
    this.codeId = -1;
    this.src_visit = src_visit;
  }

  public VisitRecord(final int visitId, final Integer year, final Integer age, final int duration, final String sab, final int codeId, final String src_visit) {
    this.visitId = visitId;
    this.year = year;
    this.age = age;
    this.duration = duration;
    this.sab = sab;
    this.code = null;
    this.codeId = codeId;
    this.src_visit = src_visit;
  }

  public boolean isGeneratedByHierarchy() {
    return generatedByHierarchy;
  }

  public VisitRecord setGeneratedByHierarchy(final boolean value) {
    this.generatedByHierarchy = value;
    return this;
  }

  private VisitRecord(final int visitId, final Integer year, final Integer age, final int duration, final String sab, final int codeId, final String code, final String src_visit,
      final int sourceCode, final boolean primary) {
    this.src_visit = src_visit;
    this.age = age;
    this.code = code;
    this.codeId = codeId;
    this.duration = duration;
    this.primary = primary;
    this.sab = sab;
    this.sourceCode = sourceCode;
    this.visitId = visitId;
    this.year = year;
  }

  public VisitRecord copy(final String newCode, final int newCodeId) {
    final VisitRecord result = new VisitRecord(visitId, year, age, duration, sab, newCodeId, newCode, src_visit, sourceCode, primary);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public void setCodeId(final int codeId) {
    this.codeId = codeId;
  }

  @Override
  public VisitRecord getDeepCopy() {
    final VisitRecord result = new VisitRecord(visitId, year, age, duration, sab, codeId, code, src_visit, sourceCode, primary);
    result.generatedByHierarchy = generatedByHierarchy;
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public void setAge(final int age) {
    this.age = age;
  }

  public void setSourceCode(final int sourceCode) {
    this.sourceCode = sourceCode;
  }

  public int getSourceCode() {
    return sourceCode;
  }

  public VisitRecord setPrimary(final boolean primary) {
    this.primary = primary;
    return this;
  }

  public boolean isPrimary() {
    return primary;
  }

  public int getVisitId() {
    return visitId;
  }

  public void setDuration(final int duration) {
    this.duration = duration;
  }

  public Integer getAge() {
    return age;
  }

  public int getCodeId() {
    return codeId;
  }

  public String getCode() {
    return code;
  }

  public int getDuration() {
    return duration;
  }

  public String getSab() {
    return sab;
  }

  public String getSrc_visit() {
    return src_visit;
  }

  public Integer getYear() {
    return year;
  }

  public boolean isIcd9Code() {
    return (sab != null && (sab.equals("ICD") || sab.equals(AtlasDatabaseDownload.CODE_TYPE_ICD9) || sab.equals(AtlasDatabaseDownload.CODE_TYPE_ICD9_PRIMARY) || sab.equals("DX_ID")
        || (sab.equalsIgnoreCase("BILLING") && code != null && (code.length() < 4 || code.indexOf('.') != -1))));
  }

  public boolean isIcd10Code() {
    return (sab != null && (sab.equals(AtlasDatabaseDownload.CODE_TYPE_ICD10) || sab.equals(AtlasDatabaseDownload.CODE_TYPE_ICD10_PRIMARY)));
  }

  public boolean isCptCode() {
    return (sab != null && (sab.equals("CPT") || (sab.equalsIgnoreCase("BILLING") && code != null && (code.length() == 5 && code.indexOf('.') == -1)) || (sab.equalsIgnoreCase(
        "IDX") && isValidHcpCsCode(code))));
  }

  private boolean isValidHcpCsCode(final String code) {
    return (code.length() == 5 && Character.isLetter(code.charAt(0)) && Character.isDigit(code.charAt(1)) && Character.isDigit(code.charAt(2)) && Character.isDigit(code.charAt(3))
        && Character.isDigit(code.charAt(4)));
  }

  @Override
  public int hashCode() {
    return age == null ? 0 : age;
  }

  private boolean isSameCode(final VisitRecord o) {
    if (code == null && o.code == null) {
      return Integer.compare(codeId, o.codeId) == 0;
    }
    if (code != null && o.code != null) {
      return TextUtils.compareStrings(code, o.code);
    }
    if (codeId != -1 && o.codeId != -1) {
      return Integer.compare(codeId, o.codeId) == 0;
    }
    return TextUtils.compareStrings(code, o.code);
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof VisitRecord) {
      final VisitRecord o = (VisitRecord) obj;
      return Integer.compare(age == null ? 0 : age, o.age == null ? 0 : o.age) == 0 && //
          Integer.compare(duration, o.duration) == 0 && //
          Integer.compare(year == null ? 0 : year, o.year == null ? 0 : o.year) == 0 && //
          isSameCode(o) && //
          Integer.compare(sourceCode, o.sourceCode) == 0 && //
          primary == o.primary && //
          generatedByHierarchy == o.generatedByHierarchy && //
          visitId == o.visitId && //
          TextUtils.compareStrings(src_visit, o.src_visit) && //
          TextUtils.compareStrings(sab, o.sab) && //
          o.isValid() == isValid();
    }
    return false;
  }

  @Override
  public String toString() {
    return visitId + "|" + age + "|" + duration + "|" + year + "|" + code + "|" + codeId + "|" + sab + "|" + sourceCode + "|" + primary;
  }

  @Override
  public int compareTo(final VisitRecord o) {
    if (!isValid()) {
      if (!o.isValid()) {
        return 0;
      }
      return -1;
    }
    if (!o.isValid()) {
      return 1;
    }
    if (age.equals(o.age)) {
      return Integer.compare(duration, o.duration);
    }
    return Integer.compare(age, o.age);
  }

}
