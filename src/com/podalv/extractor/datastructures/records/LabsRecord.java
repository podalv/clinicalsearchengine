package com.podalv.extractor.datastructures.records;

import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;
import com.podalv.utils.text.TextUtils;

public class LabsRecord extends EventRecord<LabsRecord> implements Comparable<LabsRecord> {

  public static final String HIGH      = "HIGH";
  public static final String LOW       = "LOW";
  public static final String NORMAL    = "NORMAL";
  public static final String UNDEFINED = "UNDEFINED";
  public static final String NO_VALUE  = "NO VALUE";
  public static final String REMOVED   = "REMOVED";
  private final double       low;
  private final double       high;
  private final double       value;
  private String             code;
  private final String       calculatedValue;
  private int                time;
  private final int          year;

  public LabsRecord(final String code, final int time, final int year, final String calculatedValue) {
    this.code = code;
    this.time = time;
    this.year = year;
    low = -1;
    high = -1;
    value = Stride6DatabaseDownload.UNDEFINED;
    this.calculatedValue = calculatedValue == null ? null : calculatedValue.trim();
  }

  public LabsRecord(final String code, final int time, final int year, final String calculatedValue, final double value) {
    this.code = code;
    this.time = time;
    this.year = year;
    low = -1;
    high = -1;
    this.value = value;
    this.calculatedValue = calculatedValue == null ? null : calculatedValue.trim();
  }

  public LabsRecord(final String code, final int time, final double value, final double low, final double high, final String calculatedValue) {
    this.low = low;
    this.code = code;
    this.high = high;
    this.value = value;
    this.time = time;
    this.calculatedValue = calculatedValue == null ? null : calculatedValue.trim();
    this.year = Common.MISSING_VALUE;
  }

  LabsRecord(final String code, final int time, final double value, final double low, final double high, final int year, final String calculatedValue) {
    this.low = low;
    this.code = code;
    this.high = high;
    this.value = value;
    this.time = time;
    this.calculatedValue = calculatedValue;
    this.year = year;
  }

  public void setTime(final int time) {
    this.time = time;
  }

  @Override
  public LabsRecord getDeepCopy() {
    final LabsRecord result = new LabsRecord(code, time, value, low, high, year, calculatedValue);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public int getYear() {
    return year;
  }

  public int getTime() {
    return time;
  }

  public boolean isEmpty() {
    return (calculatedValue == null || calculatedValue.trim().isEmpty()) && value <= Stride6DatabaseDownload.UNDEFINED;
  }

  public boolean hasCalculatedValue() {
    return calculatedValue != null && !calculatedValue.trim().isEmpty() && !calculatedValue.equals(UNDEFINED);
  }

  public String getCalculatedValue() {
    return (calculatedValue == null || calculatedValue.trim().isEmpty()) && value <= Stride6DatabaseDownload.UNDEFINED ? NO_VALUE : calculatedValue;
  }

  @Override
  public int hashCode() {
    return code.hashCode();
  }

  public double getValue() {
    return value;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof LabsRecord) {
      final LabsRecord rec = (LabsRecord) obj;
      return TextUtils.compareStrings(rec.getCalculatedValue(), getCalculatedValue()) && //
          TextUtils.compareStrings(rec.code, code) && //
          Double.compare(rec.high, high) == 0 && //
          Double.compare(rec.low, low) == 0 && //
          Double.compare(rec.value, value) == 0 && //
          rec.time == time && //
          rec.year == year && //
          rec.isValid() == isValid();
    }
    return false;
  }

  @Override
  public int compareTo(final LabsRecord o) {
    if (!isValid()) {
      if (!o.isValid()) {
        return 0;
      }
      return -1;
    }
    if (!o.isValid()) {
      return 1;
    }
    if (code == null && o.code == null) {
      return 0;
    }
    if (code == null && o.code != null) {
      return -1;
    }
    if (code != null && o.code == null) {
      return 1;
    }
    return code.compareTo(o.code);
  }

}