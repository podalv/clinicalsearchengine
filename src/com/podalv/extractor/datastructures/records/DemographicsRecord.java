package com.podalv.extractor.datastructures.records;

public class DemographicsRecord extends EventRecord<DemographicsRecord> {

  private String       gender;
  private final String race;
  private final String ethnicity;
  private final double deathInDays;
  private final int    patientId;
  private final int    year;

  public DemographicsRecord(final int patientId, final String gender, final String race, final String ethnicity, final double deathInDays, final int deathYear) {
    this.gender = gender;
    this.race = race;
    this.ethnicity = ethnicity;
    this.deathInDays = deathInDays;
    this.patientId = patientId;
    this.year = deathYear;
  }

  public int getDeathYear() {
    return year;
  }

  public int getPatientId() {
    return patientId;
  }

  public void setGender(final String gender) {
    this.gender = gender;
  }

  public double getDeathInDays() {
    return deathInDays;
  }

  public String getEthnicity() {
    return ethnicity;
  }

  public String getGender() {
    return gender;
  }

  public String getRace() {
    return race;
  }

  @Override
  public DemographicsRecord getDeepCopy() {
    final DemographicsRecord result = new DemographicsRecord(patientId, gender, race, ethnicity, deathInDays, year);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }
}
