package com.podalv.extractor.datastructures.records;

import java.util.ArrayList;

public class PatientRecord<S> {

  private final int          patientId;
  private final ArrayList<S> records = new ArrayList<>();

  public PatientRecord(final int patientId) {
    this.patientId = patientId;
  }

  public void addAll(final ArrayList<S> record) {
    records.addAll(record);
  }

  public void add(final S record) {
    records.add(record);
  }

  public int getPatientId() {
    return patientId;
  }

  public ArrayList<S> getRecords() {
    return records;
  }
}
