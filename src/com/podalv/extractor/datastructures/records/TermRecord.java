package com.podalv.extractor.datastructures.records;

import com.podalv.extractor.stride6.Common;

/** Term records CANNOT have a different start and end date.
 *  They MUST be sorted by date and then by noteId.
 *
 *  These assumptions are take into consideration when evaluating expressions.
 *
 * @author podalv
 *
 */
public class TermRecord extends EventRecord<TermRecord> implements Comparable<TermRecord> {

  private final int termId;
  private final int noteId;
  private final int negated;
  private final int familyHistory;
  private int       year = Common.MISSING_VALUE;
  private int       age;
  private int       noteTypeId;

  public TermRecord(final int termId, final int noteId, final int negated, final int familyHistory, final int noteTypeId) {
    this.noteId = noteId;
    this.termId = termId;
    this.negated = negated;
    this.familyHistory = familyHistory;
    this.noteTypeId = noteTypeId;
  }

  @Override
  public TermRecord getDeepCopy() {
    final TermRecord result = new TermRecord(termId, noteId, negated, familyHistory, noteTypeId);
    result.setAge(age);
    result.setNoteTypeId(noteTypeId);
    result.setYear(year);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public void setAge(final int age) {
    this.age = age;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public void setNoteTypeId(final int noteTypeId) {
    this.noteTypeId = noteTypeId;
  }

  public int getNoteTypeId() {
    return noteTypeId;
  }

  public int getNoteId() {
    return noteId;
  }

  public int getAge() {
    return age;
  }

  public int getFamilyHistory() {
    return familyHistory;
  }

  public int getNegated() {
    return negated;
  }

  public int getTermId() {
    return termId;
  }

  public int getYear() {
    return year;
  }

  @Override
  public int hashCode() {
    return noteId;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof TermRecord) {
      final TermRecord term = (TermRecord) obj;
      return (term.termId == termId) && (term.negated == negated) && (term.familyHistory == familyHistory) && (term.age == age) && (term.noteId == noteId)
          && (term.noteTypeId == noteTypeId) && term.isValid() == isValid();
    }
    return false;
  }

  @Override
  public int compareTo(final TermRecord o) {
    if (!isValid()) {
      if (!o.isValid()) {
        return 0;
      }
      return -1;
    }
    if (!o.isValid()) {
      return 1;
    }
    final int result = Integer.compare(age, o.age);
    return result == 0 ? Integer.compare(noteId, o.noteId) : result;
  }
}
