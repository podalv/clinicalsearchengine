package com.podalv.extractor.datastructures.records;

import com.podalv.extractor.stride6.Common;
import com.podalv.utils.text.TextUtils;

public class VitalsRecord extends EventRecord<VitalsRecord> implements Comparable<VitalsRecord> {

  private final int     age;
  private final String  description;
  private final double  value;
  private final Integer year;

  public VitalsRecord(final Integer age, final String description, final double value) {
    this.age = age;
    this.description = description;
    this.value = value;
    this.year = Common.MISSING_VALUE;
  }

  public VitalsRecord(final Integer age, final String description, final double value, final int year) {
    this.age = age;
    this.description = description;
    this.value = value;
    this.year = year;
  }

  @Override
  public VitalsRecord getDeepCopy() {
    final VitalsRecord result = new VitalsRecord(age, description, value, year);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public Integer getAge() {
    return age;
  }

  public int getYear() {
    return year;
  }

  public String getDescription() {
    return description;
  }

  public double getValue() {
    return value;
  }

  @Override
  public int hashCode() {
    return description.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof VitalsRecord) {
      final VitalsRecord r = (VitalsRecord) obj;
      return (TextUtils.compareStrings(r.description, description) && r.age == age && r.value - value == 0) && (year.equals(r.year)) && r.isValid() == isValid();
    }
    return false;
  }

  @Override
  public int compareTo(final VitalsRecord o) {
    if (!isValid()) {
      if (!o.isValid()) {
        return 0;
      }
      return -1;
    }
    if (!o.isValid()) {
      return 1;
    }
    if (TextUtils.compareStrings(description, o.description) && Double.compare(value, o.value) == 0) {
      return Integer.compare(age, o.age);
    }
    if (TextUtils.compareStrings(description, o.description)) {
      return Double.compare(value, o.value);
    }
    return description.compareTo(o.description);
  }

}