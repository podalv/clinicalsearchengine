package com.podalv.extractor.datastructures.records;

/** Does not need to implement Comparable, all the data is stored in a map before the actual extraction
 *
 * @author podalv
 *
 */
public class VisitDxRecord extends EventRecord<VisitDxRecord> {

  private final int    visitId;
  private final int    visitDxId;
  private final String primary;

  public VisitDxRecord(final int visitId, final int visitDxId, final String primary) {
    this.visitId = visitId;
    this.visitDxId = visitDxId;
    this.primary = primary;
  }

  @Override
  public VisitDxRecord getDeepCopy() {
    final VisitDxRecord result = new VisitDxRecord(visitId, visitDxId, primary);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public String getPrimary() {
    return primary;
  }

  public int getVisitDxId() {
    return visitDxId;
  }

  public int getVisitId() {
    return visitId;
  }
}
