package com.podalv.extractor.datastructures.records;

import com.podalv.search.datastructures.index.StringEnumIndexBuilder;
import com.podalv.utils.text.TextUtils;

public class MedRecord extends EventRecord<MedRecord> implements Comparable<MedRecord> {

  private int           start;
  private int           duration;
  private final String  status;
  private final Integer year;
  private final Integer rxcui;
  private final String  route;
  private int           end;

  public MedRecord(final int start, final int end, final Integer year, final String status, final Integer rxcui, final String route) {
    this.start = start;
    this.year = year;
    this.status = status;
    this.rxcui = rxcui;
    this.end = end;
    this.route = route;
    this.duration = end - start;
  }

  public int getEnd() {
    return end;
  }

  @Override
  public MedRecord getDeepCopy() {
    final MedRecord result = new MedRecord(start, end, year, status, rxcui, route);
    result.setDuration(duration);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public String getRoute() {
    return route == null ? StringEnumIndexBuilder.EMPTY : route;
  }

  public int getDuration() {
    return duration;
  }

  public void setStart(final int start) {
    this.start = start;
  }

  public void setEnd(final int end) {
    this.end = end;
  }

  public Integer getRxCui() {
    return rxcui;
  }

  public int getStart() {
    return start;
  }

  public String getStatus() {
    return status == null ? StringEnumIndexBuilder.EMPTY : status;
  }

  public Integer getYear() {
    return year;
  }

  public void setDuration(final int duration) {
    this.duration = duration;
  }

  @Override
  public int hashCode() {
    return rxcui;
  }

  private boolean contentEquals(final MedRecord r) {
    return (r.start == start && r.duration == duration && r.year.equals(year) && TextUtils.compareStrings(r.status, status) && TextUtils.compareStrings(r.route, route)
        && isValid() == r.isValid());
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof MedRecord) {
      final MedRecord r = (MedRecord) obj;
      return (r.rxcui.equals(rxcui) && contentEquals(r));
    }
    return false;
  }

  @Override
  public int compareTo(final MedRecord o) {
    if (!isValid()) {
      if (!o.isValid()) {
        return 0;
      }
      return -1;
    }
    if (!o.isValid()) {
      return 1;
    }
    if (o.start == start) {
      return Integer.compare(duration, o.duration);
    }
    return Integer.compare(start, o.start);
  }

}
