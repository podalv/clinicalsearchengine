package com.podalv.extractor.datastructures.records;

public abstract class EventRecord<S> {

  private boolean valid = true;

  public boolean isValid() {
    return valid;
  }

  public abstract S getDeepCopy();

  public void invalidate() {
    valid = false;
  }
}
