package com.podalv.extractor.datastructures.records;

/** Does not need to implement Comparable, all the data is stored in a map before the actual extraction
*
* @author podalv
*
*/
public class NoteRecord extends EventRecord<NoteRecord> {

  private final int    noteId;
  private final String noteType;
  private final double age;
  private final int    year;

  public NoteRecord(final int noteId, final String noteType, final double age, final int year) {
    this.noteId = noteId;
    this.noteType = noteType;
    this.age = age;
    this.year = year;
  }

  @Override
  public NoteRecord getDeepCopy() {
    final NoteRecord result = new NoteRecord(noteId, noteType, age, year);
    if (!isValid()) {
      result.invalidate();
    }
    return result;
  }

  public double getAge() {
    return age;
  }

  public int getYear() {
    return year;
  }

  public String getNoteType() {
    return noteType;
  }

  public int getNoteId() {
    return noteId;
  }
}
