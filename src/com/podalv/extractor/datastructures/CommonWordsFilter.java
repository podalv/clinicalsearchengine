package com.podalv.extractor.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.StringHashSet;
import com.podalv.maps.string.StringKeyIntMap;
import com.podalv.utils.text.TextUtils;

/** A list of common word TIDs read from the 'commonWordTids.txt' file
 *
 * @author podalv
 *
 */
public class CommonWordsFilter {

  private static final CommonWordsFilter INSTANCE          = new CommonWordsFilter();
  private static final String            TID_LIST_FILENAME = "commonWordTids.txt";
  private final StringHashSet            commonWords       = new StringHashSet();
  private final IntOpenHashSet           commonWordTids    = new IntOpenHashSet();
  private static boolean                 enabled           = true;

  private CommonWordsFilter() {
    try {
      final ClassLoader classLoader = getClass().getClassLoader();
      final BufferedReader reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(TID_LIST_FILENAME)));
      String line;
      while ((line = reader.readLine()) != null) {
        commonWords.add(line);
      }
      reader.close();
    }
    catch (NumberFormatException | IOException e) {
      e.printStackTrace();
    }
  }

  public static Iterator<String> getCommonWords() {
    return INSTANCE.commonWords.iterator();
  }

  public static void enable(final boolean enable) {
    enabled = enable;
  }

  private static StringKeyIntMap readTermDictionary(final InputStream termDictionary) throws NumberFormatException, IOException {
    final StringKeyIntMap result = new StringKeyIntMap();
    final BufferedReader reader = new BufferedReader(new InputStreamReader(termDictionary));
    String line;
    while ((line = reader.readLine()) != null) {
      final String[] data = TextUtils.split(line, '\t');
      result.put(data[1], Integer.parseInt(data[0]));
    }
    reader.close();
    return result;
  }

  public static void init(final InputStream termDictionary) throws NumberFormatException, IOException {
    final StringKeyIntMap stringToTid = readTermDictionary(termDictionary);
    final Iterator<String> i = INSTANCE.commonWords.iterator();
    while (i.hasNext()) {
      final String commonWord = i.next();
      if (stringToTid.get(commonWord) != null) {
        INSTANCE.commonWordTids.add(stringToTid.get(commonWord));
      }
    }
  }

  public static void reset() {
    enabled = true;
    INSTANCE.commonWordTids.clear();
  }

  public static boolean isInitialized() {
    return !INSTANCE.commonWordTids.isEmpty();
  }

  public static boolean isCommonWord(final int tid) {
    return enabled ? INSTANCE.commonWordTids.contains(tid) : false;
  }

  public static boolean isCommonWord(final String lowerCaseWord) {
    return enabled ? INSTANCE.commonWords.contains(lowerCaseWord) : false;
  }

}
