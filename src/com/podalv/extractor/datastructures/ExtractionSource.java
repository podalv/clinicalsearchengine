package com.podalv.extractor.datastructures;

import java.io.Closeable;

public interface ExtractionSource extends Closeable {

  /** ColumnNr is 1 based **/
  public String getString(int columnNr) throws Exception;

  /** ColumnNr is 1 based **/
  public int getInt(int columnNr) throws Exception;

  /** ColumnNr is 1 based **/
  public long getLong(int columnNr) throws Exception;

  /** ColumnNr is 1 based **/
  public double getDouble(int columnNr) throws Exception;

  /** ColumnNr is 1 based **/
  public int getByte(int columnNr) throws Exception;

  /** ColumnNr is 1 based **/
  public int getShort(int columnNr) throws Exception;

  /** ColumnNr is 1 based **/
  public boolean next() throws Exception;

  public boolean isAfterLast() throws Exception;

}
