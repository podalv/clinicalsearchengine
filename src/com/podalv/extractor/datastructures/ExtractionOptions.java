package com.podalv.extractor.datastructures;

import java.util.ArrayList;

import com.podalv.extractor.datastructures.ExtractionOption.IDENTIFIER;

/** Contains options whether to extract certain parts of dataset
 *
 * @author podalv
 *
 */
public class ExtractionOptions {

  private final ArrayList<ExtractionOption> options = new ArrayList<ExtractionOption>();
  private boolean[]                         cache   = null;

  public static String getDescriptions() {
    return new ExtractionOptions("").toString();
  }

  private void initOptions() {
    options.add(new ExtractionOption('d', IDENTIFIER.UMLS, "extracts UMLS dictionary"));
    options.add(new ExtractionOption('i', IDENTIFIER.CODES, "extracts codes"));
    options.add(new ExtractionOption('t', IDENTIFIER.TERMS, "extracts terms"));
  }

  private void initCache() {
    if (cache == null) {
      cache = new boolean[100];
      for (int x = 0; x < options.size(); x++) {
        cache[options.get(x).getIdentifier().ordinal()] = options.get(x).getValue();
      }
    }
  }

  /** Can be initialized from a String argument where:
   *  d = dictionaryExtraction
   *  i = codes (ICD9 + CPT ... + visits)
   *  t = terms extraction
   *
   * @param fromArguments
   */
  public ExtractionOptions(final String fromArgument) {
    initOptions();
    parseArgument(fromArgument);
  }

  private void parseArgument(final String argument) {
    for (int x = 0; x < options.size(); x++) {
      options.get(x).setValue(argument.indexOf(options.get(x).getTrigger()) != -1);
    }
  }

  private boolean getOption(final IDENTIFIER id) {
    initCache();
    return cache[id.ordinal()];
  }

  public boolean extractIcd9() {
    return getOption(IDENTIFIER.CODES);
  }

  public boolean extractTerms() {
    return getOption(IDENTIFIER.TERMS);
  }

  boolean extractDictionary() {
    return getOption(IDENTIFIER.UMLS);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder();
    for (int x = 0; x < options.size(); x++) {
      System.out.println(options.get(x).getTrigger() + " : " + options.get(x).getDescription() + "\n");
    }
    return result.toString();
  }
}
