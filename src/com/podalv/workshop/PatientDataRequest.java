package com.podalv.workshop;

public class PatientDataRequest {

  private final int[] patientIds;

  public PatientDataRequest(final int[] patientIds) {
    this.patientIds = patientIds;
  }

  public int[] getPatientId() {
    return patientIds;
  }
}
