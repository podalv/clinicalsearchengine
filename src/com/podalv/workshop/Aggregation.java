package com.podalv.workshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.utils.datastructures.SortPair;
import com.podalv.workshop.datastructures.DumpResponse;

/** Aggregates data in the patient dump by feature type
 *
 * @author podalv
 *
 */
public class Aggregation {

  public static int[] extractRx(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    if (response.getRx() != null) {
      final Iterator<Entry<String, ArrayList<String>>> iterator = response.getRx().entrySet().iterator();
      final ArrayList<SortPair<Integer, Integer>> sortablePairs = new ArrayList<>();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<String>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 4) {
          sortablePairs.add(new SortPair<>(Integer.parseInt(entry.getValue().get(x)), Integer.parseInt(entry.getValue().get(x + 1))));
        }
      }
      Collections.sort(sortablePairs);
      if (sortablePairs.size() != 0) {
        int currentLeft = sortablePairs.get(0).getComparable();
        int currentRight = sortablePairs.get(0).getObject();
        for (int x = 0; x < sortablePairs.size(); x++) {
          if (currentLeft != sortablePairs.get(x).getComparable()) {
            result.add(currentLeft);
            result.add(currentRight);
            currentLeft = sortablePairs.get(x).getComparable();
            currentRight = sortablePairs.get(x).getObject();
          }
          else {
            currentRight = sortablePairs.get(x).getObject();
          }
        }
        result.add(currentLeft);
        result.add(currentRight);
      }
    }
    return result.toArray();
  }

  public static int[] extractIcd9Ranges(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    if (response.getIcd9() != null) {
      final Iterator<Entry<String, ArrayList<String>>> iterator = response.getIcd9().entrySet().iterator();
      final ArrayList<SortPair<Integer, Integer>> sortablePairs = new ArrayList<>();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<String>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 3) {
          sortablePairs.add(new SortPair<>(Integer.parseInt(entry.getValue().get(x)), Integer.parseInt(entry.getValue().get(x + 1))));
        }
      }
      Collections.sort(sortablePairs);
      if (sortablePairs.size() != 0) {
        int currentLeft = sortablePairs.get(0).getComparable();
        int currentRight = sortablePairs.get(0).getObject();
        for (int x = 0; x < sortablePairs.size(); x++) {
          if (currentLeft != sortablePairs.get(x).getComparable()) {
            result.add(currentLeft);
            result.add(currentRight);
            currentLeft = sortablePairs.get(x).getComparable();
            currentRight = sortablePairs.get(x).getObject();
          }
          else {
            currentRight = sortablePairs.get(x).getObject();
          }
        }
        result.add(currentLeft);
        result.add(currentRight);
      }
    }
    return result.toArray();
  }

  public static int[] extractIcd10Ranges(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    if (response.getIcd10() != null) {
      final Iterator<Entry<String, ArrayList<String>>> iterator = response.getIcd10().entrySet().iterator();
      final ArrayList<SortPair<Integer, Integer>> sortablePairs = new ArrayList<>();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<String>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 3) {
          sortablePairs.add(new SortPair<>(Integer.parseInt(entry.getValue().get(x)), Integer.parseInt(entry.getValue().get(x + 1))));
        }
      }
      Collections.sort(sortablePairs);
      if (sortablePairs.size() != 0) {
        int currentLeft = sortablePairs.get(0).getComparable();
        int currentRight = sortablePairs.get(0).getObject();
        for (int x = 0; x < sortablePairs.size(); x++) {
          if (currentLeft != sortablePairs.get(x).getComparable()) {
            result.add(currentLeft);
            result.add(currentRight);
            currentLeft = sortablePairs.get(x).getComparable();
            currentRight = sortablePairs.get(x).getObject();
          }
          else {
            currentRight = sortablePairs.get(x).getObject();
          }
        }
        result.add(currentLeft);
        result.add(currentRight);
      }
    }
    return result.toArray();
  }

  public static int[] extractCptRanges(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    if (response.getCpt() != null) {
      final ArrayList<SortPair<Integer, Integer>> sortablePairs = new ArrayList<>();
      final Iterator<Entry<String, ArrayList<Integer>>> iterator = response.getCpt().entrySet().iterator();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<Integer>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 2) {
          sortablePairs.add(new SortPair<>(entry.getValue().get(x), entry.getValue().get(x + 1)));
        }
      }
      Collections.sort(sortablePairs);
      if (sortablePairs.size() != 0) {
        int currentLeft = sortablePairs.get(0).getComparable();
        int currentRight = sortablePairs.get(0).getObject();
        for (int x = 0; x < sortablePairs.size(); x++) {
          if (currentLeft != sortablePairs.get(x).getComparable()) {
            result.add(currentLeft);
            result.add(currentRight);
            currentLeft = sortablePairs.get(x).getComparable();
            currentRight = sortablePairs.get(x).getObject();
          }
          else {
            currentRight = sortablePairs.get(x).getObject();
          }
        }
        result.add(currentLeft);
        result.add(currentRight);
      }
    }
    return result.toArray();
  }

  public static int[] extractLabsRanges(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    final IntOpenHashSet set = new IntOpenHashSet();
    if (response.getLabs() != null) {
      final Iterator<Entry<String, ArrayList<String>>> iterator = response.getLabs().entrySet().iterator();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<String>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 2) {
          set.add(Integer.parseInt(entry.getValue().get(x)));
        }
      }
    }
    final IntIterator i = set.iterator();
    while (i.hasNext()) {
      result.add(i.next());
    }
    result.sort();
    return result.toArray();
  }

  public static int[] extractVitalsRanges(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    final IntOpenHashSet set = new IntOpenHashSet();
    if (response.getVitals() != null) {
      final Iterator<Entry<String, ArrayList<String>>> iterator = response.getVitals().entrySet().iterator();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<String>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 2) {
          set.add(Integer.parseInt(entry.getValue().get(x)));
        }
      }
    }
    final IntIterator i = set.iterator();
    while (i.hasNext()) {
      result.add(i.next());
    }
    result.sort();
    return result.toArray();
  }

  public static int[] extractNoteTypes(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    final IntOpenHashSet set = new IntOpenHashSet();
    if (response.getNoteTypes() != null) {
      final Iterator<Entry<String, ArrayList<Integer>>> iterator = response.getNoteTypes().entrySet().iterator();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<Integer>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x++) {
          set.add(entry.getValue().get(x));
        }
      }
    }
    final IntIterator i = set.iterator();
    while (i.hasNext()) {
      result.add(i.next());
    }
    result.sort();
    return result.toArray();
  }

  public static int[] extractDepartments(final DumpResponse response) {
    final IntArrayList result = new IntArrayList();
    if (response.getDepartments() != null) {
      final ArrayList<SortPair<Integer, Integer>> sortablePairs = new ArrayList<>();
      final Iterator<Entry<String, ArrayList<Integer>>> iterator = response.getDepartments().entrySet().iterator();
      while (iterator.hasNext()) {
        final Entry<String, ArrayList<Integer>> entry = iterator.next();
        for (int x = 0; x < entry.getValue().size(); x += 2) {
          sortablePairs.add(new SortPair<>(entry.getValue().get(x), entry.getValue().get(x + 1)));
        }
      }
      Collections.sort(sortablePairs);
      if (sortablePairs.size() != 0) {
        int currentLeft = sortablePairs.get(0).getComparable();
        int currentRight = sortablePairs.get(0).getObject();
        for (int x = 0; x < sortablePairs.size(); x++) {
          if (currentLeft != sortablePairs.get(x).getComparable()) {
            result.add(currentLeft);
            result.add(currentRight);
            currentLeft = sortablePairs.get(x).getComparable();
            currentRight = sortablePairs.get(x).getObject();
          }
          else {
            currentRight = sortablePairs.get(x).getObject();
          }
        }
        result.add(currentLeft);
        result.add(currentRight);
      }
    }
    return result.toArray();
  }

}
