package com.podalv.workshop;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.podalv.workshop.datastructures.DumpResponse;

public class AggregatedPatientResponse {

  @JsonProperty("patientId") private final int patientId;
  @JsonProperty("icd9") private int[]          icd9;
  @JsonProperty("icd10") private int[]         icd10;
  @JsonProperty("cpt") private int[]           cpt;
  @JsonProperty("rx") private int[]            rx;
  @JsonProperty("labs") private int[]          labs;
  @JsonProperty("vitals") private int[]        vitals;
  @JsonProperty("notes") private int[]         notes;
  @JsonProperty("departments") private int[]   departments;

  public AggregatedPatientResponse(final int patientId) {
    this.patientId = patientId;
  }

  public AggregatedPatientResponse(final int patientId, final DumpResponse dump) {
    this.patientId = patientId;
    setIcd9(Aggregation.extractIcd9Ranges(dump));
    setIcd10(Aggregation.extractIcd10Ranges(dump));
    setCpt(Aggregation.extractCptRanges(dump));
    setRx(Aggregation.extractRx(dump));
    setLabs(Aggregation.extractLabsRanges(dump));
    setVitals(Aggregation.extractVitalsRanges(dump));
    setNotes(Aggregation.extractNoteTypes(dump));
    setDepartments(Aggregation.extractDepartments(dump));
  }

  public void setNotes(final int[] notes) {
    this.notes = notes;
  }

  public void setDepartments(final int[] departments) {
    this.departments = departments;
  }

  public int[] getDepartments() {
    return departments;
  }

  public int[] getNotes() {
    return notes;
  }

  public void setIcd9(final int[] icd9) {
    this.icd9 = icd9;
  }

  public int[] getVitals() {
    return vitals;
  }

  public void setVitals(final int[] vitals) {
    this.vitals = vitals;
  }

  public void setCpt(final int[] cpt) {
    this.cpt = cpt;
  }

  public int[] getIcd10() {
    return icd10;
  }

  public void setIcd10(final int[] icd10) {
    this.icd10 = icd10;
  }

  public void setLabs(final int[] labs) {
    this.labs = labs;
  }

  public int[] getLabs() {
    return labs;
  }

  public void setRx(final int[] rx) {
    this.rx = rx;
  }

  public int[] getRx() {
    return rx;
  }

  public int[] getCpt() {
    return cpt;
  }

  public int[] getIcd9() {
    return icd9;
  }

  public int getPatientId() {
    return patientId;
  }
}
