package com.podalv.workshop;

public class PatientQueryResponse {

  private int[] patientIds;

  public void setPatientIds(final int[] patientIds) {
    this.patientIds = patientIds;
  }

  public int[] getPatientIds() {
    return patientIds;
  }
}
