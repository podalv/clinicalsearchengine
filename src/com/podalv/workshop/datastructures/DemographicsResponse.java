package com.podalv.workshop.datastructures;

import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.string.IntKeyObjectMap;

/** Used in workshop to get demographics table in a more compact for than parsing array of jsons
 *
 * @author podalv
 *
 */
@JsonIgnoreProperties({"sort"})
public class DemographicsResponse {

  @JsonProperty("patientIds") private final IntArrayList        patientIds     = new IntArrayList();
  @JsonProperty("recordStart") private final IntArrayList       recordStart    = new IntArrayList();
  @JsonProperty("recordEnd") private final IntArrayList         recordEnd      = new IntArrayList();
  @JsonProperty("death") private final IntArrayList             death          = new IntArrayList();
  @JsonProperty("gender") private final IntArrayList            gender         = new IntArrayList();
  @JsonProperty("race") private final IntArrayList              race           = new IntArrayList();
  @JsonProperty("ethnicity") private final IntArrayList         ethnicity      = new IntArrayList();
  @JsonProperty("genderMap") private final ArrayList<String>    genderMap      = new ArrayList<String>();
  @JsonProperty("raceMap") private final ArrayList<String>      raceMap        = new ArrayList<String>();
  @JsonProperty("ethnicityMap") private final ArrayList<String> ethnicityMap   = new ArrayList<String>();
  @JsonProperty("sortedPids") private final IntArrayList        sortedPids     = new IntArrayList();
  private transient HashMap<String, Integer>                    genderToId     = new HashMap<String, Integer>();
  private transient HashMap<String, Integer>                    raceToId       = new HashMap<String, Integer>();
  private transient HashMap<String, Integer>                    ethnicityToId  = new HashMap<String, Integer>();
  private transient IntKeyObjectMap<IntArrayList>               durationToPids = new IntKeyObjectMap<IntArrayList>();
  private transient IntArrayList                                durations      = new IntArrayList();

  private int getGenderId(final String gender) {
    if (genderToId.containsKey(gender)) {
      return genderToId.get(gender);
    }
    genderMap.add(gender);
    genderToId.put(gender, genderMap.size() - 1);
    return genderMap.size() - 1;
  }

  private int getRaceId(final String race) {
    if (raceToId.containsKey(race)) {
      return raceToId.get(race);
    }
    raceMap.add(race);
    raceToId.put(race, raceMap.size() - 1);
    return raceMap.size() - 1;
  }

  private int getEthnicityId(final String ethnicity) {
    if (ethnicityToId.containsKey(ethnicity)) {
      return ethnicityToId.get(ethnicity);
    }
    ethnicityMap.add(ethnicity);
    ethnicityToId.put(ethnicity, ethnicityMap.size() - 1);
    return ethnicityMap.size() - 1;
  }

  public synchronized void addPatient(final int id, final int recordStart, final int recordEnd, final int death, final String gender, final String race, final String ethnicity) {
    this.patientIds.add(id);
    this.recordStart.add(recordStart);
    this.recordEnd.add(recordEnd);
    this.death.add(death);
    this.gender.add(getGenderId(gender));
    this.race.add(getRaceId(race));
    this.ethnicity.add(getEthnicityId(ethnicity));
    this.durations.add(recordEnd - recordStart);
    IntArrayList list = this.durationToPids.get(recordEnd - recordStart);
    if (list == null) {
      list = new IntArrayList();
      this.durationToPids.put(recordEnd - recordStart, list);
    }
    list.add(this.patientIds.size() - 1);
  }

  public IntArrayList getDeath() {
    return death;
  }

  public IntArrayList getEthnicity() {
    return ethnicity;
  }

  public IntArrayList getGender() {
    return gender;
  }

  public IntArrayList getPatientIds() {
    return patientIds;
  }

  public IntArrayList getRace() {
    return race;
  }

  public IntArrayList getRecordEnd() {
    return recordEnd;
  }

  public IntArrayList getRecordStart() {
    return recordStart;
  }

  public void sort() {
    durations.sort();
    int prevDuration = Integer.MIN_VALUE;
    for (int x = 0; x < durations.size(); x++) {
      if (prevDuration != durations.get(x)) {
        final IntArrayList ids = durationToPids.get(durations.get(x));
        for (int y = 0; y < ids.size(); y++) {
          sortedPids.add(ids.get(y));
        }
        durationToPids.remove(durations.get(x));
      }
      prevDuration = durations.get(x);
    }
  }
}
