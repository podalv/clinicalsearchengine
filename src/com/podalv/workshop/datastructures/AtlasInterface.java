package com.podalv.workshop.datastructures;

import com.podalv.objectdb.Transaction;

public interface AtlasInterface {

  public DumpResponse dump(Transaction transaction, DumpRequest req);

}
