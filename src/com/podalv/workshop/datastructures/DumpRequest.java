package com.podalv.workshop.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

/** Request to dump selected patients
 *
 * @author podalv
 *
 */
public class DumpRequest {

  @JsonProperty("patientId") private final Integer patientId;
  @JsonProperty("patientIds") private final int[]  patientIds;
  @JsonProperty("icd9") private boolean            icd9;
  @JsonProperty("icd10") private boolean           icd10;
  @JsonProperty("departments") private boolean     departments;
  @JsonProperty("cpt") private boolean             cpt;
  @JsonProperty("rx") private boolean              rx;
  @JsonProperty("snomed") private boolean          snomed;
  @JsonProperty("notes") private boolean           notes;
  @JsonProperty("visitTypes") private boolean      visitTypes;
  @JsonProperty("noteTypes") private boolean       noteTypes;
  @JsonProperty("encounterDays") private boolean   encounterDays;
  @JsonProperty("ageRanges") private boolean       ageRanges;
  @JsonProperty("labs") private boolean            labs;
  @JsonProperty("vitals") private boolean          vitals;
  @JsonProperty("atc") private boolean             atc;
  @JsonProperty("selectionQuery") private String   selectionQuery;
  @JsonProperty("containsStart") private boolean   containsStart;
  @JsonProperty("containsEnd") private boolean     containsEnd;

  public static DumpRequest createWorkshopRequest(final int patientId) {
    final DumpRequest r = new DumpRequest(patientId);
    r.setIcd9(true);
    r.setIcd10(true);
    r.setIcd10(true);
    r.setDepartments(true);
    r.setCpt(true);
    r.setRx(true);
    r.setLabs(true);
    r.setVitals(true);
    r.setNotes(false);
    r.setNoteTypes(true);
    return r;
  }

  public static DumpRequest createWorkshopRequest(final int[] patientIds) {
    final DumpRequest r = new DumpRequest(patientIds);
    r.setIcd9(true);
    r.setIcd10(true);
    r.setIcd10(true);
    r.setDepartments(true);
    r.setCpt(true);
    r.setRx(true);
    r.setLabs(true);
    r.setVitals(true);
    r.setNotes(false);
    r.setNoteTypes(true);
    return r;
  }

  public static DumpRequest createFull(final int patientId) {
    final DumpRequest req = new DumpRequest(patientId);
    req.setAgeRanges(true);
    req.setAtc(true);
    req.setCpt(true);
    req.setIcd10(true);
    req.setDepartments(true);
    req.setEncounterDays(true);
    req.setIcd9(true);
    req.setLabs(true);
    req.setNotes(true);
    req.setRx(true);
    req.setSnomed(true);
    req.setVisitTypes(true);
    req.setVitals(true);
    req.setNoteTypes(true);
    return req;
  }

  public static DumpRequest createFull(final int[] patientIds) {
    final DumpRequest req = new DumpRequest(patientIds);
    req.setAgeRanges(true);
    req.setAtc(true);
    req.setCpt(true);
    req.setIcd10(true);
    req.setDepartments(true);
    req.setEncounterDays(true);
    req.setIcd9(true);
    req.setLabs(true);
    req.setNotes(true);
    req.setRx(true);
    req.setSnomed(true);
    req.setVisitTypes(true);
    req.setVitals(true);
    req.setNoteTypes(true);
    return req;
  }

  public DumpRequest setQuery(final String query, final boolean start, final boolean end) {
    this.selectionQuery = query;
    this.containsStart = start;
    this.containsEnd = end;
    return this;
  }

  public DumpRequest(final int patientId) {
    this.patientId = patientId;
    this.patientIds = null;
  }

  public DumpRequest(final int[] patientIds) {
    this.patientId = null;
    this.patientIds = patientIds;
  }

  public void setAtc(final boolean atc) {
    this.atc = atc;
  }

  public void setVitals(final boolean vitals) {
    this.vitals = vitals;
  }

  public void setLabs(final boolean labs) {
    this.labs = labs;
  }

  public void setNoteTypes(final boolean noteTypes) {
    this.noteTypes = noteTypes;
  }

  public void setAgeRanges(final boolean ageRanges) {
    this.ageRanges = ageRanges;
  }

  public void setSnomed(final boolean snomed) {
    this.snomed = snomed;
  }

  public void setIcd10(final boolean icd10) {
    this.icd10 = icd10;
  }

  public void setDepartments(final boolean departments) {
    this.departments = departments;
  }

  public boolean isIcd10() {
    return icd10;
  }

  public boolean isDepartments() {
    return departments;
  }

  public void setEncounterDays(final boolean encounterDays) {
    this.encounterDays = encounterDays;
  }

  public void setVisitTypes(final boolean visitTypes) {
    this.visitTypes = visitTypes;
  }

  public void setNotes(final boolean notes) {
    this.notes = notes;
  }

  public void setRx(final boolean rx) {
    this.rx = rx;
  }

  public void setCpt(final boolean cpt) {
    this.cpt = cpt;
  }

  public void setIcd9(final boolean icd9) {
    this.icd9 = icd9;
  }

  public Integer getPatientId() {
    return patientId;
  }

  public boolean isIcd9() {
    return icd9;
  }

  public boolean isRx() {
    return rx;
  }

  public String getSelectionQuery() {
    return selectionQuery;
  }

  public boolean isContainsEnd() {
    return containsEnd;
  }

  public boolean isContainsStart() {
    return containsStart;
  }

  public boolean isNotes() {
    return notes;
  }

  public int[] getPatientIds() {
    return patientIds;
  }

  public boolean isSnomed() {
    return snomed;
  }

  public boolean isCpt() {
    return cpt;
  }

  public boolean isVisitTypes() {
    return visitTypes;
  }

  public boolean isEncounterDays() {
    return encounterDays;
  }

  public boolean isAgeRanges() {
    return ageRanges;
  }

  public boolean isLabs() {
    return labs;
  }

  public boolean isVitals() {
    return vitals;
  }

  public boolean isNoteTypes() {
    return noteTypes;
  }

  public boolean isAtc() {
    return atc;
  }

  public DumpRequest clone(final int patientId) {
    final DumpRequest req = new DumpRequest(patientId);
    req.setAgeRanges(this.ageRanges);
    req.setAtc(this.atc);
    req.setCpt(this.cpt);
    req.setDepartments(this.departments);
    req.setEncounterDays(this.encounterDays);
    req.setIcd10(this.icd10);
    req.setIcd9(this.icd9);
    req.setLabs(this.labs);
    req.setNotes(this.notes);
    req.setNoteTypes(this.noteTypes);
    req.setQuery(this.selectionQuery, this.containsStart, this.containsEnd);
    req.setRx(this.rx);
    req.setSnomed(this.snomed);
    req.setVisitTypes(this.visitTypes);
    req.setVitals(this.vitals);
    return req;
  }

  public static void main(final String[] args) {
    final DumpRequest req = new DumpRequest(new int[] {5538, 5539, 5540, 5541, 5542});
    req.setAgeRanges(true);
    req.setAtc(true);
    req.setIcd9(true);
    req.setRx(true);
    req.setCpt(true);
    req.setEncounterDays(true);
    req.setLabs(true);
    req.setNotes(true);
    req.setNoteTypes(true);
    req.setSnomed(true);
    req.setVisitTypes(true);
    req.setVitals(true);
    System.out.println(new Gson().toJson(req));
  }

}