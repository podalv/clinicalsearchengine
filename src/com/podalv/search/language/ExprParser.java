// Generated from Expr.g4 by ANTLR 4.7.1
package com.podalv.search.language;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ExprParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, T__62=63, T__63=64, T__64=65, T__65=66, 
		T__66=67, T__67=68, T__68=69, T__69=70, T__70=71, T__71=72, T__72=73, 
		T__73=74, T__74=75, T__75=76, T__76=77, T__77=78, T__78=79, T__79=80, 
		T__80=81, T__81=82, T__82=83, T__83=84, T__84=85, T__85=86, T__86=87, 
		T__87=88, T__88=89, T__89=90, T__90=91, T__91=92, T__92=93, T__93=94, 
		T__94=95, T__95=96, T__96=97, T__97=98, T__98=99, T__99=100, T__100=101, 
		T__101=102, T__102=103, T__103=104, T__104=105, T__105=106, T__106=107, 
		T__107=108, T__108=109, T__109=110, T__110=111, T__111=112, T__112=113, 
		T__113=114, T__114=115, T__115=116, T__116=117, T__117=118, T__118=119, 
		T__119=120, T__120=121, T__121=122, T__122=123, T__123=124, T__124=125, 
		T__125=126, T__126=127, T__127=128, T__128=129, T__129=130, T__130=131, 
		T__131=132, T__132=133, T__133=134, T__134=135, T__135=136, T__136=137, 
		T__137=138, T__138=139, T__139=140, T__140=141, T__141=142, T__142=143, 
		T__143=144, T__144=145, T__145=146, T__146=147, T__147=148, T__148=149, 
		T__149=150, T__150=151, T__151=152, T__152=153, T__153=154, T__154=155, 
		T__155=156, T__156=157, T__157=158, T__158=159, T__159=160, T__160=161, 
		T__161=162, T__162=163, T__163=164, HASICD9=165, HASICD10=166, HASRXNORM=167, 
		HASCPT=168, DEATH=169, TEXT=170, NEGATED_TEXT=171, FAMILY_HISTORY_TEXT=172, 
		ENCOUNTERS=173, TIMELINE=174, AND=175, OR=176, NOT=177, INTERSECT=178, 
		UNION=179, INVERT=180, INTERVAL=181, CPT=182, ICD10=183, ICD9=184, POSITIVE_RANGE=185, 
		NEGATIVE_RANGE=186, DIGIT=187, CHARACTER=188, TERM=189, WS=190;
	public static final int
		RULE_program = 0, RULE_output_commands = 1, RULE_each = 2, RULE_each_content = 3, 
		RULE_main_command = 4, RULE_each_name = 5, RULE_each_line = 6, RULE_print_atom = 7, 
		RULE_top_level = 8, RULE_commands = 9, RULE_command = 10, RULE_atom = 11, 
		RULE_icd9_atom = 12, RULE_double_value = 13, RULE_before_atom_left = 14, 
		RULE_before_atom_right = 15, RULE_before_atom_left_left = 16, RULE_before_atom_left_no_asterisk = 17, 
		RULE_before = 18, RULE_before_command = 19, RULE_before_modifier = 20, 
		RULE_start_time = 21, RULE_end_time = 22, RULE_limit_type = 23, RULE_time_modifier = 24, 
		RULE_note_atom = 25, RULE_note_atoms = 26, RULE_atoms = 27;
	public static final String[] ruleNames = {
		"program", "output_commands", "each", "each_content", "main_command", 
		"each_name", "each_line", "print_atom", "top_level", "commands", "command", 
		"atom", "icd9_atom", "double_value", "before_atom_left", "before_atom_right", 
		"before_atom_left_left", "before_atom_left_no_asterisk", "before", "before_command", 
		"before_modifier", "start_time", "end_time", "limit_type", "time_modifier", 
		"note_atom", "note_atoms", "atoms"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'OUTPUT'", "'('", "')'", "'EVENT'", "'FLOW'", "'EVENT_FLOW'", "'EVENTFLOW'", 
		"'CSV'", "','", "'='", "'TIME'", "'START'", "'END'", "'DUMP'", "'FOR'", 
		"'EACH'", "'AS'", "'{'", "'}'", "'A'", "'B'", "'C'", "'D'", "'E'", "'F'", 
		"'G'", "'H'", "'I'", "'J'", "'K'", "'L'", "'M'", "'N'", "'O'", "'P'", 
		"'Q'", "'R'", "'S'", "'T'", "'U'", "'V'", "'W'", "'X'", "'Y'", "'Z'", 
		"'_'", "';'", "'RETURN'", "'CLEAR'", "'IF'", "'!'", "'EMPTY'", "'=='", 
		"'!='", "'EXIT'", "'PRINT'", "'+'", "'CONTINUE'", "'FAIL'", "'PATIENT'", 
		"'LIMIT'", "'ESTIMATE'", "'EST'", "'EVALUATE'", "'EVAL'", "'INTERSECTING'", 
		"'ALL'", "'ANY'", "'NEVER'", "'ALWAYS'", "'START OF'", "'START_OF'", "'END OF'", 
		"'END_OF'", "'PAIRS'", "'FIRST'", "'MENTION'", "'FIRST_MENTION'", "'LAST'", 
		"'LAST_MENTION'", "'EXTEND'", "'RESIZE'", "'EXTEND BY'", "'DURATION'", 
		"'SINGLE'", "'MIN'", "'MAX'", "'HISTORY'", "'OF'", "'HISTORY_OF'", "'NO'", 
		"'NO_HISTORY_OF'", "'HAD'", "'NEVER_HAD'", "'COUNT'", "'EQUAL'", "'EQUALS'", 
		"'CONTAINS'", "'CONTAIN'", "'*'", "'IDENTICAL'", "'SAME'", "'DIFF'", "'MERGE'", 
		"'PRIMARY'", "'ORIGINAL'", "'DEPARTMENT'", "'DEPT'", "'NOTE_TYPE'", "'NOTETYPE'", 
		"'NOTE'", "'TYPE'", "'VISIT_TYPE'", "'VISITTYPE'", "'VISIT'", "'DRUG'", 
		"'DRUGS'", "'MED'", "'MEDS'", "'ROUTE'", "'STATUS'", "'GENDER'", "'YEAR'", 
		"'YEARS'", "'RACE'", "'ETHNICITY'", "'SNOMED'", "'NOTES'", "'RECORD_START'", 
		"'RECORD'", "'RECORD_END'", "'AGE'", "'VITALS'", "'LABS'", "'LAB'", "'NULL'", 
		"'ATC'", "'PATIENTS'", "'.'", "'BEFORE'", "'AFTER'", "'SEQUENCE'", "'-'", 
		"'<'", "'>'", "'SEC'", "'SECS'", "'SECOND'", "'SECONDS'", "'MINS'", "'MINUTE'", 
		"'MINUTES'", "'CACHE'", "'CACHED'", "'DAYS'", "'DAY'", "'MONTH'", "'MONTHS'", 
		"'HOUR'", "'HOURS'", "'WEEKS'", "'WKS'", "'WK'", "'WEEK'", "'ICD9'", "'ICD10'", 
		null, "'CPT'", null, "'TEXT'", "'!TEXT'", "'~TEXT'", null, "'TIMELINE'", 
		"'AND'", "'OR'", "'NOT'", "'INTERSECT'", "'UNION'", "'INVERT'", "'INTERVAL'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "HASICD9", "HASICD10", 
		"HASRXNORM", "HASCPT", "DEATH", "TEXT", "NEGATED_TEXT", "FAMILY_HISTORY_TEXT", 
		"ENCOUNTERS", "TIMELINE", "AND", "OR", "NOT", "INTERSECT", "UNION", "INVERT", 
		"INTERVAL", "CPT", "ICD10", "ICD9", "POSITIVE_RANGE", "NEGATIVE_RANGE", 
		"DIGIT", "CHARACTER", "TERM", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Expr.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ExprParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public Output_commandsContext output_commands() {
			return getRuleContext(Output_commandsContext.class,0);
		}
		public List<EachContext> each() {
			return getRuleContexts(EachContext.class);
		}
		public EachContext each(int i) {
			return getRuleContext(EachContext.class,i);
		}
		public Main_commandContext main_command() {
			return getRuleContext(Main_commandContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			setState(74);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(61);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__14) {
					{
					setState(57); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(56);
						each();
						}
						}
						setState(59); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__14 );
					}
				}

				setState(63);
				output_commands();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__14) {
					{
					setState(65); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(64);
						each();
						}
						}
						setState(67); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__14 );
					}
				}

				setState(71);
				main_command();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(72);
				output_commands();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(73);
				main_command();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Output_commandsContext extends ParserRuleContext {
		public Output_commandsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_output_commands; }
	 
		public Output_commandsContext() { }
		public void copyFrom(Output_commandsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EventFlowCommandContext extends Output_commandsContext {
		public Main_commandContext main_command() {
			return getRuleContext(Main_commandContext.class,0);
		}
		public EventFlowCommandContext(Output_commandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEventFlowCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEventFlowCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEventFlowCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CsvCommandContext extends Output_commandsContext {
		public Main_commandContext cohort;
		public Main_commandContext time_cohort;
		public Main_commandContext start_cohort;
		public Main_commandContext end_cohort;
		public List<Main_commandContext> main_command() {
			return getRuleContexts(Main_commandContext.class);
		}
		public Main_commandContext main_command(int i) {
			return getRuleContext(Main_commandContext.class,i);
		}
		public List<TerminalNode> TERM() { return getTokens(ExprParser.TERM); }
		public TerminalNode TERM(int i) {
			return getToken(ExprParser.TERM, i);
		}
		public CsvCommandContext(Output_commandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterCsvCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitCsvCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitCsvCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OutputCommandContext extends Output_commandsContext {
		public Main_commandContext main_command() {
			return getRuleContext(Main_commandContext.class,0);
		}
		public OutputCommandContext(Output_commandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOutputCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOutputCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitOutputCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DumpCommandContext extends Output_commandsContext {
		public Main_commandContext cohort;
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public Main_commandContext main_command() {
			return getRuleContext(Main_commandContext.class,0);
		}
		public DumpCommandContext(Output_commandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDumpCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDumpCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDumpCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Output_commandsContext output_commands() throws RecognitionException {
		Output_commandsContext _localctx = new Output_commandsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_output_commands);
		int _la;
		try {
			setState(210);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				_localctx = new OutputCommandContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(76);
				match(T__0);
				setState(77);
				match(T__1);
				{
				setState(78);
				main_command();
				}
				setState(79);
				match(T__2);
				}
				break;
			case T__3:
			case T__5:
			case T__6:
				_localctx = new EventFlowCommandContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(85);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__3:
					{
					setState(81);
					match(T__3);
					setState(82);
					match(T__4);
					}
					break;
				case T__5:
					{
					setState(83);
					match(T__5);
					}
					break;
				case T__6:
					{
					setState(84);
					match(T__6);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(87);
				match(T__1);
				{
				setState(88);
				main_command();
				}
				setState(89);
				match(T__2);
				}
				break;
			case T__7:
				_localctx = new CsvCommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(201);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
				case 1:
					{
					setState(91);
					match(T__7);
					setState(92);
					match(T__1);
					setState(93);
					((CsvCommandContext)_localctx).cohort = main_command();
					{
					{
					setState(100); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(94);
						match(T__8);
						{
						setState(97);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==TERM) {
							{
							setState(95);
							match(TERM);
							setState(96);
							match(T__9);
							}
						}

						setState(99);
						main_command();
						}
						}
						}
						setState(102); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__8 );
					}
					}
					setState(104);
					match(T__2);
					}
					break;
				case 2:
					{
					setState(106);
					match(T__7);
					setState(107);
					match(T__1);
					setState(108);
					((CsvCommandContext)_localctx).cohort = main_command();
					{
					setState(114);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
					case 1:
						{
						{
						setState(109);
						match(T__8);
						{
						{
						setState(110);
						match(T__10);
						setState(111);
						match(T__9);
						}
						setState(113);
						((CsvCommandContext)_localctx).time_cohort = main_command();
						}
						}
						}
						break;
					}
					}
					{
					{
					setState(122); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(116);
						match(T__8);
						{
						setState(119);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==TERM) {
							{
							setState(117);
							match(TERM);
							setState(118);
							match(T__9);
							}
						}

						setState(121);
						main_command();
						}
						}
						}
						setState(124); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__8 );
					}
					}
					setState(126);
					match(T__2);
					}
					break;
				case 3:
					{
					setState(128);
					match(T__7);
					setState(129);
					match(T__1);
					setState(130);
					((CsvCommandContext)_localctx).cohort = main_command();
					{
					setState(136);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
					case 1:
						{
						{
						setState(131);
						match(T__8);
						{
						{
						setState(132);
						match(T__11);
						setState(133);
						match(T__9);
						}
						setState(135);
						((CsvCommandContext)_localctx).start_cohort = main_command();
						}
						}
						}
						break;
					}
					}
					{
					{
					setState(144); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(138);
						match(T__8);
						{
						setState(141);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==TERM) {
							{
							setState(139);
							match(TERM);
							setState(140);
							match(T__9);
							}
						}

						setState(143);
						main_command();
						}
						}
						}
						setState(146); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__8 );
					}
					}
					setState(148);
					match(T__2);
					}
					break;
				case 4:
					{
					setState(150);
					match(T__7);
					setState(151);
					match(T__1);
					setState(152);
					((CsvCommandContext)_localctx).cohort = main_command();
					{
					setState(158);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
					case 1:
						{
						{
						setState(153);
						match(T__8);
						{
						{
						setState(154);
						match(T__12);
						setState(155);
						match(T__9);
						}
						setState(157);
						((CsvCommandContext)_localctx).end_cohort = main_command();
						}
						}
						}
						break;
					}
					}
					{
					{
					setState(166); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(160);
						match(T__8);
						{
						setState(163);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==TERM) {
							{
							setState(161);
							match(TERM);
							setState(162);
							match(T__9);
							}
						}

						setState(165);
						main_command();
						}
						}
						}
						setState(168); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__8 );
					}
					}
					setState(170);
					match(T__2);
					}
					break;
				case 5:
					{
					setState(172);
					match(T__7);
					setState(173);
					match(T__1);
					setState(174);
					((CsvCommandContext)_localctx).cohort = main_command();
					{
					setState(180);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
					case 1:
						{
						{
						setState(175);
						match(T__8);
						{
						{
						setState(176);
						match(T__11);
						setState(177);
						match(T__9);
						}
						setState(179);
						((CsvCommandContext)_localctx).start_cohort = main_command();
						}
						}
						}
						break;
					}
					}
					{
					setState(187);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
					case 1:
						{
						{
						setState(182);
						match(T__8);
						{
						{
						setState(183);
						match(T__12);
						setState(184);
						match(T__9);
						}
						setState(186);
						((CsvCommandContext)_localctx).end_cohort = main_command();
						}
						}
						}
						break;
					}
					}
					{
					{
					setState(195); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(189);
						match(T__8);
						{
						setState(192);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==TERM) {
							{
							setState(190);
							match(TERM);
							setState(191);
							match(T__9);
							}
						}

						setState(194);
						main_command();
						}
						}
						}
						setState(197); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__8 );
					}
					}
					setState(199);
					match(T__2);
					}
					break;
				}
				}
				break;
			case T__13:
				_localctx = new DumpCommandContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(203);
				match(T__13);
				setState(204);
				match(T__1);
				setState(205);
				((DumpCommandContext)_localctx).cohort = main_command();
				setState(206);
				match(T__8);
				setState(207);
				match(TERM);
				setState(208);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EachContext extends ParserRuleContext {
		public EachContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_each; }
	 
		public EachContext() { }
		public void copyFrom(EachContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EachCommandContext extends EachContext {
		public Main_commandContext main_command() {
			return getRuleContext(Main_commandContext.class,0);
		}
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public Each_contentContext each_content() {
			return getRuleContext(Each_contentContext.class,0);
		}
		public EachCommandContext(EachContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EachContext each() throws RecognitionException {
		EachContext _localctx = new EachContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_each);
		try {
			_localctx = new EachCommandContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			match(T__14);
			setState(213);
			match(T__15);
			setState(214);
			match(T__1);
			setState(215);
			main_command();
			setState(216);
			match(T__2);
			setState(217);
			match(T__16);
			setState(218);
			match(T__1);
			setState(219);
			each_name();
			setState(220);
			match(T__2);
			setState(221);
			each_content();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Each_contentContext extends ParserRuleContext {
		public List<Each_lineContext> each_line() {
			return getRuleContexts(Each_lineContext.class);
		}
		public Each_lineContext each_line(int i) {
			return getRuleContext(Each_lineContext.class,i);
		}
		public Each_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_each_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEach_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEach_content(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEach_content(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Each_contentContext each_content() throws RecognitionException {
		Each_contentContext _localctx = new Each_contentContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_each_content);
		int _la;
		try {
			setState(233);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(223);
				match(T__17);
				setState(225); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(224);
					each_line();
					}
					}
					setState(227); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__47) | (1L << T__48) | (1L << T__49) | (1L << T__54) | (1L << T__55) | (1L << T__57) | (1L << T__58))) != 0) );
				setState(229);
				match(T__18);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(231);
				match(T__17);
				setState(232);
				match(T__18);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Main_commandContext extends ParserRuleContext {
		public Top_levelContext top_level() {
			return getRuleContext(Top_levelContext.class,0);
		}
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public Main_commandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterMain_command(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitMain_command(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitMain_command(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Main_commandContext main_command() throws RecognitionException {
		Main_commandContext _localctx = new Main_commandContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_main_command);
		try {
			setState(237);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(235);
				top_level();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(236);
				each_name();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Each_nameContext extends ParserRuleContext {
		public Each_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_each_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEach_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEach_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEach_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Each_nameContext each_name() throws RecognitionException {
		Each_nameContext _localctx = new Each_nameContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_each_name);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(240); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(239);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(242); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Each_lineContext extends ParserRuleContext {
		public Each_lineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_each_line; }
	 
		public Each_lineContext() { }
		public void copyFrom(Each_lineContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EachLineEqualsContext extends Each_lineContext {
		public Token label;
		public Each_contentContext each_content() {
			return getRuleContext(Each_contentContext.class,0);
		}
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public EachLineEqualsContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachLineEquals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachLineEquals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachLineEquals(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrintSingleContext extends Each_lineContext {
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public Print_atomContext print_atom() {
			return getRuleContext(Print_atomContext.class,0);
		}
		public PrintSingleContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterPrintSingle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitPrintSingle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitPrintSingle(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachNodeFailPatientContext extends Each_lineContext {
		public EachNodeFailPatientContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachNodeFailPatient(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachNodeFailPatient(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachNodeFailPatient(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrintContext extends Each_lineContext {
		public List<Each_nameContext> each_name() {
			return getRuleContexts(Each_nameContext.class);
		}
		public Each_nameContext each_name(int i) {
			return getRuleContext(Each_nameContext.class,i);
		}
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public List<Print_atomContext> print_atom() {
			return getRuleContexts(Print_atomContext.class);
		}
		public Print_atomContext print_atom(int i) {
			return getRuleContext(Print_atomContext.class,i);
		}
		public PrintContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachNodeExitContext extends Each_lineContext {
		public EachNodeExitContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachNodeExit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachNodeExit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachNodeExit(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachNodeAssignContext extends Each_lineContext {
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public EachNodeAssignContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachNodeAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachNodeAssign(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachNodeAssign(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachNodeSaveContext extends Each_lineContext {
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public EachNodeSaveContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachNodeSave(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachNodeSave(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachNodeSave(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClearVariableContext extends Each_lineContext {
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public ClearVariableContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterClearVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitClearVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitClearVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachLineIfContext extends Each_lineContext {
		public Token not_label;
		public Each_contentContext each_content() {
			return getRuleContext(Each_contentContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public EachLineIfContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachLineIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachLineIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachLineIf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnusedEachContext extends Each_lineContext {
		public EachContext each() {
			return getRuleContext(EachContext.class,0);
		}
		public UnusedEachContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnusedEach(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnusedEach(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnusedEach(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachNodeContinueContext extends Each_lineContext {
		public EachNodeContinueContext(Each_lineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachNodeContinue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachNodeContinue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachNodeContinue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Each_lineContext each_line() throws RecognitionException {
		Each_lineContext _localctx = new Each_lineContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_each_line);
		int _la;
		try {
			setState(341);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				_localctx = new EachNodeAssignContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(244);
				each_name();
				setState(245);
				match(T__9);
				setState(248);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
				case 1:
					{
					setState(246);
					command();
					}
					break;
				case 2:
					{
					setState(247);
					atom();
					}
					break;
				}
				setState(250);
				match(T__46);
				}
				break;
			case 2:
				_localctx = new EachNodeSaveContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(252);
				match(T__47);
				setState(255);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
				case 1:
					{
					setState(253);
					command();
					}
					break;
				case 2:
					{
					setState(254);
					atom();
					}
					break;
				}
				setState(257);
				match(T__16);
				setState(258);
				each_name();
				setState(259);
				match(T__46);
				}
				break;
			case 3:
				_localctx = new ClearVariableContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(261);
				match(T__48);
				setState(262);
				each_name();
				setState(263);
				match(T__46);
				}
				break;
			case 4:
				_localctx = new EachLineIfContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(265);
				match(T__49);
				setState(267);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__50) {
					{
					setState(266);
					((EachLineIfContext)_localctx).not_label = match(T__50);
					}
				}

				setState(269);
				match(T__51);
				setState(270);
				match(T__1);
				setState(273);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
				case 1:
					{
					setState(271);
					command();
					}
					break;
				case 2:
					{
					setState(272);
					atom();
					}
					break;
				}
				setState(275);
				match(T__2);
				setState(276);
				each_content();
				setState(278);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__46) {
					{
					setState(277);
					match(T__46);
					}
				}

				}
				break;
			case 5:
				_localctx = new EachLineEqualsContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(280);
				match(T__49);
				setState(281);
				match(T__1);
				setState(284);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
				case 1:
					{
					setState(282);
					command();
					}
					break;
				case 2:
					{
					setState(283);
					atom();
					}
					break;
				}
				setState(286);
				match(T__2);
				setState(287);
				((EachLineEqualsContext)_localctx).label = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__52 || _la==T__53) ) {
					((EachLineEqualsContext)_localctx).label = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(288);
				match(T__1);
				setState(291);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
				case 1:
					{
					setState(289);
					command();
					}
					break;
				case 2:
					{
					setState(290);
					atom();
					}
					break;
				}
				setState(293);
				match(T__2);
				setState(294);
				each_content();
				setState(296);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__46) {
					{
					setState(295);
					match(T__46);
					}
				}

				}
				break;
			case 6:
				_localctx = new EachNodeExitContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(298);
				match(T__54);
				setState(299);
				match(T__46);
				}
				break;
			case 7:
				_localctx = new PrintContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(300);
				match(T__55);
				setState(301);
				match(T__1);
				setState(306);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
				case 1:
					{
					setState(302);
					each_name();
					}
					break;
				case 2:
					{
					setState(303);
					command();
					}
					break;
				case 3:
					{
					setState(304);
					atom();
					}
					break;
				case 4:
					{
					setState(305);
					print_atom();
					}
					break;
				}
				setState(316); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(308);
					match(T__56);
					setState(314);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
					case 1:
						{
						setState(309);
						each_name();
						setState(310);
						command();
						}
						break;
					case 2:
						{
						setState(312);
						atom();
						}
						break;
					case 3:
						{
						setState(313);
						print_atom();
						}
						break;
					}
					}
					}
					setState(318); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__56 );
				setState(320);
				match(T__2);
				setState(321);
				match(T__46);
				}
				break;
			case 8:
				_localctx = new PrintSingleContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(323);
				match(T__55);
				setState(324);
				match(T__1);
				setState(330);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
				case 1:
					{
					setState(325);
					each_name();
					setState(326);
					command();
					}
					break;
				case 2:
					{
					setState(328);
					atom();
					}
					break;
				case 3:
					{
					setState(329);
					print_atom();
					}
					break;
				}
				setState(332);
				match(T__2);
				setState(333);
				match(T__46);
				}
				break;
			case 9:
				_localctx = new EachNodeContinueContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(335);
				match(T__57);
				setState(336);
				match(T__46);
				}
				break;
			case 10:
				_localctx = new EachNodeFailPatientContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(337);
				match(T__58);
				setState(338);
				match(T__59);
				setState(339);
				match(T__46);
				}
				break;
			case 11:
				_localctx = new UnusedEachContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(340);
				each();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Print_atomContext extends ParserRuleContext {
		public Print_atomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print_atom; }
	 
		public Print_atomContext() { }
		public void copyFrom(Print_atomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParseTermContext extends Print_atomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public ParseTermContext(Print_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterParseTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitParseTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitParseTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Print_atomContext print_atom() throws RecognitionException {
		Print_atomContext _localctx = new Print_atomContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_print_atom);
		try {
			_localctx = new ParseTermContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(343);
			match(TERM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Top_levelContext extends ParserRuleContext {
		public Top_levelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_top_level; }
	 
		public Top_levelContext() { }
		public void copyFrom(Top_levelContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EvaluateCommandContext extends Top_levelContext {
		public Limit_typeContext limit_type() {
			return getRuleContext(Limit_typeContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public EvaluateCommandContext(Top_levelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEvaluateCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEvaluateCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEvaluateCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unused2Context extends Top_levelContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public Unused2Context(Top_levelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnused2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnused2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnused2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EstimateCommandContext extends Top_levelContext {
		public Limit_typeContext limit_type() {
			return getRuleContext(Limit_typeContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public EstimateCommandContext(Top_levelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEstimateCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEstimateCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEstimateCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LimitCommandContext extends Top_levelContext {
		public Limit_typeContext limit_type() {
			return getRuleContext(Limit_typeContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public LimitCommandContext(Top_levelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterLimitCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitLimitCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitLimitCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unused1Context extends Top_levelContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public Unused1Context(Top_levelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnused1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnused1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnused1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Top_levelContext top_level() throws RecognitionException {
		Top_levelContext _localctx = new Top_levelContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_top_level);
		int _la;
		try {
			setState(377);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				_localctx = new Unused1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(345);
				command();
				}
				break;
			case 2:
				_localctx = new Unused2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(346);
				atom();
				}
				break;
			case 3:
				_localctx = new LimitCommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(347);
				match(T__60);
				setState(348);
				match(T__1);
				setState(349);
				limit_type();
				setState(350);
				match(T__8);
				setState(353);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
				case 1:
					{
					setState(351);
					command();
					}
					break;
				case 2:
					{
					setState(352);
					atom();
					}
					break;
				}
				setState(355);
				match(T__2);
				}
				break;
			case 4:
				_localctx = new EstimateCommandContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(357);
				_la = _input.LA(1);
				if ( !(_la==T__61 || _la==T__62) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(358);
				match(T__1);
				setState(359);
				limit_type();
				setState(360);
				match(T__8);
				setState(363);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
				case 1:
					{
					setState(361);
					command();
					}
					break;
				case 2:
					{
					setState(362);
					atom();
					}
					break;
				}
				setState(365);
				match(T__2);
				}
				break;
			case 5:
				_localctx = new EvaluateCommandContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(367);
				_la = _input.LA(1);
				if ( !(_la==T__63 || _la==T__64) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(368);
				match(T__1);
				setState(369);
				limit_type();
				setState(370);
				match(T__8);
				setState(373);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
				case 1:
					{
					setState(371);
					command();
					}
					break;
				case 2:
					{
					setState(372);
					atom();
					}
					break;
				}
				setState(375);
				match(T__2);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandsContext extends ParserRuleContext {
		public CommandsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commands; }
	 
		public CommandsContext() { }
		public void copyFrom(CommandsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class UnusedMultipleCommandsContext extends CommandsContext {
		public List<CommandsContext> commands() {
			return getRuleContexts(CommandsContext.class);
		}
		public CommandsContext commands(int i) {
			return getRuleContext(CommandsContext.class,i);
		}
		public UnusedMultipleCommandsContext(CommandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnusedMultipleCommands(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnusedMultipleCommands(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnusedMultipleCommands(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnusedMultipleCommandContext extends CommandsContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public UnusedMultipleCommandContext(CommandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnusedMultipleCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnusedMultipleCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnusedMultipleCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnusedAtomsContext extends CommandsContext {
		public AtomsContext atoms() {
			return getRuleContext(AtomsContext.class,0);
		}
		public UnusedAtomsContext(CommandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnusedAtoms(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnusedAtoms(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnusedAtoms(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnusedCommandContext extends CommandsContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public UnusedCommandContext(CommandsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnusedCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnusedCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnusedCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandsContext commands() throws RecognitionException {
		return commands(0);
	}

	private CommandsContext commands(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		CommandsContext _localctx = new CommandsContext(_ctx, _parentState);
		CommandsContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_commands, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(386);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,44,_ctx) ) {
			case 1:
				{
				_localctx = new UnusedCommandContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(380);
				command();
				}
				break;
			case 2:
				{
				_localctx = new UnusedMultipleCommandContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(381);
				command();
				setState(382);
				match(T__8);
				setState(383);
				command();
				}
				break;
			case 3:
				{
				_localctx = new UnusedAtomsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(385);
				atoms();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(393);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new UnusedMultipleCommandsContext(new CommandsContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_commands);
					setState(388);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(389);
					match(T__8);
					setState(390);
					commands(3);
					}
					} 
				}
				setState(395);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
	 
		public CommandContext() { }
		public void copyFrom(CommandContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class DurationSimpleCommandContext extends CommandContext {
		public Token type;
		public Token startTime;
		public Time_modifierContext start_time_modifier;
		public Token endTime;
		public Time_modifierContext end_time_modifier;
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public DurationSimpleCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDurationSimpleCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDurationSimpleCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDurationSimpleCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdenticalCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public IdenticalCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIdenticalCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIdenticalCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIdenticalCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoHistoryOfCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NoHistoryOfCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoHistoryOfCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoHistoryOfCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoHistoryOfCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComplexBeforeCommandContext extends CommandContext {
		public Before_commandContext before_command() {
			return getRuleContext(Before_commandContext.class,0);
		}
		public ComplexBeforeCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterComplexBeforeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitComplexBeforeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitComplexBeforeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AlwaysIntersectAnyContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AlwaysIntersectAnyContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAlwaysIntersectAny(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAlwaysIntersectAny(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAlwaysIntersectAny(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NonIntersectAllCommandContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NonIntersectAllCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNonIntersectAllCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNonIntersectAllCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNonIntersectAllCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DrugsStatusRouteContext extends CommandContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public List<TerminalNode> TERM() { return getTokens(ExprParser.TERM); }
		public TerminalNode TERM(int i) {
			return getToken(ExprParser.TERM, i);
		}
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public DrugsStatusRouteContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDrugsStatusRoute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDrugsStatusRoute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDrugsStatusRoute(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoteTypeCommandContext extends CommandContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public NoteTypeCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoteTypeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoteTypeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoteTypeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public EqualCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEqualCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEqualCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEqualCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LastMentionCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public LastMentionCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterLastMentionCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitLastMentionCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitLastMentionCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DrugsSimpleContext extends CommandContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public DrugsSimpleContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDrugsSimple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDrugsSimple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDrugsSimple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExtendByCommandContext extends CommandContext {
		public Time_modifierContext start_time_modifier;
		public Time_modifierContext end_time_modifier;
		public Start_timeContext start_time() {
			return getRuleContext(Start_timeContext.class,0);
		}
		public End_timeContext end_time() {
			return getRuleContext(End_timeContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public ExtendByCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterExtendByCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitExtendByCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitExtendByCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VisitTypeCommandContext extends CommandContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public VisitTypeCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterVisitTypeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitVisitTypeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitVisitTypeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CountSimpleCommandContext extends CommandContext {
		public Token startCount;
		public Token endCount;
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public CountSimpleCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterCountSimpleCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitCountSimpleCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitCountSimpleCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnionCommandContext extends CommandContext {
		public TerminalNode UNION() { return getToken(ExprParser.UNION, 0); }
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public UnionCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnionCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnionCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnionCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DrugsRouteContext extends CommandContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public DrugsRouteContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDrugsRoute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDrugsRoute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDrugsRoute(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnusedBeforeContext extends CommandContext {
		public BeforeContext before() {
			return getRuleContext(BeforeContext.class,0);
		}
		public UnusedBeforeContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterUnusedBefore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitUnusedBefore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitUnusedBefore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MergeCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public MergeCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterMergeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitMergeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitMergeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FindCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public FindCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFindCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFindCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitFindCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DurationComplexCommandContext extends CommandContext {
		public Token type;
		public Token startTime;
		public Time_modifierContext start_time_modifier;
		public Token endTime;
		public Time_modifierContext end_time_modifier;
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public DurationComplexCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDurationComplexCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDurationComplexCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDurationComplexCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContainsCommandContext extends CommandContext {
		public Token start_type;
		public Token end_type;
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public ContainsCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterContainsCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitContainsCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitContainsCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StartCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public StartCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterStartCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitStartCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitStartCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DrugsStatusContext extends CommandContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public DrugsStatusContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDrugsStatus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDrugsStatus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDrugsStatus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NonIntersectCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public NonIntersectCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNonIntersectCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNonIntersectCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNonIntersectCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OriginalPrimaryCommandContext extends CommandContext {
		public Icd9_atomContext icd9_atom() {
			return getRuleContext(Icd9_atomContext.class,0);
		}
		public OriginalPrimaryCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOriginalPrimaryCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOriginalPrimaryCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitOriginalPrimaryCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrCommandContext extends CommandContext {
		public TerminalNode OR() { return getToken(ExprParser.OR, 0); }
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public OrCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOrCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOrCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitOrCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NonIntersectManyCommandContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NonIntersectManyCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNonIntersectManyCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNonIntersectManyCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNonIntersectManyCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntervalPairwiseCommandContext extends CommandContext {
		public TerminalNode INTERVAL() { return getToken(ExprParser.INTERVAL, 0); }
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public IntervalPairwiseCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIntervalPairwiseCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIntervalPairwiseCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIntervalPairwiseCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AnyVisitTypeCommandContext extends CommandContext {
		public AnyVisitTypeCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAnyVisitTypeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAnyVisitTypeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAnyVisitTypeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AlwaysIntersectContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public AlwaysIntersectContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAlwaysIntersect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAlwaysIntersect(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAlwaysIntersect(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CountComplexCommandContext extends CommandContext {
		public Token type;
		public Token startCount;
		public Token endCount;
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public CountComplexCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterCountComplexCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitCountComplexCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitCountComplexCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InvertCommandContext extends CommandContext {
		public TerminalNode INVERT() { return getToken(ExprParser.INVERT, 0); }
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public InvertCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterInvertCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitInvertCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitInvertCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EndCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public EndCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEndCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEndCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEndCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FirstMentionCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public FirstMentionCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFirstMentionCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFirstMentionCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitFirstMentionCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DiffCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public DiffCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDiffCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDiffCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDiffCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HistoryOfCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public HistoryOfCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHistoryOfCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHistoryOfCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHistoryOfCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DrugsRouteStatusContext extends CommandContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public List<TerminalNode> TERM() { return getTokens(ExprParser.TERM); }
		public TerminalNode TERM(int i) {
			return getToken(ExprParser.TERM, i);
		}
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public DrugsRouteStatusContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDrugsRouteStatus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDrugsRouteStatus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDrugsRouteStatus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AlwaysIntersectAllContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AlwaysIntersectAllContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAlwaysIntersectAll(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAlwaysIntersectAll(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAlwaysIntersectAll(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FirstMentionContextCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public FirstMentionContextCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFirstMentionContextCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFirstMentionContextCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitFirstMentionContextCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LastMentionContextCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public LastMentionContextCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterLastMentionContextCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitLastMentionContextCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitLastMentionContextCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrimaryCommandContext extends CommandContext {
		public Icd9_atomContext icd9_atom() {
			return getRuleContext(Icd9_atomContext.class,0);
		}
		public PrimaryCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterPrimaryCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitPrimaryCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitPrimaryCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NeverIntersectCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public NeverIntersectCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNeverIntersectCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNeverIntersectCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNeverIntersectCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntersectCommandContext extends CommandContext {
		public TerminalNode INTERSECT() { return getToken(ExprParser.INTERSECT, 0); }
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public IntersectCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIntersectCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIntersectCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIntersectCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotCommandContext extends CommandContext {
		public TerminalNode NOT() { return getToken(ExprParser.NOT, 0); }
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NotCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNotCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNotCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNotCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntersectingAllContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public IntersectingAllContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIntersectingAll(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIntersectingAll(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIntersectingAll(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NeverIntersectManyCommandContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NeverIntersectManyCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNeverIntersectManyCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNeverIntersectManyCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNeverIntersectManyCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NeverHadCommandContext extends CommandContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NeverHadCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNeverHadCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNeverHadCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNeverHadCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SameCommandContext extends CommandContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public SameCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterSameCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitSameCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitSameCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntersectingAnyContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public IntersectingAnyContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIntersectingAny(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIntersectingAny(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIntersectingAny(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NeverIntersectAllCommandContext extends CommandContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NeverIntersectAllCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNeverIntersectAllCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNeverIntersectAllCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNeverIntersectAllCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CountComplex2CommandContext extends CommandContext {
		public Token type;
		public Token startCount;
		public Token endCount;
		public Before_atom_leftContext before_atom_left() {
			return getRuleContext(Before_atom_leftContext.class,0);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public CountComplex2CommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterCountComplex2Command(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitCountComplex2Command(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitCountComplex2Command(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DurationComplex2CommandContext extends CommandContext {
		public Token type;
		public Token startTime;
		public Time_modifierContext start_time_modifier;
		public Token endTime;
		public Time_modifierContext end_time_modifier;
		public Before_atom_leftContext before_atom_left() {
			return getRuleContext(Before_atom_leftContext.class,0);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public DurationComplex2CommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDurationComplex2Command(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDurationComplex2Command(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDurationComplex2Command(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DepartmentCommandContext extends CommandContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public DepartmentCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDepartmentCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDepartmentCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDepartmentCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndCommandContext extends CommandContext {
		public TerminalNode AND() { return getToken(ExprParser.AND, 0); }
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public AndCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAndCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAndCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAndCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComplexBeforeContext extends CommandContext {
		public List<BeforeContext> before() {
			return getRuleContexts(BeforeContext.class);
		}
		public BeforeContext before(int i) {
			return getRuleContext(BeforeContext.class,i);
		}
		public ComplexBeforeContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterComplexBefore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitComplexBefore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitComplexBefore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntervalCommandContext extends CommandContext {
		public TerminalNode INTERVAL() { return getToken(ExprParser.INTERVAL, 0); }
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public IntervalCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIntervalCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIntervalCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIntervalCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OriginalCommandContext extends CommandContext {
		public Icd9_atomContext icd9_atom() {
			return getRuleContext(Icd9_atomContext.class,0);
		}
		public OriginalCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOriginalCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOriginalCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitOriginalCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_command);
		int _la;
		try {
			int _alt;
			setState(1033);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,118,_ctx) ) {
			case 1:
				_localctx = new AndCommandContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(396);
				match(AND);
				setState(397);
				match(T__1);
				setState(398);
				commands(0);
				setState(399);
				match(T__2);
				}
				break;
			case 2:
				_localctx = new OrCommandContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(401);
				match(OR);
				setState(402);
				match(T__1);
				setState(403);
				commands(0);
				setState(404);
				match(T__2);
				}
				break;
			case 3:
				_localctx = new NotCommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(406);
				match(NOT);
				setState(407);
				match(T__1);
				setState(410);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
				case 1:
					{
					setState(408);
					command();
					}
					break;
				case 2:
					{
					setState(409);
					atom();
					}
					break;
				}
				setState(412);
				match(T__2);
				}
				break;
			case 4:
				_localctx = new UnusedBeforeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(414);
				before();
				}
				break;
			case 5:
				_localctx = new ComplexBeforeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(415);
				before();
				setState(418); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(416);
						match(AND);
						setState(417);
						before();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(420); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case 6:
				_localctx = new FindCommandContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(422);
				match(T__47);
				setState(425);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
				case 1:
					{
					setState(423);
					command();
					}
					break;
				case 2:
					{
					setState(424);
					atom();
					}
					break;
				}
				setState(427);
				match(T__65);
				setState(430);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
				case 1:
					{
					setState(428);
					command();
					}
					break;
				case 2:
					{
					setState(429);
					atom();
					}
					break;
				}
				}
				break;
			case 7:
				_localctx = new IntersectingAllContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(432);
				match(T__47);
				setState(435);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
				case 1:
					{
					setState(433);
					command();
					}
					break;
				case 2:
					{
					setState(434);
					atom();
					}
					break;
				}
				setState(437);
				match(T__65);
				setState(438);
				match(T__66);
				setState(439);
				match(T__1);
				setState(440);
				commands(0);
				setState(441);
				match(T__2);
				}
				break;
			case 8:
				_localctx = new IntersectingAnyContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(443);
				match(T__47);
				setState(446);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
				case 1:
					{
					setState(444);
					command();
					}
					break;
				case 2:
					{
					setState(445);
					atom();
					}
					break;
				}
				setState(448);
				match(T__65);
				setState(449);
				match(T__67);
				setState(450);
				match(T__1);
				setState(451);
				commands(0);
				setState(452);
				match(T__2);
				}
				break;
			case 9:
				_localctx = new NonIntersectManyCommandContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(454);
				match(T__47);
				setState(457);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
				case 1:
					{
					setState(455);
					command();
					}
					break;
				case 2:
					{
					setState(456);
					atom();
					}
					break;
				}
				setState(459);
				match(NOT);
				setState(460);
				match(T__65);
				setState(461);
				match(T__67);
				setState(462);
				match(T__1);
				setState(463);
				commands(0);
				setState(464);
				match(T__2);
				}
				break;
			case 10:
				_localctx = new NonIntersectAllCommandContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(466);
				match(T__47);
				setState(469);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
				case 1:
					{
					setState(467);
					command();
					}
					break;
				case 2:
					{
					setState(468);
					atom();
					}
					break;
				}
				setState(471);
				match(NOT);
				setState(472);
				match(T__65);
				setState(473);
				match(T__66);
				setState(474);
				match(T__1);
				setState(475);
				commands(0);
				setState(476);
				match(T__2);
				}
				break;
			case 11:
				_localctx = new NonIntersectCommandContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(478);
				match(T__47);
				setState(481);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
				case 1:
					{
					setState(479);
					command();
					}
					break;
				case 2:
					{
					setState(480);
					atom();
					}
					break;
				}
				setState(483);
				match(NOT);
				setState(484);
				match(T__65);
				setState(487);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
				case 1:
					{
					setState(485);
					command();
					}
					break;
				case 2:
					{
					setState(486);
					atom();
					}
					break;
				}
				}
				break;
			case 12:
				_localctx = new NeverIntersectCommandContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(489);
				match(T__47);
				setState(492);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
				case 1:
					{
					setState(490);
					command();
					}
					break;
				case 2:
					{
					setState(491);
					atom();
					}
					break;
				}
				setState(494);
				match(T__68);
				setState(495);
				match(T__65);
				setState(498);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,57,_ctx) ) {
				case 1:
					{
					setState(496);
					command();
					}
					break;
				case 2:
					{
					setState(497);
					atom();
					}
					break;
				}
				}
				break;
			case 13:
				_localctx = new NeverIntersectManyCommandContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(500);
				match(T__47);
				setState(503);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,58,_ctx) ) {
				case 1:
					{
					setState(501);
					command();
					}
					break;
				case 2:
					{
					setState(502);
					atom();
					}
					break;
				}
				setState(505);
				match(T__68);
				setState(506);
				match(T__65);
				setState(507);
				match(T__67);
				setState(508);
				match(T__1);
				setState(509);
				commands(0);
				setState(510);
				match(T__2);
				}
				break;
			case 14:
				_localctx = new NeverIntersectAllCommandContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(512);
				match(T__47);
				setState(515);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,59,_ctx) ) {
				case 1:
					{
					setState(513);
					command();
					}
					break;
				case 2:
					{
					setState(514);
					atom();
					}
					break;
				}
				setState(517);
				match(T__68);
				setState(518);
				match(T__65);
				setState(519);
				match(T__66);
				setState(520);
				match(T__1);
				setState(521);
				commands(0);
				setState(522);
				match(T__2);
				}
				break;
			case 15:
				_localctx = new AlwaysIntersectContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(524);
				match(T__47);
				setState(527);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
				case 1:
					{
					setState(525);
					command();
					}
					break;
				case 2:
					{
					setState(526);
					atom();
					}
					break;
				}
				setState(529);
				match(T__69);
				setState(530);
				match(T__65);
				setState(533);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
				case 1:
					{
					setState(531);
					command();
					}
					break;
				case 2:
					{
					setState(532);
					atom();
					}
					break;
				}
				}
				break;
			case 16:
				_localctx = new AlwaysIntersectAnyContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(535);
				match(T__47);
				setState(538);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,62,_ctx) ) {
				case 1:
					{
					setState(536);
					command();
					}
					break;
				case 2:
					{
					setState(537);
					atom();
					}
					break;
				}
				setState(540);
				match(T__69);
				setState(541);
				match(T__65);
				setState(542);
				match(T__67);
				setState(543);
				match(T__1);
				setState(544);
				commands(0);
				setState(545);
				match(T__2);
				}
				break;
			case 17:
				_localctx = new AlwaysIntersectAllContext(_localctx);
				enterOuterAlt(_localctx, 17);
				{
				setState(547);
				match(T__47);
				setState(550);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
				case 1:
					{
					setState(548);
					command();
					}
					break;
				case 2:
					{
					setState(549);
					atom();
					}
					break;
				}
				setState(552);
				match(T__69);
				setState(553);
				match(T__65);
				setState(554);
				match(T__66);
				setState(555);
				match(T__1);
				setState(556);
				commands(0);
				setState(557);
				match(T__2);
				}
				break;
			case 18:
				_localctx = new InvertCommandContext(_localctx);
				enterOuterAlt(_localctx, 18);
				{
				setState(559);
				match(INVERT);
				setState(560);
				match(T__1);
				setState(563);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,64,_ctx) ) {
				case 1:
					{
					setState(561);
					command();
					}
					break;
				case 2:
					{
					setState(562);
					atom();
					}
					break;
				}
				setState(565);
				match(T__2);
				}
				break;
			case 19:
				_localctx = new StartCommandContext(_localctx);
				enterOuterAlt(_localctx, 19);
				{
				setState(567);
				_la = _input.LA(1);
				if ( !(((((_la - 12)) & ~0x3f) == 0 && ((1L << (_la - 12)) & ((1L << (T__11 - 12)) | (1L << (T__70 - 12)) | (1L << (T__71 - 12)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(568);
				match(T__1);
				setState(571);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
				case 1:
					{
					setState(569);
					command();
					}
					break;
				case 2:
					{
					setState(570);
					atom();
					}
					break;
				}
				setState(573);
				match(T__2);
				}
				break;
			case 20:
				_localctx = new EndCommandContext(_localctx);
				enterOuterAlt(_localctx, 20);
				{
				setState(575);
				_la = _input.LA(1);
				if ( !(((((_la - 13)) & ~0x3f) == 0 && ((1L << (_la - 13)) & ((1L << (T__12 - 13)) | (1L << (T__72 - 13)) | (1L << (T__73 - 13)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(576);
				match(T__1);
				setState(579);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,66,_ctx) ) {
				case 1:
					{
					setState(577);
					command();
					}
					break;
				case 2:
					{
					setState(578);
					atom();
					}
					break;
				}
				setState(581);
				match(T__2);
				}
				break;
			case 21:
				_localctx = new IntervalCommandContext(_localctx);
				enterOuterAlt(_localctx, 21);
				{
				setState(583);
				match(INTERVAL);
				setState(584);
				match(T__1);
				setState(585);
				commands(0);
				setState(586);
				match(T__2);
				}
				break;
			case 22:
				_localctx = new IntervalPairwiseCommandContext(_localctx);
				enterOuterAlt(_localctx, 22);
				{
				setState(588);
				match(INTERVAL);
				setState(589);
				match(T__1);
				setState(590);
				commands(0);
				setState(591);
				match(T__8);
				setState(592);
				match(T__74);
				setState(593);
				match(T__2);
				}
				break;
			case 23:
				_localctx = new IntersectCommandContext(_localctx);
				enterOuterAlt(_localctx, 23);
				{
				setState(595);
				match(INTERSECT);
				setState(596);
				match(T__1);
				setState(597);
				commands(0);
				setState(598);
				match(T__2);
				}
				break;
			case 24:
				_localctx = new UnionCommandContext(_localctx);
				enterOuterAlt(_localctx, 24);
				{
				setState(600);
				match(UNION);
				setState(601);
				match(T__1);
				setState(602);
				commands(0);
				setState(603);
				match(T__2);
				}
				break;
			case 25:
				_localctx = new ComplexBeforeCommandContext(_localctx);
				enterOuterAlt(_localctx, 25);
				{
				setState(605);
				before_command();
				}
				break;
			case 26:
				_localctx = new FirstMentionCommandContext(_localctx);
				enterOuterAlt(_localctx, 26);
				{
				setState(609);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__75:
					{
					setState(606);
					match(T__75);
					setState(607);
					match(T__76);
					}
					break;
				case T__77:
					{
					setState(608);
					match(T__77);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(611);
				match(T__1);
				setState(614);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,68,_ctx) ) {
				case 1:
					{
					setState(612);
					command();
					}
					break;
				case 2:
					{
					setState(613);
					atom();
					}
					break;
				}
				setState(616);
				match(T__2);
				}
				break;
			case 27:
				_localctx = new LastMentionCommandContext(_localctx);
				enterOuterAlt(_localctx, 27);
				{
				setState(621);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__78:
					{
					setState(618);
					match(T__78);
					setState(619);
					match(T__76);
					}
					break;
				case T__79:
					{
					setState(620);
					match(T__79);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(623);
				match(T__1);
				setState(626);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,70,_ctx) ) {
				case 1:
					{
					setState(624);
					command();
					}
					break;
				case 2:
					{
					setState(625);
					atom();
					}
					break;
				}
				setState(628);
				match(T__2);
				}
				break;
			case 28:
				_localctx = new FirstMentionContextCommandContext(_localctx);
				enterOuterAlt(_localctx, 28);
				{
				setState(633);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__75:
					{
					setState(630);
					match(T__75);
					setState(631);
					match(T__76);
					}
					break;
				case T__77:
					{
					setState(632);
					match(T__77);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(635);
				match(T__1);
				setState(638);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,72,_ctx) ) {
				case 1:
					{
					setState(636);
					command();
					}
					break;
				case 2:
					{
					setState(637);
					atom();
					}
					break;
				}
				setState(640);
				match(T__8);
				setState(643);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,73,_ctx) ) {
				case 1:
					{
					setState(641);
					command();
					}
					break;
				case 2:
					{
					setState(642);
					atom();
					}
					break;
				}
				setState(645);
				match(T__2);
				}
				break;
			case 29:
				_localctx = new LastMentionContextCommandContext(_localctx);
				enterOuterAlt(_localctx, 29);
				{
				setState(650);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__78:
					{
					setState(647);
					match(T__78);
					setState(648);
					match(T__76);
					}
					break;
				case T__79:
					{
					setState(649);
					match(T__79);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(652);
				match(T__1);
				setState(655);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
				case 1:
					{
					setState(653);
					command();
					}
					break;
				case 2:
					{
					setState(654);
					atom();
					}
					break;
				}
				setState(657);
				match(T__8);
				setState(660);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,76,_ctx) ) {
				case 1:
					{
					setState(658);
					command();
					}
					break;
				case 2:
					{
					setState(659);
					atom();
					}
					break;
				}
				setState(662);
				match(T__2);
				}
				break;
			case 30:
				_localctx = new LastMentionContextCommandContext(_localctx);
				enterOuterAlt(_localctx, 30);
				{
				setState(667);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__78:
					{
					setState(664);
					match(T__78);
					setState(665);
					match(T__76);
					}
					break;
				case T__79:
					{
					setState(666);
					match(T__79);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(669);
				match(T__1);
				setState(672);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,78,_ctx) ) {
				case 1:
					{
					setState(670);
					command();
					}
					break;
				case 2:
					{
					setState(671);
					atom();
					}
					break;
				}
				setState(674);
				match(T__8);
				setState(677);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
				case 1:
					{
					setState(675);
					command();
					}
					break;
				case 2:
					{
					setState(676);
					atom();
					}
					break;
				}
				setState(679);
				match(T__2);
				}
				break;
			case 31:
				_localctx = new ExtendByCommandContext(_localctx);
				enterOuterAlt(_localctx, 31);
				{
				setState(681);
				_la = _input.LA(1);
				if ( !(((((_la - 81)) & ~0x3f) == 0 && ((1L << (_la - 81)) & ((1L << (T__80 - 81)) | (1L << (T__81 - 81)) | (1L << (T__82 - 81)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(682);
				match(T__1);
				setState(685);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,80,_ctx) ) {
				case 1:
					{
					setState(683);
					command();
					}
					break;
				case 2:
					{
					setState(684);
					atom();
					}
					break;
				}
				setState(687);
				match(T__8);
				setState(688);
				start_time();
				setState(690);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(689);
					((ExtendByCommandContext)_localctx).start_time_modifier = time_modifier();
					}
				}

				setState(692);
				match(T__8);
				setState(693);
				end_time();
				setState(695);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(694);
					((ExtendByCommandContext)_localctx).end_time_modifier = time_modifier();
					}
				}

				setState(697);
				match(T__2);
				}
				break;
			case 32:
				_localctx = new DurationSimpleCommandContext(_localctx);
				enterOuterAlt(_localctx, 32);
				{
				setState(699);
				match(T__83);
				setState(700);
				match(T__1);
				setState(703);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,83,_ctx) ) {
				case 1:
					{
					setState(701);
					command();
					}
					break;
				case 2:
					{
					setState(702);
					atom();
					}
					break;
				}
				setState(705);
				match(T__8);
				setState(706);
				((DurationSimpleCommandContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__66 || _la==T__84) ) {
					((DurationSimpleCommandContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(707);
				match(T__8);
				setState(708);
				((DurationSimpleCommandContext)_localctx).startTime = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((DurationSimpleCommandContext)_localctx).startTime = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(710);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(709);
					((DurationSimpleCommandContext)_localctx).start_time_modifier = time_modifier();
					}
				}

				setState(712);
				match(T__8);
				setState(713);
				((DurationSimpleCommandContext)_localctx).endTime = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__86 || _la==POSITIVE_RANGE) ) {
					((DurationSimpleCommandContext)_localctx).endTime = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(715);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(714);
					((DurationSimpleCommandContext)_localctx).end_time_modifier = time_modifier();
					}
				}

				setState(717);
				match(T__2);
				}
				break;
			case 33:
				_localctx = new DurationComplexCommandContext(_localctx);
				enterOuterAlt(_localctx, 33);
				{
				setState(719);
				match(T__83);
				setState(720);
				match(T__1);
				setState(723);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,86,_ctx) ) {
				case 1:
					{
					setState(721);
					command();
					}
					break;
				case 2:
					{
					setState(722);
					atom();
					}
					break;
				}
				setState(725);
				match(T__8);
				setState(728);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
				case 1:
					{
					setState(726);
					command();
					}
					break;
				case 2:
					{
					setState(727);
					atom();
					}
					break;
				}
				setState(730);
				match(T__8);
				setState(731);
				((DurationComplexCommandContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__66 || _la==T__84) ) {
					((DurationComplexCommandContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(732);
				match(T__8);
				setState(733);
				((DurationComplexCommandContext)_localctx).startTime = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((DurationComplexCommandContext)_localctx).startTime = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(735);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(734);
					((DurationComplexCommandContext)_localctx).start_time_modifier = time_modifier();
					}
				}

				setState(737);
				match(T__8);
				setState(738);
				((DurationComplexCommandContext)_localctx).endTime = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((DurationComplexCommandContext)_localctx).endTime = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(740);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(739);
					((DurationComplexCommandContext)_localctx).end_time_modifier = time_modifier();
					}
				}

				setState(742);
				match(T__2);
				}
				break;
			case 34:
				_localctx = new DurationComplex2CommandContext(_localctx);
				enterOuterAlt(_localctx, 34);
				{
				setState(744);
				match(T__83);
				setState(745);
				match(T__1);
				setState(746);
				before_atom_left();
				setState(747);
				match(T__8);
				setState(748);
				before_atom_right();
				setState(749);
				match(T__8);
				setState(750);
				((DurationComplex2CommandContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__66 || _la==T__84) ) {
					((DurationComplex2CommandContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(751);
				match(T__8);
				setState(752);
				((DurationComplex2CommandContext)_localctx).startTime = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((DurationComplex2CommandContext)_localctx).startTime = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(754);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(753);
					((DurationComplex2CommandContext)_localctx).start_time_modifier = time_modifier();
					}
				}

				setState(756);
				match(T__8);
				setState(757);
				((DurationComplex2CommandContext)_localctx).endTime = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((DurationComplex2CommandContext)_localctx).endTime = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(759);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(758);
					((DurationComplex2CommandContext)_localctx).end_time_modifier = time_modifier();
					}
				}

				setState(761);
				match(T__2);
				}
				break;
			case 35:
				_localctx = new HistoryOfCommandContext(_localctx);
				enterOuterAlt(_localctx, 35);
				{
				setState(766);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__87:
					{
					setState(763);
					match(T__87);
					setState(764);
					match(T__88);
					}
					break;
				case T__89:
					{
					setState(765);
					match(T__89);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(768);
				match(T__1);
				setState(771);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,93,_ctx) ) {
				case 1:
					{
					setState(769);
					command();
					}
					break;
				case 2:
					{
					setState(770);
					atom();
					}
					break;
				}
				setState(773);
				match(T__2);
				}
				break;
			case 36:
				_localctx = new NoHistoryOfCommandContext(_localctx);
				enterOuterAlt(_localctx, 36);
				{
				setState(779);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__90:
					{
					setState(775);
					match(T__90);
					setState(776);
					match(T__87);
					setState(777);
					match(T__88);
					}
					break;
				case T__91:
					{
					setState(778);
					match(T__91);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(781);
				match(T__1);
				setState(784);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
				case 1:
					{
					setState(782);
					command();
					}
					break;
				case 2:
					{
					setState(783);
					atom();
					}
					break;
				}
				setState(786);
				match(T__2);
				}
				break;
			case 37:
				_localctx = new NeverHadCommandContext(_localctx);
				enterOuterAlt(_localctx, 37);
				{
				setState(791);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__68:
					{
					setState(788);
					match(T__68);
					setState(789);
					match(T__92);
					}
					break;
				case T__93:
					{
					setState(790);
					match(T__93);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(793);
				match(T__1);
				setState(796);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,97,_ctx) ) {
				case 1:
					{
					setState(794);
					command();
					}
					break;
				case 2:
					{
					setState(795);
					atom();
					}
					break;
				}
				setState(798);
				match(T__2);
				}
				break;
			case 38:
				_localctx = new CountSimpleCommandContext(_localctx);
				enterOuterAlt(_localctx, 38);
				{
				setState(800);
				match(T__94);
				setState(801);
				match(T__1);
				setState(804);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,98,_ctx) ) {
				case 1:
					{
					setState(802);
					command();
					}
					break;
				case 2:
					{
					setState(803);
					atom();
					}
					break;
				}
				setState(806);
				match(T__8);
				setState(807);
				((CountSimpleCommandContext)_localctx).startCount = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((CountSimpleCommandContext)_localctx).startCount = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(808);
				match(T__8);
				setState(809);
				((CountSimpleCommandContext)_localctx).endCount = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((CountSimpleCommandContext)_localctx).endCount = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(810);
				match(T__2);
				}
				break;
			case 39:
				_localctx = new CountComplexCommandContext(_localctx);
				enterOuterAlt(_localctx, 39);
				{
				setState(812);
				match(T__94);
				setState(813);
				match(T__1);
				setState(816);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,99,_ctx) ) {
				case 1:
					{
					setState(814);
					command();
					}
					break;
				case 2:
					{
					setState(815);
					atom();
					}
					break;
				}
				setState(818);
				match(T__8);
				setState(821);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,100,_ctx) ) {
				case 1:
					{
					setState(819);
					command();
					}
					break;
				case 2:
					{
					setState(820);
					atom();
					}
					break;
				}
				setState(823);
				match(T__8);
				setState(824);
				((CountComplexCommandContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__66 || _la==T__84) ) {
					((CountComplexCommandContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(825);
				match(T__8);
				setState(826);
				((CountComplexCommandContext)_localctx).startCount = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((CountComplexCommandContext)_localctx).startCount = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(827);
				match(T__8);
				setState(828);
				((CountComplexCommandContext)_localctx).endCount = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((CountComplexCommandContext)_localctx).endCount = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(829);
				match(T__2);
				}
				break;
			case 40:
				_localctx = new CountComplex2CommandContext(_localctx);
				enterOuterAlt(_localctx, 40);
				{
				setState(831);
				match(T__94);
				setState(832);
				match(T__1);
				setState(833);
				before_atom_left();
				setState(834);
				match(T__8);
				setState(835);
				before_atom_right();
				setState(836);
				match(T__8);
				setState(837);
				((CountComplex2CommandContext)_localctx).type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__66 || _la==T__84) ) {
					((CountComplex2CommandContext)_localctx).type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(838);
				match(T__8);
				setState(839);
				((CountComplex2CommandContext)_localctx).startCount = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((CountComplex2CommandContext)_localctx).startCount = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(840);
				match(T__8);
				setState(841);
				((CountComplex2CommandContext)_localctx).endCount = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((CountComplex2CommandContext)_localctx).endCount = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(842);
				match(T__2);
				}
				break;
			case 41:
				_localctx = new EqualCommandContext(_localctx);
				enterOuterAlt(_localctx, 41);
				{
				setState(844);
				_la = _input.LA(1);
				if ( !(_la==T__95 || _la==T__96) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(845);
				match(T__1);
				setState(848);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,101,_ctx) ) {
				case 1:
					{
					setState(846);
					command();
					}
					break;
				case 2:
					{
					setState(847);
					atom();
					}
					break;
				}
				setState(850);
				match(T__8);
				setState(853);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,102,_ctx) ) {
				case 1:
					{
					setState(851);
					command();
					}
					break;
				case 2:
					{
					setState(852);
					atom();
					}
					break;
				}
				setState(855);
				match(T__2);
				}
				break;
			case 42:
				_localctx = new ContainsCommandContext(_localctx);
				enterOuterAlt(_localctx, 42);
				{
				setState(857);
				_la = _input.LA(1);
				if ( !(_la==T__97 || _la==T__98) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(858);
				match(T__1);
				setState(861);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
				case 1:
					{
					setState(859);
					command();
					}
					break;
				case 2:
					{
					setState(860);
					atom();
					}
					break;
				}
				setState(864);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(863);
					((ContainsCommandContext)_localctx).start_type = match(T__99);
					}
				}

				setState(866);
				match(T__8);
				setState(869);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
				case 1:
					{
					setState(867);
					command();
					}
					break;
				case 2:
					{
					setState(868);
					atom();
					}
					break;
				}
				setState(872);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(871);
					((ContainsCommandContext)_localctx).end_type = match(T__99);
					}
				}

				setState(874);
				match(T__2);
				}
				break;
			case 43:
				_localctx = new IdenticalCommandContext(_localctx);
				enterOuterAlt(_localctx, 43);
				{
				setState(876);
				match(T__100);
				setState(877);
				match(T__1);
				setState(880);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,107,_ctx) ) {
				case 1:
					{
					setState(878);
					command();
					}
					break;
				case 2:
					{
					setState(879);
					atom();
					}
					break;
				}
				setState(882);
				match(T__8);
				setState(885);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,108,_ctx) ) {
				case 1:
					{
					setState(883);
					command();
					}
					break;
				case 2:
					{
					setState(884);
					atom();
					}
					break;
				}
				setState(887);
				match(T__2);
				}
				break;
			case 44:
				_localctx = new SameCommandContext(_localctx);
				enterOuterAlt(_localctx, 44);
				{
				setState(889);
				match(T__101);
				setState(890);
				match(T__1);
				setState(893);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,109,_ctx) ) {
				case 1:
					{
					setState(891);
					command();
					}
					break;
				case 2:
					{
					setState(892);
					atom();
					}
					break;
				}
				setState(895);
				match(T__8);
				setState(898);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,110,_ctx) ) {
				case 1:
					{
					setState(896);
					command();
					}
					break;
				case 2:
					{
					setState(897);
					atom();
					}
					break;
				}
				setState(900);
				match(T__2);
				}
				break;
			case 45:
				_localctx = new DiffCommandContext(_localctx);
				enterOuterAlt(_localctx, 45);
				{
				setState(902);
				match(T__102);
				setState(903);
				match(T__1);
				setState(906);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,111,_ctx) ) {
				case 1:
					{
					setState(904);
					command();
					}
					break;
				case 2:
					{
					setState(905);
					atom();
					}
					break;
				}
				setState(908);
				match(T__8);
				setState(911);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,112,_ctx) ) {
				case 1:
					{
					setState(909);
					command();
					}
					break;
				case 2:
					{
					setState(910);
					atom();
					}
					break;
				}
				setState(913);
				match(T__2);
				}
				break;
			case 46:
				_localctx = new MergeCommandContext(_localctx);
				enterOuterAlt(_localctx, 46);
				{
				setState(915);
				match(T__103);
				setState(916);
				match(T__1);
				setState(919);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,113,_ctx) ) {
				case 1:
					{
					setState(917);
					command();
					}
					break;
				case 2:
					{
					setState(918);
					atom();
					}
					break;
				}
				setState(921);
				match(T__8);
				setState(924);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,114,_ctx) ) {
				case 1:
					{
					setState(922);
					command();
					}
					break;
				case 2:
					{
					setState(923);
					atom();
					}
					break;
				}
				setState(926);
				match(T__2);
				}
				break;
			case 47:
				_localctx = new PrimaryCommandContext(_localctx);
				enterOuterAlt(_localctx, 47);
				{
				setState(928);
				match(T__104);
				setState(929);
				match(T__1);
				setState(930);
				icd9_atom();
				setState(931);
				match(T__2);
				}
				break;
			case 48:
				_localctx = new OriginalCommandContext(_localctx);
				enterOuterAlt(_localctx, 48);
				{
				setState(933);
				match(T__105);
				setState(934);
				match(T__1);
				setState(935);
				icd9_atom();
				setState(936);
				match(T__2);
				}
				break;
			case 49:
				_localctx = new OriginalPrimaryCommandContext(_localctx);
				enterOuterAlt(_localctx, 49);
				{
				setState(938);
				match(T__105);
				setState(939);
				match(T__1);
				setState(940);
				match(T__104);
				setState(941);
				match(T__1);
				setState(942);
				icd9_atom();
				setState(943);
				match(T__2);
				setState(944);
				match(T__2);
				}
				break;
			case 50:
				_localctx = new OriginalPrimaryCommandContext(_localctx);
				enterOuterAlt(_localctx, 50);
				{
				setState(946);
				match(T__104);
				setState(947);
				match(T__1);
				setState(948);
				match(T__105);
				setState(949);
				match(T__1);
				setState(950);
				icd9_atom();
				setState(951);
				match(T__2);
				setState(952);
				match(T__2);
				}
				break;
			case 51:
				_localctx = new DepartmentCommandContext(_localctx);
				enterOuterAlt(_localctx, 51);
				{
				setState(954);
				_la = _input.LA(1);
				if ( !(_la==T__106 || _la==T__107) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(955);
				match(T__9);
				setState(956);
				match(TERM);
				}
				break;
			case 52:
				_localctx = new NoteTypeCommandContext(_localctx);
				enterOuterAlt(_localctx, 52);
				{
				setState(961);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__108:
					{
					setState(957);
					match(T__108);
					}
					break;
				case T__109:
					{
					setState(958);
					match(T__109);
					}
					break;
				case T__110:
					{
					{
					setState(959);
					match(T__110);
					setState(960);
					match(T__111);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(963);
				match(T__9);
				setState(964);
				match(TERM);
				}
				break;
			case 53:
				_localctx = new VisitTypeCommandContext(_localctx);
				enterOuterAlt(_localctx, 53);
				{
				setState(969);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__112:
					{
					setState(965);
					match(T__112);
					}
					break;
				case T__113:
					{
					setState(966);
					match(T__113);
					}
					break;
				case T__114:
					{
					{
					setState(967);
					match(T__114);
					setState(968);
					match(T__111);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(971);
				match(T__9);
				setState(972);
				match(TERM);
				}
				break;
			case 54:
				_localctx = new AnyVisitTypeCommandContext(_localctx);
				enterOuterAlt(_localctx, 54);
				{
				setState(977);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__112:
					{
					setState(973);
					match(T__112);
					}
					break;
				case T__113:
					{
					setState(974);
					match(T__113);
					}
					break;
				case T__114:
					{
					{
					setState(975);
					match(T__114);
					setState(976);
					match(T__111);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 55:
				_localctx = new DrugsSimpleContext(_localctx);
				enterOuterAlt(_localctx, 55);
				{
				setState(979);
				_la = _input.LA(1);
				if ( !(((((_la - 116)) & ~0x3f) == 0 && ((1L << (_la - 116)) & ((1L << (T__115 - 116)) | (1L << (T__116 - 116)) | (1L << (T__117 - 116)) | (1L << (T__118 - 116)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(980);
				match(T__1);
				setState(981);
				match(HASRXNORM);
				setState(982);
				match(T__9);
				{
				setState(983);
				match(POSITIVE_RANGE);
				}
				setState(984);
				match(T__2);
				}
				break;
			case 56:
				_localctx = new DrugsRouteContext(_localctx);
				enterOuterAlt(_localctx, 56);
				{
				setState(985);
				_la = _input.LA(1);
				if ( !(((((_la - 116)) & ~0x3f) == 0 && ((1L << (_la - 116)) & ((1L << (T__115 - 116)) | (1L << (T__116 - 116)) | (1L << (T__117 - 116)) | (1L << (T__118 - 116)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(986);
				match(T__1);
				setState(987);
				match(HASRXNORM);
				setState(988);
				match(T__9);
				{
				setState(989);
				match(POSITIVE_RANGE);
				}
				setState(990);
				match(T__8);
				setState(991);
				match(T__119);
				setState(992);
				match(T__9);
				setState(993);
				match(TERM);
				setState(994);
				match(T__2);
				}
				break;
			case 57:
				_localctx = new DrugsStatusContext(_localctx);
				enterOuterAlt(_localctx, 57);
				{
				setState(995);
				_la = _input.LA(1);
				if ( !(((((_la - 116)) & ~0x3f) == 0 && ((1L << (_la - 116)) & ((1L << (T__115 - 116)) | (1L << (T__116 - 116)) | (1L << (T__117 - 116)) | (1L << (T__118 - 116)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(996);
				match(T__1);
				setState(997);
				match(HASRXNORM);
				setState(998);
				match(T__9);
				{
				setState(999);
				match(POSITIVE_RANGE);
				}
				setState(1000);
				match(T__8);
				setState(1001);
				match(T__120);
				setState(1002);
				match(T__9);
				setState(1003);
				match(TERM);
				setState(1004);
				match(T__2);
				}
				break;
			case 58:
				_localctx = new DrugsRouteStatusContext(_localctx);
				enterOuterAlt(_localctx, 58);
				{
				setState(1005);
				_la = _input.LA(1);
				if ( !(((((_la - 116)) & ~0x3f) == 0 && ((1L << (_la - 116)) & ((1L << (T__115 - 116)) | (1L << (T__116 - 116)) | (1L << (T__117 - 116)) | (1L << (T__118 - 116)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1006);
				match(T__1);
				setState(1007);
				match(HASRXNORM);
				setState(1008);
				match(T__9);
				{
				setState(1009);
				match(POSITIVE_RANGE);
				}
				setState(1010);
				match(T__8);
				setState(1011);
				match(T__119);
				setState(1012);
				match(T__9);
				setState(1013);
				match(TERM);
				setState(1014);
				match(T__8);
				setState(1015);
				match(T__120);
				setState(1016);
				match(T__9);
				setState(1017);
				match(TERM);
				setState(1018);
				match(T__2);
				}
				break;
			case 59:
				_localctx = new DrugsStatusRouteContext(_localctx);
				enterOuterAlt(_localctx, 59);
				{
				setState(1019);
				_la = _input.LA(1);
				if ( !(((((_la - 116)) & ~0x3f) == 0 && ((1L << (_la - 116)) & ((1L << (T__115 - 116)) | (1L << (T__116 - 116)) | (1L << (T__117 - 116)) | (1L << (T__118 - 116)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1020);
				match(T__1);
				setState(1021);
				match(HASRXNORM);
				setState(1022);
				match(T__9);
				{
				setState(1023);
				match(POSITIVE_RANGE);
				}
				setState(1024);
				match(T__8);
				setState(1025);
				match(T__120);
				setState(1026);
				match(T__9);
				setState(1027);
				match(TERM);
				setState(1028);
				match(T__8);
				setState(1029);
				match(T__119);
				setState(1030);
				match(T__9);
				setState(1031);
				match(TERM);
				setState(1032);
				match(T__2);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
	 
		public AtomContext() { }
		public void copyFrom(AtomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NoteAtomCommandContext extends AtomContext {
		public Note_atomContext note_atom() {
			return getRuleContext(Note_atomContext.class,0);
		}
		public NoteAtomCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoteAtomCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoteAtomCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoteAtomCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AnyVitalsCommandContext extends AtomContext {
		public AnyVitalsCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAnyVitalsCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAnyVitalsCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAnyVitalsCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LabValuesCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public List<Double_valueContext> double_value() {
			return getRuleContexts(Double_valueContext.class);
		}
		public Double_valueContext double_value(int i) {
			return getRuleContext(Double_valueContext.class,i);
		}
		public LabValuesCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterLabValuesCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitLabValuesCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitLabValuesCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasCptCommandContext extends AtomContext {
		public TerminalNode HASCPT() { return getToken(ExprParser.HASCPT, 0); }
		public TerminalNode CPT() { return getToken(ExprParser.CPT, 0); }
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public HasCptCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasCptCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasCptCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasCptCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Icd9AtomCommandContext extends AtomContext {
		public Icd9_atomContext icd9_atom() {
			return getRuleContext(Icd9_atomContext.class,0);
		}
		public Icd9AtomCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIcd9AtomCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIcd9AtomCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitIcd9AtomCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PatientsCommandContext extends AtomContext {
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public PatientsCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterPatientsCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitPatientsCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitPatientsCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasRxNormCommandContext extends AtomContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public HasRxNormCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasRxNormCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasRxNormCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasRxNormCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EncountersCommandContext extends AtomContext {
		public TerminalNode ENCOUNTERS() { return getToken(ExprParser.ENCOUNTERS, 0); }
		public EncountersCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEncountersCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEncountersCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEncountersCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoteWithTypeCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public Note_atomsContext note_atoms() {
			return getRuleContext(Note_atomsContext.class,0);
		}
		public NoteWithTypeCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoteWithTypeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoteWithTypeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoteWithTypeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoteBooleanCommandContext extends AtomContext {
		public CommandsContext commands() {
			return getRuleContext(CommandsContext.class,0);
		}
		public NoteBooleanCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoteBooleanCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoteBooleanCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoteBooleanCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AtcCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public AtcCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAtcCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAtcCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAtcCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullCommandContext extends AtomContext {
		public NullCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNullCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNullCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNullCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EthnicityCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public EthnicityCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEthnicityCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEthnicityCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEthnicityCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasAnyRxNormCommandContext extends AtomContext {
		public TerminalNode HASRXNORM() { return getToken(ExprParser.HASRXNORM, 0); }
		public HasAnyRxNormCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasAnyRxNormCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasAnyRxNormCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasAnyRxNormCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SimpleVitalsContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public SimpleVitalsContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterSimpleVitals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitSimpleVitals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitSimpleVitals(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecordStartCommandContext extends AtomContext {
		public RecordStartCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterRecordStartCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitRecordStartCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitRecordStartCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AgeCommandContext extends AtomContext {
		public Token minAge;
		public Time_modifierContext minAgeModifier;
		public Token maxAge;
		public Time_modifierContext maxAgeModifier;
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public AgeCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAgeCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAgeCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAgeCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SnomedCommandContext extends AtomContext {
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public SnomedCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterSnomedCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitSnomedCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitSnomedCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AnyLabsCommandContext extends AtomContext {
		public AnyLabsCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAnyLabsCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAnyLabsCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAnyLabsCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class YearCommandContext extends AtomContext {
		public Token start;
		public Token end;
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public YearCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterYearCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitYearCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitYearCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GenderCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public GenderCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterGenderCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitGenderCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitGenderCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RaceCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public RaceCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterRaceCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitRaceCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitRaceCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoteCommandContext extends AtomContext {
		public Note_atomsContext note_atoms() {
			return getRuleContext(Note_atomsContext.class,0);
		}
		public NoteCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoteCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoteCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoteCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SimpleLabsContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public SimpleLabsContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterSimpleLabs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitSimpleLabs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitSimpleLabs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoteTimeInstancesCommandContext extends AtomContext {
		public NoteTimeInstancesCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNoteTimeInstancesCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNoteTimeInstancesCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNoteTimeInstancesCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TimelineCommandContext extends AtomContext {
		public TerminalNode TIMELINE() { return getToken(ExprParser.TIMELINE, 0); }
		public TimelineCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterTimelineCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitTimelineCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitTimelineCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PatientsSingleCommandContext extends AtomContext {
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public PatientsSingleCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterPatientsSingleCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitPatientsSingleCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitPatientsSingleCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecordEndCommandContext extends AtomContext {
		public RecordEndCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterRecordEndCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitRecordEndCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitRecordEndCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AnySnomedCommandContext extends AtomContext {
		public AnySnomedCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAnySnomedCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAnySnomedCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAnySnomedCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EachNameCommandContext extends AtomContext {
		public Each_nameContext each_name() {
			return getRuleContext(Each_nameContext.class,0);
		}
		public EachNameCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEachNameCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEachNameCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEachNameCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DeathCommandContext extends AtomContext {
		public TerminalNode DEATH() { return getToken(ExprParser.DEATH, 0); }
		public DeathCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDeathCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDeathCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDeathCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasAnyCptCommandContext extends AtomContext {
		public HasAnyCptCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasAnyCptCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasAnyCptCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasAnyCptCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VitalsCommandContext extends AtomContext {
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public List<Double_valueContext> double_value() {
			return getRuleContexts(Double_valueContext.class);
		}
		public Double_valueContext double_value(int i) {
			return getRuleContext(Double_valueContext.class,i);
		}
		public VitalsCommandContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterVitalsCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitVitalsCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitVitalsCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComplexLabsContext extends AtomContext {
		public Token labType;
		public List<TerminalNode> TERM() { return getTokens(ExprParser.TERM); }
		public TerminalNode TERM(int i) {
			return getToken(ExprParser.TERM, i);
		}
		public ComplexLabsContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterComplexLabs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitComplexLabs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitComplexLabs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumericIntervalContext extends AtomContext {
		public Token startTimeValue;
		public Time_modifierContext startTimeModifier;
		public Token endTimeValue;
		public Time_modifierContext endTimeModifier;
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public NumericIntervalContext(AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNumericInterval(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNumericInterval(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNumericInterval(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_atom);
		int _la;
		try {
			int _alt;
			setState(1179);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,127,_ctx) ) {
			case 1:
				_localctx = new HasCptCommandContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1035);
				match(HASCPT);
				setState(1036);
				match(T__9);
				setState(1037);
				_la = _input.LA(1);
				if ( !(_la==CPT || _la==POSITIVE_RANGE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 2:
				_localctx = new HasAnyCptCommandContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1038);
				match(HASCPT);
				}
				break;
			case 3:
				_localctx = new HasRxNormCommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(1039);
				match(HASRXNORM);
				setState(1040);
				match(T__9);
				{
				setState(1041);
				match(POSITIVE_RANGE);
				}
				}
				break;
			case 4:
				_localctx = new HasAnyRxNormCommandContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(1042);
				match(HASRXNORM);
				}
				break;
			case 5:
				_localctx = new GenderCommandContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(1043);
				match(T__121);
				setState(1044);
				match(T__9);
				setState(1045);
				match(TERM);
				}
				break;
			case 6:
				_localctx = new YearCommandContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(1046);
				_la = _input.LA(1);
				if ( !(_la==T__122 || _la==T__123) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1047);
				match(T__1);
				setState(1048);
				((YearCommandContext)_localctx).start = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==POSITIVE_RANGE) ) {
					((YearCommandContext)_localctx).start = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1049);
				match(T__8);
				setState(1050);
				((YearCommandContext)_localctx).end = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__86 || _la==POSITIVE_RANGE) ) {
					((YearCommandContext)_localctx).end = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1051);
				match(T__2);
				}
				break;
			case 7:
				_localctx = new RaceCommandContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(1052);
				match(T__124);
				setState(1053);
				match(T__9);
				setState(1054);
				match(TERM);
				}
				break;
			case 8:
				_localctx = new EthnicityCommandContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(1055);
				match(T__125);
				setState(1056);
				match(T__9);
				setState(1057);
				match(TERM);
				}
				break;
			case 9:
				_localctx = new DeathCommandContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(1058);
				match(DEATH);
				}
				break;
			case 10:
				_localctx = new AnySnomedCommandContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(1059);
				match(T__126);
				}
				break;
			case 11:
				_localctx = new SnomedCommandContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(1060);
				match(T__126);
				setState(1061);
				match(T__9);
				setState(1062);
				match(POSITIVE_RANGE);
				}
				break;
			case 12:
				_localctx = new EncountersCommandContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(1063);
				match(ENCOUNTERS);
				}
				break;
			case 13:
				_localctx = new NoteTimeInstancesCommandContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(1064);
				_la = _input.LA(1);
				if ( !(_la==T__110 || _la==T__127) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 14:
				_localctx = new TimelineCommandContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(1065);
				match(TIMELINE);
				}
				break;
			case 15:
				_localctx = new RecordStartCommandContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(1069);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__128:
					{
					setState(1066);
					match(T__128);
					}
					break;
				case T__129:
					{
					setState(1067);
					match(T__129);
					setState(1068);
					match(T__11);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 16:
				_localctx = new RecordEndCommandContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(1074);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__130:
					{
					setState(1071);
					match(T__130);
					}
					break;
				case T__129:
					{
					setState(1072);
					match(T__129);
					setState(1073);
					match(T__12);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 17:
				_localctx = new NoteCommandContext(_localctx);
				enterOuterAlt(_localctx, 17);
				{
				setState(1076);
				_la = _input.LA(1);
				if ( !(_la==T__110 || _la==T__127) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1077);
				match(T__1);
				setState(1078);
				note_atoms();
				setState(1079);
				match(T__2);
				}
				break;
			case 18:
				_localctx = new NoteBooleanCommandContext(_localctx);
				enterOuterAlt(_localctx, 18);
				{
				setState(1081);
				_la = _input.LA(1);
				if ( !(_la==T__110 || _la==T__127) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1082);
				match(T__1);
				setState(1083);
				commands(0);
				setState(1084);
				match(T__2);
				}
				break;
			case 19:
				_localctx = new NoteWithTypeCommandContext(_localctx);
				enterOuterAlt(_localctx, 19);
				{
				setState(1086);
				_la = _input.LA(1);
				if ( !(_la==T__110 || _la==T__127) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1087);
				match(T__1);
				setState(1092);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__108:
					{
					setState(1088);
					match(T__108);
					}
					break;
				case T__109:
					{
					setState(1089);
					match(T__109);
					}
					break;
				case T__110:
					{
					{
					setState(1090);
					match(T__110);
					setState(1091);
					match(T__111);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(1094);
				match(T__9);
				setState(1095);
				match(TERM);
				setState(1096);
				match(T__8);
				setState(1097);
				note_atoms();
				setState(1098);
				match(T__2);
				}
				break;
			case 20:
				_localctx = new NoteAtomCommandContext(_localctx);
				enterOuterAlt(_localctx, 20);
				{
				setState(1100);
				note_atom();
				}
				break;
			case 21:
				_localctx = new AgeCommandContext(_localctx);
				enterOuterAlt(_localctx, 21);
				{
				setState(1101);
				match(T__131);
				setState(1102);
				match(T__1);
				setState(1103);
				((AgeCommandContext)_localctx).minAge = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((AgeCommandContext)_localctx).minAge = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1105);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1104);
					((AgeCommandContext)_localctx).minAgeModifier = time_modifier();
					}
				}

				setState(1107);
				match(T__8);
				setState(1108);
				((AgeCommandContext)_localctx).maxAge = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((AgeCommandContext)_localctx).maxAge = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1110);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1109);
					((AgeCommandContext)_localctx).maxAgeModifier = time_modifier();
					}
				}

				setState(1112);
				match(T__2);
				}
				break;
			case 22:
				_localctx = new VitalsCommandContext(_localctx);
				enterOuterAlt(_localctx, 22);
				{
				setState(1113);
				match(T__132);
				setState(1114);
				match(T__1);
				setState(1115);
				match(TERM);
				setState(1116);
				match(T__8);
				setState(1117);
				double_value();
				setState(1118);
				match(T__8);
				setState(1119);
				double_value();
				setState(1120);
				match(T__2);
				}
				break;
			case 23:
				_localctx = new LabValuesCommandContext(_localctx);
				enterOuterAlt(_localctx, 23);
				{
				setState(1122);
				_la = _input.LA(1);
				if ( !(_la==T__133 || _la==T__134) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1123);
				match(T__1);
				setState(1124);
				match(TERM);
				setState(1125);
				match(T__8);
				setState(1126);
				double_value();
				setState(1127);
				match(T__8);
				setState(1128);
				double_value();
				setState(1129);
				match(T__2);
				}
				break;
			case 24:
				_localctx = new SimpleVitalsContext(_localctx);
				enterOuterAlt(_localctx, 24);
				{
				setState(1131);
				match(T__132);
				setState(1132);
				match(T__1);
				setState(1133);
				match(TERM);
				setState(1134);
				match(T__2);
				}
				break;
			case 25:
				_localctx = new AnyLabsCommandContext(_localctx);
				enterOuterAlt(_localctx, 25);
				{
				setState(1135);
				_la = _input.LA(1);
				if ( !(_la==T__133 || _la==T__134) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 26:
				_localctx = new AnyVitalsCommandContext(_localctx);
				enterOuterAlt(_localctx, 26);
				{
				setState(1136);
				match(T__132);
				}
				break;
			case 27:
				_localctx = new ComplexLabsContext(_localctx);
				enterOuterAlt(_localctx, 27);
				{
				setState(1137);
				_la = _input.LA(1);
				if ( !(_la==T__133 || _la==T__134) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1138);
				match(T__1);
				setState(1139);
				match(TERM);
				setState(1140);
				match(T__8);
				setState(1141);
				((ComplexLabsContext)_localctx).labType = match(TERM);
				setState(1142);
				match(T__2);
				}
				break;
			case 28:
				_localctx = new SimpleLabsContext(_localctx);
				enterOuterAlt(_localctx, 28);
				{
				setState(1143);
				_la = _input.LA(1);
				if ( !(_la==T__133 || _la==T__134) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1144);
				match(T__1);
				setState(1145);
				match(TERM);
				setState(1146);
				match(T__2);
				}
				break;
			case 29:
				_localctx = new NumericIntervalContext(_localctx);
				enterOuterAlt(_localctx, 29);
				{
				setState(1147);
				match(INTERVAL);
				setState(1148);
				match(T__1);
				setState(1149);
				((NumericIntervalContext)_localctx).startTimeValue = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((NumericIntervalContext)_localctx).startTimeValue = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1151);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1150);
					((NumericIntervalContext)_localctx).startTimeModifier = time_modifier();
					}
				}

				setState(1153);
				match(T__8);
				setState(1154);
				((NumericIntervalContext)_localctx).endTimeValue = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__85 || _la==T__86 || _la==POSITIVE_RANGE) ) {
					((NumericIntervalContext)_localctx).endTimeValue = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1156);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1155);
					((NumericIntervalContext)_localctx).endTimeModifier = time_modifier();
					}
				}

				setState(1158);
				match(T__2);
				}
				break;
			case 30:
				_localctx = new NullCommandContext(_localctx);
				enterOuterAlt(_localctx, 30);
				{
				setState(1159);
				_la = _input.LA(1);
				if ( !(_la==T__51 || _la==T__135) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 31:
				_localctx = new AtcCommandContext(_localctx);
				enterOuterAlt(_localctx, 31);
				{
				setState(1160);
				match(T__136);
				setState(1161);
				match(T__9);
				setState(1162);
				match(TERM);
				}
				break;
			case 32:
				_localctx = new Icd9AtomCommandContext(_localctx);
				enterOuterAlt(_localctx, 32);
				{
				setState(1163);
				icd9_atom();
				}
				break;
			case 33:
				_localctx = new PatientsCommandContext(_localctx);
				enterOuterAlt(_localctx, 33);
				{
				setState(1164);
				_la = _input.LA(1);
				if ( !(_la==T__59 || _la==T__137) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1165);
				match(T__1);
				setState(1166);
				match(POSITIVE_RANGE);
				setState(1169); 
				_errHandler.sync(this);
				_alt = 1+1;
				do {
					switch (_alt) {
					case 1+1:
						{
						{
						setState(1167);
						match(T__8);
						setState(1168);
						match(POSITIVE_RANGE);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1171); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,126,_ctx);
				} while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(1173);
				match(T__2);
				}
				break;
			case 34:
				_localctx = new PatientsSingleCommandContext(_localctx);
				enterOuterAlt(_localctx, 34);
				{
				setState(1174);
				_la = _input.LA(1);
				if ( !(_la==T__59 || _la==T__137) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1175);
				match(T__1);
				setState(1176);
				match(POSITIVE_RANGE);
				setState(1177);
				match(T__2);
				}
				break;
			case 35:
				_localctx = new EachNameCommandContext(_localctx);
				enterOuterAlt(_localctx, 35);
				{
				setState(1178);
				each_name();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Icd9_atomContext extends ParserRuleContext {
		public Icd9_atomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_icd9_atom; }
	 
		public Icd9_atomContext() { }
		public void copyFrom(Icd9_atomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class HasIcd9CommandContext extends Icd9_atomContext {
		public TerminalNode HASICD9() { return getToken(ExprParser.HASICD9, 0); }
		public TerminalNode ICD9() { return getToken(ExprParser.ICD9, 0); }
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public TerminalNode ICD10() { return getToken(ExprParser.ICD10, 0); }
		public HasIcd9CommandContext(Icd9_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasIcd9Command(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasIcd9Command(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasIcd9Command(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasAnyIcd9CodesContext extends Icd9_atomContext {
		public HasAnyIcd9CodesContext(Icd9_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasAnyIcd9Codes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasAnyIcd9Codes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasAnyIcd9Codes(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasAnyIcd10CodesContext extends Icd9_atomContext {
		public HasAnyIcd10CodesContext(Icd9_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasAnyIcd10Codes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasAnyIcd10Codes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasAnyIcd10Codes(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasIcd10CommandContext extends Icd9_atomContext {
		public TerminalNode HASICD10() { return getToken(ExprParser.HASICD10, 0); }
		public TerminalNode ICD10() { return getToken(ExprParser.ICD10, 0); }
		public HasIcd10CommandContext(Icd9_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterHasIcd10Command(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitHasIcd10Command(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitHasIcd10Command(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Icd9_atomContext icd9_atom() throws RecognitionException {
		Icd9_atomContext _localctx = new Icd9_atomContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_icd9_atom);
		int _la;
		try {
			setState(1189);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,128,_ctx) ) {
			case 1:
				_localctx = new HasIcd9CommandContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1181);
				match(HASICD9);
				setState(1182);
				match(T__9);
				setState(1183);
				_la = _input.LA(1);
				if ( !(((((_la - 183)) & ~0x3f) == 0 && ((1L << (_la - 183)) & ((1L << (ICD10 - 183)) | (1L << (ICD9 - 183)) | (1L << (POSITIVE_RANGE - 183)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 2:
				_localctx = new HasAnyIcd9CodesContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1184);
				match(HASICD9);
				}
				break;
			case 3:
				_localctx = new HasIcd10CommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(1185);
				match(HASICD10);
				setState(1186);
				match(T__9);
				setState(1187);
				match(ICD10);
				}
				break;
			case 4:
				_localctx = new HasAnyIcd10CodesContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(1188);
				match(HASICD10);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Double_valueContext extends ParserRuleContext {
		public List<TerminalNode> POSITIVE_RANGE() { return getTokens(ExprParser.POSITIVE_RANGE); }
		public TerminalNode POSITIVE_RANGE(int i) {
			return getToken(ExprParser.POSITIVE_RANGE, i);
		}
		public TerminalNode NEGATIVE_RANGE() { return getToken(ExprParser.NEGATIVE_RANGE, 0); }
		public TerminalNode ICD9() { return getToken(ExprParser.ICD9, 0); }
		public Double_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_double_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterDouble_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitDouble_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitDouble_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Double_valueContext double_value() throws RecognitionException {
		Double_valueContext _localctx = new Double_valueContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_double_value);
		try {
			setState(1202);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,129,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1191);
				match(POSITIVE_RANGE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1192);
				match(NEGATIVE_RANGE);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(1193);
				match(POSITIVE_RANGE);
				setState(1194);
				match(T__138);
				setState(1195);
				match(POSITIVE_RANGE);
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(1196);
				match(NEGATIVE_RANGE);
				setState(1197);
				match(T__138);
				setState(1198);
				match(POSITIVE_RANGE);
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1199);
				match(ICD9);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1200);
				match(T__86);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1201);
				match(T__85);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Before_atom_leftContext extends ParserRuleContext {
		public Before_atom_leftContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before_atom_left; }
	 
		public Before_atom_leftContext() { }
		public void copyFrom(Before_atom_leftContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtomWithAsterisk1Context extends Before_atom_leftContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AtomWithAsterisk1Context(Before_atom_leftContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAtomWithAsterisk1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAtomWithAsterisk1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAtomWithAsterisk1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Before_atom_leftContext before_atom_left() throws RecognitionException {
		Before_atom_leftContext _localctx = new Before_atom_leftContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_before_atom_left);
		int _la;
		try {
			_localctx = new AtomWithAsterisk1Context(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(1206);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,130,_ctx) ) {
			case 1:
				{
				setState(1204);
				command();
				}
				break;
			case 2:
				{
				setState(1205);
				atom();
				}
				break;
			}
			setState(1209);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__99) {
				{
				setState(1208);
				match(T__99);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Before_atom_rightContext extends ParserRuleContext {
		public Before_atom_rightContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before_atom_right; }
	 
		public Before_atom_rightContext() { }
		public void copyFrom(Before_atom_rightContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtomWithAsterisk2Context extends Before_atom_rightContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AtomWithAsterisk2Context(Before_atom_rightContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAtomWithAsterisk2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAtomWithAsterisk2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAtomWithAsterisk2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Before_atom_rightContext before_atom_right() throws RecognitionException {
		Before_atom_rightContext _localctx = new Before_atom_rightContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_before_atom_right);
		try {
			_localctx = new AtomWithAsterisk2Context(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(1213);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,132,_ctx) ) {
			case 1:
				{
				setState(1211);
				command();
				}
				break;
			case 2:
				{
				setState(1212);
				atom();
				}
				break;
			}
			setState(1216);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,133,_ctx) ) {
			case 1:
				{
				setState(1215);
				match(T__99);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Before_atom_left_leftContext extends ParserRuleContext {
		public Before_atom_left_leftContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before_atom_left_left; }
	 
		public Before_atom_left_leftContext() { }
		public void copyFrom(Before_atom_left_leftContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtomWithAsterisk3Context extends Before_atom_left_leftContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AtomWithAsterisk3Context(Before_atom_left_leftContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAtomWithAsterisk3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAtomWithAsterisk3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAtomWithAsterisk3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Before_atom_left_leftContext before_atom_left_left() throws RecognitionException {
		Before_atom_left_leftContext _localctx = new Before_atom_left_leftContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_before_atom_left_left);
		int _la;
		try {
			_localctx = new AtomWithAsterisk3Context(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(1220);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,134,_ctx) ) {
			case 1:
				{
				setState(1218);
				command();
				}
				break;
			case 2:
				{
				setState(1219);
				atom();
				}
				break;
			}
			setState(1223);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__99) {
				{
				setState(1222);
				match(T__99);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Before_atom_left_no_asteriskContext extends ParserRuleContext {
		public Before_atom_left_no_asteriskContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before_atom_left_no_asterisk; }
	 
		public Before_atom_left_no_asteriskContext() { }
		public void copyFrom(Before_atom_left_no_asteriskContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtomWithAsterisk4Context extends Before_atom_left_no_asteriskContext {
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AtomWithAsterisk4Context(Before_atom_left_no_asteriskContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAtomWithAsterisk4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAtomWithAsterisk4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAtomWithAsterisk4(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Before_atom_left_no_asteriskContext before_atom_left_no_asterisk() throws RecognitionException {
		Before_atom_left_no_asteriskContext _localctx = new Before_atom_left_no_asteriskContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_before_atom_left_no_asterisk);
		try {
			_localctx = new AtomWithAsterisk4Context(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(1227);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,136,_ctx) ) {
			case 1:
				{
				setState(1225);
				command();
				}
				break;
			case 2:
				{
				setState(1226);
				atom();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BeforeContext extends ParserRuleContext {
		public BeforeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before; }
	 
		public BeforeContext() { }
		public void copyFrom(BeforeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BeforeBothContext extends BeforeContext {
		public Token command_type;
		public List<Before_atom_left_leftContext> before_atom_left_left() {
			return getRuleContexts(Before_atom_left_leftContext.class);
		}
		public Before_atom_left_leftContext before_atom_left_left(int i) {
			return getRuleContext(Before_atom_left_leftContext.class,i);
		}
		public List<Before_atom_left_no_asteriskContext> before_atom_left_no_asterisk() {
			return getRuleContexts(Before_atom_left_no_asteriskContext.class);
		}
		public Before_atom_left_no_asteriskContext before_atom_left_no_asterisk(int i) {
			return getRuleContext(Before_atom_left_no_asteriskContext.class,i);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public BeforeBothContext(BeforeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeBoth(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeBoth(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeBoth(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforePositiveContext extends BeforeContext {
		public Token command_type;
		public List<Before_atom_leftContext> before_atom_left() {
			return getRuleContexts(Before_atom_leftContext.class);
		}
		public Before_atom_leftContext before_atom_left(int i) {
			return getRuleContext(Before_atom_leftContext.class,i);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public BeforePositiveContext(BeforeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforePositive(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforePositive(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforePositive(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforeNegativeTimeContext extends BeforeContext {
		public Token asterisk;
		public Token command_type;
		public List<Before_atom_left_no_asteriskContext> before_atom_left_no_asterisk() {
			return getRuleContexts(Before_atom_left_no_asteriskContext.class);
		}
		public Before_atom_left_no_asteriskContext before_atom_left_no_asterisk(int i) {
			return getRuleContext(Before_atom_left_no_asteriskContext.class,i);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public Time_modifierContext time_modifier() {
			return getRuleContext(Time_modifierContext.class,0);
		}
		public BeforeNegativeTimeContext(BeforeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeNegativeTime(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeNegativeTime(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeNegativeTime(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforeNegativeContext extends BeforeContext {
		public Token command_type;
		public List<Before_atom_left_no_asteriskContext> before_atom_left_no_asterisk() {
			return getRuleContexts(Before_atom_left_no_asteriskContext.class);
		}
		public Before_atom_left_no_asteriskContext before_atom_left_no_asterisk(int i) {
			return getRuleContext(Before_atom_left_no_asteriskContext.class,i);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public BeforeNegativeContext(BeforeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeNegative(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeNegative(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeNegative(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforeBothTimeContext extends BeforeContext {
		public Token asterisk;
		public Token command_type;
		public List<Before_atom_left_leftContext> before_atom_left_left() {
			return getRuleContexts(Before_atom_left_leftContext.class);
		}
		public Before_atom_left_leftContext before_atom_left_left(int i) {
			return getRuleContext(Before_atom_left_leftContext.class,i);
		}
		public List<Before_atom_left_no_asteriskContext> before_atom_left_no_asterisk() {
			return getRuleContexts(Before_atom_left_no_asteriskContext.class);
		}
		public Before_atom_left_no_asteriskContext before_atom_left_no_asterisk(int i) {
			return getRuleContext(Before_atom_left_no_asteriskContext.class,i);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public Time_modifierContext time_modifier() {
			return getRuleContext(Time_modifierContext.class,0);
		}
		public BeforeBothTimeContext(BeforeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeBothTime(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeBothTime(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeBothTime(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforePositiveTimeContext extends BeforeContext {
		public Token asterisk;
		public Token command_type;
		public List<Before_atom_leftContext> before_atom_left() {
			return getRuleContexts(Before_atom_leftContext.class);
		}
		public Before_atom_leftContext before_atom_left(int i) {
			return getRuleContext(Before_atom_leftContext.class,i);
		}
		public Before_atom_rightContext before_atom_right() {
			return getRuleContext(Before_atom_rightContext.class,0);
		}
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public Time_modifierContext time_modifier() {
			return getRuleContext(Time_modifierContext.class,0);
		}
		public BeforePositiveTimeContext(BeforeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforePositiveTime(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforePositiveTime(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforePositiveTime(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BeforeContext before() throws RecognitionException {
		BeforeContext _localctx = new BeforeContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_before);
		int _la;
		try {
			setState(1356);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,156,_ctx) ) {
			case 1:
				_localctx = new BeforePositiveContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1229);
				match(T__1);
				setState(1230);
				before_atom_left();
				setState(1237);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1233); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1231);
						match(AND);
						setState(1232);
						before_atom_left();
						}
						}
						setState(1235); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1239);
				match(T__2);
				setState(1240);
				((BeforePositiveContext)_localctx).command_type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__140) ) {
					((BeforePositiveContext)_localctx).command_type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1241);
				before_atom_right();
				}
				break;
			case 2:
				_localctx = new BeforeNegativeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1243);
				match(T__90);
				setState(1244);
				match(T__1);
				setState(1245);
				before_atom_left_no_asterisk();
				setState(1252);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1248); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1246);
						match(AND);
						setState(1247);
						before_atom_left_no_asterisk();
						}
						}
						setState(1250); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1254);
				match(T__2);
				setState(1255);
				((BeforeNegativeContext)_localctx).command_type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__140) ) {
					((BeforeNegativeContext)_localctx).command_type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1256);
				before_atom_right();
				}
				break;
			case 3:
				_localctx = new BeforePositiveTimeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(1258);
				match(T__1);
				setState(1259);
				before_atom_left();
				setState(1266);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1262); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1260);
						match(AND);
						setState(1261);
						before_atom_left();
						}
						}
						setState(1264); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1268);
				match(T__2);
				{
				setState(1269);
				match(POSITIVE_RANGE);
				setState(1270);
				time_modifier();
				setState(1272);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1271);
					((BeforePositiveTimeContext)_localctx).asterisk = match(T__99);
					}
				}

				}
				setState(1274);
				((BeforePositiveTimeContext)_localctx).command_type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__140) ) {
					((BeforePositiveTimeContext)_localctx).command_type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1275);
				before_atom_right();
				}
				break;
			case 4:
				_localctx = new BeforeNegativeTimeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(1277);
				match(T__90);
				setState(1278);
				match(T__1);
				setState(1279);
				before_atom_left_no_asterisk();
				setState(1286);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1282); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1280);
						match(AND);
						setState(1281);
						before_atom_left_no_asterisk();
						}
						}
						setState(1284); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1288);
				match(T__2);
				{
				setState(1289);
				match(POSITIVE_RANGE);
				setState(1290);
				time_modifier();
				setState(1292);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1291);
					((BeforeNegativeTimeContext)_localctx).asterisk = match(T__99);
					}
				}

				}
				setState(1294);
				((BeforeNegativeTimeContext)_localctx).command_type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__140) ) {
					((BeforeNegativeTimeContext)_localctx).command_type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1295);
				before_atom_right();
				}
				break;
			case 5:
				_localctx = new BeforeBothContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(1297);
				match(T__1);
				setState(1298);
				before_atom_left_left();
				setState(1305);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1301); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1299);
						match(AND);
						setState(1300);
						before_atom_left_left();
						}
						}
						setState(1303); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1307);
				match(T__2);
				setState(1308);
				match(AND);
				setState(1309);
				match(T__90);
				setState(1310);
				match(T__1);
				setState(1311);
				before_atom_left_no_asterisk();
				setState(1318);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1314); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1312);
						match(AND);
						setState(1313);
						before_atom_left_no_asterisk();
						}
						}
						setState(1316); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1320);
				match(T__2);
				setState(1321);
				((BeforeBothContext)_localctx).command_type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__140) ) {
					((BeforeBothContext)_localctx).command_type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1322);
				before_atom_right();
				}
				break;
			case 6:
				_localctx = new BeforeBothTimeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(1324);
				match(T__1);
				setState(1325);
				before_atom_left_left();
				setState(1332);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1328); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1326);
						match(AND);
						setState(1327);
						before_atom_left_left();
						}
						}
						setState(1330); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1334);
				match(T__2);
				setState(1335);
				match(AND);
				setState(1336);
				match(T__90);
				setState(1337);
				match(T__1);
				setState(1338);
				before_atom_left_no_asterisk();
				setState(1345);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AND) {
					{
					setState(1341); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1339);
						match(AND);
						setState(1340);
						before_atom_left_no_asterisk();
						}
						}
						setState(1343); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==AND );
					}
				}

				setState(1347);
				match(T__2);
				{
				setState(1348);
				match(POSITIVE_RANGE);
				setState(1349);
				time_modifier();
				setState(1351);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1350);
					((BeforeBothTimeContext)_localctx).asterisk = match(T__99);
					}
				}

				}
				setState(1353);
				((BeforeBothTimeContext)_localctx).command_type = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__140) ) {
					((BeforeBothTimeContext)_localctx).command_type = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1354);
				before_atom_right();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Before_commandContext extends ParserRuleContext {
		public Before_commandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before_command; }
	 
		public Before_commandContext() { }
		public void copyFrom(Before_commandContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BeforeCommandAtom2Context extends Before_commandContext {
		public Token start_type;
		public Token end_type;
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public List<Before_modifierContext> before_modifier() {
			return getRuleContexts(Before_modifierContext.class);
		}
		public Before_modifierContext before_modifier(int i) {
			return getRuleContext(Before_modifierContext.class,i);
		}
		public BeforeCommandAtom2Context(Before_commandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeCommandAtom2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeCommandAtom2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeCommandAtom2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforeCommandAtomContext extends Before_commandContext {
		public Token start_type;
		public Token end_type;
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public List<Before_modifierContext> before_modifier() {
			return getRuleContexts(Before_modifierContext.class);
		}
		public Before_modifierContext before_modifier(int i) {
			return getRuleContext(Before_modifierContext.class,i);
		}
		public BeforeCommandAtomContext(Before_commandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeCommandAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeCommandAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeCommandAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Before_commandContext before_command() throws RecognitionException {
		Before_commandContext _localctx = new Before_commandContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_before_command);
		int _la;
		try {
			int _alt;
			setState(1406);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,168,_ctx) ) {
			case 1:
				_localctx = new BeforeCommandAtomContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1358);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__141) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1359);
				match(T__1);
				setState(1362);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,157,_ctx) ) {
				case 1:
					{
					setState(1360);
					command();
					}
					break;
				case 2:
					{
					setState(1361);
					atom();
					}
					break;
				}
				setState(1365);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1364);
					((BeforeCommandAtomContext)_localctx).start_type = match(T__99);
					}
				}

				setState(1367);
				match(T__8);
				setState(1370);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,159,_ctx) ) {
				case 1:
					{
					setState(1368);
					command();
					}
					break;
				case 2:
					{
					setState(1369);
					atom();
					}
					break;
				}
				setState(1373);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1372);
					((BeforeCommandAtomContext)_localctx).end_type = match(T__99);
					}
				}

				setState(1375);
				match(T__2);
				setState(1381);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,162,_ctx) ) {
				case 1:
					{
					setState(1377); 
					_errHandler.sync(this);
					_alt = 1;
					do {
						switch (_alt) {
						case 1:
							{
							{
							setState(1376);
							before_modifier();
							}
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(1379); 
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,161,_ctx);
					} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
					}
					break;
				}
				}
				break;
			case 2:
				_localctx = new BeforeCommandAtom2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1383);
				_la = _input.LA(1);
				if ( !(_la==T__139 || _la==T__141) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1384);
				match(T__1);
				setState(1387);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,163,_ctx) ) {
				case 1:
					{
					setState(1385);
					command();
					}
					break;
				case 2:
					{
					setState(1386);
					atom();
					}
					break;
				}
				setState(1390);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1389);
					((BeforeCommandAtom2Context)_localctx).start_type = match(T__99);
					}
				}

				setState(1392);
				match(T__8);
				setState(1395);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,165,_ctx) ) {
				case 1:
					{
					setState(1393);
					command();
					}
					break;
				case 2:
					{
					setState(1394);
					atom();
					}
					break;
				}
				setState(1398);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1397);
					((BeforeCommandAtom2Context)_localctx).end_type = match(T__99);
					}
				}

				setState(1400);
				match(T__2);
				{
				setState(1402); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1401);
						before_modifier();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1404); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,167,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Before_modifierContext extends ParserRuleContext {
		public Before_modifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_before_modifier; }
	 
		public Before_modifierContext() { }
		public void copyFrom(Before_modifierContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BeforeModifierContext extends Before_modifierContext {
		public Token in_type;
		public Token return_type;
		public Token start_type;
		public Token end_type;
		public Time_modifierContext startModifier;
		public Time_modifierContext endModifier;
		public Start_timeContext start_time() {
			return getRuleContext(Start_timeContext.class,0);
		}
		public End_timeContext end_time() {
			return getRuleContext(End_timeContext.class,0);
		}
		public List<Time_modifierContext> time_modifier() {
			return getRuleContexts(Time_modifierContext.class);
		}
		public Time_modifierContext time_modifier(int i) {
			return getRuleContext(Time_modifierContext.class,i);
		}
		public BeforeModifierContext(Before_modifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBeforeModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBeforeModifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitBeforeModifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Before_modifierContext before_modifier() throws RecognitionException {
		Before_modifierContext _localctx = new Before_modifierContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_before_modifier);
		int _la;
		try {
			_localctx = new BeforeModifierContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(1504);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,193,_ctx) ) {
			case 1:
				{
				{
				setState(1409);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__56 || _la==T__142) {
					{
					setState(1408);
					((BeforeModifierContext)_localctx).in_type = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__56 || _la==T__142) ) {
						((BeforeModifierContext)_localctx).in_type = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1412);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1411);
					((BeforeModifierContext)_localctx).return_type = match(T__99);
					}
				}

				setState(1415);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__143) {
					{
					setState(1414);
					((BeforeModifierContext)_localctx).start_type = match(T__143);
					}
				}

				setState(1418);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__144) {
					{
					setState(1417);
					((BeforeModifierContext)_localctx).end_type = match(T__144);
					}
				}

				setState(1420);
				match(T__1);
				setState(1421);
				start_time();
				setState(1423);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1422);
					((BeforeModifierContext)_localctx).startModifier = time_modifier();
					}
				}

				setState(1425);
				match(T__8);
				setState(1426);
				end_time();
				setState(1428);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1427);
					((BeforeModifierContext)_localctx).endModifier = time_modifier();
					}
				}

				setState(1430);
				match(T__2);
				}
				}
				break;
			case 2:
				{
				{
				setState(1433);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__56 || _la==T__142) {
					{
					setState(1432);
					((BeforeModifierContext)_localctx).in_type = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__56 || _la==T__142) ) {
						((BeforeModifierContext)_localctx).in_type = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1436);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__143) {
					{
					setState(1435);
					((BeforeModifierContext)_localctx).start_type = match(T__143);
					}
				}

				setState(1439);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1438);
					((BeforeModifierContext)_localctx).return_type = match(T__99);
					}
				}

				setState(1442);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__144) {
					{
					setState(1441);
					((BeforeModifierContext)_localctx).end_type = match(T__144);
					}
				}

				setState(1444);
				match(T__1);
				setState(1445);
				start_time();
				setState(1447);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1446);
					((BeforeModifierContext)_localctx).startModifier = time_modifier();
					}
				}

				setState(1449);
				match(T__8);
				setState(1450);
				end_time();
				setState(1452);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1451);
					((BeforeModifierContext)_localctx).endModifier = time_modifier();
					}
				}

				setState(1454);
				match(T__2);
				}
				}
				break;
			case 3:
				{
				{
				setState(1457);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__56 || _la==T__142) {
					{
					setState(1456);
					((BeforeModifierContext)_localctx).in_type = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__56 || _la==T__142) ) {
						((BeforeModifierContext)_localctx).in_type = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1460);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__143) {
					{
					setState(1459);
					((BeforeModifierContext)_localctx).start_type = match(T__143);
					}
				}

				setState(1463);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__144) {
					{
					setState(1462);
					((BeforeModifierContext)_localctx).end_type = match(T__144);
					}
				}

				setState(1466);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1465);
					((BeforeModifierContext)_localctx).return_type = match(T__99);
					}
				}

				setState(1468);
				match(T__1);
				setState(1469);
				start_time();
				setState(1471);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1470);
					((BeforeModifierContext)_localctx).startModifier = time_modifier();
					}
				}

				setState(1473);
				match(T__8);
				setState(1474);
				end_time();
				setState(1476);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1475);
					((BeforeModifierContext)_localctx).endModifier = time_modifier();
					}
				}

				setState(1478);
				match(T__2);
				}
				}
				break;
			case 4:
				{
				{
				setState(1481);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__56 || _la==T__142) {
					{
					setState(1480);
					((BeforeModifierContext)_localctx).in_type = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__56 || _la==T__142) ) {
						((BeforeModifierContext)_localctx).in_type = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1484);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__99) {
					{
					setState(1483);
					((BeforeModifierContext)_localctx).return_type = match(T__99);
					}
				}

				setState(1487);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__143) {
					{
					setState(1486);
					((BeforeModifierContext)_localctx).start_type = match(T__143);
					}
				}

				setState(1490);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__144) {
					{
					setState(1489);
					((BeforeModifierContext)_localctx).end_type = match(T__144);
					}
				}

				setState(1492);
				match(T__1);
				setState(1493);
				start_time();
				setState(1495);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1494);
					((BeforeModifierContext)_localctx).startModifier = time_modifier();
					}
				}

				setState(1497);
				match(T__8);
				setState(1498);
				end_time();
				setState(1500);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) {
					{
					setState(1499);
					((BeforeModifierContext)_localctx).endModifier = time_modifier();
					}
				}

				setState(1502);
				match(T__2);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Start_timeContext extends ParserRuleContext {
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public TerminalNode NEGATIVE_RANGE() { return getToken(ExprParser.NEGATIVE_RANGE, 0); }
		public Start_timeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start_time; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterStart_time(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitStart_time(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitStart_time(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Start_timeContext start_time() throws RecognitionException {
		Start_timeContext _localctx = new Start_timeContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_start_time);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1528);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,194,_ctx) ) {
			case 1:
				{
				setState(1506);
				match(T__85);
				}
				break;
			case 2:
				{
				setState(1507);
				match(T__86);
				}
				break;
			case 3:
				{
				setState(1508);
				match(POSITIVE_RANGE);
				}
				break;
			case 4:
				{
				setState(1509);
				match(NEGATIVE_RANGE);
				}
				break;
			case 5:
				{
				setState(1510);
				match(T__12);
				setState(1511);
				match(T__56);
				setState(1512);
				match(POSITIVE_RANGE);
				}
				break;
			case 6:
				{
				setState(1513);
				match(T__12);
				}
				break;
			case 7:
				{
				setState(1514);
				match(T__12);
				setState(1515);
				_la = _input.LA(1);
				if ( !(_la==POSITIVE_RANGE || _la==NEGATIVE_RANGE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 8:
				{
				setState(1516);
				match(T__11);
				setState(1517);
				match(T__56);
				setState(1518);
				match(POSITIVE_RANGE);
				}
				break;
			case 9:
				{
				setState(1519);
				match(T__11);
				}
				break;
			case 10:
				{
				setState(1520);
				match(T__11);
				setState(1521);
				_la = _input.LA(1);
				if ( !(_la==POSITIVE_RANGE || _la==NEGATIVE_RANGE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 11:
				{
				setState(1522);
				match(T__12);
				setState(1523);
				match(T__142);
				{
				setState(1524);
				match(POSITIVE_RANGE);
				}
				}
				break;
			case 12:
				{
				setState(1525);
				match(T__11);
				setState(1526);
				match(T__142);
				{
				setState(1527);
				match(POSITIVE_RANGE);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class End_timeContext extends ParserRuleContext {
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public TerminalNode NEGATIVE_RANGE() { return getToken(ExprParser.NEGATIVE_RANGE, 0); }
		public End_timeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_end_time; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEnd_time(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEnd_time(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitEnd_time(this);
			else return visitor.visitChildren(this);
		}
	}

	public final End_timeContext end_time() throws RecognitionException {
		End_timeContext _localctx = new End_timeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_end_time);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1552);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,195,_ctx) ) {
			case 1:
				{
				setState(1530);
				match(T__86);
				}
				break;
			case 2:
				{
				setState(1531);
				match(POSITIVE_RANGE);
				}
				break;
			case 3:
				{
				setState(1532);
				match(T__85);
				}
				break;
			case 4:
				{
				setState(1533);
				match(NEGATIVE_RANGE);
				}
				break;
			case 5:
				{
				setState(1534);
				match(T__12);
				setState(1535);
				match(T__56);
				setState(1536);
				match(POSITIVE_RANGE);
				}
				break;
			case 6:
				{
				setState(1537);
				match(T__12);
				}
				break;
			case 7:
				{
				setState(1538);
				match(T__12);
				setState(1539);
				_la = _input.LA(1);
				if ( !(_la==POSITIVE_RANGE || _la==NEGATIVE_RANGE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 8:
				{
				setState(1540);
				match(T__11);
				setState(1541);
				match(T__56);
				setState(1542);
				match(POSITIVE_RANGE);
				}
				break;
			case 9:
				{
				setState(1543);
				match(T__11);
				}
				break;
			case 10:
				{
				setState(1544);
				match(T__11);
				setState(1545);
				_la = _input.LA(1);
				if ( !(_la==POSITIVE_RANGE || _la==NEGATIVE_RANGE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 11:
				{
				setState(1546);
				match(T__12);
				setState(1547);
				match(T__142);
				{
				setState(1548);
				match(POSITIVE_RANGE);
				}
				}
				break;
			case 12:
				{
				setState(1549);
				match(T__11);
				setState(1550);
				match(T__142);
				{
				setState(1551);
				match(POSITIVE_RANGE);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Limit_typeContext extends ParserRuleContext {
		public TerminalNode POSITIVE_RANGE() { return getToken(ExprParser.POSITIVE_RANGE, 0); }
		public Limit_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limit_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterLimit_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitLimit_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitLimit_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Limit_typeContext limit_type() throws RecognitionException {
		Limit_typeContext _localctx = new Limit_typeContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_limit_type);
		int _la;
		try {
			setState(1559);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case POSITIVE_RANGE:
				enterOuterAlt(_localctx, 1);
				{
				setState(1554);
				match(POSITIVE_RANGE);
				setState(1556);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 32)) & ~0x3f) == 0 && ((1L << (_la - 32)) & ((1L << (T__31 - 32)) | (1L << (T__37 - 32)) | (1L << (T__85 - 32)))) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & ((1L << (T__145 - 146)) | (1L << (T__146 - 146)) | (1L << (T__147 - 146)) | (1L << (T__148 - 146)) | (1L << (T__149 - 146)) | (1L << (T__150 - 146)) | (1L << (T__151 - 146)))) != 0)) {
					{
					setState(1555);
					_la = _input.LA(1);
					if ( !(((((_la - 32)) & ~0x3f) == 0 && ((1L << (_la - 32)) & ((1L << (T__31 - 32)) | (1L << (T__37 - 32)) | (1L << (T__85 - 32)))) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & ((1L << (T__145 - 146)) | (1L << (T__146 - 146)) | (1L << (T__147 - 146)) | (1L << (T__148 - 146)) | (1L << (T__149 - 146)) | (1L << (T__150 - 146)) | (1L << (T__151 - 146)))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				}
				break;
			case T__152:
			case T__153:
				enterOuterAlt(_localctx, 2);
				{
				setState(1558);
				_la = _input.LA(1);
				if ( !(_la==T__152 || _la==T__153) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Time_modifierContext extends ParserRuleContext {
		public Time_modifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_time_modifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterTime_modifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitTime_modifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitTime_modifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Time_modifierContext time_modifier() throws RecognitionException {
		Time_modifierContext _localctx = new Time_modifierContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_time_modifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1561);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__26) | (1L << T__31) | (1L << T__41) | (1L << T__43))) != 0) || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (T__122 - 123)) | (1L << (T__123 - 123)) | (1L << (T__150 - 123)) | (1L << (T__151 - 123)) | (1L << (T__154 - 123)) | (1L << (T__155 - 123)) | (1L << (T__156 - 123)) | (1L << (T__157 - 123)) | (1L << (T__158 - 123)) | (1L << (T__159 - 123)) | (1L << (T__160 - 123)) | (1L << (T__161 - 123)) | (1L << (T__162 - 123)) | (1L << (T__163 - 123)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Note_atomContext extends ParserRuleContext {
		public Note_atomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_note_atom; }
	 
		public Note_atomContext() { }
		public void copyFrom(Note_atomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NegatedTextCommandContext extends Note_atomContext {
		public TerminalNode NEGATED_TEXT() { return getToken(ExprParser.NEGATED_TEXT, 0); }
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public NegatedTextCommandContext(Note_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNegatedTextCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNegatedTextCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNegatedTextCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TextCommandContext extends Note_atomContext {
		public TerminalNode TEXT() { return getToken(ExprParser.TEXT, 0); }
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public TextCommandContext(Note_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterTextCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitTextCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitTextCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FamilyHistoryTextCommandContext extends Note_atomContext {
		public TerminalNode FAMILY_HISTORY_TEXT() { return getToken(ExprParser.FAMILY_HISTORY_TEXT, 0); }
		public TerminalNode TERM() { return getToken(ExprParser.TERM, 0); }
		public FamilyHistoryTextCommandContext(Note_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFamilyHistoryTextCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFamilyHistoryTextCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitFamilyHistoryTextCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Note_atomContext note_atom() throws RecognitionException {
		Note_atomContext _localctx = new Note_atomContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_note_atom);
		try {
			setState(1572);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TEXT:
				_localctx = new TextCommandContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1563);
				match(TEXT);
				setState(1564);
				match(T__9);
				setState(1565);
				match(TERM);
				}
				break;
			case NEGATED_TEXT:
				_localctx = new NegatedTextCommandContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1566);
				match(NEGATED_TEXT);
				setState(1567);
				match(T__9);
				setState(1568);
				match(TERM);
				}
				break;
			case FAMILY_HISTORY_TEXT:
				_localctx = new FamilyHistoryTextCommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(1569);
				match(FAMILY_HISTORY_TEXT);
				setState(1570);
				match(T__9);
				setState(1571);
				match(TERM);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Note_atomsContext extends ParserRuleContext {
		public List<Note_atomContext> note_atom() {
			return getRuleContexts(Note_atomContext.class);
		}
		public Note_atomContext note_atom(int i) {
			return getRuleContext(Note_atomContext.class,i);
		}
		public Note_atomsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_note_atoms; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterNote_atoms(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitNote_atoms(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitNote_atoms(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Note_atomsContext note_atoms() throws RecognitionException {
		Note_atomsContext _localctx = new Note_atomsContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_note_atoms);
		int _la;
		try {
			setState(1582);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,200,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1574);
				note_atom();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1575);
				note_atom();
				setState(1578); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(1576);
					match(T__8);
					setState(1577);
					note_atom();
					}
					}
					setState(1580); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__8 );
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomsContext extends ParserRuleContext {
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public AtomsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atoms; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAtoms(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAtoms(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ExprVisitor ) return ((ExprVisitor<? extends T>)visitor).visitAtoms(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomsContext atoms() throws RecognitionException {
		AtomsContext _localctx = new AtomsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_atoms);
		try {
			int _alt;
			setState(1592);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,202,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1584);
				atom();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1585);
				atom();
				setState(1588); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1586);
						match(T__8);
						setState(1587);
						atom();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1590); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,201,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 9:
			return commands_sempred((CommandsContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean commands_sempred(CommandsContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00c0\u063d\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\3\2\6\2<\n\2\r\2\16\2=\5\2@\n"+
		"\2\3\2\3\2\6\2D\n\2\r\2\16\2E\5\2H\n\2\3\2\3\2\3\2\5\2M\n\2\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3X\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\5\3d\n\3\3\3\6\3g\n\3\r\3\16\3h\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\5\3u\n\3\3\3\3\3\3\3\5\3z\n\3\3\3\6\3}\n\3\r\3\16\3~\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\u008b\n\3\3\3\3\3\3\3\5\3\u0090\n"+
		"\3\3\3\6\3\u0093\n\3\r\3\16\3\u0094\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\5\3\u00a1\n\3\3\3\3\3\3\3\5\3\u00a6\n\3\3\3\6\3\u00a9\n\3\r\3\16"+
		"\3\u00aa\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\u00b7\n\3\3\3\3\3"+
		"\3\3\3\3\3\3\5\3\u00be\n\3\3\3\3\3\3\3\5\3\u00c3\n\3\3\3\6\3\u00c6\n\3"+
		"\r\3\16\3\u00c7\3\3\3\3\5\3\u00cc\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3"+
		"\u00d5\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\6\5\u00e4"+
		"\n\5\r\5\16\5\u00e5\3\5\3\5\3\5\3\5\5\5\u00ec\n\5\3\6\3\6\5\6\u00f0\n"+
		"\6\3\7\6\7\u00f3\n\7\r\7\16\7\u00f4\3\b\3\b\3\b\3\b\5\b\u00fb\n\b\3\b"+
		"\3\b\3\b\3\b\3\b\5\b\u0102\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\5\b\u010e\n\b\3\b\3\b\3\b\3\b\5\b\u0114\n\b\3\b\3\b\3\b\5\b\u0119\n\b"+
		"\3\b\3\b\3\b\3\b\5\b\u011f\n\b\3\b\3\b\3\b\3\b\3\b\5\b\u0126\n\b\3\b\3"+
		"\b\3\b\5\b\u012b\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u0135\n\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\5\b\u013d\n\b\6\b\u013f\n\b\r\b\16\b\u0140\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u014d\n\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\5\b\u0158\n\b\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5"+
		"\n\u0164\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u016e\n\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\5\n\u0178\n\n\3\n\3\n\5\n\u017c\n\n\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\5\13\u0185\n\13\3\13\3\13\3\13\7\13\u018a\n\13\f"+
		"\13\16\13\u018d\13\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\5\f\u019d\n\f\3\f\3\f\3\f\3\f\3\f\3\f\6\f\u01a5\n\f\r\f\16\f"+
		"\u01a6\3\f\3\f\3\f\5\f\u01ac\n\f\3\f\3\f\3\f\5\f\u01b1\n\f\3\f\3\f\3\f"+
		"\5\f\u01b6\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u01c1\n\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u01cc\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\5\f\u01d8\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5"+
		"\f\u01e4\n\f\3\f\3\f\3\f\3\f\5\f\u01ea\n\f\3\f\3\f\3\f\5\f\u01ef\n\f\3"+
		"\f\3\f\3\f\3\f\5\f\u01f5\n\f\3\f\3\f\3\f\5\f\u01fa\n\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0206\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\5\f\u0212\n\f\3\f\3\f\3\f\3\f\5\f\u0218\n\f\3\f\3\f\3\f\5\f\u021d"+
		"\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0229\n\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0236\n\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\5\f\u023e\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0246\n\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0264\n\f\3\f\3\f\3\f\5\f\u0269\n\f\3\f\3"+
		"\f\3\f\3\f\3\f\5\f\u0270\n\f\3\f\3\f\3\f\5\f\u0275\n\f\3\f\3\f\3\f\3\f"+
		"\3\f\5\f\u027c\n\f\3\f\3\f\3\f\5\f\u0281\n\f\3\f\3\f\3\f\5\f\u0286\n\f"+
		"\3\f\3\f\3\f\3\f\3\f\5\f\u028d\n\f\3\f\3\f\3\f\5\f\u0292\n\f\3\f\3\f\3"+
		"\f\5\f\u0297\n\f\3\f\3\f\3\f\3\f\3\f\5\f\u029e\n\f\3\f\3\f\3\f\5\f\u02a3"+
		"\n\f\3\f\3\f\3\f\5\f\u02a8\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u02b0\n\f\3"+
		"\f\3\f\3\f\5\f\u02b5\n\f\3\f\3\f\3\f\5\f\u02ba\n\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\5\f\u02c2\n\f\3\f\3\f\3\f\3\f\3\f\5\f\u02c9\n\f\3\f\3\f\3\f\5\f\u02ce"+
		"\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u02d6\n\f\3\f\3\f\3\f\5\f\u02db\n\f\3"+
		"\f\3\f\3\f\3\f\3\f\5\f\u02e2\n\f\3\f\3\f\3\f\5\f\u02e7\n\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u02f5\n\f\3\f\3\f\3\f\5\f\u02fa"+
		"\n\f\3\f\3\f\3\f\3\f\3\f\5\f\u0301\n\f\3\f\3\f\3\f\5\f\u0306\n\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\5\f\u030e\n\f\3\f\3\f\3\f\5\f\u0313\n\f\3\f\3\f\3\f"+
		"\3\f\3\f\5\f\u031a\n\f\3\f\3\f\3\f\5\f\u031f\n\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\5\f\u0327\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0333\n\f"+
		"\3\f\3\f\3\f\5\f\u0338\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0353\n\f"+
		"\3\f\3\f\3\f\5\f\u0358\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0360\n\f\3\f\5"+
		"\f\u0363\n\f\3\f\3\f\3\f\5\f\u0368\n\f\3\f\5\f\u036b\n\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\5\f\u0373\n\f\3\f\3\f\3\f\5\f\u0378\n\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\5\f\u0380\n\f\3\f\3\f\3\f\5\f\u0385\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5"+
		"\f\u038d\n\f\3\f\3\f\3\f\5\f\u0392\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u039a"+
		"\n\f\3\f\3\f\3\f\5\f\u039f\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u03c4\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5"+
		"\f\u03cc\n\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u03d4\n\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u040c\n\f"+
		"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r"+
		"\u0430\n\r\3\r\3\r\3\r\5\r\u0435\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u0447\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\5\r\u0454\n\r\3\r\3\r\3\r\5\r\u0459\n\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\5\r\u0482\n\r\3\r\3\r\3\r\5\r\u0487\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\6\r\u0494\n\r\r\r\16\r\u0495\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\5\r\u049e\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u04a8\n"+
		"\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u04b5"+
		"\n\17\3\20\3\20\5\20\u04b9\n\20\3\20\5\20\u04bc\n\20\3\21\3\21\5\21\u04c0"+
		"\n\21\3\21\5\21\u04c3\n\21\3\22\3\22\5\22\u04c7\n\22\3\22\5\22\u04ca\n"+
		"\22\3\23\3\23\5\23\u04ce\n\23\3\24\3\24\3\24\3\24\6\24\u04d4\n\24\r\24"+
		"\16\24\u04d5\5\24\u04d8\n\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\6\24\u04e3\n\24\r\24\16\24\u04e4\5\24\u04e7\n\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\6\24\u04f1\n\24\r\24\16\24\u04f2\5\24\u04f5\n"+
		"\24\3\24\3\24\3\24\3\24\5\24\u04fb\n\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\6\24\u0505\n\24\r\24\16\24\u0506\5\24\u0509\n\24\3\24\3\24"+
		"\3\24\3\24\5\24\u050f\n\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\6\24\u0518"+
		"\n\24\r\24\16\24\u0519\5\24\u051c\n\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\6\24\u0525\n\24\r\24\16\24\u0526\5\24\u0529\n\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\6\24\u0533\n\24\r\24\16\24\u0534\5\24\u0537\n"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\6\24\u0540\n\24\r\24\16\24\u0541"+
		"\5\24\u0544\n\24\3\24\3\24\3\24\3\24\5\24\u054a\n\24\3\24\3\24\3\24\5"+
		"\24\u054f\n\24\3\25\3\25\3\25\3\25\5\25\u0555\n\25\3\25\5\25\u0558\n\25"+
		"\3\25\3\25\3\25\5\25\u055d\n\25\3\25\5\25\u0560\n\25\3\25\3\25\6\25\u0564"+
		"\n\25\r\25\16\25\u0565\5\25\u0568\n\25\3\25\3\25\3\25\3\25\5\25\u056e"+
		"\n\25\3\25\5\25\u0571\n\25\3\25\3\25\3\25\5\25\u0576\n\25\3\25\5\25\u0579"+
		"\n\25\3\25\3\25\6\25\u057d\n\25\r\25\16\25\u057e\5\25\u0581\n\25\3\26"+
		"\5\26\u0584\n\26\3\26\5\26\u0587\n\26\3\26\5\26\u058a\n\26\3\26\5\26\u058d"+
		"\n\26\3\26\3\26\3\26\5\26\u0592\n\26\3\26\3\26\3\26\5\26\u0597\n\26\3"+
		"\26\3\26\3\26\5\26\u059c\n\26\3\26\5\26\u059f\n\26\3\26\5\26\u05a2\n\26"+
		"\3\26\5\26\u05a5\n\26\3\26\3\26\3\26\5\26\u05aa\n\26\3\26\3\26\3\26\5"+
		"\26\u05af\n\26\3\26\3\26\3\26\5\26\u05b4\n\26\3\26\5\26\u05b7\n\26\3\26"+
		"\5\26\u05ba\n\26\3\26\5\26\u05bd\n\26\3\26\3\26\3\26\5\26\u05c2\n\26\3"+
		"\26\3\26\3\26\5\26\u05c7\n\26\3\26\3\26\3\26\5\26\u05cc\n\26\3\26\5\26"+
		"\u05cf\n\26\3\26\5\26\u05d2\n\26\3\26\5\26\u05d5\n\26\3\26\3\26\3\26\5"+
		"\26\u05da\n\26\3\26\3\26\3\26\5\26\u05df\n\26\3\26\3\26\5\26\u05e3\n\26"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u05fb\n\27\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\5\30\u0613\n\30\3\31\3\31\5\31\u0617\n\31\3"+
		"\31\5\31\u061a\n\31\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\5\33\u0627\n\33\3\34\3\34\3\34\3\34\6\34\u062d\n\34\r\34\16\34\u062e"+
		"\5\34\u0631\n\34\3\35\3\35\3\35\3\35\6\35\u0637\n\35\r\35\16\35\u0638"+
		"\5\35\u063b\n\35\3\35\3\u0495\3\24\36\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(*,.\60\62\64\668\2\37\3\2\26\60\3\2\678\3\2@A\3\2BC\4\2\16"+
		"\16IJ\4\2\17\17KL\3\2SU\4\2EEWW\4\2XY\u00bb\u00bb\4\2YY\u00bb\u00bb\3"+
		"\2bc\3\2de\3\2mn\3\2vy\4\2\u00b8\u00b8\u00bb\u00bb\3\2}~\4\2XX\u00bb\u00bb"+
		"\4\2qq\u0082\u0082\3\2\u0088\u0089\4\2\66\66\u008a\u008a\4\2>>\u008c\u008c"+
		"\3\2\u00b9\u00bb\3\2\u008e\u008f\4\2\u008e\u008e\u0090\u0090\4\2;;\u0091"+
		"\u0091\3\2\u00bb\u00bc\6\2\"\"((XX\u0094\u009a\3\2\u009b\u009c\n\2\31"+
		"\31\35\35\"\",,..}~\u0099\u009a\u009d\u00a6\2\u0784\2L\3\2\2\2\4\u00d4"+
		"\3\2\2\2\6\u00d6\3\2\2\2\b\u00eb\3\2\2\2\n\u00ef\3\2\2\2\f\u00f2\3\2\2"+
		"\2\16\u0157\3\2\2\2\20\u0159\3\2\2\2\22\u017b\3\2\2\2\24\u0184\3\2\2\2"+
		"\26\u040b\3\2\2\2\30\u049d\3\2\2\2\32\u04a7\3\2\2\2\34\u04b4\3\2\2\2\36"+
		"\u04b8\3\2\2\2 \u04bf\3\2\2\2\"\u04c6\3\2\2\2$\u04cd\3\2\2\2&\u054e\3"+
		"\2\2\2(\u0580\3\2\2\2*\u05e2\3\2\2\2,\u05fa\3\2\2\2.\u0612\3\2\2\2\60"+
		"\u0619\3\2\2\2\62\u061b\3\2\2\2\64\u0626\3\2\2\2\66\u0630\3\2\2\28\u063a"+
		"\3\2\2\2:<\5\6\4\2;:\3\2\2\2<=\3\2\2\2=;\3\2\2\2=>\3\2\2\2>@\3\2\2\2?"+
		";\3\2\2\2?@\3\2\2\2@A\3\2\2\2AM\5\4\3\2BD\5\6\4\2CB\3\2\2\2DE\3\2\2\2"+
		"EC\3\2\2\2EF\3\2\2\2FH\3\2\2\2GC\3\2\2\2GH\3\2\2\2HI\3\2\2\2IM\5\n\6\2"+
		"JM\5\4\3\2KM\5\n\6\2L?\3\2\2\2LG\3\2\2\2LJ\3\2\2\2LK\3\2\2\2M\3\3\2\2"+
		"\2NO\7\3\2\2OP\7\4\2\2PQ\5\n\6\2QR\7\5\2\2R\u00d5\3\2\2\2ST\7\6\2\2TX"+
		"\7\7\2\2UX\7\b\2\2VX\7\t\2\2WS\3\2\2\2WU\3\2\2\2WV\3\2\2\2XY\3\2\2\2Y"+
		"Z\7\4\2\2Z[\5\n\6\2[\\\7\5\2\2\\\u00d5\3\2\2\2]^\7\n\2\2^_\7\4\2\2_f\5"+
		"\n\6\2`c\7\13\2\2ab\7\u00bf\2\2bd\7\f\2\2ca\3\2\2\2cd\3\2\2\2de\3\2\2"+
		"\2eg\5\n\6\2f`\3\2\2\2gh\3\2\2\2hf\3\2\2\2hi\3\2\2\2ij\3\2\2\2jk\7\5\2"+
		"\2k\u00cc\3\2\2\2lm\7\n\2\2mn\7\4\2\2nt\5\n\6\2op\7\13\2\2pq\7\r\2\2q"+
		"r\7\f\2\2rs\3\2\2\2su\5\n\6\2to\3\2\2\2tu\3\2\2\2u|\3\2\2\2vy\7\13\2\2"+
		"wx\7\u00bf\2\2xz\7\f\2\2yw\3\2\2\2yz\3\2\2\2z{\3\2\2\2{}\5\n\6\2|v\3\2"+
		"\2\2}~\3\2\2\2~|\3\2\2\2~\177\3\2\2\2\177\u0080\3\2\2\2\u0080\u0081\7"+
		"\5\2\2\u0081\u00cc\3\2\2\2\u0082\u0083\7\n\2\2\u0083\u0084\7\4\2\2\u0084"+
		"\u008a\5\n\6\2\u0085\u0086\7\13\2\2\u0086\u0087\7\16\2\2\u0087\u0088\7"+
		"\f\2\2\u0088\u0089\3\2\2\2\u0089\u008b\5\n\6\2\u008a\u0085\3\2\2\2\u008a"+
		"\u008b\3\2\2\2\u008b\u0092\3\2\2\2\u008c\u008f\7\13\2\2\u008d\u008e\7"+
		"\u00bf\2\2\u008e\u0090\7\f\2\2\u008f\u008d\3\2\2\2\u008f\u0090\3\2\2\2"+
		"\u0090\u0091\3\2\2\2\u0091\u0093\5\n\6\2\u0092\u008c\3\2\2\2\u0093\u0094"+
		"\3\2\2\2\u0094\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0096\3\2\2\2\u0096"+
		"\u0097\7\5\2\2\u0097\u00cc\3\2\2\2\u0098\u0099\7\n\2\2\u0099\u009a\7\4"+
		"\2\2\u009a\u00a0\5\n\6\2\u009b\u009c\7\13\2\2\u009c\u009d\7\17\2\2\u009d"+
		"\u009e\7\f\2\2\u009e\u009f\3\2\2\2\u009f\u00a1\5\n\6\2\u00a0\u009b\3\2"+
		"\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a8\3\2\2\2\u00a2\u00a5\7\13\2\2\u00a3"+
		"\u00a4\7\u00bf\2\2\u00a4\u00a6\7\f\2\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6"+
		"\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a9\5\n\6\2\u00a8\u00a2\3\2\2\2\u00a9"+
		"\u00aa\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2"+
		"\2\2\u00ac\u00ad\7\5\2\2\u00ad\u00cc\3\2\2\2\u00ae\u00af\7\n\2\2\u00af"+
		"\u00b0\7\4\2\2\u00b0\u00b6\5\n\6\2\u00b1\u00b2\7\13\2\2\u00b2\u00b3\7"+
		"\16\2\2\u00b3\u00b4\7\f\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b7\5\n\6\2\u00b6"+
		"\u00b1\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00bd\3\2\2\2\u00b8\u00b9\7\13"+
		"\2\2\u00b9\u00ba\7\17\2\2\u00ba\u00bb\7\f\2\2\u00bb\u00bc\3\2\2\2\u00bc"+
		"\u00be\5\n\6\2\u00bd\u00b8\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\u00c5\3\2"+
		"\2\2\u00bf\u00c2\7\13\2\2\u00c0\u00c1\7\u00bf\2\2\u00c1\u00c3\7\f\2\2"+
		"\u00c2\u00c0\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c6"+
		"\5\n\6\2\u00c5\u00bf\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7"+
		"\u00c8\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ca\7\5\2\2\u00ca\u00cc\3\2"+
		"\2\2\u00cb]\3\2\2\2\u00cbl\3\2\2\2\u00cb\u0082\3\2\2\2\u00cb\u0098\3\2"+
		"\2\2\u00cb\u00ae\3\2\2\2\u00cc\u00d5\3\2\2\2\u00cd\u00ce\7\20\2\2\u00ce"+
		"\u00cf\7\4\2\2\u00cf\u00d0\5\n\6\2\u00d0\u00d1\7\13\2\2\u00d1\u00d2\7"+
		"\u00bf\2\2\u00d2\u00d3\7\5\2\2\u00d3\u00d5\3\2\2\2\u00d4N\3\2\2\2\u00d4"+
		"W\3\2\2\2\u00d4\u00cb\3\2\2\2\u00d4\u00cd\3\2\2\2\u00d5\5\3\2\2\2\u00d6"+
		"\u00d7\7\21\2\2\u00d7\u00d8\7\22\2\2\u00d8\u00d9\7\4\2\2\u00d9\u00da\5"+
		"\n\6\2\u00da\u00db\7\5\2\2\u00db\u00dc\7\23\2\2\u00dc\u00dd\7\4\2\2\u00dd"+
		"\u00de\5\f\7\2\u00de\u00df\7\5\2\2\u00df\u00e0\5\b\5\2\u00e0\7\3\2\2\2"+
		"\u00e1\u00e3\7\24\2\2\u00e2\u00e4\5\16\b\2\u00e3\u00e2\3\2\2\2\u00e4\u00e5"+
		"\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7"+
		"\u00e8\7\25\2\2\u00e8\u00ec\3\2\2\2\u00e9\u00ea\7\24\2\2\u00ea\u00ec\7"+
		"\25\2\2\u00eb\u00e1\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec\t\3\2\2\2\u00ed"+
		"\u00f0\5\22\n\2\u00ee\u00f0\5\f\7\2\u00ef\u00ed\3\2\2\2\u00ef\u00ee\3"+
		"\2\2\2\u00f0\13\3\2\2\2\u00f1\u00f3\t\2\2\2\u00f2\u00f1\3\2\2\2\u00f3"+
		"\u00f4\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\r\3\2\2\2"+
		"\u00f6\u00f7\5\f\7\2\u00f7\u00fa\7\f\2\2\u00f8\u00fb\5\26\f\2\u00f9\u00fb"+
		"\5\30\r\2\u00fa\u00f8\3\2\2\2\u00fa\u00f9\3\2\2\2\u00fb\u00fc\3\2\2\2"+
		"\u00fc\u00fd\7\61\2\2\u00fd\u0158\3\2\2\2\u00fe\u0101\7\62\2\2\u00ff\u0102"+
		"\5\26\f\2\u0100\u0102\5\30\r\2\u0101\u00ff\3\2\2\2\u0101\u0100\3\2\2\2"+
		"\u0102\u0103\3\2\2\2\u0103\u0104\7\23\2\2\u0104\u0105\5\f\7\2\u0105\u0106"+
		"\7\61\2\2\u0106\u0158\3\2\2\2\u0107\u0108\7\63\2\2\u0108\u0109\5\f\7\2"+
		"\u0109\u010a\7\61\2\2\u010a\u0158\3\2\2\2\u010b\u010d\7\64\2\2\u010c\u010e"+
		"\7\65\2\2\u010d\u010c\3\2\2\2\u010d\u010e\3\2\2\2\u010e\u010f\3\2\2\2"+
		"\u010f\u0110\7\66\2\2\u0110\u0113\7\4\2\2\u0111\u0114\5\26\f\2\u0112\u0114"+
		"\5\30\r\2\u0113\u0111\3\2\2\2\u0113\u0112\3\2\2\2\u0114\u0115\3\2\2\2"+
		"\u0115\u0116\7\5\2\2\u0116\u0118\5\b\5\2\u0117\u0119\7\61\2\2\u0118\u0117"+
		"\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u0158\3\2\2\2\u011a\u011b\7\64\2\2"+
		"\u011b\u011e\7\4\2\2\u011c\u011f\5\26\f\2\u011d\u011f\5\30\r\2\u011e\u011c"+
		"\3\2\2\2\u011e\u011d\3\2\2\2\u011f\u0120\3\2\2\2\u0120\u0121\7\5\2\2\u0121"+
		"\u0122\t\3\2\2\u0122\u0125\7\4\2\2\u0123\u0126\5\26\f\2\u0124\u0126\5"+
		"\30\r\2\u0125\u0123\3\2\2\2\u0125\u0124\3\2\2\2\u0126\u0127\3\2\2\2\u0127"+
		"\u0128\7\5\2\2\u0128\u012a\5\b\5\2\u0129\u012b\7\61\2\2\u012a\u0129\3"+
		"\2\2\2\u012a\u012b\3\2\2\2\u012b\u0158\3\2\2\2\u012c\u012d\79\2\2\u012d"+
		"\u0158\7\61\2\2\u012e\u012f\7:\2\2\u012f\u0134\7\4\2\2\u0130\u0135\5\f"+
		"\7\2\u0131\u0135\5\26\f\2\u0132\u0135\5\30\r\2\u0133\u0135\5\20\t\2\u0134"+
		"\u0130\3\2\2\2\u0134\u0131\3\2\2\2\u0134\u0132\3\2\2\2\u0134\u0133\3\2"+
		"\2\2\u0135\u013e\3\2\2\2\u0136\u013c\7;\2\2\u0137\u0138\5\f\7\2\u0138"+
		"\u0139\5\26\f\2\u0139\u013d\3\2\2\2\u013a\u013d\5\30\r\2\u013b\u013d\5"+
		"\20\t\2\u013c\u0137\3\2\2\2\u013c\u013a\3\2\2\2\u013c\u013b\3\2\2\2\u013d"+
		"\u013f\3\2\2\2\u013e\u0136\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u013e\3\2"+
		"\2\2\u0140\u0141\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0143\7\5\2\2\u0143"+
		"\u0144\7\61\2\2\u0144\u0158\3\2\2\2\u0145\u0146\7:\2\2\u0146\u014c\7\4"+
		"\2\2\u0147\u0148\5\f\7\2\u0148\u0149\5\26\f\2\u0149\u014d\3\2\2\2\u014a"+
		"\u014d\5\30\r\2\u014b\u014d\5\20\t\2\u014c\u0147\3\2\2\2\u014c\u014a\3"+
		"\2\2\2\u014c\u014b\3\2\2\2\u014d\u014e\3\2\2\2\u014e\u014f\7\5\2\2\u014f"+
		"\u0150\7\61\2\2\u0150\u0158\3\2\2\2\u0151\u0152\7<\2\2\u0152\u0158\7\61"+
		"\2\2\u0153\u0154\7=\2\2\u0154\u0155\7>\2\2\u0155\u0158\7\61\2\2\u0156"+
		"\u0158\5\6\4\2\u0157\u00f6\3\2\2\2\u0157\u00fe\3\2\2\2\u0157\u0107\3\2"+
		"\2\2\u0157\u010b\3\2\2\2\u0157\u011a\3\2\2\2\u0157\u012c\3\2\2\2\u0157"+
		"\u012e\3\2\2\2\u0157\u0145\3\2\2\2\u0157\u0151\3\2\2\2\u0157\u0153\3\2"+
		"\2\2\u0157\u0156\3\2\2\2\u0158\17\3\2\2\2\u0159\u015a\7\u00bf\2\2\u015a"+
		"\21\3\2\2\2\u015b\u017c\5\26\f\2\u015c\u017c\5\30\r\2\u015d\u015e\7?\2"+
		"\2\u015e\u015f\7\4\2\2\u015f\u0160\5\60\31\2\u0160\u0163\7\13\2\2\u0161"+
		"\u0164\5\26\f\2\u0162\u0164\5\30\r\2\u0163\u0161\3\2\2\2\u0163\u0162\3"+
		"\2\2\2\u0164\u0165\3\2\2\2\u0165\u0166\7\5\2\2\u0166\u017c\3\2\2\2\u0167"+
		"\u0168\t\4\2\2\u0168\u0169\7\4\2\2\u0169\u016a\5\60\31\2\u016a\u016d\7"+
		"\13\2\2\u016b\u016e\5\26\f\2\u016c\u016e\5\30\r\2\u016d\u016b\3\2\2\2"+
		"\u016d\u016c\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u0170\7\5\2\2\u0170\u017c"+
		"\3\2\2\2\u0171\u0172\t\5\2\2\u0172\u0173\7\4\2\2\u0173\u0174\5\60\31\2"+
		"\u0174\u0177\7\13\2\2\u0175\u0178\5\26\f\2\u0176\u0178\5\30\r\2\u0177"+
		"\u0175\3\2\2\2\u0177\u0176\3\2\2\2\u0178\u0179\3\2\2\2\u0179\u017a\7\5"+
		"\2\2\u017a\u017c\3\2\2\2\u017b\u015b\3\2\2\2\u017b\u015c\3\2\2\2\u017b"+
		"\u015d\3\2\2\2\u017b\u0167\3\2\2\2\u017b\u0171\3\2\2\2\u017c\23\3\2\2"+
		"\2\u017d\u017e\b\13\1\2\u017e\u0185\5\26\f\2\u017f\u0180\5\26\f\2\u0180"+
		"\u0181\7\13\2\2\u0181\u0182\5\26\f\2\u0182\u0185\3\2\2\2\u0183\u0185\5"+
		"8\35\2\u0184\u017d\3\2\2\2\u0184\u017f\3\2\2\2\u0184\u0183\3\2\2\2\u0185"+
		"\u018b\3\2\2\2\u0186\u0187\f\4\2\2\u0187\u0188\7\13\2\2\u0188\u018a\5"+
		"\24\13\5\u0189\u0186\3\2\2\2\u018a\u018d\3\2\2\2\u018b\u0189\3\2\2\2\u018b"+
		"\u018c\3\2\2\2\u018c\25\3\2\2\2\u018d\u018b\3\2\2\2\u018e\u018f\7\u00b1"+
		"\2\2\u018f\u0190\7\4\2\2\u0190\u0191\5\24\13\2\u0191\u0192\7\5\2\2\u0192"+
		"\u040c\3\2\2\2\u0193\u0194\7\u00b2\2\2\u0194\u0195\7\4\2\2\u0195\u0196"+
		"\5\24\13\2\u0196\u0197\7\5\2\2\u0197\u040c\3\2\2\2\u0198\u0199\7\u00b3"+
		"\2\2\u0199\u019c\7\4\2\2\u019a\u019d\5\26\f\2\u019b\u019d\5\30\r\2\u019c"+
		"\u019a\3\2\2\2\u019c\u019b\3\2\2\2\u019d\u019e\3\2\2\2\u019e\u019f\7\5"+
		"\2\2\u019f\u040c\3\2\2\2\u01a0\u040c\5&\24\2\u01a1\u01a4\5&\24\2\u01a2"+
		"\u01a3\7\u00b1\2\2\u01a3\u01a5\5&\24\2\u01a4\u01a2\3\2\2\2\u01a5\u01a6"+
		"\3\2\2\2\u01a6\u01a4\3\2\2\2\u01a6\u01a7\3\2\2\2\u01a7\u040c\3\2\2\2\u01a8"+
		"\u01ab\7\62\2\2\u01a9\u01ac\5\26\f\2\u01aa\u01ac\5\30\r\2\u01ab\u01a9"+
		"\3\2\2\2\u01ab\u01aa\3\2\2\2\u01ac\u01ad\3\2\2\2\u01ad\u01b0\7D\2\2\u01ae"+
		"\u01b1\5\26\f\2\u01af\u01b1\5\30\r\2\u01b0\u01ae\3\2\2\2\u01b0\u01af\3"+
		"\2\2\2\u01b1\u040c\3\2\2\2\u01b2\u01b5\7\62\2\2\u01b3\u01b6\5\26\f\2\u01b4"+
		"\u01b6\5\30\r\2\u01b5\u01b3\3\2\2\2\u01b5\u01b4\3\2\2\2\u01b6\u01b7\3"+
		"\2\2\2\u01b7\u01b8\7D\2\2\u01b8\u01b9\7E\2\2\u01b9\u01ba\7\4\2\2\u01ba"+
		"\u01bb\5\24\13\2\u01bb\u01bc\7\5\2\2\u01bc\u040c\3\2\2\2\u01bd\u01c0\7"+
		"\62\2\2\u01be\u01c1\5\26\f\2\u01bf\u01c1\5\30\r\2\u01c0\u01be\3\2\2\2"+
		"\u01c0\u01bf\3\2\2\2\u01c1\u01c2\3\2\2\2\u01c2\u01c3\7D\2\2\u01c3\u01c4"+
		"\7F\2\2\u01c4\u01c5\7\4\2\2\u01c5\u01c6\5\24\13\2\u01c6\u01c7\7\5\2\2"+
		"\u01c7\u040c\3\2\2\2\u01c8\u01cb\7\62\2\2\u01c9\u01cc\5\26\f\2\u01ca\u01cc"+
		"\5\30\r\2\u01cb\u01c9\3\2\2\2\u01cb\u01ca\3\2\2\2\u01cc\u01cd\3\2\2\2"+
		"\u01cd\u01ce\7\u00b3\2\2\u01ce\u01cf\7D\2\2\u01cf\u01d0\7F\2\2\u01d0\u01d1"+
		"\7\4\2\2\u01d1\u01d2\5\24\13\2\u01d2\u01d3\7\5\2\2\u01d3\u040c\3\2\2\2"+
		"\u01d4\u01d7\7\62\2\2\u01d5\u01d8\5\26\f\2\u01d6\u01d8\5\30\r\2\u01d7"+
		"\u01d5\3\2\2\2\u01d7\u01d6\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9\u01da\7\u00b3"+
		"\2\2\u01da\u01db\7D\2\2\u01db\u01dc\7E\2\2\u01dc\u01dd\7\4\2\2\u01dd\u01de"+
		"\5\24\13\2\u01de\u01df\7\5\2\2\u01df\u040c\3\2\2\2\u01e0\u01e3\7\62\2"+
		"\2\u01e1\u01e4\5\26\f\2\u01e2\u01e4\5\30\r\2\u01e3\u01e1\3\2\2\2\u01e3"+
		"\u01e2\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5\u01e6\7\u00b3\2\2\u01e6\u01e9"+
		"\7D\2\2\u01e7\u01ea\5\26\f\2\u01e8\u01ea\5\30\r\2\u01e9\u01e7\3\2\2\2"+
		"\u01e9\u01e8\3\2\2\2\u01ea\u040c\3\2\2\2\u01eb\u01ee\7\62\2\2\u01ec\u01ef"+
		"\5\26\f\2\u01ed\u01ef\5\30\r\2\u01ee\u01ec\3\2\2\2\u01ee\u01ed\3\2\2\2"+
		"\u01ef\u01f0\3\2\2\2\u01f0\u01f1\7G\2\2\u01f1\u01f4\7D\2\2\u01f2\u01f5"+
		"\5\26\f\2\u01f3\u01f5\5\30\r\2\u01f4\u01f2\3\2\2\2\u01f4\u01f3\3\2\2\2"+
		"\u01f5\u040c\3\2\2\2\u01f6\u01f9\7\62\2\2\u01f7\u01fa\5\26\f\2\u01f8\u01fa"+
		"\5\30\r\2\u01f9\u01f7\3\2\2\2\u01f9\u01f8\3\2\2\2\u01fa\u01fb\3\2\2\2"+
		"\u01fb\u01fc\7G\2\2\u01fc\u01fd\7D\2\2\u01fd\u01fe\7F\2\2\u01fe\u01ff"+
		"\7\4\2\2\u01ff\u0200\5\24\13\2\u0200\u0201\7\5\2\2\u0201\u040c\3\2\2\2"+
		"\u0202\u0205\7\62\2\2\u0203\u0206\5\26\f\2\u0204\u0206\5\30\r\2\u0205"+
		"\u0203\3\2\2\2\u0205\u0204\3\2\2\2\u0206\u0207\3\2\2\2\u0207\u0208\7G"+
		"\2\2\u0208\u0209\7D\2\2\u0209\u020a\7E\2\2\u020a\u020b\7\4\2\2\u020b\u020c"+
		"\5\24\13\2\u020c\u020d\7\5\2\2\u020d\u040c\3\2\2\2\u020e\u0211\7\62\2"+
		"\2\u020f\u0212\5\26\f\2\u0210\u0212\5\30\r\2\u0211\u020f\3\2\2\2\u0211"+
		"\u0210\3\2\2\2\u0212\u0213\3\2\2\2\u0213\u0214\7H\2\2\u0214\u0217\7D\2"+
		"\2\u0215\u0218\5\26\f\2\u0216\u0218\5\30\r\2\u0217\u0215\3\2\2\2\u0217"+
		"\u0216\3\2\2\2\u0218\u040c\3\2\2\2\u0219\u021c\7\62\2\2\u021a\u021d\5"+
		"\26\f\2\u021b\u021d\5\30\r\2\u021c\u021a\3\2\2\2\u021c\u021b\3\2\2\2\u021d"+
		"\u021e\3\2\2\2\u021e\u021f\7H\2\2\u021f\u0220\7D\2\2\u0220\u0221\7F\2"+
		"\2\u0221\u0222\7\4\2\2\u0222\u0223\5\24\13\2\u0223\u0224\7\5\2\2\u0224"+
		"\u040c\3\2\2\2\u0225\u0228\7\62\2\2\u0226\u0229\5\26\f\2\u0227\u0229\5"+
		"\30\r\2\u0228\u0226\3\2\2\2\u0228\u0227\3\2\2\2\u0229\u022a\3\2\2\2\u022a"+
		"\u022b\7H\2\2\u022b\u022c\7D\2\2\u022c\u022d\7E\2\2\u022d\u022e\7\4\2"+
		"\2\u022e\u022f\5\24\13\2\u022f\u0230\7\5\2\2\u0230\u040c\3\2\2\2\u0231"+
		"\u0232\7\u00b6\2\2\u0232\u0235\7\4\2\2\u0233\u0236\5\26\f\2\u0234\u0236"+
		"\5\30\r\2\u0235\u0233\3\2\2\2\u0235\u0234\3\2\2\2\u0236\u0237\3\2\2\2"+
		"\u0237\u0238\7\5\2\2\u0238\u040c\3\2\2\2\u0239\u023a\t\6\2\2\u023a\u023d"+
		"\7\4\2\2\u023b\u023e\5\26\f\2\u023c\u023e\5\30\r\2\u023d\u023b\3\2\2\2"+
		"\u023d\u023c\3\2\2\2\u023e\u023f\3\2\2\2\u023f\u0240\7\5\2\2\u0240\u040c"+
		"\3\2\2\2\u0241\u0242\t\7\2\2\u0242\u0245\7\4\2\2\u0243\u0246\5\26\f\2"+
		"\u0244\u0246\5\30\r\2\u0245\u0243\3\2\2\2\u0245\u0244\3\2\2\2\u0246\u0247"+
		"\3\2\2\2\u0247\u0248\7\5\2\2\u0248\u040c\3\2\2\2\u0249\u024a\7\u00b7\2"+
		"\2\u024a\u024b\7\4\2\2\u024b\u024c\5\24\13\2\u024c\u024d\7\5\2\2\u024d"+
		"\u040c\3\2\2\2\u024e\u024f\7\u00b7\2\2\u024f\u0250\7\4\2\2\u0250\u0251"+
		"\5\24\13\2\u0251\u0252\7\13\2\2\u0252\u0253\7M\2\2\u0253\u0254\7\5\2\2"+
		"\u0254\u040c\3\2\2\2\u0255\u0256\7\u00b4\2\2\u0256\u0257\7\4\2\2\u0257"+
		"\u0258\5\24\13\2\u0258\u0259\7\5\2\2\u0259\u040c\3\2\2\2\u025a\u025b\7"+
		"\u00b5\2\2\u025b\u025c\7\4\2\2\u025c\u025d\5\24\13\2\u025d\u025e\7\5\2"+
		"\2\u025e\u040c\3\2\2\2\u025f\u040c\5(\25\2\u0260\u0261\7N\2\2\u0261\u0264"+
		"\7O\2\2\u0262\u0264\7P\2\2\u0263\u0260\3\2\2\2\u0263\u0262\3\2\2\2\u0264"+
		"\u0265\3\2\2\2\u0265\u0268\7\4\2\2\u0266\u0269\5\26\f\2\u0267\u0269\5"+
		"\30\r\2\u0268\u0266\3\2\2\2\u0268\u0267\3\2\2\2\u0269\u026a\3\2\2\2\u026a"+
		"\u026b\7\5\2\2\u026b\u040c\3\2\2\2\u026c\u026d\7Q\2\2\u026d\u0270\7O\2"+
		"\2\u026e\u0270\7R\2\2\u026f\u026c\3\2\2\2\u026f\u026e\3\2\2\2\u0270\u0271"+
		"\3\2\2\2\u0271\u0274\7\4\2\2\u0272\u0275\5\26\f\2\u0273\u0275\5\30\r\2"+
		"\u0274\u0272\3\2\2\2\u0274\u0273\3\2\2\2\u0275\u0276\3\2\2\2\u0276\u0277"+
		"\7\5\2\2\u0277\u040c\3\2\2\2\u0278\u0279\7N\2\2\u0279\u027c\7O\2\2\u027a"+
		"\u027c\7P\2\2\u027b\u0278\3\2\2\2\u027b\u027a\3\2\2\2\u027c\u027d\3\2"+
		"\2\2\u027d\u0280\7\4\2\2\u027e\u0281\5\26\f\2\u027f\u0281\5\30\r\2\u0280"+
		"\u027e\3\2\2\2\u0280\u027f\3\2\2\2\u0281\u0282\3\2\2\2\u0282\u0285\7\13"+
		"\2\2\u0283\u0286\5\26\f\2\u0284\u0286\5\30\r\2\u0285\u0283\3\2\2\2\u0285"+
		"\u0284\3\2\2\2\u0286\u0287\3\2\2\2\u0287\u0288\7\5\2\2\u0288\u040c\3\2"+
		"\2\2\u0289\u028a\7Q\2\2\u028a\u028d\7O\2\2\u028b\u028d\7R\2\2\u028c\u0289"+
		"\3\2\2\2\u028c\u028b\3\2\2\2\u028d\u028e\3\2\2\2\u028e\u0291\7\4\2\2\u028f"+
		"\u0292\5\26\f\2\u0290\u0292\5\30\r\2\u0291\u028f\3\2\2\2\u0291\u0290\3"+
		"\2\2\2\u0292\u0293\3\2\2\2\u0293\u0296\7\13\2\2\u0294\u0297\5\26\f\2\u0295"+
		"\u0297\5\30\r\2\u0296\u0294\3\2\2\2\u0296\u0295\3\2\2\2\u0297\u0298\3"+
		"\2\2\2\u0298\u0299\7\5\2\2\u0299\u040c\3\2\2\2\u029a\u029b\7Q\2\2\u029b"+
		"\u029e\7O\2\2\u029c\u029e\7R\2\2\u029d\u029a\3\2\2\2\u029d\u029c\3\2\2"+
		"\2\u029e\u029f\3\2\2\2\u029f\u02a2\7\4\2\2\u02a0\u02a3\5\26\f\2\u02a1"+
		"\u02a3\5\30\r\2\u02a2\u02a0\3\2\2\2\u02a2\u02a1\3\2\2\2\u02a3\u02a4\3"+
		"\2\2\2\u02a4\u02a7\7\13\2\2\u02a5\u02a8\5\26\f\2\u02a6\u02a8\5\30\r\2"+
		"\u02a7\u02a5\3\2\2\2\u02a7\u02a6\3\2\2\2\u02a8\u02a9\3\2\2\2\u02a9\u02aa"+
		"\7\5\2\2\u02aa\u040c\3\2\2\2\u02ab\u02ac\t\b\2\2\u02ac\u02af\7\4\2\2\u02ad"+
		"\u02b0\5\26\f\2\u02ae\u02b0\5\30\r\2\u02af\u02ad\3\2\2\2\u02af\u02ae\3"+
		"\2\2\2\u02b0\u02b1\3\2\2\2\u02b1\u02b2\7\13\2\2\u02b2\u02b4\5,\27\2\u02b3"+
		"\u02b5\5\62\32\2\u02b4\u02b3\3\2\2\2\u02b4\u02b5\3\2\2\2\u02b5\u02b6\3"+
		"\2\2\2\u02b6\u02b7\7\13\2\2\u02b7\u02b9\5.\30\2\u02b8\u02ba\5\62\32\2"+
		"\u02b9\u02b8\3\2\2\2\u02b9\u02ba\3\2\2\2\u02ba\u02bb\3\2\2\2\u02bb\u02bc"+
		"\7\5\2\2\u02bc\u040c\3\2\2\2\u02bd\u02be\7V\2\2\u02be\u02c1\7\4\2\2\u02bf"+
		"\u02c2\5\26\f\2\u02c0\u02c2\5\30\r\2\u02c1\u02bf\3\2\2\2\u02c1\u02c0\3"+
		"\2\2\2\u02c2\u02c3\3\2\2\2\u02c3\u02c4\7\13\2\2\u02c4\u02c5\t\t\2\2\u02c5"+
		"\u02c6\7\13\2\2\u02c6\u02c8\t\n\2\2\u02c7\u02c9\5\62\32\2\u02c8\u02c7"+
		"\3\2\2\2\u02c8\u02c9\3\2\2\2\u02c9\u02ca\3\2\2\2\u02ca\u02cb\7\13\2\2"+
		"\u02cb\u02cd\t\13\2\2\u02cc\u02ce\5\62\32\2\u02cd\u02cc\3\2\2\2\u02cd"+
		"\u02ce\3\2\2\2\u02ce\u02cf\3\2\2\2\u02cf\u02d0\7\5\2\2\u02d0\u040c\3\2"+
		"\2\2\u02d1\u02d2\7V\2\2\u02d2\u02d5\7\4\2\2\u02d3\u02d6\5\26\f\2\u02d4"+
		"\u02d6\5\30\r\2\u02d5\u02d3\3\2\2\2\u02d5\u02d4\3\2\2\2\u02d6\u02d7\3"+
		"\2\2\2\u02d7\u02da\7\13\2\2\u02d8\u02db\5\26\f\2\u02d9\u02db\5\30\r\2"+
		"\u02da\u02d8\3\2\2\2\u02da\u02d9\3\2\2\2\u02db\u02dc\3\2\2\2\u02dc\u02dd"+
		"\7\13\2\2\u02dd\u02de\t\t\2\2\u02de\u02df\7\13\2\2\u02df\u02e1\t\n\2\2"+
		"\u02e0\u02e2\5\62\32\2\u02e1\u02e0\3\2\2\2\u02e1\u02e2\3\2\2\2\u02e2\u02e3"+
		"\3\2\2\2\u02e3\u02e4\7\13\2\2\u02e4\u02e6\t\n\2\2\u02e5\u02e7\5\62\32"+
		"\2\u02e6\u02e5\3\2\2\2\u02e6\u02e7\3\2\2\2\u02e7\u02e8\3\2\2\2\u02e8\u02e9"+
		"\7\5\2\2\u02e9\u040c\3\2\2\2\u02ea\u02eb\7V\2\2\u02eb\u02ec\7\4\2\2\u02ec"+
		"\u02ed\5\36\20\2\u02ed\u02ee\7\13\2\2\u02ee\u02ef\5 \21\2\u02ef\u02f0"+
		"\7\13\2\2\u02f0\u02f1\t\t\2\2\u02f1\u02f2\7\13\2\2\u02f2\u02f4\t\n\2\2"+
		"\u02f3\u02f5\5\62\32\2\u02f4\u02f3\3\2\2\2\u02f4\u02f5\3\2\2\2\u02f5\u02f6"+
		"\3\2\2\2\u02f6\u02f7\7\13\2\2\u02f7\u02f9\t\n\2\2\u02f8\u02fa\5\62\32"+
		"\2\u02f9\u02f8\3\2\2\2\u02f9\u02fa\3\2\2\2\u02fa\u02fb\3\2\2\2\u02fb\u02fc"+
		"\7\5\2\2\u02fc\u040c\3\2\2\2\u02fd\u02fe\7Z\2\2\u02fe\u0301\7[\2\2\u02ff"+
		"\u0301\7\\\2\2\u0300\u02fd\3\2\2\2\u0300\u02ff\3\2\2\2\u0301\u0302\3\2"+
		"\2\2\u0302\u0305\7\4\2\2\u0303\u0306\5\26\f\2\u0304\u0306\5\30\r\2\u0305"+
		"\u0303\3\2\2\2\u0305\u0304\3\2\2\2\u0306\u0307\3\2\2\2\u0307\u0308\7\5"+
		"\2\2\u0308\u040c\3\2\2\2\u0309\u030a\7]\2\2\u030a\u030b\7Z\2\2\u030b\u030e"+
		"\7[\2\2\u030c\u030e\7^\2\2\u030d\u0309\3\2\2\2\u030d\u030c\3\2\2\2\u030e"+
		"\u030f\3\2\2\2\u030f\u0312\7\4\2\2\u0310\u0313\5\26\f\2\u0311\u0313\5"+
		"\30\r\2\u0312\u0310\3\2\2\2\u0312\u0311\3\2\2\2\u0313\u0314\3\2\2\2\u0314"+
		"\u0315\7\5\2\2\u0315\u040c\3\2\2\2\u0316\u0317\7G\2\2\u0317\u031a\7_\2"+
		"\2\u0318\u031a\7`\2\2\u0319\u0316\3\2\2\2\u0319\u0318\3\2\2\2\u031a\u031b"+
		"\3\2\2\2\u031b\u031e\7\4\2\2\u031c\u031f\5\26\f\2\u031d\u031f\5\30\r\2"+
		"\u031e\u031c\3\2\2\2\u031e\u031d\3\2\2\2\u031f\u0320\3\2\2\2\u0320\u0321"+
		"\7\5\2\2\u0321\u040c\3\2\2\2\u0322\u0323\7a\2\2\u0323\u0326\7\4\2\2\u0324"+
		"\u0327\5\26\f\2\u0325\u0327\5\30\r\2\u0326\u0324\3\2\2\2\u0326\u0325\3"+
		"\2\2\2\u0327\u0328\3\2\2\2\u0328\u0329\7\13\2\2\u0329\u032a\t\n\2\2\u032a"+
		"\u032b\7\13\2\2\u032b\u032c\t\n\2\2\u032c\u032d\7\5\2\2\u032d\u040c\3"+
		"\2\2\2\u032e\u032f\7a\2\2\u032f\u0332\7\4\2\2\u0330\u0333\5\26\f\2\u0331"+
		"\u0333\5\30\r\2\u0332\u0330\3\2\2\2\u0332\u0331\3\2\2\2\u0333\u0334\3"+
		"\2\2\2\u0334\u0337\7\13\2\2\u0335\u0338\5\26\f\2\u0336\u0338\5\30\r\2"+
		"\u0337\u0335\3\2\2\2\u0337\u0336\3\2\2\2\u0338\u0339\3\2\2\2\u0339\u033a"+
		"\7\13\2\2\u033a\u033b\t\t\2\2\u033b\u033c\7\13\2\2\u033c\u033d\t\n\2\2"+
		"\u033d\u033e\7\13\2\2\u033e\u033f\t\n\2\2\u033f\u0340\7\5\2\2\u0340\u040c"+
		"\3\2\2\2\u0341\u0342\7a\2\2\u0342\u0343\7\4\2\2\u0343\u0344\5\36\20\2"+
		"\u0344\u0345\7\13\2\2\u0345\u0346\5 \21\2\u0346\u0347\7\13\2\2\u0347\u0348"+
		"\t\t\2\2\u0348\u0349\7\13\2\2\u0349\u034a\t\n\2\2\u034a\u034b\7\13\2\2"+
		"\u034b\u034c\t\n\2\2\u034c\u034d\7\5\2\2\u034d\u040c\3\2\2\2\u034e\u034f"+
		"\t\f\2\2\u034f\u0352\7\4\2\2\u0350\u0353\5\26\f\2\u0351\u0353\5\30\r\2"+
		"\u0352\u0350\3\2\2\2\u0352\u0351\3\2\2\2\u0353\u0354\3\2\2\2\u0354\u0357"+
		"\7\13\2\2\u0355\u0358\5\26\f\2\u0356\u0358\5\30\r\2\u0357\u0355\3\2\2"+
		"\2\u0357\u0356\3\2\2\2\u0358\u0359\3\2\2\2\u0359\u035a\7\5\2\2\u035a\u040c"+
		"\3\2\2\2\u035b\u035c\t\r\2\2\u035c\u035f\7\4\2\2\u035d\u0360\5\26\f\2"+
		"\u035e\u0360\5\30\r\2\u035f\u035d\3\2\2\2\u035f\u035e\3\2\2\2\u0360\u0362"+
		"\3\2\2\2\u0361\u0363\7f\2\2\u0362\u0361\3\2\2\2\u0362\u0363\3\2\2\2\u0363"+
		"\u0364\3\2\2\2\u0364\u0367\7\13\2\2\u0365\u0368\5\26\f\2\u0366\u0368\5"+
		"\30\r\2\u0367\u0365\3\2\2\2\u0367\u0366\3\2\2\2\u0368\u036a\3\2\2\2\u0369"+
		"\u036b\7f\2\2\u036a\u0369\3\2\2\2\u036a\u036b\3\2\2\2\u036b\u036c\3\2"+
		"\2\2\u036c\u036d\7\5\2\2\u036d\u040c\3\2\2\2\u036e\u036f\7g\2\2\u036f"+
		"\u0372\7\4\2\2\u0370\u0373\5\26\f\2\u0371\u0373\5\30\r\2\u0372\u0370\3"+
		"\2\2\2\u0372\u0371\3\2\2\2\u0373\u0374\3\2\2\2\u0374\u0377\7\13\2\2\u0375"+
		"\u0378\5\26\f\2\u0376\u0378\5\30\r\2\u0377\u0375\3\2\2\2\u0377\u0376\3"+
		"\2\2\2\u0378\u0379\3\2\2\2\u0379\u037a\7\5\2\2\u037a\u040c\3\2\2\2\u037b"+
		"\u037c\7h\2\2\u037c\u037f\7\4\2\2\u037d\u0380\5\26\f\2\u037e\u0380\5\30"+
		"\r\2\u037f\u037d\3\2\2\2\u037f\u037e\3\2\2\2\u0380\u0381\3\2\2\2\u0381"+
		"\u0384\7\13\2\2\u0382\u0385\5\26\f\2\u0383\u0385\5\30\r\2\u0384\u0382"+
		"\3\2\2\2\u0384\u0383\3\2\2\2\u0385\u0386\3\2\2\2\u0386\u0387\7\5\2\2\u0387"+
		"\u040c\3\2\2\2\u0388\u0389\7i\2\2\u0389\u038c\7\4\2\2\u038a\u038d\5\26"+
		"\f\2\u038b\u038d\5\30\r\2\u038c\u038a\3\2\2\2\u038c\u038b\3\2\2\2\u038d"+
		"\u038e\3\2\2\2\u038e\u0391\7\13\2\2\u038f\u0392\5\26\f\2\u0390\u0392\5"+
		"\30\r\2\u0391\u038f\3\2\2\2\u0391\u0390\3\2\2\2\u0392\u0393\3\2\2\2\u0393"+
		"\u0394\7\5\2\2\u0394\u040c\3\2\2\2\u0395\u0396\7j\2\2\u0396\u0399\7\4"+
		"\2\2\u0397\u039a\5\26\f\2\u0398\u039a\5\30\r\2\u0399\u0397\3\2\2\2\u0399"+
		"\u0398\3\2\2\2\u039a\u039b\3\2\2\2\u039b\u039e\7\13\2\2\u039c\u039f\5"+
		"\26\f\2\u039d\u039f\5\30\r\2\u039e\u039c\3\2\2\2\u039e\u039d\3\2\2\2\u039f"+
		"\u03a0\3\2\2\2\u03a0\u03a1\7\5\2\2\u03a1\u040c\3\2\2\2\u03a2\u03a3\7k"+
		"\2\2\u03a3\u03a4\7\4\2\2\u03a4\u03a5\5\32\16\2\u03a5\u03a6\7\5\2\2\u03a6"+
		"\u040c\3\2\2\2\u03a7\u03a8\7l\2\2\u03a8\u03a9\7\4\2\2\u03a9\u03aa\5\32"+
		"\16\2\u03aa\u03ab\7\5\2\2\u03ab\u040c\3\2\2\2\u03ac\u03ad\7l\2\2\u03ad"+
		"\u03ae\7\4\2\2\u03ae\u03af\7k\2\2\u03af\u03b0\7\4\2\2\u03b0\u03b1\5\32"+
		"\16\2\u03b1\u03b2\7\5\2\2\u03b2\u03b3\7\5\2\2\u03b3\u040c\3\2\2\2\u03b4"+
		"\u03b5\7k\2\2\u03b5\u03b6\7\4\2\2\u03b6\u03b7\7l\2\2\u03b7\u03b8\7\4\2"+
		"\2\u03b8\u03b9\5\32\16\2\u03b9\u03ba\7\5\2\2\u03ba\u03bb\7\5\2\2\u03bb"+
		"\u040c\3\2\2\2\u03bc\u03bd\t\16\2\2\u03bd\u03be\7\f\2\2\u03be\u040c\7"+
		"\u00bf\2\2\u03bf\u03c4\7o\2\2\u03c0\u03c4\7p\2\2\u03c1\u03c2\7q\2\2\u03c2"+
		"\u03c4\7r\2\2\u03c3\u03bf\3\2\2\2\u03c3\u03c0\3\2\2\2\u03c3\u03c1\3\2"+
		"\2\2\u03c4\u03c5\3\2\2\2\u03c5\u03c6\7\f\2\2\u03c6\u040c\7\u00bf\2\2\u03c7"+
		"\u03cc\7s\2\2\u03c8\u03cc\7t\2\2\u03c9\u03ca\7u\2\2\u03ca\u03cc\7r\2\2"+
		"\u03cb\u03c7\3\2\2\2\u03cb\u03c8\3\2\2\2\u03cb\u03c9\3\2\2\2\u03cc\u03cd"+
		"\3\2\2\2\u03cd\u03ce\7\f\2\2\u03ce\u040c\7\u00bf\2\2\u03cf\u03d4\7s\2"+
		"\2\u03d0\u03d4\7t\2\2\u03d1\u03d2\7u\2\2\u03d2\u03d4\7r\2\2\u03d3\u03cf"+
		"\3\2\2\2\u03d3\u03d0\3\2\2\2\u03d3\u03d1\3\2\2\2\u03d4\u040c\3\2\2\2\u03d5"+
		"\u03d6\t\17\2\2\u03d6\u03d7\7\4\2\2\u03d7\u03d8\7\u00a9\2\2\u03d8\u03d9"+
		"\7\f\2\2\u03d9\u03da\7\u00bb\2\2\u03da\u040c\7\5\2\2\u03db\u03dc\t\17"+
		"\2\2\u03dc\u03dd\7\4\2\2\u03dd\u03de\7\u00a9\2\2\u03de\u03df\7\f\2\2\u03df"+
		"\u03e0\7\u00bb\2\2\u03e0\u03e1\7\13\2\2\u03e1\u03e2\7z\2\2\u03e2\u03e3"+
		"\7\f\2\2\u03e3\u03e4\7\u00bf\2\2\u03e4\u040c\7\5\2\2\u03e5\u03e6\t\17"+
		"\2\2\u03e6\u03e7\7\4\2\2\u03e7\u03e8\7\u00a9\2\2\u03e8\u03e9\7\f\2\2\u03e9"+
		"\u03ea\7\u00bb\2\2\u03ea\u03eb\7\13\2\2\u03eb\u03ec\7{\2\2\u03ec\u03ed"+
		"\7\f\2\2\u03ed\u03ee\7\u00bf\2\2\u03ee\u040c\7\5\2\2\u03ef\u03f0\t\17"+
		"\2\2\u03f0\u03f1\7\4\2\2\u03f1\u03f2\7\u00a9\2\2\u03f2\u03f3\7\f\2\2\u03f3"+
		"\u03f4\7\u00bb\2\2\u03f4\u03f5\7\13\2\2\u03f5\u03f6\7z\2\2\u03f6\u03f7"+
		"\7\f\2\2\u03f7\u03f8\7\u00bf\2\2\u03f8\u03f9\7\13\2\2\u03f9\u03fa\7{\2"+
		"\2\u03fa\u03fb\7\f\2\2\u03fb\u03fc\7\u00bf\2\2\u03fc\u040c\7\5\2\2\u03fd"+
		"\u03fe\t\17\2\2\u03fe\u03ff\7\4\2\2\u03ff\u0400\7\u00a9\2\2\u0400\u0401"+
		"\7\f\2\2\u0401\u0402\7\u00bb\2\2\u0402\u0403\7\13\2\2\u0403\u0404\7{\2"+
		"\2\u0404\u0405\7\f\2\2\u0405\u0406\7\u00bf\2\2\u0406\u0407\7\13\2\2\u0407"+
		"\u0408\7z\2\2\u0408\u0409\7\f\2\2\u0409\u040a\7\u00bf\2\2\u040a\u040c"+
		"\7\5\2\2\u040b\u018e\3\2\2\2\u040b\u0193\3\2\2\2\u040b\u0198\3\2\2\2\u040b"+
		"\u01a0\3\2\2\2\u040b\u01a1\3\2\2\2\u040b\u01a8\3\2\2\2\u040b\u01b2\3\2"+
		"\2\2\u040b\u01bd\3\2\2\2\u040b\u01c8\3\2\2\2\u040b\u01d4\3\2\2\2\u040b"+
		"\u01e0\3\2\2\2\u040b\u01eb\3\2\2\2\u040b\u01f6\3\2\2\2\u040b\u0202\3\2"+
		"\2\2\u040b\u020e\3\2\2\2\u040b\u0219\3\2\2\2\u040b\u0225\3\2\2\2\u040b"+
		"\u0231\3\2\2\2\u040b\u0239\3\2\2\2\u040b\u0241\3\2\2\2\u040b\u0249\3\2"+
		"\2\2\u040b\u024e\3\2\2\2\u040b\u0255\3\2\2\2\u040b\u025a\3\2\2\2\u040b"+
		"\u025f\3\2\2\2\u040b\u0263\3\2\2\2\u040b\u026f\3\2\2\2\u040b\u027b\3\2"+
		"\2\2\u040b\u028c\3\2\2\2\u040b\u029d\3\2\2\2\u040b\u02ab\3\2\2\2\u040b"+
		"\u02bd\3\2\2\2\u040b\u02d1\3\2\2\2\u040b\u02ea\3\2\2\2\u040b\u0300\3\2"+
		"\2\2\u040b\u030d\3\2\2\2\u040b\u0319\3\2\2\2\u040b\u0322\3\2\2\2\u040b"+
		"\u032e\3\2\2\2\u040b\u0341\3\2\2\2\u040b\u034e\3\2\2\2\u040b\u035b\3\2"+
		"\2\2\u040b\u036e\3\2\2\2\u040b\u037b\3\2\2\2\u040b\u0388\3\2\2\2\u040b"+
		"\u0395\3\2\2\2\u040b\u03a2\3\2\2\2\u040b\u03a7\3\2\2\2\u040b\u03ac\3\2"+
		"\2\2\u040b\u03b4\3\2\2\2\u040b\u03bc\3\2\2\2\u040b\u03c3\3\2\2\2\u040b"+
		"\u03cb\3\2\2\2\u040b\u03d3\3\2\2\2\u040b\u03d5\3\2\2\2\u040b\u03db\3\2"+
		"\2\2\u040b\u03e5\3\2\2\2\u040b\u03ef\3\2\2\2\u040b\u03fd\3\2\2\2\u040c"+
		"\27\3\2\2\2\u040d\u040e\7\u00aa\2\2\u040e\u040f\7\f\2\2\u040f\u049e\t"+
		"\20\2\2\u0410\u049e\7\u00aa\2\2\u0411\u0412\7\u00a9\2\2\u0412\u0413\7"+
		"\f\2\2\u0413\u049e\7\u00bb\2\2\u0414\u049e\7\u00a9\2\2\u0415\u0416\7|"+
		"\2\2\u0416\u0417\7\f\2\2\u0417\u049e\7\u00bf\2\2\u0418\u0419\t\21\2\2"+
		"\u0419\u041a\7\4\2\2\u041a\u041b\t\22\2\2\u041b\u041c\7\13\2\2\u041c\u041d"+
		"\t\13\2\2\u041d\u049e\7\5\2\2\u041e\u041f\7\177\2\2\u041f\u0420\7\f\2"+
		"\2\u0420\u049e\7\u00bf\2\2\u0421\u0422\7\u0080\2\2\u0422\u0423\7\f\2\2"+
		"\u0423\u049e\7\u00bf\2\2\u0424\u049e\7\u00ab\2\2\u0425\u049e\7\u0081\2"+
		"\2\u0426\u0427\7\u0081\2\2\u0427\u0428\7\f\2\2\u0428\u049e\7\u00bb\2\2"+
		"\u0429\u049e\7\u00af\2\2\u042a\u049e\t\23\2\2\u042b\u049e\7\u00b0\2\2"+
		"\u042c\u0430\7\u0083\2\2\u042d\u042e\7\u0084\2\2\u042e\u0430\7\16\2\2"+
		"\u042f\u042c\3\2\2\2\u042f\u042d\3\2\2\2\u0430\u049e\3\2\2\2\u0431\u0435"+
		"\7\u0085\2\2\u0432\u0433\7\u0084\2\2\u0433\u0435\7\17\2\2\u0434\u0431"+
		"\3\2\2\2\u0434\u0432\3\2\2\2\u0435\u049e\3\2\2\2\u0436\u0437\t\23\2\2"+
		"\u0437\u0438\7\4\2\2\u0438\u0439\5\66\34\2\u0439\u043a\7\5\2\2\u043a\u049e"+
		"\3\2\2\2\u043b\u043c\t\23\2\2\u043c\u043d\7\4\2\2\u043d\u043e\5\24\13"+
		"\2\u043e\u043f\7\5\2\2\u043f\u049e\3\2\2\2\u0440\u0441\t\23\2\2\u0441"+
		"\u0446\7\4\2\2\u0442\u0447\7o\2\2\u0443\u0447\7p\2\2\u0444\u0445\7q\2"+
		"\2\u0445\u0447\7r\2\2\u0446\u0442\3\2\2\2\u0446\u0443\3\2\2\2\u0446\u0444"+
		"\3\2\2\2\u0447\u0448\3\2\2\2\u0448\u0449\7\f\2\2\u0449\u044a\7\u00bf\2"+
		"\2\u044a\u044b\7\13\2\2\u044b\u044c\5\66\34\2\u044c\u044d\7\5\2\2\u044d"+
		"\u049e\3\2\2\2\u044e\u049e\5\64\33\2\u044f\u0450\7\u0086\2\2\u0450\u0451"+
		"\7\4\2\2\u0451\u0453\t\n\2\2\u0452\u0454\5\62\32\2\u0453\u0452\3\2\2\2"+
		"\u0453\u0454\3\2\2\2\u0454\u0455\3\2\2\2\u0455\u0456\7\13\2\2\u0456\u0458"+
		"\t\n\2\2\u0457\u0459\5\62\32\2\u0458\u0457\3\2\2\2\u0458\u0459\3\2\2\2"+
		"\u0459\u045a\3\2\2\2\u045a\u049e\7\5\2\2\u045b\u045c\7\u0087\2\2\u045c"+
		"\u045d\7\4\2\2\u045d\u045e\7\u00bf\2\2\u045e\u045f\7\13\2\2\u045f\u0460"+
		"\5\34\17\2\u0460\u0461\7\13\2\2\u0461\u0462\5\34\17\2\u0462\u0463\7\5"+
		"\2\2\u0463\u049e\3\2\2\2\u0464\u0465\t\24\2\2\u0465\u0466\7\4\2\2\u0466"+
		"\u0467\7\u00bf\2\2\u0467\u0468\7\13\2\2\u0468\u0469\5\34\17\2\u0469\u046a"+
		"\7\13\2\2\u046a\u046b\5\34\17\2\u046b\u046c\7\5\2\2\u046c\u049e\3\2\2"+
		"\2\u046d\u046e\7\u0087\2\2\u046e\u046f\7\4\2\2\u046f\u0470\7\u00bf\2\2"+
		"\u0470\u049e\7\5\2\2\u0471\u049e\t\24\2\2\u0472\u049e\7\u0087\2\2\u0473"+
		"\u0474\t\24\2\2\u0474\u0475\7\4\2\2\u0475\u0476\7\u00bf\2\2\u0476\u0477"+
		"\7\13\2\2\u0477\u0478\7\u00bf\2\2\u0478\u049e\7\5\2\2\u0479\u047a\t\24"+
		"\2\2\u047a\u047b\7\4\2\2\u047b\u047c\7\u00bf\2\2\u047c\u049e\7\5\2\2\u047d"+
		"\u047e\7\u00b7\2\2\u047e\u047f\7\4\2\2\u047f\u0481\t\n\2\2\u0480\u0482"+
		"\5\62\32\2\u0481\u0480\3\2\2\2\u0481\u0482\3\2\2\2\u0482\u0483\3\2\2\2"+
		"\u0483\u0484\7\13\2\2\u0484\u0486\t\n\2\2\u0485\u0487\5\62\32\2\u0486"+
		"\u0485\3\2\2\2\u0486\u0487\3\2\2\2\u0487\u0488\3\2\2\2\u0488\u049e\7\5"+
		"\2\2\u0489\u049e\t\25\2\2\u048a\u048b\7\u008b\2\2\u048b\u048c\7\f\2\2"+
		"\u048c\u049e\7\u00bf\2\2\u048d\u049e\5\32\16\2\u048e\u048f\t\26\2\2\u048f"+
		"\u0490\7\4\2\2\u0490\u0493\7\u00bb\2\2\u0491\u0492\7\13\2\2\u0492\u0494"+
		"\7\u00bb\2\2\u0493\u0491\3\2\2\2\u0494\u0495\3\2\2\2\u0495\u0496\3\2\2"+
		"\2\u0495\u0493\3\2\2\2\u0496\u0497\3\2\2\2\u0497\u049e\7\5\2\2\u0498\u0499"+
		"\t\26\2\2\u0499\u049a\7\4\2\2\u049a\u049b\7\u00bb\2\2\u049b\u049e\7\5"+
		"\2\2\u049c\u049e\5\f\7\2\u049d\u040d\3\2\2\2\u049d\u0410\3\2\2\2\u049d"+
		"\u0411\3\2\2\2\u049d\u0414\3\2\2\2\u049d\u0415\3\2\2\2\u049d\u0418\3\2"+
		"\2\2\u049d\u041e\3\2\2\2\u049d\u0421\3\2\2\2\u049d\u0424\3\2\2\2\u049d"+
		"\u0425\3\2\2\2\u049d\u0426\3\2\2\2\u049d\u0429\3\2\2\2\u049d\u042a\3\2"+
		"\2\2\u049d\u042b\3\2\2\2\u049d\u042f\3\2\2\2\u049d\u0434\3\2\2\2\u049d"+
		"\u0436\3\2\2\2\u049d\u043b\3\2\2\2\u049d\u0440\3\2\2\2\u049d\u044e\3\2"+
		"\2\2\u049d\u044f\3\2\2\2\u049d\u045b\3\2\2\2\u049d\u0464\3\2\2\2\u049d"+
		"\u046d\3\2\2\2\u049d\u0471\3\2\2\2\u049d\u0472\3\2\2\2\u049d\u0473\3\2"+
		"\2\2\u049d\u0479\3\2\2\2\u049d\u047d\3\2\2\2\u049d\u0489\3\2\2\2\u049d"+
		"\u048a\3\2\2\2\u049d\u048d\3\2\2\2\u049d\u048e\3\2\2\2\u049d\u0498\3\2"+
		"\2\2\u049d\u049c\3\2\2\2\u049e\31\3\2\2\2\u049f\u04a0\7\u00a7\2\2\u04a0"+
		"\u04a1\7\f\2\2\u04a1\u04a8\t\27\2\2\u04a2\u04a8\7\u00a7\2\2\u04a3\u04a4"+
		"\7\u00a8\2\2\u04a4\u04a5\7\f\2\2\u04a5\u04a8\7\u00b9\2\2\u04a6\u04a8\7"+
		"\u00a8\2\2\u04a7\u049f\3\2\2\2\u04a7\u04a2\3\2\2\2\u04a7\u04a3\3\2\2\2"+
		"\u04a7\u04a6\3\2\2\2\u04a8\33\3\2\2\2\u04a9\u04b5\7\u00bb\2\2\u04aa\u04b5"+
		"\7\u00bc\2\2\u04ab\u04ac\7\u00bb\2\2\u04ac\u04ad\7\u008d\2\2\u04ad\u04b5"+
		"\7\u00bb\2\2\u04ae\u04af\7\u00bc\2\2\u04af\u04b0\7\u008d\2\2\u04b0\u04b5"+
		"\7\u00bb\2\2\u04b1\u04b5\7\u00ba\2\2\u04b2\u04b5\7Y\2\2\u04b3\u04b5\7"+
		"X\2\2\u04b4\u04a9\3\2\2\2\u04b4\u04aa\3\2\2\2\u04b4\u04ab\3\2\2\2\u04b4"+
		"\u04ae\3\2\2\2\u04b4\u04b1\3\2\2\2\u04b4\u04b2\3\2\2\2\u04b4\u04b3\3\2"+
		"\2\2\u04b5\35\3\2\2\2\u04b6\u04b9\5\26\f\2\u04b7\u04b9\5\30\r\2\u04b8"+
		"\u04b6\3\2\2\2\u04b8\u04b7\3\2\2\2\u04b9\u04bb\3\2\2\2\u04ba\u04bc\7f"+
		"\2\2\u04bb\u04ba\3\2\2\2\u04bb\u04bc\3\2\2\2\u04bc\37\3\2\2\2\u04bd\u04c0"+
		"\5\26\f\2\u04be\u04c0\5\30\r\2\u04bf\u04bd\3\2\2\2\u04bf\u04be\3\2\2\2"+
		"\u04c0\u04c2\3\2\2\2\u04c1\u04c3\7f\2\2\u04c2\u04c1\3\2\2\2\u04c2\u04c3"+
		"\3\2\2\2\u04c3!\3\2\2\2\u04c4\u04c7\5\26\f\2\u04c5\u04c7\5\30\r\2\u04c6"+
		"\u04c4\3\2\2\2\u04c6\u04c5\3\2\2\2\u04c7\u04c9\3\2\2\2\u04c8\u04ca\7f"+
		"\2\2\u04c9\u04c8\3\2\2\2\u04c9\u04ca\3\2\2\2\u04ca#\3\2\2\2\u04cb\u04ce"+
		"\5\26\f\2\u04cc\u04ce\5\30\r\2\u04cd\u04cb\3\2\2\2\u04cd\u04cc\3\2\2\2"+
		"\u04ce%\3\2\2\2\u04cf\u04d0\7\4\2\2\u04d0\u04d7\5\36\20\2\u04d1\u04d2"+
		"\7\u00b1\2\2\u04d2\u04d4\5\36\20\2\u04d3\u04d1\3\2\2\2\u04d4\u04d5\3\2"+
		"\2\2\u04d5\u04d3\3\2\2\2\u04d5\u04d6\3\2\2\2\u04d6\u04d8\3\2\2\2\u04d7"+
		"\u04d3\3\2\2\2\u04d7\u04d8\3\2\2\2\u04d8\u04d9\3\2\2\2\u04d9\u04da\7\5"+
		"\2\2\u04da\u04db\t\30\2\2\u04db\u04dc\5 \21\2\u04dc\u054f\3\2\2\2\u04dd"+
		"\u04de\7]\2\2\u04de\u04df\7\4\2\2\u04df\u04e6\5$\23\2\u04e0\u04e1\7\u00b1"+
		"\2\2\u04e1\u04e3\5$\23\2\u04e2\u04e0\3\2\2\2\u04e3\u04e4\3\2\2\2\u04e4"+
		"\u04e2\3\2\2\2\u04e4\u04e5\3\2\2\2\u04e5\u04e7\3\2\2\2\u04e6\u04e2\3\2"+
		"\2\2\u04e6\u04e7\3\2\2\2\u04e7\u04e8\3\2\2\2\u04e8\u04e9\7\5\2\2\u04e9"+
		"\u04ea\t\30\2\2\u04ea\u04eb\5 \21\2\u04eb\u054f\3\2\2\2\u04ec\u04ed\7"+
		"\4\2\2\u04ed\u04f4\5\36\20\2\u04ee\u04ef\7\u00b1\2\2\u04ef\u04f1\5\36"+
		"\20\2\u04f0\u04ee\3\2\2\2\u04f1\u04f2\3\2\2\2\u04f2\u04f0\3\2\2\2\u04f2"+
		"\u04f3\3\2\2\2\u04f3\u04f5\3\2\2\2\u04f4\u04f0\3\2\2\2\u04f4\u04f5\3\2"+
		"\2\2\u04f5\u04f6\3\2\2\2\u04f6\u04f7\7\5\2\2\u04f7\u04f8\7\u00bb\2\2\u04f8"+
		"\u04fa\5\62\32\2\u04f9\u04fb\7f\2\2\u04fa\u04f9\3\2\2\2\u04fa\u04fb\3"+
		"\2\2\2\u04fb\u04fc\3\2\2\2\u04fc\u04fd\t\30\2\2\u04fd\u04fe\5 \21\2\u04fe"+
		"\u054f\3\2\2\2\u04ff\u0500\7]\2\2\u0500\u0501\7\4\2\2\u0501\u0508\5$\23"+
		"\2\u0502\u0503\7\u00b1\2\2\u0503\u0505\5$\23\2\u0504\u0502\3\2\2\2\u0505"+
		"\u0506\3\2\2\2\u0506\u0504\3\2\2\2\u0506\u0507\3\2\2\2\u0507\u0509\3\2"+
		"\2\2\u0508\u0504\3\2\2\2\u0508\u0509\3\2\2\2\u0509\u050a\3\2\2\2\u050a"+
		"\u050b\7\5\2\2\u050b\u050c\7\u00bb\2\2\u050c\u050e\5\62\32\2\u050d\u050f"+
		"\7f\2\2\u050e\u050d\3\2\2\2\u050e\u050f\3\2\2\2\u050f\u0510\3\2\2\2\u0510"+
		"\u0511\t\30\2\2\u0511\u0512\5 \21\2\u0512\u054f\3\2\2\2\u0513\u0514\7"+
		"\4\2\2\u0514\u051b\5\"\22\2\u0515\u0516\7\u00b1\2\2\u0516\u0518\5\"\22"+
		"\2\u0517\u0515\3\2\2\2\u0518\u0519\3\2\2\2\u0519\u0517\3\2\2\2\u0519\u051a"+
		"\3\2\2\2\u051a\u051c\3\2\2\2\u051b\u0517\3\2\2\2\u051b\u051c\3\2\2\2\u051c"+
		"\u051d\3\2\2\2\u051d\u051e\7\5\2\2\u051e\u051f\7\u00b1\2\2\u051f\u0520"+
		"\7]\2\2\u0520\u0521\7\4\2\2\u0521\u0528\5$\23\2\u0522\u0523\7\u00b1\2"+
		"\2\u0523\u0525\5$\23\2\u0524\u0522\3\2\2\2\u0525\u0526\3\2\2\2\u0526\u0524"+
		"\3\2\2\2\u0526\u0527\3\2\2\2\u0527\u0529\3\2\2\2\u0528\u0524\3\2\2\2\u0528"+
		"\u0529\3\2\2\2\u0529\u052a\3\2\2\2\u052a\u052b\7\5\2\2\u052b\u052c\t\30"+
		"\2\2\u052c\u052d\5 \21\2\u052d\u054f\3\2\2\2\u052e\u052f\7\4\2\2\u052f"+
		"\u0536\5\"\22\2\u0530\u0531\7\u00b1\2\2\u0531\u0533\5\"\22\2\u0532\u0530"+
		"\3\2\2\2\u0533\u0534\3\2\2\2\u0534\u0532\3\2\2\2\u0534\u0535\3\2\2\2\u0535"+
		"\u0537\3\2\2\2\u0536\u0532\3\2\2\2\u0536\u0537\3\2\2\2\u0537\u0538\3\2"+
		"\2\2\u0538\u0539\7\5\2\2\u0539\u053a\7\u00b1\2\2\u053a\u053b\7]\2\2\u053b"+
		"\u053c\7\4\2\2\u053c\u0543\5$\23\2\u053d\u053e\7\u00b1\2\2\u053e\u0540"+
		"\5$\23\2\u053f\u053d\3\2\2\2\u0540\u0541\3\2\2\2\u0541\u053f\3\2\2\2\u0541"+
		"\u0542\3\2\2\2\u0542\u0544\3\2\2\2\u0543\u053f\3\2\2\2\u0543\u0544\3\2"+
		"\2\2\u0544\u0545\3\2\2\2\u0545\u0546\7\5\2\2\u0546\u0547\7\u00bb\2\2\u0547"+
		"\u0549\5\62\32\2\u0548\u054a\7f\2\2\u0549\u0548\3\2\2\2\u0549\u054a\3"+
		"\2\2\2\u054a\u054b\3\2\2\2\u054b\u054c\t\30\2\2\u054c\u054d\5 \21\2\u054d"+
		"\u054f\3\2\2\2\u054e\u04cf\3\2\2\2\u054e\u04dd\3\2\2\2\u054e\u04ec\3\2"+
		"\2\2\u054e\u04ff\3\2\2\2\u054e\u0513\3\2\2\2\u054e\u052e\3\2\2\2\u054f"+
		"\'\3\2\2\2\u0550\u0551\t\31\2\2\u0551\u0554\7\4\2\2\u0552\u0555\5\26\f"+
		"\2\u0553\u0555\5\30\r\2\u0554\u0552\3\2\2\2\u0554\u0553\3\2\2\2\u0555"+
		"\u0557\3\2\2\2\u0556\u0558\7f\2\2\u0557\u0556\3\2\2\2\u0557\u0558\3\2"+
		"\2\2\u0558\u0559\3\2\2\2\u0559\u055c\7\13\2\2\u055a\u055d\5\26\f\2\u055b"+
		"\u055d\5\30\r\2\u055c\u055a\3\2\2\2\u055c\u055b\3\2\2\2\u055d\u055f\3"+
		"\2\2\2\u055e\u0560\7f\2\2\u055f\u055e\3\2\2\2\u055f\u0560\3\2\2\2\u0560"+
		"\u0561\3\2\2\2\u0561\u0567\7\5\2\2\u0562\u0564\5*\26\2\u0563\u0562\3\2"+
		"\2\2\u0564\u0565\3\2\2\2\u0565\u0563\3\2\2\2\u0565\u0566\3\2\2\2\u0566"+
		"\u0568\3\2\2\2\u0567\u0563\3\2\2\2\u0567\u0568\3\2\2\2\u0568\u0581\3\2"+
		"\2\2\u0569\u056a\t\31\2\2\u056a\u056d\7\4\2\2\u056b\u056e\5\26\f\2\u056c"+
		"\u056e\5\30\r\2\u056d\u056b\3\2\2\2\u056d\u056c\3\2\2\2\u056e\u0570\3"+
		"\2\2\2\u056f\u0571\7f\2\2\u0570\u056f\3\2\2\2\u0570\u0571\3\2\2\2\u0571"+
		"\u0572\3\2\2\2\u0572\u0575\7\13\2\2\u0573\u0576\5\26\f\2\u0574\u0576\5"+
		"\30\r\2\u0575\u0573\3\2\2\2\u0575\u0574\3\2\2\2\u0576\u0578\3\2\2\2\u0577"+
		"\u0579\7f\2\2\u0578\u0577\3\2\2\2\u0578\u0579\3\2\2\2\u0579\u057a\3\2"+
		"\2\2\u057a\u057c\7\5\2\2\u057b\u057d\5*\26\2\u057c\u057b\3\2\2\2\u057d"+
		"\u057e\3\2\2\2\u057e\u057c\3\2\2\2\u057e\u057f\3\2\2\2\u057f\u0581\3\2"+
		"\2\2\u0580\u0550\3\2\2\2\u0580\u0569\3\2\2\2\u0581)\3\2\2\2\u0582\u0584"+
		"\t\32\2\2\u0583\u0582\3\2\2\2\u0583\u0584\3\2\2\2\u0584\u0586\3\2\2\2"+
		"\u0585\u0587\7f\2\2\u0586\u0585\3\2\2\2\u0586\u0587\3\2\2\2\u0587\u0589"+
		"\3\2\2\2\u0588\u058a\7\u0092\2\2\u0589\u0588\3\2\2\2\u0589\u058a\3\2\2"+
		"\2\u058a\u058c\3\2\2\2\u058b\u058d\7\u0093\2\2\u058c\u058b\3\2\2\2\u058c"+
		"\u058d\3\2\2\2\u058d\u058e\3\2\2\2\u058e\u058f\7\4\2\2\u058f\u0591\5,"+
		"\27\2\u0590\u0592\5\62\32\2\u0591\u0590\3\2\2\2\u0591\u0592\3\2\2\2\u0592"+
		"\u0593\3\2\2\2\u0593\u0594\7\13\2\2\u0594\u0596\5.\30\2\u0595\u0597\5"+
		"\62\32\2\u0596\u0595\3\2\2\2\u0596\u0597\3\2\2\2\u0597\u0598\3\2\2\2\u0598"+
		"\u0599\7\5\2\2\u0599\u05e3\3\2\2\2\u059a\u059c\t\32\2\2\u059b\u059a\3"+
		"\2\2\2\u059b\u059c\3\2\2\2\u059c\u059e\3\2\2\2\u059d\u059f\7\u0092\2\2"+
		"\u059e\u059d\3\2\2\2\u059e\u059f\3\2\2\2\u059f\u05a1\3\2\2\2\u05a0\u05a2"+
		"\7f\2\2\u05a1\u05a0\3\2\2\2\u05a1\u05a2\3\2\2\2\u05a2\u05a4\3\2\2\2\u05a3"+
		"\u05a5\7\u0093\2\2\u05a4\u05a3\3\2\2\2\u05a4\u05a5\3\2\2\2\u05a5\u05a6"+
		"\3\2\2\2\u05a6\u05a7\7\4\2\2\u05a7\u05a9\5,\27\2\u05a8\u05aa\5\62\32\2"+
		"\u05a9\u05a8\3\2\2\2\u05a9\u05aa\3\2\2\2\u05aa\u05ab\3\2\2\2\u05ab\u05ac"+
		"\7\13\2\2\u05ac\u05ae\5.\30\2\u05ad\u05af\5\62\32\2\u05ae\u05ad\3\2\2"+
		"\2\u05ae\u05af\3\2\2\2\u05af\u05b0\3\2\2\2\u05b0\u05b1\7\5\2\2\u05b1\u05e3"+
		"\3\2\2\2\u05b2\u05b4\t\32\2\2\u05b3\u05b2\3\2\2\2\u05b3\u05b4\3\2\2\2"+
		"\u05b4\u05b6\3\2\2\2\u05b5\u05b7\7\u0092\2\2\u05b6\u05b5\3\2\2\2\u05b6"+
		"\u05b7\3\2\2\2\u05b7\u05b9\3\2\2\2\u05b8\u05ba\7\u0093\2\2\u05b9\u05b8"+
		"\3\2\2\2\u05b9\u05ba\3\2\2\2\u05ba\u05bc\3\2\2\2\u05bb\u05bd\7f\2\2\u05bc"+
		"\u05bb\3\2\2\2\u05bc\u05bd\3\2\2\2\u05bd\u05be\3\2\2\2\u05be\u05bf\7\4"+
		"\2\2\u05bf\u05c1\5,\27\2\u05c0\u05c2\5\62\32\2\u05c1\u05c0\3\2\2\2\u05c1"+
		"\u05c2\3\2\2\2\u05c2\u05c3\3\2\2\2\u05c3\u05c4\7\13\2\2\u05c4\u05c6\5"+
		".\30\2\u05c5\u05c7\5\62\32\2\u05c6\u05c5\3\2\2\2\u05c6\u05c7\3\2\2\2\u05c7"+
		"\u05c8\3\2\2\2\u05c8\u05c9\7\5\2\2\u05c9\u05e3\3\2\2\2\u05ca\u05cc\t\32"+
		"\2\2\u05cb\u05ca\3\2\2\2\u05cb\u05cc\3\2\2\2\u05cc\u05ce\3\2\2\2\u05cd"+
		"\u05cf\7f\2\2\u05ce\u05cd\3\2\2\2\u05ce\u05cf\3\2\2\2\u05cf\u05d1\3\2"+
		"\2\2\u05d0\u05d2\7\u0092\2\2\u05d1\u05d0\3\2\2\2\u05d1\u05d2\3\2\2\2\u05d2"+
		"\u05d4\3\2\2\2\u05d3\u05d5\7\u0093\2\2\u05d4\u05d3\3\2\2\2\u05d4\u05d5"+
		"\3\2\2\2\u05d5\u05d6\3\2\2\2\u05d6\u05d7\7\4\2\2\u05d7\u05d9\5,\27\2\u05d8"+
		"\u05da\5\62\32\2\u05d9\u05d8\3\2\2\2\u05d9\u05da\3\2\2\2\u05da\u05db\3"+
		"\2\2\2\u05db\u05dc\7\13\2\2\u05dc\u05de\5.\30\2\u05dd\u05df\5\62\32\2"+
		"\u05de\u05dd\3\2\2\2\u05de\u05df\3\2\2\2\u05df\u05e0\3\2\2\2\u05e0\u05e1"+
		"\7\5\2\2\u05e1\u05e3\3\2\2\2\u05e2\u0583\3\2\2\2\u05e2\u059b\3\2\2\2\u05e2"+
		"\u05b3\3\2\2\2\u05e2\u05cb\3\2\2\2\u05e3+\3\2\2\2\u05e4\u05fb\7X\2\2\u05e5"+
		"\u05fb\7Y\2\2\u05e6\u05fb\7\u00bb\2\2\u05e7\u05fb\7\u00bc\2\2\u05e8\u05e9"+
		"\7\17\2\2\u05e9\u05ea\7;\2\2\u05ea\u05fb\7\u00bb\2\2\u05eb\u05fb\7\17"+
		"\2\2\u05ec\u05ed\7\17\2\2\u05ed\u05fb\t\33\2\2\u05ee\u05ef\7\16\2\2\u05ef"+
		"\u05f0\7;\2\2\u05f0\u05fb\7\u00bb\2\2\u05f1\u05fb\7\16\2\2\u05f2\u05f3"+
		"\7\16\2\2\u05f3\u05fb\t\33\2\2\u05f4\u05f5\7\17\2\2\u05f5\u05f6\7\u0091"+
		"\2\2\u05f6\u05fb\7\u00bb\2\2\u05f7\u05f8\7\16\2\2\u05f8\u05f9\7\u0091"+
		"\2\2\u05f9\u05fb\7\u00bb\2\2\u05fa\u05e4\3\2\2\2\u05fa\u05e5\3\2\2\2\u05fa"+
		"\u05e6\3\2\2\2\u05fa\u05e7\3\2\2\2\u05fa\u05e8\3\2\2\2\u05fa\u05eb\3\2"+
		"\2\2\u05fa\u05ec\3\2\2\2\u05fa\u05ee\3\2\2\2\u05fa\u05f1\3\2\2\2\u05fa"+
		"\u05f2\3\2\2\2\u05fa\u05f4\3\2\2\2\u05fa\u05f7\3\2\2\2\u05fb-\3\2\2\2"+
		"\u05fc\u0613\7Y\2\2\u05fd\u0613\7\u00bb\2\2\u05fe\u0613\7X\2\2\u05ff\u0613"+
		"\7\u00bc\2\2\u0600\u0601\7\17\2\2\u0601\u0602\7;\2\2\u0602\u0613\7\u00bb"+
		"\2\2\u0603\u0613\7\17\2\2\u0604\u0605\7\17\2\2\u0605\u0613\t\33\2\2\u0606"+
		"\u0607\7\16\2\2\u0607\u0608\7;\2\2\u0608\u0613\7\u00bb\2\2\u0609\u0613"+
		"\7\16\2\2\u060a\u060b\7\16\2\2\u060b\u0613\t\33\2\2\u060c\u060d\7\17\2"+
		"\2\u060d\u060e\7\u0091\2\2\u060e\u0613\7\u00bb\2\2\u060f\u0610\7\16\2"+
		"\2\u0610\u0611\7\u0091\2\2\u0611\u0613\7\u00bb\2\2\u0612\u05fc\3\2\2\2"+
		"\u0612\u05fd\3\2\2\2\u0612\u05fe\3\2\2\2\u0612\u05ff\3\2\2\2\u0612\u0600"+
		"\3\2\2\2\u0612\u0603\3\2\2\2\u0612\u0604\3\2\2\2\u0612\u0606\3\2\2\2\u0612"+
		"\u0609\3\2\2\2\u0612\u060a\3\2\2\2\u0612\u060c\3\2\2\2\u0612\u060f\3\2"+
		"\2\2\u0613/\3\2\2\2\u0614\u0616\7\u00bb\2\2\u0615\u0617\t\34\2\2\u0616"+
		"\u0615\3\2\2\2\u0616\u0617\3\2\2\2\u0617\u061a\3\2\2\2\u0618\u061a\t\35"+
		"\2\2\u0619\u0614\3\2\2\2\u0619\u0618\3\2\2\2\u061a\61\3\2\2\2\u061b\u061c"+
		"\t\36\2\2\u061c\63\3\2\2\2\u061d\u061e\7\u00ac\2\2\u061e\u061f\7\f\2\2"+
		"\u061f\u0627\7\u00bf\2\2\u0620\u0621\7\u00ad\2\2\u0621\u0622\7\f\2\2\u0622"+
		"\u0627\7\u00bf\2\2\u0623\u0624\7\u00ae\2\2\u0624\u0625\7\f\2\2\u0625\u0627"+
		"\7\u00bf\2\2\u0626\u061d\3\2\2\2\u0626\u0620\3\2\2\2\u0626\u0623\3\2\2"+
		"\2\u0627\65\3\2\2\2\u0628\u0631\5\64\33\2\u0629\u062c\5\64\33\2\u062a"+
		"\u062b\7\13\2\2\u062b\u062d\5\64\33\2\u062c\u062a\3\2\2\2\u062d\u062e"+
		"\3\2\2\2\u062e\u062c\3\2\2\2\u062e\u062f\3\2\2\2\u062f\u0631\3\2\2\2\u0630"+
		"\u0628\3\2\2\2\u0630\u0629\3\2\2\2\u0631\67\3\2\2\2\u0632\u063b\5\30\r"+
		"\2\u0633\u0636\5\30\r\2\u0634\u0635\7\13\2\2\u0635\u0637\5\30\r\2\u0636"+
		"\u0634\3\2\2\2\u0637\u0638\3\2\2\2\u0638\u0636\3\2\2\2\u0638\u0639\3\2"+
		"\2\2\u0639\u063b\3\2\2\2\u063a\u0632\3\2\2\2\u063a\u0633\3\2\2\2\u063b"+
		"9\3\2\2\2\u00cd=?EGLWchty~\u008a\u008f\u0094\u00a0\u00a5\u00aa\u00b6\u00bd"+
		"\u00c2\u00c7\u00cb\u00d4\u00e5\u00eb\u00ef\u00f4\u00fa\u0101\u010d\u0113"+
		"\u0118\u011e\u0125\u012a\u0134\u013c\u0140\u014c\u0157\u0163\u016d\u0177"+
		"\u017b\u0184\u018b\u019c\u01a6\u01ab\u01b0\u01b5\u01c0\u01cb\u01d7\u01e3"+
		"\u01e9\u01ee\u01f4\u01f9\u0205\u0211\u0217\u021c\u0228\u0235\u023d\u0245"+
		"\u0263\u0268\u026f\u0274\u027b\u0280\u0285\u028c\u0291\u0296\u029d\u02a2"+
		"\u02a7\u02af\u02b4\u02b9\u02c1\u02c8\u02cd\u02d5\u02da\u02e1\u02e6\u02f4"+
		"\u02f9\u0300\u0305\u030d\u0312\u0319\u031e\u0326\u0332\u0337\u0352\u0357"+
		"\u035f\u0362\u0367\u036a\u0372\u0377\u037f\u0384\u038c\u0391\u0399\u039e"+
		"\u03c3\u03cb\u03d3\u040b\u042f\u0434\u0446\u0453\u0458\u0481\u0486\u0495"+
		"\u049d\u04a7\u04b4\u04b8\u04bb\u04bf\u04c2\u04c6\u04c9\u04cd\u04d5\u04d7"+
		"\u04e4\u04e6\u04f2\u04f4\u04fa\u0506\u0508\u050e\u0519\u051b\u0526\u0528"+
		"\u0534\u0536\u0541\u0543\u0549\u054e\u0554\u0557\u055c\u055f\u0565\u0567"+
		"\u056d\u0570\u0575\u0578\u057e\u0580\u0583\u0586\u0589\u058c\u0591\u0596"+
		"\u059b\u059e\u05a1\u05a4\u05a9\u05ae\u05b3\u05b6\u05b9\u05bc\u05c1\u05c6"+
		"\u05cb\u05ce\u05d1\u05d4\u05d9\u05de\u05e2\u05fa\u0612\u0616\u0619\u0626"+
		"\u062e\u0630\u0638\u063a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}