grammar Expr;

program: (each+)? output_commands
       | (each+)? main_command
       | output_commands
       | main_command
       ;

output_commands: 'OUTPUT' '(' (main_command) ')' #outputCommand
	   		   | ('EVENT' 'FLOW' | 'EVENT_FLOW' | 'EVENTFLOW') '(' (main_command) ')' #eventFlowCommand
	   		   | ('CSV' '(' cohort=main_command ( ((','((TERM '=')? main_command))+)) ')' 
	   		   | 'CSV' '(' cohort=main_command ( ((','(('TIME' '=') time_cohort=main_command)))?) ( ((','((TERM '=')? main_command))+)) ')'
	   		   | 'CSV' '(' cohort=main_command ( ((','(('START' '=') start_cohort=main_command)))?) ( ((','((TERM '=')? main_command))+)) ')'
	   		   | 'CSV' '(' cohort=main_command ( ((','(('END' '=') end_cohort=main_command)))?) ( ((','((TERM '=')? main_command))+)) ')'
	   		   | 'CSV' '(' cohort=main_command ( ((','(('START' '=') start_cohort=main_command)))?)  ( ((','(('END' '=') end_cohort=main_command)))?) ( ((','((TERM '=')? main_command))+)) ')') # csvCommand
	   		   | 'DUMP' '(' cohort=main_command ',' TERM ')' #dumpCommand
	   		   ;

each: 'FOR' 'EACH' '(' main_command ')' 'AS' '(' each_name ')' each_content #eachCommand
    ;

each_content: '{' (each_line)+ '}'
            | '{' '}'; 

main_command: top_level
            | each_name
            ;

each_name: ('A'|'B'|'C'|'D'|'E'|'F'|'G'|'H'|'I'|'J'|'K'|'L'|'M'|'N'|'O'|'P'|'Q'|'R'|'S'|'T'|'U'|'V'|'W'|'X'|'Y'|'Z'|'_')+;

each_line: each_name '=' (command | atom) ';' #eachNodeAssign
         | 'RETURN' (command | atom) 'AS' each_name ';' #eachNodeSave
         | 'CLEAR' each_name ';' #clearVariable
         | 'IF' not_label='!'? 'EMPTY' '(' (command | atom) ')' each_content ';'? #eachLineIf
         | 'IF' '(' (command | atom) ')' label=('==' | '!=') '(' (command | atom) ')' each_content ';'? #eachLineEquals
         | 'EXIT' ';' #eachNodeExit
         | 'PRINT' '(' (each_name | command | atom | print_atom) ('+' (each_name command | atom | print_atom))+  ')' ';' #print
         | 'PRINT' '(' (each_name command | atom | print_atom) ')' ';' #printSingle
         | 'CONTINUE' ';' #eachNodeContinue
         | 'FAIL' 'PATIENT' ';' #eachNodeFailPatient
         | each #unusedEach
         ;

print_atom: TERM #parseTerm;

top_level: command #unused1 
       | atom #unused2
       | 'LIMIT' '(' limit_type ',' (command | atom) ')' #limitCommand
       | ('ESTIMATE' | 'EST') '(' limit_type ',' (command | atom) ')' #estimateCommand
       | ('EVALUATE' | 'EVAL') '(' limit_type ',' (command | atom) ')' #evaluateCommand
       ;

commands: command #unusedCommand
        | command ',' command #unusedMultipleCommand
        | commands ',' commands #unusedMultipleCommands
        | atoms #unusedAtoms
        ;

command: AND '(' commands ')' #andCommand
       | OR '(' commands ')'  #orCommand
       | NOT '(' (command | atom)')'  #notCommand
       | before #unusedBefore
       | before ('AND' before)+ #complexBefore
       | 'RETURN' (command | atom) 'INTERSECTING' (command | atom) #findCommand
       | 'RETURN' (command | atom) 'INTERSECTING' 'ALL' '(' commands ')' #intersectingAll
       | 'RETURN' (command | atom) 'INTERSECTING' 'ANY' '(' commands ')' #intersectingAny
       | 'RETURN' (command | atom) 'NOT' 'INTERSECTING' 'ANY' '(' commands ')' #nonIntersectManyCommand
       | 'RETURN' (command | atom) 'NOT' 'INTERSECTING' 'ALL' '(' commands ')' #nonIntersectAllCommand
       | 'RETURN' (command | atom) 'NOT' 'INTERSECTING' (command | atom) #nonIntersectCommand
       | 'RETURN' (command | atom) 'NEVER' 'INTERSECTING' (command | atom) #neverIntersectCommand
       | 'RETURN' (command | atom) 'NEVER' 'INTERSECTING' 'ANY' '(' commands ')' #neverIntersectManyCommand
       | 'RETURN' (command | atom) 'NEVER' 'INTERSECTING' 'ALL' '(' commands ')' #neverIntersectAllCommand
       | 'RETURN' (command | atom) 'ALWAYS' 'INTERSECTING' (command | atom) #alwaysIntersect
       | 'RETURN' (command | atom) 'ALWAYS' 'INTERSECTING' 'ANY' '(' commands ')' #alwaysIntersectAny
       | 'RETURN' (command | atom) 'ALWAYS' 'INTERSECTING' 'ALL' '(' commands ')' #alwaysIntersectAll
       | INVERT '(' (command | atom)')'  #invertCommand
       | ('START' | 'START OF' | 'START_OF') '(' (command | atom)')' #startCommand
       | ('END' | 'END OF' | 'END_OF') '(' (command | atom)')' #endCommand       
       | INTERVAL '(' commands')' #intervalCommand
       | INTERVAL '(' commands ',' 'PAIRS' ')' #intervalPairwiseCommand
       | INTERSECT '(' commands ')' #intersectCommand
       | UNION '(' commands ')' #unionCommand
       | before_command #complexBeforeCommand
       | ('FIRST' 'MENTION' | 'FIRST_MENTION') '(' (command | atom) ')' #firstMentionCommand
       | ('LAST' 'MENTION' | 'LAST_MENTION') '(' (command | atom) ')' #lastMentionCommand
       | ('FIRST' 'MENTION' | 'FIRST_MENTION') '(' (command | atom) ',' (command | atom) ')' #firstMentionContextCommand
       | ('LAST' 'MENTION' | 'LAST_MENTION') '(' (command | atom) ',' (command | atom) ')' #lastMentionContextCommand
       | ('LAST' 'MENTION' | 'LAST_MENTION') '(' (command | atom) ',' (command | atom) ')' #lastMentionContextCommand
       | ('EXTEND' | 'RESIZE' | 'EXTEND BY') '(' (command | atom) ',' start_time start_time_modifier=time_modifier? ',' end_time end_time_modifier=time_modifier? ')' #extendByCommand
       | 'DURATION' '(' (command | atom) ',' type=('SINGLE' | 'ALL') ',' startTime=('MIN' | 'MAX' | POSITIVE_RANGE) start_time_modifier=time_modifier? ',' endTime=('MAX' | POSITIVE_RANGE) end_time_modifier=time_modifier?')' #durationSimpleCommand
       | 'DURATION' '(' (command | atom) ',' (command | atom) ',' type=('SINGLE' | 'ALL')  ',' startTime=('MAX' | 'MIN' | POSITIVE_RANGE) start_time_modifier=time_modifier? ',' endTime=('MIN' | 'MAX' | POSITIVE_RANGE) end_time_modifier=time_modifier? ')' #durationComplexCommand
       | 'DURATION' '(' before_atom_left ',' before_atom_right ',' type=('SINGLE' | 'ALL')  ',' startTime=('MAX' | 'MIN' | POSITIVE_RANGE) start_time_modifier=time_modifier? ',' endTime=('MIN' | 'MAX' | POSITIVE_RANGE) end_time_modifier=time_modifier? ')' #durationComplex2Command
       | ('HISTORY' 'OF' | 'HISTORY_OF') '(' (command | atom) ')' #historyOfCommand
       | ('NO' 'HISTORY' 'OF' | 'NO_HISTORY_OF') '(' (command | atom) ')' #noHistoryOfCommand
       | ('NEVER' 'HAD' | 'NEVER_HAD') '(' (command | atom) ')' #neverHadCommand
       | 'COUNT' '(' (command | atom) ',' startCount=('MAX' | 'MIN'| POSITIVE_RANGE) ',' endCount=('MIN' | 'MAX' | POSITIVE_RANGE) ')' #countSimpleCommand
       | 'COUNT' '(' (command | atom) ',' (command | atom) ',' type=('SINGLE' | 'ALL')  ',' startCount=('MIN' | 'MAX' | POSITIVE_RANGE) ',' endCount=('MIN' | 'MAX' | POSITIVE_RANGE) ')' #countComplexCommand
       | 'COUNT' '(' before_atom_left ',' before_atom_right ',' type=('SINGLE' | 'ALL')  ',' startCount=('MIN' | 'MAX' | POSITIVE_RANGE) ',' endCount=('MIN' | 'MAX' | POSITIVE_RANGE) ')' #countComplex2Command
       | ('EQUAL' | 'EQUALS') '(' (command | atom) ',' (command | atom) ')' #equalCommand
       | ('CONTAINS' | 'CONTAIN') '(' (command | atom) start_type='*'? ',' (command | atom) end_type='*'? ')' #containsCommand
       | 'IDENTICAL' '(' (command | atom) ',' (command | atom) ')' #identicalCommand
       | 'SAME' '(' (command | atom) ',' (command | atom) ')' #sameCommand
       | 'DIFF' '(' (command | atom) ',' (command | atom) ')' #diffCommand
       | 'MERGE' '(' (command | atom) ',' (command | atom) ')' #mergeCommand
       | 'PRIMARY' '(' icd9_atom ')' #primaryCommand
       | 'ORIGINAL' '(' icd9_atom ')' #originalCommand
       | 'ORIGINAL' '(' 'PRIMARY' '(' icd9_atom ')' ')' #originalPrimaryCommand
       | 'PRIMARY' '(' 'ORIGINAL' '(' icd9_atom ')' ')' #originalPrimaryCommand       
       | ('DEPARTMENT' | 'DEPT') '=' TERM #departmentCommand
       | ('NOTE_TYPE' | 'NOTETYPE' | ('NOTE' 'TYPE')) '=' TERM  #noteTypeCommand
       | ('VISIT_TYPE' | 'VISITTYPE' | ('VISIT' 'TYPE')) '=' TERM #visitTypeCommand
       | ('VISIT_TYPE' | 'VISITTYPE' | ('VISIT' 'TYPE')) #anyVisitTypeCommand
       | ('DRUG' | 'DRUGS' | 'MED' | 'MEDS') '(' HASRXNORM '=' (POSITIVE_RANGE)  ')' #drugsSimple
       | ('DRUG' | 'DRUGS' | 'MED' | 'MEDS') '(' HASRXNORM '=' (POSITIVE_RANGE)  ',' 'ROUTE' '=' TERM ')' #drugsRoute
       | ('DRUG' | 'DRUGS' | 'MED' | 'MEDS') '(' HASRXNORM '=' (POSITIVE_RANGE)  ',' 'STATUS' '=' TERM ')' #drugsStatus
       | ('DRUG' | 'DRUGS' | 'MED' | 'MEDS') '(' HASRXNORM '=' (POSITIVE_RANGE)  ',' 'ROUTE' '=' TERM ',' 'STATUS' '=' TERM ')' #drugsRouteStatus
       | ('DRUG' | 'DRUGS' | 'MED' | 'MEDS') '(' HASRXNORM '=' (POSITIVE_RANGE)  ',' 'STATUS' '=' TERM ',' 'ROUTE' '=' TERM  ')' #drugsStatusRoute
       ;

atom: HASCPT '=' (CPT | POSITIVE_RANGE) #hasCptCommand
	| 'CPT' #hasAnyCptCommand 
    | HASRXNORM '=' (POSITIVE_RANGE) #hasRxNormCommand
    | HASRXNORM #hasAnyRxNormCommand
    | 'GENDER' '=' TERM #genderCommand
    | ('YEAR' | 'YEARS') '(' start=(POSITIVE_RANGE | 'MIN') ',' end=(POSITIVE_RANGE | 'MAX') ')' #yearCommand
    | 'RACE' '=' TERM #raceCommand
    | 'ETHNICITY' '=' TERM #ethnicityCommand
    | DEATH #deathCommand
    | 'SNOMED' #anySnomedCommand
    | 'SNOMED' '=' POSITIVE_RANGE #snomedCommand
    | ENCOUNTERS #encountersCommand
    | ('NOTE' | 'NOTES') #noteTimeInstancesCommand
    | TIMELINE #timelineCommand
    | ('RECORD_START' | 'RECORD' 'START') #recordStartCommand
    | ('RECORD_END' | 'RECORD' 'END') #recordEndCommand
    | ('NOTE' | 'NOTES') '(' note_atoms ')' #noteCommand
    | ('NOTE' | 'NOTES') '(' commands ')' #noteBooleanCommand
    | ('NOTE' | 'NOTES') '(' ('NOTE_TYPE' | 'NOTETYPE' | ('NOTE' 'TYPE')) '=' TERM ',' note_atoms ')' #noteWithTypeCommand
    | note_atom #noteAtomCommand
    | 'AGE' '(' minAge=('MAX' | 'MIN' | POSITIVE_RANGE) minAgeModifier=time_modifier? ',' maxAge=('MAX' | 'MIN' | POSITIVE_RANGE) maxAgeModifier=time_modifier? ')' #ageCommand
    | 'VITALS' '(' TERM ',' double_value ',' double_value ')' #vitalsCommand
    | ('LABS' | 'LAB') '(' TERM ',' double_value ',' double_value ')' #labValuesCommand
    | 'VITALS' '(' TERM ')' #simpleVitals
    | ('LABS' | 'LAB') #anyLabsCommand
    | 'VITALS' #anyVitalsCommand
    | ('LABS' | 'LAB') '(' TERM ',' labType=TERM ')' #complexLabs
    | ('LABS' | 'LAB') '(' TERM ')' #simpleLabs
    | 'INTERVAL' '(' startTimeValue=('MAX' | 'MIN' | POSITIVE_RANGE) startTimeModifier=time_modifier? ',' endTimeValue=('MAX' | 'MIN' | POSITIVE_RANGE) endTimeModifier=time_modifier? ')' #numericInterval
    | ('NULL' | 'EMPTY') #nullCommand
    | 'ATC' '=' TERM  #atcCommand
    | icd9_atom #icd9AtomCommand
    | ('PATIENTS' | 'PATIENT') '(' POSITIVE_RANGE (',' POSITIVE_RANGE)+? ')' #patientsCommand  
    | ('PATIENTS' | 'PATIENT') '(' POSITIVE_RANGE ')' #patientsSingleCommand
    | each_name #eachNameCommand
    ;  

icd9_atom: HASICD9 '=' (ICD9 | POSITIVE_RANGE | ICD10) #hasIcd9Command
         | 'ICD9' #hasAnyIcd9Codes
         | HASICD10 '=' ICD10 #hasIcd10Command
         | 'ICD10' #hasAnyIcd10Codes
         ;

double_value: POSITIVE_RANGE
            | NEGATIVE_RANGE
            | (POSITIVE_RANGE '.' POSITIVE_RANGE)
            | (NEGATIVE_RANGE '.' POSITIVE_RANGE)
            | ICD9
            | 'MAX'
            | 'MIN'
            ;

before_atom_left: (command | atom)'*'? #atomWithAsterisk1;
before_atom_right: (command | atom)'*'? #atomWithAsterisk2;
before_atom_left_left: (command | atom)'*'? #atomWithAsterisk3;
before_atom_left_no_asterisk: (command | atom) #atomWithAsterisk4;

before: '(' before_atom_left (('AND' before_atom_left)+)? ')' command_type=('BEFORE' | 'AFTER') before_atom_right #beforePositive
      | 'NO' '(' before_atom_left_no_asterisk (('AND' before_atom_left_no_asterisk)+)? ')' command_type=('BEFORE' | 'AFTER') before_atom_right #beforeNegative
      | '(' before_atom_left (('AND' before_atom_left)+)? ')' (POSITIVE_RANGE time_modifier asterisk='*'?) command_type=('BEFORE' | 'AFTER') before_atom_right #beforePositiveTime
      | 'NO' '(' before_atom_left_no_asterisk (('AND' before_atom_left_no_asterisk)+)? ')' (POSITIVE_RANGE time_modifier asterisk='*'?) command_type=('BEFORE' | 'AFTER') before_atom_right #beforeNegativeTime
      | '(' before_atom_left_left (('AND' before_atom_left_left)+)? ')' 'AND' 'NO' '(' before_atom_left_no_asterisk (('AND' before_atom_left_no_asterisk)+)? ')' command_type=('BEFORE' | 'AFTER') before_atom_right #beforeBoth
      | '(' before_atom_left_left (('AND' before_atom_left_left)+)? ')' 'AND' 'NO' '(' before_atom_left_no_asterisk (('AND' before_atom_left_no_asterisk)+)? ')' (POSITIVE_RANGE time_modifier asterisk='*'?) command_type=('BEFORE' | 'AFTER') before_atom_right #beforeBothTime
      ;

before_command: ('BEFORE' | 'SEQUENCE') '(' (command | atom) start_type='*'? ',' (command | atom) end_type='*'? ')' (before_modifier+)? #beforeCommandAtom
              | ('BEFORE' | 'SEQUENCE') '(' (command | atom) start_type='*'? ',' (command | atom) end_type='*'? ')' (before_modifier+) #beforeCommandAtom2
              ;

before_modifier: ((in_type=('-' | '+')? return_type='*'? start_type='<'? end_type='>'? '(' start_time startModifier=time_modifier? ',' end_time endModifier=time_modifier? ')')
               | (in_type=('-' | '+')? start_type='<'? return_type='*'? end_type='>'? '(' start_time startModifier=time_modifier? ',' end_time endModifier=time_modifier? ')') 
               | (in_type=('-' | '+')? start_type='<'?  end_type='>'? return_type='*'? '(' start_time startModifier=time_modifier? ',' end_time endModifier=time_modifier? ')')
               | (in_type=('-' | '+')? return_type='*'? start_type='<'? end_type='>'? '(' start_time startModifier=time_modifier? ',' end_time endModifier=time_modifier? ')')
               ) #beforeModifier;

start_time: ('MIN' | 'MAX' | POSITIVE_RANGE | NEGATIVE_RANGE | 'END' '+' POSITIVE_RANGE | 'END' | 'END' (POSITIVE_RANGE | NEGATIVE_RANGE) | 'START' '+' POSITIVE_RANGE | 'START' | 'START' (POSITIVE_RANGE | NEGATIVE_RANGE) | 'END' '-' (POSITIVE_RANGE) | 'START' '-' (POSITIVE_RANGE));
end_time: ('MAX' | POSITIVE_RANGE | 'MIN' | NEGATIVE_RANGE | 'END' '+' POSITIVE_RANGE | 'END' | 'END' (POSITIVE_RANGE | NEGATIVE_RANGE) | 'START' '+' POSITIVE_RANGE | 'START' | 'START' (POSITIVE_RANGE | NEGATIVE_RANGE) | 'END' '-' (POSITIVE_RANGE) | 'START' '-' (POSITIVE_RANGE));

limit_type: POSITIVE_RANGE ('S' | 'SEC' | 'SECS' | 'SECOND' | 'SECONDS' | 'M' | 'MIN' | 'MINS' | 'MINUTE' | 'MINUTES')?
          | ('CACHE' | 'CACHED');

time_modifier: 'DAYS' 
             | 'DAY' 
             | 'MONTH'
             | 'MONTHS'
             | 'HOUR'
             | 'HOURS'
             | 'MINUTE'
             | 'MINUTES'
             | 'YEAR'
             | 'YEARS'
             | 'D'
             | 'M'
             | 'H'
             | 'Y'
             | 'W'
             | 'WEEKS'
             | 'WKS'
             | 'WK'
             | 'WEEK'
             ; 

note_atom: TEXT '=' TERM #textCommand
         | NEGATED_TEXT '=' TERM # negatedTextCommand
         | FAMILY_HISTORY_TEXT '=' TERM #familyHistoryTextCommand
         ;

note_atoms: note_atom
		  | note_atom (',' note_atom)+
		  ; 
    
atoms: atom
     | atom (',' atom)+;    

// ATOMS    
HASICD9: 'ICD9';
HASICD10: 'ICD10';
HASRXNORM: ('RX' | 'RXNORM');
HASCPT: 'CPT';
DEATH: ('DEATH' | 'DEAD');

//NOTE ATOM
TEXT: 'TEXT';
NEGATED_TEXT: '!TEXT';
FAMILY_HISTORY_TEXT: '~TEXT';

//TIME RANGES
ENCOUNTERS: ('ENCOUNTERS' | 'ENCOUNTER');
TIMELINE: 'TIMELINE';

//BOOLEAN
AND: 'AND';
OR: 'OR';
NOT: 'NOT';

//TIME
INTERSECT: 'INTERSECT';
UNION: 'UNION';
INVERT: 'INVERT';
INTERVAL: 'INTERVAL';

CPT: [0-9][0-9][0-9][0-9]'A'..'Z'
   | ['A'|'B'|'C'|'D'|'E'|'G'|'H'|'J'|'K'|'L'|'M'|'P'|'Q'|'R'|'S'|'T'|'V'][0-9][0-9][0-9][0-9];
   
ICD10:'A'..'Z'[0-9][0-9]'.'[0-9]
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9]
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9][0-9]
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9][0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z''A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9]'A'..'Z''A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9][0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z''A'..'Z''A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9]'A'..'Z''A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9]'A'..'Z'[0-9]
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z'[0-9]
     |'A'..'Z'[0-9]'A'..'Z''.'[0-9][0-9]
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z'[0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z''A'..'Z'[0-9]
     |'A'..'Z'[0-9][0-9]'-''A'..'Z'[0-9][0-9]
     |'A'..'Z'[0-9]'A'..'Z''-''A'..'Z'[0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.''A'..'Z''A'..'Z''A'..'Z''A'..'Z'
     |'A'..'Z'[0-9][0-9]'.''A'..'Z'[0-9]
     |'A'..'Z'[0-9][0-9]'.''A'..'Z'[0-9]'A'..'Z''A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9][0-9][0-9][0-9]
     |'A'..'Z'[0-9][0-9]'.''A'..'Z'[0-9][0-9]
     |'A'..'Z'[0-9]'A'..'Z''.'[0-9]
     |'A'..'Z'[0-9][0-9]'.''A'..'Z'[0-9][0-9]'A'..'Z'
     |'A'..'Z'[0-9]'A'..'Z''.'[0-9][0-9][0-9][0-9]
     |'A'..'Z'[0-9]'A'..'Z''.'[0-9][0-9]'A'..'Z'[0-9]
     |'A'..'Z'[0-9][0-9]'-''A'..'Z'[0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.''A'..'Z'
     |'A'..'Z'[0-9]'A'..'Z''.'[0-9][0-9][0-9]
     |'A'..'Z'[0-9]'A'..'Z''.'[0-9]'A'..'Z''A'..'Z'[0-9]
     |'A'..'Z'[0-9][0-9]
     |'A'..'Z'[0-9]'A'..'Z'
     |'A'..'Z'[0-9][0-9]'.'[0-9]'A'..'Z'[0-9][0-9]
     ;
   
ICD9: [0-9][0-9]'.'[0-9]
    | [0-9][0-9]'.'[0-9][0-9]
    | [0-9][0-9][0-9]'.'[0-9] 
    | [0-9][0-9][0-9]'.'[0-9][0-9]
    | 'A'..'Z'[0-9][0-9][0-9]
    | 'A'..'Z'[0-9][0-9][0-9]'.'[0-9]
    | 'A'..'Z'[0-9][0-9][0-9]'.'[0-9][0-9]
    | [0-9][0-9]'.'
    | [0-9][0-9]'-'[0-9][0-9]'.'[0-9][0-9]
    | [0-9][0-9][0-9]'-'[0-9][0-9][0-9]'.'[0-9][0-9]
    | [0-9][0-9]'.'[0-9]
    | [0-9][0-9][0-9]'.'[0-9]
    | [0-9][0-9][0-9]'.'
    | 'A'..'Z'[0-9][0-9][0-9]'.'
    | 'A'..'Z'[0-9][0-9][0-9]'-' 'A'..'Z'[0-9][0-9][0-9]'.'[0-9][0-9]
    | 'A'..'Z'[0-9][0-9][0-9]'-' 'A'..'Z'[0-9][0-9][0-9]'.'[0-9]
    | 'A'..'Z'[0-9][0-9]'.'
    | 'A'..'Z'[0-9][0-9]'-''A'..'Z'[0-9][0-9]'.'[0-9][0-9]
    ;
    
POSITIVE_RANGE: (DIGIT)+;
NEGATIVE_RANGE: '-' (DIGIT)+;
DIGIT: ('0' .. '9');
CHARACTER: 'A'..'Z' | 'a'..'z';

TERM: ('"' | '”' | '“') (DIGIT | CHARACTER | ',' | '-' | '_' | '%' | '(' | ')' | '+' | ' ' | '/' | '\\' | ';' | '~' | '`' | '!' | '@' | '#' | '^' | '&' | '*' | '{' | '}' | '[' | ']' | '|' | ':' | '<' | '>' | '?' | '.' | '=' | '\'')+('"' | '”' | '“');
WS : (' ' | '\t' '|' '\r' | '\n' | '\r\n' | ' ') -> skip;