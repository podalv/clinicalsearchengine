// Generated from Expr.g4 by ANTLR 4.7.1
package com.podalv.search.language;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ExprParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(ExprParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code outputCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutputCommand(ExprParser.OutputCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eventFlowCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEventFlowCommand(ExprParser.EventFlowCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code csvCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCsvCommand(ExprParser.CsvCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dumpCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDumpCommand(ExprParser.DumpCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachCommand}
	 * labeled alternative in {@link ExprParser#each}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachCommand(ExprParser.EachCommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#each_content}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEach_content(ExprParser.Each_contentContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#main_command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain_command(ExprParser.Main_commandContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#each_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEach_name(ExprParser.Each_nameContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachNodeAssign}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachNodeAssign(ExprParser.EachNodeAssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachNodeSave}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachNodeSave(ExprParser.EachNodeSaveContext ctx);
	/**
	 * Visit a parse tree produced by the {@code clearVariable}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClearVariable(ExprParser.ClearVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachLineIf}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachLineIf(ExprParser.EachLineIfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachLineEquals}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachLineEquals(ExprParser.EachLineEqualsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachNodeExit}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachNodeExit(ExprParser.EachNodeExitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code print}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(ExprParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code printSingle}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintSingle(ExprParser.PrintSingleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachNodeContinue}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachNodeContinue(ExprParser.EachNodeContinueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachNodeFailPatient}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachNodeFailPatient(ExprParser.EachNodeFailPatientContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unusedEach}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnusedEach(ExprParser.UnusedEachContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parseTerm}
	 * labeled alternative in {@link ExprParser#print_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParseTerm(ExprParser.ParseTermContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unused1}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnused1(ExprParser.Unused1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code unused2}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnused2(ExprParser.Unused2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code limitCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitCommand(ExprParser.LimitCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code estimateCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEstimateCommand(ExprParser.EstimateCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code evaluateCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEvaluateCommand(ExprParser.EvaluateCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unusedMultipleCommands}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnusedMultipleCommands(ExprParser.UnusedMultipleCommandsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unusedMultipleCommand}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnusedMultipleCommand(ExprParser.UnusedMultipleCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unusedAtoms}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnusedAtoms(ExprParser.UnusedAtomsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unusedCommand}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnusedCommand(ExprParser.UnusedCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndCommand(ExprParser.AndCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code orCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrCommand(ExprParser.OrCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotCommand(ExprParser.NotCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unusedBefore}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnusedBefore(ExprParser.UnusedBeforeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code complexBefore}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexBefore(ExprParser.ComplexBeforeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code findCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFindCommand(ExprParser.FindCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intersectingAll}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntersectingAll(ExprParser.IntersectingAllContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intersectingAny}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntersectingAny(ExprParser.IntersectingAnyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nonIntersectManyCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonIntersectManyCommand(ExprParser.NonIntersectManyCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nonIntersectAllCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonIntersectAllCommand(ExprParser.NonIntersectAllCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nonIntersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonIntersectCommand(ExprParser.NonIntersectCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neverIntersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeverIntersectCommand(ExprParser.NeverIntersectCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neverIntersectManyCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeverIntersectManyCommand(ExprParser.NeverIntersectManyCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neverIntersectAllCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeverIntersectAllCommand(ExprParser.NeverIntersectAllCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code alwaysIntersect}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlwaysIntersect(ExprParser.AlwaysIntersectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code alwaysIntersectAny}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlwaysIntersectAny(ExprParser.AlwaysIntersectAnyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code alwaysIntersectAll}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlwaysIntersectAll(ExprParser.AlwaysIntersectAllContext ctx);
	/**
	 * Visit a parse tree produced by the {@code invertCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInvertCommand(ExprParser.InvertCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code startCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStartCommand(ExprParser.StartCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code endCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEndCommand(ExprParser.EndCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intervalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalCommand(ExprParser.IntervalCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intervalPairwiseCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalPairwiseCommand(ExprParser.IntervalPairwiseCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntersectCommand(ExprParser.IntersectCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnionCommand(ExprParser.UnionCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code complexBeforeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexBeforeCommand(ExprParser.ComplexBeforeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code firstMentionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstMentionCommand(ExprParser.FirstMentionCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lastMentionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastMentionCommand(ExprParser.LastMentionCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code firstMentionContextCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstMentionContextCommand(ExprParser.FirstMentionContextCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lastMentionContextCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastMentionContextCommand(ExprParser.LastMentionContextCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code extendByCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtendByCommand(ExprParser.ExtendByCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code durationSimpleCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationSimpleCommand(ExprParser.DurationSimpleCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code durationComplexCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationComplexCommand(ExprParser.DurationComplexCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code durationComplex2Command}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationComplex2Command(ExprParser.DurationComplex2CommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code historyOfCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHistoryOfCommand(ExprParser.HistoryOfCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noHistoryOfCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoHistoryOfCommand(ExprParser.NoHistoryOfCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neverHadCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeverHadCommand(ExprParser.NeverHadCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code countSimpleCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCountSimpleCommand(ExprParser.CountSimpleCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code countComplexCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCountComplexCommand(ExprParser.CountComplexCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code countComplex2Command}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCountComplex2Command(ExprParser.CountComplex2CommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualCommand(ExprParser.EqualCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code containsCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContainsCommand(ExprParser.ContainsCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identicalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdenticalCommand(ExprParser.IdenticalCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sameCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSameCommand(ExprParser.SameCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code diffCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiffCommand(ExprParser.DiffCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mergeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMergeCommand(ExprParser.MergeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primaryCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryCommand(ExprParser.PrimaryCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code originalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOriginalCommand(ExprParser.OriginalCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code originalPrimaryCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOriginalPrimaryCommand(ExprParser.OriginalPrimaryCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code departmentCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDepartmentCommand(ExprParser.DepartmentCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noteTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoteTypeCommand(ExprParser.NoteTypeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code visitTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVisitTypeCommand(ExprParser.VisitTypeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code anyVisitTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnyVisitTypeCommand(ExprParser.AnyVisitTypeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code drugsSimple}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrugsSimple(ExprParser.DrugsSimpleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code drugsRoute}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrugsRoute(ExprParser.DrugsRouteContext ctx);
	/**
	 * Visit a parse tree produced by the {@code drugsStatus}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrugsStatus(ExprParser.DrugsStatusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code drugsRouteStatus}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrugsRouteStatus(ExprParser.DrugsRouteStatusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code drugsStatusRoute}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrugsStatusRoute(ExprParser.DrugsStatusRouteContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasCptCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasCptCommand(ExprParser.HasCptCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasAnyCptCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasAnyCptCommand(ExprParser.HasAnyCptCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasRxNormCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasRxNormCommand(ExprParser.HasRxNormCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasAnyRxNormCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasAnyRxNormCommand(ExprParser.HasAnyRxNormCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code genderCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenderCommand(ExprParser.GenderCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code yearCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitYearCommand(ExprParser.YearCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code raceCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRaceCommand(ExprParser.RaceCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ethnicityCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEthnicityCommand(ExprParser.EthnicityCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code deathCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeathCommand(ExprParser.DeathCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code anySnomedCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnySnomedCommand(ExprParser.AnySnomedCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code snomedCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSnomedCommand(ExprParser.SnomedCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code encountersCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEncountersCommand(ExprParser.EncountersCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noteTimeInstancesCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoteTimeInstancesCommand(ExprParser.NoteTimeInstancesCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code timelineCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimelineCommand(ExprParser.TimelineCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code recordStartCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordStartCommand(ExprParser.RecordStartCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code recordEndCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordEndCommand(ExprParser.RecordEndCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noteCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoteCommand(ExprParser.NoteCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noteBooleanCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoteBooleanCommand(ExprParser.NoteBooleanCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noteWithTypeCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoteWithTypeCommand(ExprParser.NoteWithTypeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noteAtomCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoteAtomCommand(ExprParser.NoteAtomCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ageCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgeCommand(ExprParser.AgeCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code vitalsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVitalsCommand(ExprParser.VitalsCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code labValuesCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabValuesCommand(ExprParser.LabValuesCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpleVitals}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleVitals(ExprParser.SimpleVitalsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code anyLabsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnyLabsCommand(ExprParser.AnyLabsCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code anyVitalsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnyVitalsCommand(ExprParser.AnyVitalsCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code complexLabs}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexLabs(ExprParser.ComplexLabsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpleLabs}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleLabs(ExprParser.SimpleLabsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numericInterval}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumericInterval(ExprParser.NumericIntervalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullCommand(ExprParser.NullCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atcCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtcCommand(ExprParser.AtcCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code icd9AtomCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIcd9AtomCommand(ExprParser.Icd9AtomCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code patientsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPatientsCommand(ExprParser.PatientsCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code patientsSingleCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPatientsSingleCommand(ExprParser.PatientsSingleCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eachNameCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEachNameCommand(ExprParser.EachNameCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasIcd9Command}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasIcd9Command(ExprParser.HasIcd9CommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasAnyIcd9Codes}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasAnyIcd9Codes(ExprParser.HasAnyIcd9CodesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasIcd10Command}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasIcd10Command(ExprParser.HasIcd10CommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hasAnyIcd10Codes}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHasAnyIcd10Codes(ExprParser.HasAnyIcd10CodesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#double_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDouble_value(ExprParser.Double_valueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atomWithAsterisk1}
	 * labeled alternative in {@link ExprParser#before_atom_left}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomWithAsterisk1(ExprParser.AtomWithAsterisk1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code atomWithAsterisk2}
	 * labeled alternative in {@link ExprParser#before_atom_right}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomWithAsterisk2(ExprParser.AtomWithAsterisk2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code atomWithAsterisk3}
	 * labeled alternative in {@link ExprParser#before_atom_left_left}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomWithAsterisk3(ExprParser.AtomWithAsterisk3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code atomWithAsterisk4}
	 * labeled alternative in {@link ExprParser#before_atom_left_no_asterisk}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomWithAsterisk4(ExprParser.AtomWithAsterisk4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code beforePositive}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforePositive(ExprParser.BeforePositiveContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeNegative}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeNegative(ExprParser.BeforeNegativeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforePositiveTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforePositiveTime(ExprParser.BeforePositiveTimeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeNegativeTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeNegativeTime(ExprParser.BeforeNegativeTimeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeBoth}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeBoth(ExprParser.BeforeBothContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeBothTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeBothTime(ExprParser.BeforeBothTimeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeCommandAtom}
	 * labeled alternative in {@link ExprParser#before_command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeCommandAtom(ExprParser.BeforeCommandAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeCommandAtom2}
	 * labeled alternative in {@link ExprParser#before_command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeCommandAtom2(ExprParser.BeforeCommandAtom2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code beforeModifier}
	 * labeled alternative in {@link ExprParser#before_modifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeforeModifier(ExprParser.BeforeModifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#start_time}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart_time(ExprParser.Start_timeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#end_time}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnd_time(ExprParser.End_timeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#limit_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimit_type(ExprParser.Limit_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#time_modifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTime_modifier(ExprParser.Time_modifierContext ctx);
	/**
	 * Visit a parse tree produced by the {@code textCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTextCommand(ExprParser.TextCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negatedTextCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegatedTextCommand(ExprParser.NegatedTextCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code familyHistoryTextCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFamilyHistoryTextCommand(ExprParser.FamilyHistoryTextCommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#note_atoms}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNote_atoms(ExprParser.Note_atomsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExprParser#atoms}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtoms(ExprParser.AtomsContext ctx);
}