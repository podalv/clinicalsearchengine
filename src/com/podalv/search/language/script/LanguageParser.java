package com.podalv.search.language.script;

import com.podalv.search.datastructures.CheckSyntaxRequest;
import com.podalv.search.datastructures.CheckSyntaxResponse;
import com.podalv.search.language.node.RootNode;
import com.podalv.utils.text.TextUtils;
import com.podalv.utils.text.TextUtils.REMOVE_TYPE;

public class LanguageParser {

  /** Since the parser ignores SPACE, TAB, etc. characters we need to adjust the last position by adding the
   *  characters that were removed by the parser. Then we can assess the EXTRANEOUS INPUT error
   *
   * @param originalQuery
   * @return
   */
  public static int recalculateLastPosition(final String originalQuery) {
    int sizeAdjustment = 0;
    boolean insideParenthesis = false;
    for (int x = 0; x < originalQuery.length(); x++) {
      if (originalQuery.charAt(x) == '"') {
        insideParenthesis = !insideParenthesis;
      }
      if (!insideParenthesis && (originalQuery.charAt(x) == ' ' || originalQuery.charAt(x) == '\t' || originalQuery.charAt(x) == '\n' || originalQuery.charAt(x) == '\r')) {
        sizeAdjustment++;
      }
    }
    return sizeAdjustment;
  }

  public static String trimQuery(final String query) {
    String result = TextUtils.removeTextBetweenChars(query, "/*", "*/");
    result = result.replace('\r', '\n');
    result = TextUtils.removeTextBetweenChars(result + "\n", "//", "\n", REMOVE_TYPE.REMOVE_START);
    result = TextUtils.removeTextBetweenChars(result + "\n", "#", "\n", REMOVE_TYPE.REMOVE_START);
    return result;
  }

  public static RootNode parse(final String query) {
    return parse(query, false);
  }

  public static RootNode parse(final String query, final boolean returnTis) {
    return RootNode.parse(query, returnTis, null);
  }

  public static CheckSyntaxResponse checkSyntax(final CheckSyntaxRequest request) {
    return new CheckSyntaxResponse();
  }

}