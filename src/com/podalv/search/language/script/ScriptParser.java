package com.podalv.search.language.script;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.search.autosuggest.AutosuggestSearch;
import com.podalv.utils.datastructures.SortPairSingle;
import com.podalv.utils.text.TextUtils;

/** Parses multi line script and returns parsed command
 *
 * @author podalv
 *
 */
public class ScriptParser {

  private final ScriptStorage persistedScripts;
  private static ScriptParser INSTANCE           = null;
  private boolean             containsNewContent = false;
  private AutosuggestSearch   autosuggest        = null;

  static {
    INSTANCE = new ScriptParser();
  }

  public String getScriptContent(final String name) {
    return persistedScripts.get(name);
  }

  public ScriptStorage getPersistedScripts() {
    return persistedScripts;
  }

  public synchronized boolean containsNewContent() {
    if (containsNewContent) {
      containsNewContent = false;
      return true;
    }
    return false;
  }

  public synchronized void setAutosuggest(final AutosuggestSearch autosuggest) {
    this.autosuggest = autosuggest;
  }

  public static ScriptParser getInstance() {
    return INSTANCE;
  }

  public static void setInstance(final ScriptParser instance) {
    INSTANCE = instance;
  }

  public ScriptParser(final ScriptStorage storage) {
    this.persistedScripts = storage;
  }

  public ScriptParser() {
    this.persistedScripts = null;
  }

  public String[] getScriptNames() {
    return persistedScripts == null ? new String[0] : persistedScripts.getScriptNames();
  }

  public String[] getScriptDomains() {
    return persistedScripts == null ? new String[0] : persistedScripts.getScriptDomains();
  }

  protected synchronized HashMap<String, SortPairSingle<String, StringPosition>> extractVariables(final String[] lines) {
    final HashSet<String> newVariables = new HashSet<>();
    final HashMap<String, SortPairSingle<String, StringPosition>> nameToQuery = new HashMap<>();
    int nonVarLines = 0;
    int varLines = 0;
    for (int x = 0; x < lines.length; x++) {
      final String line = lines[x].trim();
      if (line.startsWith("VAR ")) {
        varLines++;
        final int equalsPos = line.indexOf('=');
        final String variable = line.substring(4, equalsPos).trim().toLowerCase();
        final String content = line.substring(equalsPos + 1).trim();
        final int contentStartPos = line.indexOf(content, equalsPos);
        nameToQuery.put(variable, new SortPairSingle<>(content, new StringPosition(contentStartPos, x, content.length())));
        if (variable.indexOf('.') != -1) {
          newVariables.add(variable);
        }
      }
      else {
        if (!lines[x].isEmpty()) {
          nonVarLines++;
        }
      }
    }

    if (nonVarLines == 0 && varLines != 0) {
      throw new UnsupportedOperationException("A variable was defined but nothing was queried. To query the defined variable invoke it with a $ sign");
    }
    else if (nonVarLines == 0 && varLines == 0) {
      throw new UnsupportedOperationException("Query string is empty");
    }

    final Iterator<String> iterator = newVariables.iterator();
    while (iterator.hasNext()) {
      final String newVariable = iterator.next();
      if (persistedScripts.containsVariable(newVariable)) {
        throw new UnsupportedOperationException("Variable '" + newVariable + "' is already defined globally. Local variable cannot have the same name");
      }
      replaceVariableWithContent(nameToQuery.get(newVariable), -1, nameToQuery, null);
    }

    if (persistedScripts != null) {
      final Iterator<Entry<String, String>> i = persistedScripts.getScriptToContent();
      while (i.hasNext()) {
        final Entry<String, String> entry = i.next();
        nameToQuery.put(entry.getKey(), new SortPairSingle<>(entry.getValue(), new StringPosition(-1, -1, entry.getValue().length())));
      }
    }

    if (containsNewContent() && autosuggest != null) {
      autosuggest.buildScriptAutosuggest();
    }
    containsNewContent = false;
    return nameToQuery;
  }

  private String fixParenthesis(final String singleVariable) {
    int parenthesisCnt = 0;
    for (int x = 0; x < singleVariable.length(); x++) {
      if (singleVariable.charAt(x) == '(') {
        parenthesisCnt++;
      }
      else if (singleVariable.charAt(x) == ')') {
        parenthesisCnt--;
      }
    }
    final StringBuilder result = new StringBuilder(singleVariable);
    if (parenthesisCnt < 0) {
      for (int x = 0; x < Math.abs(parenthesisCnt); x++) {
        parenthesisCnt++;
        if (result.charAt(result.length() - 1) == ')') {
          result.setLength(result.length() - 1);
        }
        else {
          break;
        }
      }
    }
    if (parenthesisCnt > 0) {
      for (int x = 0; x < parenthesisCnt; x++) {
        parenthesisCnt--;
        result.append(")");
      }
    }
    return result.toString();
  }

  protected String replaceVariableWithContent(final SortPairSingle<String, StringPosition> content, final int row,
      final HashMap<String, SortPairSingle<String, StringPosition>> variables, final HashMap<StringPosition, StringPosition> toFrom) {
    String result = content.getComparable();
    final HashSet<String> processedCommand = new HashSet<>();
    int inflation = 0;
    while (true) {
      final int curPos = 0;
      final int varStartPos = result.indexOf('$', curPos);
      if (varStartPos == -1) {
        break;
      }
      int varEndPos = varStartPos;
      for (int y = varStartPos + 1; y < result.length(); y++) {
        if (!(((result.charAt(y) >= 'A' && result.charAt(y) <= 'Z') || result.charAt(y) == '_' || result.charAt(y) == '.') || (result.charAt(y) >= '0' && result.charAt(
            y) <= '9'))) {
          varEndPos = y;
          break;
        }
      }
      if (varEndPos == varStartPos) {
        varEndPos = result.length();
      }
      String varName = result.substring(varStartPos + 1, varEndPos).toLowerCase().trim();
      boolean save = false;
      if (varName.endsWith(".finalize")) {
        save = true;
        varName = varName.substring(0, varName.lastIndexOf(".finalize"));
      }
      if (varName.endsWith(".save")) {
        save = true;
        varName = varName.substring(0, varName.lastIndexOf(".save"));
      }
      if (varName.endsWith(".persist")) {
        save = true;
        varName = varName.substring(0, varName.lastIndexOf(".persist"));
      }
      if (varName.endsWith(".store")) {
        save = true;
        varName = varName.substring(0, varName.lastIndexOf(".store"));
      }
      SortPairSingle<String, StringPosition> replace = variables.get(varName);
      if (replace == null && persistedScripts != null) {
        final String persistedScript = persistedScripts.get(varName);
        replace = new SortPairSingle<>(persistedScripts.get(varName), new StringPosition(-1, -1, persistedScript == null ? 0 : persistedScripts.get(varName).length()));
      }
      if (persistedScripts != null && varName.indexOf('.') != -1 && save) {
        final boolean newVar = persistedScripts.save(varName, replace.getComparable());
        if (newVar) {
          containsNewContent = newVar;
        }
      }
      if (replace == null || replace.getComparable() == null) {
        containsNewContent = false;
        final StringPosition pos = ScriptParserResult.getOriginalPosition(toFrom, (varStartPos), row);
        throw new UnsupportedOperationException("[" + (pos.getColumn() + 1 + inflation) + "," + (pos.getColumn() + varName.length() + 1 + inflation) + ":" + pos.getRow()
            + "] Could not find variable '$" + varName + "'");
      }
      final String toReplace = fixParenthesis(replace.getComparable());
      if (toFrom != null) {
        shiftPositions(toFrom, row, varStartPos, toReplace.length());
      }

      if (toFrom != null) {
        final StringPosition original = new StringPosition(replace.getObject().getColumn(), replace.getObject().getRow(), replace.getObject().getLength());
        toFrom.get(original);
        toFrom.remove(original);
        final StringPosition newParent = new StringPosition(varStartPos, row, toReplace.length());
        removeAllChildren(toFrom, newParent);
        toFrom.put(newParent, original);
      }

      result = result.substring(0, varStartPos) + toReplace + result.substring(varEndPos);
      inflation += (varName.length() - toReplace.length() + 1);
      if (processedCommand.contains(result)) {
        throw new UnsupportedOperationException("Circular reference detected");
      }
      processedCommand.add(result);
    }
    if (containsNewContent && autosuggest != null) {
      autosuggest.buildScriptAutosuggest();
    }
    containsNewContent = false;
    return result;
  }

  private void removeAllChildren(final HashMap<StringPosition, StringPosition> toFrom, final StringPosition newParent) {
    final Iterator<Entry<StringPosition, StringPosition>> iterator = toFrom.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<StringPosition, StringPosition> entry = iterator.next();
      if (newParent.getColumn() <= entry.getValue().getColumn() && newParent.getColumn() + newParent.getLength() >= entry.getValue().getColumn() + entry.getValue().getLength()
          && entry.getValue().getRow() == newParent.getRow()) {
        iterator.remove();
      }
    }
  }

  /** Shifts all the tracked original positions in the specified row from the start by the specified length
   *
   * @param toFrom
   * @param row
   * @param start
   * @param length
   */
  private void shiftPositions(final HashMap<StringPosition, StringPosition> toFrom, final int row, final int start, final int length) {
    final Iterator<Entry<StringPosition, StringPosition>> iterator = toFrom.entrySet().iterator();
    final ArrayList<StringPosition[]> newPoints = new ArrayList<>();
    while (iterator.hasNext()) {
      final Entry<StringPosition, StringPosition> entry = iterator.next();
      if (entry.getKey().getRow() == row && entry.getKey().getColumn() >= start) {
        newPoints.add(new StringPosition[] {new StringPosition(entry.getKey().getColumn() + length, row, entry.getKey().getLength()), entry.getValue()});
        iterator.remove();
      }
    }
    for (final StringPosition[] point : newPoints) {
      toFrom.put(point[0], point[1]);
    }
  }

  public static String getOriginalUnparsedQuery(final String query) {
    final String[] lines = TextUtils.split(LanguageParser.trimQuery(query).toUpperCase(), '\n');
    for (int x = 0; x < lines.length; x++) {
      lines[x] = lines[x].trim();
    }

    int nonVarPos = -1;
    for (int x = 0; x < lines.length; x++) {
      if (nonVarPos == -1 && !lines[x].toLowerCase().startsWith("var") && !lines[x].isEmpty()) {
        nonVarPos = x;
      }
      else if (nonVarPos != -1 && !lines[x].toLowerCase().startsWith("var")) {
        lines[nonVarPos] += " " + lines[x];
        lines[x] = "";
      }
    }
    return lines[nonVarPos];
  }

  public ScriptParserResult parseScripts(String query) {
    query = LanguageParser.trimQuery(query);
    final HashMap<StringPosition, StringPosition> toFrom = new HashMap<>();
    final String[] lines = TextUtils.split(query, '\n');
    compressNonVariableLines(toFrom, lines);
    final HashMap<String, SortPairSingle<String, StringPosition>> nameToQuery = extractVariables(lines);
    for (int x = lines.length - 1; x >= 0; x--) {
      if (!lines[x].isEmpty() && !lines[x].toLowerCase().startsWith("var")) {
        final String result = replaceVariableWithContent(new SortPairSingle<>(lines[x], new StringPosition(0, x, lines[x].length())), x, nameToQuery, toFrom);
        return new ScriptParserResult(query, result, toFrom, x);
      }
    }
    return new ScriptParserResult(query, query, toFrom, 0);
  }

  private void compressNonVariableLines(final HashMap<StringPosition, StringPosition> toFrom, final String[] lines) {
    int nonVarPos = -1;
    for (int x = 0; x < lines.length; x++) {
      if (lines[x].trim().isEmpty()) {
        continue;
      }
      if (nonVarPos == -1 && !lines[x].toLowerCase().startsWith("var") && !lines[x].isEmpty()) {
        nonVarPos = x;
      }
      else if (nonVarPos != -1 && !lines[x].toLowerCase().startsWith("var")) {
        StringPosition toPoint = new StringPosition(0, x, lines[x].length());
        StringPosition fromPoint = toFrom.get(toPoint);
        if (fromPoint == null) {
          fromPoint = toPoint;
        }
        else {
          toFrom.remove(toPoint);
        }
        toPoint = new StringPosition(lines[nonVarPos].length() + 1, nonVarPos, lines[x].length());
        toFrom.put(toPoint, fromPoint);
        lines[nonVarPos] += " " + lines[x];
        lines[x] = "";
      }
    }

    if (nonVarPos > 0) {
      toFrom.put(new StringPosition(0, 0, lines[nonVarPos].length()), new StringPosition(0, nonVarPos, lines[nonVarPos].length()));
    }
  }

}