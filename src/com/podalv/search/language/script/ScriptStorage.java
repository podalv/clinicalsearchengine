package com.podalv.search.language.script;

import static com.podalv.utils.Logging.error;
import static com.podalv.utils.Logging.exception;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.maps.string.StringArrayList;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.text.TextUtils;

/** Manages storage of scripts on the server
 *
 * @author podalv
 *
 */
public class ScriptStorage {

  private static final String           SCRIPT_FILE_NAME = "scripts.dat";
  private final File                    outputFileName;
  private final HashMap<String, String> scriptToContent  = new HashMap<String, String>();
  private final HashSet<String>         domains          = new HashSet<String>();
  private String[]                      domainsSorted    = new String[0];
  private final HashSet<String>         temporaryScripts = new HashSet<String>();

  public ScriptStorage() {
    outputFileName = null;
  }

  public synchronized boolean containsVariable(final String var) {
    return scriptToContent.containsKey(var.toLowerCase());
  }

  public ScriptStorage(final File folder) {
    outputFileName = new File(folder, SCRIPT_FILE_NAME);
    load(outputFileName);
  }

  public String get(final String scriptName) {
    return scriptToContent.get(scriptName);
  }

  public Iterator<Entry<String, String>> getScriptToContent() {
    return scriptToContent.entrySet().iterator();
  }

  private void calculateSortedDomains() {
    final StringArrayList result = new StringArrayList();
    final Iterator<String> i = domains.iterator();
    while (i.hasNext()) {
      result.add(i.next());
    }
    result.sort();
    domainsSorted = result.toArray();
  }

  protected String[] getScriptDomains() {
    return domainsSorted;
  }

  protected String[] getScriptNames() {
    final StringArrayList result = new StringArrayList();
    final Iterator<String> i = scriptToContent.keySet().iterator();
    while (i.hasNext()) {
      result.add(i.next().toLowerCase());
    }
    result.sort();
    return result.toArray();
  }

  private void load(final File file) {
    if (file.exists()) {
      try {
        final TextFileReader reader = FileUtils.readFileIterator(outputFileName, Charset.forName("UTF-8"));
        while (reader.hasNext()) {
          final String[] data = TextUtils.split(reader.next(), '\t');
          scriptToContent.put(data[0].toLowerCase(), data[1]);
          final String[] domain = TextUtils.split(data[0].toLowerCase(), '.');
          if (domain.length > 1) {
            domains.add(domain[0].toLowerCase());
          }
        }
        reader.close();
        calculateSortedDomains();
      }
      catch (final Exception e) {
        exception(e);
      }
    }
    else {
      error("Script file does not exist. No scripts are loaded...");
    }
  }

  public void addTemporaryScript(final String variable, final String content) {
    temporaryScripts.add(variable);
    scriptToContent.put(variable, content);
    final int pos = variable.indexOf('.');
    final String domain = variable.substring(pos + 1);
    domains.add(domain);
  }

  protected synchronized boolean save(final String newVariable, final String newContent) {
    if (newContent != null) {
      final int pos = newVariable.indexOf('.');
      final String domain = newVariable.substring(pos + 1);
      if (!scriptToContent.containsKey(newVariable)) {
        domains.add(domain);
        scriptToContent.put(newVariable, newContent);
        calculateSortedDomains();
        if (outputFileName != null) {
          try {
            final BufferedWriter out = new BufferedWriter(new FileWriter(outputFileName, true));
            out.write(newVariable + "\t" + newContent + "\n");
            out.close();
          }
          catch (final Exception e) {
            exception(e);
          }
        }
        return true;
      }
      else {
        throw new UnsupportedOperationException("Cannot redefine a finalized global variable");
      }
    }
    return false;
  }

}