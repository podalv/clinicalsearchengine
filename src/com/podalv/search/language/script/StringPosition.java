package com.podalv.search.language.script;

/** Coordinates in a string [column, row]
 *
 * @author podalv
 *
 */
public class StringPosition {

  private final int column;
  private final int row;
  private final int length;

  public StringPosition(final int column, final int row, final int length) {
    this.column = column;
    this.row = row;
    this.length = length;
  }

  @Override
  public int hashCode() {
    return column;
  }

  public int getColumn() {
    return column;
  }

  public int getLength() {
    return length;
  }

  public int getRow() {
    return row;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof StringPosition) {
      return ((StringPosition) obj).column == column && ((StringPosition) obj).row == row && ((StringPosition) obj).length == length;
    }
    return false;
  }

  @Override
  public String toString() {
    return "[" + column + "-" + (column + length) + "," + row + "]";
  }
}
