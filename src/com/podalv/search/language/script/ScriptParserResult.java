package com.podalv.search.language.script;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/** Encapsulates parsing result as well as mapping of positions to original string
 *
 * @author podalv
 *
 */
public class ScriptParserResult {

  private final String                                  result;
  private final String                                  originalQuery;
  private final HashMap<StringPosition, StringPosition> toFrom;
  private final int                                     finalVariableRowNr;

  public ScriptParserResult(final String originalQuery, final String result, final HashMap<StringPosition, StringPosition> toFrom, final int finalVariableRowNr) {
    this.result = result.trim();
    this.toFrom = toFrom;
    this.finalVariableRowNr = finalVariableRowNr;
    this.originalQuery = originalQuery;
  }

  public String getOriginalQuery() {
    return originalQuery;
  }

  public int getFinalVariableRowNr() {
    return finalVariableRowNr;
  }

  public static StringPosition getOriginalPosition(final HashMap<StringPosition, StringPosition> toFrom, final int currentColumn, final int currentRow) {
    final Iterator<Entry<StringPosition, StringPosition>> iterator = toFrom.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<StringPosition, StringPosition> entry = iterator.next();
      if (currentColumn >= entry.getKey().getColumn() && currentColumn <= entry.getKey().getColumn() + entry.getKey().getLength() && currentRow == entry.getKey().getRow()) {
        final int columnDiff = Math.abs(currentColumn - entry.getKey().getColumn());
        return new StringPosition(entry.getValue().getColumn() + columnDiff, entry.getValue().getRow(), entry.getValue().getLength());
      }
    }
    return new StringPosition(currentColumn, currentRow, 1);
  }

  public StringPosition getOriginalPosition(final int currentColumn, final int currentRow) {
    return getOriginalPosition(this.toFrom, currentColumn, currentRow);
  }

  public String getResult() {
    return result;
  }

}
