package com.podalv.search.language;

import java.util.ArrayList;
import java.util.Iterator;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import com.podalv.extractor.stride6.Common;
import com.podalv.search.datastructures.Enums.RANGE_REFERENCE;
import com.podalv.search.datastructures.Enums.RANGE_TYPE;
import com.podalv.search.language.ExprParser.*;
import com.podalv.search.language.each.EachNode;
import com.podalv.search.language.each.EachNodeAtom;
import com.podalv.search.language.each.EachNodeClearNode;
import com.podalv.search.language.each.EachNodeContinue;
import com.podalv.search.language.each.EachNodeExit;
import com.podalv.search.language.each.EachNodeFail;
import com.podalv.search.language.each.EachNodeIf;
import com.podalv.search.language.each.EachNodeNamedCommand;
import com.podalv.search.language.each.EachNodeReturnNode;
import com.podalv.search.language.each.PrintNode;
import com.podalv.search.language.each.TextPrintNode;
import com.podalv.search.language.node.AgeNode;
import com.podalv.search.language.node.AndNode;
import com.podalv.search.language.node.AnyLabsNode;
import com.podalv.search.language.node.AnyRxNormNode;
import com.podalv.search.language.node.AnySnomedNode;
import com.podalv.search.language.node.AnyVisitTypeNode;
import com.podalv.search.language.node.AnyVitalsNode;
import com.podalv.search.language.node.AtcNode;
import com.podalv.search.language.node.BeforeNode;
import com.podalv.search.language.node.BeforeNodeParameter;
import com.podalv.search.language.node.CountNode;
import com.podalv.search.language.node.CsvNode;
import com.podalv.search.language.node.DeathNode;
import com.podalv.search.language.node.DepartmentNode;
import com.podalv.search.language.node.DiffNode;
import com.podalv.search.language.node.DrugNode;
import com.podalv.search.language.node.DumpNode;
import com.podalv.search.language.node.DurationNode;
import com.podalv.search.language.node.EncountersNode;
import com.podalv.search.language.node.EndNode;
import com.podalv.search.language.node.EqualNode;
import com.podalv.search.language.node.ErrorNode;
import com.podalv.search.language.node.EstimateNode;
import com.podalv.search.language.node.EthnicityNode;
import com.podalv.search.language.node.EvaluateNode;
import com.podalv.search.language.node.EventFlowNode;
import com.podalv.search.language.node.ExtendByNode;
import com.podalv.search.language.node.FamilyHistoryTextNode;
import com.podalv.search.language.node.FirstMentionNode;
import com.podalv.search.language.node.GenderNode;
import com.podalv.search.language.node.HasAnyCptNode;
import com.podalv.search.language.node.HasAnyIcd10Node;
import com.podalv.search.language.node.HasAnyIcd9Node;
import com.podalv.search.language.node.HasCptNode;
import com.podalv.search.language.node.HasIcd10Node;
import com.podalv.search.language.node.HasIcd9Node;
import com.podalv.search.language.node.IdenticalNode;
import com.podalv.search.language.node.IntersectNode;
import com.podalv.search.language.node.IntervalNode;
import com.podalv.search.language.node.IntervalNumericNode;
import com.podalv.search.language.node.InvertNode;
import com.podalv.search.language.node.LabValuesNode;
import com.podalv.search.language.node.LabsNode;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.LanguageNodeWithChildren;
import com.podalv.search.language.node.LanguageNodeWithData;
import com.podalv.search.language.node.LastMentionNode;
import com.podalv.search.language.node.LimitNode;
import com.podalv.search.language.node.MergeNode;
import com.podalv.search.language.node.NegatedTextNode;
import com.podalv.search.language.node.NotNode;
import com.podalv.search.language.node.NoteAndNode;
import com.podalv.search.language.node.NoteNode;
import com.podalv.search.language.node.NoteNotNode;
import com.podalv.search.language.node.NoteOrNode;
import com.podalv.search.language.node.NoteTimeInstancesNode;
import com.podalv.search.language.node.NoteTypeNode;
import com.podalv.search.language.node.NullNode;
import com.podalv.search.language.node.OrNode;
import com.podalv.search.language.node.OutputNode;
import com.podalv.search.language.node.PatientsNode;
import com.podalv.search.language.node.PrimaryNode;
import com.podalv.search.language.node.RaceNode;
import com.podalv.search.language.node.RxNormNode;
import com.podalv.search.language.node.SameNode;
import com.podalv.search.language.node.SnomedNode;
import com.podalv.search.language.node.StartNode;
import com.podalv.search.language.node.TextNode;
import com.podalv.search.language.node.TimeNode;
import com.podalv.search.language.node.TimelineNode;
import com.podalv.search.language.node.UnionNode;
import com.podalv.search.language.node.VisitTypeNode;
import com.podalv.search.language.node.VitalsNode;
import com.podalv.search.language.node.YearNode;
import com.podalv.search.language.script.LanguageParser;

public class CommandVisitor extends ExprBaseVisitor<LanguageNode> {

  private final LanguageNode            root;
  private final ArrayList<LanguageNode> stack                      = new ArrayList<>();
  private int                           lastCommandVisitedPosition = 0;

  public CommandVisitor(final LanguageNode node) {
    this.root = node;
    stack.add(root);
  }

  @Override
  public LanguageNode visitLimitCommand(final LimitCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new LimitNode(ctx.limit_type().getText()), ctx);
  }

  @Override
  public LanguageNode visitIntersectingAll(final IntersectingAllContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final IntersectNode result = new IntersectNode();
    result.addChild(child.getChildren().get(0));

    for (int x = 1; x < child.getChildren().size(); x++) {
      final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
          ctx.getStop().getLine());
      before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
      before.addChild(child.getChildren().get(x));
      before.addChild(child.getChildren().get(0));
      result.addChild(before);
    }

    return result;
  }

  @Override
  public LanguageNode visitParseTerm(final ParseTermContext ctx) {
    recordCommandPosition(ctx);
    return new TextPrintNode(TextNode.trimString(ctx.getText(), false));
  }

  @Override
  public LanguageNode visitDumpCommand(final DumpCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new DumpNode(ctx.TERM().toString()), ctx);
  }

  @Override
  public LanguageNode visitPrintSingle(final PrintSingleContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new PrintNode(), ctx);
  }

  @Override
  public LanguageNode visitPrint(final PrintContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new PrintNode(), ctx);
  }

  @Override
  public LanguageNode visitEvaluateCommand(final EvaluateCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new EvaluateNode(ctx.limit_type().getText()), ctx);
  }

  @Override
  public LanguageNode visitEstimateCommand(final EstimateCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new EstimateNode(ctx.limit_type().getText()), ctx);
  }

  @Override
  public LanguageNode visitClearVariable(final ClearVariableContext ctx) {
    recordCommandPosition(ctx);
    return new EachNodeClearNode(ctx.each_name().getText());
  }

  @Override
  protected LanguageNode aggregateResult(final LanguageNode aggregate, final LanguageNode nextResult) {
    if (nextResult != null) {
      ((LanguageNodeWithChildren) stack.get(stack.size() - 1)).addChild(nextResult);
    }

    return null;
  }

  private void addToStack(final LanguageNode child) {
    stack.add(child);
  }

  private void popStack() {
    stack.remove(stack.size() - 1);
  }

  private ArrayList<LanguageNode> getSubTree(final RuleNode ctx) {
    return visitSubTree(new IntersectNode(), ctx).getChildren();
  }

  private LanguageNodeWithChildren visitSubTree(final LanguageNodeWithChildren parent, final RuleNode ctx) {
    addToStack(parent);
    visitChildren(ctx);
    popStack();

    return parent;
  }

  @Override
  public LanguageNode visitHasRxNormCommand(final HasRxNormCommandContext ctx) {
    recordCommandPosition(ctx);
    return new RxNormNode(Integer.parseInt(ctx.POSITIVE_RANGE().getText())).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitOutputCommand(final OutputCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new OutputNode(), ctx);
  }

  @Override
  public LanguageNode visitHasIcd10Command(final HasIcd10CommandContext ctx) {
    recordCommandPosition(ctx);
    if (ctx.ICD10() != null) {
      return new HasIcd10Node(ctx.ICD10().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    }
    return new HasIcd10Node(ctx.ICD10().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitDepartmentCommand(final DepartmentCommandContext ctx) {
    recordCommandPosition(ctx);
    return new DepartmentNode(ctx.TERM().toString()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitHasIcd9Command(final HasIcd9CommandContext ctx) {
    recordCommandPosition(ctx);
    if (ctx.ICD9() != null) {
      return new HasIcd9Node(ctx.ICD9().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    }
    if (ctx.ICD10() != null) {
      return new HasIcd9Node(ctx.ICD10().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    }
    if (ctx.POSITIVE_RANGE() != null) {
      return new HasIcd9Node(ctx.POSITIVE_RANGE().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    }
    return new ErrorNode("Invalid parameter").setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  private void copyChildren(final LanguageNode src, final LanguageNode tgt) {
    if (src instanceof LanguageNodeWithChildren && tgt instanceof LanguageNodeWithChildren) {
      for (int x = 0; x < ((LanguageNodeWithChildren) src).getChildren().size(); x++) {
        ((LanguageNodeWithChildren) tgt).addChild(((LanguageNodeWithChildren) src).getChildren().get(x));
      }
    }
    else {
      throw new UnsupportedOperationException("Copy of node children not available for these nodes");
    }
  }

  private LanguageNode reformatNoteBooleanCommand(final LanguageNode node) {
    if (node instanceof LanguageNodeWithChildren) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).getChildren().size(); x++) {
        if (((LanguageNodeWithChildren) node).getChildren().get(x) instanceof AndNode) {
          final NoteAndNode result = new NoteAndNode();
          copyChildren(((LanguageNodeWithChildren) node).getChildren().get(x), result);
          ((LanguageNodeWithChildren) node).getChildren().remove(x);
          ((LanguageNodeWithChildren) node).getChildren().add(x, result);
        }
        else if (((LanguageNodeWithChildren) node).getChildren().get(x) instanceof OrNode) {
          final NoteOrNode result = new NoteOrNode();
          copyChildren(((LanguageNodeWithChildren) node).getChildren().get(x), result);
          ((LanguageNodeWithChildren) node).getChildren().remove(x);
          ((LanguageNodeWithChildren) node).getChildren().add(x, result);
        }
        else if (((LanguageNodeWithChildren) node).getChildren().get(x) instanceof NotNode) {
          final NoteNotNode result = new NoteNotNode();
          copyChildren(((LanguageNodeWithChildren) node).getChildren().get(x), result);
          ((LanguageNodeWithChildren) node).getChildren().remove(x);
          ((LanguageNodeWithChildren) node).getChildren().add(x, result);
        }
        reformatNoteBooleanCommand(((LanguageNodeWithChildren) node).getChildren().get(x));
      }
    }
    return node;
  }

  @Override
  public LanguageNode visitNoteBooleanCommand(final NoteBooleanCommandContext ctx) {
    recordCommandPosition(ctx);
    final NoteNode result = (NoteNode) visitSubTree(new NoteNode(), ctx);
    LanguageNode errorResult = evaluateNoteCommand(result);
    int fndNoteTypes = 0;
    for (int x = 0; x < result.getChildren().size(); x++) {
      if (result.getChildren().get(x) instanceof NoteTypeNode) {
        result.getChildren().remove(x);
        fndNoteTypes++;
        x--;
      }
    }
    if (fndNoteTypes > 1) {
      errorResult = new ErrorNode("There can be only one NoteType node in Notes command");
    }
    if (result.getChildren().size() == 0) {
      errorResult = new ErrorNode("There must be at least one Text node defined in Notes command");
    }
    return errorResult instanceof ErrorNode ? errorResult : reformatNoteBooleanCommand(result);
  }

  @Override
  public LanguageNode visitHasCptCommand(final HasCptCommandContext ctx) {
    recordCommandPosition(ctx);
    if (ctx.CPT() != null) {
      return new HasCptNode(ctx.CPT().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    }
    return new HasCptNode(ctx.POSITIVE_RANGE().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  private void recordCommandPosition(final ParserRuleContext ctx) {
    lastCommandVisitedPosition = Math.max(ctx.getStop().getStopIndex(), lastCommandVisitedPosition);
    if (ctx.getStop().getCharPositionInLine() < ctx.getText().length()) {
      lastCommandVisitedPosition = Math.max(ctx.getText().length(), lastCommandVisitedPosition);
    }
  }

  @Override
  public LanguageNode visitGenderCommand(final GenderCommandContext ctx) {
    recordCommandPosition(ctx);
    return new GenderNode(ctx.TERM().toString());
  }

  @Override
  public LanguageNode visitEthnicityCommand(final EthnicityCommandContext ctx) {
    recordCommandPosition(ctx);
    return new EthnicityNode(ctx.TERM().toString());
  }

  @Override
  public LanguageNode visitRaceCommand(final RaceCommandContext ctx) {
    recordCommandPosition(ctx);
    return new RaceNode(ctx.TERM().toString());
  }

  // BOOLEAN
  @Override
  public LanguageNode visitAndCommand(final AndCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new AndNode(), ctx);
  }

  @Override
  public LanguageNode visitNotCommand(final NotCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new NotNode(), ctx);
  }

  @Override
  public LanguageNode visitOrCommand(final OrCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new OrNode(), ctx);
  }

  private LanguageNode evaluateNoteCommand(final LanguageNode node) {
    if (node instanceof LanguageNodeWithChildren && ((LanguageNodeWithChildren) node).getChildren().size() != 0) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).getChildren().size(); x++) {
        final LanguageNode result = evaluateNoteChildren(((LanguageNodeWithChildren) node).getChildren().get(x));
        if (result instanceof ErrorNode) {
          return result;
        }
      }
      return node;
    }
    else {
      return new ErrorNode("NOTE node has to contain some children");
    }
  }

  @Override
  public LanguageNode visitNoteTimeInstancesCommand(final NoteTimeInstancesCommandContext ctx) {
    recordCommandPosition(ctx);
    return new NoteTimeInstancesNode();
  }

  private LanguageNode evaluateNoteChildren(final LanguageNode node) {
    if (node.getParent() instanceof NoteNode && node instanceof NoteTypeNode) {
      ((NoteNode) node.getParent()).setNoteTypeString(((NoteTypeNode) node).getNoteTypeString());
      return node;
    }
    if (node instanceof AndNode || node instanceof OrNode || node instanceof NotNode) {
      if (((LanguageNodeWithChildren) node).getChildren().size() != 0) {
        for (int x = 0; x < ((LanguageNodeWithChildren) node).getChildren().size(); x++) {
          evaluateNoteCommand(((LanguageNodeWithChildren) node).getChildren().get(x));
        }
        return node;
      }
      else {
        return new ErrorNode("AND, OR and NOT nodes cannot be empty");
      }
    }
    else if (!(node instanceof TextNode)) {
      return new ErrorNode("NOTE node cannot contain other children than AND, OR, NOT and TEXT. Found '" + node.getClass().getName() + "'");
    }
    return node;
  }

  @Override
  public LanguageNode visitNoteWithTypeCommand(final NoteWithTypeCommandContext ctx) {
    recordCommandPosition(ctx);
    final NoteNode result = new NoteNode();
    result.setNoteTypeString(ctx.TERM().getText());
    return visitSubTree(result, ctx);
  }

  @Override
  public LanguageNode visitNoteCommand(final NoteCommandContext ctx) {
    recordCommandPosition(ctx);
    final NoteNode result = (NoteNode) visitSubTree(new NoteNode(), ctx);
    LanguageNode errorResult = result;
    int fndNoteTypes = 0;
    for (int x = 0; x < result.getChildren().size(); x++) {
      if (result.getChildren().get(x) instanceof NoteTypeNode) {
        result.getChildren().remove(x);
        fndNoteTypes++;
        x--;
      }
    }
    if (fndNoteTypes > 1) {
      errorResult = new ErrorNode("There can be only one NoteType node in Notes command");
    }
    if (result.getChildren().size() == 0) {
      errorResult = new ErrorNode("There must be at least one Text node defined in Notes command");
    }

    return errorResult instanceof ErrorNode ? errorResult : reformatNoteBooleanCommand(result);
  }

  @Override
  public LanguageNode visitTextCommand(final TextCommandContext ctx) {
    recordCommandPosition(ctx);
    return new TextNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitNegatedTextCommand(final NegatedTextCommandContext ctx) {
    recordCommandPosition(ctx);
    return new NegatedTextNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitFamilyHistoryTextCommand(final FamilyHistoryTextCommandContext ctx) {
    recordCommandPosition(ctx);
    return new FamilyHistoryTextNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitEachNodeExit(final EachNodeExitContext ctx) {
    recordCommandPosition(ctx);
    return new EachNodeExit();
  }

  @Override
  public LanguageNode visitEachLineEquals(final EachLineEqualsContext ctx) {
    recordCommandPosition(ctx);
    final boolean isPositive = (ctx.label.getText().equals("=="));
    final LanguageNodeWithChildren intersect = visitSubTree(new IntersectNode(), ctx);
    final EachNodeIf result = new EachNodeIf(!isPositive);
    final EqualNode equal = new EqualNode();
    equal.addChild(intersect.getChildren().get(0));
    equal.addChild(intersect.getChildren().get(1));
    result.addChild(equal);
    for (int x = 2; x < intersect.getChildren().size(); x++) {
      result.addChild(intersect.getChildren().get(x));
    }
    return result;
  }

  @Override
  public LanguageNode visitEachLineIf(final EachLineIfContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new EachNodeIf(!(ctx.not_label != null)), ctx);
  }

  @Override
  public LanguageNode visitEachNodeFailPatient(final EachNodeFailPatientContext ctx) {
    recordCommandPosition(ctx);
    return new EachNodeFail();
  }

  @Override
  public LanguageNode visitEachNodeContinue(final EachNodeContinueContext ctx) {
    recordCommandPosition(ctx);
    return new EachNodeContinue();
  }

  @Override
  public LanguageNode visitCsvCommand(final CsvCommandContext ctx) {
    recordCommandPosition(ctx);
    CsvNode node = null;
    int startPos = 0;
    if (ctx.time_cohort != null) {
      final LanguageNode timeNode = getSubTree(ctx.time_cohort).get(0);
      final LanguageNode cohortNode = getSubTree(ctx.cohort).get(0);
      node = (CsvNode) new CsvNode(cohortNode, timeNode, null).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
      timeNode.setParent(node);
      cohortNode.setParent(node);
      startPos = 2;
    }
    else if (ctx.start_cohort != null && ctx.end_cohort == null) {
      final EndNode end = new EndNode();
      final LastMentionNode last = new LastMentionNode();
      last.addChild(new TimelineNode());
      end.addChild(last);
      final StartNode start = new StartNode();
      final FirstMentionNode fm = new FirstMentionNode();
      fm.addChild(getSubTree(ctx.start_cohort).get(0));
      start.addChild(fm);
      final LanguageNode cohortNode = getSubTree(ctx.cohort).get(0);
      node = (CsvNode) new CsvNode(cohortNode, start, end).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
      start.setParent(node);
      cohortNode.setParent(node);
      end.setParent(node);
      startPos = 2;
    }
    else if (ctx.start_cohort != null && ctx.end_cohort != null) {
      final EndNode end = new EndNode();
      final LastMentionNode lm = new LastMentionNode();
      lm.addChild(getSubTree(ctx.end_cohort).get(0));
      end.addChild(lm);
      final StartNode start = new StartNode();
      final FirstMentionNode fm = new FirstMentionNode();
      fm.addChild(getSubTree(ctx.start_cohort).get(0));
      start.addChild(fm);
      final LanguageNode cohortNode = getSubTree(ctx.cohort).get(0);
      node = (CsvNode) new CsvNode(cohortNode, start, end).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
      start.setParent(node);
      cohortNode.setParent(node);
      end.setParent(node);
      startPos = 3;
    }
    else if (ctx.start_cohort == null && ctx.end_cohort != null) {
      final EndNode end = new EndNode();
      final LastMentionNode lm = new LastMentionNode();
      lm.addChild(getSubTree(ctx.end_cohort).get(0));
      end.addChild(lm);
      final StartNode start = new StartNode();
      final FirstMentionNode fm = new FirstMentionNode();
      fm.addChild(new TimelineNode());
      start.addChild(fm);
      final LanguageNode cohort = getSubTree(ctx.cohort).get(0);
      node = (CsvNode) new CsvNode(cohort, start, end).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
      start.setParent(node);
      end.setParent(node);
      cohort.setParent(node);
      startPos = 2;
    }
    else {
      final LanguageNode cohort = getSubTree(ctx.cohort).get(0);
      node = (CsvNode) new CsvNode(cohort).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
      cohort.setParent(node);
      startPos = 1;
    }
    int termPos = 0;
    for (int x = startPos; x < ctx.main_command().size(); x++) {
      String text = null;
      if (termPos < ctx.TERM().size() && ctx.TERM(termPos).getSymbol().getStartIndex() <= ctx.main_command(x).getStart().getStartIndex()) {
        text = TextNode.trimString(ctx.TERM(termPos).getText()).toUpperCase();
        termPos++;
      }
      node.addChild(getSubTree(ctx.main_command(x)).iterator().next(), text);
    }

    return node;
  }

  @Override
  public LanguageNode visitAnyVitalsCommand(final AnyVitalsCommandContext ctx) {
    recordCommandPosition(ctx);
    return new AnyVitalsNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitAnyVisitTypeCommand(final AnyVisitTypeCommandContext ctx) {
    recordCommandPosition(ctx);
    return new AnyVisitTypeNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitAnyLabsCommand(final AnyLabsCommandContext ctx) {
    recordCommandPosition(ctx);
    return new AnyLabsNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitAnySnomedCommand(final AnySnomedCommandContext ctx) {
    recordCommandPosition(ctx);
    return new AnySnomedNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitHasAnyRxNormCommand(final HasAnyRxNormCommandContext ctx) {
    recordCommandPosition(ctx);
    return new AnyRxNormNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitHasAnyCptCommand(final HasAnyCptCommandContext ctx) {
    recordCommandPosition(ctx);
    return new HasAnyCptNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitHasAnyIcd10Codes(final HasAnyIcd10CodesContext ctx) {
    recordCommandPosition(ctx);
    return new HasAnyIcd10Node().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitHasAnyIcd9Codes(final HasAnyIcd9CodesContext ctx) {
    recordCommandPosition(ctx);
    return new HasAnyIcd9Node().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitDiffCommand(final DiffCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new DiffNode(), ctx);
  }

  @Override
  public LanguageNode visitSameCommand(final SameCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new SameNode(), ctx);
  }

  @Override
  public LanguageNode visitSimpleVitals(final SimpleVitalsContext ctx) {
    recordCommandPosition(ctx);
    return new VitalsNode(ctx.TERM().getText(), Double.MIN_VALUE, Double.MAX_VALUE).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitMergeCommand(final MergeCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new MergeNode(), ctx);
  }

  @Override
  public LanguageNode visitIntersectCommand(final IntersectCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new IntersectNode(), ctx);
  }

  @Override
  public LanguageNode visitIntersectingAny(final IntersectingAnyContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final UnionNode union = new UnionNode();
    for (int x = 1; x < child.getChildren().size(); x++) {
      union.addChild(child.getChildren().get(x));
    }

    final BeforeNode before = (BeforeNode) new BeforeNode(true, false).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(child.getChildren().get(0));
    before.addChild(union);
    return before;
  }

  @Override
  public LanguageNode visitFindCommand(final FindCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final BeforeNode before = (BeforeNode) new BeforeNode(true, false).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(child.getChildren().get(0));
    before.addChild(child.getChildren().get(1));
    return before;
  }

  @Override
  public LanguageNode visitEach_name(final Each_nameContext ctx) {
    recordCommandPosition(ctx);
    return new EachNodeAtom(ctx.getText());
  }

  @Override
  public LanguageNode visitNeverIntersectCommand(final NeverIntersectCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(child.getChildren().get(1));

    final NotNode not = new NotNode();
    not.addChild(before);

    final IntersectNode result = new IntersectNode();
    result.addChild(not);
    result.addChild(before);

    return result;
  }

  @Override
  public LanguageNode visitNeverIntersectManyCommand(final NeverIntersectManyCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final UnionNode union = new UnionNode();

    for (int x = 1; x < child.getChildren().size(); x++) {
      final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
          ctx.getStop().getLine());
      before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
      before.addChild(child.getChildren().get(x));
      before.addChild(child.getChildren().get(0));
      union.addChild(before);
    }

    final NotNode not = new NotNode();
    not.addChild(union);

    final IntersectNode result = new IntersectNode();
    result.addChild(not);
    result.addChild(child.getChildren().get(0));

    return result;
  }

  @Override
  public LanguageNode visitNonIntersectAllCommand(final NonIntersectAllCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final IntersectNode intersect = new IntersectNode();

    for (int x = 1; x < child.getChildren().size(); x++) {
      final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
          ctx.getStop().getLine());
      before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
      before.addChild(child.getChildren().get(x));
      before.addChild(child.getChildren().get(0));
      intersect.addChild(before);
    }

    final InvertNode invert = new InvertNode();
    invert.addChild(intersect);

    final IntersectNode result = new IntersectNode();
    result.addChild(invert);
    result.addChild(child.getChildren().get(0));

    return result;
  }

  @Override
  public LanguageNode visitEachNodeAssign(final EachNodeAssignContext ctx) {
    recordCommandPosition(ctx);
    final String name = ctx.each_name().getText();
    final LanguageNodeWithChildren parent = visitSubTree(new IntersectNode(), ctx);
    return new EachNodeNamedCommand(name, ctx.getText(), parent.getChildren().get(1));
  }

  @Override
  public LanguageNode visitEachCommand(final EachCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new EachNode(ctx.each_name().getText(), visitSubTree(new IntersectNode(), ctx.main_command()).getChildren().get(0)), ctx);
    //node.getChildren().remove(0);
    node.getChildren().remove(1);
    node.getChildren().remove(1);
    return node;
  }

  @Override
  public LanguageNode visitAlwaysIntersectAll(final AlwaysIntersectAllContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final UnionNode intersect = new UnionNode();

    for (int x = 1; x < child.getChildren().size(); x++) {
      final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
          ctx.getStop().getLine());
      before.addParameter(new BeforeNodeParameter(RANGE_TYPE.NOT_IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
      before.addChild(child.getChildren().get(x));
      before.addChild(child.getChildren().get(0));
      intersect.addChild(before);
    }

    final NotNode not = new NotNode();
    not.addChild(intersect);

    final IntersectNode result = new IntersectNode();
    result.addChild(not);
    result.addChild(child.getChildren().get(0));

    return result;
  }

  @Override
  public LanguageNode visitAlwaysIntersectAny(final AlwaysIntersectAnyContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final UnionNode union = new UnionNode();
    final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.NOT_IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(union);
    before.addChild(child.getChildren().get(0));

    for (int x = 1; x < child.getChildren().size(); x++) {
      union.addChild(child.getChildren().get(x));
    }

    final NotNode not = new NotNode();
    not.addChild(before);

    final IntersectNode result = new IntersectNode();
    result.addChild(not);
    result.addChild(child.getChildren().get(0));

    return result;
  }

  @Override
  public LanguageNode visitAlwaysIntersect(final AlwaysIntersectContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.NOT_IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(child.getChildren().get(1));
    before.addChild(child.getChildren().get(0));

    final NotNode not = new NotNode();
    not.addChild(before);

    final IntersectNode result = new IntersectNode();
    result.addChild(not);
    result.addChild(child.getChildren().get(0));

    return result;
  }

  @Override
  public LanguageNode visitNonIntersectManyCommand(final NonIntersectManyCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final UnionNode union = new UnionNode();
    final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.NOT_IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(union);
    before.addChild(child.getChildren().get(0));

    for (int x = 1; x < child.getChildren().size(); x++) {
      union.addChild(child.getChildren().get(x));
    }

    return before;
  }

  @Override
  public LanguageNode visitNonIntersectCommand(final NonIntersectCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.NOT_IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(child.getChildren().get(1));
    before.addChild(child.getChildren().get(0));

    return before;
  }

  @Override
  public LanguageNode visitNeverIntersectAllCommand(final NeverIntersectAllCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final IntersectNode intersect = new IntersectNode();

    for (int x = 1; x < child.getChildren().size(); x++) {
      final BeforeNode before = (BeforeNode) new BeforeNode(false, true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
          ctx.getStop().getLine());
      before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
      before.addChild(child.getChildren().get(x));
      before.addChild(child.getChildren().get(0));
      intersect.addChild(before);
    }

    final NotNode not = new NotNode();
    not.addChild(intersect);

    final IntersectNode result = new IntersectNode();
    result.addChild(not);
    result.addChild(child.getChildren().get(0));

    return result;
  }

  @Override
  public LanguageNode visitOriginalCommand(final OriginalCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(PrimaryNode.createOriginal(), ctx);
  }

  @Override
  public LanguageNode visitOriginalPrimaryCommand(final OriginalPrimaryCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(PrimaryNode.createPrimaryOriginal(), ctx);
  }

  @Override
  public LanguageNode visitPrimaryCommand(final PrimaryCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(PrimaryNode.createPrimary(), ctx);
  }

  @Override
  public LanguageNode visitExtendByCommand(final ExtendByCommandContext ctx) {
    recordCommandPosition(ctx);
    final Object[] start = parseTime(ctx.start_time().getText());
    final Object[] end = parseTime(ctx.end_time().getText());

    final int minValue = Common.timeValueToTimeInMinutes(start[1].toString(), ctx.start_time_modifier == null ? null : ctx.start_time_modifier.getText());
    final int maxValue = Common.timeValueToTimeInMinutes(end[1].toString(), ctx.end_time_modifier == null ? null : ctx.end_time_modifier.getText());

    final ExtendByNode node = new ExtendByNode((RANGE_REFERENCE) start[0], (RANGE_REFERENCE) end[0], minValue, maxValue);
    return visitSubTree(node, ctx);
  }

  @Override
  public LanguageNode visitUnionCommand(final UnionCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new UnionNode(), ctx);
  }

  @Override
  public LanguageNode visitEncountersCommand(final EncountersCommandContext ctx) {
    recordCommandPosition(ctx);
    return EncountersNode.getInstance();
  }

  @Override
  public LanguageNode visitTimelineCommand(final TimelineCommandContext ctx) {
    recordCommandPosition(ctx);
    return new TimelineNode();
  }

  @Override
  public LanguageNode visitStartCommand(final StartCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new StartNode(), ctx);
  }

  @Override
  public LanguageNode visitDurationSimpleCommand(final DurationSimpleCommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.startTime.getText(), ctx.start_time_modifier == null ? null : ctx.start_time_modifier.getText());
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.endTime.getText(), ctx.end_time_modifier == null ? null : ctx.end_time_modifier.getText());
    final DurationNode duration = new DurationNode(ctx.type.getText().equalsIgnoreCase("SINGLE"), minValue, maxValue);
    return visitSubTree(duration, ctx);
  }

  @Override
  public LanguageNode visitCountSimpleCommand(final CountSimpleCommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.startCount.getText(), null);
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.endCount.getText(), null);
    final CountNode duration = new CountNode(false, minValue, maxValue);
    return visitSubTree(duration, ctx);
  }

  @Override
  public LanguageNode visitEachNodeSave(final EachNodeSaveContext ctx) {
    recordCommandPosition(ctx);
    final IntersectNode intersect = new IntersectNode();
    visitSubTree(intersect, ctx);
    return new EachNodeReturnNode(intersect.getChildren().get(0), ctx.each_name().getText());
  }

  @Override
  public LanguageNode visitEqualCommand(final EqualCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new EqualNode(), ctx);
  }

  @Override
  public LanguageNode visitCountComplexCommand(final CountComplexCommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.startCount.getText(), null);
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.endCount.getText(), null);
    final CountNode duration = new CountNode(ctx.type.getText().equalsIgnoreCase("SINGLE"), minValue, maxValue);
    return visitSubTree(duration, ctx);
  }

  @Override
  public LanguageNode visitCountComplex2Command(final CountComplex2CommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.startCount.getText(), null);
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.endCount.getText(), null);
    final boolean hasAsteriskLeft = ctx.before_atom_left().getText().trim().endsWith("*");
    final boolean hasAsteriskRight = ctx.before_atom_right().getText().trim().endsWith("*");
    final CountNode duration = new CountNode(ctx.type.getText().equalsIgnoreCase("SINGLE"), minValue, maxValue, hasAsteriskLeft, hasAsteriskRight);
    return visitSubTree(duration, ctx);
  }

  @Override
  public LanguageNode visitDurationComplex2Command(final DurationComplex2CommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.startTime.getText(), ctx.start_time_modifier == null ? null : ctx.start_time_modifier.getText());
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.endTime.getText(), ctx.end_time_modifier == null ? null : ctx.end_time_modifier.getText());
    final boolean hasAsteriskLeft = ctx.before_atom_left().getText().trim().endsWith("*");
    final boolean hasAsteriskRight = ctx.before_atom_right().getText().trim().endsWith("*");
    final DurationNode duration = new DurationNode(ctx.type.getText().equalsIgnoreCase("SINGLE"), minValue, maxValue, hasAsteriskLeft, hasAsteriskRight);
    return visitSubTree(duration, ctx);
  }

  @Override
  public LanguageNode visitDurationComplexCommand(final DurationComplexCommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.startTime.getText(), ctx.start_time_modifier == null ? null : ctx.start_time_modifier.getText());
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.endTime.getText(), ctx.end_time_modifier == null ? null : ctx.end_time_modifier.getText());
    final DurationNode duration = new DurationNode(ctx.type.getText().equalsIgnoreCase("SINGLE"), minValue, maxValue);
    return visitSubTree(duration, ctx);
  }

  @Override
  public LanguageNode visitYearCommand(final YearCommandContext ctx) {
    recordCommandPosition(ctx);
    int start = Integer.MIN_VALUE;
    int end = Integer.MAX_VALUE;
    try {
      start = Integer.parseInt(ctx.start.getText());
    }
    catch (final Exception e) {
      // use default
    }
    try {
      end = Integer.parseInt(ctx.end.getText());
    }
    catch (final Exception e) {
      // use default
    }
    return new YearNode(start, end);
  }

  @Override
  public LanguageNode visitInvertCommand(final InvertCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new InvertNode(), ctx);
  }

  @Override
  public LanguageNode visitEventFlowCommand(final EventFlowCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new EventFlowNode(), ctx);
  }

  @Override
  public LanguageNode visitDeathCommand(final DeathCommandContext ctx) {
    recordCommandPosition(ctx);
    return new DeathNode().setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitVisitTypeCommand(final VisitTypeCommandContext ctx) {
    recordCommandPosition(ctx);
    return new VisitTypeNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitIntervalPairwiseCommand(final IntervalPairwiseCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree((LanguageNodeWithChildren) new IntervalNode(true).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine()), ctx);
  }

  @Override
  public LanguageNode visitIntervalCommand(final IntervalCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree((LanguageNodeWithChildren) new IntervalNode(false).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine()), ctx);
  }

  @Override
  public LanguageNode visitEndCommand(final EndCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new EndNode(), ctx);
  }

  @Override
  public LanguageNode visitContainsCommand(final ContainsCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);

    final BeforeNode before = (BeforeNode) new BeforeNode(ctx.end_type != null && ctx.end_type.getText().equals("*"), ctx.start_type != null && ctx.start_type.getText().equals(
        "*")).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    before.addParameter(new BeforeNodeParameter(RANGE_TYPE.IN_START_END, RANGE_REFERENCE.START, RANGE_REFERENCE.END, 0, 0, false));
    before.addChild(child.getChildren().get(1));
    before.addChild(child.getChildren().get(0));
    return before;
  }

  @Override
  public LanguageNode visitBeforeNegative(final BeforeNegativeContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx.before_atom_right());
    boolean hasAsterisk = ctx.before_atom_right().getText().trim().endsWith("*");
    final TimeNode time = (TimeNode) new TimeNode(new LanguageNodeWithData(node.getChildren().get(0), hasAsterisk ? "*" : null)).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    for (int x = 0; x < ctx.before_atom_left_no_asterisk().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left_no_asterisk().get(x));
      hasAsterisk = ctx.before_atom_left_no_asterisk().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null), true);
    }

    if (ctx.command_type.getText().equals("AFTER")) {
      time.setTime(Integer.MAX_VALUE, false);
    }
    else {
      time.setTime(Integer.MIN_VALUE, false);
    }

    return time;
  }

  @Override
  public LanguageNode visitBeforeNegativeTime(final BeforeNegativeTimeContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx.before_atom_right());
    boolean hasAsterisk = ctx.before_atom_right().getText().trim().endsWith("*");
    final TimeNode time = (TimeNode) new TimeNode(new LanguageNodeWithData(node.getChildren().get(0), hasAsterisk ? "*" : null)).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    for (int x = 0; x < ctx.before_atom_left_no_asterisk().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left_no_asterisk().get(x));
      hasAsterisk = ctx.before_atom_left_no_asterisk().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null), true);
    }
    final Object[] start = parseTime(ctx.POSITIVE_RANGE().getText());

    int modifier = -1;
    if (ctx.command_type.getText().equals("AFTER")) {
      modifier = 1;
    }

    time.setTime(Common.timeValueToTimeInMinutes(start[1].toString(), ctx.time_modifier() == null ? null : ctx.time_modifier().getText()) * modifier, ctx.asterisk != null
        && ctx.asterisk.getText().trim().equals("*"));

    return time;
  }

  @Override
  public LanguageNode visitBeforeBothTime(final BeforeBothTimeContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx.before_atom_right());
    boolean hasAsterisk = ctx.before_atom_right().getText().trim().endsWith("*");
    final TimeNode time = (TimeNode) new TimeNode(new LanguageNodeWithData(node.getChildren().get(0), hasAsterisk ? "*" : null)).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    for (int x = 0; x < ctx.before_atom_left_left().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left_left().get(x));
      hasAsterisk = ctx.before_atom_left_left().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null));
    }

    for (int x = 0; x < ctx.before_atom_left_no_asterisk().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left_no_asterisk().get(x));
      hasAsterisk = ctx.before_atom_left_no_asterisk().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null), true);
    }

    final Object[] start = parseTime(ctx.POSITIVE_RANGE().getText());

    int modifier = -1;
    if (ctx.command_type.getText().equals("AFTER")) {
      modifier = 1;
    }

    time.setTime(modifier * Common.timeValueToTimeInMinutes(start[1].toString(), ctx.time_modifier() == null ? null : ctx.time_modifier().getText()), ctx.asterisk != null
        && ctx.asterisk.getText().trim().equals("*"));

    return time;
  }

  @Override
  public LanguageNode visitBeforeBoth(final BeforeBothContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx.before_atom_right());
    boolean hasAsterisk = ctx.before_atom_right().getText().trim().endsWith("*");
    final TimeNode time = (TimeNode) new TimeNode(new LanguageNodeWithData(node.getChildren().get(0), hasAsterisk ? "*" : null)).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    for (int x = 0; x < ctx.before_atom_left_left().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left_left().get(x));
      hasAsterisk = ctx.before_atom_left_left().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null));
    }

    for (int x = 0; x < ctx.before_atom_left_no_asterisk().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left_no_asterisk().get(x));
      hasAsterisk = ctx.before_atom_left_no_asterisk().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null), true);
    }

    if (ctx.command_type.getText().equals("AFTER")) {
      time.setTime(Integer.MAX_VALUE, false);
    }
    else {
      time.setTime(Integer.MIN_VALUE, false);
    }

    return time;
  }

  @Override
  public LanguageNode visitBeforePositiveTime(final BeforePositiveTimeContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx.before_atom_right());
    boolean hasAsterisk = ctx.before_atom_right().getText().trim().endsWith("*");
    final TimeNode time = (TimeNode) new TimeNode(new LanguageNodeWithData(node.getChildren().get(0), hasAsterisk ? "*" : null)).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    for (int x = 0; x < ctx.before_atom_left().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left().get(x));
      hasAsterisk = ctx.before_atom_left().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null));
    }
    final Object[] start = parseTime(ctx.POSITIVE_RANGE().getText());

    int modifier = -1;
    if (ctx.command_type.getText().equals("AFTER")) {
      modifier = 1;
    }

    time.setTime(modifier * Common.timeValueToTimeInMinutes(start[1].toString(), ctx.time_modifier() == null ? null : ctx.time_modifier().getText()), ctx.asterisk != null
        && ctx.asterisk.getText().trim().equals("*"));

    return time;
  }

  @Override
  public LanguageNode visitFirstMentionContextCommand(final FirstMentionContextCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx);
    final FirstMentionNode result = new FirstMentionNode();
    result.addChild(node.getChildren().get(0));
    result.setContext(node.getChildren().get(1));
    return result;
  }

  @Override
  public LanguageNode visitLastMentionContextCommand(final LastMentionContextCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx);
    final LastMentionNode result = new LastMentionNode();
    result.addChild(node.getChildren().get(0));
    result.setContext(node.getChildren().get(1));
    return result;
  }

  @Override
  public LanguageNode visitBeforePositive(final BeforePositiveContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren node = visitSubTree(new IntersectNode(), ctx.before_atom_right());
    boolean hasAsterisk = ctx.before_atom_right().getText().trim().endsWith("*");
    final TimeNode time = (TimeNode) new TimeNode(new LanguageNodeWithData(node.getChildren().get(0), hasAsterisk ? "*" : null)).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
    for (int x = 0; x < ctx.before_atom_left().size(); x++) {
      final LanguageNodeWithChildren n = visitSubTree(new IntersectNode(), ctx.before_atom_left().get(x));
      hasAsterisk = ctx.before_atom_left().get(x).getText().trim().endsWith("*");
      time.addChild(new LanguageNodeWithData(n.getChildren().get(0), hasAsterisk ? "*" : null));
    }

    if (ctx.command_type.getText().equals("AFTER")) {
      time.setTime(Integer.MAX_VALUE, false);
    }
    else {
      time.setTime(Integer.MIN_VALUE, false);
    }

    return time;
  }

  @Override
  public LanguageNode visitLabValuesCommand(final LabValuesCommandContext ctx) {
    recordCommandPosition(ctx);
    double min = Double.MIN_VALUE;
    double max = Double.MAX_VALUE;
    try {
      min = Double.parseDouble(ctx.double_value(0).getText());
    }
    catch (final Exception e) {
      // unparseable is set to MIN_VALUE
    }
    try {
      max = Double.parseDouble(ctx.double_value(1).getText());
    }
    catch (final Exception e) {
      // unparseable is set to MAX_VALUE
    }
    return new LabValuesNode(ctx.TERM().getText(), min, max);
  }

  @Override
  public LanguageNode visitHistoryOfCommand(final HistoryOfCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);
    //INTERVAL(START(FIRST MENTION(X)), RECORD END)
    final IntervalNode interval = (IntervalNode) new IntervalNode(false).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    final StartNode start = new StartNode();
    final FirstMentionNode fm = new FirstMentionNode();
    fm.addChild(child.getChildren().get(0));
    start.addChild(fm);
    interval.addChild(start);
    final EndNode end = new EndNode();
    end.addChild(new TimelineNode());
    interval.addChild(end);
    return interval;
  }

  @Override
  public LanguageNode visitNeverHadCommand(final NeverHadCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new NotNode(), ctx);
  }

  @Override
  public LanguageNode visitNoHistoryOfCommand(final NoHistoryOfCommandContext ctx) {
    recordCommandPosition(ctx);
    final LanguageNodeWithChildren child = visitSubTree(new IntersectNode(), ctx);
    //INVERT(HISTORY_OF(X))
    final IntervalNode historyOf = (IntervalNode) new IntervalNode(false).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
    final StartNode start = new StartNode();
    final FirstMentionNode fm = new FirstMentionNode();
    fm.addChild(child.getChildren().get(0));
    start.addChild(fm);
    historyOf.addChild(start);
    final EndNode end = new EndNode();
    end.addChild(new TimelineNode());
    historyOf.addChild(end);
    final InvertNode invert = new InvertNode();
    invert.addChild(historyOf);
    return invert;
  }

  @Override
  public LanguageNode visitFirstMentionCommand(final FirstMentionCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new FirstMentionNode(), ctx);
  }

  @Override
  public LanguageNode visitLastMentionCommand(final LastMentionCommandContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree(new LastMentionNode(), ctx);
  }

  @Override
  public LanguageNode visitRecordStartCommand(final RecordStartCommandContext ctx) {
    recordCommandPosition(ctx);
    final StartNode start = new StartNode();
    start.addChild(new TimelineNode());
    return start;
  }

  @Override
  public LanguageNode visitRecordEndCommand(final RecordEndCommandContext ctx) {
    recordCommandPosition(ctx);
    final EndNode end = new EndNode();
    end.addChild(new TimelineNode());
    return end;
  }

  @Override
  public LanguageNode visitVitalsCommand(final VitalsCommandContext ctx) {
    recordCommandPosition(ctx);
    double min = Double.MIN_VALUE;
    double max = Double.MAX_VALUE;
    try {
      min = Double.parseDouble(ctx.double_value(0).getText());
    }
    catch (final Exception e) {
      // unparseable is set to MIN_VALUE
    }
    try {
      max = Double.parseDouble(ctx.double_value(1).getText());
    }
    catch (final Exception e) {
      // unparseable is set to MAX_VALUE
    }
    return new VitalsNode(ctx.TERM().getText(), min, max).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitComplexLabs(final ComplexLabsContext ctx) {
    recordCommandPosition(ctx);
    return new LabsNode(ctx.TERM(0).getText(), ctx.TERM(1).getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitSimpleLabs(final SimpleLabsContext ctx) {
    recordCommandPosition(ctx);
    return new LabsNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitAgeCommand(final AgeCommandContext ctx) {
    recordCommandPosition(ctx);
    final int minValue = Common.timeValueToTimeInMinutes(ctx.minAge.getText(), ctx.minAgeModifier == null ? null : ctx.minAgeModifier.getText());
    final int maxValue = Common.timeValueToTimeInMinutes(ctx.maxAge.getText(), ctx.maxAgeModifier == null ? null : ctx.maxAgeModifier.getText());

    return new AgeNode(minValue, maxValue);
  }

  @Override
  public LanguageNode visitBeforeModifier(final BeforeModifierContext ctx) {
    recordCommandPosition(ctx);
    final boolean returnType = ctx.return_type != null;
    final RANGE_TYPE rangeType = parseRangeType(ctx);
    if (rangeType == null) {
      throw new UnsupportedOperationException("[" + ctx.getStart().getStartIndex() + "," + ctx.getStop().getStopIndex() + ":" + ctx.getStart().getLine() + ","
          + ctx.getStop().getLine() + "] " + "BEFORE command parameters must start with '+' or '-'");
    }
    final Object[] start = parseTime(ctx.start_time().getText());
    final Object[] end = parseTime(ctx.end_time().getText());
    start[1] = Common.timeValueToTimeInMinutes(start[1].toString(), ctx.startModifier == null ? null : ctx.startModifier.getText());
    end[1] = Common.timeValueToTimeInMinutes(end[1].toString(), ctx.endModifier == null ? null : ctx.endModifier.getText());
    ((BeforeNode) stack.get(stack.size() - 1)).addParameter(new BeforeNodeParameter(rangeType, (RANGE_REFERENCE) start[0], (RANGE_REFERENCE) end[0], (Integer) start[1],
        (Integer) end[1], returnType));
    return null;
  }

  private Object[] parseTime(final String time) {
    final Object[] result = new Object[2];
    if (time.equals("START")) {
      result[0] = RANGE_REFERENCE.START;
      result[1] = 0;
    }
    else if (time.equals("END")) {
      result[0] = RANGE_REFERENCE.END;
      result[1] = 0;
    }
    else if (time.indexOf("START") != -1) {
      result[0] = RANGE_REFERENCE.START;
      result[1] = Common.parseInt(time.substring(time.indexOf("START") + 5));
    }
    else if (time.indexOf("END") != -1) {
      result[0] = RANGE_REFERENCE.END;
      result[1] = Common.parseInt(time.substring(time.indexOf("END") + 3));
    }
    else {
      result[0] = RANGE_REFERENCE.NUMERIC;
      result[1] = time.equals("MAX") ? Integer.MAX_VALUE : time.equals("MIN") ? Integer.MIN_VALUE : Common.parseInt(time);
    }
    return result;
  }

  private RANGE_TYPE parseRangeType(final BeforeModifierContext ctx) {
    recordCommandPosition(ctx);
    RANGE_TYPE type = null;
    if (ctx.in_type != null) {
      if (ctx.in_type.getText().equals("-")) {
        if (ctx.start_type != null) {
          type = ctx.end_type == null ? RANGE_TYPE.NOT_IN_START : RANGE_TYPE.NOT_IN_START_END;
        }
        else {
          type = ctx.end_type != null ? RANGE_TYPE.NOT_IN_END : RANGE_TYPE.NOT_IN;
        }
      }
      else if (ctx.in_type.getText().equals("+")) {
        if (ctx.start_type != null) {
          type = ctx.end_type == null ? RANGE_TYPE.IN_START : RANGE_TYPE.IN_START_END;
        }
        else {
          type = ctx.end_type != null ? RANGE_TYPE.IN_END : RANGE_TYPE.IN;
        }
      }
      else {
        return null;
      }
    }
    return type;
  }

  @Override
  public LanguageNode visitIdenticalCommand(final IdenticalCommandContext ctx) {
    recordCommandPosition(ctx);
    final IdenticalNode result = new IdenticalNode();
    final LanguageNodeWithChildren temp = visitSubTree(new IntersectNode(), ctx);
    final LanguageNode child1 = temp.getChildren().get(0);
    final LanguageNode child2 = temp.getChildren().get(1);
    result.addChild(child1);
    result.addChild(child2);
    return result;
  }

  @Override
  public LanguageNode visitNullCommand(final NullCommandContext ctx) {
    recordCommandPosition(ctx);
    return new NullNode();
  }

  @Override
  public LanguageNode visitAtcCommand(final AtcCommandContext ctx) {
    recordCommandPosition(ctx);
    return new AtcNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitNoteTypeCommand(final NoteTypeCommandContext ctx) {
    recordCommandPosition(ctx);
    return new NoteTypeNode(ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitPatientsCommand(final PatientsCommandContext ctx) {
    recordCommandPosition(ctx);
    final Iterator<TerminalNode> i = ctx.POSITIVE_RANGE().iterator();
    final ArrayList<String> patients = new ArrayList<>();
    while (i.hasNext()) {
      patients.add(i.next().getText());
    }
    return new PatientsNode(patients);
  }

  @Override
  public LanguageNode visitPatientsSingleCommand(final PatientsSingleCommandContext ctx) {
    recordCommandPosition(ctx);
    final ArrayList<String> patients = new ArrayList<>();
    patients.add(ctx.POSITIVE_RANGE().getText());
    return new PatientsNode(patients);
  }

  @Override
  public LanguageNode visitNumericInterval(final NumericIntervalContext ctx) {
    recordCommandPosition(ctx);
    final Object[] start = parseTime(ctx.startTimeValue.getText());
    final Object[] end = parseTime(ctx.endTimeValue.getText());
    start[1] = Common.timeValueToTimeInMinutes(start[1].toString(), ctx.startTimeModifier == null ? null : ctx.startTimeModifier.getText());
    end[1] = Common.timeValueToTimeInMinutes(end[1].toString(), ctx.endTimeModifier == null ? null : ctx.endTimeModifier.getText());
    return new IntervalNumericNode((Integer) start[1], (Integer) end[1]);
  }

  @Override
  public LanguageNode visitBeforeCommandAtom(final BeforeCommandAtomContext ctx) {
    recordCommandPosition(ctx);
    return visitSubTree((LanguageNodeWithChildren) new BeforeNode(ctx.start_type != null, ctx.end_type != null).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(),
        ctx.getStart().getLine(), ctx.getStop().getLine()), ctx);
  }

  @Override
  public LanguageNode visitDrugsSimple(final DrugsSimpleContext ctx) {
    recordCommandPosition(ctx);
    return new RxNormNode(Integer.parseInt(ctx.POSITIVE_RANGE().getText())).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(), ctx.getStart().getLine(),
        ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitDrugsRoute(final DrugsRouteContext ctx) {
    recordCommandPosition(ctx);
    return new DrugNode(Integer.parseInt(ctx.POSITIVE_RANGE().getText()), ctx.TERM().getText(), null).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(),
        ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitDrugsStatus(final DrugsStatusContext ctx) {
    recordCommandPosition(ctx);
    return new DrugNode(Integer.parseInt(ctx.POSITIVE_RANGE().getText()), null, ctx.TERM().getText()).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(),
        ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitDrugsRouteStatus(final DrugsRouteStatusContext ctx) {
    recordCommandPosition(ctx);
    return new DrugNode(Integer.parseInt(ctx.POSITIVE_RANGE().getText()), ctx.TERM().get(0).getText(), ctx.TERM().get(1).getText()).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitDrugsStatusRoute(final DrugsStatusRouteContext ctx) {
    recordCommandPosition(ctx);
    return new DrugNode(Integer.parseInt(ctx.POSITIVE_RANGE().getText()), ctx.TERM().get(1).getText(), ctx.TERM().get(0).getText()).setPosition(ctx.start.getStartIndex(),
        ctx.stop.getStopIndex(), ctx.getStart().getLine(), ctx.getStop().getLine());
  }

  @Override
  public LanguageNode visitSnomedCommand(final SnomedCommandContext ctx) {
    recordCommandPosition(ctx);
    if (ctx.POSITIVE_RANGE() == null) {
      throw new NumberFormatException();
    }
    return new SnomedNode(ctx.POSITIVE_RANGE().getText());
  }

  @Override
  public LanguageNode visitBeforeCommandAtom2(final BeforeCommandAtom2Context ctx) {
    recordCommandPosition(ctx);
    return visitSubTree((LanguageNodeWithChildren) new BeforeNode(ctx.start_type != null, ctx.end_type != null).setPosition(ctx.start.getStartIndex(), ctx.stop.getStopIndex(),
        ctx.getStart().getLine(), ctx.getStop().getLine()), ctx);
  }

  public int getLastCommandVisitedPosition() {
    return lastCommandVisitedPosition;
  }

  public static void main(final String[] args) {
    LanguageParser.parse("SNOMED=12312");
  }
}