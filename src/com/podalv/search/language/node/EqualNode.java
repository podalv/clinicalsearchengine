package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class EqualNode extends LanguageNodeWithAndChildren implements Cloneable {

  private final IntArrayList result = new IntArrayList();

  public static PayloadIterator equal(final IntArrayList result, final PatientSearchModel patient, final PayloadIterator i1, final PayloadIterator i2) {
    result.clear();
    boolean move1 = true;
    boolean move2 = true;
    boolean finished1 = false;
    boolean finished2 = false;
    while (!finished1 || !finished2) {
      if (move1) {
        if (i1.hasNext()) {
          i1.next();
        }
        else {
          finished1 = true;
        }
      }
      if (move2) {
        if (i2.hasNext()) {
          i2.next();
        }
        else {
          finished2 = true;
        }
      }
      move1 = false;
      move2 = false;
      if (finished1 || finished2) {
        break;
      }
      // overlap
      if ((i1.getStartId() == i2.getStartId() && i2.getEndId() == i1.getEndId())) {
        result.add(i1.getStartId());
        result.add(i1.getEndId());
        move1 = true;
        move2 = true;
      }
      else if (i1.getStartId() < i2.getEndId()) {
        move1 = true;
      }
      else {
        move2 = true;
      }
    }

    return new EmptyPayloadIterator(((IntArrayList) result.clone()).iterator());
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult ch1 = children.get(0).evaluate(context, patient);
    final EvaluationResult ch2 = children.get(1).evaluate(context, patient);
    if (ch1 instanceof AbortedResult || ch2 instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    equal(result, patient, ch1.toTimeIntervals(patient).iterator(patient), ch2.toTimeIntervals(patient).iterator(patient));
    return new TimeIntervals(this, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    final EqualNode result = new EqualNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("EQUAL(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

}
