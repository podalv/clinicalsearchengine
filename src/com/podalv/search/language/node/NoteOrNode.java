package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.NotesIterator;

public class NoteOrNode extends LanguageNodeWithOrChildren {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    NotesIterator result = null;
    final IntArrayList temp = new IntArrayList();
    for (int x = 0; x < children.size(); x++) {
      if (x == 0) {
        final EvaluationResult c = children.get(x).evaluate(context, patient);
        if (c instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        result = (NotesIterator) ((TimeIntervals) c).iterator(patient);
      }
      else {
        final EvaluationResult c = children.get(x).evaluate(context, patient);
        if (c instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        result = NoteNode.union(patient, new IntArrayList(), result, (NotesIterator) ((TimeIntervals) c).iterator(patient));
      }
    }

    temp.clear();
    if (result != null) {
      while (result.hasNext()) {
        result.next();
        temp.add(result.getPayloadId());
      }
    }

    return new PayloadPointers(TYPE.NOTE_RAW, this, patient, temp);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    final NoteOrNode result = new NoteOrNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("OR(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

}
