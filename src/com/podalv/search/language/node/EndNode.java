package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class EndNode extends LanguageNodeWithAndChildren {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final IntArrayList result = new IntArrayList();
    final EvaluationResult res = children.get(0).evaluate(context, patient);
    if (res instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator child = res.toTimeIntervals(patient).iterator(patient);
    while (child.hasNext()) {
      child.next();
      result.add(child.getEndId());
      result.add(child.getEndId());
    }
    return (result.size() == 0) ? TimeIntervals.getEmptyTimeLine() : new TimeIntervals(this, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    final EndNode result = new EndNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("END(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

}