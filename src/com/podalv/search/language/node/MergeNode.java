package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;

/** Returns true if either the first or second cohorts are true
 *
 * @author podalv
 *
 */
public class MergeNode extends LanguageNodeWithOrChildren {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult ch1 = children.get(0).evaluate(context, patient);
    final EvaluationResult ch2 = children.get(1).evaluate(context, patient);
    if (ch1 instanceof AbortedResult || ch2 instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    return BooleanResult.getInstance(ch1.toBooleanResult().result() || ch2.toBooleanResult().result());
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public LanguageNode copy() {
    final MergeNode result = new MergeNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("MERGE(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

}