package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AtomIterablePreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.AtomicNode;

public class RaceNode extends LanguageNode implements AtomicNode {

  private final String race;
  private int          raceId = -1;

  public RaceNode(final String race) {
    this.race = TextNode.trimString(race).toUpperCase();
  }

  private void processRaceId(final IndexCollection context) {
    if (raceId == -1) {
      raceId = context.getDemographics().getRace().getId(race);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    processRaceId(context);
    return BooleanResult.getInstance(context.getDemographics().containsRace(patient.getId(), raceId));
  }

  @Override
  public LanguageNode copy() {
    return new RaceNode(race);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public String toString() {
    return "RACE=\"" + race + "\"";
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    processRaceId(context);
    return new AtomIterablePreprocessingNode(context.getRaceIterator(raceId));
  }

  @Override
  public int hashCode() {
    return race.hashCode() * 47;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof RaceNode && ((RaceNode) obj).race.equals(race);
  }
}
