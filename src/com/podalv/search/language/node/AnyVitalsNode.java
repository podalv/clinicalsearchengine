package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.CsvWriter;
import com.podalv.search.datastructures.MeasurementOffHeapData;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class AnyVitalsNode extends LanguageNode implements AtomicNode, CsvNodeType {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(!patient.getVitals().isEmpty());
    }
    return new TimeIntervals(this, patient.getVitals().getAllTimes());
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new AnyVitalsNode();
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "VITALS" : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithVitalsCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "VITALS information is missing in this dataset", this.getClass().getName(), "VITALS");
    }
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final String nodeName = toString();
    patient.getVitals().extract(new CsvWriter() {

      String prevName = null;

      @Override
      public void write(final String name, final int time, final double value) {
        int evaluatedStartEndIntervalsPos = 0;
        if (prevName == null || !prevName.equals(name)) {
          evaluatedStartEndIntervalsPos = 0;
          prevName = name;
        }
        if (MeasurementOffHeapData.isEventInvalid(time)) {
          result.append(patient.getId() + separator + nodeName + "=" + name + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator + "1"
              + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
          return;
        }
        if (evaluatedStartEndIntervals == null) {
          result.append(patient.getId() + separator + nodeName + "=" + name + separator + CsvNode.getYear(patient, time) + separator + value + separator + "" + separator + "1"
              + separator + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
        }
        else {
          for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
            if (BeforeNode.intersect(time, time, evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
              result.append(patient.getId() + separator + nodeName + "=" + name + separator + CsvNode.getYear(patient, time) + separator + value + separator + "" + separator + "1"
                  + separator + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
              evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
            }
          }
        }
      }
    }, context);
    return result.toString();
  }

  @Override
  public int hashCode() {
    return 353;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AnyVitalsNode;
  }
}