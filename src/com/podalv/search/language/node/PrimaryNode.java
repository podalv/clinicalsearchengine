package com.podalv.search.language.node;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.Icd9Iterator;

public class PrimaryNode extends LanguageNodeWithAndChildren {

  private final boolean primary;
  private final boolean original;

  public static PrimaryNode createPrimary() {
    return new PrimaryNode(true, false);
  }

  public static PrimaryNode createOriginal() {
    return new PrimaryNode(false, true);
  }

  public static PrimaryNode createPrimaryOriginal() {
    return new PrimaryNode(true, true);
  }

  private PrimaryNode(final boolean primary, final boolean original) {
    this.primary = primary;
    this.original = original;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (children.get(0) instanceof HasIcd9Node) {
      return evaluateIcd9(context, patient);
    }
    else {
      return evaluateIcd10(context, patient);
    }
  }

  private EvaluationResult evaluateIcd9(final IndexCollection context, final PatientSearchModel patient) {
    final int icd9Index = ((HasIcd9Node) children.get(0)).getIcd9Index();
    if (icd9Index < 0) {
      return new PayloadPointers(TYPE.ICD9_RAW_PRIMARY, this, patient, new IntArrayList());
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      final IntArrayList payloads = patient.getIcd9(icd9Index);
      if (PayloadPointers.containsInvalidEvents(payloads)) {
        return AbortedResult.getInstance();
      }
      Icd9Iterator p;
      if (primary && !original) {
        p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW_PRIMARY, this, patient, payloads).iterator(patient);
      }
      else if (!primary && original) {
        p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW_ORIGINAL, this, patient, payloads).iterator(patient);
      }
      else if (primary && original) {
        p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW_ORIGINAL_PRIMARY, this, patient, payloads).iterator(patient);
      }
      else {
        return new ErrorResult(this, "Incorrect parameters for primary node '" + primary + "' / '" + original + "'");
      }
      return BooleanResult.getInstance(p.isEligible());
    }
    else {
      final IntArrayList result = new IntArrayList();
      final IntArrayList payloads = patient.getIcd9(icd9Index);
      if (PayloadPointers.containsInvalidEvents(payloads)) {
        return AbortedResult.getInstance();
      }
      final Icd9Iterator p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW, this, patient, patient.getIcd9(icd9Index)).iterator(patient);
      while (p.hasNext()) {
        p.next();
        if (p.isPrimary()) {
          result.add(p.getStartId());
          result.add(p.getEndId());
        }
      }
      if (result.size() == 0) {
        return TimeIntervals.getEmptyTimeLine();
      }
      return new TimeIntervals(this, result);
    }
  }

  private EvaluationResult evaluateIcd10(final IndexCollection context, final PatientSearchModel patient) {
    final int icd10Index = ((HasIcd10Node) children.get(0)).getIcd10Index();
    if (icd10Index < 0) {
      return new PayloadPointers(TYPE.ICD9_RAW_PRIMARY, this, patient, new IntArrayList());
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      final IntArrayList payloads = patient.getIcd10(icd10Index);
      if (PayloadPointers.containsInvalidEvents(payloads)) {
        return AbortedResult.getInstance();
      }
      Icd9Iterator p;
      if (primary && !original) {
        p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW_PRIMARY, this, patient, payloads).iterator(patient);
      }
      else if (!primary && original) {
        p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW_ORIGINAL, this, patient, payloads).iterator(patient);
      }
      else if (primary && original) {
        p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW_ORIGINAL_PRIMARY, this, patient, payloads).iterator(patient);
      }
      else {
        return new ErrorResult(this, "Incorrect parameters for primary node '" + primary + "' / '" + original + "'");
      }
      return BooleanResult.getInstance(p.isEligible());
    }
    else {
      final IntArrayList result = new IntArrayList();
      final IntArrayList payloads = patient.getIcd10(icd10Index);
      if (PayloadPointers.containsInvalidEvents(payloads)) {
        return AbortedResult.getInstance();
      }
      final Icd9Iterator p = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW, this, patient, payloads).iterator(patient);
      while (p.hasNext()) {
        p.next();
        if (p.isPrimary()) {
          result.add(p.getStartId());
          result.add(p.getEndId());
        }
      }
      if (result.size() == 0) {
        return TimeIntervals.getEmptyTimeLine();
      }
      return new TimeIntervals(this, result);
    }
  }

  @Override
  public LanguageNode copy() {
    final PrimaryNode result = new PrimaryNode(primary, original);
    result.children = cloneChildren();
    return result;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    if (primary && !original) {
      return "PRIMARY(" + children.get(0).toString() + ")";
    }
    else if (original && !primary) {
      return "ORIGINAL(" + children.get(0).toString() + ")";
    }
    else if (original && primary) {
      return "PRIMARY(ORIGINAL(" + children.get(0).toString() + "))";
    }
    throw new UnsupportedOperationException("Primary command with this configuration of '" + primary + "' / '" + original + "' is not supported");
  }

}
