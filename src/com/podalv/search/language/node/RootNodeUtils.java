package com.podalv.search.language.node;

import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import com.podalv.search.language.each.EachNode;
import com.podalv.search.language.each.EachNodeAtom;
import com.podalv.search.language.each.EachNodeReturnNode;

public class RootNodeUtils {

  public static final Predicate<LanguageNode> csvTest       = new Predicate<LanguageNode>() {

                                                              @Override
                                                              public boolean test(final LanguageNode t) {
                                                                return t instanceof CsvNode;
                                                              };
                                                            };
  public static final Predicate<LanguageNode> dumpTest      = new Predicate<LanguageNode>() {

                                                              @Override
                                                              public boolean test(final LanguageNode t) {
                                                                return t instanceof DumpNode;
                                                              };
                                                            };
  public static final Predicate<LanguageNode> estimateTest  = new Predicate<LanguageNode>() {

                                                              @Override
                                                              public boolean test(final LanguageNode t) {
                                                                return t instanceof EstimateNode;
                                                              };
                                                            };
  public static final Predicate<LanguageNode> limitTest     = new Predicate<LanguageNode>() {

                                                              @Override
                                                              public boolean test(final LanguageNode t) {
                                                                return t instanceof LimitNode;
                                                              };
                                                            };
  public static final Predicate<LanguageNode> eventFlowTest = new Predicate<LanguageNode>() {

                                                              @Override
                                                              public boolean test(final LanguageNode t) {
                                                                return t instanceof EventFlowNode;
                                                              };
                                                            };

  private RootNodeUtils() {
    // static access only
  }

  public static boolean nodeContainsError(final LanguageNode node) {
    if (node == null) {
      return true;
    }
    if (node instanceof ErrorNode) {
      return true;
    }
    if (node instanceof LanguageNodeWithChildren) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).getChildren().size(); x++) {
        if (nodeContainsError(((LanguageNodeWithChildren) node).getChildren().get(x))) {
          return true;
        }
      }
    }
    return false;
  }

  public static void getAssignedEachNodeAtoms(final HashSet<String> names, final LanguageNode node) {
    if (node instanceof EachNodeReturnNode) {
      names.add(((EachNodeReturnNode) node).getReturnName().toLowerCase());
    }
    if (node instanceof LanguageNodeWithChildren) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).getChildren().size(); x++) {
        getAssignedEachNodeAtoms(names, ((LanguageNodeWithChildren) node).getChildren().get(x));
      }
    }
  }

  static void getEachNodeAtoms(final HashSet<String> names, final LanguageNode node) {
    if (node instanceof EachNodeAtom) {
      names.add(node.toString().toLowerCase());
    }
    if (!(node instanceof EachNode) && node instanceof LanguageNodeWithChildren) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).getChildren().size(); x++) {
        getEachNodeAtoms(names, ((LanguageNodeWithChildren) node).getChildren().get(x));
      }
    }
  }

  static void recordParsingError(final StringBuilder syntaxErrors, final AtomicInteger syntaxErrorCnt, final StringBuilder errorsFound, final String errorMessage) {
    syntaxErrorCnt.set(0);
    syntaxErrors.setLength(0);
    errorsFound.setLength(0);
    errorsFound.append(errorMessage);
  }

  private static boolean isClassIdentical(final LanguageNode node1, final LanguageNode node2) {
    return node1.getClass().getName().equals(node2.getClass().getName());
  }

  private static void propagateNodeToParent(final LanguageNodeWithChildren parent, final LanguageNode child, final String childName) {
    if (isClassIdentical(parent.getParent(), child)) {
      propagateNodeToParent((LanguageNodeWithChildren) parent.getParent(), child, childName);
    }
    else {
      parent.addChild(child, childName);
    }
  }

  private static void simplifyChild(final LanguageNode node) {
    if (node instanceof LanguageNodeWithChildren) {
      final LanguageNodeWithChildren nodewithChildren = (LanguageNodeWithChildren) node;
      for (int x = 0; x < nodewithChildren.children.size(); x++) {
        if (nodewithChildren.children.get(x) instanceof LanguageNodeWithChildren) {
          simplifyChild(nodewithChildren.children.get(x));
        }
        else {
          if (canSimplify(node)) {
            propagateNodeToParent((LanguageNodeWithChildren) node.getParent(), nodewithChildren.children.get(x), nodewithChildren.childrenNames.get(x));
            nodewithChildren.children.remove(x);
            nodewithChildren.childrenNames.remove(x);
            x--;
          }
        }
      }
    }
  }

  private static boolean canSimplify(final LanguageNode node) {
    return ((node instanceof IntersectNode && node.parent != null && node.parent instanceof IntersectNode) || //
        (node instanceof UnionNode && node.parent != null && node.parent instanceof UnionNode) || //
        (node instanceof AndNode && node.parent != null && node.parent instanceof AndNode) || //
        (node instanceof OrNode && node.parent != null && node.parent instanceof OrNode));
  }

  private static boolean canDelete(final LanguageNode node) {
    return ((node instanceof IntersectNode && ((IntersectNode) node).children.size() == 0) || //
        (node instanceof UnionNode && ((UnionNode) node).children.size() == 0) || //
        (node instanceof AndNode && ((AndNode) node).children.size() == 0) || //
        (node instanceof OrNode && ((OrNode) node).children.size() == 0));
  }

  private static void deleteEmptyNodes(final LanguageNode node) {
    if (node instanceof LanguageNodeWithChildren) {
      final LanguageNodeWithChildren nodeWithChildren = (LanguageNodeWithChildren) node;
      for (int x = 0; x < nodeWithChildren.children.size(); x++) {
        if (canDelete(nodeWithChildren.children.get(x))) {
          nodeWithChildren.children.remove(x);
          nodeWithChildren.childrenNames.remove(x);
          x--;
          if (nodeWithChildren.children.size() == 0) {
            deleteEmptyNodes(node.getParent());
          }
        }
        else {
          if (nodeWithChildren.children.get(x) instanceof LanguageNodeWithChildren) {
            deleteEmptyNodes(nodeWithChildren.children.get(x));
          }
        }
      }
    }
  }

  static RootNode simplify(final RootNode node) {
    for (int x = 0; x < node.children.size(); x++) {
      if (node.children.get(x) instanceof LanguageNodeWithChildren) {
        simplifyChild(node.children.get(x));
        deleteEmptyNodes(node.children.get(x));
      }
    }
    return node;
  }

}
