package com.podalv.search.language.node;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class YearNode extends LanguageNode implements AtomicNode {

  private final int minYear;
  private final int maxYear;

  public YearNode(final int minYear, final int maxYear) {
    this.minYear = Math.min(minYear, maxYear);
    this.maxYear = Math.max(maxYear, minYear);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final int[] minMax = patient.getYear(minYear, maxYear);
    return minMax == null ? TimeIntervals.getEmptyTimeLine() : new TimeIntervals(this, new IntArrayList(minMax));
  }

  @Override
  public LanguageNode copy() {
    return new YearNode(minYear, maxYear);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String toString() {
    return "YEAR(" + minYear + ", " + maxYear + ")";
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof YearNode && ((YearNode) obj).minYear == minYear && ((YearNode) obj).maxYear == maxYear;
  }

  @Override
  public int hashCode() {
    return minYear * maxYear * 3;
  }
}
