package com.podalv.search.language.node;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;

public interface CsvNodeType {

  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient, final IntArrayList timeIntervals);

}
