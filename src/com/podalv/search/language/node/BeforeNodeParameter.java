package com.podalv.search.language.node;

import com.podalv.extractor.stride6.Common;
import com.podalv.search.datastructures.Enums.RANGE_REFERENCE;
import com.podalv.search.datastructures.Enums.RANGE_TYPE;

/** Allows specifying ranges and their relationships in regards to the BEFORE command
 *
 * @author podalv
 *
 */
public class BeforeNodeParameter {

  private final RANGE_TYPE      type;
  private final boolean         returnRange;
  private final RANGE_REFERENCE startReference;
  private final RANGE_REFERENCE endReference;
  private final int             startOffset;
  private final int             endOffset;

  public BeforeNodeParameter(final RANGE_TYPE type, final RANGE_REFERENCE startReference, final RANGE_REFERENCE endReference, final int startOffset, final int endOffset,
      final boolean returnRange) {
    this.type = type;
    this.returnRange = returnRange;
    if (startOffset <= endOffset) {
      this.startReference = startReference;
      this.endReference = endReference;
      this.endOffset = endOffset;
      this.startOffset = startOffset;
    }
    else {
      this.startReference = endReference;
      this.endReference = startReference;
      this.endOffset = startOffset;
      this.startOffset = endOffset;
    }
  }

  public RANGE_TYPE getType() {
    return type;
  }

  public int getEndOffset() {
    return endOffset;
  }

  public RANGE_REFERENCE getEndReference() {
    return endReference;
  }

  public int getStartOffset() {
    return startOffset;
  }

  public RANGE_REFERENCE getStartReference() {
    return startReference;
  }

  public boolean returnRange() {
    return returnRange;
  }

  public boolean isRangeTypeIn() {
    return type == RANGE_TYPE.IN || type == RANGE_TYPE.IN_END || type == RANGE_TYPE.IN_START_END || type == RANGE_TYPE.IN_START;
  }

  public static String getOffsetReference(final RANGE_REFERENCE reference) {
    return reference == RANGE_REFERENCE.START ? "START" : reference == RANGE_REFERENCE.END ? "END" : "";
  }

  public static String getOffset(final RANGE_REFERENCE reference, final int offset) {
    if (reference != RANGE_REFERENCE.NUMERIC) {
      if (offset > 0) {
        return "+" + offset;
      }
      else if (offset == 0) {
        return "";
      }
    }
    return Common.languageNodeTimeArgumentToText(offset);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder();
    result.append(isRangeTypeIn() ? '+' : '-');
    if (returnRange) {
      result.append('*');
    }
    if (type == RANGE_TYPE.IN_START_END || type == RANGE_TYPE.IN_START || type == RANGE_TYPE.NOT_IN_START || type == RANGE_TYPE.NOT_IN_START_END) {
      result.append('<');
    }
    if (type == RANGE_TYPE.IN_START_END || type == RANGE_TYPE.IN_END || type == RANGE_TYPE.NOT_IN_END || type == RANGE_TYPE.NOT_IN_START_END) {
      result.append('>');
    }
    result.append("(" + getOffsetReference(startReference) + getOffset(startReference, startOffset) + ", " + getOffsetReference(endReference) + getOffset(endReference, endOffset)
        + ")");
    return result.toString();
  }
}
