package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;

public class VisitTypeNode extends LanguageNode {

  final String text;
  int          tid = Common.UNDEFINED;

  public VisitTypeNode(final String text) {
    this.text = TextNode.trimString(text).toUpperCase();
  }

  private void init(final IndexCollection context) {
    if (tid == -1) {
      tid = context.getVisitType(text);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    init(context);
    if (tid == Common.UNDEFINED) {
      return new PayloadPointers(TYPE.CPT_RAW, this, patient, new IntArrayList());
    }
    final IntArrayList payloads = patient.getVisitTypes(tid);
    if (PayloadPointers.containsInvalidEvents(payloads)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsVisitType(tid));
    }
    return new PayloadPointers(TYPE.CPT_RAW, this, patient, payloads);
  }

  @Override
  public LanguageNode copy() {
    return new VisitTypeNode(text);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithVisitTypeCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "VISIT TYPE information is missing in this dataset", this.getClass().getName(), "VISIT TYPE");
    }
    init(context);
    return new AtomPreprocessingNode(statistics.getVisitTypesPatients(tid));
  }

  @Override
  public String toString() {
    return "VISIT TYPE=\"" + text + "\"";
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof VisitTypeNode) {
      final VisitTypeNode node = (VisitTypeNode) obj;
      return node.text.equals(text);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return text.hashCode() * 7;
  }
}
