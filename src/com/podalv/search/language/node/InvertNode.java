package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_EMPTY;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class InvertNode extends NegatedNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (negatedChildren == null) {
      evaluateChildren();
    }
    if (NegationEvaluation.skipPatient(patient, this)) {
      return AbortedResult.getInstance();
    }
    final IntArrayList result = new IntArrayList();
    final EvaluationResult ch = children.get(0).evaluate(context, patient);
    if (ch instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator iterator = Common.compressIterator(patient, ch.toTimeIntervals(patient).iterator(patient));
    int prevEnd = patient.getStartTime();
    if (iterator.hasNext()) {
      while (iterator.hasNext()) {
        iterator.next();
        if (prevEnd < iterator.getStartId()) {
          result.add(prevEnd);
          result.add(iterator.getStartId() - 1);
        }
        prevEnd = iterator.getEndId() + 1;
      }
      if (prevEnd < patient.getEndTime()) {
        result.add(prevEnd);
        result.add(patient.getEndTime());
      }
    }
    else {
      result.add(patient.getStartTime());
      result.add(patient.getEndTime());
    }
    return new TimeIntervals(this, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("INVERT(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final InvertNode result = new InvertNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasBooleanChildren(children)) {
      result.add(TEMPORAL_COMMAND_EMPTY);
    }
    if (hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_EMPTY);
    }
    if (result.size() == 0 && !isTemporalValid(this)) {
      result.add(ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
    }
    return result.toArray(new String[result.size()]);
  }

}
