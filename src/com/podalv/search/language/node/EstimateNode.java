package com.podalv.search.language.node;

public class EstimateNode extends LimitNode {

  public EstimateNode(final String limitType) {
    super(limitType);
  }

  @Override
  public String toString() {
    return "ESTIMATE(" + limitType + "," + children.get(0).toString() + ")";
  }

}