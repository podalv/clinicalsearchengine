package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;
import com.podalv.search.language.node.base.AtomicNode;

public class HasAnyCptNode extends LanguageNode implements AtomicNode, CsvNodeType {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(!patient.getUniqueCptCodes().isEmpty());
    }
    PayloadIterator result = null;
    PayloadIterator resultList = null;
    final IntArrayList resultArray = new IntArrayList();
    if (patient.getUniqueCptCodes().size() == 1) {
      resultList = new RawPayloadIterator(patient, patient.getCpt(patient.getUniqueCptCodes().get(0)));
      if (resultList.containsInvalidEvent()) {
        return AbortedResult.getInstance();
      }
    }
    else if (patient.getUniqueCptCodes().size() > 0) {
      result = new RawPayloadIterator(patient, patient.getCpt(patient.getUniqueCptCodes().get(0)));
      if (result.containsInvalidEvent()) {
        result = null;
      }
      for (int x = 1; x < patient.getUniqueCptCodes().size(); x++) {
        final RawPayloadIterator i = new RawPayloadIterator(patient, patient.getCpt(patient.getUniqueCptCodes().get(x)));
        if (result == null) {
          if (!i.containsInvalidEvent()) {
            result = i;
          }
        }
        if (i.containsInvalidEvent()) {
          continue;
        }
        resultList = UnionNode.union(resultArray, result, i);
        result = resultList;
      }
    }
    final IntArrayList out = new IntArrayList();
    if (resultList != null) {
      while (resultList.hasNext()) {
        resultList.next();
        out.add(resultList.getStartId());
        out.add(resultList.getEndId());
      }

    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "CPT" : getNodeName();
  }

  @Override
  public LanguageNode copy() {
    return new HasAnyCptNode();
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueCptCodes());
    int evaluatedStartEndIntervalsPos = 0;
    final IntIterator iterator = sortedSet.iterator();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      final IntArrayList cpt = patient.getCpt(id);
      if (PayloadPointers.containsInvalidEvents(cpt)) {
        result.append(patient.getId() + separator + toString() + "=" + context.getCpt(id) + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator
            + separator + "1" + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
        continue;
      }
      for (int x = 0; x < cpt.size(); x++) {
        final IntArrayList payload = patient.getPayload(cpt.get(x));
        if (evaluatedStartEndIntervals == null) {
          result.append(patient.getId() + separator + toString() + "=" + context.getCpt(id) + separator + CsvNode.getYear(patient, payload.get(0)) + separator + separator
              + separator + "1" + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
        }
        else {
          for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
            if (BeforeNode.intersect(payload.get(0), payload.get(1), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
              result.append(patient.getId() + separator + toString() + "=" + context.getCpt(id) + separator + CsvNode.getYear(patient, payload.get(0)) + separator + separator
                  + separator + "1" + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
              evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
            }
          }
        }
      }
    }
    return result.toString();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithCptCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "CPT information is missing in this dataset", this.getClass().getName(), "CPT");
    }
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public int hashCode() {
    return 197;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof HasAnyCptNode;
  }

}