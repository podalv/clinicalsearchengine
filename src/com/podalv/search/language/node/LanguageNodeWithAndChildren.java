package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AndPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;

public abstract class LanguageNodeWithAndChildren extends LanguageNodeWithChildren {

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (children.size() < 2) {
      return children.get(0).generatePreprocessingNode(context, statistics);
    }
    final PreprocessingNode n = getRootNode(this).getNode(this);
    if (n != null) {
      return n;
    }
    final AndPreprocessingNode result = new AndPreprocessingNode();
    for (int x = 0; x < children.size(); x++) {
      result.addChild(children.get(x).generatePreprocessingNode(context, statistics));
    }
    return getRootNode(this).putNode(this, result);
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof LanguageNodeWithAndChildren && super.equals(obj);
  }

}
