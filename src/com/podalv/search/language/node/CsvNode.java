package com.podalv.search.language.node;

import java.util.Arrays;
import java.util.HashSet;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AndPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.each.EachNodeFail;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.TimePoint;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.utils.text.TextUtils;

public class CsvNode extends LanguageNodeWithAndChildren {

  private final static int    COHORT_INDEX               = 0;
  private final static int    START_INDEX                = 1;
  private final static int    END_INDEX                  = 2;
  private int                 evaluatedStartEndForPid    = -1;
  private IntArrayList        evaluatedStartEndIntervals = null;
  private static final String HEADER                     = "PID\tVARIABLE\tYEAR\tVALUE1\tVALUE2\tBOOLEAN\tSTART_AGE_IN_DAYS\tEND_AGE_IN_DAYS\tVARIABLE_DEFINITION\n";
  private boolean[]           firstLineGenerated         = null;

  public CsvNode(final LanguageNode cohort, final LanguageNode start, final LanguageNode end) {
    addChild(cohort);
    addChild(start == null ? new EachNodeFail() : start);
    addChild(end == null ? new EachNodeFail() : end);
  }

  public LanguageNode getCohort() {
    return children.get(COHORT_INDEX);
  }

  private CsvNode() {
    // only for clone method
  }

  public CsvNode(final LanguageNode cohort) {
    addChild(cohort);
    addChild(new EachNodeFail());
    addChild(new EachNodeFail());
  }

  public LanguageNode getStart() {
    return children.get(START_INDEX) instanceof EachNodeFail ? null : children.get(START_INDEX);
  }

  public LanguageNode getEnd() {
    return children.get(END_INDEX) instanceof EachNodeFail ? null : children.get(END_INDEX);
  }

  public static String getHeader() {
    return HEADER;
  }

  @Override
  public LanguageNode copy() {
    final CsvNode result = new CsvNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return children.get(COHORT_INDEX).evaluate(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    propagateChildNames();
    final PreprocessingNode n = getRootNode(this).getNode(this);
    if (n != null) {
      return n;
    }
    final AndPreprocessingNode result = new AndPreprocessingNode();
    result.addChild(getCohort().generatePreprocessingNode(context, statistics));
    if (getStart() != null) {
      result.addChild(getStart().generatePreprocessingNode(context, statistics));
    }
    if (getEnd() != null) {
      result.addChild(getEnd().generatePreprocessingNode(context, statistics));
    }
    return result.getChildren().size() > 1 ? getRootNode(this).putNode(this, result) : result;
  }

  private String getChildName(final int index) {
    if (!firstLineGenerated[index] && childrenNames.get(index) == null) {
      childrenNames.set(index, children.get(index).toString());
    }
    return childrenNames.get(index);
  }

  private String getChildDescription(final int index) {
    if (!firstLineGenerated[index] && childrenNames.get(index) != null && !TextUtils.compareStrings(childrenNames.get(index), children.get(index).toString())) {
      return children.get(index).toString();
    }
    return "";
  }

  private IntArrayList evaluateTimeIntervals(final IndexCollection context, final PatientSearchModel patient) {
    if (getStart() == null && getEnd() == null) {
      return null;
    }
    else if (getStart() != null && getEnd() == null) {
      final IntArrayList result = new IntArrayList();
      final PayloadIterator iterator = getStart().evaluate(context, patient).toTimeIntervals(patient).iterator(patient);
      while (iterator.hasNext()) {
        iterator.next();
        result.add(iterator.getStartId());
        result.add(iterator.getEndId());
      }
      return result;
    }
    else if (getStart() != null && getEnd() != null) {
      final IntArrayList result = new IntArrayList();
      PayloadIterator iterator = getStart().evaluate(context, patient).toTimeIntervals(patient).iterator(patient);
      int startPoint = -1;
      int endPoint = -1;
      if (iterator.hasNext()) {
        iterator.next();
        startPoint = iterator.getStartId();
        iterator = getEnd().evaluate(context, patient).toTimeIntervals(patient).iterator(patient);
        if (iterator.hasNext()) {
          while (iterator.hasNext()) {
            iterator.next();
            endPoint = iterator.getEndId();
          }
        }
      }
      if (startPoint != -1 && endPoint != -1) {
        result.add(Math.min(startPoint, endPoint));
        result.add(Math.max(startPoint, endPoint));
      }
      return result;
    }
    throw new UnsupportedOperationException(getPositionInfo() + "CSV command must specify START as well as END time points");
  }

  public static String getYear(final Patient patient, final int time) {
    final int year = patient.getYear(time);
    return year < 0 ? "NA" : year + "";
  }

  private void getLineFromTimePoints(final StringBuilder result, final String separator, final String childName, final String childDescription, final int startTime,
      final int endTime, final Patient patient) {
    result.append(patient.getId());
    result.append(separator);
    result.append(childName);
    result.append(separator);
    result.append(getYear(patient, startTime));
    result.append(separator);
    result.append(separator);
    result.append(separator);
    result.append("1");
    result.append(separator);
    result.append(Common.minutesToDays(startTime) + separator + Common.minutesToDays(endTime));
    result.append(separator);
    result.append(childDescription);
    result.append("\n");
  }

  private void propagateChildNames() {
    if (childrenNames != null) {
      for (int x = 0; x < childrenNames.size(); x++) {
        if (childrenNames.get(x) != null) {
          children.get(x).setNodeName(childrenNames.get(x));
        }
      }
    }
  }

  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient) {
    if (firstLineGenerated == null) {
      firstLineGenerated = new boolean[children.size()];
      Arrays.fill(firstLineGenerated, false);
    }
    if (evaluatedStartEndForPid != patient.getId()) {
      evaluatedStartEndIntervals = evaluateTimeIntervals(context, patient);
      evaluatedStartEndForPid = patient.getId();
    }
    final StringBuilder result = new StringBuilder();
    @SuppressWarnings("rawtypes")
    final HashSet<Class> usedClasses = new HashSet<>();
    for (int x = END_INDEX + 1; x < children.size(); x++) {
      if (children.get(x) instanceof CsvNodeType) {
        if (!usedClasses.contains(children.get(x).getClass())) {
          result.append(((CsvNodeType) children.get(x)).getLine(separator, context, statistics, patient, evaluatedStartEndIntervals));
        }
        usedClasses.add(children.get(x).getClass());
      }
      else if (children.get(x).generatesResultType().equals(BooleanResult.class)) {
        result.append(patient.getId());
        result.append(separator);
        result.append(getChildName(x));
        result.append(separator);
        result.append("NA");
        result.append(separator);
        if (children.get(x).evaluate(context, patient).toBooleanResult().result()) {
          result.append(separator + separator + "1" + separator + separator + separator + getChildDescription(x) + "\n");
        }
        else {
          result.append(separator + separator + "0" + separator + separator + separator + getChildDescription(x) + "\n");
        }
        firstLineGenerated[x] = true;
      }
      else if (children.get(x).generatesResultType().equals(TimeIntervals.class) || children.get(x).generatesResultType().equals(PayloadPointers.class)) {
        final PayloadIterator pi = children.get(x).evaluate(context, patient).toTimeIntervals(patient).iterator(patient);
        int prevStart = -1;
        int prevEnd = -1;
        int evaluatedStartEndIntervalsPos = 0;
        while (pi.hasNext()) {
          pi.next();
          if (prevStart != pi.getStartId() || prevEnd != pi.getEndId()) {
            if (evaluatedStartEndIntervals == null) {
              getLineFromTimePoints(result, separator, getChildName(x), getChildDescription(x), pi.getStartId(), pi.getEndId(), patient);
              firstLineGenerated[x] = true;
            }
            else {
              for (int y = evaluatedStartEndIntervalsPos; y < evaluatedStartEndIntervals.size(); y += 2) {
                if (BeforeNode.intersect(pi.getStartId(), pi.getEndId(), evaluatedStartEndIntervals.get(y), evaluatedStartEndIntervals.get(y + 1))) {
                  getLineFromTimePoints(result, separator, getChildName(x), getChildDescription(x), pi.getStartId(), pi.getEndId(), patient);
                  evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, y);
                  firstLineGenerated[x] = true;
                }
              }
            }
          }
          prevStart = pi.getStartId();
          prevEnd = pi.getEndId();
        }
      }
      else if (children.get(x).generatesResultType().equals(TimePoint.class)) {
        int evaluatedStartEndIntervalsPos = 0;
        final TimePoint pi = children.get(x).evaluate(context, patient).toTimePoint();
        if (pi.isEmpty()) {
          continue;
        }
        if (evaluatedStartEndIntervals == null) {
          getLineFromTimePoints(result, separator, getChildName(x), getChildDescription(x), pi.getTimePoint(), pi.getTimePoint(), patient);
          firstLineGenerated[x] = true;
        }
        else {
          for (int y = evaluatedStartEndIntervalsPos; y < evaluatedStartEndIntervals.size(); y += 2) {
            if (BeforeNode.intersect(pi.getTimePoint(), pi.getTimePoint(), evaluatedStartEndIntervals.get(y), evaluatedStartEndIntervals.get(y + 1))) {
              getLineFromTimePoints(result, separator, getChildName(x), getChildDescription(x), pi.getTimePoint(), pi.getTimePoint(), patient);
              evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, y);
              firstLineGenerated[x] = true;
            }
          }
        }
      }
      else {
        throw new UnsupportedOperationException(getPositionInfo() + "Result type " + children.get(x).generatesResultType() + " is not supported");
      }
    }
    return result.length() > 1 ? result.toString() : "";
  }

  private String getChildName(final String name) {
    return name == null || name.isEmpty() ? "" : "\"" + name + "\"";
  }

  private String getTimeModifiers() {
    if (getStart() != null && getEnd() != null) {
      return ",START=" + getStart().toString() + ",END=" + getEnd().toString();
    }
    else if (getEnd() == null && getStart() != null) {
      return ",TIME=" + getStart().toString();
    }
    return "";
  }

  @Override
  public String toString() {
    final StringBuilder childrenStr = new StringBuilder();
    for (int x = END_INDEX + 1; x < children.size(); x++) {
      childrenStr.append(",");
      if (TextUtils.compareStrings(childrenNames.get(x), children.get(x).toString())) {
        childrenStr.append(getChildName(childrenNames.get(x)));
      }
      else {
        if (childrenNames.get(x) == null) {
          childrenStr.append(children.get(x).toString());
        }
        else {
          childrenStr.append(getChildName(childrenNames.get(x)) + "=" + children.get(x).toString());
        }
      }
    }
    return "CSV(" + getCohort().toString() + getTimeModifiers() + childrenStr.toString() + ")";
  }

  @Override
  public int hashCode() {
    return getCohort().hashCode() * 311;
  }

  private boolean nullCompare(final Object obj1, final Object obj2) {
    return (obj1 == null && obj2 == null) || //
        (obj1 != null && obj2 != null && obj1.equals(obj2));
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof CsvNode && ((CsvNode) obj).getCohort().equals(getCohort()) && //
        nullCompare(getStart(), ((CsvNode) obj).getStart()) && nullCompare(getEnd(), ((CsvNode) obj).getEnd()) && super.equals(obj);
  }

}
