package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class StartNode extends LanguageNodeWithAndChildren {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final IntArrayList result = new IntArrayList();
    final EvaluationResult r = children.get(0).evaluate(context, patient);
    if (r instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator child = r.toTimeIntervals(patient).iterator(patient);
    while (child.hasNext()) {
      child.next();
      result.add(child.getStartId());
      result.add(child.getStartId());
    }
    return (result.size() == 0) ? TimeIntervals.getEmptyTimeLine() : new TimeIntervals(this, result);
  }

  @Override
  public LanguageNode copy() {
    final StartNode result = new StartNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("START(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

}
