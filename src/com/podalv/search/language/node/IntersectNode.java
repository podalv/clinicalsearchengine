package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.IterableResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.base.MultiChildNode;

public class IntersectNode extends LanguageNodeWithAndChildren implements MultiChildNode, Cloneable {

  public static PayloadIterator intersect(final IntArrayList result, final PatientSearchModel patient, final PayloadIterator i1, final PayloadIterator i2) {
    result.clear();
    boolean getNext1 = true;
    boolean getNext2 = true;
    int start1 = -1;
    int end1 = -1;
    int start2 = -1;
    int end2 = -1;
    if (i1.hasNext() && i2.hasNext()) {
      while (i1.hasNext() || i2.hasNext()) {
        boolean changed1 = false;
        boolean changed2 = false;
        if (getNext1 && i1.hasNext()) {
          changed1 = true;
          i1.next();
          start1 = i1.getStartId();
          end1 = i1.getEndId();
        }
        if (getNext2 && i2.hasNext()) {
          changed2 = true;
          i2.next();
          start2 = i2.getStartId();
          end2 = i2.getEndId();
        }
        if (!changed1 && !changed2) {
          break;
        }
        final int end = Math.min(end1, end2);
        if ((start1 <= start2 && end1 >= start2) || (start2 <= start1 && end2 >= start1)) {
          result.add(Math.max(start1, start2));
          result.add(end);
        }
        getNext1 = false;
        getNext2 = false;
        if (end1 == end) {
          getNext1 = true;
        }
        if (end2 == end) {
          getNext2 = true;
        }
        if ((!i1.hasNext() && end1 < start2) || (!i2.hasNext() && end2 < start1)) {
          break;
        }
      }
    }
    return new EmptyPayloadIterator(((IntArrayList) result.clone()).iterator());
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    LanguageNode child = null;
    PayloadIterator result = null;
    PayloadIterator resultList = null;
    final IntArrayList resultArray = new IntArrayList();
    if (children.size() < 2) {
      final EvaluationResult r = children.iterator().next().evaluate(context, patient);
      if (r instanceof AbortedResult) {
        return AbortedResult.getInstance();
      }
      resultList = Common.compressIterator(patient, r.toTimeIntervals(patient).iterator(patient));
    }
    else {
      final Iterator<LanguageNode> i = children.iterator();
      while (i.hasNext()) {
        if (child == null) {
          child = i.next();
          final EvaluationResult r = child.evaluate(context, patient);
          if (r instanceof AbortedResult) {
            return AbortedResult.getInstance();
          }
          result = Common.compressIterator(patient, r.toTimeIntervals(patient).iterator(patient));
          continue;
        }
        final LanguageNode currentChild = i.next();
        final EvaluationResult cc = currentChild.evaluate(context, patient);
        if (cc instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        final IterableResult nextResult = cc.toTimeIntervals(patient);
        resultList = intersect(resultArray, patient, result, Common.compressIterator(patient, nextResult.iterator(patient)));
        result = resultList;
        if (!resultList.hasNext()) {
          break;
        }
      }
    }
    final IntArrayList out = new IntArrayList();
    while (resultList.hasNext()) {
      resultList.next();
      out.add(resultList.getStartId());
      out.add(resultList.getEndId());
    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("INTERSECT(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final IntersectNode result = new IntersectNode();
    result.children = cloneChildren();
    return result;
  }

}
