package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.NotesIterator;

public class NoteNotNode extends NegatedNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (children.size() != 1) {
      return new ErrorResult(this, "NOT must have only 1 child");
    }
    if (negatedChildren == null) {
      evaluateChildren();
    }
    if (NegationEvaluation.skipPatient(patient, this)) {
      return AbortedResult.getInstance();
    }
    final EvaluationResult c = children.get(0).evaluate(context, patient);
    if (c instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final NotesIterator child = (NotesIterator) ((TimeIntervals) c).iterator(patient);
    final IntOpenHashSet notNoteIds = new IntOpenHashSet();
    while (child.hasNext()) {
      child.next();
      notNoteIds.add(child.getNoteId());
    }

    final IntArrayList list = patient.getUniqueNotePayloadIds();
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < list.size(); x++) {
      final IntArrayList payloads = patient.getPayload(list.get(x));
      if (!notNoteIds.contains(payloads.get(PatientBuilder.NOTE_ID_POSITION_IN_PAYLOAD_DATA))) {
        result.add(list.get(x));
      }
    }

    return new PayloadPointers(TYPE.NOTE_RAW, this, patient, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public LanguageNode copy() {
    final NoteNotNode result = new NoteNotNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("NOT(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";

  }

  @Override
  public String[] getWarning() {
    return new String[0];
  }

}
