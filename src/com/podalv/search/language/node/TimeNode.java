package com.podalv.search.language.node;

import java.util.ArrayList;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class TimeNode extends LanguageNodeWithOrChildren {

  private final IntOpenHashSet           negativeChildIds        = new IntOpenHashSet();
  private final ArrayList<TimeIntervals> childrenResults         = new ArrayList<>();
  private final ArrayList<TimeIntervals> negativeChildrenResults = new ArrayList<>();
  private final LanguageNode             right;
  private int                            time                    = Integer.MIN_VALUE;
  private boolean                        returnTime              = false;

  public TimeNode(final LanguageNode right) {
    this.right = right;
  }

  public void setTime(final int time, final boolean returnTime) {
    this.time = time;
    this.returnTime = returnTime;
  }

  public LanguageNode getRight() {
    return right;
  }

  public void addChild(final LanguageNode node, final boolean negative) {
    super.addChild(node);
    negativeChildIds.add(super.children.size() - 1);
  }

  @Override
  public LanguageNode copy() {
    // TODO Auto-generated method stub
    return null;
  }

  private boolean containsAtLeastOneReturn() {
    for (int x = 0; x < children.size(); x++) {
      if (!negativeChildIds.contains(x) && (((LanguageNodeWithData) children.get(x)).getObject() != null)) {
        return true;
      }
    }
    if (returnTime || ((LanguageNodeWithData) right).getObject() != null) {
      return true;
    }
    return false;
  }

  protected boolean evaluateChildrenResults(final IndexCollection context, final PatientSearchModel patient) {
    for (int x = 0; x < children.size(); x++) {
      final EvaluationResult r = children.get(x).evaluate(context, patient);
      if (r instanceof AbortedResult) {
        return false;
      }
      if (negativeChildIds.contains(x)) {
        negativeChildrenResults.add(r.toTimeIntervals(patient));
      }
      else {
        childrenResults.add(r.toTimeIntervals(patient));
      }
    }
    return true;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (!containsAtLeastOneReturn()) {
      throw new UnsupportedOperationException(getPositionInfo() + "X BEFORE Y node needs to contain at least one return statements (*)");
    }
    final EvaluationResult r = right.evaluate(context, patient);
    if (r instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator rightIterator = r.toTimeIntervals(patient).iterator(patient);
    if (!evaluateChildrenResults(context, patient)) {
      return AbortedResult.getInstance();
    }
    final IntArrayList totalStartEnd = new IntArrayList();
    while (rightIterator.hasNext()) {
      final IntArrayList startEnd = new IntArrayList();
      rightIterator.next();
      int startInterval;
      int endInterval;
      if (time <= 0) {
        startInterval = time == Integer.MIN_VALUE ? 0 : rightIterator.getStartId() + time - 1;
        endInterval = rightIterator.getStartId() - 1;
      }
      else {
        startInterval = rightIterator.getStartId();
        endInterval = time == Integer.MAX_VALUE ? patient.getEndTime() : rightIterator.getStartId() + time;
      }
      boolean isValid = true;
      for (int x = 0; x < negativeChildrenResults.size(); x++) {
        final PayloadIterator i = negativeChildrenResults.get(x).iterator(patient);
        while (i.hasNext()) {
          i.next();
          if (i.getStartId() > endInterval) {
            break;
          }
          if (BeforeNode.intersect(startInterval, endInterval, i.getStartId(), i.getEndId())) {
            isValid = false;
            break;
          }
        }
        if (!isValid) {
          break;
        }
      }
      if (!isValid) {
        continue;
      }
      boolean totalFnd = true;
      for (int x = 0; x < childrenResults.size(); x++) {
        final PayloadIterator i = childrenResults.get(x).iterator(patient);
        boolean fnd = false;
        while (i.hasNext()) {
          i.next();
          if (i.getStartId() > endInterval) {
            break;
          }
          if (BeforeNode.intersect(startInterval, endInterval, i.getStartId(), i.getEndId())) {
            if (((LanguageNodeWithData) children.get(x)).getObject() != null) {
              startEnd.add(i.getStartId());
              startEnd.add(i.getEndId());
            }
            fnd = true;
          }
        }
        if (!fnd) {
          startEnd.clear();
          totalFnd = false;
          break;
        }
      }
      if (returnTime && totalFnd) {
        startEnd.add(startInterval);
        startEnd.add(endInterval);
      }
      if (totalFnd) {
        totalStartEnd.addAll(startEnd);
        if (((LanguageNodeWithData) right).getObject() != null) {
          totalStartEnd.add(rightIterator.getStartId());
          totalStartEnd.add(rightIterator.getEndId());
        }
      }
    }
    return new TimeIntervals(this, totalStartEnd);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return right.generatePreprocessingNode(context, statistics);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder();
    final ArrayList<String> positive = new ArrayList<>();
    final ArrayList<String> negative = new ArrayList<>();
    for (int x = 0; x < children.size(); x++) {
      if (!negativeChildIds.contains(x)) {
        positive.add(children.get(x).toString());
      }
      else {
        negative.add(children.get(x).toString());
      }
    }

    if (positive.size() == 1) {
      result.append(positive.get(0));
      result.append(" ");
    }
    else if (positive.size() > 1) {
      result.append("(");
      for (int x = 0; x < positive.size(); x++) {
        if (x != 0) {
          result.append(" AND ");
        }
        result.append(positive.get(x));
      }
      result.append(")");
    }

    if (result.length() != 0 && negative.size() != 0) {
      result.append(" AND NO ");
    }

    if (negative.size() == 1) {
      result.append(negative.get(0));
    }
    else if (negative.size() > 1) {
      result.append("(");
      for (int x = 0; x < negative.size(); x++) {
        if (x != 0) {
          result.append(" AND ");
        }
        result.append(negative.get(x));
      }
      result.append(")");
    }

    if (time > 0) {
      if (result.length() != 0) {
        result.append(" ");
      }
      result.append(time + " MINUTES");
      if (returnTime) {
        result.append("*");
      }
    }

    result.append(" BEFORE ");

    result.append(right.toString());

    return result.toString();
  }

  @Override
  public int hashCode() {
    return time * 13;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof TimeNode) {
      if (super.equals(obj)) {
        return ((TimeNode) obj).right.equals(right);
      }
    }
    return false;
  }
}