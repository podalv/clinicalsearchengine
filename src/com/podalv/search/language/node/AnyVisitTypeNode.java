package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;
import com.podalv.search.language.node.base.AtomicNode;

public class AnyVisitTypeNode extends LanguageNode implements AtomicNode, CsvNodeType {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.getUniqueVisitTypes().length != 0);
    }
    PayloadIterator result = null;
    PayloadIterator resultList = null;
    final IntArrayList resultArray = new IntArrayList();
    final int[] uniqueVisitTypes = patient.getUniqueVisitTypes();
    if (uniqueVisitTypes.length == 1) {
      resultList = new RawPayloadIterator(patient, patient.getVisitTypes(uniqueVisitTypes[0]));
      if (resultList.containsInvalidEvent()) {
        return AbortedResult.getInstance();
      }
    }
    else if (uniqueVisitTypes.length > 0) {
      result = new RawPayloadIterator(patient, patient.getVisitTypes(uniqueVisitTypes[0]));
      if (result.containsInvalidEvent()) {
        result = null;
      }
      for (int x = 1; x < uniqueVisitTypes.length; x++) {
        final RawPayloadIterator i = new RawPayloadIterator(patient, patient.getVisitTypes(uniqueVisitTypes[x]));
        if (result == null) {
          if (!i.containsInvalidEvent()) {
            result = i;
          }
        }
        if (i.containsInvalidEvent()) {
          continue;
        }
        resultList = UnionNode.union(resultArray, result, i);
        result = resultList;
      }
    }
    final IntArrayList out = new IntArrayList();
    if (resultList != null) {
      while (resultList.hasNext()) {
        resultList.next();
        out.add(resultList.getStartId());
        out.add(resultList.getEndId());
      }

    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new AnyVisitTypeNode();
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "VISIT TYPE" : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithVisitTypeCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "VISIT TYPE information is missing in this dataset", this.getClass().getName(), "VISIT TYPE");
    }
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final int[] uniqueVisitTypes = patient.getUniqueVisitTypes();
    int evaluatedStartEndIntervalsPos = 0;
    if (uniqueVisitTypes != null && uniqueVisitTypes.length != 0) {
      for (final int uniqueVisitType : uniqueVisitTypes) {
        final String id = context.getVisitTypeName(uniqueVisitType);
        final PayloadIterator list = new PayloadPointers(TYPE.CPT_RAW, this, patient, patient.getVisitTypes(uniqueVisitType)).iterator(patient);
        if (list.containsInvalidEvent()) {
          result.append(patient.getId() + separator + toString() + "=" + id + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator + "1"
              + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
          continue;
        }
        while (list.hasNext()) {
          list.next();
          if (evaluatedStartEndIntervals == null) {
            result.append(patient.getId() + separator + toString() + "=" + id + separator + CsvNode.getYear(patient, list.getStartId()) + separator + "" + separator + ""
                + separator + "1" + separator + Common.minutesToDays(list.getStartId()) + separator + Common.minutesToDays(list.getEndId()) + separator + "\n");
          }
          else {
            for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
              if (BeforeNode.intersect(list.getStartId(), list.getEndId(), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                result.append(patient.getId() + separator + toString() + "=" + id + separator + CsvNode.getYear(patient, list.getStartId()) + separator + "" + separator + ""
                    + separator + "1" + separator + Common.minutesToDays(list.getStartId()) + separator + Common.minutesToDays(list.getEndId()) + separator + "\n");
                evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
              }
            }
          }
        }
      }
    }
    return result.toString();
  }

  @Override
  public int hashCode() {
    return 359;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AnyVisitTypeNode;
  }
}