package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE;
import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.base.MultiChildNode;

public class LastMentionNode extends LanguageNodeWithAndChildren implements MultiChildNode {

  private LanguageNode context = null;

  public void setContext(final LanguageNode node) {
    this.context = node;
  }

  public static TimeIntervals getLastMention(final LanguageNode node, final PatientSearchModel patient, final TimeIntervals ti, final IntArrayList contextStartEnd) {
    final IntArrayList startEnd = getLastMention(patient, ti, contextStartEnd);
    return (startEnd.size() == 0) ? new TimeIntervals(node) : new TimeIntervals(node, startEnd);
  }

  public static IntArrayList getLastMention(final PatientSearchModel patient, final TimeIntervals ti, final IntArrayList contextStartEnd) {
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < contextStartEnd.size(); x += 2) {
      int start = -1;
      int end = -1;
      final PayloadIterator iterator = ti.iterator(patient);
      while (iterator.hasNext()) {
        iterator.next();
        if (BeforeNode.intersect(iterator.getStartId(), iterator.getEndId(), contextStartEnd.get(x), contextStartEnd.get(x + 1))) {
          start = Math.max(iterator.getStartId(), contextStartEnd.get(x));
          end = Math.min(iterator.getEndId(), contextStartEnd.get(x + 1));
        }
      }
      if (start != -1 && end != -1) {
        result.add(start);
        result.add(end);
      }
    }
    return result;
  }

  private IntArrayList evaluateContext(final Patient patient, final PayloadIterator i) {
    final IntArrayList result = new IntArrayList();
    if (i == null) {
      result.add(0);
      result.add(Integer.MAX_VALUE);
    }
    else {
      while (i.hasNext()) {
        i.next();
        result.add(i.getStartId());
        result.add(i.getEndId());
      }
    }
    return result;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    PayloadIterator i = null;
    if (this.context != null) {
      final EvaluationResult r = this.context.evaluate(context, patient);
      if (r instanceof AbortedResult) {
        return AbortedResult.getInstance();
      }
      i = r.toTimeIntervals(patient).iterator(patient);
      ;
    }
    final EvaluationResult ch = children.get(0).evaluate(context, patient);
    if (ch instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    return children.size() != 1 ? new ErrorResult(this, "Last mention can have only one child")
        : getLastMention(this, patient, ch.toTimeIntervals(patient), evaluateContext(patient, i));
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("LAST MENTION(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final LastMentionNode result = new LastMentionNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasBooleanChildren(children)) {
      result.add(TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT);
    }
    if (hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    }
    if (result.size() == 0 && !isTemporalValid(this)) {
      result.add(ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
    }
    return result.toArray(new String[result.size()]);
  }

}
