package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_EXTEND_BY;

import java.math.BigInteger;
import java.util.HashSet;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.Enums.RANGE_REFERENCE;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.IterableResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class ExtendByNode extends LanguageNodeWithAndChildren {

  private final RANGE_REFERENCE startReference;
  private final RANGE_REFERENCE endReference;
  private final int             startOffset;
  private final int             endOffset;

  public ExtendByNode(final RANGE_REFERENCE startReference, final RANGE_REFERENCE endReference, final int startOffset, final int endOffset) {
    if (startOffset <= endOffset) {
      this.startOffset = startOffset;
      this.endOffset = endOffset;
      this.startReference = startReference;
      this.endReference = endReference;
    }
    else {
      this.startOffset = endOffset;
      this.endOffset = startOffset;
      this.startReference = endReference;
      this.endReference = startReference;
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult evaluatedChild = children.get(0).evaluate(context, patient);
    if (evaluatedChild instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final IntArrayList result = new IntArrayList();
    final PayloadIterator iterator = Common.compressIterator(patient, ((IterableResult) evaluatedChild.toTimeIntervals(patient)).iterator(patient));
    long prevEnd = -1;
    while (iterator.hasNext()) {
      iterator.next();
      BigInteger start = BigInteger.valueOf(iterator.getStartId());
      BigInteger end = BigInteger.valueOf(iterator.getEndId());
      if (startReference == RANGE_REFERENCE.END) {
        start = BigInteger.valueOf(iterator.getEndId());
      }
      if (endReference == RANGE_REFERENCE.START) {
        end = BigInteger.valueOf(iterator.getStartId());
      }
      start = start.add(BigInteger.valueOf(startOffset));
      end = end.add(BigInteger.valueOf(endOffset));
      if (start.longValue() > patient.getEndTime()) {
        start = BigInteger.valueOf(patient.getEndTime());
      }
      if (end.longValue() > patient.getEndTime()) {
        end = BigInteger.valueOf(patient.getEndTime());
      }
      if (start.longValue() < patient.getStartTime()) {
        start = BigInteger.valueOf(patient.getStartTime());
      }
      if (end.longValue() < patient.getStartTime()) {
        end = BigInteger.valueOf(patient.getStartTime());
      }

      if (start.longValue() > end.longValue()) {
        final long temp = end.longValue();
        end = BigInteger.valueOf(start.longValue());
        start = BigInteger.valueOf(temp);
      }
      if (prevEnd == -1 || prevEnd < start.longValue()) {
        result.add(start.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : start.intValue());
        result.add(end.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : end.intValue());
      }
      if (prevEnd >= start.longValue()) {
        if (prevEnd < end.longValue()) {
          result.set(result.size() - 1, end.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : end.intValue());
        }
      }
      prevEnd = end.longValue();
    }

    return new TimeIntervals(this, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    return "EXTEND BY(" + children.get(0).toString() + ", " + BeforeNodeParameter.getOffsetReference(startReference) + BeforeNodeParameter.getOffset(startReference, startOffset)
        + ", " + BeforeNodeParameter.getOffsetReference(endReference) + BeforeNodeParameter.getOffset(endReference, endOffset) + ")";
  }

  @Override
  public LanguageNode copy() {
    final ExtendByNode result = new ExtendByNode(startReference, endReference, startOffset, endOffset);
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasBooleanChildren(children) || hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_EXTEND_BY);
    }
    if (result.size() == 0 && !isTemporalValid(this)) {
      result.add(ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
    }
    return result.toArray(new String[result.size()]);
  }

}