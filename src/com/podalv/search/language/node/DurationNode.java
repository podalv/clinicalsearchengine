package com.podalv.search.language.node;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.CompressedStartEndIterator;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class DurationNode extends LanguageNodeWithAndChildren {

  private final int     minDuration;
  private final int     maxDuration;
  private final boolean singleType;
  private boolean       returnLeft  = true;
  private boolean       returnRight = false;

  public DurationNode(final boolean singleType, final int minDuration, final int maxDuration) {
    this.singleType = singleType;
    this.minDuration = Math.min(minDuration, maxDuration);
    this.maxDuration = Math.max(maxDuration, minDuration);
  }

  public DurationNode(final boolean singleType, final int minDuration, final int maxDuration, final boolean returnLeft, final boolean returnRight) {
    this.singleType = singleType;
    this.returnLeft = returnLeft;
    this.returnRight = returnRight;
    this.minDuration = Math.min(minDuration, maxDuration);
    this.maxDuration = Math.max(maxDuration, minDuration);
  }

  private IntArrayList mergeResultIntervals(final PatientSearchModel patient, final IntArrayList total, final IntArrayList newIntervals) {
    final PayloadIterator pi = UnionNode.union(new IntArrayList(), new EmptyPayloadIterator(total.iterator()), new EmptyPayloadIterator(newIntervals.iterator()));
    final IntArrayList result = new IntArrayList();
    while (pi.hasNext()) {
      pi.next();
      result.add(pi.getStartId());
      result.add(pi.getEndId());
    }
    return result;
  }

  private EvaluationResult evaluateComplex(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult sections = children.get(1).evaluate(context, patient);
    final EvaluationResult result = children.get(0).evaluate(context, patient);
    if (sections instanceof AbortedResult || result instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final TimeIntervals sec = sections.toTimeIntervals(patient);
    final TimeIntervals res = result.toTimeIntervals(patient);
    if (res != null && sec != null && !res.isEmpty() && !sec.isEmpty()) {
      final PayloadIterator pi = Common.compressIterator(patient, sec.iterator(patient));
      IntArrayList resultIntervals = new IntArrayList();
      final IntOpenHashSet countedId = new IntOpenHashSet();
      while (pi.hasNext()) {
        final PayloadIterator iterator = Common.compressIterator(patient, res.toTimeIntervals(patient).iterator(patient));
        final IntArrayList out = new IntArrayList();
        pi.next();
        int cnt = 0;
        int accumulatedDuration = 0;
        while (iterator.hasNext()) {
          cnt++;
          iterator.next();
          if (CountNode.intersect(iterator, pi)) {
            accumulatedDuration += (iterator.getEndId() - iterator.getStartId());
            if (singleType && ((iterator.getEndId() - iterator.getStartId()) >= minDuration && ((iterator.getEndId() - iterator.getStartId()) <= maxDuration))) {
              out.add(iterator.getStartId());
              out.add(iterator.getEndId());
            }
            if (!singleType && !countedId.contains(cnt)) {
              out.add(iterator.getStartId());
              out.add(iterator.getEndId());
            }
          }
          else if (iterator.getStartId() > pi.getEndId()) {
            break;
          }
        }
        if (singleType || (accumulatedDuration >= minDuration && accumulatedDuration <= maxDuration)) {
          if (returnLeft) {
            resultIntervals = mergeResultIntervals(patient, resultIntervals, out);
          }
          if (returnRight) {
            resultIntervals = mergeResultIntervals(patient, resultIntervals, new IntArrayList(new int[] {pi.getStartId(), pi.getEndId()}));
          }
        }
      }
      if (returnRight && returnLeft) {
        final CompressedStartEndIterator c = new CompressedStartEndIterator(resultIntervals);
        final IntArrayList l = new IntArrayList();
        while (c.hasNext()) {
          c.next();
          l.add(c.getStartId());
          l.add(c.getEndId());
        }
        resultIntervals = l;
      }
      return new TimeIntervals(this, resultIntervals);
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  private EvaluationResult evaluateSimple(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult result = children.get(0).evaluate(context, patient);
    if (result instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    if (result != null) {
      int accumulatedDuration = 0;
      final PayloadIterator iterator = result.toTimeIntervals(patient).iterator(patient);
      final IntArrayList out = new IntArrayList();
      while (iterator.hasNext()) {
        iterator.next();
        accumulatedDuration += (iterator.getEndId() - iterator.getStartId());
        if (singleType && ((iterator.getEndId() - iterator.getStartId()) >= minDuration && ((iterator.getEndId() - iterator.getStartId()) <= maxDuration))) {
          out.add(iterator.getStartId());
          out.add(iterator.getEndId());
        }
        if (!singleType) {
          out.add(iterator.getStartId());
          out.add(iterator.getEndId());
        }
      }
      if (singleType || (accumulatedDuration >= minDuration && accumulatedDuration <= maxDuration)) {
        return new TimeIntervals(this, out);
      }
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return children.size() == 1 ? evaluateSimple(context, patient) : evaluateComplex(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    final DurationNode result = new DurationNode(singleType, minDuration, maxDuration);
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    if (children.size() < 2) {
      return "DURATION(" + children.get(0) + ", " + (singleType ? "SINGLE" : "ALL") + ", " + Common.languageNodeTimeArgumentToText(minDuration) + ", "
          + Common.languageNodeTimeArgumentToText(maxDuration) + ")";
    }
    return "DURATION(" + children.get(0) + ", " + children.get(1) + ", " + (singleType ? "SINGLE" : "ALL") + ", " + Common.languageNodeTimeArgumentToText(minDuration) + ", "
        + Common.languageNodeTimeArgumentToText(maxDuration) + ")";
  }

}
