package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;

public class FamilyHistoryTextNode extends TextNode {

  public FamilyHistoryTextNode(final String text) {
    super(text);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initTid(context);
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsFamilyHistoryTid(tid));
    }
    final IntArrayList payload = patient.getFamilyHistoryTerms(tid);
    if (PayloadPointers.containsInvalidEvents(payload)) {
      return AbortedResult.getInstance();
    }
    return new PayloadPointers(TYPE.NOTE_RAW, this, patient, payload);
  }

  @Override
  public String toString() {
    return "~TEXT=\"" + text + "\"";
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithTidFamilyHistoryCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "~TEXT information is missing in this dataset", this.getClass().getName(), "~TEXT");
    }
    initTid(context);
    return new AtomPreprocessingNode(statistics.getTidFamilyHistoryPatients(tid));
  }

}