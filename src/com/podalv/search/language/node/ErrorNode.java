package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.AtomicNode;

/** Contains error message. This is added when there were errors parsing the command
 *
 * @author podalv
 *
 */
public class ErrorNode extends LanguageNode implements AtomicNode {

  private final String errorMessage;

  public ErrorNode(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return new ErrorResult(this, errorMessage);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return ErrorResult.class;
  }

  @Override
  public String toString() {
    return errorMessage;
  }

  @Override
  public LanguageNode copy() {
    return new ErrorNode(errorMessage);
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return null;
  }

  @Override
  public int hashCode() {
    return errorMessage.hashCode() * 251;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof ErrorNode && ((ErrorNode) obj).errorMessage.equals(errorMessage);
  }
}
