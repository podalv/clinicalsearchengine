package com.podalv.search.language.node;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class IntervalNode extends LanguageNodeWithAndChildren {

  private final boolean pairEvaluation;

  private static enum RETURN_TYPE {
    TIMELINE, START, END, OTHER, EMPTY, START_END
  };

  public IntervalNode(final boolean pairEvaluation) {
    this.pairEvaluation = pairEvaluation;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (children.size() != 2) {
      throw new UnsupportedOperationException(getPositionInfo() + "INTERVAL node should have exactly 2 parameters");
    }
    final EvaluationResult c1 = children.get(0).evaluate(context, patient);
    final EvaluationResult c2 = children.get(1).evaluate(context, patient);
    if (c1 instanceof AbortedResult || c2 instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator iterator1 = c1.toTimeIntervals(patient).iterator(patient);
    final PayloadIterator iterator2 = c2.toTimeIntervals(patient).iterator(patient);
    final IntArrayList result = new IntArrayList();
    if (pairEvaluation) {
      while (iterator1.hasNext() && iterator2.hasNext()) {
        iterator1.next();
        iterator2.next();
        if (iterator1.getStartId() <= iterator2.getEndId()) {
          result.add(iterator1.getStartId());
          result.add(iterator2.getEndId());
        }
      }
    }
    else {
      final IntArrayList ends = new IntArrayList();
      if (iterator2.hasNext()) {
        int prevEnd = -1;
        while (iterator2.hasNext()) {
          iterator2.next();
          if (prevEnd != iterator2.getEndId()) {
            ends.add(iterator2.getEndId());
          }
          prevEnd = iterator2.getEndId();
        }
      }

      if (ends.size() != 0) {
        int prevStart = -1;
        while (iterator1.hasNext()) {
          iterator1.next();
          final int start = iterator1.getStartId();
          if (prevStart != start) {
            for (int x = 0; x < ends.size(); x++) {
              if (ends.get(x) >= start) {
                result.add(start);
                result.add(ends.get(x));
              }
            }
          }
          prevStart = iterator1.getStartId();
        }
      }
    }
    return result.size() == 0 ? TimeIntervals.getEmptyTimeLine() : new TimeIntervals(this, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("INTERVAL(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }

    if (pairEvaluation) {
      result.append(", PAIRS");
    }

    return result.toString() + ")";
  }

  private RETURN_TYPE isStartOfTimeline(final LanguageNode node) {
    if (node instanceof StartNode) {
      return RETURN_TYPE.START;
    }
    if (node instanceof EndNode) {
      return RETURN_TYPE.END;
    }
    if (node instanceof IntersectNode) {
      final HashSet<RETURN_TYPE> types = new HashSet<>();
      for (int x = 0; x < ((IntersectNode) node).children.size(); x++) {
        types.add(isStartOfTimeline(((IntersectNode) node).children.get(x)));
      }
      if (types.contains(RETURN_TYPE.START) && types.contains(RETURN_TYPE.END) && types.size() == 2) {
        return RETURN_TYPE.EMPTY;
      }
      if (types.contains(RETURN_TYPE.START) && types.contains(RETURN_TYPE.END) && types.contains(RETURN_TYPE.OTHER)) {
        return RETURN_TYPE.START;
      }
      if (types.contains(RETURN_TYPE.END) && types.contains(RETURN_TYPE.START) && types.contains(RETURN_TYPE.OTHER)) {
        return RETURN_TYPE.END;
      }
      return RETURN_TYPE.OTHER;
    }
    if (node instanceof UnionNode) {
      final HashSet<RETURN_TYPE> types = new HashSet<>();
      for (int x = 0; x < ((UnionNode) node).children.size(); x++) {
        types.add(isStartOfTimeline(((UnionNode) node).children.get(x)));
      }
      if (types.contains(RETURN_TYPE.TIMELINE)) {
        return RETURN_TYPE.TIMELINE;
      }
      if (types.contains(RETURN_TYPE.START) && types.contains(RETURN_TYPE.END)) {
        return RETURN_TYPE.START_END;
      }
      if (types.contains(RETURN_TYPE.START)) {
        return RETURN_TYPE.START;
      }
      if (types.contains(RETURN_TYPE.END)) {
        return RETURN_TYPE.END;
      }
    }
    if (node.generatesResultType().equals(BooleanResult.class)) {
      return RETURN_TYPE.TIMELINE;
    }
    return RETURN_TYPE.OTHER;
  }

  @Override
  public LanguageNode copy() {
    final IntervalNode result = new IntervalNode(this.pairEvaluation);
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();

    if (children.size() != 0) {
      if (!isTemporalValid(children.get(0)) && !isTemporalValid(children.get(0))) {
        result.add(ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
      }
      final RETURN_TYPE start = isStartOfTimeline(children.get(0));
      if (((start == RETURN_TYPE.TIMELINE || start == RETURN_TYPE.EMPTY))) {
        result.add(ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
      }
    }

    return result.toArray(new String[result.size()]);
  }

}