package com.podalv.search.language.node;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;

public class NoteTimeInstancesNode extends LanguageNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final IntArrayList list = patient.getUniqueNotePayloadIds();
    if (PayloadPointers.containsInvalidEvents(list)) {
      return AbortedResult.getInstance();
    }
    return new PayloadPointers(TYPE.NOTE_RAW, this, patient, list);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new NoteTimeInstancesNode();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String toString() {
    return "NOTES";
  }

  @Override
  public int hashCode() {
    return 89;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof NoteTimeInstancesNode;
  }
}