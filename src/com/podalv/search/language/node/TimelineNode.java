package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;

public class TimelineNode extends LanguageNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (patient.getEndTime() == 0 && patient.getStartTime() == 0) {
      return AbortedResult.getInstance();
    }
    return new TimeIntervals(this, patient.getStartTime(), patient.getEndTime());
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    return new TimelineNode();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String toString() {
    return "TIMELINE";
  }

  @Override
  public int hashCode() {
    return 17;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof TimelineNode;
  }
}
