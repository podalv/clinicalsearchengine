package com.podalv.search.language.node;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class LabValuesNode extends LanguageNode implements AtomicNode {

  private final String labsName;
  private int          labsId = Integer.MIN_VALUE;
  private final double min;
  private final double max;

  public LabValuesNode(final String labsName, final double min, final double max) {
    this.labsName = TextNode.trimString(labsName);
    this.min = min;
    this.max = max;
  }

  private void initLabValuesId(final IndexCollection context) {
    if (labsId == Integer.MIN_VALUE) {
      labsId = context.getLabsCode(labsName);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initLabValuesId(context);
    final IntArrayList result = patient.getLabValues().get(context, labsId, min, max);
    if (result != null && PayloadPointers.containsInvalidEvents(result)) {
      return AbortedResult.getInstance();
    }
    if (result == null) {
      return TimeIntervals.getEmptyTimeLine();
    }
    final IntArrayList resultTimes = new IntArrayList();
    for (int x = 0; x < result.size(); x++) {
      resultTimes.add(patient.getPayload(result.get(x)).get(0));
    }
    return new TimeIntervals(this, resultTimes);
  }

  @Override
  public LanguageNode copy() {
    return new LabValuesNode(labsName, min, max);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    initLabValuesId(context);
    return new AtomPreprocessingNode(statistics.getLabsPatients(labsId));
  }

  @Override
  public String toString() {
    return "LABS(" + "\"" + labsName + "\"" + ", " + String.valueOf(min) + ", " + String.valueOf(max) + ")";
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof LabValuesNode) {
      final LabValuesNode node = (LabValuesNode) obj;
      return node.labsName.equals(labsName) && Double.compare(max, node.max) == 0 && Double.compare(min, node.min) == 0;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return labsName.hashCode() * 137;
  }
}