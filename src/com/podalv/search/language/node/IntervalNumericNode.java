package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;

public class IntervalNumericNode extends LanguageNode {

  private final TimeIntervals result;
  private final int           start;
  private final int           end;

  public IntervalNumericNode(final int start, final int end) {
    this.start = start <= end ? start : end;
    this.end = end >= start ? end : start;
    result = new TimeIntervals(this, this.start, this.end);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return result;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public LanguageNode copy() {
    return new IntervalNumericNode(start, end);
  }

  @Override
  public String toString() {
    return "INTERVAL(" + start + ", " + end + ")";
  }

  @Override
  public int hashCode() {
    return start * end * 151;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof IntervalNumericNode && start == ((IntervalNumericNode) obj).start && end == ((IntervalNumericNode) obj).end;
  }

}