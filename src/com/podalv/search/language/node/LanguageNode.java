package com.podalv.search.language.node;

import java.util.ArrayList;

import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.script.StringPosition;

public abstract class LanguageNode {

  LanguageNode     parent;
  private int      columnStart = -1;
  private int      columnEnd   = -1;
  private int      rowStart    = -1;
  private int      rowEnd      = -1;
  private RootNode root        = null;
  private String   nodeName    = null;

  public String[] getWarning() {
    return null;
  }

  public String getNodeName() {
    return nodeName;
  }

  public void setNodeName(final String nodeName) {
    this.nodeName = nodeName;
  }

  protected RootNode getRootNode(final LanguageNode node) {
    if (root == null) {
      if (node.parent != null) {
        if (node.parent instanceof RootNode) {
          this.root = (RootNode) node.parent;
        }
        else {
          getRootNode(node.getParent());
        }
      }
    }
    return root;
  }

  public LanguageNode setPosition(final int startCol, final int endCol, final int rowStart, final int rowEnd) {
    this.columnStart = startCol;
    this.columnEnd = endCol;
    this.rowStart = rowStart - 1;
    this.rowEnd = rowEnd - 1;
    return this;
  }

  public int getColumnStart() {
    return columnStart;
  }

  public int getColumnEnd() {
    return columnEnd;
  }

  public int getRowStart() {
    return rowStart;
  }

  public int getRowEnd() {
    return rowEnd;
  }

  public String getPositionInfo() {
    final RootNode node = getRootNode(this);
    if (node != null) {
      final StringPosition pos = node.getOriginalQueryPosition(columnStart, rowStart);
      final int colLength = this.columnEnd - this.columnStart;
      this.columnStart = pos.getColumn();
      this.columnEnd = pos.getColumn() + colLength;
      this.rowStart = pos.getRow();
      this.rowEnd = pos.getRow();
    }
    if (columnStart >= 0 && columnEnd >= 0 && rowStart >= 0 && rowEnd >= 0) {
      return "[" + columnStart + "," + columnEnd + ":" + rowStart + "," + rowEnd + "] ";
    }
    return "";
  }

  public LanguageNode getParent() {
    return parent;
  }

  public void setParent(final LanguageNode parent) {
    this.parent = parent;
  }

  boolean hasBooleanChildren(final ArrayList<LanguageNode> children) {
    boolean booleanFnd = false;
    for (final LanguageNode child : children) {
      if (child.generatesResultType().equals(BooleanResult.class)) {
        booleanFnd = true;
      }
    }
    return booleanFnd;
  }

  boolean hasTimelineChildren(final ArrayList<LanguageNode> children) {
    boolean fnd = false;
    for (final LanguageNode child : children) {
      if (child instanceof TimelineNode) {
        fnd = true;
      }
    }
    return fnd;
  }

  boolean isTemporalValid(final LanguageNode node) {
    if (node instanceof UnionNode || node instanceof BeforeNode) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).children.size(); x++) {
        if (!isTemporalValid(((LanguageNodeWithChildren) node).getChildren().get(x))) {
          return false;
        }
      }
    }
    else if (node instanceof IntersectNode) {
      for (int x = 0; x < ((LanguageNodeWithChildren) node).children.size(); x++) {
        if (isTemporalValid(((LanguageNodeWithChildren) node).children.get(x))) {
          return true;
        }
      }
      return false;
    }
    else if (node.generatesResultType().equals(BooleanResult.class) || node instanceof TimelineNode || node instanceof NullNode) {
      return false;
    }
    return true;
  }

  public abstract LanguageNode copy();

  public abstract EvaluationResult evaluate(IndexCollection context, PatientSearchModel patient);

  public abstract Class<? extends EvaluationResult> generatesResultType();

  public abstract PreprocessingNode generatePreprocessingNode(IndexCollection context, Statistics statistics);
}