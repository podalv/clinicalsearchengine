package com.podalv.search.language.node;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.EvaluationResult;

public class DumpNode extends LanguageNodeWithAndChildren {

  private final String namedQuery;

  public DumpNode(final String namedQuery) {
    this.namedQuery = TextNode.trimString(namedQuery, false);
  }

  @Override
  public LanguageNode copy() {
    final DumpNode result = new DumpNode(namedQuery);
    result.children = cloneChildren();
    return result;
  }

  public String getNamedQuery() {
    return namedQuery;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return children.get(0).evaluate(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return children.get(0).generatesResultType();
  }

  @Override
  public String toString() {
    return "DUMP(" + children.get(0).toString() + ",\"" + namedQuery + "\")";
  }

}