package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE;
import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.base.MultiChildNode;

public class FirstMentionNode extends LanguageNodeWithAndChildren implements MultiChildNode {

  private LanguageNode context = null;

  public void setContext(final LanguageNode node) {
    this.context = node;
  }

  public static TimeIntervals getFirstMention(final LanguageNode node, final PatientSearchModel patient, final PayloadIterator iterator, final IntArrayList contextStartEnd) {
    final IntArrayList startEnd = getFirstMention(iterator, contextStartEnd);
    return (startEnd.size() == 0) ? TimeIntervals.getEmptyTimeLine() : new TimeIntervals(node, startEnd);
  }

  public static IntArrayList getFirstMention(final PayloadIterator iterator, final IntArrayList contextStartEnd) {
    final IntArrayList result = new IntArrayList();
    int nextContextId = 0;
    while (iterator.hasNext()) {
      iterator.next();
      for (int x = nextContextId; x < contextStartEnd.size(); x += 2) {
        if (BeforeNode.intersect(iterator.getStartId(), iterator.getEndId(), contextStartEnd.get(x), contextStartEnd.get(x + 1))) {
          result.add(Math.max(iterator.getStartId(), contextStartEnd.get(x)));
          result.add(Math.min(iterator.getEndId(), contextStartEnd.get(x + 1)));
          nextContextId = x + 2;
          break;
        }
      }
      if (nextContextId >= contextStartEnd.size()) {
        break;
      }
      if (contextStartEnd.size() == 2 && contextStartEnd.get(0) == 0 && contextStartEnd.get(1) == Integer.MAX_VALUE) {
        break;
      }
    }
    return result;
  }

  private IntArrayList evaluateContext(final Patient patient, final PayloadIterator i) {
    final IntArrayList result = new IntArrayList();
    if (i == null) {
      result.add(0);
      result.add(Integer.MAX_VALUE);
    }
    else {
      while (i.hasNext()) {
        i.next();
        result.add(i.getStartId());
        result.add(i.getEndId());
      }
    }
    return result;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    PayloadIterator i = null;
    if (this.context != null) {
      final EvaluationResult t = this.context.evaluate(context, patient);
      if (t instanceof AbortedResult) {
        return AbortedResult.getInstance();
      }
      i = t.toTimeIntervals(patient).iterator(patient);
    }
    final EvaluationResult ch = children.get(0).evaluate(context, patient);
    if (ch instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    return children.size() != 1 ? new ErrorResult(this, "First mention can have only one child")
        : getFirstMention(this, patient, ch.toTimeIntervals(patient).iterator(patient), evaluateContext(patient, i));
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("FIRST MENTION(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
    }
    if (context != null) {
      result.append(", ");
      result.append(context);
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final FirstMentionNode result = new FirstMentionNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasBooleanChildren(children)) {
      result.add(TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT);
    }
    if (hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    }
    if (result.size() == 0 && !isTemporalValid(this)) {
      result.add(ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
    }
    return result.toArray(new String[result.size()]);
  }

}
