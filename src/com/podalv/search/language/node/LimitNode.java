package com.podalv.search.language.node;

import java.math.BigInteger;

import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.LimitNodeEvaluator;

/** Top level node that limits the number of patients returned
 *
 * @author podalv
 *
 */
public class LimitNode extends LanguageNodeWithAndChildren {

  final String limitType;

  public LimitNode(final String limitType) {
    this.limitType = limitType;
  }

  public LimitNodeEvaluator getEvaluator(final int cachedCnt, final int preprocessedCnt) {
    if (limitType.equals("CACHED") || limitType.equals("CACHE")) {
      return LimitNodeEvaluator.createProcessed(cachedCnt);
    }
    else if (limitType.toUpperCase().indexOf("M") != -1) {
      BigInteger result = BigInteger.valueOf(Integer.parseInt(limitType.substring(0, limitType.toUpperCase().indexOf("M")).trim()));
      result = result.multiply(BigInteger.valueOf(60000l));
      return LimitNodeEvaluator.createTime(result.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : result.intValue());
    }
    else if (limitType.toUpperCase().indexOf("S") != -1) {
      BigInteger result = BigInteger.valueOf(Integer.parseInt(limitType.substring(0, limitType.toUpperCase().indexOf("M")).trim()));
      result = result.multiply(BigInteger.valueOf(1000l));
      return LimitNodeEvaluator.createTime(result.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : result.intValue());
    }
    else {
      return LimitNodeEvaluator.createCorrect(Integer.parseInt(limitType));
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return children.get(0).evaluate(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    if (parent != null && !(parent instanceof RootNode)) {
      return parent.generatesResultType();
    }
    return BooleanResult.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return children.get(0).generatePreprocessingNode(context, statistics);
  }

  @Override
  public LanguageNode copy() {
    final OutputNode result = new OutputNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    return "LIMIT(" + limitType + "," + children.get(0).toString() + ")";
  }

  @Override
  public int hashCode() {
    return children.get(0).hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return children.get(0).equals(obj);
  }

}
