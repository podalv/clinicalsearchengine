package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AtomIterablePreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.AtomicNode;

public class EthnicityNode extends LanguageNode implements AtomicNode {

  private final String ethnicity;
  private int          ethnicityId = -1;

  public EthnicityNode(final String ethnicity) {
    this.ethnicity = TextNode.trimString(ethnicity).toUpperCase();
  }

  private void processEthnicityId(final IndexCollection context) {
    if (ethnicityId == -1) {
      ethnicityId = context.getDemographics().getEthnicity().getId(ethnicity);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    processEthnicityId(context);
    return BooleanResult.getInstance(context.getDemographics().containsEthnicity(patient.getId(), ethnicityId));
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public LanguageNode copy() {
    return new EthnicityNode(ethnicity);
  }

  @Override
  public String toString() {
    return "ETHNICITY=\"" + ethnicity + "\"";
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    processEthnicityId(context);
    return new AtomIterablePreprocessingNode(context.getEthnicityIterator(ethnicityId));
  }

  @Override
  public int hashCode() {
    return ethnicity.hashCode() * 239;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof EthnicityNode && ((EthnicityNode) obj).ethnicity.equals(ethnicity);
  }
}
