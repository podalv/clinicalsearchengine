package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.MedsIterator;
import com.podalv.utils.text.TextUtils;

/** Allows binding of query information for RX =  + ROUTE = "" + STATUS = ""
 *
 * @author podalv
 *
 */
public class DrugNode extends LanguageNode {

  private final String route;
  private final String status;
  private final int    rx;
  private int          routeId  = -1;
  private int          statusId = -1;

  public DrugNode(final int rx, final String route, final String status) {
    this.rx = rx;
    this.route = route != null ? TextNode.trimString(route) : null;
    this.status = status != null ? TextNode.trimString(status) : null;
  }

  public int getRx() {
    return rx;
  }

  private void initRouteStatus(final IndexCollection indices) {
    if (route != null) {
      routeId = indices.getDrugRouteId(route);
      if (routeId == -1) {
        throw new UnsupportedOperationException(getPositionInfo() + "Could not resolve the ROUTE='" + route + "'");
      }
    }
    if (status != null) {
      statusId = indices.getDrugStatusId(status);
      if (statusId == -1) {
        throw new UnsupportedOperationException(getPositionInfo() + "Could not resolve the STATUS='" + status + "'");
      }
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initRouteStatus(context);
    final IntArrayList payloads = patient.getRxNorm(rx);
    if (PayloadPointers.containsInvalidEvents(payloads)) {
      return AbortedResult.getInstance();
    }
    final MedsIterator iterator = (MedsIterator) new PayloadPointers(TYPE.MEDS, this, patient, payloads).iterator(patient);
    final IntArrayList result = new IntArrayList();
    while (iterator.hasNext()) {
      iterator.next();
      if ((route == null || iterator.getDrugRouteId() == routeId) && (status == null || iterator.getDrugStatusId() == statusId)) {
        result.add(iterator.getStartId());
        result.add(iterator.getEndId());
      }
    }
    return new PayloadPointers(TYPE.START_END_TIME, this, patient, result);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("DRUG(RX=" + rx);
    if (route != null) {
      result.append(", ROUTE=\"" + route + "\"");
    }
    if (status != null) {
      result.append(", STATUS=\"" + status + "\"");
    }
    result.append(")");
    return result.toString();
  }

  @Override
  public LanguageNode copy() {
    return new DrugNode(rx, route, status);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithRxNormCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "RX information is missing in this dataset", this.getClass().getName(), "RX");
    }
    return new AtomPreprocessingNode(statistics.getRxNormPatients(rx));
  }

  @Override
  public int hashCode() {
    return 7 * rx;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof DrugNode) {
      return ((DrugNode) obj).rx == rx && TextUtils.compareStrings(route, ((DrugNode) obj).route) && TextUtils.compareStrings(status, ((DrugNode) obj).status);
    }
    return false;
  }
}
