package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.MultiChildNode;

public class AndNode extends LanguageNodeWithAndChildren implements MultiChildNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    for (final LanguageNode child : children) {
      final EvaluationResult result = child.evaluate(context, patient);
      if (!(result instanceof AbortedResult)) {
        if (!result.toBooleanResult().result()) {
          return BooleanResult.getFalse();
        }
      }
      else {
        return result;
      }
    }
    return BooleanResult.getTrue();
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("AND(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final AndNode result = new AndNode();
    result.children = cloneChildren();
    return result;
  }

}