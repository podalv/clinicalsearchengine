package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class EncountersNode extends LanguageNode implements AtomicNode {

  private static EncountersNode instance = new EncountersNode();

  private EncountersNode() {
    // accessible only by Instance
  }

  public static EncountersNode getInstance() {
    return instance;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return new TimeIntervals(this, patient.getEncounterDays());
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public LanguageNode copy() {
    return new EncountersNode();
  }

  @Override
  public String toString() {
    return "ENCOUNTERS";
  }

  @Override
  public int hashCode() {
    return 269;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof EncountersNode;
  }
}