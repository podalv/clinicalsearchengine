package com.podalv.search.language.node;

import java.util.HashSet;

/** All the negation nodes must implement this interface
 *
 * @author podalv
 *
 */
public abstract class NegatedNode extends LanguageNodeWithAndChildren {

  HashSet<Class<? extends LanguageNode>> negatedChildren = null;

  public void evaluateChildren() {
    negatedChildren = new HashSet<>();
    NegationEvaluation.evaluateNegatedNode(this);
  }

  public void registerNegatedChild(final Class<? extends LanguageNode> child) {
    negatedChildren.add(child);
  }

  public HashSet<Class<? extends LanguageNode>> getSkipClasses() {
    return negatedChildren;
  }
}
