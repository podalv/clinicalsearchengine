package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.MultiChildNode;

public class OrNode extends LanguageNodeWithOrChildren implements MultiChildNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    for (final LanguageNode child : children) {
      final EvaluationResult r = child.evaluate(context, patient);
      if (r instanceof AbortedResult) {
        return AbortedResult.getInstance();
      }
      if (r.toBooleanResult().result()) {
        return BooleanResult.getTrue();
      }
    }
    return BooleanResult.getFalse();
  }

  public LanguageNode getTemporalClone() {
    final UnionNode node = new UnionNode();
    for (final LanguageNode child : children) {
      node.addChild(child);
    }
    return node;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public LanguageNode copy() {
    final OrNode result = new OrNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public final String toString() {
    final StringBuilder result = new StringBuilder("OR(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

}