package com.podalv.search.language.node;

import java.util.ArrayList;
import java.util.Arrays;

import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.utils.arrays.ArrayUtils;
import com.podalv.utils.text.TextUtils;

public class PatientsNode extends LanguageNode {

  private final IntOpenHashSet patients    = new IntOpenHashSet();
  private final IntArrayList   patientList = new IntArrayList();

  public PatientsNode(final String parameters) {
    final String[] data = TextUtils.split(parameters, ',');
    for (final String number : data) {
      final int val = Integer.parseInt(number.trim());
      patients.add(val);
      patientList.add(val);
    }
    patientList.sort();
  }

  public PatientsNode(final IntArrayList patientList) {
    for (int x = 0; x < patientList.size(); x++) {
      patients.add(patientList.get(x));
      this.patientList.add(patientList.get(x));
    }
    this.patientList.sort();
  }

  public PatientsNode(final ArrayList<String> parameters) {
    for (final String number : parameters) {
      final int val = Integer.parseInt(number.trim());
      patients.add(val);
      patientList.add(val);
    }
    patientList.sort();
  }

  int[] getPatientListCopy() {
    return patientList.toArray();
  }

  @Override
  public LanguageNode copy() {
    return new PatientsNode(patientList);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return BooleanResult.getInstance(patients.contains(patient.getId()));
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(new IntArrayCloneableIterator(ArrayUtils.intersectSorted(patientList.iterator(), statistics.getPatientIds().iterator())));
  }

  @Override
  public String toString() {
    final String listArray = Arrays.toString(patientList.toArray());
    return "PATIENTS(" + listArray.substring(1, listArray.length() - 1) + ')';
  }

  @Override
  public int hashCode() {
    return patients.size() * 61;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof PatientsNode) {
      return ((PatientsNode) obj).patients.equals(patients);
    }
    return false;
  }

}