package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE;
import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AndPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.Enums.RANGE_REFERENCE;
import com.podalv.search.datastructures.Enums.RANGE_TYPE;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.base.TwoChildNode;
import com.podalv.utils.arrays.StartEndTimeTupletArraySort;

public class BeforeNode extends NegatedNode implements TwoChildNode {

  private final ArrayList<BeforeNodeParameter> parameters                    = new ArrayList<>();
  private final boolean                        returnLeftInterval;
  private final boolean                        returnRightInterval;
  private final IntArrayList                   negativeParameterPositions    = new IntArrayList();
  private boolean                              evaluatedParameters           = false;
  private Boolean                              containsOnlyNegatedParameters = null;

  public BeforeNode(final boolean returnLeftInterval, final boolean returnRightInterval) {
    this.returnLeftInterval = returnLeftInterval;
    this.returnRightInterval = returnRightInterval;
  }

  public void addParameter(final BeforeNodeParameter parameter) {
    parameters.add(parameter);
  }

  private IntArrayList merge(final Patient patient, final IntArrayList left, final IntArrayList right) {
    IntArrayList result = new IntArrayList();
    if (left.size() != 0 && right.size() != 0) {
      UnionNode.union(result, new EmptyPayloadIterator(left.iterator()), new EmptyPayloadIterator(right.iterator()));
    }
    else if (left.size() != 0) {
      result = left;
    }
    else {
      result = right;
    }
    return result;
  }

  private IntArrayList getValidIntervals(final PatientSearchModel patient, final TimeIntervals intervals, final IntOpenHashSet validIntervals) {
    final IntArrayList result = new IntArrayList();
    if (validIntervals.size() != 0) {
      final PayloadIterator iterator = Common.compressIterator(patient, intervals.iterator(patient));
      while (iterator.hasNext()) {
        iterator.next();
        if (validIntervals.contains(iterator.getStartId())) {
          result.add(iterator.getStartId());
          result.add(iterator.getEndId());
        }
      }
    }
    return result;
  }

  private boolean onlyNegativeParameters() {
    boolean onlyNotIn = true;
    if (parameters.size() != 0) {
      for (int x = 0; x < parameters.size(); x++) {
        if (parameters.get(x).isRangeTypeIn()) {
          onlyNotIn = false;
          break;
        }
      }
    }
    return onlyNotIn;
  }

  private void evaluateParameters() {
    for (int x = 0; x < parameters.size(); x++) {
      if (parameters.get(x).returnRange()) {
        negativeParameterPositions.add(x);
      }
    }
    evaluatedParameters = true;
  }

  private int getStart(final BeforeNodeParameter param, final PayloadIterator iterator) {
    BigInteger bigInt = BigInteger.valueOf(param.getStartOffset());
    if (param.getStartReference() == RANGE_REFERENCE.NUMERIC || param.getStartReference() == RANGE_REFERENCE.START) {
      bigInt = bigInt.add(BigInteger.valueOf(iterator.getStartId()));
    }
    else {
      bigInt = bigInt.add(BigInteger.valueOf(iterator.getEndId()));
    }
    return bigInt.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : bigInt.intValue();
  }

  private int getEnd(final BeforeNodeParameter param, final PayloadIterator iterator) {
    BigInteger bigInt = BigInteger.valueOf(param.getEndOffset());
    if (param.getEndReference() == RANGE_REFERENCE.NUMERIC || param.getEndReference() == RANGE_REFERENCE.START) {
      bigInt = bigInt.add(BigInteger.valueOf(iterator.getStartId()));

    }
    else {
      bigInt = bigInt.add(BigInteger.valueOf(iterator.getEndId()));
    }
    return bigInt.longValue() > Integer.MAX_VALUE ? Integer.MAX_VALUE : bigInt.intValue();
  }

  public static boolean intersect(final int s1, final int e1, final int s2, final int e2) {
    return ((s1 <= s2 && e1 >= s2) || (s2 <= s1 && e2 >= s1));
  }

  private boolean isIn(final RANGE_TYPE rangeType, final PayloadIterator leftIterator, final int rangeStart, final int rangeEnd) {
    if (leftIterator == null) {
      return ((rangeType == RANGE_TYPE.NOT_IN || rangeType == RANGE_TYPE.NOT_IN_END || rangeType == RANGE_TYPE.NOT_IN_START || rangeType == RANGE_TYPE.NOT_IN_START_END));
    }
    else {
      if (rangeType == RANGE_TYPE.IN || rangeType == RANGE_TYPE.NOT_IN) {
        return intersect(rangeStart, rangeEnd, leftIterator.getStartId(), leftIterator.getEndId());
      }
      if (rangeType == RANGE_TYPE.IN_END || rangeType == RANGE_TYPE.NOT_IN_END) {
        return leftIterator.getEndId() >= rangeStart && leftIterator.getEndId() <= rangeEnd;
      }
      if (rangeType == RANGE_TYPE.IN_START || rangeType == RANGE_TYPE.NOT_IN_START) {
        return leftIterator.getStartId() >= rangeStart && leftIterator.getStartId() <= rangeEnd;
      }
      if (rangeType == RANGE_TYPE.IN_START_END || rangeType == RANGE_TYPE.NOT_IN_START_END) {
        return (leftIterator.getStartId() >= rangeStart && leftIterator.getStartId() <= rangeEnd) && (leftIterator.getEndId() >= rangeStart && leftIterator.getEndId() <= rangeEnd);
      }
    }
    return false;
  }

  private boolean isTrue(final boolean[] arr) {
    for (int x = 0; x < arr.length; x++) {
      if (!arr[x] && parameters.get(x).isRangeTypeIn()) {
        return false;
      }
    }
    return true;
  }

  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient, final TimeIntervals resultLeft, final TimeIntervals resultRight) {
    if (!returnLeftInterval && !returnRightInterval && !containsReturn()) {
      throw new UnsupportedOperationException(getPositionInfo() + "Use asterisk(*) after one of the arguments to define return type in the BEFORE command");
    }
    if (returnLeftInterval && containsOnlyNotIn()) {
      throw new UnsupportedOperationException(getPositionInfo() + "BEFORE command cannot return absence of an interval");
    }
    if (!resultRight.isEmpty()) {
      final PayloadIterator rightIterator = Common.compressIterator(patient, resultRight.iterator(patient));
      final IntOpenHashSet totalLeftStarts = new IntOpenHashSet();
      final IntOpenHashSet totalRightStarts = new IntOpenHashSet();
      final IntArrayList totalNegativeRanges = new IntArrayList();
      if (parameters.size() != 0) {
        if (!evaluatedParameters) {
          evaluateParameters();
        }
        while (rightIterator.hasNext()) {
          rightIterator.next();
          final PayloadIterator leftIterator = Common.compressIterator(patient, resultLeft.iterator(patient));
          final IntOpenHashSet validLeftStarts = new IntOpenHashSet();
          final IntOpenHashSet validRightStarts = new IntOpenHashSet();
          final IntArrayList negativeRanges = new IntArrayList();
          final boolean[] correct = new boolean[parameters.size()];
          boolean correctNegative = true;
          if (leftIterator.hasNext()) {
            while (leftIterator.hasNext()) {
              leftIterator.next();
              boolean currentCorrect = false;
              for (int x = 0; x < parameters.size(); x++) {
                final BeforeNodeParameter param = parameters.get(x);
                int rangeStart = getStart(param, rightIterator);
                int rangeEnd = getEnd(param, rightIterator);
                if (rangeStart > rangeEnd) {
                  final int temp = rangeStart;
                  rangeStart = rangeEnd;
                  rangeEnd = temp;
                }
                if (!param.isRangeTypeIn()) {
                  final boolean isIn = isIn(param.getType(), leftIterator, rangeStart, rangeEnd);
                  if (isIn) {
                    correctNegative = false;
                    break;
                  }
                }
                final boolean isIn = isIn(param.getType(), leftIterator, rangeStart, rangeEnd);
                if (param.isRangeTypeIn() && isIn) {
                  correct[x] = true;
                  currentCorrect = true;
                }
              }
              if (correctNegative && (currentCorrect || onlyNegativeParameters())) {
                if (returnLeftInterval) {
                  validLeftStarts.add(leftIterator.getStartId());
                }
                if (returnRightInterval) {
                  validRightStarts.add(rightIterator.getStartId());
                }
                for (int y = 0; y < negativeParameterPositions.size(); y++) {
                  int rangeS = getStart(parameters.get(negativeParameterPositions.get(y)), rightIterator);
                  int rangeE = getEnd(parameters.get(negativeParameterPositions.get(y)), rightIterator);
                  if (rangeS > rangeE) {
                    final int temp = rangeS;
                    rangeS = rangeE;
                    rangeE = temp;
                  }
                  negativeRanges.add(rangeS);
                  negativeRanges.add(rangeE);
                }
              }
            }
            if (correctNegative && isTrue(correct)) {
              totalLeftStarts.addAll(validLeftStarts);
              totalRightStarts.addAll(validRightStarts);
              totalNegativeRanges.addAll(negativeRanges);
            }
          }
          else {
            for (int x = 0; x < parameters.size(); x++) {
              final BeforeNodeParameter param = parameters.get(x);
              int rangeStart = getStart(param, rightIterator);
              int rangeEnd = getEnd(param, rightIterator);
              if (rangeStart > rangeEnd) {
                final int temp = rangeStart;
                rangeStart = rangeEnd;
                rangeEnd = temp;
              }
              if (!param.isRangeTypeIn()) {
                final boolean isIn = isIn(param.getType(), null, rangeStart, rangeEnd);
                if (!isIn) {
                  correctNegative = false;
                  break;
                }
              }
              if (param.isRangeTypeIn()) {
                correct[x] = false;
              }
            }
            if (isTrue(correct)) {
              if (returnRightInterval) {
                totalRightStarts.add(rightIterator.getStartId());
              }
            }
            for (int x = 0; x < negativeParameterPositions.size(); x++) {
              int rangeStart = getStart(parameters.get(negativeParameterPositions.get(x)), rightIterator);
              int rangeEnd = getEnd(parameters.get(negativeParameterPositions.get(x)), rightIterator);
              if (rangeStart > rangeEnd) {
                final int temp = rangeStart;
                rangeStart = rangeEnd;
                rangeEnd = temp;
              }
              totalNegativeRanges.add(rangeStart);
              totalNegativeRanges.add(rangeEnd);
            }
          }
        }
        StartEndTimeTupletArraySort.sort(totalNegativeRanges);
        IntArrayList result = merge(patient, getValidIntervals(patient, resultLeft, totalLeftStarts), getValidIntervals(patient, resultRight, totalRightStarts));
        if (totalNegativeRanges.size() != 0) {
          final IntArrayList negatives = new IntArrayList(totalNegativeRanges.size() * 2);
          for (int x = 0; x < totalNegativeRanges.size(); x += 2) {
            final int addLeft = totalNegativeRanges.get(x);
            final int addRight = totalNegativeRanges.get(x + 1);
            if (negatives.size() == 0 || (addLeft != negatives.get(negatives.size() - 2) && addRight != negatives.get(negatives.size() - 1))) {
              negatives.add(addLeft);
              negatives.add(addRight);
            }
          }
          result = merge(patient, result, negatives);
        }
        return new TimeIntervals(this, result);
      }
      else {
        if (!returnLeftInterval && !returnRightInterval && !containsReturn()) {
          throw new UnsupportedOperationException(getPositionInfo() + "Use asterisk(*) after one of the arguments to define return type in the BEFORE command");
        }
        else {
          return evaluateWithoutParameters(patient, resultLeft, resultRight, rightIterator);
        }
      }
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  private boolean containsReturn() {
    for (final BeforeNodeParameter param : parameters) {
      if (param.returnRange()) {
        return true;
      }
    }
    return false;
  }

  private EvaluationResult evaluateWithoutParameters(final PatientSearchModel patient, final TimeIntervals resultLeft, final TimeIntervals resultRight,
      final PayloadIterator rightIterator) {
    final IntOpenHashSet validLeftStarts = new IntOpenHashSet();
    final IntOpenHashSet validRightStarts = new IntOpenHashSet();
    while (rightIterator.hasNext()) {
      rightIterator.next();
      final PayloadIterator leftIterator = Common.compressIterator(patient, resultLeft.iterator(patient));
      while (leftIterator.hasNext()) {
        leftIterator.next();
        if (leftIterator.getStartId() < rightIterator.getStartId()) {
          if (returnLeftInterval && !validLeftStarts.contains(leftIterator.getStartId())) {
            validLeftStarts.add(leftIterator.getStartId());
          }
          if (returnRightInterval && !validRightStarts.contains(rightIterator.getStartId())) {
            validRightStarts.add(rightIterator.getStartId());
          }
        }
      }
    }
    if (validLeftStarts.size() != 0 || validRightStarts.size() != 0) {
      return new TimeIntervals(this, merge(patient, getValidIntervals(patient, resultLeft, validLeftStarts), getValidIntervals(patient, resultRight, validRightStarts)));
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  public LanguageNode getLeft() {
    return children.get(0);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult resultFirst = children.get(0).evaluate(context, patient);
    final EvaluationResult resultSecond = children.get(children.size() - 1).evaluate(context, patient);
    if (resultFirst instanceof AbortedResult || resultSecond instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    if (containsOnlyNegatedParameters == null) {
      containsOnlyNegatedParameters = onlyNegativeParameters();
    }
    if (containsOnlyNegatedParameters) {
      if (negatedChildren == null) {
        evaluateChildren();
      }
      if (NegationEvaluation.skipPatient(patient, this)) {
        return AbortedResult.getInstance();
      }
    }
    return evaluate(context, patient, resultFirst.toTimeIntervals(patient), resultSecond.toTimeIntervals(patient));
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("BEFORE(");
    final Iterator<LanguageNode> i = children.iterator();
    int cnt = 0;
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (cnt == 0 && returnLeftInterval) {
        result.append("*");
      }
      if (cnt == 1 && returnRightInterval) {
        result.append("*");
      }
      if (i.hasNext()) {
        result.append(", ");
      }
      cnt++;
    }
    result.append(')');
    for (int x = 0; x < parameters.size(); x++) {
      result.append(parameters.get(x).toString());
    }
    return result.toString();
  }

  private boolean containsOnlyNotIn() {
    boolean containsOnlyNegative = false;
    for (final BeforeNodeParameter param : parameters) {
      if (!param.isRangeTypeIn()) {
        containsOnlyNegative = true;
      }
      if (param.isRangeTypeIn()) {
        return false;
      }
    }
    return containsOnlyNegative;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    final PreprocessingNode n = getRootNode(this).getNode(this);
    if (n != null) {
      return n;
    }
    final AndPreprocessingNode result = new AndPreprocessingNode();
    if (containsOnlyNotIn()) {
      result.addChild(children.get(1).generatePreprocessingNode(context, statistics));
    }
    else {
      for (int x = 0; x < children.size(); x++) {
        result.addChild(children.get(x).generatePreprocessingNode(context, statistics));
      }
    }
    return result.getChildren().size() == 1 ? result : getRootNode(this).putNode(this, result);
  }

  @Override
  public LanguageNode copy() {
    final BeforeNode result = new BeforeNode(returnLeftInterval, returnRightInterval);
    for (int x = 0; x < parameters.size(); x++) {
      result.addParameter(parameters.get(x));
    }
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasBooleanChildren(children)) {
      result.add(TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT);
    }
    if (hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    }
    if (result.size() == 0 && !isTemporalValid(this)) {
      result.add(ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
    }
    return result.toArray(new String[result.size()]);
  }

  @Override
  public boolean equals(final Object obj) {
    if (containsOnlyNotIn()) {
      if (obj instanceof BeforeNode) {
        return (((BeforeNode) obj).containsOnlyNotIn() == containsOnlyNotIn()) && super.equals(obj);
      }
    }
    else {
      return super.equals(obj);
    }
    return false;
  }

}