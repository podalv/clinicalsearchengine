package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_EMPTY;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.SingleChildNode;

public class NotNode extends NegatedNode implements SingleChildNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (negatedChildren == null) {
      evaluateChildren();
    }
    if (NegationEvaluation.skipPatient(patient, this)) {
      return AbortedResult.getInstance();
    }
    final EvaluationResult e = children.get(0).evaluate(context, patient);
    if (e instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    return BooleanResult.getInstance(!e.toBooleanResult().result());
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("NOT(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final NotNode result = new NotNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_EMPTY);
    }
    return result.toArray(new String[result.size()]);
  }

}