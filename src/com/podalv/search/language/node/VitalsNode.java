package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.CsvWriter;
import com.podalv.search.datastructures.MeasurementOffHeapData;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class VitalsNode extends LanguageNode implements AtomicNode, CsvNodeType {

  private final String vitalsName;
  private String       parsedVitalsName = null;
  private int          vitalsId         = Integer.MIN_VALUE;
  private final double min;
  private final double max;

  public VitalsNode(final String vitalsName, final double min, final double max) {
    this.vitalsName = TextNode.trimString(vitalsName);
    this.min = min;
    this.max = max;
  }

  private void initVitalsId(final IndexCollection context) {
    if (vitalsId == Integer.MIN_VALUE) {
      vitalsId = context.getVitalsCode(vitalsName);
      parsedVitalsName = vitalsId < 0 ? "UNDEFINED" : context.getVitalsName(vitalsId);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initVitalsId(context);
    final IntArrayList result = patient.getVitals().get(context, vitalsId, min, max);
    if (result != null && PayloadPointers.containsInvalidEvents(result)) {
      return AbortedResult.getInstance();
    }
    return result == null ? TimeIntervals.getEmptyTimeLine() : new TimeIntervals(this, result);
  }

  @Override
  public LanguageNode copy() {
    return new VitalsNode(vitalsName, min, max);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithVitalsCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "VITALS information is missing in this dataset", this.getClass().getName(), "VITALS");
    }
    initVitalsId(context);
    return new AtomPreprocessingNode(statistics.getVitalsPatients(vitalsId));
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "VITALS(" + "\"" + vitalsName + "\"" + ", " + String.valueOf(min) + ", " + String.valueOf(max) + ")" : getNodeName();
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    initVitalsId(context);
    final StringBuilder result = new StringBuilder();
    final String nodeName = toString();
    patient.getVitals().extract(new CsvWriter() {

      int evaluatedStartEndIntervalsPos = 0;

      @Override
      public void write(final String name, final int time, final double value) {
        if (parsedVitalsName != null && vitalsName != null && parsedVitalsName.equals(vitalsName)) {
          if (MeasurementOffHeapData.isEventInvalid(time)) {
            result.append(patient.getId() + separator + nodeName + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator + "1" + separator
                + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
            return;
          }
          if (evaluatedStartEndIntervals == null) {
            result.append(patient.getId() + separator + nodeName + separator + CsvNode.getYear(patient, time) + separator + value + separator + "" + separator + "1" + separator
                + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
          }
          else {
            for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
              if (BeforeNode.intersect(time, time, evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                result.append(patient.getId() + separator + nodeName + separator + CsvNode.getYear(patient, time) + separator + value + separator + "" + separator + "1" + separator
                    + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
                evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
              }
            }
          }
        }
      }
    }, context);
    return result.toString();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof VitalsNode) {
      final VitalsNode node = (VitalsNode) obj;
      return node.vitalsName.equalsIgnoreCase(vitalsName) && Double.compare(max, node.max) == 0 && Double.compare(min, node.min) == 0;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return vitalsName.hashCode() * 5;
  }
}