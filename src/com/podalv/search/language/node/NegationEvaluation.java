package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.each.EachNode;

/** Node that allows for a negation query: NOT, INVERT, BEFORE(X, Y)-(Z, A), NoteNot
 *
 * @author podalv
 *
 */
public final class NegationEvaluation {

  private NegationEvaluation() {
    // static access only
  }

  public static boolean skipPatient(final Patient patient, final NegatedNode node) {
    final Iterator<Class<? extends LanguageNode>> skipClasses = node.getSkipClasses().iterator();
    while (skipClasses.hasNext()) {
      final Class<? extends LanguageNode> skipClass = skipClasses.next();
      if ((skipClass.equals(AtcNode.class) || skipClass.equals(RxNormNode.class)) && patient.getUniqueRxNormCodes().size() == 0) {
        return true;
      }
      else if (skipClass.equals(DepartmentNode.class) && patient.getUniqueDepartmentCodes().size() == 0) {
        return true;
      }
      else if (skipClass.equals(HasCptNode.class) && patient.getUniqueCptCodes().size() == 0) {
        return true;
      }
      else if (skipClass.equals(HasIcd9Node.class) && patient.getUniqueIcd9Codes().size() == 0) {
        return true;
      }
      else if (skipClass.equals(HasIcd10Node.class) && patient.getUniqueIcd10Codes().size() == 0) {
        return true;
      }
      else if (skipClass.equals(LabsNode.class) && !patient.containsLabs()) {
        return true;
      }
      else if (skipClass.equals(LabValuesNode.class) && patient.getLabValues().isEmpty()) {
        return true;
      }
      else if (skipClass.equals(NoteTypeNode.class) && patient.getUniqueNotePayloadIds().size() == 0) {
        return true;
      }
      else if (skipClass.equals(SnomedNode.class) && patient.getUniqueSnomedCodes().size() == 0) {
        return true;
      }
      else if (skipClass.equals(VisitTypeNode.class) && !patient.containsVisitTypes()) {
        return true;
      }
      else if (skipClass.equals(VitalsNode.class) && patient.getVitals().isEmpty()) {
        return true;
      }
      else if (skipClass.equals(FamilyHistoryTextNode.class) && !patient.containsFamilyHistoryTerms()) {
        return true;
      }
      else if (skipClass.equals(NegatedTextNode.class) && !patient.containsNegatedTerms()) {
        return true;
      }
      else if (skipClass.equals(TextNode.class) && !patient.containsPositiveTerms()) {
        return true;
      }
    }
    return false;
  }

  public static void evaluateNegatedNode(final LanguageNode negatedNode) {
    if (negatedNode instanceof NegatedNode) {
      evaluateNegatedNodeRec(negatedNode, negatedNode);
    }
  }

  private static void evaluateNegatedNodeRec(final LanguageNode negatedNode, final LanguageNode childNode) {
    if (childNode instanceof BeforeNode) {
      evaluateNegatedNodeChildren((NegatedNode) negatedNode, ((BeforeNode) childNode).getLeft());
      evaluateNegatedNodeRec(negatedNode, ((BeforeNode) childNode).getLeft());
      return;
    }
    if (childNode instanceof LanguageNodeWithChildren) {
      final LanguageNodeWithChildren chNode = (LanguageNodeWithChildren) childNode;
      for (int x = 0; x < chNode.children.size(); x++) {
        if (chNode.getChildren().get(x) instanceof CsvNode) {
          evaluateNegatedNodeChildren((NegatedNode) negatedNode, ((CsvNode) chNode.getChildren().get(x)).getCohort());
          evaluateNegatedNodeRec(negatedNode, ((CsvNode) chNode.getChildren().get(x)).getCohort());
        }
        if (chNode.getChildren().get(x) instanceof TimeNode) {
          evaluateNegatedNodeChildren((NegatedNode) negatedNode, ((TimeNode) chNode.getChildren().get(x)).getRight());
          evaluateNegatedNodeRec(negatedNode, ((TimeNode) chNode.getChildren().get(x)).getRight());
        }
        if (chNode.getChildren().get(x) instanceof EachNode) {
          evaluateNegatedNodeChildren((NegatedNode) negatedNode, ((EachNode) chNode.getChildren().get(x)).getMainNode());
          evaluateNegatedNodeRec(negatedNode, ((EachNode) chNode.getChildren().get(x)).getMainNode());
        }
        evaluateNegatedNodeChildren((NegatedNode) negatedNode, chNode.getChildren().get(x));
        evaluateNegatedNodeRec(negatedNode, chNode.getChildren().get(x));
      }
    }
  }

  private static void evaluateNegatedNodeChildren(final NegatedNode negatedNode, final LanguageNode childNode) {
    if (childNode instanceof AtcNode || childNode instanceof DepartmentNode || childNode instanceof HasCptNode || childNode instanceof HasIcd9Node
        || childNode instanceof HasIcd10Node || childNode instanceof LabsNode || childNode instanceof LabValuesNode || childNode instanceof NoteTypeNode
        || childNode instanceof RxNormNode || childNode instanceof SnomedNode || childNode instanceof VisitTypeNode || childNode instanceof VitalsNode
        || childNode instanceof FamilyHistoryTextNode || childNode instanceof TextNode || childNode instanceof NegatedTextNode) {
      negatedNode.registerNegatedChild(childNode.getClass());
    }
    if (childNode instanceof DrugNode) {
      negatedNode.registerNegatedChild(RxNormNode.class);
    }
  }

}
