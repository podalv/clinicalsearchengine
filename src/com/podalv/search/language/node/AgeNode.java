package com.podalv.search.language.node;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class AgeNode extends LanguageNode implements AtomicNode {

  private final int low;
  private final int high;

  public AgeNode(final int low, final int high) {
    this.low = Math.min(high, low);
    this.high = Math.max(high, low);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    int position = IntArrayList.binarySearch(patient.getAgeRanges(), low);
    if (position < 0) {
      position = Math.abs(position) - 2;
      if (position < 0) {
        position = 0;
      }
    }
    if (position % 2 != 0) {
      position--;
    }
    if (position < 0) {
      position = 0;
    }
    final IntArrayList result = new IntArrayList();
    for (int y = position; y < patient.getAgeRanges().size(); y += 2) {
      if ((patient.getAgeRanges().get(y) >= low && patient.getAgeRanges().get(y + 1) <= high) || //
          (patient.getAgeRanges().get(y) <= low && patient.getAgeRanges().get(y + 1) >= high) || //
          (patient.getAgeRanges().get(y) <= low && patient.getAgeRanges().get(y + 1) >= low) || //
          (patient.getAgeRanges().get(y) <= high && patient.getAgeRanges().get(y + 1) >= high)) {
        if (patient.getAgeRanges().get(y) < low) {
          result.add(low);
        }
        else {
          result.add(patient.getAgeRanges().get(y));
        }
        if (patient.getAgeRanges().get(y + 1) <= high) {
          result.add(patient.getAgeRanges().get(y + 1));
        }
        else {
          result.add(high);
          break;
        }
      }
      if (patient.getAgeRanges().get(y) > high) {
        break;
      }
    }
    return new TimeIntervals(this, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    return "AGE(" + Common.languageNodeTimeArgumentToText(low) + ", " + Common.languageNodeTimeArgumentToText(high) + ")";
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    final PreprocessingNode n = getRootNode(this).getNode(this);
    if (n != null) {
      return n;
    }
    return getRootNode(this).putNode(this, new AtomPreprocessingNode(statistics.getPidsForAges(low, high)));
  }

  @Override
  public LanguageNode copy() {
    return new AgeNode(low, high);
  }

  @Override
  public int hashCode() {
    return low * high;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AgeNode && ((AgeNode) obj).high == high && ((AgeNode) obj).low == low;
  }
}