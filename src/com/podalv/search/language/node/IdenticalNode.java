package com.podalv.search.language.node;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class IdenticalNode extends LanguageNodeWithAndChildren {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult r1 = children.get(0).evaluate(context, patient);
    final EvaluationResult r2 = children.get(1).evaluate(context, patient);
    if (r1 instanceof AbortedResult || r2 instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator result1 = r1.toTimeIntervals(patient).iterator(patient);
    final PayloadIterator result2 = r2.toTimeIntervals(patient).iterator(patient);
    boolean result = true;
    while (result1.hasNext() || result2.hasNext()) {
      if (!result1.hasNext() || !result2.hasNext()) {
        result = false;
        break;
      }
      result1.next();
      result2.next();
      if (result1.getStartId() != result2.getStartId() || result1.getEndId() != result2.getEndId()) {
        result = false;
        break;
      }
    }
    return BooleanResult.getInstance(result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    // A bit counter-intuitive. It produces Boolean result, but all the command children need to be evaluated as time intervals
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    final IdenticalNode result = new IdenticalNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    return "IDENTICAL(" + children.get(0).toString() + ", " + children.get(1).toString() + ")";
  }

}