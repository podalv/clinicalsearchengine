package com.podalv.search.language.node;

import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.NotesIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;
import com.podalv.search.language.node.base.MultiChildNode;

public class NoteNode extends LanguageNodeWithAndChildren implements MultiChildNode {

  private String noteTypeString = null;
  private int    noteTypeId     = Integer.MIN_VALUE;

  public void setNoteTypeString(final String noteTypeString) {
    this.noteTypeString = noteTypeString;
  }

  private void initNoteTypeId(final IndexCollection context) {
    if (noteTypeString != null && noteTypeId == Integer.MIN_VALUE) {
      noteTypeId = context.getNoteTypeId(noteTypeString);
    }
  }

  public static NotesIterator union(final PatientSearchModel patient, final IntArrayList result, final NotesIterator i1, final NotesIterator i2) {
    result.clear();
    boolean move1 = true;
    boolean move2 = true;
    boolean finished1 = false;
    boolean finished2 = false;
    while (!finished1 || !finished2) {
      if (move1) {
        if (i1.hasNext()) {
          i1.next();
        }
        else {
          finished1 = true;
        }
      }
      if (move2) {
        if (i2.hasNext()) {
          i2.next();
        }
        else {
          finished2 = true;
        }
      }
      move1 = false;
      move2 = false;
      if (finished1 && !finished2) {
        result.add(i2.getPayloadId());
        move2 = true;
      }
      else if (finished2 && !finished1) {
        result.add(i1.getPayloadId());
        move1 = true;
      }
      else if (!finished1 && !finished2) {
        final int start = Math.min(i1.getStartId(), i2.getStartId());
        int payloadId = start == i1.getStartId() ? i1.getPayloadId() : i2.getPayloadId();
        // overlap
        if (i1.getStartId() == i2.getStartId()) {
          payloadId = Math.min(i1.getPayloadId(), i2.getPayloadId());
          final int otherPayloadId = Math.max(i1.getPayloadId(), i2.getPayloadId());
          result.add(payloadId);
          if (payloadId != otherPayloadId) {
            result.add(otherPayloadId);
          }
          move1 = true;
          move2 = true;
        }
        else if (i1.getStartId() < i2.getEndId()) {
          move1 = true;
          result.add(payloadId);
        }
        else {
          move2 = true;
          result.add(payloadId);
        }
      }
    }

    return new NotesIterator(new RawPayloadIterator(patient, result));
  }

  private static boolean isCorrectNoteType(final NotesIterator i1, final int noteTypeId) {
    return noteTypeId == Integer.MIN_VALUE || i1.getNoteTypeId() == noteTypeId;
  }

  public static NotesIterator intersect(final PatientSearchModel patient, final IntArrayList result, final NotesIterator i1, final NotesIterator i2, final int noteTypeId) {
    result.clear();
    boolean getNext1 = true;
    boolean getNext2 = true;
    int start1 = -1;
    int noteId1 = -1;
    int payloadId1 = -1;
    int start2 = -1;
    int noteId2 = -1;
    if (i1.hasNext() && i2.hasNext()) {
      while (i1.hasNext() || i2.hasNext()) {
        boolean changed1 = false;
        boolean changed2 = false;
        if (getNext1 && i1.hasNext()) {
          changed1 = true;
          i1.next();
          start1 = i1.getStartId();
          noteId1 = i1.getNoteId();
          payloadId1 = i1.getPayloadId();
        }
        if (getNext2 && i2.hasNext()) {
          changed2 = true;
          i2.next();
          start2 = i2.getStartId();
          noteId2 = i2.getNoteId();
        }
        if (!changed1 && !changed2) {
          break;
        }
        getNext1 = false;
        getNext2 = false;
        if (start1 == start2) {
          if (noteId1 == noteId2) {
            if (isCorrectNoteType(i1, noteTypeId) && isCorrectNoteType(i2, noteTypeId)) {
              result.add(payloadId1);
            }
            getNext1 = true;
            getNext2 = true;
          }
          else if (noteId1 < noteId2) {
            getNext1 = true;
          }
          else {
            getNext2 = true;
          }
        }
        else if (start1 > start2) {
          getNext2 = true;
        }
        else if (start2 > start1) {
          getNext1 = true;
        }
        else if (start1 == start2) {
        }
        if ((!i1.hasNext() && start1 < start2) || (!i2.hasNext() && start2 < start1)) {
          break;
        }
      }
    }

    return new NotesIterator(new RawPayloadIterator(patient, result));
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initNoteTypeId(context);
    LanguageNode child = null;
    NotesIterator result = null;
    NotesIterator resultList = null;
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      if (child == null) {
        child = i.next();
        if (!(child.generatesResultType().equals(PayloadPointers.class))) {
          return new ErrorResult(this, "NOTE() needs to contain operators that return PayloadPointer results");
        }
        final EvaluationResult chE = child.evaluate(context, patient);
        if (chE instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        final PayloadIterator payload = chE.toTimeIntervals(patient).iterator(patient);
        if (!(payload instanceof NotesIterator)) {
          return new ErrorResult(this, "NOTE() needs to contain operators that return NotesPayloadIterator results");
        }
        result = (NotesIterator) payload;
        continue;
      }
      final LanguageNode currentChild = i.next();
      if (!(currentChild.generatesResultType().equals(PayloadPointers.class))) {
        return new ErrorResult(this, "NOTE() needs to contain operators that return PayloadPointer results");
      }
      final EvaluationResult cc = currentChild.evaluate(context, patient);
      if (cc instanceof AbortedResult) {
        return AbortedResult.getInstance();
      }
      final PayloadIterator payload = cc.toTimeIntervals(patient).iterator(patient);
      if (!(payload instanceof NotesIterator)) {
        return new ErrorResult(this, "NOTE() needs to contain operators that return NotesPayloadIterator results");
      }
      final NotesIterator nextResult = (NotesIterator) payload;
      resultList = intersect(patient, new IntArrayList(), result, nextResult, noteTypeId);
      result = resultList;
      if (!resultList.hasNext()) {
        break;
      }
    }
    if (resultList == null && result != null) {
      resultList = result;
    }
    final IntArrayList out = new IntArrayList();
    if (resultList != null) {
      while (resultList.hasNext()) {
        resultList.next();
        if (isCorrectNoteType(resultList, noteTypeId)) {
          out.add(resultList.getStartId());
          out.add(resultList.getEndId());
        }
      }
    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("NOTE(");
    final Iterator<LanguageNode> i = children.iterator();
    if (noteTypeString != null) {
      result.append("NOTE TYPE=\"" + noteTypeString + "\", ");
    }
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final NoteNode result = new NoteNode();
    result.noteTypeString = noteTypeString;
    result.children = cloneChildren();
    return result;
  }

}
