package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class RxNormNode extends LanguageNode implements AtomicNode, CsvNodeType {

  private final int rxNorm;

  public RxNormNode(final int rxNorm) {
    this.rxNorm = rxNorm;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final IntArrayList payloads = patient.getRxNorm(rxNorm);
    if (PayloadPointers.containsInvalidEvents(payloads)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsRxNorm(rxNorm));
    }
    return new PayloadPointers(TYPE.MEDS, this, patient, payloads);
  }

  @Override
  public LanguageNode copy() {
    return new RxNormNode(rxNorm);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "RX=" + rxNorm : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithRxNormCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "RX information is missing in this dataset", this.getClass().getName(), "RX");
    }
    return new AtomPreprocessingNode(statistics.getRxNormPatients(rxNorm));
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueRxNormCodes());

    final IntIterator iterator = sortedSet.iterator();
    int evaluatedStartEndIntervalsPos = 0;
    while (iterator.hasNext()) {
      final int id = iterator.next();
      if (id == rxNorm) {
        final IntArrayList list = patient.getRxNorm(id);
        if (PayloadPointers.containsInvalidEvents(list)) {
          result.append(patient.getId() + separator + toString() + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + separator + "1" + separator
              + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
          continue;
        }
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList startEnd = patient.getPayload(list.get(x));
          String route = "";
          String status = "";
          if (startEnd.size() > PatientBuilder.DRUG_ROUTE_ID_POSITION_IN_PAYLOAD_DATA) {
            route = "ROUTE=" + context.getDrugRouteString(startEnd.get(PatientBuilder.DRUG_ROUTE_ID_POSITION_IN_PAYLOAD_DATA));
          }
          if (startEnd.size() > PatientBuilder.DRUG_STATUS_ID_POSITION_IN_PAYLOAD_DATA) {
            status = "STATUS=" + context.getDrugRouteString(startEnd.get(PatientBuilder.DRUG_STATUS_ID_POSITION_IN_PAYLOAD_DATA));
          }
          if (evaluatedStartEndIntervals == null) {
            result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, startEnd.get(0)) + separator + route + separator + status + separator
                + "1" + separator + Common.minutesToDays(startEnd.get(0)) + separator + Common.minutesToDays(startEnd.get(1)) + separator + "\n");
          }
          else {
            for (int y = evaluatedStartEndIntervalsPos; y < evaluatedStartEndIntervals.size(); y += 2) {
              if (BeforeNode.intersect(startEnd.get(0), startEnd.get(1), evaluatedStartEndIntervals.get(y), evaluatedStartEndIntervals.get(y + 1))) {
                result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, startEnd.get(0)) + separator + route + separator + status + separator
                    + "1" + separator + Common.minutesToDays(startEnd.get(0)) + separator + Common.minutesToDays(startEnd.get(1)) + separator + "\n");
                evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, y);
              }
            }
          }
        }
      }
    }
    return result.toString();
  }

  @Override
  public int hashCode() {
    return rxNorm * 41;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof RxNormNode && ((RxNormNode) obj).rxNorm == rxNorm;
  }

  @Override
  public String[] getWarning() {
    return null;
  }
}