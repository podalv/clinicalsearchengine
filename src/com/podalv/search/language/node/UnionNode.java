package com.podalv.search.language.node;

import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE;
import static com.podalv.search.language.ErrorMessages.TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.IterableResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class UnionNode extends LanguageNodeWithOrChildren implements Cloneable {

  public static PayloadIterator union(final IntArrayList result, final PayloadIterator i1, final PayloadIterator i2) {
    result.clear();
    boolean move1 = true;
    boolean move2 = true;
    boolean finished1 = false;
    boolean finished2 = false;
    while (!finished1 || !finished2) {
      if (move1) {
        if (i1.hasNext()) {
          i1.next();
        }
        else {
          finished1 = true;
        }
      }
      if (move2) {
        if (i2.hasNext()) {
          i2.next();
        }
        else {
          finished2 = true;
        }
      }
      move1 = false;
      move2 = false;
      if (finished1 && !finished2) {
        result.add(i2.getStartId());
        result.add(i2.getEndId());
        move2 = true;
      }
      else if (finished2 && !finished1) {
        result.add(i1.getStartId());
        result.add(i1.getEndId());
        move1 = true;
      }
      else if (!finished1 && !finished2) {
        final int start = Math.min(i1.getStartId(), i2.getStartId());
        int end = Math.min(i1.getEndId(), i2.getEndId());
        // overlap
        if ((i1.getStartId() <= i2.getStartId() && i2.getStartId() <= i1.getEndId()) || (i2.getStartId() <= i1.getStartId() && i1.getStartId() <= i2.getEndId())) {
          end = Math.max(i1.getEndId(), i2.getEndId());
          move1 = true;
          move2 = true;
        }
        else if (i1.getStartId() < i2.getEndId()) {
          move1 = true;
        }
        else {
          move2 = true;
        }
        result.add(start);
        result.add(end);
      }
    }

    return new EmptyPayloadIterator(((IntArrayList) result.clone()).iterator());
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    LanguageNode child = null;
    PayloadIterator result = null;
    PayloadIterator resultList = null;
    final IntArrayList resultArray = new IntArrayList();
    if (children.size() < 2) {
      final EvaluationResult r = children.iterator().next().evaluate(context, patient);
      if (r instanceof AbortedResult) {
        return AbortedResult.getInstance();
      }
      resultList = Common.compressIterator(patient, r.toTimeIntervals(patient).iterator(patient));
    }
    else {
      final Iterator<LanguageNode> i = children.iterator();
      while (i.hasNext()) {
        if (child == null) {
          child = i.next();
          final EvaluationResult c = child.evaluate(context, patient);
          if (c instanceof AbortedResult) {
            return AbortedResult.getInstance();
          }
          result = Common.compressIterator(patient, c.toTimeIntervals(patient).iterator(patient));
          continue;
        }
        final LanguageNode currentChild = i.next();
        final EvaluationResult cc = currentChild.evaluate(context, patient);
        if (cc instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        final IterableResult nextResult = cc.toTimeIntervals(patient);
        resultList = union(resultArray, result, Common.compressIterator(patient, nextResult.iterator(patient)));
        result = resultList;
      }
    }
    final IntArrayList out = new IntArrayList();
    if (resultList != null) {
      while (resultList.hasNext()) {
        resultList.next();
        out.add(resultList.getStartId());
        out.add(resultList.getEndId());
      }

    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("UNION(");
    final Iterator<LanguageNode> i = children.iterator();
    while (i.hasNext()) {
      result.append(i.next().toString());
      if (i.hasNext()) {
        result.append(", ");
      }
    }
    return result.toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final UnionNode result = new UnionNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String[] getWarning() {
    final HashSet<String> result = new HashSet<>();
    if (hasBooleanChildren(children)) {
      result.add(TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT);
    }
    if (hasTimelineChildren(children)) {
      result.add(TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE);
    }
    if (result.size() == 0 && !isTemporalValid(this)) {
      result.add(ErrorMessages.TEMPORAL_COMMAND_INCORRECT);
    }
    return result.toArray(new String[result.size()]);
  }

}