package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.CsvWriter;
import com.podalv.search.datastructures.MeasurementOffHeapData;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.base.AtomicNode;

public class AnyLabsNode extends LanguageNode implements AtomicNode, CsvNodeType {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsLabs());
    }
    final int[] uniqueIds = patient.getUniqueLabIds();
    PayloadIterator ppx = null;
    if (uniqueIds != null & uniqueIds.length > 0) {
      final IntArrayList result = new IntArrayList();
      if (uniqueIds.length == 1) {
        return new TimeIntervals(this, patient.getLabTimes(uniqueIds[0]));
      }
      final IntArrayList list = patient.getLabTimes(uniqueIds[0]);
      if (list != null && PayloadPointers.containsInvalidEvents(list)) {
        return AbortedResult.getInstance();
      }
      ppx = new PayloadPointers(TYPE.LABS_PAYLOADS, this, patient, list).iterator(patient);
      for (int x = 1; x < uniqueIds.length; x++) {
        final IntArrayList l = patient.getLabTimes(uniqueIds[x]);
        if (list != null && PayloadPointers.containsInvalidEvents(l)) {
          return AbortedResult.getInstance();
        }
        final PayloadPointers pp = new PayloadPointers(TYPE.LABS_PAYLOADS, this, patient, l);
        ppx = UnionNode.union(result, pp.iterator(patient), ppx);
      }
    }
    final IntArrayList out = new IntArrayList();
    if (ppx != null) {
      while (ppx.hasNext()) {
        ppx.next();
        out.add(ppx.getStartId());
        out.add(ppx.getEndId());
      }

    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new AnyLabsNode();
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "LABS" : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithLabsCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "LAB information is missing in this dataset", this.getClass().getName(), "LAB");
    }
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  private String getLineRawLabs(final String separator, final IndexCollection context, final Statistics statistics, final Patient patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final String nodeName = toString();
    patient.getLabValues().extract(new CsvWriter() {

      String prevName = null;

      @Override
      public void write(final String name, final int time, final double value) {
        int evaluatedStartEndIntervalsPos = 0;
        if (prevName == null || !prevName.equals(name)) {
          evaluatedStartEndIntervalsPos = 0;
          prevName = name;
        }
        if (MeasurementOffHeapData.isEventInvalid(time)) {
          result.append(patient.getId() + separator + nodeName + "=" + name + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator + "1"
              + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
          return;
        }
        if (evaluatedStartEndIntervals == null) {
          result.append(patient.getId() + separator + nodeName + "=" + name + separator + CsvNode.getYear(patient, time) + separator + "" + separator + value + separator + "1"
              + separator + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
        }
        else {
          for (int z = 0; z < evaluatedStartEndIntervals.size(); z += 2) {
            if (BeforeNode.intersect(time, time, evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
              result.append(patient.getId() + separator + nodeName + "=" + name + separator + CsvNode.getYear(patient, time) + separator + "" + separator + value + separator + "1"
                  + separator + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
              evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
            }
          }
        }
      }
    }, context);
    return result.toString();
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueLabIds());

    final IntIterator iterator = sortedSet.iterator();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      final IntArrayList list = patient.getLabs().getRawData(id);
      int evaluatedStartEndIntervalsPos = 0;
      if (list != null && list.size() != 0) {
        int pos = 0;
        for (int y = pos; y < list.size(); y++) {
          final String value = context.getLabValueText(list.get(pos));
          for (int x = 0; x < list.get(pos + 1); x++) {
            if (evaluatedStartEndIntervals == null) {
              if (list.get(pos + 2 + x) == Integer.MIN_VALUE) {
                result.append(patient.getId() + separator + toString() + "=" + context.getLabName(id) + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE
                    + separator + "" + separator + "1" + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
                continue;
              }
              result.append(patient.getId() + separator + toString() + "=" + context.getLabName(id) + separator + CsvNode.getYear(patient, list.get(pos + 2 + x)) + separator
                  + value + separator + "" + separator + "1" + separator + Common.minutesToDays(list.get(pos + 2 + x)) + separator + Common.minutesToDays(list.get(pos + 2 + x))
                  + separator + "\n");
            }
            else {
              for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
                if (list.get(pos + 2 + x) == Integer.MIN_VALUE) {
                  continue;
                }
                if (BeforeNode.intersect(list.get(pos + 2 + x), list.get(pos + 2 + x), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                  result.append(patient.getId() + separator + toString() + "=" + context.getLabName(id) + separator + CsvNode.getYear(patient, list.get(pos + 2 + x)) + separator
                      + value + separator + "" + separator + "1" + separator + Common.minutesToDays(list.get(pos + 2 + x)) + separator + Common.minutesToDays(list.get(pos + 2 + x))
                      + separator + "\n");
                  evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
                }
              }
            }
          }
          pos += 2 + list.get(pos + 1);
          if (pos > list.size() - 1) {
            break;
          }
        }
      }
    }

    return result.toString() + getLineRawLabs(separator, context, statistics, patient, evaluatedStartEndIntervals);
  }

  @Override
  public int hashCode() {
    return 379;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AnyLabsNode;
  }
}