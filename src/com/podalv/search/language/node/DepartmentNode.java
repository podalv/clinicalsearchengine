package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class DepartmentNode extends LanguageNode implements AtomicNode {

  private final String departmentCode;
  private int          departmentId = Common.UNDEFINED;

  public DepartmentNode(final String departmentString) {
    this.departmentCode = TextNode.trimString(departmentString);
  }

  private void initDepartmentId(final IndexCollection indices) {
    if (departmentId == Common.UNDEFINED) {
      departmentId = indices.getDepartment(departmentCode);
    }
  }

  @Override
  public LanguageNode copy() {
    return new DepartmentNode(departmentCode);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initDepartmentId(context);
    if (departmentId == Common.UNDEFINED) {
      return new PayloadPointers(TYPE.LABS_PAYLOADS, this, patient, new IntArrayList());
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsDepartment(departmentId));
    }
    final IntArrayList payloads = patient.getDepartment(departmentId);
    if (PayloadPointers.containsInvalidEvents(payloads)) {
      return AbortedResult.getInstance();
    }
    return new PayloadPointers(TYPE.LABS_PAYLOADS, this, patient, payloads);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    return "DEPARTMENT=\"" + departmentCode.toUpperCase() + "\"";
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithDepartmentCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "DEPARTMENT information is missing in this dataset", this.getClass().getName(), "DEPARTMENT");
    }
    initDepartmentId(context);
    return new AtomPreprocessingNode(statistics.getDepartmentPatients(departmentId));
  }

  @Override
  public int hashCode() {
    return departmentCode.hashCode() * 293;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof DepartmentNode && ((DepartmentNode) obj).departmentCode.equals(departmentCode);
  }

}