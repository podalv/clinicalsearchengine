package com.podalv.search.language.node;

import java.io.Closeable;
import java.io.IOException;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.server.EventFlowDispatcher;
import com.podalv.search.server.EventFlowPatientExport;

/** Kicks off the Event Flow file generation
 *
 * @author podalv
 *
 */
public class EventFlowNode extends LanguageNodeWithAndChildren implements Closeable {

  private EventFlowPatientExport export = null;

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (export == null) {
      export = EventFlowDispatcher.getInstance().export();
    }
    final EvaluationResult result = children.get(0).evaluate(context, patient);
    if (export != null && !(result instanceof AbortedResult) && result.toBooleanResult().result()) {
      export.export(context, patient);
    }
    return result;
  }

  public EventFlowPatientExport getExport() {
    return export;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public String toString() {
    return "EVENT FLOW(" + children.get(0).toString() + ")";
  }

  @Override
  public LanguageNode copy() {
    final EventFlowNode result = new EventFlowNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public void close() throws IOException {
    if (export != null) {
      export.close();
    }
  }

}
