package com.podalv.search.language.node;

import com.podalv.search.language.node.base.LimitNodeEvaluator;

/** Top level node that limits the number of patients returned
 *
 * @author podalv
 *
 */
public class EvaluateNode extends LimitNode {

  public EvaluateNode(final String limitType) {
    super(limitType);
  }

  @Override
  public LimitNodeEvaluator getEvaluator(final int cachedCnt, final int preprocessedCnt) {
    if (limitType.equals("CACHED") || limitType.equals("CACHE")) {
      return LimitNodeEvaluator.createProcessed(cachedCnt);
    }
    else if (limitType.toUpperCase().indexOf("M") != -1) {
      return LimitNodeEvaluator.createTime(Integer.parseInt(limitType.substring(0, limitType.toUpperCase().indexOf("M")).trim()) * 60000l);
    }
    else if (limitType.toUpperCase().indexOf("S") != -1) {
      return LimitNodeEvaluator.createTime(Integer.parseInt(limitType.substring(0, limitType.toUpperCase().indexOf("S")).trim()) * 1000l);
    }
    else {
      return LimitNodeEvaluator.createProcessed(Integer.parseInt(limitType));
    }
  }

  @Override
  public String toString() {
    return "EVALUATE(" + limitType + "," + children.get(0).toString() + ")";
  }

}
