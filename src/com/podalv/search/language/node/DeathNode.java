package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.preprocessing.datastructures.AtomIterablePreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.Demographics;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimePoint;
import com.podalv.search.language.node.base.AtomicNode;

public class DeathNode extends LanguageNode implements AtomicNode {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final int deathTime = context.getDemographics().getDeathTime(patient.getId());
    return deathTime == Demographics.DEATH_NOT_RECORDED ? TimePoint.createEmpty(this) : new TimePoint(this, deathTime);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimePoint.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (!context.getDemographics().getDeathTimeIterator().hasNext()) {
      throw new MissingResourceException(getPositionInfo() + "DEATH information is missing in this dataset", this.getClass().getName(), "DEATH");
    }
    return new AtomIterablePreprocessingNode(context.getDemographics().getDeathTimeIterator());
  }

  @Override
  public LanguageNode copy() {
    return new DeathNode();
  }

  @Override
  public String toString() {
    return "DEATH";
  }

  @Override
  public int hashCode() {
    return 307;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof DeathNode;
  }
}
