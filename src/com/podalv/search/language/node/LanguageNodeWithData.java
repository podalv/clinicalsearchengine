package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;

/** LanguageNode that can carry some additional Object
 *
 * @author podalv
 *
 */
public class LanguageNodeWithData extends LanguageNode {

  private final LanguageNode node;
  private final Object       object;

  public LanguageNodeWithData(final LanguageNode node, final Object object) {
    this.node = node;
    this.object = object;
  }

  public Object getObject() {
    return object;
  }

  @Override
  public LanguageNode copy() {
    return new LanguageNodeWithData(node.copy(), object);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return node.evaluate(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return node.generatesResultType();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return node.generatePreprocessingNode(context, statistics);
  }

  @Override
  public String toString() {
    return node.toString() + (object == null ? "" : object);
  }
}
