package com.podalv.search.language.node;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.CompressedStartEndIterator;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;

public class CountNode extends LanguageNodeWithAndChildren {

  private final int     minCount;
  private final int     maxCount;
  private final boolean singleType;
  private boolean       returnLeft  = true;
  private boolean       returnRight = false;

  public CountNode(final boolean singleType, final int minCount, final int maxCount) {
    this.singleType = singleType;
    this.minCount = Math.min(minCount, maxCount);
    this.maxCount = Math.max(minCount, maxCount);
  }

  public CountNode(final boolean singleType, final int minCount, final int maxCount, final boolean returnLeft, final boolean returnRight) {
    this.singleType = singleType;
    if (!returnLeft && !returnRight) {
      this.returnLeft = true;
      this.returnRight = false;
    }
    else {
      this.returnLeft = returnLeft;
      this.returnRight = returnRight;
    }
    this.minCount = Math.min(minCount, maxCount);
    this.maxCount = Math.max(minCount, maxCount);
  }

  private IntArrayList mergeResultIntervals(final PatientSearchModel patient, final IntArrayList total, final IntArrayList newIntervals) {
    final PayloadIterator pi = UnionNode.union(new IntArrayList(), new EmptyPayloadIterator(total.iterator()), new EmptyPayloadIterator(newIntervals.iterator()));
    final IntArrayList result = new IntArrayList();
    while (pi.hasNext()) {
      pi.next();
      result.add(pi.getStartId());
      result.add(pi.getEndId());
    }
    return result;
  }

  private EvaluationResult evaluateComplexSingle(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult sections = children.get(1).evaluate(context, patient);
    final EvaluationResult result = children.get(0).evaluate(context, patient);
    if (sections instanceof AbortedResult || result instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final TimeIntervals res = result.toTimeIntervals(patient);
    final TimeIntervals sec = sections.toTimeIntervals(patient);
    if (res != null && sec != null && !res.isEmpty() && !sec.isEmpty()) {
      final PayloadIterator pi = sec.iterator(patient);
      IntArrayList resultIntervals = new IntArrayList();
      while (pi.hasNext()) {
        final PayloadIterator iterator = res.iterator(patient);
        final IntArrayList out = new IntArrayList();
        pi.next();
        int accumulatedCount = 0;
        while (iterator.hasNext()) {
          iterator.next();
          if (iterator.getStartId() <= pi.getEndId() && iterator.getEndId() >= pi.getStartId()) {
            accumulatedCount++;
            if (returnLeft) {
              out.add(iterator.getStartId());
              out.add(iterator.getEndId());
            }
          }
          else if (iterator.getStartId() > pi.getEndId()) {
            break;
          }
        }
        if (accumulatedCount >= minCount && accumulatedCount <= maxCount) {
          resultIntervals = mergeResultIntervals(patient, resultIntervals, out);
          if (returnRight) {
            resultIntervals = mergeResultIntervals(patient, resultIntervals, new IntArrayList(new int[] {pi.getStartId(), pi.getEndId()}));
          }
        }
      }
      if (returnRight && returnLeft) {
        final CompressedStartEndIterator c = new CompressedStartEndIterator(resultIntervals);
        final IntArrayList l = new IntArrayList();
        while (c.hasNext()) {
          c.next();
          l.add(c.getStartId());
          l.add(c.getEndId());
        }
        resultIntervals = l;
      }
      return new TimeIntervals(this, resultIntervals);
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  public static boolean intersect(final PayloadIterator a, final PayloadIterator b) {
    return (b.getStartId() >= a.getStartId() && a.getEndId() >= b.getStartId()) || //
        (a.getStartId() >= b.getStartId() && a.getEndId() <= b.getEndId()) || //
        (b.getStartId() <= a.getStartId() && b.getEndId() >= a.getStartId()) || //
        (b.getStartId() >= a.getStartId() && b.getEndId() <= a.getEndId());
  }

  private EvaluationResult evaluateComplexAll(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult sections = children.get(1).evaluate(context, patient);
    final EvaluationResult result = children.get(0).evaluate(context, patient);
    if (sections instanceof AbortedResult || result instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final TimeIntervals res = result.toTimeIntervals(patient);
    final TimeIntervals sec = sections.toTimeIntervals(patient);
    if (res != null && sec != null && !res.isEmpty() && !sec.isEmpty()) {
      final PayloadIterator pi = sec.iterator(patient);
      IntArrayList resultIntervals = new IntArrayList();
      int accumulatedCount = 0;
      final IntOpenHashSet countedId = new IntOpenHashSet();
      while (pi.hasNext()) {
        final PayloadIterator iterator = res.iterator(patient);
        final IntArrayList out = new IntArrayList();
        pi.next();
        int cnt = 0;
        while (iterator.hasNext()) {
          cnt++;
          iterator.next();
          if (!countedId.contains(cnt) && intersect(iterator, pi)) {
            accumulatedCount++;
            countedId.add(cnt);
            if (returnLeft) {
              out.add(iterator.getStartId());
              out.add(iterator.getEndId());
            }
          }
          else if (iterator.getStartId() > pi.getEndId()) {
            break;
          }
        }
        if (returnRight) {
          resultIntervals = mergeResultIntervals(patient, resultIntervals, new IntArrayList(new int[] {pi.getStartId(), pi.getEndId()}));
        }
        resultIntervals = mergeResultIntervals(patient, resultIntervals, out);
      }
      if (accumulatedCount >= minCount && accumulatedCount <= maxCount) {
        if (returnRight && returnLeft) {
          final CompressedStartEndIterator c = new CompressedStartEndIterator(resultIntervals);
          final IntArrayList l = new IntArrayList();
          while (c.hasNext()) {
            c.next();
            l.add(c.getStartId());
            l.add(c.getEndId());
          }
          resultIntervals = l;
        }
        return new TimeIntervals(this, resultIntervals);
      }
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  private EvaluationResult evaluateSimple(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult result = children.get(0).evaluate(context, patient);
    if (result instanceof AbortedResult) {
      return result;
    }
    if (result != null) {
      int accumulatedCount = 0;
      final PayloadIterator iterator = result.toTimeIntervals(patient).iterator(patient);
      final IntArrayList out = new IntArrayList();
      while (iterator.hasNext()) {
        iterator.next();
        accumulatedCount++;
        out.add(iterator.getStartId());
        out.add(iterator.getEndId());
      }
      if (accumulatedCount >= minCount && accumulatedCount <= maxCount) {
        return new TimeIntervals(this, out);
      }
    }
    return TimeIntervals.getEmptyTimeLine();
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return children.size() == 1 ? evaluateSimple(context, patient) : singleType ? evaluateComplexSingle(context, patient) : evaluateComplexAll(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    final CountNode result = new CountNode(singleType, minCount, maxCount);
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    if (children.size() < 2) {
      return "COUNT(" + children.get(0) + ", " + Common.languageNodeTimeArgumentToText(minCount) + ", " + Common.languageNodeTimeArgumentToText(maxCount) + ")";
    }
    String leftAsterisk = "";
    String rightAsterisk = "";
    if (returnRight) {
      rightAsterisk = "*";
      if (returnLeft) {
        leftAsterisk = "*";
      }
    }
    return "COUNT(" + children.get(0) + leftAsterisk + ", " + children.get(1) + rightAsterisk + ", " + (singleType ? "SINGLE" : "ALL") + ", "
        + Common.languageNodeTimeArgumentToText(minCount) + ", " + Common.languageNodeTimeArgumentToText(maxCount) + ")";
  }

}