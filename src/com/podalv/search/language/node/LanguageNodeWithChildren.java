package com.podalv.search.language.node;

import java.util.ArrayList;

public abstract class LanguageNodeWithChildren extends LanguageNode {

  ArrayList<LanguageNode> children      = new ArrayList<>();
  ArrayList<String>       childrenNames = new ArrayList<>();

  public void addChild(final LanguageNode node, final String name) {
    node.setParent(this);
    children.add(node);
    childrenNames.add(name);
  }

  public ArrayList<String> getChildrenNames() {
    return childrenNames;
  }

  public ArrayList<LanguageNode> getChildren() {
    return children;
  }

  void propagateParent() {
    for (int x = 0; x < children.size(); x++) {
      children.get(x).setParent(this);
      if (children.get(x) instanceof LanguageNodeWithChildren) {
        ((LanguageNodeWithChildren) children.get(x)).propagateParent();
      }
    }
  }

  ArrayList<LanguageNode> cloneChildren() {
    final ArrayList<LanguageNode> result = new ArrayList<>();
    for (int x = 0; x < children.size(); x++) {
      final LanguageNode child = children.get(x).copy();
      child.setParent(this);
      result.add(child);
    }
    propagateParent();
    return result;
  }

  public void addChild(final LanguageNode node) {
    addChild(node, "");
  }

  @Override
  public int hashCode() {
    return children.size();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof LanguageNodeWithChildren) {
      final LanguageNodeWithChildren node = (LanguageNodeWithChildren) obj;
      if (node.children.size() == children.size()) {
        for (int x = 0; x < children.size(); x++) {
          if (!children.get(x).equals(node.children.get(x))) {
            return false;
          }
        }
        return true;
      }
    }
    return false;
  }

}
