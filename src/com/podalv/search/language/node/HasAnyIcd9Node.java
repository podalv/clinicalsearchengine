package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;
import com.podalv.search.language.node.base.AtomicNode;

public class HasAnyIcd9Node extends LanguageNode implements AtomicNode, CsvNodeType {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(!patient.getUniqueIcd9Codes().isEmpty());
    }
    PayloadIterator result = null;
    PayloadIterator resultList = null;
    final IntArrayList resultArray = new IntArrayList();
    if (patient.getUniqueIcd9Codes().size() == 1) {
      final RawPayloadIterator i = new RawPayloadIterator(patient, patient.getIcd9(patient.getUniqueIcd9Codes().get(0)));
      if (i.containsInvalidEvent()) {
        return AbortedResult.getInstance();
      }
      resultList = new RawPayloadIterator(patient, patient.getIcd9(patient.getUniqueIcd9Codes().get(0)));
    }
    else if (patient.getUniqueIcd9Codes().size() > 0) {
      result = new RawPayloadIterator(patient, patient.getIcd9(patient.getUniqueIcd9Codes().get(0)));
      if (result.containsInvalidEvent()) {
        result = null;
      }
      for (int x = 1; x < patient.getUniqueIcd9Codes().size(); x++) {
        final RawPayloadIterator i = new RawPayloadIterator(patient, patient.getIcd9(patient.getUniqueIcd9Codes().get(x)));
        if (result == null) {
          if (!i.containsInvalidEvent()) {
            result = i;
          }
        }
        if (i.containsInvalidEvent()) {
          continue;
        }
        resultList = UnionNode.union(resultArray, result, i);
        result = resultList;
      }
    }
    final IntArrayList out = new IntArrayList();
    if (resultList != null) {
      while (resultList.hasNext()) {
        resultList.next();
        out.add(resultList.getStartId());
        out.add(resultList.getEndId());
      }

    }
    return new TimeIntervals(this, out);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new HasAnyIcd9Node();
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "ICD9" : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithIcd9Cnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "ICD9 information is missing in this dataset", this.getClass().getName(), "ICD9");
    }
    return new AtomPreprocessingNode(statistics.getPatientIdIterator());
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueIcd9Codes());
    int evaluatedStartEndIntervalsPos = 0;
    final IntIterator iterator = sortedSet.iterator();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      final IntArrayList list = patient.getIcd9(id);
      if (PayloadPointers.containsInvalidEvents(list)) {
        result.append(patient.getId() + separator + toString() + "=" + context.getIcd9(id) + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator
            + separator + "1" + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
        continue;
      }
      for (int x = 0; x < list.size(); x++) {
        final IntArrayList payload = patient.getPayload(list.get(x));
        String primary = "";
        if (payload.size() > PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA) {
          primary = PatientBuilder.isPrimaryIcdCode(payload.get(PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA)) ? "PRIMARY=TRUE" : "PRIMARY=FALSE";
        }
        if (evaluatedStartEndIntervals == null) {
          result.append(patient.getId() + separator + toString() + "=" + context.getIcd9(id) + separator + CsvNode.getYear(patient, payload.get(0)) + separator + primary
              + separator + separator + "1" + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
        }
        else {
          for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
            if (BeforeNode.intersect(payload.get(0), payload.get(1), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
              result.append(patient.getId() + separator + toString() + "=" + context.getIcd9(id) + separator + CsvNode.getYear(patient, payload.get(0)) + separator + primary
                  + separator + separator + "1" + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
              evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
            }
          }
        }
      }
    }
    return result.toString();
  }

  @Override
  public int hashCode() {
    return 191;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof HasAnyIcd9Node;
  }
}