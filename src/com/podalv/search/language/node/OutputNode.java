package com.podalv.search.language.node;

import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;

/** Kicks off the Event Flow file generation
*
* @author podalv
*
*/
public class OutputNode extends LanguageNodeWithOrChildren {

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return children.get(0).evaluate(context, patient);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    final OutputNode result = new OutputNode();
    result.children = cloneChildren();
    return result;
  }

  @Override
  public String toString() {
    return "OUTPUT(" + children.get(0).toString() + ")";
  }

}