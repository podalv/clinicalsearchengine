package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class TextNode extends LanguageNode implements AtomicNode {

  final String text;
  int          tid = -1;

  public static String trimString(final String str) {
    if (str != null && str.length() > 1 && (str.charAt(0) == '"' || str.charAt(0) == '“' || str.charAt(0) == '”') && (str.charAt(str.length() - 1) == '"' || str.charAt(str.length()
        - 1) == '”' || str.charAt(str.length() - 1) == '“')) {
      return str.substring(1, str.length() - 1).toLowerCase().trim();
    }
    return str == null ? null : str.toLowerCase().trim();
  }

  public static String trimString(final String str, final boolean toLowerCase) {
    if (str != null && str.length() > 1 && (str.charAt(0) == '"' || str.charAt(0) == '“' || str.charAt(0) == '”') && (str.charAt(str.length() - 1) == '"' || str.charAt(str.length()
        - 1) == '”' || str.charAt(str.length() - 1) == '“')) {
      return str.substring(1, str.length() - 1);
    }
    return str == null ? null : str.trim();
  }

  public TextNode(final String text) {
    this.text = trimString(text);
  }

  public String getText() {
    return text;
  }

  public int getTid() {
    return tid;
  }

  void initTid(final IndexCollection context) {
    if (tid == Common.UNDEFINED) {
      this.tid = context.getTid(text);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initTid(context);
    final IntArrayList payloads = patient.getPositiveTerms(tid);
    if (PayloadPointers.containsInvalidEvents(payloads)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsPositiveTid(tid));
    }
    return new PayloadPointers(TYPE.NOTE_RAW, this, patient, payloads);
  }

  @Override
  public LanguageNode copy() {
    return new TextNode(text);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    return "TEXT=\"" + text + "\"";
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithTidCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "TEXT information is missing in this dataset", this.getClass().getName(), "TEXT");
    }
    initTid(context);
    return new AtomPreprocessingNode(statistics.getTidPatients(tid));
  }

  @Override
  public int hashCode() {
    return text.hashCode() * 23;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof TextNode && ((TextNode) obj).text.equals(text);
  }
}
