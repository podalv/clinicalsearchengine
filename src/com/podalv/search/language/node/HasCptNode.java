package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class HasCptNode extends LanguageNode implements AtomicNode {

  private final String cpt;
  private int          cptIndex = Integer.MIN_VALUE;

  public HasCptNode(final String cpt) {
    this.cpt = cpt;
  }

  public String getCpt() {
    return cpt;
  }

  private void parseCpt(final IndexCollection context) {
    if (cptIndex == Integer.MIN_VALUE) {
      if (context.containsCpt(cpt)) {
        cptIndex = context.getCpt(cpt);
      }
      else {
        cptIndex = Common.UNDEFINED;
      }
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    parseCpt(context);
    if (cptIndex == Common.UNDEFINED) {
      return new PayloadPointers(TYPE.CPT_RAW, this, patient, new IntArrayList());
    }
    final IntArrayList payload = patient.getCpt(cptIndex);
    if (PayloadPointers.containsInvalidEvents(payload)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsCpt(cptIndex));
    }
    return new PayloadPointers(TYPE.CPT_RAW, this, patient, payload);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    return "CPT=" + cpt;
  }

  @Override
  public LanguageNode copy() {
    return new HasCptNode(cpt);
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithCptCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "CPT code information is missing in this dataset", this.getClass().getName(), "CPT");
    }
    parseCpt(context);
    return new AtomPreprocessingNode(statistics.getCptPatients(cptIndex));
  }

  @Override
  public int hashCode() {
    return cpt.hashCode() * 181;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof HasCptNode && ((HasCptNode) obj).cpt.equals(cpt);
  }
}