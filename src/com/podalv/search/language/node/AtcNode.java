package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.base.AtomicNode;

public class AtcNode extends LanguageNode implements AtomicNode {

  private int          atcCode = -1;
  private final String atcString;

  public AtcNode(final String atcString) {
    this.atcString = TextNode.trimString(atcString);
  }

  private void initAtcCode(final IndexCollection context) {
    if (atcCode == -1) {
      atcCode = context.getAtcCode(atcString);
    }
  }

  public String getAtcString() {
    return atcString;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initAtcCode(context);
    final IntArrayList rxNormCodes = patient.getRxNormCodesFromAtc(atcCode);
    if (rxNormCodes != null && rxNormCodes.size() == 1) {
      final IntArrayList payloads = patient.getRxNorm(rxNormCodes.get(0));
      if (PayloadPointers.containsInvalidEvents(payloads)) {
        return AbortedResult.getInstance();
      }
      if (parent.generatesResultType().equals(BooleanResult.class)) {
        return BooleanResult.getTrue();
      }
      return new PayloadPointers(TYPE.MEDS, this, patient, payloads);
    }
    else if (rxNormCodes != null && rxNormCodes.size() > 1) {
      final IntArrayList payloads = patient.getRxNorm(rxNormCodes.get(0));
      if (PayloadPointers.containsInvalidEvents(payloads)) {
        return AbortedResult.getInstance();
      }
      final IntArrayList tempList = new IntArrayList();
      final IntArrayList r = patient.getRxNorm(rxNormCodes.get(0));
      if (PayloadPointers.containsInvalidEvents(r)) {
        return AbortedResult.getInstance();
      }
      PayloadIterator result = new PayloadPointers(TYPE.MEDS, this, patient, r).iterator(patient);
      for (int x = 1; x < rxNormCodes.size(); x++) {
        final IntArrayList r1 = patient.getRxNorm(rxNormCodes.get(x));
        if (PayloadPointers.containsInvalidEvents(r1)) {
          return AbortedResult.getInstance();
        }
        result = UnionNode.union(tempList, result, new PayloadPointers(TYPE.MEDS, this, patient, r1).iterator(patient));
      }
      if (parent == null || parent.generatesResultType().equals(BooleanResult.class)) {
        return BooleanResult.getTrue();
      }
      final IntArrayList out = new IntArrayList();
      while (result.hasNext()) {
        result.next();
        out.add(result.getStartId());
        out.add(result.getEndId());
      }
      return new TimeIntervals(this, out);
    }

    return TimeIntervals.getEmptyTimeLine();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithAtcCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "ATC information is missing in this dataset", this.getClass().getName(), "ATC");
    }
    initAtcCode(context);
    return new AtomPreprocessingNode(statistics.getAtcPatients(atcCode));
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    return new AtcNode(atcString);
  }

  @Override
  public String toString() {
    return "ATC=\"" + atcString + "\"";
  }

  @Override
  public int hashCode() {
    return atcString.hashCode() * 349;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AtcNode && ((AtcNode) obj).atcString.equals(atcString);
  }
}