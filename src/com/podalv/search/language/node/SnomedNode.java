package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class SnomedNode extends LanguageNode implements AtomicNode {

  private final String snomedCode;
  private int          snomedId = Common.UNDEFINED;

  public SnomedNode(final String snomedString) {
    this.snomedCode = TextNode.trimString(snomedString);
  }

  private void initSnomedId(final IndexCollection indices) {
    if (snomedId == Common.UNDEFINED) {
      snomedId = indices.getSnomed(snomedCode);
    }
  }

  @Override
  public LanguageNode copy() {
    return new SnomedNode(snomedCode);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initSnomedId(context);
    if (snomedId == Common.UNDEFINED) {
      return new PayloadPointers(TYPE.CPT_RAW, this, patient, new IntArrayList());
    }
    final IntArrayList payloads = patient.getSnomed(snomedId);
    if (PayloadPointers.containsInvalidEvents(payloads)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsSnomed(snomedId));
    }
    return new PayloadPointers(TYPE.CPT_RAW, this, patient, payloads);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public String toString() {
    return "SNOMED=" + snomedCode;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithSnomedCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "SNOMED information is missing in this dataset", this.getClass().getName(), "SNOMED");
    }
    initSnomedId(context);
    return new AtomPreprocessingNode(statistics.getSnomedPatients(snomedId));
  }

  @Override
  public int hashCode() {
    return snomedCode.hashCode() * 31;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof SnomedNode && ((SnomedNode) obj).snomedCode.equals(snomedCode);
  }

}
