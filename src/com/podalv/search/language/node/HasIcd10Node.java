package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class HasIcd10Node extends LanguageNode implements AtomicNode, CsvNodeType {

  private final String icd10;
  private int          icd10Index = Integer.MIN_VALUE;

  public HasIcd10Node(final String icd10) {
    this.icd10 = icd10;
  }

  public int getIcd10Index() {
    return icd10Index;
  }

  public String getIcd10() {
    return icd10;
  }

  private void parseIcd10(final IndexCollection context) {
    if (icd10Index == Integer.MIN_VALUE) {
      if (context.containsIcd10(icd10)) {
        icd10Index = context.getIcd10(icd10);
      }
      else {
        icd10Index = Common.UNDEFINED;
      }
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    parseIcd10(context);
    if (icd10Index == Common.UNDEFINED) {
      return new PayloadPointers(TYPE.ICD9_RAW, this, patient, new IntArrayList());
    }
    final IntArrayList payload = patient.getIcd10(icd10Index);
    if (PayloadPointers.containsInvalidEvents(payload)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsIcd10(icd10Index));
    }
    return new PayloadPointers(TYPE.ICD9_RAW, this, patient, payload);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new HasIcd10Node(icd10);
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "ICD10=" + icd10 : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithIcd10Cnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "ICD10 information is missing in this dataset", this.getClass().getName(), "ICD10");
    }
    parseIcd10(context);
    return new AtomPreprocessingNode(statistics.getIcd10Patients(icd10Index));
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    parseIcd10(context);
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueIcd10Codes());
    int evaluatedStartEndIntervalsPos = 0;
    final IntIterator iterator = sortedSet.iterator();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      if (id == icd10Index) {
        final IntArrayList list = patient.getIcd10(id);
        if (PayloadPointers.containsInvalidEvents(list)) {
          result.append(patient.getId() + separator + toString() + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + separator + "1" + separator
              + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
          continue;
        }
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList payload = patient.getPayload(list.get(x));
          String primary = "";
          if (payload.size() > PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA) {
            primary = PatientBuilder.isPrimaryIcdCode(payload.get(PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA)) ? "PRIMARY=TRUE" : "PRIMARY=FALSE";
          }
          if (evaluatedStartEndIntervals == null) {
            result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, payload.get(0)) + separator + primary + separator + separator + "1"
                + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
          }
          else {
            for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
              if (BeforeNode.intersect(payload.get(0), payload.get(1), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, payload.get(0)) + separator + primary + separator + separator + "1"
                    + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
                evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
              }
            }
          }
        }
      }
    }
    return result.toString();
  }

  @Override
  public int hashCode() {
    return icd10.hashCode() * 179;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof HasIcd10Node && ((HasIcd10Node) obj).icd10.equals(icd10);
  }
}