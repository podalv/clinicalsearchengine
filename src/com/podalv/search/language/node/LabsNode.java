package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.CsvWriter;
import com.podalv.search.datastructures.MeasurementOffHeapData;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.base.AtomicNode;

public class LabsNode extends LanguageNode implements AtomicNode, CsvNodeType {

  private final String codeName;
  private int          codeId         = -1;
  private String       parsedCodeName = null;
  private int          expectedValue  = -1;
  private final String expectedValueString;

  public LabsNode(final String codeName) {
    this.codeName = TextNode.trimString(codeName);
    this.expectedValueString = null;
  }

  public LabsNode(final String codeName, final String expectedValue) {
    this.codeName = TextNode.trimString(codeName);
    this.expectedValueString = TextNode.trimString(expectedValue).toUpperCase();
  }

  private void parseCodeName(final IndexCollection context) {
    if (codeId == -1) {
      codeId = context.getLabsCode(codeName);
      parsedCodeName = (codeId == Integer.MIN_VALUE) ? "UNDEFINED" : context.getLabName(codeId);
    }
    if (expectedValueString != null && expectedValue == -1) {
      expectedValue = context.getLabValueId(expectedValueString);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    parseCodeName(context);
    IntArrayList result = null;
    if (expectedValue >= 0) {
      result = patient.getLabs().get(codeId, expectedValue);
    }
    else {
      result = patient.getLabTimes(codeId);
    }
    if (result != null && PayloadPointers.containsInvalidEvents(result)) {
      return AbortedResult.getInstance();
    }
    return (result == null || result.size() == 0) ? TimeIntervals.getEmptyTimeLine() : new PayloadPointers(TYPE.LABS_PAYLOADS, this, patient, result);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public LanguageNode copy() {
    return new LabsNode(codeName, expectedValueString);
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithLabsCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "LAB information is missing in this dataset", this.getClass().getName(), "LAB");
    }
    parseCodeName(context);
    return new AtomPreprocessingNode(statistics.getLabsPatients(codeId));
  }

  @Override
  public String toString() {
    if (getNodeName() == null) {
      if (expectedValueString == null) {
        return "LABS(\"" + codeName + "\")";
      }
      return "LABS(\"" + codeName + "\", \"" + expectedValueString + "\")";
    }
    return getNodeName();
  }

  private String getLineRawLabs(final String separator, final IndexCollection context, final Statistics statistics, final Patient patient,
      final IntArrayList evaluatedStartEndIntervals) {
    final StringBuilder result = new StringBuilder();
    final String nodeName = toString();
    patient.getLabValues().extract(new CsvWriter() {

      int evaluatedStartEndIntervalsPos = 0;

      @Override
      public void write(final String name, final int time, final double value) {
        if (parsedCodeName != null && name != null && name.equals(parsedCodeName)) {
          if (evaluatedStartEndIntervals == null) {
            if (MeasurementOffHeapData.isEventInvalid(time)) {
              result.append(patient.getId() + separator + nodeName + "=" + name + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator
                  + "1" + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
              return;
            }
            else {
              result.append(patient.getId() + separator + nodeName + separator + CsvNode.getYear(patient, time) + separator + "" + separator + value + separator + "1" + separator
                  + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
            }
          }
          else {
            for (int z = 0; z < evaluatedStartEndIntervals.size(); z += 2) {
              if (MeasurementOffHeapData.isEventInvalid(time)) {
                result.append(patient.getId() + separator + nodeName + "=" + name + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator
                    + "1" + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
                return;
              }
              else {
                if (BeforeNode.intersect(time, time, evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                  result.append(patient.getId() + separator + nodeName + separator + CsvNode.getYear(patient, time) + separator + "" + separator + value + separator + "1"
                      + separator + Common.minutesToDays(time) + separator + Common.minutesToDays(time) + separator + "\n");
                  evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
                }
              }
            }
          }
        }
      }
    }, context);
    return result.toString();
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    parseCodeName(context);
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueLabIds());

    final IntIterator iterator = sortedSet.iterator();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      if (id == codeId) {
        final IntArrayList list = patient.getLabs().getRawData(id);
        int evaluatedStartEndIntervalsPos = 0;
        if (list != null && list.size() != 0) {
          int pos = 0;
          for (int y = pos; y < list.size(); y++) {
            final String value = context.getLabValueText(list.get(pos));
            for (int x = 0; x < list.get(pos + 1); x++) {
              if (evaluatedStartEndIntervals == null) {
                if (list.get(pos + 2 + x) == Integer.MIN_VALUE) {
                  result.append(patient.getId() + separator + toString() + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + "" + separator + "1"
                      + separator + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
                  continue;
                }
                result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, list.get(pos + 2 + x)) + separator + value + separator + ""
                    + separator + "1" + separator + Common.minutesToDays(list.get(pos + 2 + x)) + separator + Common.minutesToDays(list.get(pos + 2 + x)) + separator + "\n");
              }
              else {
                for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
                  if (list.get(pos + 2 + x) == Integer.MIN_VALUE) {
                    continue;
                  }
                  if (BeforeNode.intersect(list.get(pos + 2 + x), list.get(pos + 2 + x), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                    result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, list.get(pos + 2 + x)) + separator + value + separator + ""
                        + separator + "1" + separator + Common.minutesToDays(list.get(pos + 2 + x)) + separator + Common.minutesToDays(list.get(pos + 2 + x)) + separator + "\n");
                    evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
                  }
                }
              }
            }
            pos += 2 + list.get(pos + 1);
            if (pos > list.size() - 1) {
              break;
            }
          }
        }
      }
    }

    return result.toString() + getLineRawLabs(separator, context, statistics, patient, evaluatedStartEndIntervals);
  }

  @Override
  public int hashCode() {
    return codeName.hashCode() * 139;
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }
}