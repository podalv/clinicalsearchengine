package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.node.base.AtomicNode;

public class HasIcd9Node extends LanguageNode implements AtomicNode, CsvNodeType {

  private final String icd9;
  private int          icd9Index = Integer.MIN_VALUE;

  public HasIcd9Node(final String icd9) {
    this.icd9 = icd9;
  }

  public int getIcd9Index() {
    return icd9Index;
  }

  public String getIcd9() {
    return icd9;
  }

  private void parseIcd9(final IndexCollection context) {
    if (icd9Index == Integer.MIN_VALUE) {
      if (context.containsIcd9(icd9)) {
        icd9Index = context.getIcd9(icd9);
      }
      else {
        icd9Index = Common.UNDEFINED;
      }
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    parseIcd9(context);
    if (icd9Index == Common.UNDEFINED) {
      return new PayloadPointers(TYPE.ICD9_RAW, this, patient, new IntArrayList());
    }
    final IntArrayList payload = patient.getIcd9(icd9Index);
    if (PayloadPointers.containsInvalidEvents(payload)) {
      return AbortedResult.getInstance();
    }
    if (parent.generatesResultType().equals(BooleanResult.class)) {
      return BooleanResult.getInstance(patient.containsIcd9(icd9Index));
    }
    return new PayloadPointers(TYPE.ICD9_RAW, this, patient, payload);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return PayloadPointers.class;
  }

  @Override
  public LanguageNode copy() {
    return new HasIcd9Node(icd9);
  }

  @Override
  public String toString() {
    return getNodeName() == null ? "ICD9=" + icd9 : getNodeName();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithIcd9Cnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "ICD9 information is missing in this dataset", this.getClass().getName(), "ICD9");
    }
    parseIcd9(context);
    return new AtomPreprocessingNode(statistics.getIcd9Patients(icd9Index));
  }

  @Override
  public String getLine(final String separator, final IndexCollection context, final Statistics statistics, final PatientSearchModel patient,
      final IntArrayList evaluatedStartEndIntervals) {
    parseIcd9(context);
    final StringBuilder result = new StringBuilder();
    final IntOpenHashSet sortedSet = new IntOpenHashSet(patient.getUniqueIcd9Codes());
    int evaluatedStartEndIntervalsPos = 0;
    final IntIterator iterator = sortedSet.iterator();
    while (iterator.hasNext()) {
      final int id = iterator.next();
      if (id == icd9Index) {
        final IntArrayList list = patient.getIcd9(id);
        if (PayloadPointers.containsInvalidEvents(list)) {
          result.append(patient.getId() + separator + toString() + separator + Common.NOT_AVAILABLE + separator + Common.INVALID_CODE + separator + separator + "1" + separator
              + Common.NOT_AVAILABLE + separator + Common.NOT_AVAILABLE + separator + "\n");
          continue;
        }
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList payload = patient.getPayload(list.get(x));
          String primary = "";
          if (payload.size() > PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA) {
            primary = PatientBuilder.isPrimaryIcdCode(payload.get(PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA)) ? "PRIMARY=TRUE" : "PRIMARY=FALSE";
          }
          if (evaluatedStartEndIntervals == null) {
            result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, payload.get(0)) + separator + primary + separator + separator + "1"
                + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
          }
          else {
            for (int z = evaluatedStartEndIntervalsPos; z < evaluatedStartEndIntervals.size(); z += 2) {
              if (BeforeNode.intersect(payload.get(0), payload.get(1), evaluatedStartEndIntervals.get(z), evaluatedStartEndIntervals.get(z + 1))) {
                result.append(patient.getId() + separator + toString() + separator + CsvNode.getYear(patient, payload.get(0)) + separator + primary + separator + separator + "1"
                    + separator + Common.minutesToDays(payload.get(0)) + separator + Common.minutesToDays(payload.get(1)) + separator + "\n");
                evaluatedStartEndIntervalsPos = Math.max(evaluatedStartEndIntervalsPos, z);
              }
            }
          }
        }
      }
    }
    return result.toString();
  }

  @Override
  public int hashCode() {
    return icd9.hashCode() * 173;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof HasIcd9Node && ((HasIcd9Node) obj).icd9.equals(icd9);
  }
}