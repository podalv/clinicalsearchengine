package com.podalv.search.language.node;

import com.podalv.preprocessing.datastructures.AtomIterablePreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.base.AtomicNode;

public class GenderNode extends LanguageNode implements AtomicNode {

  private final String gender;
  private int          genderId = -1;

  public GenderNode(final String gender) {
    this.gender = TextNode.trimString(gender).toUpperCase();
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    processGenderId(context);
    return BooleanResult.getInstance(context.getDemographics().containsGender(patient.getId(), genderId));
  }

  private void processGenderId(final IndexCollection context) {
    if (genderId == -1) {
      this.genderId = context.getDemographics().getGender().getId(gender);
    }
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public String toString() {
    return "GENDER=\"" + gender + "\"";
  }

  @Override
  public LanguageNode copy() {
    return new GenderNode(gender);
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    processGenderId(context);
    return new AtomIterablePreprocessingNode(context.getGenderIterator(genderId));
  }

  @Override
  public int hashCode() {
    return gender.hashCode() * 199;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof GenderNode && ((GenderNode) obj).gender.equals(gender);
  }

}
