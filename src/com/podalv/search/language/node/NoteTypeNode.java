package com.podalv.search.language.node;

import java.util.MissingResourceException;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.TimeIntervals;

public class NoteTypeNode extends LanguageNode {

  private final String noteTypeString;
  private int          noteTypeId = Integer.MIN_VALUE;

  public NoteTypeNode(final String noteTypeString) {
    this.noteTypeString = TextNode.trimString(noteTypeString);
  }

  public String getNoteTypeString() {
    return noteTypeString;
  }

  private void initNoteTypeId(final IndexCollection context) {
    if (noteTypeId == Integer.MIN_VALUE) {
      noteTypeId = context.getNoteTypeId(noteTypeString);
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initNoteTypeId(context);
    final IntArrayList time = patient.getNoteTimePointsFromNoteType(noteTypeId);
    if (PayloadPointers.containsInvalidEvents(time)) {
      return AbortedResult.getInstance();
    }
    return new PayloadPointers(TYPE.SINGLE_TIME_POINT, this, patient, time);
  }

  @Override
  public LanguageNode copy() {
    return new NoteTypeNode(noteTypeString);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    if (statistics.getPatientsWithNoteTypeCnt() == 0) {
      throw new MissingResourceException(getPositionInfo() + "NOTE TYPE information is missing in this dataset", this.getClass().getName(), "NOTE TYPE");
    }
    initNoteTypeId(context);
    return new AtomPreprocessingNode(statistics.getNoteTypesPatients(noteTypeId));
  }

  @Override
  public String toString() {
    return "NOTE TYPE=\"" + noteTypeString + "\"";
  }

  @Override
  public int hashCode() {
    return noteTypeString.hashCode() * 83;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof NoteTypeNode && ((NoteTypeNode) obj).noteTypeString.equals(noteTypeString);
  }
}