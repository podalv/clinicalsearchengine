package com.podalv.search.language.node.base;

/** Node that is not supposed to have children
 *
 * @author podalv
 *
 */
public interface AtomicNode {

}
