package com.podalv.search.language.node.base;

/** Must have exactly 1 child
 *
 * @author podalv
 *
 */
public interface SingleChildNode {

}
