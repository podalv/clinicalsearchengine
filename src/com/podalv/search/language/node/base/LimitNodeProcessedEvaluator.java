package com.podalv.search.language.node.base;

public class LimitNodeProcessedEvaluator extends LimitNodeEvaluator {

  private final long processedLimit;

  public LimitNodeProcessedEvaluator(final long processedLimit) {
    this.processedLimit = processedLimit;
  }

  @Override
  public boolean terminate(final boolean evaluationResult) {
    processed++;
    return processed >= processedLimit;
  }
}
