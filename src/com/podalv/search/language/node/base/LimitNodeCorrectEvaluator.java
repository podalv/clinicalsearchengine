package com.podalv.search.language.node.base;

public class LimitNodeCorrectEvaluator extends LimitNodeEvaluator {

  private final long correctToEvaluate;

  public LimitNodeCorrectEvaluator(final long correctToEvaluate) {
    this.correctToEvaluate = correctToEvaluate;
  }

  @Override
  public boolean terminate(final boolean evaluationResult) {
    if (evaluationResult) {
      correct++;
    }
    return correct >= correctToEvaluate;
  }

}
