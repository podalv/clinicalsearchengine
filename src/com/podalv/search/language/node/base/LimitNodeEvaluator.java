package com.podalv.search.language.node.base;

/** Evaluates LIMIT, ESTIMATE, EVALUATE nodes to see whether to continue evaluating or whether to terminate
 *
 * @author podalv
 *
 */
public abstract class LimitNodeEvaluator {

  long processed = 0;
  long correct   = 0;

  public abstract boolean terminate(boolean evaluationResult);

  public static LimitNodeEvaluator createProcessed(final long processed) {
    return new LimitNodeProcessedEvaluator(processed);
  }

  public static LimitNodeEvaluator createDisabled() {
    return new DisabledLimitNodeEvaluator();
  }

  public static LimitNodeEvaluator createCorrect(final long correct) {
    return new LimitNodeCorrectEvaluator(correct);
  }

  public static LimitNodeEvaluator createTime(final long milliseconds) {
    return new LimitNodeTimeEvaluator(milliseconds);
  }

}
