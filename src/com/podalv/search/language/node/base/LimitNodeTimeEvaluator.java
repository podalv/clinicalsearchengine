package com.podalv.search.language.node.base;

public class LimitNodeTimeEvaluator extends LimitNodeEvaluator {

  private final long processTime;
  private long       start = -1;

  public LimitNodeTimeEvaluator(final long milliseconds) {
    this.processTime = milliseconds;
  }

  @Override
  public boolean terminate(final boolean evaluationResult) {
    if (start == -1) {
      start = System.currentTimeMillis();
    }
    return (System.currentTimeMillis() - start >= processTime);
  }

}
