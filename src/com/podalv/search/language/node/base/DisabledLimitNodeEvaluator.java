package com.podalv.search.language.node.base;

public class DisabledLimitNodeEvaluator extends LimitNodeEvaluator {

  @Override
  public boolean terminate(final boolean evaluationResult) {
    return false;
  }

}
