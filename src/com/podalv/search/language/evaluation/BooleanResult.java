package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.node.LanguageNode;

/** Returns true / false
 *
 * @author podalv
 *
 */
public class BooleanResult extends EvaluationResult {

  private final boolean        result;
  private static BooleanResult FALSE = new BooleanResult(null, false);
  private static BooleanResult TRUE  = new BooleanResult(null, true);

  public static BooleanResult getFalse() {
    return FALSE;
  }

  public static BooleanResult getTrue() {
    return TRUE;
  }

  public static BooleanResult getInstance(final boolean value) {
    return value ? getTrue() : getFalse();
  }

  private BooleanResult(final LanguageNode node, final boolean result) {
    super(node);
    this.result = result;
  }

  public boolean result() {
    return this.result;
  }

  @Override
  public BooleanResult toBooleanResult() {
    return this;
  }

  @Override
  public TimePoint toTimePoint() {
    throw new UnsupportedOperationException("Conversion from boolean to Time Point is undefined");
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    return result ? new TimeIntervals(node, patient.getStartTime(), patient.getEndTime()) : TimeIntervals.getEmptyTimeLine();
  }

}
