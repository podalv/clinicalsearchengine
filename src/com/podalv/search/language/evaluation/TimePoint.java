package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.node.LanguageNode;

/** A set of time points
 *
 * @author podalv
 *
 */
public class TimePoint extends EvaluationResult {

  final int                      timePoint;
  boolean                        empty = false;
  private static final TimePoint EMPTY = new TimePoint(null, -1);

  public static TimePoint createEmpty(final LanguageNode node) {
    return EMPTY;
  }

  public TimePoint(final LanguageNode node, final int timePoint) {
    super(node);
    this.empty = timePoint == -1 ? true : false;
    this.timePoint = timePoint;
  }

  public boolean isEmpty() {
    return empty;
  }

  public int getTimePoint() {
    return timePoint;
  }

  @Override
  public BooleanResult toBooleanResult() {
    return isEmpty() ? BooleanResult.getFalse() : BooleanResult.getTrue();
  }

  @Override
  public TimePoint toTimePoint() {
    return this;
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    if (!isEmpty()) {
      return new TimeIntervals(getNode(), timePoint, timePoint);
    }
    return TimeIntervals.getEmptyTimeLine();
  }

}
