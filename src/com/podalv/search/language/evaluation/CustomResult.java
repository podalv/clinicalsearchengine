package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.Patient;

public class CustomResult extends EvaluationResult {

  private final Object object;

  public CustomResult(final Object object) {
    super(null);
    this.object = object;
  }

  public Object getObject() {
    return object;
  }

  @Override
  public BooleanResult toBooleanResult() {
    throw new UnsupportedOperationException();
  }

  @Override
  public TimePoint toTimePoint() {
    throw new UnsupportedOperationException();
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    throw new UnsupportedOperationException();
  }

}
