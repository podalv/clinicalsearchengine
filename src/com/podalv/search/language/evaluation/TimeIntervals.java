package com.podalv.search.language.evaluation;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.ObjectWithPayload;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.evaluation.iterators.EmptyPayloadIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.LanguageNode;

/** A set of time intervals, they contain payload as well as start / end.
 *  The structure of the array is: start, end, payload, start, end, payload ...
 *  Once the first time operator is invoked, these will be converted to TimeIntervals without payload
 *
 * @author podalv
 *
 */
public class TimeIntervals extends IterableResult {

  IntArrayList                       result;
  private static final TimeIntervals EMPTY_TIMELINE = new TimeIntervals(null);

  public boolean isEmpty() {
    return result == null || result.size() == 0;
  }

  public static TimeIntervals getEmptyTimeLine() {
    return EMPTY_TIMELINE;
  }

  public TimeIntervals(final LanguageNode node) {
    super(node);
  }

  public TimeIntervals(final LanguageNode node, final IntArrayList iterator) {
    super(node);
    this.result = iterator;
  }

  public TimeIntervals(final LanguageNode node, final int start, final int end) {
    super(node);
    result = new IntArrayList(new int[] {start, end});
  }

  public void set(final IntArrayList contents) {
    result = contents;
  }

  public TimeIntervals getClone() {
    final TimeIntervals result = new TimeIntervals(node);
    if (this.result == null) {
      result.result = null;
    }
    else {
      result.result = new IntArrayList(this.result.toArray());
    }
    return result;
  }

  @Override
  public BooleanResult toBooleanResult() {
    return (result == null || result.size() == 0) ? BooleanResult.getFalse() : BooleanResult.getTrue();
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    return this;
  }

  @Override
  public TimePoint toTimePoint() {
    throw new UnsupportedOperationException("Cast from Time Interval to Time Point is undefined");
  }

  @Override
  public PayloadIterator iterator(final ObjectWithPayload patient) {
    return (result == null) ? new EmptyPayloadIterator() : new EmptyPayloadIterator(result.iterator());
  }
}
