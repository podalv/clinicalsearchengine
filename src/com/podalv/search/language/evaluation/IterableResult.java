package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.ObjectWithPayload;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.LanguageNode;

public abstract class IterableResult extends EvaluationResult {

  public IterableResult(final LanguageNode node) {
    super(node);
  }

  public abstract PayloadIterator iterator(final ObjectWithPayload patient);
}
