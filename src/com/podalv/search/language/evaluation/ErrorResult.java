package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.node.LanguageNode;

/** Placeholder to populate Error message to the ROOT
 *
 * @author podalv
 *
 */
public class ErrorResult extends TimeIntervals {

  private final String errorMessage;

  public ErrorResult(final LanguageNode node, final String error) {
    super(node);
    this.errorMessage = error;
  }

  @Override
  public BooleanResult toBooleanResult() {
    return BooleanResult.getFalse();
  }

  @Override
  public TimePoint toTimePoint() {
    return TimePoint.createEmpty(getNode());
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    return new ErrorResult(getNode(), errorMessage);
  }

}
