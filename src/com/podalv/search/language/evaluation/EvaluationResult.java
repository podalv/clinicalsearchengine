package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.node.LanguageNode;

public abstract class EvaluationResult {

  protected final LanguageNode node;

  public EvaluationResult(final LanguageNode node) {
    this.node = node;
  }

  public abstract BooleanResult toBooleanResult();

  public abstract TimePoint toTimePoint();

  public abstract TimeIntervals toTimeIntervals(final Patient patient);

  public LanguageNode getNode() {
    return this.node;
  }

}
