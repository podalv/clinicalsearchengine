package com.podalv.search.language.evaluation.iterators;

/** Payload iterator without any payload
 *  Just a wrapper around IntIterator
 *
 */
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;

public class EmptyPayloadIterator implements PayloadIterator {

  private final IntIterator iterator;
  private int               startValue = -1;
  private int               endValue   = -1;

  public EmptyPayloadIterator(final IntIterator iterator) {
    this.iterator = iterator;
  }

  public EmptyPayloadIterator() {
    this.iterator = null;
  }

  @Override
  public boolean hasNext() {
    return iterator != null && iterator.hasNext();
  }

  @Override
  public void next() {
    startValue = iterator.next();
    endValue = iterator.next();
  }

  @Override
  public int getPayloadId() {
    return -1;
  }

  @Override
  public IntArrayList getPayload() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getStartId() {
    return startValue;
  }

  @Override
  public int getEndId() {
    return endValue;
  }

  @Override
  public boolean containsInvalidEvent() {
    return false;
  }
}
