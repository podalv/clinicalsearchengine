package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;

/** Wraps around raw data stored in IntArrayList and produces RawPayloadIterator
 *
 * @author podalv
 *
 */
public class RawPayloadIteratorFromList extends RawPayloadIterator {

  private int       iteratorPosition = 0;
  private final int dataLength;

  public RawPayloadIteratorFromList(final IntArrayList rawData, final int dataLength) {
    super(null, rawData);
    this.dataLength = dataLength;
    iteratorPosition = 0 - dataLength;
  }

  @Override
  void getNext() {
    if (iteratorPosition + dataLength > result.size()) {
      throw new IndexOutOfBoundsException(result.size() + "");
    }
    iteratorPosition += dataLength;
    currentPayload = new IntArrayList(dataLength);
    for (int x = iteratorPosition; x < iteratorPosition + dataLength; x++) {
      currentPayload.add(result.get(x));
    }
  }

  @Override
  public boolean hasNext() {
    return (result != null) && (iteratorPosition + dataLength < result.size());
  }

  @Override
  public void next() {
    if (hasNext()) {
      getNext();
    }
  }

  @Override
  public IntArrayList getPayload() {
    return currentPayload;
  }
}
