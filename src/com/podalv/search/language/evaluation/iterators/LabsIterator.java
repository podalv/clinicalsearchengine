package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.language.evaluation.PayloadPointers;

/** Iterates over raw data from labs and gives access to labIds, etc.
*
* @author podalv
*
*/
public class LabsIterator implements PayloadIterator {

  final IntIterator iterator;
  final boolean     isInvalid;
  int               currentStartValue = -1;
  int               currentEndValue   = -1;

  public LabsIterator(final IntArrayList list) {
    this.iterator = list.iterator();
    isInvalid = PayloadPointers.containsInvalidEvents(list);
  }

  @Override
  public boolean hasNext() {
    return iterator != null && iterator.hasNext();
  }

  @Override
  public void next() {
    currentStartValue = iterator.next();
    currentEndValue = iterator.next();
  }

  @Override
  public int getPayloadId() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getStartId() {
    return currentStartValue;
  }

  @Override
  public IntArrayList getPayload() {
    return null;
  }

  @Override
  public int getEndId() {
    return currentEndValue;
  }

  @Override
  public boolean containsInvalidEvent() {
    return isInvalid;
  }
}