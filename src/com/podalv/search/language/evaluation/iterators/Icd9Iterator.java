package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientBuilder;

public class Icd9Iterator implements PayloadIterator {

  final RawPayloadIterator iterator;
  private final boolean    primary;
  private final boolean    original;

  public Icd9Iterator(final RawPayloadIterator iterator, final boolean primary, final boolean original) {
    this.iterator = iterator;
    this.primary = primary;
    this.original = original;
    rewindToNextPrimary();
  }

  public boolean isPrimary() {
    final IntArrayList payload = iterator.getPayload();
    return payload != null && PatientBuilder.isPrimaryIcdCode(payload.get(PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA));
  }

  public boolean isOriginal() {
    final IntArrayList payload = iterator.getPayload();
    return payload != null && PatientBuilder.isOriginalIcdCode(payload.get(PatientBuilder.PRIMARY_ID_POSITION_IN_PAYLOAD_DATA));
  }

  public boolean isEligible() {
    return (!primary && !original) || (primary && original && isPrimary() && isOriginal()) || ((!original && primary && isPrimary()) || (!primary && original && isOriginal()));
  }

  private void rewindToNextPrimary() {
    if (!isEligible()) {
      while (iterator.hasNext() && !isEligible()) {
        iterator.next();
      }
    }
  }

  @Override
  public boolean hasNext() {
    return iterator != null && iterator.hasNext();
  }

  @Override
  public void next() {
    iterator.next();
    rewindToNextPrimary();
  }

  @Override
  public int getPayloadId() {
    return iterator.getPayloadId();
  }

  @Override
  public int getStartId() {
    return iterator.getStartId();
  }

  @Override
  public IntArrayList getPayload() {
    return iterator.getPayload();
  }

  @Override
  public int getEndId() {
    return iterator.getEndId();
  }

  @Override
  public boolean containsInvalidEvent() {
    return iterator.containsInvalidEvent();
  }
}
