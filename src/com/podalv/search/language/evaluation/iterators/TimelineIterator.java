package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;

public class TimelineIterator implements PayloadIterator {

  private final int start;
  private final int end;
  private boolean   hasNext = true;

  public TimelineIterator(final int start, final int end) {
    this.start = start;
    this.end = end;
  }

  @Override
  public IntArrayList getPayload() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getStartId() {
    return start;
  }

  @Override
  public int getEndId() {
    return end;
  }

  @Override
  public boolean hasNext() {
    return hasNext;
  }

  @Override
  public void next() {
    hasNext = false;
  }

  @Override
  public int getPayloadId() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsInvalidEvent() {
    return false;
  }
}
