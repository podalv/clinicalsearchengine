package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;

public interface PayloadIterator {

  public abstract IntArrayList getPayload();

  public abstract int getStartId();

  public abstract int getEndId();

  public abstract boolean hasNext();

  public abstract void next();

  public int getPayloadId();

  public boolean containsInvalidEvent();
}
