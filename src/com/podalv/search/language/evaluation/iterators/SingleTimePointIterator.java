package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.language.evaluation.PayloadPointers;

/** From [payloadId, payLoadId, payloadIt], iterates over [startTime, startTime ...]
 *  These are RAW data, so no compression is done
 *
 * @author podalv
 *
 */
public class SingleTimePointIterator implements PayloadIterator {

  private int        iteratorPosition = 0;
  final IntArrayList result;
  int                currentTimePoint = -1;

  public SingleTimePointIterator(final IntArrayList timePoints) {
    this.result = timePoints;
  }

  void getNext() {
    currentTimePoint = result.get(iteratorPosition++);
  }

  @Override
  public boolean hasNext() {
    return (result != null && iteratorPosition < result.size());
  }

  @Override
  public void next() {
    if (hasNext()) {
      getNext();
    }
  }

  @Override
  public int getPayloadId() {
    return currentTimePoint;
  }

  @Override
  public IntArrayList getPayload() {
    throw new UnsupportedOperationException(getClass().toString() + " does not contain payload information");
  }

  @Override
  public int getStartId() {
    return currentTimePoint;
  }

  @Override
  public int getEndId() {
    return currentTimePoint;
  }

  @Override
  public boolean containsInvalidEvent() {
    return PayloadPointers.containsInvalidEvents(result);
  }

}