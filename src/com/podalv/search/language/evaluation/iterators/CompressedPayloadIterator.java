package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.ObjectWithPayload;
import com.podalv.search.language.evaluation.PayloadPointers;

/** From [payloadId, payLoadId, payloadIt], iterates over [startTime, endTime, startTime, endTime ...]
 *  If there are overlaps, etc. they will be compressed to make non-overlapping intervals
 *
 * @author podalv
 *
 */
public class CompressedPayloadIterator implements PayloadIterator {

  private int                     iteratorPosition = -1;
  private final ObjectWithPayload patient;
  private final IntArrayList      result;
  private final PayloadIterator   resultIterator;
  private int                     nextStartId      = -1;
  private int                     nextEndId        = -1;
  private int                     startId          = -1;
  private int                     endId            = -1;
  private IntArrayList            currentPayload   = null;

  public CompressedPayloadIterator(final ObjectWithPayload patient, final IntArrayList payloadIds) {
    this.result = payloadIds;
    this.patient = patient;
    resultIterator = null;
  }

  public CompressedPayloadIterator(final ObjectWithPayload patient, final PayloadIterator iterator) {
    this.result = null;
    this.patient = patient;
    resultIterator = iterator;
  }

  @Override
  public int getPayloadId() {
    if (resultIterator != null) {
      return resultIterator.getPayloadId();
    }
    throw new UnsupportedOperationException();
  }

  private void getNext() {
    if (result != null) {
      getNextList();
    }
    else {
      getNextIterator();
    }
  }

  private void getNextIterator() {
    if (startId == nextStartId) {
      nextStartId = -1;
      nextEndId = -1;
    }
    startId = nextStartId;
    endId = nextEndId;
    int startVal = nextStartId;
    int endVal = nextEndId;
    if (resultIterator.hasNext()) {
      while (resultIterator.hasNext()) {
        if (nextStartId == -1 && nextEndId == -1) {
          resultIterator.next();
          startVal = resultIterator.getStartId();
          endVal = resultIterator.getEndId();
        }
        if (startId == -1 && endId == -1) {
          startId = startVal;
          endId = endVal;
          nextStartId = -1;
          nextEndId = -1;
        }
        if (startVal <= endId) {
          if (endVal >= endId) {
            endId = endVal;
            nextStartId = -1;
            nextEndId = -1;
          }
        }
        else {
          nextStartId = startVal;
          nextEndId = endVal;
          break;
        }
      }
    }
    else {
      nextStartId = -1;
      nextEndId = -1;
    }
  }

  private void getNextList() {
    currentPayload = patient.getPayload(result.get(iteratorPosition + 1));
    startId = currentPayload.get(0);
    endId = currentPayload.get(1);
    for (int x = 0; x < result.size(); x++) {
      currentPayload = patient.getPayload(result.get(x));
      if (currentPayload.get(0) <= endId) {
        if (currentPayload.get(1) >= endId) {
          endId = currentPayload.get(1);
        }
        iteratorPosition = x;
      }
      else {
        break;
      }
    }
  }

  @Override
  public boolean hasNext() {
    return result != null ? (iteratorPosition < result.size() - 1) : (resultIterator.hasNext() || nextStartId != -1);
  }

  @Override
  public void next() {
    if (hasNext()) {
      getNext();
    }
  }

  @Override
  public IntArrayList getPayload() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getStartId() {
    return startId;
  }

  @Override
  public int getEndId() {
    return endId;
  }

  @Override
  public boolean containsInvalidEvent() {
    if (resultIterator != null) {
      return resultIterator.containsInvalidEvent();
    }
    return PayloadPointers.containsInvalidEvents(result);
  }
}