package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.ObjectWithPayload;
import com.podalv.search.language.evaluation.PayloadPointers;

/** From [payloadId, payLoadId, payloadIt], iterates over [startTime, endTime, startTime, endTime ...]
 *  These are RAW data, so no compression is done
 *
 * @author podalv
 *
 */
public class RawPayloadIterator implements PayloadIterator {

  private int             iteratorPosition = 0;
  final ObjectWithPayload patient;
  final IntArrayList      result;
  IntArrayList            currentPayload   = null;
  int                     currentPayloadId = -1;

  public RawPayloadIterator(final ObjectWithPayload patient, final IntArrayList payloadIds) {
    this.result = payloadIds;
    this.patient = patient;
  }

  void getNext() {
    currentPayloadId = result.get(iteratorPosition++);
    currentPayload = patient.getPayload(currentPayloadId);
  }

  @Override
  public boolean hasNext() {
    return (result != null && iteratorPosition < result.size() && !containsInvalidEvent());
  }

  @Override
  public void next() {
    if (hasNext()) {
      getNext();
    }
  }

  @Override
  public int getPayloadId() {
    return currentPayloadId;
  }

  @Override
  public IntArrayList getPayload() {
    return currentPayload;
  }

  @Override
  public int getStartId() {
    return currentPayload.get(0);
  }

  @Override
  public int getEndId() {
    return currentPayload.get(1);
  }

  @Override
  public boolean containsInvalidEvent() {
    return PayloadPointers.containsInvalidEvents(result);
  }

}