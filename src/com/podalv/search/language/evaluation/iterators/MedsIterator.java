package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientBuilder;

public class MedsIterator implements PayloadIterator {

  final RawPayloadIterator iterator;

  public MedsIterator(final RawPayloadIterator iterator) {
    this.iterator = iterator;
  }

  public int getDrugRouteId() {
    return iterator.getPayload().get(PatientBuilder.DRUG_ROUTE_ID_POSITION_IN_PAYLOAD_DATA);
  }

  public int getDrugStatusId() {
    return iterator.getPayload().get(PatientBuilder.DRUG_STATUS_ID_POSITION_IN_PAYLOAD_DATA);
  }

  @Override
  public boolean hasNext() {
    return iterator != null && iterator.hasNext();
  }

  @Override
  public void next() {
    iterator.next();
  }

  @Override
  public int getPayloadId() {
    return iterator.getPayloadId();
  }

  @Override
  public int getStartId() {
    return iterator.getStartId();
  }

  @Override
  public IntArrayList getPayload() {
    return iterator.getPayload();
  }

  @Override
  public int getEndId() {
    return iterator.getEndId();
  }

  @Override
  public boolean containsInvalidEvent() {
    return iterator.containsInvalidEvent();
  }
}