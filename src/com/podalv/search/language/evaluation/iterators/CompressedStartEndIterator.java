package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.language.evaluation.PayloadPointers;

/** Iterates over [startTime, endTime, startTime, endTime ...]
 *  If there are overlaps, etc. they will be compressed to make non-overlapping intervals
 *
 * @author podalv
 *
 */
public class CompressedStartEndIterator implements PayloadIterator {

  private int                iteratorPosition = -1;
  private final IntArrayList result;
  private int                startId          = -1;
  private int                endId            = -1;

  public CompressedStartEndIterator(final IntArrayList payloadIds) {
    this.result = payloadIds;
  }

  @Override
  public int getPayloadId() {
    throw new UnsupportedOperationException();
  }

  private void getNext() {
    getNextList();
  }

  private void getNextList() {
    startId = result.get(iteratorPosition + 1);
    endId = result.get(iteratorPosition + 2);
    for (int x = 0; x < result.size(); x += 2) {
      if (result.get(x) <= endId) {
        if (result.get(x + 1) >= endId) {
          endId = result.get(x + 1);
        }
        iteratorPosition = x + 1;
      }
      else {
        break;
      }
    }
  }

  @Override
  public boolean hasNext() {
    return iteratorPosition + 1 < result.size() - 1;
  }

  @Override
  public void next() {
    if (hasNext()) {
      getNext();
    }
  }

  @Override
  public IntArrayList getPayload() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getStartId() {
    return startId;
  }

  @Override
  public int getEndId() {
    return endId;
  }

  @Override
  public boolean containsInvalidEvent() {
    return PayloadPointers.containsInvalidEvents(result);
  }
}