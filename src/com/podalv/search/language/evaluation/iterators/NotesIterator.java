package com.podalv.search.language.evaluation.iterators;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientBuilder;

/** Iterates over raw data from notes and gives access to noteIds, etc.
 *
 * @author podalv
 *
 */
public class NotesIterator implements PayloadIterator {

  final RawPayloadIterator iterator;

  public NotesIterator(final RawPayloadIterator iterator) {
    this.iterator = iterator;
  }

  public int getNoteId() {
    return iterator.getPayload().get(PatientBuilder.NOTE_ID_POSITION_IN_PAYLOAD_DATA);
  }

  public int getNoteTypeId() {
    return iterator.getPayload().get(PatientBuilder.NOTE_TYPE_ID_POSITION_IN_PAYLOAD_DATA);
  }

  @Override
  public boolean hasNext() {
    return iterator != null && iterator.hasNext();
  }

  @Override
  public void next() {
    iterator.next();
  }

  @Override
  public int getPayloadId() {
    return iterator.getPayloadId();
  }

  @Override
  public int getStartId() {
    return iterator.getStartId();
  }

  @Override
  public IntArrayList getPayload() {
    return iterator.getPayload();
  }

  @Override
  public int getEndId() {
    return iterator.getStartId();
  }

  @Override
  public boolean containsInvalidEvent() {
    return iterator.containsInvalidEvent();
  }
}