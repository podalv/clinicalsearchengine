package com.podalv.search.language.evaluation;

import com.podalv.search.datastructures.Patient;
import com.podalv.search.language.node.LanguageNode;

/** Causes the evaluation to abort. Used in cases when patient has invalid data
 *
 * @author podalv
 *
 */
public class AbortedResult extends EvaluationResult {

  public static AbortedResult INSTANCE = new AbortedResult(null);

  public static AbortedResult getInstance() {
    return INSTANCE;
  }

  public AbortedResult(final LanguageNode node) {
    super(node);
  }

  @Override
  public BooleanResult toBooleanResult() {
    throw new UnsupportedOperationException("Conversion from AbortedResult undefined");
  }

  @Override
  public TimePoint toTimePoint() {
    throw new UnsupportedOperationException("Conversion from AbortedResult undefined");
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    throw new UnsupportedOperationException("Conversion from AbortedResult undefined");
  }

}
