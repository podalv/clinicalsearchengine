package com.podalv.search.language.evaluation;

import java.util.ArrayList;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.ObjectWithPayload;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.language.evaluation.iterators.CompressedPayloadIterator;
import com.podalv.search.language.evaluation.iterators.Icd9Iterator;
import com.podalv.search.language.evaluation.iterators.LabsIterator;
import com.podalv.search.language.evaluation.iterators.MedsIterator;
import com.podalv.search.language.evaluation.iterators.NotesIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIteratorSingleTimePoint;
import com.podalv.search.language.evaluation.iterators.SingleTimePointIterator;
import com.podalv.search.language.node.LanguageNode;

/** Produces just pointer ids to the payload data. These need to interpreted to start / end intervals
 *
 * @author podalv
 *
 */
public class PayloadPointers extends TimeIntervals {

  public static enum TYPE {
    START_END_TIME, CPT_RAW, ICD9_RAW, ICD9_RAW_ORIGINAL, ICD9_RAW_ORIGINAL_PRIMARY, ICD9_RAW_PRIMARY, NOTE_RAW, SINGLE_TIME_POINT, LABS_PAYLOADS, MEDS, OTHER
  };
  final TYPE type;

  public static boolean containsInvalidEvents(final IntArrayList payloadIds) {
    return payloadIds != null && ((payloadIds.size() == 1 && payloadIds.get(0) == PatientBuilder.INVALID_PAYLOAD) || (payloadIds.size() == 2 && payloadIds.get(
        0) == PatientBuilder.INVALID_PAYLOAD && payloadIds.get(1) == PatientBuilder.INVALID_PAYLOAD));
  }

  public static boolean containsInvalidEvents(final ArrayList<String> measurementTimes) {
    return measurementTimes != null && measurementTimes.size() == 1 && measurementTimes.get(0).equals("" + PatientBuilder.INVALID_PAYLOAD);
  }

  public PayloadPointers(final TYPE type, final LanguageNode node, final ObjectWithPayload patient, final IntArrayList payloadIds) {
    super(node, payloadIds);
    this.type = type;
  }

  public TYPE getType() {
    return type;
  }

  @Override
  public BooleanResult toBooleanResult() {
    return (result == null || result.size() == 0) ? BooleanResult.getFalse() : BooleanResult.getTrue();
  }

  @Override
  public TimePoint toTimePoint() {
    throw new UnsupportedOperationException("Cast from Time Interval to Time Point is undefined");
  }

  @Override
  public TimeIntervals toTimeIntervals(final Patient patient) {
    return this;
  }

  @Override
  public PayloadIterator iterator(final ObjectWithPayload patient) {
    if (type.equals(TYPE.START_END_TIME)) {
      return new LabsIterator(result);
    }
    if (type.equals(TYPE.MEDS)) {
      return new MedsIterator(new RawPayloadIterator(patient, result));
    }
    if (type.equals(TYPE.SINGLE_TIME_POINT)) {
      return new SingleTimePointIterator(result);
    }
    if (type.equals(TYPE.ICD9_RAW)) {
      final RawPayloadIterator i = new RawPayloadIterator(patient, result);
      return new Icd9Iterator(i, false, false);
    }
    if (type.equals(TYPE.ICD9_RAW_PRIMARY)) {
      final RawPayloadIterator i = new RawPayloadIterator(patient, result);
      return new Icd9Iterator(i, true, false);
    }
    if (type.equals(TYPE.ICD9_RAW_ORIGINAL)) {
      final RawPayloadIterator i = new RawPayloadIterator(patient, result);
      return new Icd9Iterator(i, false, true);
    }
    if (type.equals(TYPE.ICD9_RAW_ORIGINAL_PRIMARY)) {
      final RawPayloadIterator i = new RawPayloadIterator(patient, result);
      return new Icd9Iterator(i, true, true);
    }
    if (type.equals(TYPE.NOTE_RAW) || type.equals(TYPE.CPT_RAW) || type.equals(TYPE.LABS_PAYLOADS)) {
      if (type.equals(TYPE.LABS_PAYLOADS)) {
        return new RawPayloadIteratorSingleTimePoint(patient, result);
      }
      else {
        final RawPayloadIterator i = new RawPayloadIterator(patient, result);
        if (type.equals(TYPE.NOTE_RAW)) {
          return new NotesIterator(i);
        }
        return i;
      }
    }
    return new CompressedPayloadIterator(patient, result);
  }
}