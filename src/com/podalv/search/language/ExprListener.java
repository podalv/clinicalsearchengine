// Generated from Expr.g4 by ANTLR 4.7.1
package com.podalv.search.language;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExprParser}.
 */
public interface ExprListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ExprParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(ExprParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(ExprParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by the {@code outputCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void enterOutputCommand(ExprParser.OutputCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code outputCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void exitOutputCommand(ExprParser.OutputCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eventFlowCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void enterEventFlowCommand(ExprParser.EventFlowCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eventFlowCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void exitEventFlowCommand(ExprParser.EventFlowCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code csvCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void enterCsvCommand(ExprParser.CsvCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code csvCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void exitCsvCommand(ExprParser.CsvCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dumpCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void enterDumpCommand(ExprParser.DumpCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dumpCommand}
	 * labeled alternative in {@link ExprParser#output_commands}.
	 * @param ctx the parse tree
	 */
	void exitDumpCommand(ExprParser.DumpCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachCommand}
	 * labeled alternative in {@link ExprParser#each}.
	 * @param ctx the parse tree
	 */
	void enterEachCommand(ExprParser.EachCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachCommand}
	 * labeled alternative in {@link ExprParser#each}.
	 * @param ctx the parse tree
	 */
	void exitEachCommand(ExprParser.EachCommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#each_content}.
	 * @param ctx the parse tree
	 */
	void enterEach_content(ExprParser.Each_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#each_content}.
	 * @param ctx the parse tree
	 */
	void exitEach_content(ExprParser.Each_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#main_command}.
	 * @param ctx the parse tree
	 */
	void enterMain_command(ExprParser.Main_commandContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#main_command}.
	 * @param ctx the parse tree
	 */
	void exitMain_command(ExprParser.Main_commandContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#each_name}.
	 * @param ctx the parse tree
	 */
	void enterEach_name(ExprParser.Each_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#each_name}.
	 * @param ctx the parse tree
	 */
	void exitEach_name(ExprParser.Each_nameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachNodeAssign}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachNodeAssign(ExprParser.EachNodeAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachNodeAssign}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachNodeAssign(ExprParser.EachNodeAssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachNodeSave}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachNodeSave(ExprParser.EachNodeSaveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachNodeSave}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachNodeSave(ExprParser.EachNodeSaveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code clearVariable}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterClearVariable(ExprParser.ClearVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code clearVariable}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitClearVariable(ExprParser.ClearVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachLineIf}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachLineIf(ExprParser.EachLineIfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachLineIf}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachLineIf(ExprParser.EachLineIfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachLineEquals}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachLineEquals(ExprParser.EachLineEqualsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachLineEquals}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachLineEquals(ExprParser.EachLineEqualsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachNodeExit}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachNodeExit(ExprParser.EachNodeExitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachNodeExit}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachNodeExit(ExprParser.EachNodeExitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code print}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterPrint(ExprParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code print}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitPrint(ExprParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printSingle}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterPrintSingle(ExprParser.PrintSingleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printSingle}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitPrintSingle(ExprParser.PrintSingleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachNodeContinue}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachNodeContinue(ExprParser.EachNodeContinueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachNodeContinue}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachNodeContinue(ExprParser.EachNodeContinueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachNodeFailPatient}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterEachNodeFailPatient(ExprParser.EachNodeFailPatientContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachNodeFailPatient}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitEachNodeFailPatient(ExprParser.EachNodeFailPatientContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unusedEach}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void enterUnusedEach(ExprParser.UnusedEachContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unusedEach}
	 * labeled alternative in {@link ExprParser#each_line}.
	 * @param ctx the parse tree
	 */
	void exitUnusedEach(ExprParser.UnusedEachContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parseTerm}
	 * labeled alternative in {@link ExprParser#print_atom}.
	 * @param ctx the parse tree
	 */
	void enterParseTerm(ExprParser.ParseTermContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parseTerm}
	 * labeled alternative in {@link ExprParser#print_atom}.
	 * @param ctx the parse tree
	 */
	void exitParseTerm(ExprParser.ParseTermContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unused1}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void enterUnused1(ExprParser.Unused1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unused1}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void exitUnused1(ExprParser.Unused1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code unused2}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void enterUnused2(ExprParser.Unused2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unused2}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void exitUnused2(ExprParser.Unused2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code limitCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void enterLimitCommand(ExprParser.LimitCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code limitCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void exitLimitCommand(ExprParser.LimitCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code estimateCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void enterEstimateCommand(ExprParser.EstimateCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code estimateCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void exitEstimateCommand(ExprParser.EstimateCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code evaluateCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void enterEvaluateCommand(ExprParser.EvaluateCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code evaluateCommand}
	 * labeled alternative in {@link ExprParser#top_level}.
	 * @param ctx the parse tree
	 */
	void exitEvaluateCommand(ExprParser.EvaluateCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unusedMultipleCommands}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void enterUnusedMultipleCommands(ExprParser.UnusedMultipleCommandsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unusedMultipleCommands}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void exitUnusedMultipleCommands(ExprParser.UnusedMultipleCommandsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unusedMultipleCommand}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void enterUnusedMultipleCommand(ExprParser.UnusedMultipleCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unusedMultipleCommand}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void exitUnusedMultipleCommand(ExprParser.UnusedMultipleCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unusedAtoms}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void enterUnusedAtoms(ExprParser.UnusedAtomsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unusedAtoms}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void exitUnusedAtoms(ExprParser.UnusedAtomsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unusedCommand}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void enterUnusedCommand(ExprParser.UnusedCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unusedCommand}
	 * labeled alternative in {@link ExprParser#commands}.
	 * @param ctx the parse tree
	 */
	void exitUnusedCommand(ExprParser.UnusedCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterAndCommand(ExprParser.AndCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitAndCommand(ExprParser.AndCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterOrCommand(ExprParser.OrCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitOrCommand(ExprParser.OrCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNotCommand(ExprParser.NotCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNotCommand(ExprParser.NotCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unusedBefore}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterUnusedBefore(ExprParser.UnusedBeforeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unusedBefore}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitUnusedBefore(ExprParser.UnusedBeforeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code complexBefore}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterComplexBefore(ExprParser.ComplexBeforeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code complexBefore}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitComplexBefore(ExprParser.ComplexBeforeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code findCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterFindCommand(ExprParser.FindCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code findCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitFindCommand(ExprParser.FindCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intersectingAll}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIntersectingAll(ExprParser.IntersectingAllContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intersectingAll}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIntersectingAll(ExprParser.IntersectingAllContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intersectingAny}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIntersectingAny(ExprParser.IntersectingAnyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intersectingAny}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIntersectingAny(ExprParser.IntersectingAnyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nonIntersectManyCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNonIntersectManyCommand(ExprParser.NonIntersectManyCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nonIntersectManyCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNonIntersectManyCommand(ExprParser.NonIntersectManyCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nonIntersectAllCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNonIntersectAllCommand(ExprParser.NonIntersectAllCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nonIntersectAllCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNonIntersectAllCommand(ExprParser.NonIntersectAllCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nonIntersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNonIntersectCommand(ExprParser.NonIntersectCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nonIntersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNonIntersectCommand(ExprParser.NonIntersectCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code neverIntersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNeverIntersectCommand(ExprParser.NeverIntersectCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code neverIntersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNeverIntersectCommand(ExprParser.NeverIntersectCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code neverIntersectManyCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNeverIntersectManyCommand(ExprParser.NeverIntersectManyCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code neverIntersectManyCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNeverIntersectManyCommand(ExprParser.NeverIntersectManyCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code neverIntersectAllCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNeverIntersectAllCommand(ExprParser.NeverIntersectAllCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code neverIntersectAllCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNeverIntersectAllCommand(ExprParser.NeverIntersectAllCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alwaysIntersect}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterAlwaysIntersect(ExprParser.AlwaysIntersectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alwaysIntersect}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitAlwaysIntersect(ExprParser.AlwaysIntersectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alwaysIntersectAny}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterAlwaysIntersectAny(ExprParser.AlwaysIntersectAnyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alwaysIntersectAny}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitAlwaysIntersectAny(ExprParser.AlwaysIntersectAnyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alwaysIntersectAll}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterAlwaysIntersectAll(ExprParser.AlwaysIntersectAllContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alwaysIntersectAll}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitAlwaysIntersectAll(ExprParser.AlwaysIntersectAllContext ctx);
	/**
	 * Enter a parse tree produced by the {@code invertCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterInvertCommand(ExprParser.InvertCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code invertCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitInvertCommand(ExprParser.InvertCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code startCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterStartCommand(ExprParser.StartCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code startCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitStartCommand(ExprParser.StartCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code endCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterEndCommand(ExprParser.EndCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code endCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitEndCommand(ExprParser.EndCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intervalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIntervalCommand(ExprParser.IntervalCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intervalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIntervalCommand(ExprParser.IntervalCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intervalPairwiseCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIntervalPairwiseCommand(ExprParser.IntervalPairwiseCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intervalPairwiseCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIntervalPairwiseCommand(ExprParser.IntervalPairwiseCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIntersectCommand(ExprParser.IntersectCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intersectCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIntersectCommand(ExprParser.IntersectCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterUnionCommand(ExprParser.UnionCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitUnionCommand(ExprParser.UnionCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code complexBeforeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterComplexBeforeCommand(ExprParser.ComplexBeforeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code complexBeforeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitComplexBeforeCommand(ExprParser.ComplexBeforeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code firstMentionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterFirstMentionCommand(ExprParser.FirstMentionCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code firstMentionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitFirstMentionCommand(ExprParser.FirstMentionCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lastMentionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterLastMentionCommand(ExprParser.LastMentionCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lastMentionCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitLastMentionCommand(ExprParser.LastMentionCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code firstMentionContextCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterFirstMentionContextCommand(ExprParser.FirstMentionContextCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code firstMentionContextCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitFirstMentionContextCommand(ExprParser.FirstMentionContextCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lastMentionContextCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterLastMentionContextCommand(ExprParser.LastMentionContextCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lastMentionContextCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitLastMentionContextCommand(ExprParser.LastMentionContextCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code extendByCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterExtendByCommand(ExprParser.ExtendByCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code extendByCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitExtendByCommand(ExprParser.ExtendByCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code durationSimpleCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDurationSimpleCommand(ExprParser.DurationSimpleCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code durationSimpleCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDurationSimpleCommand(ExprParser.DurationSimpleCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code durationComplexCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDurationComplexCommand(ExprParser.DurationComplexCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code durationComplexCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDurationComplexCommand(ExprParser.DurationComplexCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code durationComplex2Command}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDurationComplex2Command(ExprParser.DurationComplex2CommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code durationComplex2Command}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDurationComplex2Command(ExprParser.DurationComplex2CommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code historyOfCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterHistoryOfCommand(ExprParser.HistoryOfCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code historyOfCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitHistoryOfCommand(ExprParser.HistoryOfCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noHistoryOfCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNoHistoryOfCommand(ExprParser.NoHistoryOfCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noHistoryOfCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNoHistoryOfCommand(ExprParser.NoHistoryOfCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code neverHadCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNeverHadCommand(ExprParser.NeverHadCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code neverHadCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNeverHadCommand(ExprParser.NeverHadCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code countSimpleCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCountSimpleCommand(ExprParser.CountSimpleCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code countSimpleCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCountSimpleCommand(ExprParser.CountSimpleCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code countComplexCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCountComplexCommand(ExprParser.CountComplexCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code countComplexCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCountComplexCommand(ExprParser.CountComplexCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code countComplex2Command}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCountComplex2Command(ExprParser.CountComplex2CommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code countComplex2Command}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCountComplex2Command(ExprParser.CountComplex2CommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterEqualCommand(ExprParser.EqualCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitEqualCommand(ExprParser.EqualCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code containsCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterContainsCommand(ExprParser.ContainsCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code containsCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitContainsCommand(ExprParser.ContainsCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identicalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIdenticalCommand(ExprParser.IdenticalCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identicalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIdenticalCommand(ExprParser.IdenticalCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sameCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterSameCommand(ExprParser.SameCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sameCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitSameCommand(ExprParser.SameCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code diffCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDiffCommand(ExprParser.DiffCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code diffCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDiffCommand(ExprParser.DiffCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mergeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterMergeCommand(ExprParser.MergeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mergeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitMergeCommand(ExprParser.MergeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primaryCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryCommand(ExprParser.PrimaryCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryCommand(ExprParser.PrimaryCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code originalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterOriginalCommand(ExprParser.OriginalCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code originalCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitOriginalCommand(ExprParser.OriginalCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code originalPrimaryCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterOriginalPrimaryCommand(ExprParser.OriginalPrimaryCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code originalPrimaryCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitOriginalPrimaryCommand(ExprParser.OriginalPrimaryCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code departmentCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDepartmentCommand(ExprParser.DepartmentCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code departmentCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDepartmentCommand(ExprParser.DepartmentCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noteTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterNoteTypeCommand(ExprParser.NoteTypeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noteTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitNoteTypeCommand(ExprParser.NoteTypeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code visitTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterVisitTypeCommand(ExprParser.VisitTypeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code visitTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitVisitTypeCommand(ExprParser.VisitTypeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code anyVisitTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterAnyVisitTypeCommand(ExprParser.AnyVisitTypeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code anyVisitTypeCommand}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitAnyVisitTypeCommand(ExprParser.AnyVisitTypeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code drugsSimple}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDrugsSimple(ExprParser.DrugsSimpleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code drugsSimple}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDrugsSimple(ExprParser.DrugsSimpleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code drugsRoute}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDrugsRoute(ExprParser.DrugsRouteContext ctx);
	/**
	 * Exit a parse tree produced by the {@code drugsRoute}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDrugsRoute(ExprParser.DrugsRouteContext ctx);
	/**
	 * Enter a parse tree produced by the {@code drugsStatus}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDrugsStatus(ExprParser.DrugsStatusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code drugsStatus}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDrugsStatus(ExprParser.DrugsStatusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code drugsRouteStatus}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDrugsRouteStatus(ExprParser.DrugsRouteStatusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code drugsRouteStatus}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDrugsRouteStatus(ExprParser.DrugsRouteStatusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code drugsStatusRoute}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDrugsStatusRoute(ExprParser.DrugsStatusRouteContext ctx);
	/**
	 * Exit a parse tree produced by the {@code drugsStatusRoute}
	 * labeled alternative in {@link ExprParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDrugsStatusRoute(ExprParser.DrugsStatusRouteContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasCptCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterHasCptCommand(ExprParser.HasCptCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasCptCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitHasCptCommand(ExprParser.HasCptCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasAnyCptCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterHasAnyCptCommand(ExprParser.HasAnyCptCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasAnyCptCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitHasAnyCptCommand(ExprParser.HasAnyCptCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasRxNormCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterHasRxNormCommand(ExprParser.HasRxNormCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasRxNormCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitHasRxNormCommand(ExprParser.HasRxNormCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasAnyRxNormCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterHasAnyRxNormCommand(ExprParser.HasAnyRxNormCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasAnyRxNormCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitHasAnyRxNormCommand(ExprParser.HasAnyRxNormCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code genderCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterGenderCommand(ExprParser.GenderCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code genderCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitGenderCommand(ExprParser.GenderCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code yearCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterYearCommand(ExprParser.YearCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code yearCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitYearCommand(ExprParser.YearCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code raceCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterRaceCommand(ExprParser.RaceCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code raceCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitRaceCommand(ExprParser.RaceCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ethnicityCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterEthnicityCommand(ExprParser.EthnicityCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ethnicityCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitEthnicityCommand(ExprParser.EthnicityCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code deathCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterDeathCommand(ExprParser.DeathCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code deathCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitDeathCommand(ExprParser.DeathCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code anySnomedCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAnySnomedCommand(ExprParser.AnySnomedCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code anySnomedCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAnySnomedCommand(ExprParser.AnySnomedCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code snomedCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterSnomedCommand(ExprParser.SnomedCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code snomedCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitSnomedCommand(ExprParser.SnomedCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code encountersCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterEncountersCommand(ExprParser.EncountersCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code encountersCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitEncountersCommand(ExprParser.EncountersCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noteTimeInstancesCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNoteTimeInstancesCommand(ExprParser.NoteTimeInstancesCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noteTimeInstancesCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNoteTimeInstancesCommand(ExprParser.NoteTimeInstancesCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code timelineCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterTimelineCommand(ExprParser.TimelineCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code timelineCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitTimelineCommand(ExprParser.TimelineCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code recordStartCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterRecordStartCommand(ExprParser.RecordStartCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code recordStartCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitRecordStartCommand(ExprParser.RecordStartCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code recordEndCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterRecordEndCommand(ExprParser.RecordEndCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code recordEndCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitRecordEndCommand(ExprParser.RecordEndCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noteCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNoteCommand(ExprParser.NoteCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noteCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNoteCommand(ExprParser.NoteCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noteBooleanCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNoteBooleanCommand(ExprParser.NoteBooleanCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noteBooleanCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNoteBooleanCommand(ExprParser.NoteBooleanCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noteWithTypeCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNoteWithTypeCommand(ExprParser.NoteWithTypeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noteWithTypeCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNoteWithTypeCommand(ExprParser.NoteWithTypeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noteAtomCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNoteAtomCommand(ExprParser.NoteAtomCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noteAtomCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNoteAtomCommand(ExprParser.NoteAtomCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ageCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAgeCommand(ExprParser.AgeCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ageCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAgeCommand(ExprParser.AgeCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code vitalsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterVitalsCommand(ExprParser.VitalsCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code vitalsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitVitalsCommand(ExprParser.VitalsCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code labValuesCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterLabValuesCommand(ExprParser.LabValuesCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code labValuesCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitLabValuesCommand(ExprParser.LabValuesCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleVitals}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterSimpleVitals(ExprParser.SimpleVitalsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleVitals}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitSimpleVitals(ExprParser.SimpleVitalsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code anyLabsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAnyLabsCommand(ExprParser.AnyLabsCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code anyLabsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAnyLabsCommand(ExprParser.AnyLabsCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code anyVitalsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAnyVitalsCommand(ExprParser.AnyVitalsCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code anyVitalsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAnyVitalsCommand(ExprParser.AnyVitalsCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code complexLabs}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterComplexLabs(ExprParser.ComplexLabsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code complexLabs}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitComplexLabs(ExprParser.ComplexLabsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleLabs}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterSimpleLabs(ExprParser.SimpleLabsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleLabs}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitSimpleLabs(ExprParser.SimpleLabsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numericInterval}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNumericInterval(ExprParser.NumericIntervalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numericInterval}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNumericInterval(ExprParser.NumericIntervalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNullCommand(ExprParser.NullCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNullCommand(ExprParser.NullCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atcCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtcCommand(ExprParser.AtcCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code atcCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtcCommand(ExprParser.AtcCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code icd9AtomCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterIcd9AtomCommand(ExprParser.Icd9AtomCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code icd9AtomCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitIcd9AtomCommand(ExprParser.Icd9AtomCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code patientsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterPatientsCommand(ExprParser.PatientsCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code patientsCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitPatientsCommand(ExprParser.PatientsCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code patientsSingleCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterPatientsSingleCommand(ExprParser.PatientsSingleCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code patientsSingleCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitPatientsSingleCommand(ExprParser.PatientsSingleCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eachNameCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterEachNameCommand(ExprParser.EachNameCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eachNameCommand}
	 * labeled alternative in {@link ExprParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitEachNameCommand(ExprParser.EachNameCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasIcd9Command}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void enterHasIcd9Command(ExprParser.HasIcd9CommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasIcd9Command}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void exitHasIcd9Command(ExprParser.HasIcd9CommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasAnyIcd9Codes}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void enterHasAnyIcd9Codes(ExprParser.HasAnyIcd9CodesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasAnyIcd9Codes}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void exitHasAnyIcd9Codes(ExprParser.HasAnyIcd9CodesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasIcd10Command}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void enterHasIcd10Command(ExprParser.HasIcd10CommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasIcd10Command}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void exitHasIcd10Command(ExprParser.HasIcd10CommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hasAnyIcd10Codes}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void enterHasAnyIcd10Codes(ExprParser.HasAnyIcd10CodesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hasAnyIcd10Codes}
	 * labeled alternative in {@link ExprParser#icd9_atom}.
	 * @param ctx the parse tree
	 */
	void exitHasAnyIcd10Codes(ExprParser.HasAnyIcd10CodesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#double_value}.
	 * @param ctx the parse tree
	 */
	void enterDouble_value(ExprParser.Double_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#double_value}.
	 * @param ctx the parse tree
	 */
	void exitDouble_value(ExprParser.Double_valueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atomWithAsterisk1}
	 * labeled alternative in {@link ExprParser#before_atom_left}.
	 * @param ctx the parse tree
	 */
	void enterAtomWithAsterisk1(ExprParser.AtomWithAsterisk1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code atomWithAsterisk1}
	 * labeled alternative in {@link ExprParser#before_atom_left}.
	 * @param ctx the parse tree
	 */
	void exitAtomWithAsterisk1(ExprParser.AtomWithAsterisk1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code atomWithAsterisk2}
	 * labeled alternative in {@link ExprParser#before_atom_right}.
	 * @param ctx the parse tree
	 */
	void enterAtomWithAsterisk2(ExprParser.AtomWithAsterisk2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code atomWithAsterisk2}
	 * labeled alternative in {@link ExprParser#before_atom_right}.
	 * @param ctx the parse tree
	 */
	void exitAtomWithAsterisk2(ExprParser.AtomWithAsterisk2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code atomWithAsterisk3}
	 * labeled alternative in {@link ExprParser#before_atom_left_left}.
	 * @param ctx the parse tree
	 */
	void enterAtomWithAsterisk3(ExprParser.AtomWithAsterisk3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code atomWithAsterisk3}
	 * labeled alternative in {@link ExprParser#before_atom_left_left}.
	 * @param ctx the parse tree
	 */
	void exitAtomWithAsterisk3(ExprParser.AtomWithAsterisk3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code atomWithAsterisk4}
	 * labeled alternative in {@link ExprParser#before_atom_left_no_asterisk}.
	 * @param ctx the parse tree
	 */
	void enterAtomWithAsterisk4(ExprParser.AtomWithAsterisk4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code atomWithAsterisk4}
	 * labeled alternative in {@link ExprParser#before_atom_left_no_asterisk}.
	 * @param ctx the parse tree
	 */
	void exitAtomWithAsterisk4(ExprParser.AtomWithAsterisk4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code beforePositive}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void enterBeforePositive(ExprParser.BeforePositiveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforePositive}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void exitBeforePositive(ExprParser.BeforePositiveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeNegative}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void enterBeforeNegative(ExprParser.BeforeNegativeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeNegative}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void exitBeforeNegative(ExprParser.BeforeNegativeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforePositiveTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void enterBeforePositiveTime(ExprParser.BeforePositiveTimeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforePositiveTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void exitBeforePositiveTime(ExprParser.BeforePositiveTimeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeNegativeTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void enterBeforeNegativeTime(ExprParser.BeforeNegativeTimeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeNegativeTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void exitBeforeNegativeTime(ExprParser.BeforeNegativeTimeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeBoth}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void enterBeforeBoth(ExprParser.BeforeBothContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeBoth}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void exitBeforeBoth(ExprParser.BeforeBothContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeBothTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void enterBeforeBothTime(ExprParser.BeforeBothTimeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeBothTime}
	 * labeled alternative in {@link ExprParser#before}.
	 * @param ctx the parse tree
	 */
	void exitBeforeBothTime(ExprParser.BeforeBothTimeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeCommandAtom}
	 * labeled alternative in {@link ExprParser#before_command}.
	 * @param ctx the parse tree
	 */
	void enterBeforeCommandAtom(ExprParser.BeforeCommandAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeCommandAtom}
	 * labeled alternative in {@link ExprParser#before_command}.
	 * @param ctx the parse tree
	 */
	void exitBeforeCommandAtom(ExprParser.BeforeCommandAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeCommandAtom2}
	 * labeled alternative in {@link ExprParser#before_command}.
	 * @param ctx the parse tree
	 */
	void enterBeforeCommandAtom2(ExprParser.BeforeCommandAtom2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeCommandAtom2}
	 * labeled alternative in {@link ExprParser#before_command}.
	 * @param ctx the parse tree
	 */
	void exitBeforeCommandAtom2(ExprParser.BeforeCommandAtom2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code beforeModifier}
	 * labeled alternative in {@link ExprParser#before_modifier}.
	 * @param ctx the parse tree
	 */
	void enterBeforeModifier(ExprParser.BeforeModifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code beforeModifier}
	 * labeled alternative in {@link ExprParser#before_modifier}.
	 * @param ctx the parse tree
	 */
	void exitBeforeModifier(ExprParser.BeforeModifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#start_time}.
	 * @param ctx the parse tree
	 */
	void enterStart_time(ExprParser.Start_timeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#start_time}.
	 * @param ctx the parse tree
	 */
	void exitStart_time(ExprParser.Start_timeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#end_time}.
	 * @param ctx the parse tree
	 */
	void enterEnd_time(ExprParser.End_timeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#end_time}.
	 * @param ctx the parse tree
	 */
	void exitEnd_time(ExprParser.End_timeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#limit_type}.
	 * @param ctx the parse tree
	 */
	void enterLimit_type(ExprParser.Limit_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#limit_type}.
	 * @param ctx the parse tree
	 */
	void exitLimit_type(ExprParser.Limit_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#time_modifier}.
	 * @param ctx the parse tree
	 */
	void enterTime_modifier(ExprParser.Time_modifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#time_modifier}.
	 * @param ctx the parse tree
	 */
	void exitTime_modifier(ExprParser.Time_modifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code textCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 */
	void enterTextCommand(ExprParser.TextCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code textCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 */
	void exitTextCommand(ExprParser.TextCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code negatedTextCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 */
	void enterNegatedTextCommand(ExprParser.NegatedTextCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code negatedTextCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 */
	void exitNegatedTextCommand(ExprParser.NegatedTextCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code familyHistoryTextCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 */
	void enterFamilyHistoryTextCommand(ExprParser.FamilyHistoryTextCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code familyHistoryTextCommand}
	 * labeled alternative in {@link ExprParser#note_atom}.
	 * @param ctx the parse tree
	 */
	void exitFamilyHistoryTextCommand(ExprParser.FamilyHistoryTextCommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#note_atoms}.
	 * @param ctx the parse tree
	 */
	void enterNote_atoms(ExprParser.Note_atomsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#note_atoms}.
	 * @param ctx the parse tree
	 */
	void exitNote_atoms(ExprParser.Note_atomsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#atoms}.
	 * @param ctx the parse tree
	 */
	void enterAtoms(ExprParser.AtomsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#atoms}.
	 * @param ctx the parse tree
	 */
	void exitAtoms(ExprParser.AtomsContext ctx);
}