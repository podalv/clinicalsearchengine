package com.podalv.search.language.each;

import com.podalv.preprocessing.datastructures.IgnoreProcessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.LanguageNodeWithChildren;

public class EachNodeReturnNode extends LanguageNodeWithChildren {

  private final String    returnName;
  private VariableContext globalContext      = null;
  private boolean         contextInitialized = false;

  public EachNodeReturnNode(final LanguageNode returnNode, final String returnName) {
    this.returnName = returnName;
    addChild(returnNode);
  }

  private void initContext() {
    if (!contextInitialized) {
      contextInitialized = true;
      globalContext = EachNodeAtom.findGlobalContext(this, false);
    }
  }

  public String getReturnName() {
    return returnName;
  }

  @Override
  public LanguageNode copy() {
    throw new UnsupportedOperationException();
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initContext();
    final EvaluationResult result = getChildren().get(0).evaluate(context, patient);
    if (result instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    globalContext.add(returnName, result.toTimeIntervals(patient), patient);
    return result;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return IgnoreProcessingNode.getInstance();
  }

  @Override
  public String toString() {
    return "RETURN " + getChildren().get(0).toString() + " AS " + returnName + ";";
  }

  @Override
  public int hashCode() {
    return 13;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof EachNodeReturnNode;
  }
}