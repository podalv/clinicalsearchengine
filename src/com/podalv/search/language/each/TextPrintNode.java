package com.podalv.search.language.each;

import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.CustomResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.LanguageNode;

public class TextPrintNode extends LanguageNode {

  private final String text;

  public TextPrintNode(final String text) {
    this.text = text;
  }

  @Override
  public LanguageNode copy() {
    return new TextPrintNode(text);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    return new CustomResult(text);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return CustomResult.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String toString() {
    return "\"" + text + "\"";
  }

  @Override
  public int hashCode() {
    return text.hashCode() * 19;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof TextPrintNode && ((TextPrintNode) obj).text.equals(text);
  }
}