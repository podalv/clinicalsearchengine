package com.podalv.search.language.each;

import java.util.HashSet;
import java.util.Iterator;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.CustomResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.LanguageNodeWithOrChildren;
import com.podalv.search.language.node.RootNode;

/** Encapsulates EACH node (basically a program that takes each interval of a particular command
 *  separately and processes it and returns something)
 *
 * @author podalv
 *
 */
public class EachNode extends LanguageNodeWithOrChildren {

  static enum LOOP_STATE {
    BREAK, ABORT, CONTINUE, DEFAULT
  };
  private final static int      MAIN_INDEX           = 0;
  private final VariableContext localContext         = new VariableContext();
  private VariableContext       globalContext        = null;
  private RootNode              root                 = null;
  private final HashSet<String> savedGlobalVariables = new HashSet<>();
  private boolean               contextInitialized   = false;

  public EachNode(final String mainLabel, final LanguageNode mainNode) {
    addChild(mainNode, mainLabel);
  }

  public String getMainLabel() {
    return getChildrenNames().get(MAIN_INDEX);
  }

  public LanguageNode getMainNode() {
    return getChildren().get(MAIN_INDEX);
  }

  public VariableContext getLocalContext() {
    return localContext;
  }

  void setVariable(final String variableName, final TimeIntervals ti) {
    localContext.set(variableName, ti);
  }

  @Override
  public LanguageNode copy() {
    throw new UnsupportedOperationException();
  }

  private LanguageNode findRootNode(final LanguageNode node) {
    if (node.getParent() == null) {
      throw new UnsupportedOperationException("Cannot find root node");
    }
    if (node.getParent() instanceof RootNode) {
      return node.getParent();
    }
    return findRootNode(node.getParent());
  }

  private void initContext() {
    if (!contextInitialized) {
      contextInitialized = true;
      root = (RootNode) findRootNode(this);
      globalContext = EachNodeAtom.findGlobalContext(this, false);
      if (globalContext == null) {
        throw new UnsupportedOperationException("Cannot find global context for variable '" + getMainLabel() + "'");
      }
      for (int x = MAIN_INDEX + 1; x < getChildren().size(); x++) {
        final LanguageNode child = getChildren().get(x);
        if (child instanceof EachNodeReturnNode) {
          savedGlobalVariables.add(((EachNodeReturnNode) child).getReturnName());
        }
      }
    }
  }

  private void clearGlobalVariables() {
    final Iterator<String> i = savedGlobalVariables.iterator();
    while (i.hasNext()) {
      globalContext.set(i.next(), TimeIntervals.getEmptyTimeLine());
    }
  }

  public static LOOP_STATE evaluateChild(final IndexCollection context, final PatientSearchModel patient, final LanguageNode child) {
    if (child instanceof EachNodeContinue) {
      return LOOP_STATE.CONTINUE;
    }
    else if (child instanceof EachNodeFail) {
      return LOOP_STATE.ABORT;
    }
    else if (child instanceof EachNodeExit) {
      return LOOP_STATE.BREAK;
    }
    else {
      final EvaluationResult result = child.evaluate(context, patient);
      if (result instanceof CustomResult) {
        return (LOOP_STATE) ((CustomResult) result).getObject();
      }
    }
    return LOOP_STATE.DEFAULT;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initContext();
    final EvaluationResult result = getMainNode().evaluate(context, patient);
    if (result instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final PayloadIterator pi = result.toTimeIntervals(patient).iterator(patient);
    while (pi.hasNext()) {
      localContext.clear();
      pi.next();
      setVariable(getMainLabel(), new TimeIntervals(this, new IntArrayList(new int[] {pi.getStartId(), pi.getEndId()})));
      LOOP_STATE state = LOOP_STATE.DEFAULT;
      for (int x = MAIN_INDEX + 1; x < getChildren().size(); x++) {
        final LanguageNode child = getChildren().get(x);
        state = evaluateChild(context, patient, child);
        if (state != LOOP_STATE.DEFAULT) {
          break;
        }
      }
      if (state == LOOP_STATE.ABORT) {
        root.abortEvaluation();
        clearGlobalVariables();
      }
      if (state != LOOP_STATE.CONTINUE && state != LOOP_STATE.DEFAULT) {
        break;
      }
    }
    return null;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return getMainNode().generatePreprocessingNode(context, statistics);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("FOR EACH (" + getMainNode() + ") AS (" + getMainLabel() + ") {");
    for (int x = MAIN_INDEX + 1; x < getChildren().size(); x++) {
      result.append(getChildren().get(x).toString());
    }
    result.append("}");
    return result.toString();
  }
}