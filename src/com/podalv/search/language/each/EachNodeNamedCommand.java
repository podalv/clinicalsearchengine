package com.podalv.search.language.each;

import com.podalv.preprocessing.datastructures.IgnoreProcessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.LanguageNodeWithChildren;

public class EachNodeNamedCommand extends LanguageNodeWithChildren {

  private final String    name;
  private VariableContext localContext       = null;
  private VariableContext globalContext      = null;
  private boolean         contextInitialized = false;

  public EachNodeNamedCommand(final String label, final String command, final LanguageNode node) {
    this.name = label;
    addChild(node);
  }

  public String getName() {
    return name;
  }

  private void initContext() {
    if (!contextInitialized) {
      contextInitialized = true;
      localContext = EachNodeAtom.findLocalContext(this);
      globalContext = EachNodeAtom.findGlobalContext(this, false);
      if (localContext == null || globalContext == null) {
        throw new UnsupportedOperationException("Cannot find context for variable '" + name + "'");
      }
    }
  }

  @Override
  public LanguageNode copy() {
    throw new UnsupportedOperationException();
  }

  private void setVariable(final LanguageNode parent, final TimeIntervals startEnds) {
    localContext.set(name, startEnds);
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initContext();
    final EvaluationResult i = getChildren().get(0).evaluate(context, patient);
    if (i instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final TimeIntervals ti = i.toTimeIntervals(patient);
    setVariable(this, ti);
    return ti;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return IgnoreProcessingNode.getInstance();
  }

  @Override
  public String toString() {
    return name + " = " + getChildren().get(0).toString() + ";";
  }

  @Override
  public int hashCode() {
    return 41;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof EachNodeNamedCommand;
  }
}
