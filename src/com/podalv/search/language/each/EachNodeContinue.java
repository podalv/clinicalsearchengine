package com.podalv.search.language.each;

public class EachNodeContinue extends EachNodeExit {

  @Override
  public String toString() {
    return "CONTINUE";
  }
}
