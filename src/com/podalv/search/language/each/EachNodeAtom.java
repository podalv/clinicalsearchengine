package com.podalv.search.language.each;

import com.podalv.preprocessing.datastructures.IgnoreProcessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.RootNode;

/** Any node referenced in the context by a string (for example. ANXIETY = ICD9=220.0)
 *
 * @author podalv
 *
 */
public class EachNodeAtom extends LanguageNode {

  private final String    name;
  private VariableContext globalContext       = null;
  private VariableContext localContext        = null;
  private boolean         initializedContexts = false;

  public EachNodeAtom(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public LanguageNode copy() {
    throw new UnsupportedOperationException();
  }

  private void initContexts() {
    if (!initializedContexts) {
      initializedContexts = true;
      localContext = findLocalContext(this);
      globalContext = findGlobalContext(this, false);
      if (localContext == null || globalContext == null) {
        throw new UnsupportedOperationException("Variable '" + name + "' does not have a context");
      }
    }
  }

  public static VariableContext findLocalContext(final LanguageNode parent) {
    if (parent.getParent() == null) {
      return null;
    }
    if (parent.getParent() instanceof EachNode) {
      return ((EachNode) parent.getParent()).getLocalContext();
    }
    if (parent.getParent() instanceof RootNode) {
      return ((RootNode) parent.getParent()).getContext();
    }
    return findLocalContext(parent.getParent());
  }

  public static VariableContext findGlobalContext(final LanguageNode parent, final boolean isGlobal) {
    if (parent.getParent() == null) {
      return null;
    }
    if (parent.getParent() instanceof EachNode) {
      if (!isGlobal) {
        return findGlobalContext(parent.getParent(), true);
      }
      return ((EachNode) parent.getParent()).getLocalContext();
    }
    if (parent.getParent() instanceof RootNode) {
      return ((RootNode) parent.getParent()).getContext();
    }
    return findGlobalContext(parent.getParent(), isGlobal);
  }

  private TimeIntervals getVariable(final LanguageNode parent) {
    TimeIntervals ti = localContext.get(name);
    if (ti == null) {
      ti = globalContext.get(name);
    }
    if (ti == null) {
      ti = TimeIntervals.getEmptyTimeLine();
    }
    return ti;
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initContexts();
    return getVariable(this);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return IgnoreProcessingNode.getInstance();
  }

  @Override
  public String toString() {
    return name;
  }

}
