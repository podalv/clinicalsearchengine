package com.podalv.search.language.each;

import java.util.HashMap;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.UnionNode;

/** Context of variables and their results
 *
 * @author podalv
 *
 */
public class VariableContext {

  private final HashMap<String, TimeIntervals> labelToStartEnd = new HashMap<String, TimeIntervals>();

  public TimeIntervals get(final String label) {
    return labelToStartEnd.get(label);
  }

  public void set(final String label, final TimeIntervals startEnd) {
    labelToStartEnd.put(label, startEnd.getClone());
  }

  public void remove(final String label) {
    labelToStartEnd.remove(label);
  }

  private void set(final String label, final TimeIntervals startEnd, final boolean clone) {
    if (clone) {
      labelToStartEnd.put(label, startEnd.getClone());
    }
    else {
      labelToStartEnd.put(label, startEnd);
    }
  }

  public void clear() {
    labelToStartEnd.clear();
  }

  public void add(final String label, final TimeIntervals startEnd, final PatientSearchModel patient) {
    final TimeIntervals left = get(label);
    if (left == null) {
      set(label, startEnd);
    }
    else {
      final IntArrayList result = new IntArrayList();
      UnionNode.union(result, left.iterator(patient), startEnd.iterator(patient));
      set(label, new TimeIntervals(startEnd.getNode(), result), false);
    }
  }
}
