package com.podalv.search.language.each;

import com.podalv.preprocessing.datastructures.IgnoreProcessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.node.LanguageNode;

public class EachNodeClearNode extends LanguageNode {

  private final String    name;
  private VariableContext globalContext      = null;
  private boolean         contextInitialized = false;

  public EachNodeClearNode(final String name) {
    this.name = name;
  }

  private void initContext() {
    if (!contextInitialized) {
      contextInitialized = true;
      globalContext = EachNodeAtom.findGlobalContext(this, false);
    }
  }

  @Override
  public LanguageNode copy() {
    throw new UnsupportedOperationException();
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    initContext();
    globalContext.remove(name);
    return null;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return TimeIntervals.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return IgnoreProcessingNode.getInstance();
  }

  @Override
  public String toString() {
    return "CLEAR " + name + ";";
  }
}