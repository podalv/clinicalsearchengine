package com.podalv.search.language.each;

import com.podalv.preprocessing.datastructures.IgnoreProcessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.each.EachNode.LOOP_STATE;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.CustomResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.LanguageNodeWithChildren;

public class EachNodeIf extends LanguageNodeWithChildren {

  private final boolean trueIfEmpty;

  public EachNodeIf(final boolean trueIfEmpty) {
    this.trueIfEmpty = trueIfEmpty;
  }

  @Override
  public LanguageNode copy() {
    throw new UnsupportedOperationException();
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    final EvaluationResult e = getChildren().get(0).evaluate(context, patient);
    if (e instanceof AbortedResult) {
      return AbortedResult.getInstance();
    }
    final boolean isEmpty = e.toTimeIntervals(patient).isEmpty();
    LOOP_STATE state = LOOP_STATE.DEFAULT;
    if ((trueIfEmpty && isEmpty) || (!trueIfEmpty && !isEmpty)) {
      for (int x = 1; x < getChildren().size(); x++) {
        state = EachNode.evaluateChild(context, patient, getChildren().get(x));
        if (state != LOOP_STATE.DEFAULT) {
          break;
        }
      }
    }
    return new CustomResult(state);
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return BooleanResult.class;
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return IgnoreProcessingNode.getInstance();
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("IF ");
    if (!trueIfEmpty) {
      result.append("NOT ");
    }
    result.append("EMPTY (");
    result.append(getChildren().get(0).toString());
    result.append(") {");

    for (int x = 1; x < getChildren().size(); x++) {
      result.append(getChildren().get(x).toString() + ";");
    }
    result.append("}");
    return result.toString();
  }

  @Override
  public int hashCode() {
    return 997;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof EachNodeIf;
  }
}
