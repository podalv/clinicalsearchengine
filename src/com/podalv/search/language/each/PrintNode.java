package com.podalv.search.language.each;

import java.rmi.UnexpectedException;

import com.podalv.preprocessing.datastructures.IgnoreProcessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.CustomResult;
import com.podalv.search.language.evaluation.ErrorResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.TimePoint;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.LanguageNode;
import com.podalv.search.language.node.LanguageNodeWithOrChildren;
import com.podalv.search.language.node.RootNode;

/** Node that emits string based on parameters (equivalent to System.out.println)
 *
 * @author podalv
 *
 */
public class PrintNode extends LanguageNodeWithOrChildren {

  private RootNode                          root          = null;
  private Class<? extends EvaluationResult> returnsResult = null;

  @Override
  public LanguageNode copy() {

    return null;
  }

  private void findRootNode(final LanguageNode node) throws UnexpectedException {
    if (node == null) {
      throw new UnexpectedException(getPositionInfo() + "Could not find root node to output print to");
    }
    if (node instanceof RootNode) {
      root = (RootNode) node;
    }
    else {
      findRootNode(node.getParent());
    }
  }

  @Override
  public EvaluationResult evaluate(final IndexCollection context, final PatientSearchModel patient) {
    if (root == null) {
      try {
        findRootNode(this);
      }
      catch (final UnexpectedException e) {
        throw new UnsupportedOperationException(getPositionInfo() + e.getMessage());
      }
    }
    final StringBuilder line = new StringBuilder();
    for (int x = 0; x < getChildren().size(); x++) {
      this.returnsResult = getChildren().get(x).generatesResultType();
      if (getChildren().get(x).generatesResultType().equals(ErrorResult.class)) {
        final EvaluationResult r = getChildren().get(x).evaluate(context, patient);
        if (r instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        line.append(((ErrorResult) r).toString());
      }
      if (getChildren().get(x).generatesResultType().equals(BooleanResult.class)) {
        final EvaluationResult r = getChildren().get(x).evaluate(context, patient);
        if (r instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        line.append(r.toBooleanResult().result());
      }
      else if (getChildren().get(x).generatesResultType().equals(TimeIntervals.class) || getChildren().get(x).generatesResultType().equals(PayloadPointers.class)) {
        line.append("{");
        final EvaluationResult r = getChildren().get(x).evaluate(context, patient);
        if (r instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        final PayloadIterator iterator = ((TimeIntervals) r).iterator(patient);
        while (iterator.hasNext()) {
          iterator.next();
          line.append("[" + iterator.getStartId() + "," + iterator.getEndId() + "]");
        }
        line.append("}");
      }
      else if (getChildren().get(x).generatesResultType().equals(TimePoint.class)) {
        final EvaluationResult r = getChildren().get(x).evaluate(context, patient);
        if (r instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        final int timePoint = ((TimePoint) r).getTimePoint();
        line.append("[" + timePoint + "]");
      }
      else if (getChildren().get(x).generatesResultType().equals(CustomResult.class)) {
        final EvaluationResult r = getChildren().get(x).evaluate(context, patient);
        if (r instanceof AbortedResult) {
          return AbortedResult.getInstance();
        }
        line.append(((CustomResult) r).getObject().toString());
      }
      else {
        line.append("UNSUPPORTED RESULT TYPE");
      }
    }
    root.printToConsole(line.toString());
    return null;
  }

  @Override
  public Class<? extends EvaluationResult> generatesResultType() {
    return this.returnsResult;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder("PRINT(");
    for (int x = 0; x < getChildren().size(); x++) {
      result.append(getChildren().get(x).toString());
      if (x != getChildren().size() - 1) {
        result.append(",");
      }
    }
    result.append(")");
    return result.toString();
  }

  @Override
  public PreprocessingNode generatePreprocessingNode(final IndexCollection context, final Statistics statistics) {
    return IgnoreProcessingNode.getInstance();
  }

}
