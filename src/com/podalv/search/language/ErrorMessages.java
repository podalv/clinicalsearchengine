package com.podalv.search.language;

import java.util.ArrayList;

/** Error messages in the Language
 *
 * @author podalv
 *
 */
public class ErrorMessages {

  public static final String EXTRANEOUS_INPUT                       = "Extraneous input. Cannot parse query";
  public static final String NUMBER_EXPECTED                        = "Expected a NUMERIC parameter, but found TEXT";
  public static final String QUERY_TOO_COMPLEX_STACK                = "Query is too complex. Request higher stack size from administrator";
  public static final String QUERY_TOO_LONG                         = "Query is too complex and would take too long to execute";
  public static final String UNPARSEABLE_QUERY                      = "Unparseable query";
  public static final String TEMPORAL_COMMAND_WITH_BOOLEAN_ARGUMENT = "Temporal command has a boolean argument";
  public static final String TEMPORAL_COMMAND_RESOLVES_TO_TIMELINE  = "Command always resolves to entire timeline";
  public static final String TEMPORAL_COMMAND_EMPTY                 = "Command always resolves to empty interval";
  public static final String TEMPORAL_COMMAND_EXTEND_BY             = "EXTEND BY trying to resize entire timeline";
  public static final String BEFORE_COMMAND_TIMELINE_ARGUMENT       = "BEFORE command has TIMELINE command as an argument";
  public static final String TEMPORAL_COMMAND_INCORRECT             = "Temporal command always returns every patient or no patients";
  public static final String SYNTAX_ERROR                           = "Syntax error";

  public static final String UNRECOGNIZED_COMMAND                   = "Unrecognized command";
  public static final String NO_ALTERNATIVE_COMMAND                 = "No viable alternative at";
  public static final String SINGLE_ARGUMENT                        = "Command has too many arguments";
  public static final String RESERVED_CHARACTER                     = "Invalid use of '$' keyword";

  public static final String EXTRANEOUS_ORIGINAL                    = "extraneous input";

  public static final String NOT_ENOUGH_PATIENTS                    = "Not enough patients in the cohort to display statistics";

  private static String[]    keywords;

  static {
    ArrayList<String> kw = new ArrayList<>();
    for (int x = 0; x < 1000; x++) {
      String name = ExprParser.VOCABULARY.getLiteralName(x);
      if (name != null) {
        if (name.startsWith("'")) {
          kw.add(name.substring(1, name.length() - 1).toLowerCase());
        }
        else {
          kw.add(name.toLowerCase());
        }
      }
    }
    kw.add("prior");
    keywords = kw.toArray(new String[kw.size()]);
  }

  private static String getKeywords(final String errorMessage) {
    int maxPos = 0;
    String result = null;
    for (int z = 0; z < keywords.length; z++) {
      final String incorrectVariablePrefix = keywords[z];
      final int pos = errorMessage.toUpperCase().lastIndexOf(incorrectVariablePrefix.toUpperCase());
      if (pos != -1) {
        for (int x = pos + incorrectVariablePrefix.length(); x < errorMessage.length(); x++) {
          if (errorMessage.charAt(x) == ' ' || errorMessage.charAt(x) == ',' || errorMessage.charAt(x) == '(' || errorMessage.charAt(x) == ')' || errorMessage.charAt(x) == '\'') {
            if (pos > maxPos) {
              maxPos = pos;
              result = incorrectVariablePrefix;
            }
          }
        }
      }
    }
    return result;
  }

  private static String checkIncorrectVariablePrefix(final String errorMessage) {
    if (errorMessage.indexOf("no viable alternative at input") != -1) {
      final String result = getKeywords(errorMessage);
      if (result != null) {
        return "Variable name cannot contain the specified keyword near " + errorMessage.substring(errorMessage.indexOf("'"));
      }
    }
    return errorMessage;
  }

  public static int getErrorStringLength(final String errorMessage) {
    if (errorMessage != null) {
      final int pos = errorMessage.indexOf(EXTRANEOUS_ORIGINAL);
      if (pos != -1) {
        final int startPos = errorMessage.indexOf('\'', pos);
        final int endPos = errorMessage.indexOf('\'', startPos + 1);
        if (startPos != -1 && endPos != -1 && endPos > startPos) {
          return Math.max(endPos - startPos - 1, 1);
        }
      }
    }
    return 2000;
  }

  public static String modifyErrorMessage(final String originalError) {
    if (originalError != null) {
      String result = originalError.replace("token recognition error at:", UNRECOGNIZED_COMMAND);
      result = checkIncorrectVariablePrefix(result);
      result = result.replace("no viable alternative at input", NO_ALTERNATIVE_COMMAND);
      result = result.replace("mismatched input ',' expecting '{'", SINGLE_ARGUMENT);
      result = result.replace("Could not find variable ''", RESERVED_CHARACTER);
      if (result.indexOf(EXTRANEOUS_ORIGINAL) != -1) {
        return SYNTAX_ERROR;
      }
      return result;
    }
    else {
      return UNRECOGNIZED_COMMAND;
    }
  }

}
