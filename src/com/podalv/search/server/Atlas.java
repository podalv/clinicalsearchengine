package com.podalv.search.server;

import static com.podalv.search.server.AtlasArguments.DISABLE_QUERY_CACHE;
import static com.podalv.search.server.AtlasArguments.ROOT;
import static com.podalv.search.server.AtlasArguments.extractDatabaseSettings;
import static com.podalv.search.server.AtlasArguments.extractPort;
import static com.podalv.search.server.AtlasArguments.extractSettings;
import static com.podalv.search.server.AtlasArguments.forceStartDisabled;
import static com.podalv.search.server.AtlasArguments.getArgument;
import static com.podalv.search.server.AtlasArguments.parseRestrictedIp;
import static com.podalv.search.server.AtlasArguments.parseServerType;
import static com.podalv.search.server.AtlasArguments.testsFailed;
import static com.podalv.utils.Logging.error;
import static com.podalv.utils.Logging.exception;
import static com.podalv.utils.Logging.info;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.Executors;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.RegisterSlaveRequest;
import com.podalv.search.datastructures.RegisterSlaveResponse;
import com.podalv.search.language.script.ScriptParser;
import com.podalv.search.server.TestRunner.RESULT;

public class Atlas {

  private static final String        TERMINATING_SERVER_DUE_TO_FAILED_TESTS = "Terminating server due to failed tests...";
  private final Server               server;
  private final ClinicalSearchEngine engine;

  public ClinicalSearchEngine getEngine() {
    return engine;
  }

  public int getPort() {
    return ((ServerConnector) server.getConnectors()[0]).getLocalPort();
  }

  boolean runTests(final File[] testFiles) {
    QueryCache.getInstance().temporaryDisabled(true);
    try {
      final RESULT r = TestRunner.test(testFiles, engine);
      info("TEST RESULT = " + r.toString());
      return r == RESULT.SUCCESS;
    }
    finally {
      QueryCache.getInstance().temporaryDisabled(false);
    }
  }

  public Atlas(final ClinicalSearchEngine engine, final int port) {
    server = new Server(port);
    this.engine = engine;
  }

  public void stop() throws Exception {
    server.stop();
    server.destroy();
  }

  private synchronized void broadcast(final String groupId, final String target, final long startTime, final int frequency) {
    while (!server.isRunning()) {
      ;
    }
    info("Starting broadcast to '" + target + "' in group '" + groupId + "' in " + Math.max(0, (startTime - System.currentTimeMillis()) / 1000) + " seconds");
    Executors.newCachedThreadPool().submit(new Runnable() {

      @Override
      public void run() {
        if (System.currentTimeMillis() < startTime) {
          try {
            Thread.sleep(Math.max(0, System.currentTimeMillis() - startTime));
          }
          catch (final InterruptedException e) {
            exception(e);
          }
        }
        info("Broadcasting...");
        while (true) {
          final long time = System.currentTimeMillis();
          try {
            final RegisterSlaveResponse resp = new Gson().fromJson(QueryUtils.query(target + AtlasRequestHandler.REGISTER_QUERY, new Gson().toJson(new RegisterSlaveRequest("",
                getPort(), groupId)), 1, 5000), RegisterSlaveResponse.class);
            if (resp.isSuccess()) {
              info("Slave succesfully attached to '" + target + "'");
              return;
            }
            error("Could not attach to '" + target + "'. Reason '" + resp.getError() + "'");
          }
          catch (SocketException | SocketTimeoutException ex) {
            error(ex.getMessage());
          }
          catch (JsonSyntaxException | IOException e) {
            exception(e);
          }
          if (System.currentTimeMillis() < (time + (frequency * 1000))) {
            try {
              Thread.sleep(Math.max(0, (time + (frequency * 1000)) - System.currentTimeMillis()));
            }
            catch (final InterruptedException e) {
              exception(e);
            }
          }
        }
      }
    });
  }

  public void start(final boolean holdThread, final String groupId, final String target, final String ip, final long startTime, final int frequency) throws Exception {
    final AbstractHandler handler = new AtlasRequestHandler(engine, ip);
    server.setHandler(handler);

    final ContextHandler context = new ContextHandler("/");
    server.setHandler(context);

    final HashSessionManager manager = new HashSessionManager();
    final SessionHandler sessions = new SessionHandler(manager);
    context.setHandler(sessions);

    sessions.setHandler(handler);

    server.start();
    if (ip == null) {
      System.out.println("Unrestriced access enabled");
    }
    else {
      System.out.println("Access restricted to '" + ip + "'");
    }
    System.out.println("Started...");
    if (target != null && startTime >= 0 && frequency >= 0) {
      broadcast(groupId, target, startTime, frequency);
    }
    if (holdThread) {
      server.join();
    }
    else {
      Executors.newCachedThreadPool().submit(new Runnable() {

        @Override
        public void run() {
          try {
            server.join();
          }
          catch (final InterruptedException e) {
            exception(e);
          }
        }
      });
    }
  }

  public void start(final boolean holdThread, final String validIp) throws Exception {
    start(holdThread, null, null, validIp, -1, -1);
  }

  public Atlas(final SERVER_TYPE serverType, final ClinicalSearchEngine engine, final ConnectionSettings settings, final int port) {
    this.engine = engine;
    server = new Server(port);

    if (settings != null) {
      try {
        TermToConceptMapExtractor extractor = null;
        extractor = new TermToConceptMapExtractor(settings);
        final HashMap<String, String> var = extractor.getVariables(engine.getIndices().getUniqueTidStrings());
        final Iterator<Entry<String, String>> iterator = var.entrySet().iterator();
        while (iterator.hasNext()) {
          final Entry<String, String> entry = iterator.next();
          ScriptParser.getInstance().getPersistedScripts().addTemporaryScript(entry.getKey(), entry.getValue());
        }
        if (serverType != SERVER_TYPE.SLAVE) {
          engine.getAutosuggest().buildScriptAutosuggest();
        }
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(final String[] args) throws Exception {
    System.out.println("Version = " + AtlasVersion.getVersion());
    System.out.println("Usage: OPTIONS DB_PATH ROOT_FOLDER PORT [database settings file (optional)]");
    System.out.println();
    System.out.println("OPTIONS:       -type=m  = start as master");
    System.out.println("               -type=s  = start as slave");
    System.out.println("               -type=i  = start as independent (default)");
    System.out.println("               -type=w  = start as independent with workshop");
    System.out.println("               -type=h  = start as html server only");
    System.out.println("               -type=mw = start as master with workshop");
    System.out.println("-db=DB_PATH              - location of the database path");
    System.out.println("-root=ROOT_FOLDER        - location where index.html and other web files are located");
    System.out.println("-settings=settings file  - location of the settings.txt file");
    System.out.println("-offheap_size=size       - size of offheap space in GB");
    System.out.println("-port=number             - port of the server");
    System.out.println("-reset_disk_cache        - deletes the off heap disk cache");
    System.out.println("-shard_range=N-M         - loads only the requested shards (example. 5-10)");
    System.out.println("-test=folder             - loads test files from the folder (pid files) and runs them");
    System.out.println("-force_start             - starts server even if tests failed");
    System.out.println("-disable_query_cache     - disables query cache");
    System.out.println("-load_reduced_statistics - loads reduced statistics if available");
    System.out.println("-group_id                - accept only broadcast connections from slaves within this groupId");
    System.out.println("-broadcast_target        - MASTER url to which the SLAVE starts broadcasting register attempts");
    System.out.println("-broadcast_delay         - delay (seconds) after which the slave will start broadcasting to broadcast_target");
    System.out.println("-broadcast_frequency     - frequency (seconds) of broadcasts to the broadcast_target");
    System.err.println("-restrict_request_ip     - accept only requests from the specified ip, if parameter='true', restrict to FIRST ip, otherwise enter ip address");

    if (getArgument(args, DISABLE_QUERY_CACHE) != null) {
      QueryCache.disable();
    }

    final File rootFolder = new File(getArgument(args, ROOT));

    EventFlowDispatcher.setInstance(new EventFlowDispatcher(new File(rootFolder, PatientExport.exportFolder)));
    PatientExport.setInstance(new PatientExport(rootFolder));

    final ClinicalSearchEngine searchEngine = new ClinicalSearchEngine(parseServerType(args), rootFolder, extractDatabaseSettings(args));

    final Atlas atlas = new Atlas(parseServerType(args), searchEngine, extractSettings(args), extractPort(args));

    if (testsFailed(args, atlas) && forceStartDisabled(args)) {
      error(TERMINATING_SERVER_DUE_TO_FAILED_TESTS);
      System.exit(0);
    }
    else {
      if (searchEngine.getServerType() == SERVER_TYPE.SLAVE && searchEngine.getBroadcastTarget() != null) {
        atlas.start(true, searchEngine.getGroupId(), searchEngine.getBroadcastTarget(), parseRestrictedIp(args), searchEngine.getBroadcastStartTime(),
            searchEngine.getBroadcastFrequency());
      }
      atlas.start(true, parseRestrictedIp(args));
    }

  }
}
