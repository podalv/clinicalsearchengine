package com.podalv.search.server;

import static com.podalv.utils.Logging.error;

import java.io.File;
import java.math.BigDecimal;

import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.objectdb.datastructures.DatabaseSettings;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.utils.text.TextUtils;

public class AtlasArguments {

  private static final String PORT                    = "-port=";
  private static final String SETTINGS2               = "-settings=";
  private static final String LOAD_REDUCED_STATISTICS = "-load_reduced_statistics";
  private static final String RESET_DISK_CACHE        = "-reset_disk_cache";
  private static final String OFFHEAP_SIZE            = "-offheap_size=";
  private static final String SLAVE_GROUP_ID          = "-group_id=";
  static final String         FORCE_START             = "-force_start";
  static final String         TYPE                    = "-type=";
  static final String         TEST                    = "-test=";
  static final String         ROOT                    = "-root=";
  static final String         DISABLE_QUERY_CACHE     = "-disable_query_cache";
  static final String         BROADCAST_TARGET        = "-broadcast_target=";
  static final String         BROADCAST_DELAY         = "-broadcast_delay=";
  static final String         BROADCAST_FREQUENCY     = "-broadcast_frequency=";
  static final String         RESTRICTED_IP           = "-restrict_request_ip=";

  private AtlasArguments() {
    // static access only
  }

  static long extractOffHeapSize(final String[] args) {
    BigDecimal offHeapSize = BigDecimal.valueOf(Double.parseDouble(AtlasArguments.getArgument(args, OFFHEAP_SIZE)));
    offHeapSize = offHeapSize.multiply(BigDecimal.valueOf(1000000000));
    return offHeapSize.longValue();
  }

  static DatabaseSettings extractDatabaseSettings(final String[] args) {
    final DatabaseSettings dbSettings = DatabaseSettings.create(PatientBuilder.class, new File(AtlasArguments.getArgument(args, "-db="))).withOffHeapSize(extractOffHeapSize(
        args)).clearDiskCache(AtlasArguments.getArgument(args, RESET_DISK_CACHE) != null).withShards(AtlasArguments.extractShardRange(AtlasArguments.getArgument(args,
            "-shard_range=")));
    dbSettings.setLoadReducedStatistics(AtlasArguments.getArgument(args, LOAD_REDUCED_STATISTICS) != null);
    dbSettings.setSlaveGroupId(AtlasArguments.parseSlaveGroupId(args));
    dbSettings.setBroadcastTarget(AtlasArguments.parseBroadcastTarget(args));
    dbSettings.setBroadcastDelay(AtlasArguments.parseBroadcastDelay(args));
    dbSettings.setBroadcastFrequency(AtlasArguments.parseBroadcastFrequency(args));
    return dbSettings;
  }

  static ConnectionSettings extractSettings(final String[] args) {
    ConnectionSettings settings = null;

    try {
      settings = ConnectionSettings.createFromFile(new File(AtlasArguments.getArgument(args, SETTINGS2)));
    }
    catch (final Exception e) {
      error("Error reading database settings. CUIs are not supported...");
      e.printStackTrace();
    }

    if (settings == null) {
      error("Error reading database settings. CUIs are not supported...");
    }
    return settings;
  }

  static int[] extractShardRange(final String shards) {
    int[] result = new int[0];
    if (shards != null) {
      final int pos = shards.indexOf('-');
      if (pos == -1) {
        final IntOpenHashSet s = new IntOpenHashSet();
        final String[] data = TextUtils.split(shards, ',');
        for (int x = 0; x < data.length; x++) {
          if (!data[x].trim().isEmpty()) {
            s.add(Integer.parseInt(data[x]));
          }
        }
        return s.toArray();
      }
      result = new int[2];
      result[0] = Integer.parseInt(shards.substring(0, pos).trim());
      if (!shards.substring(pos + 1).trim().equals("?")) {
        result[1] = Integer.parseInt(shards.substring(pos + 1).trim());
      }
    }
    return result;
  }

  static String getArgument(final String[] args, final String prefix) {
    for (int x = 0; x < args.length; x++) {
      if (args[x].toLowerCase().equals(prefix.toLowerCase())) {
        return "true";
      }
      if (args[x].toLowerCase().startsWith(prefix.toLowerCase())) {
        return args[x].substring(prefix.length());
      }
    }
    return null;
  }

  static int extractPort(final String[] args) {
    return Integer.parseInt(getArgument(args, PORT));
  }

  static boolean testsFailed(final String[] args, final Atlas atlas) {
    return getArgument(args, TEST) != null && !atlas.runTests(new File(getArgument(args, TEST)).listFiles());
  }

  static boolean forceStartDisabled(final String[] args) {
    return getArgument(args, FORCE_START) == null;
  }

  static String parseSlaveGroupId(final String[] args) {
    return getArgument(args, SLAVE_GROUP_ID);
  }

  static String parseBroadcastTarget(final String[] args) {
    return getArgument(args, BROADCAST_TARGET);
  }

  static String parseRestrictedIp(final String[] args) {
    return getArgument(args, RESTRICTED_IP);
  }

  static int parseBroadcastFrequency(final String[] args) {
    try {
      final String argument = getArgument(args, BROADCAST_FREQUENCY);
      return Integer.parseInt(argument);
    }
    catch (final Exception e) {
      error("Invalid broadcast frequency. '" + getArgument(args, BROADCAST_FREQUENCY) + "' is not a valid number");
    }

    return DatabaseSettings.DEFAULT_BROADCAST_FREQUENCY;
  }

  static int parseBroadcastDelay(final String[] args) {
    try {
      final String argument = getArgument(args, BROADCAST_DELAY);
      return Integer.parseInt(argument);
    }
    catch (final Exception e) {
      error("Invalid broadcast delay. '" + getArgument(args, BROADCAST_DELAY) + "' is not a valid number");
    }

    return DatabaseSettings.DEFAULT_BROADCAST_DELAY;
  }

  static SERVER_TYPE parseServerType(final String[] args) {
    final String type = getArgument(args, TYPE);
    if (type.equalsIgnoreCase("mw")) {
      return SERVER_TYPE.MASTER_WORKSHOP;
    }
    else if (type.equalsIgnoreCase("m")) {
      return SERVER_TYPE.MASTER;
    }
    else if (type.equalsIgnoreCase("s")) {
      return SERVER_TYPE.SLAVE;
    }
    else if (type.equalsIgnoreCase("w")) {
      return SERVER_TYPE.WORKSHOP;
    }
    else if (type.equalsIgnoreCase("h")) {
      return SERVER_TYPE.HTML;
    }
    return SERVER_TYPE.INDEPENDENT;
  }

}
