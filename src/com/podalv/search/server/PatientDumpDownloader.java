package com.podalv.search.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.Gson;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.workshop.datastructures.DumpRequest;

/** Queries the ATLAS for specific patient ids and outputs them to disk (one file par patient)
 *
 * @author podalv
 *
 */
public class PatientDumpDownloader {

  private static void query(final String address, final String query, final File output) throws IOException {
    final URL url = new URL(address);
    //make connection
    final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
    //use post mode
    urlc.setDoOutput(true);
    urlc.setRequestMethod("POST");
    urlc.setAllowUserInteraction(false);
    //send query
    final PrintStream ps = new PrintStream(urlc.getOutputStream());
    ps.print(query);
    ps.close();

    //get result
    final BufferedWriter out = new BufferedWriter(new FileWriter(output));
    final BufferedReader reader = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
    String line;
    while ((line = reader.readLine()) != null) {
      out.write(line);
      out.newLine();
    }
    out.close();
    reader.close();
  }

  public static void main(final String[] args) throws InterruptedException, IOException {
    System.out.println("Usage: URL maxThreadCnt outputFolder pids.txt");
    System.out.println("Example: http://localhost:8080/dump 5 /data/output/ input.txt");

    final File outputFolder = new File(args[2]);

    final ExecutorService executor = Executors.newFixedThreadPool(Integer.parseInt(args[1]));

    final AtomicInteger progress = new AtomicInteger(0);
    final AtomicInteger fail = new AtomicInteger(0);

    final long time = System.currentTimeMillis();

    final TextFileReader reader = new TextFileReader(new File(args[3]), Charset.forName("UTF-8"));

    while (reader.hasNext()) {
      final int pid = Integer.parseInt(reader.next());
      executor.submit(new Runnable() {

        @Override
        public void run() {
          try {
            final DumpRequest req = DumpRequest.createFull(pid);
            req.setSnomed(false);
            query(args[0], new Gson().toJson(req), new File(outputFolder, pid + ".json"));
            final int curr = progress.incrementAndGet();
            if (curr % 10 == 0) {
              System.out.println("Processed " + curr);
            }
          }
          catch (final Exception e) {
            e.printStackTrace();
            fail.incrementAndGet();
          }
        }
      });
    }

    reader.close();

    executor.shutdown();

    executor.awaitTermination(1000, TimeUnit.HOURS);
    System.out.println("DONE");
    System.out.println("SUCCESS / FAIL = " + progress.get() + " / " + fail.get());
    final int elapsed = (int) ((System.currentTimeMillis() - time) / 1000);
    System.out.println((progress.get() + fail.get()) + " patients took " + elapsed + " seconds (" + ((progress.get() + fail.get()) / (elapsed == 0 ? 1 : elapsed))
        + " patients per second)");
  }
}
