package com.podalv.search.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.utils.datastructures.SortPairSingle;

/** Takes a test file (pid file with first line being query), runs the query and compares the pids
 *
 * @author podalv
 *
 */
public class TestRunner {

  public static enum RESULT {
    FAIL, SUCCESS, UNTESTED
  };

  private static SortPairSingle<String, ArrayList<double[]>> readTestFile(final Reader reader) throws IOException {
    String query = null;
    final ArrayList<double[]> pid = new ArrayList<double[]>();
    final BufferedReader r = new BufferedReader(reader);
    String line = r.readLine();
    query = line;
    while ((line = r.readLine()) != null) {
      pid.add(new double[] {Integer.parseInt(line)});
    }
    r.close();
    return new SortPairSingle<String, ArrayList<double[]>>(query.trim(), pid);
  }

  private static boolean compareArrays(final LinkedList<double[]> actual, final ArrayList<double[]> expected) {
    final StringBuilder result = new StringBuilder();
    if (actual.size() != expected.size()) {
      result.append("EXPECTED SIZE / ACTUAL SIZE = " + expected.size() + " / " + actual.size());
    }
    for (final double[] exp : expected) {
      boolean fnd = false;
      for (final double[] act : actual) {
        if (Arrays.equals(exp, act)) {
          fnd = true;
          break;
        }
      }
      if (!fnd) {
        result.append("|MISSING:" + exp[0] + "|");
      }
    }
    System.out.println(result.toString());
    return result.length() == 0;
  }

  public static RESULT test(final File[] testFiles, final ClinicalSearchEngine engine) {
    RESULT result = RESULT.UNTESTED;
    int success = 0;
    int failure = 0;
    for (final File file : testFiles) {
      try {
        final SortPairSingle<String, ArrayList<double[]>> testFile = readTestFile(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")));
        final PatientSearchResponse response = engine.search(PatientSearchRequest.create(testFile.getComparable()));
        if (response.getErrorMessage() != null && !response.getErrorMessage().isEmpty()) {
          result = RESULT.FAIL;
          failure++;
          System.out.println("TEST '" + file.getAbsolutePath() + " FAILED with " + response.getErrorMessage());
        }
        else {
          if (!compareArrays(response.getPatientIds(), testFile.getObject())) {
            failure++;
            result = RESULT.FAIL;
          }
          else {
            success++;
          }
        }
      }
      catch (final IOException e) {
        result = RESULT.FAIL;
        System.out.println("TEST '" + file.getAbsolutePath() + " FAILED with " + e.getMessage());
      }
    }
    if (result == RESULT.UNTESTED && testFiles.length != 0) {
      result = RESULT.SUCCESS;
    }
    System.out.println((success + failure) + " TESTS RAN [SUCCESS=" + success + ", FAILURE=" + failure + "]");
    return result;
  }
}
