package com.podalv.search.server;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

/** Dispatches Event Flow output jobs
 *
 * @author podalv
 *
 */
public class EventFlowDispatcher {

  private final AtomicInteger        uniqueCounter = new AtomicInteger(0);
  private static EventFlowDispatcher INSTANCE      = null;
  private File                       exportFolder  = null;

  public EventFlowDispatcher(final File exportFolder) {
    this.exportFolder = exportFolder;
  }

  public File getExportFolder() {
    return exportFolder;
  }

  public static void setInstance(final EventFlowDispatcher instance) {
    INSTANCE = instance;
  }

  public static EventFlowDispatcher getInstance() {
    return INSTANCE;
  }

  public synchronized EventFlowPatientExport export() {
    try {
      final File fileName = new File(exportFolder, uniqueCounter.incrementAndGet() + PatientExport.EF_SUFIX);
      PatientExport.getInstance().addFileToHistory(fileName.getName());
      PatientExport.getInstance().purgeOldFiles();
      return new EventFlowPatientExport(fileName);
    }
    catch (final Exception e) {
      return null;
    }
  }
}
