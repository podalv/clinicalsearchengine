package com.podalv.search.server;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.maps.primitive.IntIteratorCounter;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.PersistentObjectDatabase;
import com.podalv.objectdb.Transaction;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.DemographicsHistogram;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.PidRequest;
import com.podalv.search.datastructures.PidResponse;
import com.podalv.search.datastructures.StatisticsResponse;
import com.podalv.search.datastructures.index.StringEnumIndex;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.utils.Logging;
import com.podalv.utils.datastructures.SortPairSingle;
import com.podalv.utils.file.FileUtils;

/** Manages search jobs, allows giving estimates for long queries, managest query cache, etc.
 *
 * @author podalv
 *
 */
public class PatientSearchManager {

  private final IndexCollection                indices;
  private final Statistics                     statistics;
  private boolean                              statisticsAggregated = false;
  private final StatisticsResponse             populationStatistics;
  private final PersistentObjectDatabase       db;
  private final HashMap<String, PatientSearch> runningJobs          = new HashMap<>();

  public PatientSearchManager(final PersistentObjectDatabase db, final IndexCollection indices, final Statistics statistics) {
    this.indices = indices;
    this.statistics = statistics;
    this.db = db;
    populationStatistics = calculatePopulatonStatistics();
  }

  public int getRunningJobCnt() {
    return runningJobs.size();
  }

  public boolean isStatisticsAggregated() {
    return statisticsAggregated;
  }

  private StatisticsResponse calculatePopulatonStatistics() {
    final StatisticsResponse result = new StatisticsResponse();
    result.setPatientCnt(statistics.getPatientCnt());
    try {
      calculateEncounterCnt(result);
    }
    catch (final Exception e) {
      e.printStackTrace();
      result.setEncounterCnt(-1);
    }
    result.setGender(calculateGenders());
    result.setRace(calculateRaces());
    result.setAges(calculateAges());
    return result;
  }

  private DemographicsHistogram[] calculateGenders() {
    final ArrayList<DemographicsHistogram> result = new ArrayList<>();
    final HashMap<String, Integer> keyToCount = new HashMap<>();
    final String[] keys = indices.getDemographics().getGender().keys();
    for (final String key : keys) {
      keyToCount.put(key, indices.getDemographics().getGender().getCount(key));
    }
    final Iterator<Entry<String, Integer>> entries = keyToCount.entrySet().iterator();
    while (entries.hasNext()) {
      final Entry<String, Integer> entry = entries.next();
      if (entry.getValue() > 0 && indices.getDemographics().getGender().getId(entry.getKey()) != StringEnumIndex.NULL_ID) {
        result.add(new DemographicsHistogram(entry.getKey(), 0, entry.getValue(), 0, entry.getValue() / (double) statistics.getPatientCnt(), statistics.getPatientCnt(),
            statistics.getPatientCnt()));
      }
    }
    return result.toArray(new DemographicsHistogram[result.size()]);
  }

  private DemographicsHistogram[] calculateRaces() {
    final ArrayList<DemographicsHistogram> result = new ArrayList<>();
    final HashMap<String, Integer> keyToCount = new HashMap<>();
    final String[] keys = indices.getDemographics().getRace().keys();
    for (final String key : keys) {
      keyToCount.put(key, indices.getDemographics().getRace().getCount(key));
    }
    final Iterator<Entry<String, Integer>> entries = keyToCount.entrySet().iterator();
    while (entries.hasNext()) {
      final Entry<String, Integer> entry = entries.next();
      if (entry.getValue() > 0 && indices.getDemographics().getRace().getId(entry.getKey()) != StringEnumIndex.NULL_ID) {
        result.add(new DemographicsHistogram(entry.getKey(), 0, entry.getValue(), 0, entry.getValue() / (double) statistics.getPatientCnt(), statistics.getPatientCnt(),
            statistics.getPatientCnt()));
      }
    }
    return result.toArray(new DemographicsHistogram[result.size()]);
  }

  private DemographicsHistogram[] calculateAges() {
    final IntArrayList ages = statistics.getAges();
    final DemographicsHistogram[] result = new DemographicsHistogram[ages.size()];
    for (int x = 0; x < ages.size(); x++) {
      result[x] = new DemographicsHistogram(x + "", 0, ages.get(x), 0, ages.get(x) / (double) statistics.getPatientCnt(), statistics.getPatientCnt(), statistics.getPatientCnt());
    }
    return result;
  }

  public static void addValue(final HashMap<Long, Long> map, final long key) {
    Long cnt = map.get(key);
    if (cnt == null) {
      cnt = Long.valueOf(1);
    }
    else {
      cnt = Long.valueOf(cnt.intValue() + 1);
    }
    map.put(key, cnt);
  }

  public static ArrayList<long[]> sortMap(final HashMap<Long, Long> valueToCnt) {
    final ArrayList<SortPairSingle<Long, Long>> sortableList = new ArrayList<>();
    final Iterator<Entry<Long, Long>> entries = valueToCnt.entrySet().iterator();
    while (entries.hasNext()) {
      final Entry<Long, Long> entry = entries.next();
      sortableList.add(new SortPairSingle<>(entry.getKey(), entry.getValue()));
    }
    Collections.sort(sortableList);
    final ArrayList<long[]> result = new ArrayList<>();
    for (int x = 0; x < sortableList.size(); x++) {
      result.add(new long[] {sortableList.get(x).getComparable(), sortableList.get(x).getObject()});
    }
    return result;
  }

  private void calculateEncounterCnt(final StatisticsResponse response) throws Exception {
    BigInteger result = BigInteger.valueOf(0);
    final HashMap<Long, Long> durationToCnt = new HashMap<>();
    final HashMap<Long, Long> encounterToCnt = new HashMap<>();
    final HashMap<Long, Long> deathToCnt = new HashMap<>();
    final HashMap<Long, Long> censoringToCnt = new HashMap<>();
    final PreprocessingNode node = db.optimizePidOrder(new AtomPreprocessingNode(new IntIteratorCounter(0, statistics.getMaxPatientId())));
    PatientSearchModel patient = null;
    Transaction transaction = null;
    int cnt = 0;
    try {
      transaction = Transaction.create();
      while (node.next()) {
        final int patientId = node.getValue();
        if (patient == null) {
          patient = new PatientSearchModel(indices, db.getCache(), patientId);
        }
        else {
          patient.setId(patientId);
        }
        db.read(transaction, patient);
        if (patient.isLoaded()) {
          cnt++;
          result = result.add(BigInteger.valueOf(patient.getEncounterDays().size() / 2));
          addValue(durationToCnt, (patient.getEndTime() - patient.getStartTime()) / (60 * 24));
          addValue(encounterToCnt, patient.getEncounterDays().size() / 2);
          if (indices.getDemographics().getDeathTime(patientId) > 0) {
            addValue(deathToCnt, (indices.getDemographics().getDeathTime(patientId) - patient.getStartTime()) / (60 * 24));
          }
          else {
            addValue(censoringToCnt, (patient.getEndTime() - patient.getStartTime()) / (60 * 24));
          }
        }
      }
    }
    finally {
      FileUtils.close(transaction);
    }
    Logging.info(cnt + " patients were read to get the statistics");
    response.setEncounterCnt(result.longValue());
    response.setDurations(sortMap(durationToCnt));
    response.setEncounters(sortMap(encounterToCnt));
    response.setDeaths(sortMap(deathToCnt));
    response.setCensored(sortMap(censoringToCnt));
  }

  public void aggregateStatistics(final StatisticsResponse[] slaveResponses) {
    if (!isStatisticsAggregated()) {
      populationStatistics.add(slaveResponses);
    }
    statisticsAggregated = true;
  }

  public StatisticsResponse getPopulationStatistics() {
    return populationStatistics;
  }

  public synchronized void kill() {
    synchronized (runningJobs) {
      final Iterator<Entry<String, PatientSearch>> i = runningJobs.entrySet().iterator();
      while (i.hasNext()) {
        final Entry<String, PatientSearch> entry = i.next();
        entry.getValue().kill();
      }
    }
  }

  public PatientSearchResponse isQueryInProgress(final PatientSearchRequest request) {
    synchronized (runningJobs) {
      if (runningJobs.containsKey(request.getQuery())) {
        final PatientSearch inProgress = runningJobs.get(request.getQuery());
        final int progress = inProgress.getProcessedPatientCnt();
        final int found = inProgress.getFoundPatients();
        return PatientSearchResponse.createError("Processed " + progress + " patients (" + found + " found)", 1);
      }
    }
    return null;
  }

  public PatientSearchResponse submitSearchTask(final int queryId, final PatientSearchRequest request, final int returnHowManyPatientIds) {
    return submitSearchTask(SERVER_TYPE.INDEPENDENT, queryId, request, returnHowManyPatientIds);
  }

  public PatientSearchResponse submitSearchTask(final SERVER_TYPE type, final int queryId, final PatientSearchRequest request, final int returnHowManyPatientIds) {
    PatientSearchResponse response = QueryCache.getInstance().getQueryResult(request);
    if (response != null) {
      return response;
    }

    PatientSearch search = null;
    int progress = 0;
    int found = 0;
    try {
      synchronized (runningJobs) {
        if (!runningJobs.containsKey(request.getQuery())) {
          search = runningJobs.get(request.getQuery());
          response = new PatientSearchResponse(queryId, indices, indices.getUmls(), statistics);
          response.withPatientIds(returnHowManyPatientIds);
          response.setBinaryResponse(request.isBinarySearchRequest());
          search = new PatientSearch(type, db, indices, statistics);
          runningJobs.put(request.getQuery(), search);
        }
        else {
          final PatientSearch inProgress = runningJobs.get(request.getQuery());
          progress = inProgress.getProcessedPatientCnt();
          found = inProgress.getFoundPatients();
        }
      }
      if (search != null) {
        search.search(response, request);
      }
      else {
        response = PatientSearchResponse.createError("Query in progress. Processed " + progress + " patients. Found " + found, queryId);
      }
      return response;
    }
    finally {
      if (search != null) {
        synchronized (runningJobs) {
          runningJobs.remove(request.getQuery());
        }
      }
    }
  }

  public void submitSearchTask(final int queryId, final PidRequest request, final OutputStream out) throws IOException {
    final PidResponse response = request.isBinaryPidRequest() ? PidResponse.createBinaryResponse(queryId, null, null, null)
        : PidResponse.createStringResponse(queryId, null, null, null);
    final PatientSearch search = new PatientSearch(db, indices, statistics);
    search.search(response, request);
    response.write(out);
  }
}