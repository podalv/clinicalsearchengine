package com.podalv.search.server;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.list.LongArrayList;
import com.podalv.maps.primitive.list.ShortArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.utils.datastructures.SortPair;

public class ShardCalculator {

  static ArrayList<SortPair<Integer, Integer>> shardPatientCnts       = new ArrayList<>();
  static IntKeyIntOpenHashMap                  shardToPatientCnt      = new IntKeyIntOpenHashMap();
  static IntArrayList                          shardSizeBytes         = new IntArrayList();
  static int[]                                 patientsPerShard;
  static final ArrayList<int[]>                shardDistribution      = new ArrayList<>();
  static final LongArrayList                   shardDistributionBytes = new LongArrayList();
  static ShortArrayList                        objectIdToShardId      = new ShortArrayList();
  static int                                   shardCount;

  private static boolean canStoreIntoShard(final int shardId, final long addToSize) {
    int patientCnt = 0;
    BigInteger size = BigInteger.valueOf(0);
    for (int x = 0; x < shardDistribution.get(shardId).length; x++) {
      patientCnt += shardPatientCnts.get(shardDistribution.get(shardId)[x]).getComparable();
      size = size.add(BigInteger.valueOf(shardSizeBytes.get(shardDistribution.get(shardId)[x])));
    }
    return patientCnt < patientsPerShard[shardId] && size.longValue() + addToSize < shardDistributionBytes.get(shardId);
  }

  private static int getEmptiest(final long size) {
    int min = Integer.MAX_VALUE;
    int pos = -1;
    for (int x = 0; x < shardDistribution.size(); x++) {
      if (shardDistribution.get(x).length < min && canStoreIntoShard(x, size)) {
        min = shardDistribution.get(x).length;
        pos = x;
      }
    }
    return pos;
  }

  private static long getShardSize(final int shardId) {
    BigInteger size = BigInteger.valueOf(0);
    for (int x = 0; x < shardDistribution.get(shardId).length; x++) {
      size = size.add(BigInteger.valueOf(shardSizeBytes.get(shardDistribution.get(shardId)[x])));
    }
    return size.longValue();
  }

  private static int getShortest(final long addToSize) {
    long min = Long.MAX_VALUE;
    int pos = -1;
    for (int x = 0; x < shardDistribution.size(); x++) {
      final long size = getShardSize(x);
      if (getShardSize(x) + addToSize < shardDistributionBytes.get(x)) {
        if (size < min) {
          min = size;
          pos = x;
        }
      }
    }
    return pos;
  }

  private static void readIndex(final File file) throws IOException {
    final DataInputStream stream = new DataInputStream(new BufferedInputStream(new FileInputStream(new File(file, "index"))));
    shardCount = stream.readInt();
    int size = stream.readInt();
    for (int x = 0; x < size; x++) {
      final int key = stream.readInt();
      for (int i = objectIdToShardId.size(); i <= key; i++) {
        objectIdToShardId.add((short) -1);
      }
      final int value = stream.readInt();
      System.out.println(file.getName() + " / " + key + " / " + value);
      if (key == 131010) {
        System.out.println("PID = " + value);
      }
      if (value > Short.MAX_VALUE) {
        stream.close();
        throw new IOException("There are too many shards. Maximum number of shards is " + Short.MAX_VALUE);
      }
      shardToPatientCnt.put(value, shardToPatientCnt.get(value) + 1);
    }
    size = stream.readInt();
    for (int x = 0; x < size; x++) {
      stream.readInt();
      final long value = stream.readLong();
      if (value > Integer.MAX_VALUE) {
        stream.close();
        throw new UnsupportedOperationException("Shard size is too large");
      }
    }
    stream.close();
  }

  public static int getPatientIdsInShard(final int shardId) {
    return shardToPatientCnt.get(shardId);
  }

  public static void main(final String[] args) throws InstantiationException, IllegalAccessException, IOException {
    System.out.println("Usage: folder MASTER_MEM_SIZE SLAVE_MEM_SIZE_GB SLAVE_CNT");
    readIndex(new File(args[0]));
    System.out.println("Database loaded...");
    final int masterSize = Integer.parseInt(args[1]);
    final int slaveSize = Integer.parseInt(args[2]);
    final int slaveCnt = Integer.parseInt(args[3]);
    int totalPatientCnt = 0;
    for (int x = 0; x < shardCount; x++) {
      System.out.println("Inspecting file " + x + ".offheap");
      shardSizeBytes.add((int) new File(args[0], x + ".offheap").length());
      shardPatientCnts.add(new SortPair<>(getPatientIdsInShard(x), x));
      totalPatientCnt += shardPatientCnts.get(shardPatientCnts.size() - 1).getComparable();
    }
    System.out.println("Total patient cnt = " + totalPatientCnt);
    final double patientsPerGig = totalPatientCnt / (double) (masterSize + (slaveSize * slaveCnt));
    patientsPerShard = new int[slaveCnt + 1];
    shardDistributionBytes.add(BigInteger.valueOf(masterSize).multiply(BigInteger.valueOf(1000000000)).longValue());
    int patientRemainder = totalPatientCnt;
    for (int x = 1; x < patientsPerShard.length; x++) {
      patientsPerShard[x] = (int) (patientsPerGig * slaveSize);
      shardDistributionBytes.add(BigInteger.valueOf(slaveSize).multiply(BigInteger.valueOf(1000000000)).longValue());
      patientRemainder -= patientsPerShard[x];
    }
    for (int x = 0; x < patientsPerShard.length; x++) {
      System.out.println("Patients per shard " + x + " = " + patientsPerShard[x]);
    }

    for (int x = 0; x < slaveCnt + 1; x++) {
      shardDistribution.add(new int[0]);
    }
    patientsPerShard[0] = patientRemainder;
    Collections.sort(shardPatientCnts);
    System.out.println("Calculating shards...");
    for (int x = shardPatientCnts.size() - 1; x >= 0; x--) {
      int emptiestId = getEmptiest(shardSizeBytes.get(shardPatientCnts.get(x).getObject()));
      if (emptiestId == -1) {
        emptiestId = getShortest(shardSizeBytes.get(shardPatientCnts.get(x).getObject()));
      }
      int[] vals = shardDistribution.get(emptiestId);
      vals = Arrays.copyOf(vals, vals.length + 1);
      vals[vals.length - 1] = shardPatientCnts.get(x).getObject();
      shardDistribution.set(emptiestId, vals);
    }
    System.out.println("Outputting...");
    for (int x = 0; x < shardDistribution.size(); x++) {
      int patientCnt = 0;
      BigInteger size = BigInteger.valueOf(0);
      final StringBuilder l = new StringBuilder();
      for (int y = 0; y < shardDistribution.get(x).length; y++) {
        patientCnt += shardPatientCnts.get(shardDistribution.get(x)[y]).getComparable();
        size = size.add(BigInteger.valueOf(shardSizeBytes.get(shardDistribution.get(x)[y])));
        l.append(shardDistribution.get(x)[y] + ",");
      }

      final StringBuilder line = new StringBuilder("ID " + x + "[" + patientCnt + "," + size.longValue() + "] " + l.toString());
      System.out.println(line.toString());
    }
  }
}
