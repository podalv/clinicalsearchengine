package com.podalv.search.server;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.podalv.input.Input;
import com.podalv.input.StreamInput;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.ShortArrayList;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;

public class StatisticsExplorer {

  static ShortArrayList objectIdToShardId = new ShortArrayList();
  static int            shardCount;

  private static void readIndex(final File file) throws IOException {
    final DataInputStream stream = new DataInputStream(new BufferedInputStream(new FileInputStream(new File(file, "index"))));
    shardCount = stream.readInt();
    System.out.println("SHARD CNT = " + shardCount);
    int size = stream.readInt();
    for (int x = 0; x < size; x++) {
      final int key = stream.readInt();
      for (int i = objectIdToShardId.size(); i <= key; i++) {
        objectIdToShardId.add((short) -1);
      }
      final int value = stream.readInt();
      if (key == 92326) {
        System.out.println(key + " = " + value);
      }
      if (value > Short.MAX_VALUE) {
        stream.close();
        throw new IOException("There are too many shards. Maximum number of shards is " + Short.MAX_VALUE);
      }
    }
    size = stream.readInt();
    for (int x = 0; x < size; x++) {
      stream.readInt();
      final long value = stream.readLong();
      if (value > Integer.MAX_VALUE) {
        stream.close();
        throw new UnsupportedOperationException("Shard size is too large");
      }
    }
    stream.close();
  }

  public static void main(final String[] args) throws IOException {
    System.out.println("Usage: path to database");
    readIndex(new File(args[0]));
    System.exit(0);
    final Statistics stat = new Statistics();
    final IndexCollection indices = new IndexCollection();
    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(new File(args[0]), "statistics")));
    Input i = new StreamInput(bis);
    stat.load(i, 0);
    bis.close();

    bis = new BufferedInputStream(new FileInputStream(new File(new File(args[0]), "indices")));
    i = new StreamInput(bis);
    indices.load(i, 0);
    bis.close();

    final int id = indices.getIcd9("96..01");
    final IntIterator l = stat.getCptPatients(id);
    while (l.hasNext()) {
      System.out.println(l.next());
    }

  }
}
