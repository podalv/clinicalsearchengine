package com.podalv.search.server;

import static com.podalv.utils.Logging.debug;
import static com.podalv.utils.Logging.error;
import static com.podalv.utils.Logging.exception;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import com.podalv.utils.datastructures.LongMutable;
import com.podalv.utils.datastructures.SortPair;
import com.podalv.utils.file.FileUtils;

/** Encapsulates all code that allows exporting the queried patient IDs into a text file
 *
 * @author podalv
 *
 */
public class PatientExport {

  public static final long                        FIVE_MINUTES    = 1 * 60000;
  private final ArrayList<SortPair<String, Long>> saveFileHistory = new ArrayList<>();
  public static final String                      FILE_SUFIX      = "_patients.txt";
  public static final String                      EF_SUFIX        = "_export.csv";
  public static final String                      exportFolder    = "export";
  private long                                    oldestFileTime  = Long.MAX_VALUE;
  private final File                              folder;
  private final LongMutable                       fileIds         = new LongMutable(System.currentTimeMillis());
  private static PatientExport                    INSTANCE        = null;

  public void addFileToHistory(final String fileName) {
    saveFileHistory.add(new SortPair<>(fileName, System.currentTimeMillis()));
  }

  public void deleteFile(final String fileNameWithoutPath) {
    final File file = new File(folder, fileNameWithoutPath);
    debug("Purging file " + file.getAbsolutePath());
    FileUtils.deleteWithMessage(file);
  }

  public void exemptFileFromPurge(final String fileName) {
    if (fileName != null) {
      try {
        final String exemptFile = fileName.substring((exportFolder + File.separator).length());
        debug("File '" + exemptFile + "' is exempt from purge");
        for (int x = 0; x < saveFileHistory.size(); x++) {
          if (saveFileHistory.get(x).getComparable().equals(exemptFile)) {
            saveFileHistory.remove(x);
            break;
          }
        }
      }
      catch (final Exception e) {
        exception(e);
      }
    }
  }

  public static PatientExport getInstance() {
    return INSTANCE;
  }

  public long getNextId() {
    return fileIds.incrementAndGet();
  }

  public static void setInstance(final PatientExport instance) {
    INSTANCE = instance;
  }

  public PatientExport(final File rootfolder) {
    this.folder = rootfolder;
    deleteAll();
  }

  private void deleteAll() {
    final File export = new File(folder, exportFolder);
    if (!export.exists()) {
      if (!export.mkdirs()) {
        error("Error creating folder " + export);
      }
    }
    final File[] files = export.listFiles();
    if (files != null) {
      for (final File file : files) {
        if (file.getAbsolutePath().endsWith(FILE_SUFIX) || file.getAbsolutePath().endsWith("event_flow.txt")) {
          FileUtils.deleteWithMessage(file);
        }
      }
    }
  }

  public String writeFile(final String contents) {
    try {
      File f = null;
      String fileName = null;
      while (f == null || f.exists()) {
        fileName = String.valueOf(fileIds.incrementAndGet()) + FILE_SUFIX;
        f = new File(new File(folder, exportFolder), fileName);
      }
      final BufferedWriter writer = new BufferedWriter(new FileWriter(f));
      writer.write(contents);
      writer.close();
      final long time = System.currentTimeMillis();
      oldestFileTime = Math.min(oldestFileTime, time);
      saveFileHistory.add(new SortPair<>(fileName, time));
      return exportFolder + File.separatorChar + fileName;
    }
    catch (final Exception e) {
      return null;
    }
  }

  public void purgeOldFiles() {
    if (oldestFileTime > System.currentTimeMillis() - FIVE_MINUTES) {
      return;
    }
    oldestFileTime = Long.MAX_VALUE;
    for (int x = 0; x < saveFileHistory.size(); x++) {
      try {
        if (saveFileHistory.get(x).getObject() <= (System.currentTimeMillis() - FIVE_MINUTES)) {
          debug("Purging file '" + saveFileHistory.get(x).getComparable() + "'");
          FileUtils.deleteWithMessage(new File(new File(folder, exportFolder), saveFileHistory.get(x).getComparable()));
          saveFileHistory.remove(x);
          x--;
        }
        else {
          oldestFileTime = Math.min(oldestFileTime, saveFileHistory.get(x).getObject());
        }
      }
      catch (final Exception e) {
        exception("Cannot delete file '" + saveFileHistory.get(x).getComparable() + "'", e);
      }
    }
  }

}
