package com.podalv.search.server;

import static com.podalv.utils.Logging.debug;
import static com.podalv.utils.Logging.error;
import static com.podalv.utils.Logging.exception;
import static com.podalv.utils.Logging.info;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.podalv.dump.datastructures.DirectRequest;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;
import com.podalv.input.OffHeapInput;
import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.PersistentObjectDatabase;
import com.podalv.objectdb.Transaction;
import com.podalv.objectdb.datastructures.DatabaseSettings;
import com.podalv.output.StreamOutput;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.preprocessing.datastructures.ProcessingNodeFromPids;
import com.podalv.search.autosuggest.AutosuggestSearch;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestResponse;
import com.podalv.search.datastructures.AutosuggestTextRequest;
import com.podalv.search.datastructures.AutosuggestTextResponse;
import com.podalv.search.datastructures.DictionaryRequest;
import com.podalv.search.datastructures.DictionaryResponse;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.PidRequest;
import com.podalv.search.datastructures.RegisterSlaveResponse;
import com.podalv.search.datastructures.SlaveListResponse;
import com.podalv.search.datastructures.StatisticsResponse;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.script.ScriptParser;
import com.podalv.search.language.script.ScriptStorage;
import com.podalv.utils.Logging;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.utils.text.TextUtils;
import com.podalv.workshop.AggregatedPatientResponse;
import com.podalv.workshop.BooleanResponse;
import com.podalv.workshop.ContainsPatientRequest;
import com.podalv.workshop.PatientDataRequest;
import com.podalv.workshop.datastructures.AtlasInterface;
import com.podalv.workshop.datastructures.DemographicsResponse;
import com.podalv.workshop.datastructures.DumpRequest;
import com.podalv.workshop.datastructures.DumpResponse;

/** Wrapper instantiating the Clinical Search Engine server
 *
 *  Prerequisites for data:
 *  There are maps of: Code => number
 *  Each of the codes in a patient has a paylod id
 *  Payload ids map to payload information, where payloads can be:
 *
 *  Notes: StartTime, NoteType, StartTime, NoteType
 *  ICD9: StartTime, EndTime, VisitId...
 *
 *  It must be guaranteed that the payload data is sorted by StartTime, EndTime, VisitId
 *  or
 *  StartTime, NoteId
 *
 *  It is also guaranteed that the order of StartTime / EndTime is always sorted with each evaluation
 *
 * @author podalv
 *
 */
public class ClinicalSearchEngine implements Closeable, AtlasInterface {

  public static final long               FIVE_MINUTES       = 10 * 60 * 1000;
  private final SERVER_TYPE              serverType;
  public static final String             SLAVES_FILE        = "slaves.dat";
  public static final String             AUTOSUGGEST_FILE   = "autosuggest.dat";
  private final File                     ROOT_FOLDER;
  private final PersistentObjectDatabase database;
  private final IndexCollection          indices;
  private final PatientDataDump          dump;
  private final UmlsDictionary           umls               = UmlsDictionary.create();
  private final Statistics               statistics         = new Statistics();
  private final AutosuggestSearch        autosuggest;
  private final DictionaryExport         dictionaryExport;
  private SearchEngineSlaveManager       slaveManager;
  private PatientSearchManager           searchManager;
  private String                         groupId            = null;
  private String                         broadcastTarget    = null;
  private long                           broadcastStartTime = DatabaseSettings.DEFAULT_BROADCAST_DELAY;
  private int                            broadcastFrequency = DatabaseSettings.DEFAULT_BROADCAST_FREQUENCY;
  private static final AtomicInteger     queryId            = new AtomicInteger(1);
  private final Semaphore                queryLimit         = new Semaphore(10, true);

  public void setSlaveManager(final SearchEngineSlaveManager manager) {
    slaveManager = manager;
  }

  DirectRequest getDumpRequest() {
    if (DumpRequestRegistry.getInstance() == null) {
      DumpRequestRegistry.create();
    }
    return DumpRequestRegistry.getInstance().process();
  }

  String killQueries() {
    searchManager.kill();
    return getQueryStatus();
  }

  public String getGroupId() {
    return groupId;
  }

  public String getBroadcastTarget() {
    return broadcastTarget;
  }

  public int getBroadcastFrequency() {
    return broadcastFrequency;
  }

  public long getBroadcastStartTime() {
    return broadcastStartTime;
  }

  String getQueryStatus() {
    return "{\"runningJobCount\":\"" + searchManager.getRunningJobCnt() + "\"}";
  }

  public SERVER_TYPE getServerType() {
    return serverType;
  }

  public PersistentObjectDatabase getDatabase() {
    return database;
  }

  public boolean containsSlave(final String url) {
    return slaveManager.containsSlave(url);
  }

  public RegisterSlaveResponse registerSlave(final String url, final String groupId) {
    if (serverType != SERVER_TYPE.MASTER && serverType != SERVER_TYPE.MASTER_WORKSHOP) {
      return new RegisterSlaveResponse(false, RegisterSlaveResponse.ERROR_NOT_MASTER);
    }
    if (this.groupId != null && groupId != null && this.groupId.equals(groupId)) {
      slaveManager.addSlave(url.toLowerCase());
      return new RegisterSlaveResponse(true, null);
    }
    return new RegisterSlaveResponse(false, RegisterSlaveResponse.ERROR_INVALID_GROUP_ID);
  }

  public Statistics getStatistics() {
    return statistics;
  }

  public IndexCollection getIndices() {
    return indices;
  }

  public AutosuggestSearch getAutosuggest() {
    return autosuggest;
  }

  private DumpResponse dumpDistributed(final DumpRequest req) {
    return dumpDistributed(req, Transaction.create());
  }

  private DumpResponse dumpDistributed(final DumpRequest req, final Transaction transaction) {
    if (database.containsInMemory(req.getPatientId()) || (serverType == SERVER_TYPE.INDEPENDENT || serverType == SERVER_TYPE.WORKSHOP)) {
      return dump(transaction, req);
    }
    else {
      if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
        return slaveManager.querySlavesDump(req);
      }
      error("Cannot dump patient data " + req.getPatientId() + " pid does not exist");
    }
    return DumpResponse.createError("Patient '" + req.getPatientId() + "' does not exist");
  }

  private void dumpDistributed(final DumpRequest req, final PrintWriter output) {
    if (req.getPatientId() == null) {
      output.write("[\n");
      Transaction transaction = null;
      try {
        transaction = Transaction.create();
        final PreprocessingNode node = database.optimizePidOrder(new AtomPreprocessingNode(new IntArrayCloneableIterator(new IntArrayList(req.getPatientIds()))));
        while (node.next()) {
          final String response = QueryUtils.toJson(dumpDistributed(req.clone(node.getValue()), transaction));
          if (!response.isEmpty()) {
            output.write(response + "\n");
          }
        }
        output.write("]");
      }
      finally {
        FileUtils.close(transaction);
      }
    }
    else {
      output.write(QueryUtils.toJson(dumpDistributed(req)));
    }
  }

  StatisticsResponse statistics() {
    if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
      if (!searchManager.isStatisticsAggregated()) {
        searchManager.aggregateStatistics(slaveManager.querySlavesStatistics());
      }
      return searchManager.getPopulationStatistics();
    }
    else {
      return searchManager.getPopulationStatistics();
    }
  }

  void serveFile(final String target, final HttpServletResponse response) throws IOException {
    final File file = new File(ROOT_FOLDER, target);
    if (!file.exists()) {
      response.sendError(404, "Requested functionality does not exist");
    }
    else {
      if (target.endsWith(".css")) {
        response.setContentType("text/css");
      }
      else if (target.endsWith(".png")) {
        response.setContentType("image/png");
        response.getOutputStream().write(FileUtils.readBinaryFile(ROOT_FOLDER, target));
        response.getOutputStream().flush();
        return;
      }
      else if (target.endsWith(".gif")) {
        response.setContentType("image/gif");
        response.getOutputStream().write(FileUtils.readBinaryFile(ROOT_FOLDER, target));
        response.getOutputStream().flush();
        return;
      }
      FileUtils.copyFileToStream(file, Charset.forName("UTF-8"), response.getWriter());
    }
  }

  void demographics(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    final PatientDataRequest req = new Gson().fromJson(new InputStreamReader(request.getInputStream()), PatientDataRequest.class);
    final Transaction t = Transaction.create();
    final DemographicsResponse r = new DemographicsResponse();
    final PreprocessingNode node = database.optimizePidOrder(new ProcessingNodeFromPids(req.getPatientId()));
    try {
      final PatientSearchModel model = new PatientSearchModel(indices, database.getCache(), -1);
      while (node.next()) {
        model.setId(node.getValue());
        try {
          database.read(t, model);
        }
        catch (final Exception e) {
          e.printStackTrace();
        }
        r.addPatient(node.getValue(), model.getStartTime(), model.getEndTime(), indices.getDeath(node.getValue()), indices.getGender(node.getValue()), indices.getRace(
            node.getValue()), indices.getEthnicity(node.getValue()));
      }
    }
    finally {
      if (t != null) {
        FileUtils.close(t);
      }
    }
    r.sort();
    QueryUtils.writeJson(r, response.getWriter());
  }

  void aggregate(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    final PatientDataRequest req = new Gson().fromJson(new InputStreamReader(request.getInputStream()), PatientDataRequest.class);
    System.out.println("Aggregate " + req.getPatientId().length);
    final ArrayList<AggregatedPatientResponse> resp = new ArrayList<>();
    final long time = System.currentTimeMillis();
    for (int x = 0; x < req.getPatientId().length; x++) {
      resp.add(new AggregatedPatientResponse(req.getPatientId()[x], dumpDistributed(DumpRequest.createWorkshopRequest(req.getPatientId()[x]))));
    }
    System.out.println("Dumped in " + (System.currentTimeMillis() - time));
    debug("Sent " + resp.size());
    QueryUtils.writeJson(resp, response.getWriter());
  }

  void query(final Request baseRequest, final HttpServletResponse response) throws IOException {
    PatientSearchRequest query = null;
    final int currentQueryId = queryId.incrementAndGet();
    boolean acquired = false;
    PatientSearchResponse result = null;
    try {
      queryLimit.acquire();
      acquired = true;
      query = new Gson().fromJson(baseRequest.getReader(), PatientSearchRequest.class);
      Logging.info("ORIGINAL QUERY='" + query.getQuery() + "'");
      if ((serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) && slaveManager.containsShardSlaves()) {
        result = QueryCache.getInstance().getQueryResult(query);
        if (result == null) {
          result = searchManager.isQueryInProgress(query);
          info(QueryUtils.toJson(result));
          if (result == null) {
            if (query.isCheckStatus()) {
              result = PatientSearchResponse.createError("Could not get status of a query", 1);
            }
            else {
              result = slaveManager.querySlaves(this, query, FIVE_MINUTES);
              QueryCache.getInstance().storeQuery(query, result);
            }
          }
        }
        result.write(response.getOutputStream());
      }
      else {
        result = search(currentQueryId, query);
        result.write(response.getOutputStream());
      }
    }
    catch (final Exception e) {
      if (result != null) {
        result.setError(e.getMessage());
        result.write(response.getOutputStream());
      }
      e.printStackTrace();
    }
    finally {
      if (acquired) {
        queryLimit.release();
      }
    }
  }

  public BooleanResponse containsPatient(final ContainsPatientRequest containsQuery) throws IOException {
    final BooleanResponse resp = new BooleanResponse(database.containsInMemory(containsQuery.getPatientId()));
    if (resp.getResponse() || serverType == SERVER_TYPE.SLAVE || serverType == SERVER_TYPE.INDEPENDENT || serverType == SERVER_TYPE.WORKSHOP) {
      return resp;
    }
    else if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
      return new BooleanResponse(slaveManager.queryContainsPid(containsQuery));
    }
    return new BooleanResponse(false);
  }

  public File getROOT_FOLDER() {
    return ROOT_FOLDER;
  }

  SlaveListResponse refreshSlaveList() {
    if (slaveManager == null || !slaveManager.isImmutable()) {
      slaveManager = new SearchEngineSlaveManager(new File(ROOT_FOLDER, SLAVES_FILE));
    }
    return slaveManager.getInitResponseString();
  }

  AutosuggestResponse autosuggestSearch(final AutosuggestRequest request) {
    return autosuggest.search(request);
  }

  AutosuggestTextResponse autosuggestReplace(final AutosuggestTextRequest request) {
    return autosuggest.replace(request);
  }

  void queryPids(final Request baseRequest, final HttpServletResponse response) throws IOException {
    boolean acquired = false;
    try {
      queryLimit.acquire();
      acquired = true;
      final PidRequest req = new Gson().fromJson(baseRequest.getReader(), PidRequest.class);
      req.setReturnPids(true);
      if (serverType == SERVER_TYPE.SLAVE) {
        req.setBinary(true);
        search(req, response.getOutputStream());
      }
      else if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
        slaveManager.queryPids(response.getOutputStream(), this, req, Integer.MAX_VALUE);
      }
      else {
        search(req, response.getOutputStream());
      }
    }
    catch (JsonSyntaxException | JsonIOException | InterruptedException e) {
      e.printStackTrace();
    }
    finally {
      if (acquired) {
        queryLimit.release();
      }
    }
  }

  @Override
  public DumpResponse dump(final Transaction transaction, final DumpRequest req) {
    return dump.dump(transaction, req, indices);
  }

  public PatientSearchResponse createResponse(final int queryId) {
    return new PatientSearchResponse(queryId, indices, umls, statistics);
  }

  public PatientSearchResponse search(final PatientSearchRequest query) {
    return search(0, query, query.getPidCntLimit());
  }

  public void search(final PidRequest query, final OutputStream out) throws IOException {
    searchManager.submitSearchTask(queryId.incrementAndGet(), query, out);
  }

  public PatientSearchResponse search(final int queryId, final PatientSearchRequest request) {
    return search(queryId, request, request.getPidCntLimit());
  }

  public PatientSearchResponse search(final PatientSearchRequest query, final int returnHowManyPatientIds) {
    return search(0, query, returnHowManyPatientIds);
  }

  public PatientSearchResponse search(final int queryId, final PatientSearchRequest request, final int returnHowManyPatientIds) {
    return searchManager.submitSearchTask(serverType, queryId, request, returnHowManyPatientIds);
  }

  private void loadStatistics(final File databaseFolder, final boolean loadReduced) throws IOException {
    File statisticsFileName = new File(databaseFolder, Common.STATISTICS_FILE_NAME);
    if (loadReduced && new File(".", Common.STATISTICS_REDUCED_FILE_NAME).exists()) {
      statisticsFileName = new File(".", Common.STATISTICS_REDUCED_FILE_NAME);
    }
    debug("Loading statistics '" + statisticsFileName + "'");
    final OffHeapInput input = new OffHeapInput(statisticsFileName);
    statistics.load(input, 0);
    debug("unique ICD9 cnt=" + statistics.getIcd9KeyCnt());
    debug("unique CPT cnt=" + statistics.getCptKeyCnt());
    debug("unique RX cnt=" + statistics.getRxNormKeyCnt());
    input.close();
  }

  private void saveReducedStatistics(final File databaseFolder) throws IOException {
    final BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(databaseFolder, Common.STATISTICS_REDUCED_FILE_NAME)));
    final StreamOutput input = new StreamOutput(stream);
    statistics.save(input);
    debug("unique ICD9 cnt=" + statistics.getIcd9KeyCnt());
    debug("unique CPT cnt=" + statistics.getCptKeyCnt());
    debug("unique RX cnt=" + statistics.getRxNormKeyCnt());
    stream.close();
  }

  private void initSearchManager() {
    searchManager = new PatientSearchManager(database, indices, statistics);
    QueryCache.getInstance().refreshCacheFromDisk(searchManager);
  }

  public ClinicalSearchEngine(final SERVER_TYPE type, final File databaseFolder, final File rootFolder) throws InstantiationException, IllegalAccessException, IOException {
    info("Starting server as " + type.toString());
    this.serverType = type;
    ROOT_FOLDER = rootFolder;
    final ScriptParser scriptParser = new ScriptParser(new ScriptStorage(rootFolder));
    ScriptParser.setInstance(scriptParser);
    indices = new IndexCollection();
    loadIndices(databaseFolder);
    loadStatistics(databaseFolder, false);

    database = PersistentObjectDatabase.create(DatabaseSettings.create(PatientBuilder.class, databaseFolder));
    umls.extractCustomCodeDictionary(new File(databaseFolder, Stride6DatabaseDownload.CODE_DICTIONARY_FILE));
    autosuggest = new AutosuggestSearch(umls, statistics, indices);
    ScriptParser.getInstance().setAutosuggest(autosuggest);
    if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
      debug("Getting the slave list...");
      slaveManager = new SearchEngineSlaveManager(new File(SLAVES_FILE));
    }
    else {
      slaveManager = SearchEngineSlaveManager.createEmpty();
    }
    dump = new PatientDataDump(database, indices);
    initSearchManager();
    dictionaryExport = new DictionaryExport(indices, umls);
  }

  public ClinicalSearchEngine(final SERVER_TYPE type, final File databaseFolder, final File rootFolder, final DatabaseSettings settings) throws InstantiationException,
      IllegalAccessException, IOException {
    info("Starting server as " + type.toString());
    broadcastTarget = settings.getBroadcastTarget();
    broadcastStartTime = settings.getBroadcastDelay() + System.currentTimeMillis();
    broadcastFrequency = settings.getBroadcastFrequency();
    this.serverType = type;
    ROOT_FOLDER = rootFolder;
    final ScriptParser scriptParser = new ScriptParser(new ScriptStorage(rootFolder));
    ScriptParser.setInstance(scriptParser);
    indices = new IndexCollection();
    this.groupId = settings.getSlaveGroupId();
    loadIndices(databaseFolder);
    loadStatistics(databaseFolder, false);

    database = PersistentObjectDatabase.create(settings);
    umls.extractCustomCodeDictionary(new File(databaseFolder, Stride6DatabaseDownload.CODE_DICTIONARY_FILE));
    autosuggest = new AutosuggestSearch(umls, statistics, indices);
    ScriptParser.getInstance().setAutosuggest(autosuggest);
    if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
      debug("Getting the slave list...");
      slaveManager = new SearchEngineSlaveManager(new File(SLAVES_FILE));
    }
    else {
      slaveManager = SearchEngineSlaveManager.createEmpty();
    }
    dump = new PatientDataDump(database, indices);
    initSearchManager();
    dictionaryExport = new DictionaryExport(indices, umls);
  }

  public ClinicalSearchEngine(final SERVER_TYPE type, final File rootFolder, final DatabaseSettings settings) throws InstantiationException, IllegalAccessException, IOException,
      SQLException, InterruptedException {
    final long time = System.currentTimeMillis();
    info("Starting server as " + type.toString());
    broadcastTarget = settings.getBroadcastTarget();
    broadcastStartTime = settings.getBroadcastDelay() + time;
    broadcastFrequency = settings.getBroadcastFrequency();
    this.serverType = type;
    ROOT_FOLDER = rootFolder;
    if (serverType != SERVER_TYPE.HTML) {
      final ScriptParser scriptParser = new ScriptParser(new ScriptStorage(rootFolder));
      ScriptParser.setInstance(scriptParser);
      indices = new IndexCollection();

      this.groupId = settings.getSlaveGroupId();
      loadIndices(settings.getInputFolder());
      loadStatistics(settings.getInputFolder(), settings.loadReducedStatistics());
      if (type == SERVER_TYPE.SLAVE && settings.isRangeSpecified() && (!settings.loadReducedStatistics() || !new File(Common.STATISTICS_REDUCED_FILE_NAME).exists())) {
        statistics.keepPatients(PersistentObjectDatabase.readPidList(settings).iterator(), true);
        saveReducedStatistics(new File("."));
      }
      if (type == SERVER_TYPE.MASTER || type == SERVER_TYPE.MASTER_WORKSHOP) {
        statistics.keepPatients(PersistentObjectDatabase.readPidList(settings).iterator(), false);
      }
      info("Startup time statistics+indices = " + (System.currentTimeMillis() - time));
      database = PersistentObjectDatabase.create(settings);
      if (serverType != SERVER_TYPE.SLAVE) {
        autosuggest = loadAutosuggest(settings);
      }
      else {
        autosuggest = null;
      }
      if (serverType == SERVER_TYPE.MASTER || serverType == SERVER_TYPE.MASTER_WORKSHOP) {
        debug("Getting the slave list...");
        slaveManager = new SearchEngineSlaveManager(new File(SLAVES_FILE));
      }
      else {
        slaveManager = SearchEngineSlaveManager.createEmpty();
      }
      debug("Initializing patient dump");
      dump = new PatientDataDump(database, indices);
      debug("Initializing search manager");
      initSearchManager();
      debug("Initializing dictionary export");
      dictionaryExport = new DictionaryExport(indices, umls);
    }
    else {
      database = null;
      indices = null;
      dump = null;
      autosuggest = null;
      dictionaryExport = null;
      slaveManager = null;
      searchManager = null;
    }
    info("Startup time = " + (System.currentTimeMillis() - time));
  }

  private AutosuggestSearch loadAutosuggest(final DatabaseSettings settings) {
    final File statisticsFileName = new File(settings.getInputFolder(), Common.STATISTICS_FILE_NAME);
    final long version = statisticsFileName.lastModified();
    final File autosuggestFileName = new File(AUTOSUGGEST_FILE);
    try {
      return new AutosuggestSearch(autosuggestFileName, version, umls, statistics, indices);
    }
    catch (final Exception e) {
      exception(e);
    }
    catch (final UnsupportedClassVersionError err) {
      exception(err);
    }

    final AutosuggestSearch result = new AutosuggestSearch(umls, statistics, indices);
    ScriptParser.getInstance().setAutosuggest(autosuggest);
    try {
      result.saveToDisk(version, autosuggestFileName);
    }
    catch (final Exception e) {
      exception(e);
    }

    return result;
  }

  DictionaryResponse dictionaryRequest(final DictionaryRequest request) {
    return dictionaryExport.request(request);
  }

  void dumpPatient(final Request baseRequest, final HttpServletResponse response) throws IOException {
    dumpDistributed(new Gson().fromJson(baseRequest.getReader(), DumpRequest.class), response.getWriter());
  }

  private void loadIndices(final File databaseFolder) throws FileNotFoundException, IOException {
    final OffHeapInput stream = new OffHeapInput(new File(databaseFolder, Common.INDICES_FILE_NAME));
    indices.load(stream, 0);
    stream.close();
    final File dictFile = new File(databaseFolder, Stride6DatabaseDownload.TERM_DICTIONARY_FILE);
    if (dictFile.exists()) {
      final TextFileReader reader = FileUtils.readFileIterator(dictFile, Charset.forName("UTF-8"));
      indices.clearTidToStringMap();
      while (reader.hasNext()) {
        final String[] data = TextUtils.split(reader.next(), '\t');
        indices.addTid(Integer.parseInt(data[0]), data[1]);
      }
      reader.close();
    }
  }

  @Override
  public void close() throws IOException {
    database.close();
  }

}