package com.podalv.search.server;

import java.io.File;

/** PatientExport for test purposes. It does not have any functionality
 *
 * @author podalv
 *
 */
public class PatientExportTestInstance extends PatientExport {

  public PatientExportTestInstance(final File rootfolder) {
    super(rootfolder);
  }

  @Override
  public String writeFile(final String contents) {
    return null;
  }
}
