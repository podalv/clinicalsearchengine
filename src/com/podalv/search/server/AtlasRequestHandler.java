package com.podalv.search.server;

import static com.podalv.utils.Logging.error;
import static com.podalv.utils.Logging.info;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.google.gson.Gson;
import com.podalv.dump.DatabaseDump;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestTextRequest;
import com.podalv.search.datastructures.CheckSyntaxRequest;
import com.podalv.search.datastructures.DictionaryRequest;
import com.podalv.search.datastructures.DumpRegistryJob.REQUEST_STATUS;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.RegisterSlaveRequest;
import com.podalv.search.datastructures.RegisterSlaveResponse;
import com.podalv.search.datastructures.ScriptContentRequest;
import com.podalv.search.datastructures.ScriptContentResponse;
import com.podalv.search.datastructures.ScriptNamesResponse;
import com.podalv.search.datastructures.ServerStatusResponse;
import com.podalv.search.language.script.LanguageParser;
import com.podalv.search.language.script.ScriptParser;
import com.podalv.utils.Logging;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.file.datastructures.TextFileReader;
import com.podalv.workshop.ContainsPatientRequest;

public class AtlasRequestHandler extends AbstractHandler {

  public static final String         SEARCH_QUERY              = "/query";
  public static final String         DUMP_STATUS               = "/dump_status";
  public static final String         CONTAINS_PID_QUERY        = "/contains_patient";
  public static final String         DEMOGRAPHICS              = "/demographics";
  public static final String         AGGREGATE                 = "/aggregate";
  public static final String         PID_LIST                  = "/pids";
  public static final String         KILL_QUERY                = "/kill";
  public static final String         CLEAR_CACHE_QUERY         = "/clear_cache";
  public static final String         REFRESH_SLAVES            = "/refresh_slave_list";
  public static final String         DUMP_QUERY                = "/dump";
  public static final String         REGISTER_QUERY            = "/register";
  public static final String         INDEX_FILE_NAME           = "index.html";
  public static final String         WORKSHOP_FILE_NAME        = "workshop_index.html";
  public static final String         DICTIONARY_QUERY          = "/dictionary";
  public static final String         WORKSHOP_QUERY            = "/workshop";
  public static final String         STATISTICS_QUERY          = "/statistics";
  public static final String         SCRIPT_NAMES_QUERY        = "/script_names";
  public static final String         SCRIPT_CONTENT_QUERY      = "/script_content";
  public static final String         AUTOSUGGEST_QUERY         = "/autosuggest";
  public static final String         STATUS_QUERY              = "/status";
  public static final String         CHECK_SYNTAX              = "/check_syntax";
  public static final String         QUERY_STATUS              = "/query_status";
  public static final String         AUTOSUGGEST_REPLACE_QUERY = "/autosuggest_replace";
  private final ClinicalSearchEngine engine;
  private String                     acceptRequestsFromIp;

  public AtlasRequestHandler(final ClinicalSearchEngine engine, final String acceptRequestsFromIp) {
    this.engine = engine;
    this.acceptRequestsFromIp = acceptRequestsFromIp;
  }

  @Override
  public void handle(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    PatientExport.getInstance().purgeOldFiles();
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("UTF-8");
    response.setStatus(HttpServletResponse.SC_OK);
    baseRequest.setHandled(true);
    if (target.endsWith(REGISTER_QUERY)) {
      final RegisterSlaveRequest query = new Gson().fromJson(baseRequest.getReader(), RegisterSlaveRequest.class);
      query.setUrl(request.getRemoteHost());
      final ServerStatusResponse status = new Gson().fromJson(QueryUtils.query(query.getUrl() + STATUS_QUERY, "", 1), ServerStatusResponse.class);
      if (!status.isOk()) {
        error("Cannot register the slave. It is unreachable. Status returned '" + status.getStatus() + "'");
        QueryUtils.writeJson(new RegisterSlaveResponse(false, "Unreachable url '" + query.getUrl() + "'. Status = '" + status.getStatus()), response.getWriter());
      }
      else {
        final RegisterSlaveResponse resp = engine.registerSlave(query.getUrl(), query.getGroupId());
        QueryUtils.writeJson(resp, response.getWriter());
        info("Register slave '" + request.getRemoteHost() + ":" + query.getPort() + "'. Response: OK=" + resp.isSuccess() + ". Error message = '" + resp.getError() + "'");
      }
      return;
    }
    if (target.endsWith(DatabaseDump.DUMP_LIST)) {
      response.getWriter().print(new Gson().toJson(engine.getDumpRequest()));
      return;
    }
    else if (target.startsWith(DatabaseDump.DUMP_PULL_RESPONSE)) {
      final String token = target.substring(target.lastIndexOf('/') + 1);
      final BufferedWriter writer = new BufferedWriter(new FileWriter(token));
      final BufferedReader reader = baseRequest.getReader();
      String line;
      while ((line = reader.readLine()) != null) {
        writer.write(line + "\n");
      }
      writer.close();
      DumpRequestRegistry.getInstance().done(token);
      return;
    }
    else if (target.startsWith(DUMP_STATUS)) {
      final String token = target.substring(target.lastIndexOf('/') + 1);
      final REQUEST_STATUS status = DumpRequestRegistry.getInstance().getStatus(token);
      final PrintWriter writer = response.getWriter();
      if (status != REQUEST_STATUS.DONE) {
        writer.write("dump job '" + token + "' status = " + status);
      }
      else {
        try {
          response.setContentType("application/txt");
          response.setHeader("content-disposition", "attachment; filename=" + token);
          final TextFileReader reader = new TextFileReader(new File(token), Charset.forName("UTF-8"));
          while (reader.hasNext()) {
            writer.write(reader.next() + "\n");
          }
          reader.close();
        }
        catch (final Exception e) {
          writer.write("Error fetching results of job '" + token + "'");
        }
      }
      writer.close();
      return;
    }

    if (acceptRequestsFromIp != null && acceptRequestsFromIp.equalsIgnoreCase("true")) {
      info("Restricted requests to '" + request.getRemoteAddr() + "'");
      acceptRequestsFromIp = request.getRemoteAddr();
    }
    if (acceptRequestsFromIp != null && !acceptRequestsFromIp.equals(request.getRemoteAddr())) {
      error("Declined request from '" + request.getRemoteAddr() + "'");
      response.getWriter().write("Invalid request");
      return;
    }
    if (target.endsWith(STATUS_QUERY)) {
      response.getWriter().print(new Gson().toJson(ServerStatusResponse.createOkResponse((engine.getServerType() == SERVER_TYPE.WORKSHOP
          || engine.getServerType() == SERVER_TYPE.MASTER_WORKSHOP))));
    }
    else if (target.endsWith(STATISTICS_QUERY)) {
      QueryUtils.writeJson(engine.statistics(), response.getWriter());
    }
    else if (target.endsWith(DICTIONARY_QUERY)) {
      QueryUtils.writeJson(engine.dictionaryRequest(new Gson().fromJson(baseRequest.getReader(), DictionaryRequest.class)), response.getWriter());
    }
    else if (target.endsWith(SCRIPT_NAMES_QUERY)) {
      QueryUtils.writeJson(new ScriptNamesResponse(ScriptParser.getInstance().getScriptNames()), response.getWriter());
    }
    else if (target.endsWith(SCRIPT_CONTENT_QUERY)) {
      final CheckSyntaxRequest syntaxRequest = new Gson().fromJson(baseRequest.getReader(), CheckSyntaxRequest.class);
      QueryUtils.writeJson(LanguageParser.checkSyntax(syntaxRequest), response.getWriter());
    }
    else if (target.endsWith(SCRIPT_CONTENT_QUERY)) {
      final ScriptContentRequest contentRequest = new Gson().fromJson(baseRequest.getReader(), ScriptContentRequest.class);
      QueryUtils.writeJson(new ScriptContentResponse(ScriptParser.getInstance().getScriptContent(contentRequest.getScriptName())), response.getWriter());
    }
    else if (target.endsWith(AUTOSUGGEST_QUERY)) {
      QueryUtils.writeJson(engine.autosuggestSearch(new Gson().fromJson(baseRequest.getReader(), AutosuggestRequest.class)), response.getWriter());
    }
    else if (target.endsWith(AUTOSUGGEST_REPLACE_QUERY)) {
      QueryUtils.writeJson(engine.autosuggestReplace(new Gson().fromJson(baseRequest.getReader(), AutosuggestTextRequest.class)), response.getWriter());
    }
    else if (target.endsWith(PID_LIST)) {
      engine.queryPids(baseRequest, response);
    }
    else if (target.endsWith(REFRESH_SLAVES)) {
      QueryUtils.writeJson(engine.refreshSlaveList(), response.getWriter());
    }
    else if (target.endsWith(CONTAINS_PID_QUERY)) {
      QueryUtils.writeJson(engine.containsPatient(new Gson().fromJson(baseRequest.getReader(), ContainsPatientRequest.class)), response.getWriter());
    }
    else if (target.endsWith(KILL_QUERY)) {
      response.getWriter().println(engine.killQueries());
    }
    else if (target.endsWith(QUERY_STATUS)) {
      response.getWriter().println(engine.getQueryStatus());
    }
    else if (target.endsWith(CLEAR_CACHE_QUERY)) {
      QueryCache.getInstance().clear();
      response.getWriter().println("{\"cacheSize\":\"" + QueryCache.getInstance().size() + "\"}");
    }
    else if (target.endsWith(DUMP_QUERY)) {
      engine.dumpPatient(baseRequest, response);
    }
    else if (target.endsWith(SEARCH_QUERY)) {
      Logging.info("Query START");
      engine.query(baseRequest, response);
      Logging.info("Query DONE");
    }
    else if (target.endsWith(WORKSHOP_QUERY)) {
      response.getWriter().write(FileUtils.readFile(new File(engine.getROOT_FOLDER(), WORKSHOP_FILE_NAME), Charset.forName("UTF-8")));
    }
    else if (target.indexOf("..") == -1 && (target.endsWith(".gif") || target.endsWith(".png") || target.endsWith(".css") || target.endsWith(".html") || target.endsWith(".js")
        || target.endsWith(PatientExport.EF_SUFIX) || target.endsWith(PatientExport.FILE_SUFIX))) {
      engine.serveFile(target, response);
    }
    else if ((engine.getServerType() == SERVER_TYPE.WORKSHOP || engine.getServerType() == SERVER_TYPE.MASTER_WORKSHOP) && target.endsWith(DEMOGRAPHICS)) {
      engine.demographics(request, response);
    }
    else if ((engine.getServerType() == SERVER_TYPE.WORKSHOP || engine.getServerType() == SERVER_TYPE.MASTER_WORKSHOP) && target.endsWith(AGGREGATE)) {
      engine.aggregate(request, response);
    }
    else if (engine.getServerType() != SERVER_TYPE.SLAVE) {
      response.getWriter().write(FileUtils.readFile(new File(engine.getROOT_FOLDER(), INDEX_FILE_NAME), Charset.forName("UTF-8")));
    }
  }
}
