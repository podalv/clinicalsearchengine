package com.podalv.search.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.podalv.extractor.stride6.Common;
import com.podalv.input.StreamInput;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.utils.Logging;
import com.podalv.utils.file.FileUtils;

@SuppressWarnings({"unchecked", "rawtypes"})
public class QueryUtils {

  private static final ObjectMapper mapper = new ObjectMapper();

  static {
    final SimpleModule module = new SimpleModule();
    module.addSerializer(Double.class, new JsonSerializer<Double>() {

      @Override
      public void serialize(final Double arg0, final JsonGenerator arg1, final SerializerProvider arg2) throws IOException, JsonProcessingException {
        final double converted = (Math.round((arg0 * 10000)) / (double) 10000);
        if (Math.abs(Math.ceil(converted) - converted) < 0.00000001) {
          arg1.writeNumber((int) Math.ceil(converted));
        }
        else {
          arg1.writeNumber(arg0);
        }
      }
    });
    module.addSerializer(IntArrayList.class, new JsonSerializer<IntArrayList>() {

      @Override
      public void serialize(final IntArrayList arg0, final JsonGenerator arg1, final SerializerProvider arg2) throws IOException, JsonProcessingException {
        arg1.writeStartArray();
        for (int x = 0; x < arg0.size(); x++) {
          arg1.writeNumber(arg0.get(x));
        }
        arg1.writeEndArray();
      }
    });
    module.addSerializer(double.class, new JsonSerializer() {

      @Override
      public void serialize(final Object arg0, final JsonGenerator arg1, final SerializerProvider arg2) throws IOException, JsonProcessingException {
        final double converted = (Math.round(((double) arg0 * 10000)) / (double) 10000);
        if (Math.abs(Math.ceil(converted) - converted) < 0.00000001) {
          arg1.writeNumber((int) Math.ceil(converted));
        }
        else {
          arg1.writeNumber((double) arg0);
        }
      }
    });
    mapper.registerModule(module);
  }

  public static String query(final String slaveUrl, final String query, final int queryId, final int timeout) throws IOException {
    final StringBuilder result = new StringBuilder();
    if (slaveUrl == null) {
      return null;
    }
    else {
      final URL url = new URL(slaveUrl);
      //make connection
      final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
      urlc.setConnectTimeout(timeout);
      urlc.setReadTimeout(timeout);
      //use post mode
      urlc.setDoOutput(true);
      urlc.setRequestMethod("POST");
      urlc.setAllowUserInteraction(false);
      //send query
      urlc.setReadTimeout(0);
      final PrintStream ps = new PrintStream(urlc.getOutputStream());
      ps.print(query);
      ps.close();

      //get result
      final BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
      String l = null;
      while ((l = br.readLine()) != null) {
        result.append(l + "\n");
      }
      br.close();
    }
    return result.toString();
  }

  public static void query(final Writer writer, final String url, final String query) throws IOException {
    final URL u = new URL(url);
    //make connection
    final HttpURLConnection urlc = (HttpURLConnection) u.openConnection();
    //use post mode
    urlc.setDoOutput(true);
    urlc.setRequestMethod("POST");
    urlc.setAllowUserInteraction(false);
    //send query
    urlc.setReadTimeout(0);
    final PrintStream ps = new PrintStream(urlc.getOutputStream());
    ps.print(query);
    ps.close();

    //get result
    final BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
    String l = null;
    while ((l = br.readLine()) != null) {
      writer.write(l + "\n");
    }
    br.close();
  }

  public static void writeJson(final Object obj, final PrintWriter writer) {
    try {
      mapper.writeValue(writer, obj);
    }
    catch (final JsonGenerationException e) {
      Logging.exception(e);
    }
    catch (final JsonMappingException e) {
      Logging.exception(e);
    }
    catch (final IOException e) {
      Logging.exception(e);
    }
  }

  public static String toJson(final Object obj) {
    try {
      return mapper.writeValueAsString(obj);
    }
    catch (final JsonGenerationException e) {
      Logging.exception(e);
    }
    catch (final JsonMappingException e) {
      Logging.exception(e);
    }
    catch (final IOException e) {
      Logging.exception(e);
    }
    return null;
  }

  public static Object queryBinaryPatientSearchResponse(final String slaveUrl, final String query, final int queryId, final int timeout) throws IOException,
      ClassNotFoundException {
    if (slaveUrl == null) {
      return null;
    }
    else {
      final URL url = new URL(slaveUrl);
      //make connection
      final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
      urlc.setConnectTimeout(timeout);
      urlc.setReadTimeout(timeout);
      //use post mode
      urlc.setDoOutput(true);
      urlc.setRequestMethod("POST");
      urlc.setAllowUserInteraction(false);
      //send query
      urlc.setReadTimeout(timeout);
      final PrintStream ps = new PrintStream(urlc.getOutputStream());
      ps.print(query);
      ps.close();

      //get result
      final DataInputStream dis = new DataInputStream(urlc.getInputStream());
      final int size = dis.readInt();
      final byte[] data = new byte[size];
      //TODO Error here
      dis.readFully(data);
      dis.close();

      final PatientSearchResponse response = new PatientSearchResponse(queryId, null, null, null);
      response.load(new StreamInput(new ByteArrayInputStream(data)), 0);
      return response;
    }
  }

  public static String query(final String slaveUrl, final String query, final int queryId) throws IOException {
    return query(slaveUrl, query, queryId, 0);
  }

  public static void queryBinaryPidList(final String slaveUrl, final String query, final int queryId, final PrintWriter writer) throws IOException {
    if (slaveUrl == null) {
      return;
    }
    else {
      final URL url = new URL(slaveUrl);
      //make connection
      final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
      urlc.setConnectTimeout(0);
      urlc.setReadTimeout(0);
      //use post mode
      urlc.setDoOutput(true);
      urlc.setRequestMethod("POST");
      urlc.setAllowUserInteraction(false);
      //send query
      final PrintStream ps = new PrintStream(urlc.getOutputStream());
      ps.print(query);
      ps.close();

      //get result
      DataInputStream in = null;
      try {
        in = new DataInputStream(new BufferedInputStream(urlc.getInputStream()));
        boolean isHeader = true;
        boolean containsTimeLines = false;
        while (true) {
          int pid = in.readInt();
          if (isHeader && pid == Integer.MIN_VALUE) {
            containsTimeLines = true;
            isHeader = false;
            continue;
          }
          if (!containsTimeLines && pid < 0) {
            break;
          }
          if (containsTimeLines) {
            double startTime;
            double endTime;
            if (pid < 0) {
              pid = Math.abs(pid);
              startTime = in.readDouble();
              if (startTime < 0) {
                break;
              }
              endTime = startTime;
            }
            else {
              startTime = in.readDouble();
              endTime = in.readDouble();
            }
            synchronized (writer) {
              writer.write(pid + ";" + Common.timeInDaysToString(startTime) + ";" + Common.timeInDaysToString(endTime) + "\n");
            }
          }
          else {
            synchronized (writer) {
              writer.write(pid + "\n");
            }
          }
        }
        synchronized (writer) {
          writer.flush();
        }
      }
      finally {
        FileUtils.close(in);
      }
    }
  }

}
