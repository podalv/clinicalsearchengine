package com.podalv.search.server;

import static com.podalv.utils.Logging.error;
import static com.podalv.utils.Logging.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.MissingResourceException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.PersistentObjectDatabase;
import com.podalv.objectdb.Transaction;
import com.podalv.preprocessing.datastructures.AtomPreprocessingNode;
import com.podalv.preprocessing.datastructures.PreprocessingNode;
import com.podalv.search.datastructures.Enums.SERVER_TYPE;
import com.podalv.search.datastructures.Histogram;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.PidRequest;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.language.evaluation.AbortedResult;
import com.podalv.search.language.evaluation.BooleanResult;
import com.podalv.search.language.evaluation.EvaluationResult;
import com.podalv.search.language.evaluation.TimeIntervals;
import com.podalv.search.language.evaluation.TimePoint;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.node.CsvNode;
import com.podalv.search.language.node.DumpNode;
import com.podalv.search.language.node.RootNode;
import com.podalv.search.language.node.RootNode.ROOT_NODE_TYPE;
import com.podalv.search.language.node.RootNodeUtils;
import com.podalv.search.language.node.base.LimitNodeEvaluator;
import com.podalv.search.language.script.ScriptParser;
import com.podalv.utils.file.FileUtils;

/** Performs patient search
 *
 * @author podalv
 *
 */
public class PatientSearch {

  private final long                     time;
  private final static int               MAX_ERROR_CNT       = 100;
  public final static int                MIN_PATIENT_CNT     = 0;
  private final PersistentObjectDatabase database;
  private final IndexCollection          indices;
  private final Statistics               statistics;
  private boolean                        isKilled            = false;
  private int                            processedPatientCnt = 0;
  private int                            foundPatients       = 0;
  private int                            skippedPatients     = 0;
  private int                            errorCnt            = 0;
  private String                         query;
  private final SERVER_TYPE              type;

  public PatientSearch(final PersistentObjectDatabase database, final IndexCollection indices, final Statistics statistics) {
    this.time = System.currentTimeMillis();
    this.database = database;
    this.indices = indices;
    this.statistics = statistics;
    this.query = "NOT STARTED YET";
    this.type = SERVER_TYPE.INDEPENDENT;
  }

  public PatientSearch(final SERVER_TYPE type, final PersistentObjectDatabase database, final IndexCollection indices, final Statistics statistics) {
    this.time = System.currentTimeMillis();
    this.database = database;
    this.indices = indices;
    this.statistics = statistics;
    this.query = "NOT STARTED YET";
    this.type = type;
  }

  public int getProcessedPatientCnt() {
    return processedPatientCnt;
  }

  public String getQuery() {
    return query;
  }

  public int getFoundPatients() {
    return foundPatients;
  }

  private ArrayList<Histogram> summarizeAges(final IntArrayList ageToCount, final int cohortCnt, final int generalPatientCnt) {
    final ArrayList<Histogram> out = new ArrayList<>();
    for (int x = 0; x < ageToCount.size(); x++) {
      int total = ageToCount.get(x);
      if (x <= statistics.getAges().size() - 1) {
        total = statistics.getAges().get(x);
      }
      out.add(new Histogram(String.valueOf(x), String.valueOf(x), ageToCount.get(x), total, cohortCnt != 0 ? ageToCount.get(x) / (double) cohortCnt : 0, total
          / (double) generalPatientCnt, statistics.getPatientCnt()));
    }
    return out;
  }

  private void removeStatisticsForSmallCohorts(final PatientSearchResponse result) {
    if (result.getCohortPatientCnt() < MIN_PATIENT_CNT && (type == SERVER_TYPE.INDEPENDENT || type == SERVER_TYPE.WORKSHOP)) {
      result.clearData();
    }
  }

  public synchronized void kill() {
    isKilled = true;
  }

  public synchronized void search(final PatientSearchResponse result, final PatientSearchRequest request) {
    this.query = request.getQuery();
    result.setEncountersBuckets(request.getEncounterBuckets());
    result.setDurationsBuckets(request.getDurationBuckets());
    RootNode root = RootNode.parse(request.getQuery(), request.isReturnTimeIntervals(), request.getSearchablePids());
    try {
      result.setOriginalUnparsedQuery(ScriptParser.getOriginalUnparsedQuery(request.getQuery()));
    }
    catch (final Exception e) {
      result.setError("Invalid query syntax. It looks like there are variables declared, but nothing is queried");
      error("Invalid query syntax. It looks like there are variables declared, but nothing is queried");
      return;
    }
    final StringBuilder errorMessages = new StringBuilder();
    HashMap<Long, Long> deathToCnt = null;
    HashMap<Long, Long> censoringToCnt = null;
    if (request.isReturnSurvivalData()) {
      deathToCnt = new HashMap<>();
      censoringToCnt = new HashMap<>();
    }
    if (RootNodeUtils.nodeContainsError(root)) {
      errorMessages.append(root.toString());
      error(root.toString());
    }
    result.addWarnings(root.getWarning());
    PreprocessingNode node = null;
    try {
      node = database.optimizePidOrder(root.generatePreprocessingNode(indices, statistics));
    }
    catch (final Exception e) {
      if (e instanceof MissingResourceException) {
        if (type != SERVER_TYPE.SLAVE) {
          result.setError(e.getMessage());
        }
      }
      else {
        result.setError(e.getMessage());
      }
    }
    LimitNodeEvaluator evaluationTerminator;
    if (node instanceof AtomPreprocessingNode) {
      evaluationTerminator = root.getEvaluator(((AtomPreprocessingNode) node).getCachedPatientCnt(), ((AtomPreprocessingNode) node).getTotalPatientCnt());
    }
    else {
      evaluationTerminator = root.getEvaluator(statistics.getPatientCnt(), statistics.getPatientCnt());
    }
    result.setParsedQuery(root.toString());
    final IntArrayList ageToCount = new IntArrayList(statistics.getAges().size());
    final HashSet<String> errors = new HashSet<>();
    final boolean outputTimeLines = root.getType() == ROOT_NODE_TYPE.OUTPUT;
    final boolean csvNode = root.getType() == ROOT_NODE_TYPE.CSV;

    Transaction transaction = null;
    EventFlowPatientExport export = null;
    if (csvNode) {
      export = EventFlowDispatcher.getInstance().export();
      export.export(CsvNode.getHeader());
    }
    try {
      transaction = Transaction.create();
      if (!RootNodeUtils.nodeContainsError(root) && root.getType() == ROOT_NODE_TYPE.SQL_DUMP) {
        if (DumpRequestRegistry.getInstance() == null) {
          root = RootNode.createError("Cannot perform DUMP operation - no DatabaseDump servers were registered to this instance of ATLAS");
          errorMessages.append(root.toString());
        }
      }
      if (node != null && !RootNodeUtils.nodeContainsError(root)) {
        final PatientSearchModel model = new PatientSearchModel(indices, database.getCache(), 0);
        PatientSearchModel patient = null;
        while (node.next()) {
          if (isKilled) {
            result.addWarnings("Execution of command was interrupted by API call");
            break;
          }
          if (errorCnt >= MAX_ERROR_CNT) {
            break;
          }
          processedPatientCnt++;
          final int patientId = node.getValue();
          model.setId(patientId);
          if (database.contains(model.getId())) {
            try {
              database.read(transaction, model);
              patient = model;
              if (!model.isLoaded()) {
                continue;
              }
              final EvaluationResult evaluationResult = root.evaluate(indices, patient);
              if (evaluationResult instanceof AbortedResult) {
                skippedPatients++;
                continue;
              }
              if (evaluationResult.toBooleanResult().result()) {
                if (csvNode) {
                  final String line = root.getCsvLine("\t", indices, statistics, patient);
                  if (!line.isEmpty()) {
                    foundPatients++;
                  }
                  export.export(line);
                }
                if (request.isReturnSurvivalData()) {
                  if (indices.getDemographics().getDeathTime(patientId) > 0) {
                    final PayloadIterator i = evaluationResult.toTimeIntervals(patient).iterator(patient);
                    i.next();
                    if (indices.getDemographics().getDeathTime(patientId) >= i.getStartId()) {
                      PatientSearchManager.addValue(deathToCnt, (indices.getDemographics().getDeathTime(patientId) - i.getStartId()) / (60 * 24));
                    }
                    else {
                      PatientSearchManager.addValue(censoringToCnt, (patient.getEndTime() - i.getStartId()) / (60 * 24));
                    }
                  }
                  else {
                    final PayloadIterator i = evaluationResult.toTimeIntervals(patient).iterator(patient);
                    i.next();
                    PatientSearchManager.addValue(censoringToCnt, (patient.getEndTime() - i.getStartId()) / (60 * 24));
                  }
                }
                if (request.isPidRequest()) {
                  if (outputTimeLines) {
                    final TimeIntervals intervals = evaluationResult.toTimeIntervals(patient);
                    final PayloadIterator iterator = intervals.iterator(patient);
                    while (iterator.hasNext()) {
                      iterator.next();
                      result.addPidLine(patientId, Common.minutesToDays(iterator.getStartId()), Common.minutesToDays(iterator.getEndId()));
                    }
                  }
                  else {
                    result.addPidLine(patientId, -1, -1);
                  }
                }
                else if (request.isReturnPids() && request.getPidCntLimit() > result.getPatientIds().size()) {
                  if (outputTimeLines) {
                    final TimeIntervals intervals = evaluationResult.toTimeIntervals(patient);
                    final PayloadIterator iterator = intervals.iterator(patient);
                    while (iterator.hasNext()) {
                      iterator.next();
                      result.addPatientTime(new double[] {patientId, Common.minutesToDays(iterator.getStartId()), Common.minutesToDays(iterator.getEndId())});
                    }
                  }
                  else {
                    result.addPatient(patientId);
                  }
                }
                if (!request.isPidRequest() && foundPatients <= request.getStatisticsLimit()) {
                  int age = -1;
                  if (evaluationResult instanceof TimePoint) {
                    age = Common.minutesToYears(((TimePoint) evaluationResult).getTimePoint());
                  }
                  else if (evaluationResult instanceof TimeIntervals) {
                    final PayloadIterator i = ((TimeIntervals) evaluationResult).iterator(patient);
                    if (i.hasNext()) {
                      i.next();
                      age = Common.minutesToYears(i.getStartId());
                    }
                  }
                  else if (evaluationResult instanceof BooleanResult) {
                    age = Common.minutesToYears(patient.getStartTime());
                  }
                  if (age < 0) {
                    age = Common.minutesToYears(patient.getStartTime());
                  }
                  recordAge(ageToCount, age);
                  result.recordDemographics(patientId);
                  result.recordStatistics(patient);
                }
                if (!csvNode) {
                  foundPatients++;
                }
              }
              if (evaluationTerminator.terminate(evaluationResult.toBooleanResult().result())) {
                break;
              }
            }
            catch (final Exception e) {
              errorCnt++;
              final String errorMessage = e.getMessage();
              if (!errors.contains(errorMessage)) {
                errors.add(errorMessage);
              }
              e.printStackTrace();
              break;
            }
          }
          else {
            error("MISSING PID " + patientId);
          }
        }
        result.setExportLocation(root.getEventFlowExportLocation());
      }
      else {
        if (!result.containsErrors()) {
          error(errorMessages.toString());
          result.setError(errorMessages.toString());
        }
      }
    }
    finally {
      if (root.getType() == ROOT_NODE_TYPE.SQL_DUMP) {
        result.setExportLocation("/dump_status/" + registerDumpRequest(result, root));
      }
      if (export != null) {
        export.close();
        result.setExportLocation(export.getOutputFile());
      }
      System.out.println("Is binary request = " + request.isBinaryPidRequest());
      System.out.println("request instanceof PidRequest = " + (request instanceof PidRequest));
      if (request.isBinaryPidRequest() && request instanceof PidRequest) {
        System.out.println("Writing line for '" + query + "'");
        result.addPidLine(-1, -1d, -1d);
      }
      FileUtils.close(transaction);
    }
    generateErrorMessage(result, errors);
    result.setAges(summarizeAges(ageToCount, foundPatients, result.getGeneralPatientCnt()));
    if (request.isReturnSurvivalData()) {
      result.setDeaths(PatientSearchManager.sortMap(deathToCnt));
      result.setCensored(PatientSearchManager.sortMap(censoringToCnt));
    }
    info("Query " + result.getQueryId() + " = '" + request.getQuery() + "'");
    info("Query " + result.getQueryId() + " parsed = '" + root.toString() + "'");
    info("Query " + result.getQueryId() + " processed " + processedPatientCnt + ", skipped " + skippedPatients + ", found " + foundPatients);
    info("Query " + result.getQueryId() + " took " + (System.currentTimeMillis() - time) + "ms");
    result.setProcessedPatients(processedPatientCnt);
    result.setSkippedPatientCnt(skippedPatients);
    result.setCohortPatientCnt(foundPatients);
    for (int x = 0; x < root.getConsoleOutput().size(); x++) {
      result.addConsoleOutput(root.getConsoleOutput().get(x));
    }
    if (root.getNodeWithType(RootNodeUtils.estimateTest) != null) {
      while (node.next()) {
        processedPatientCnt++;
      }
      result.estimatePatientStatistics(processedPatientCnt);
    }
    result.close();
    root.close();
    removeStatisticsForSmallCohorts(result);
    result.setTimeTook(System.currentTimeMillis() - time);
    QueryCache.getInstance().storeQuery(request, result);
  }

  private String registerDumpRequest(final PatientSearchResponse result, final RootNode root) {
    final int[] patients = new int[result.getPatientIds().size()];
    final Iterator<double[]> i = result.getPatientIds().iterator();
    int cnt = 0;
    while (i.hasNext()) {
      patients[cnt++] = (int) i.next()[0];
    }
    return DumpRequestRegistry.getInstance().add(((DumpNode) root.getNodeWithType(RootNodeUtils.dumpTest)).getNamedQuery(), patients);
  }

  private void generateErrorMessage(final PatientSearchResponse result, final HashSet<String> errors) {
    if (errors.size() > 1) {
      result.setError(errorCnt + " errors");
    }
    else if (errors.size() > 0) {
      final Iterator<String> errorIterator = errors.iterator();
      if (errorIterator.hasNext()) {
        final StringBuilder errorString = new StringBuilder();
        while (errorIterator.hasNext()) {
          errorString.append(errorIterator.next());
        }
        result.setError(errorString.toString());
      }
      else {
        result.setError(errorCnt + " errors");
      }
    }
  }

  private void recordAge(final IntArrayList ageToCount, final int age) {
    if (age >= 0) {
      if (age > ageToCount.size() - 1) {
        for (int y = ageToCount.size(); y <= age; y++) {
          ageToCount.add(0);
        }
      }
      ageToCount.set(age, ageToCount.get(age) + 1);
    }
    else {
      error("Error patient's age is " + age);
    }
  }
}
