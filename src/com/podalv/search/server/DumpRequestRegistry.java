package com.podalv.search.server;

import static com.podalv.utils.Logging.exception;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import com.google.gson.Gson;
import com.podalv.dump.datastructures.DirectRequest;
import com.podalv.search.datastructures.DumpRegistryJob;
import com.podalv.search.datastructures.DumpRegistryJob.REQUEST_STATUS;
import com.podalv.utils.file.FileUtils;

/** A registry that holds all the DumpRequests and supplied them to the DatabaseDump service upon request
 *  DumpRequestRegistry is only activated upon the first connection from the DatabaseDump service. Prior to that it will keep throwing errors
 *
 * @author podalv
 *
 */
public class DumpRequestRegistry {

  public static final String               TEMP_FILE_NAME   = "dump_registry";
  private final ArrayList<DumpRegistryJob> requests         = new ArrayList<>();
  private final HashSet<Integer>           existingRequests = new HashSet<>();
  private final Random                     random           = new Random();
  private static DumpRequestRegistry       INSTANCE         = null;

  public static void create() {
    INSTANCE = new DumpRequestRegistry();
  }

  public static DumpRequestRegistry getInstance() {
    return INSTANCE;
  }

  private File getFile(final String id) {
    return new File(TEMP_FILE_NAME + "_" + id);
  }

  private String getNewId() {
    int newId = random.nextInt();
    if (existingRequests.contains(newId)) {
      while (!existingRequests.contains(newId)) {
        newId = random.nextInt();
      }
    }
    return Integer.toHexString(newId);
  }

  private void writeRequest(final String id, final String namedQuery, final int[] pids) {
    final File file = getFile(id);
    final DirectRequest request = new DirectRequest();
    request.setId(id);
    request.setNamedQuery(namedQuery);
    request.setPids(pids);
    try {
      FileUtils.writeFile(file, new Gson().toJson(request));
    }
    catch (final Exception e) {
      exception(e);
    }
  }

  public String add(final String namedQuery, final int[] pids) {
    synchronized (requests) {
      final String id = getNewId();
      requests.add(new DumpRegistryJob(id).setStatus(REQUEST_STATUS.PENDING));
      writeRequest(id, namedQuery, pids);
      return id;
    }
  }

  public void done(final String id) {
    synchronized (requests) {
      if (requests.size() != 0) {
        for (int x = 0; x < requests.size(); x++) {
          if (requests.get(x).getId().equals(id)) {
            requests.get(x).setStatus(REQUEST_STATUS.DONE);
          }
        }
      }
    }
  }

  public REQUEST_STATUS getStatus(final String id) {
    synchronized (requests) {
      for (int x = 0; x < requests.size(); x++) {
        if (requests.get(x).getId().equals(id)) {
          return requests.get(x).getStatus();
        }
      }
    }
    return REQUEST_STATUS.UNKNOWN;
  }

  public DirectRequest process() {
    DirectRequest result = new DirectRequest();
    result.setNamedQuery(null);
    synchronized (requests) {
      if (requests.size() != 0) {
        for (int x = 0; x < requests.size(); x++) {
          if (requests.get(x).getStatus() == REQUEST_STATUS.PENDING) {
            final String id = requests.get(x).getId();
            BufferedReader r = null;
            try {
              r = new BufferedReader(new FileReader(getFile(id)));
              result = new Gson().fromJson(r, DirectRequest.class);
            }
            catch (final Exception e) {
              exception(e);
            }
            finally {
              FileUtils.close(r);
            }
            getFile(id).delete();
            requests.get(x).setStatus(REQUEST_STATUS.PROCESSING);
          }
        }
      }
    }
    return result;
  }

}