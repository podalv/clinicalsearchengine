package com.podalv.search.server;

import static com.podalv.utils.Logging.exception;

import java.util.LinkedList;
import java.util.concurrent.Future;

import com.podalv.search.datastructures.PatientSearchResponse;

/** Contains all the collated responses from all the shards
 *
 * @author podalv
 *
 */
public class ShardResponse {

  private boolean                                 interrupted = false;
  private boolean                                 error       = false;
  private final LinkedList<PatientSearchResponse> responses   = new LinkedList<PatientSearchResponse>();

  public static ShardResponse createInterrupted(final LinkedList<Future<PatientSearchResponse>> results) {
    return init(results, true);
  }

  public void addResponses(final PatientSearchResponse src) {
    for (final PatientSearchResponse resp : responses) {
      src.add(resp);
    }
  }

  private static ShardResponse init(final LinkedList<Future<PatientSearchResponse>> results, final boolean interrupted) {
    final ShardResponse result = new ShardResponse(interrupted);
    for (final Future<PatientSearchResponse> resp : results) {
      if (resp.isDone()) {
        try {
          if (resp.get() != null) {
            result.add(resp.get());
          }
          else {
            result.setError(true);
          }
        }
        catch (final Exception e) {
          result.setError(true);
          exception(e);
        }
      }
    }
    return result;
  }

  public static ShardResponse createEmpty() {
    return new ShardResponse(false);
  }

  public static ShardResponse create(final LinkedList<Future<PatientSearchResponse>> results) {
    return init(results, false);
  }

  public void setError(final boolean error) {
    this.error = error;
  }

  private void add(final PatientSearchResponse response) {
    responses.add(response);
  }

  private ShardResponse(final boolean interrupted) {
    this.interrupted = interrupted;
  }

  public boolean isInterrupted() {
    return this.interrupted;
  }

  public boolean isError() {
    return this.error;
  }

}
