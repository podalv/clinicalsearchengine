package com.podalv.search.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.extractor.stride6.Common;

/** Extracts variables from term_to_concept_map database
 *
 * @author podalv
 *
 */
public class TermToConceptMapExtractor {

  public static final String       QUERY           = "SELECT term_to_concept_maps.mapid, term_to_concept_maps.mapname, term_to_concept_maps.querytext, term_to_concept_values.text FROM term_concept_map.term_to_concept_maps JOIN term_concept_map.term_to_concept_values on (term_to_concept_maps.mapid = term_to_concept_values.mapid) WHERE term_to_concept_maps.experimental = \"N\" order by term_to_concept_maps.mapid";
  public static final String       VARIABLE_PREFIX = "ttcm.";

  private final ConnectionSettings dbConnection;
  private int                      undefinedCnt    = 1;

  public TermToConceptMapExtractor(final ConnectionSettings dbConnection) {
    this.dbConnection = dbConnection;
  }

  private String getName(final int mapId, final String mapName, final String queryName, final HashMap<Integer, String> mapIdToName) {
    if (mapIdToName.containsKey(mapId)) {
      return mapIdToName.get(mapId);
    }
    final StringBuilder result = new StringBuilder();
    if ((queryName == null || queryName.trim().isEmpty()) && ((mapName == null || mapName.trim().isEmpty()))) {
      mapIdToName.put(mapId, VARIABLE_PREFIX + "undefined_" + (undefinedCnt++));
      return VARIABLE_PREFIX + "undefined_" + (undefinedCnt++);
    }
    if (queryName != null && !queryName.trim().isEmpty()) {
      result.append(VARIABLE_PREFIX + queryName.toLowerCase().trim());
    }
    if (mapName != null && !mapName.trim().isEmpty() && (queryName == null || !queryName.equalsIgnoreCase(mapName))) {
      if (result.length() != 0) {
        result.append(".");
        result.append(mapName);
      }
      else {
        result.append(VARIABLE_PREFIX + mapName);
      }
    }
    mapIdToName.put(mapId, result.toString().toLowerCase().replace(' ', '_'));
    return result.toString().toLowerCase().replace(' ', '_');
  }

  private String generateVariableContent(final HashSet<String> texts) {
    final StringBuilder result = new StringBuilder();
    if (texts.size() == 1) {
      return "TEXT=\"" + Common.filterString(texts.iterator().next()) + "\"";
    }
    else {
      final Iterator<String> i = texts.iterator();
      result.append("UNION(");
      while (i.hasNext()) {
        result.append("TEXT=\"" + Common.filterString(i.next()) + "\"");
        if (i.hasNext()) {
          result.append(", ");
        }
      }
      result.append(")");
    }
    return result.toString();
  }

  public HashMap<String, String> getVariables() throws SQLException {
    return getVariables(null);
  }

  public HashMap<String, String> getVariables(final Set<Object> existingTids) throws SQLException {
    final HashMap<String, String> result = new HashMap<String, String>();
    final HashMap<String, HashSet<String>> nameToStrings = new HashMap<String, HashSet<String>>();
    final ResultSet set = Database.create(dbConnection).query(QUERY);
    final HashMap<Integer, String> mapIdToName = new HashMap<Integer, String>();
    while (set.next()) {
      final String tidString = set.getString(4).toLowerCase();
      if (existingTids == null || existingTids.contains(tidString)) {
        final String variableName = getName(set.getInt(1), set.getString(2), set.getString(3), mapIdToName);
        HashSet<String> s = nameToStrings.get(variableName);
        if (s == null) {
          s = new HashSet<String>();
          nameToStrings.put(variableName, s);
        }
        s.add(tidString);
      }
    }
    set.close();
    final Iterator<Entry<String, HashSet<String>>> iterator = nameToStrings.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, HashSet<String>> entry = iterator.next();
      result.put(entry.getKey(), generateVariableContent(entry.getValue()));
    }
    return result;
  }
}