package com.podalv.search.server;

import java.util.ArrayList;
import java.util.HashMap;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.PersistentObjectDatabase;
import com.podalv.objectdb.Transaction;
import com.podalv.search.datastructures.CsvWriter;
import com.podalv.search.datastructures.CsvWriterMapExtractor;
import com.podalv.search.datastructures.Patient;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.iterators.NotesIterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.search.language.evaluation.iterators.RawPayloadIterator;
import com.podalv.search.language.node.BeforeNode;
import com.podalv.search.language.node.RootNode;
import com.podalv.search.language.node.RootNodeUtils;
import com.podalv.search.language.script.LanguageParser;
import com.podalv.workshop.datastructures.DumpRequest;
import com.podalv.workshop.datastructures.DumpResponse;

/** Outputs data from PatientSearchModel (list of ICD9, etc.)
 *
 * @author podalv
 *
 */
public class PatientDataDump {

  public static String                   ICD9_PRIMARY_STRING = "PRIMARY";
  public static String                   ICD9_OHER_STRING    = "OTHER";
  private final PersistentObjectDatabase db;
  private final IndexCollection          indices;

  public PatientDataDump(final PersistentObjectDatabase db, final IndexCollection indices) {
    this.db = db;
    this.indices = indices;
  }

  public DumpResponse dump(final Transaction transaction, final DumpRequest req, final IndexCollection indices) {
    final DumpResponse response = new DumpResponse(req.getPatientId());
    response.setSelectionQuery(req.getSelectionQuery());
    response.setContainsEnd(req.isContainsEnd());
    response.setContainsStart(req.isContainsStart());
    int[] timeRanges = null;
    RootNode node = null;
    if (req.getSelectionQuery() != null && !req.getSelectionQuery().isEmpty()) {
      node = LanguageParser.parse(req.getSelectionQuery());
      if (RootNodeUtils.nodeContainsError(node)) {
        return DumpResponse.createError("Invalid selection query");
      }
    }
    try {
      if (!db.contains(req.getPatientId())) {
        return DumpResponse.createError("Patient not found");
      }
      final PatientSearchModel model = new PatientSearchModel(indices, this.db.getCache(), req.getPatientId());
      db.read(transaction, model);
      if (node != null) {
        final PayloadIterator i = node.evaluate(indices, model).toTimeIntervals(model).iterator(model);
        final IntArrayList startEnds = new IntArrayList();
        while (i.hasNext()) {
          i.next();
          startEnds.add(i.getStartId());
          startEnds.add(i.getEndId());
        }
        if (startEnds.size() != 0) {
          timeRanges = startEnds.toArray();
        }
        else {
          timeRanges = new int[0];
        }
      }
      response.setGender(indices.getDemographics().getGender().getString(indices.getDemographics().getGender().getValue(req.getPatientId())));
      response.setRace(indices.getDemographics().getRace().getString(indices.getDemographics().getRace().getValue(req.getPatientId())));
      response.setEthnicity(indices.getDemographics().getEthnicity().getString(indices.getDemographics().getEthnicity().getValue(req.getPatientId())));
      response.setRecordStart(model.getStartTime());
      response.setRecordEnd(model.getEndTime());
      response.setDeath(indices.getDemographics().getDeathTime(req.getPatientId()));
      response.setYearRanges(model.getAllYearData());
      if (timeRanges == null || timeRanges.length != 0) {
        if (req.isIcd9()) {
          response.setIcd9(extractIcd9(model, timeRanges, req));
        }
        if (req.isCpt()) {
          response.setCpt(extractCpt(model, timeRanges, req));
        }
        if (req.isRx()) {
          response.setRx(extractRx(model, timeRanges, req));
        }
        if (req.isSnomed()) {
          response.setSnomed(extractSnomed(model, timeRanges, req));
        }
        if (req.isNoteTypes()) {
          response.setNoteTypes(extractNoteTypes(model, timeRanges, req));
        }
        if (req.isNotes()) {
          response.setPositiveTerms(extractPositiveNotes(model, timeRanges, req));
          response.setNegatedTerms(extractNegatedNotes(model, timeRanges, req));
          response.setFhTerms(extractFhNotes(model, timeRanges, req));
        }
        if (req.isVisitTypes()) {
          response.setVisitTypes(extractVisitTypes(model, timeRanges, req));
        }

        if (req.isEncounterDays()) {
          response.setEncounterDays(extractEncounterDays(model, timeRanges, req));
        }

        if (req.isAgeRanges()) {
          response.setAgeRanges(extractAgeRanges(model, timeRanges, req));
        }
        if (req.isLabs()) {
          response.setLabs(extractLabs(model, timeRanges, req));
          final CsvWriterMapExtractor extractor = new CsvWriterMapExtractor();
          extractLabValues(extractor, model, timeRanges, req);
          response.setLabsRaw(extractor.result());
        }
        if (req.isAtc()) {
          response.setAtc(extractAtc(model));
        }
        if (req.isVitals()) {
          final CsvWriterMapExtractor extractor = new CsvWriterMapExtractor();
          extractVitals(extractor, model, timeRanges, req);
          response.setVitals(extractor.result());
        }
        if (req.isIcd10()) {
          response.setIcd10(extractIcd10(model, timeRanges, req));
        }
        if (req.isDepartments()) {
          response.setDepartments(extractDepartments(model, timeRanges, req));
        }
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
      response.setError(e.getMessage());
    }
    return response;
  }

  public static boolean isInTimeRange(final int start, final int end, final int[] timeRanges, final DumpRequest req) {
    return isInTimeRange(start, end, timeRanges, req.isContainsStart(), req.isContainsEnd());
  }

  public static boolean isInTimeRange(final int start, final int end, final int[] timeRanges, final boolean containsStart, final boolean containsEnd) {
    if (timeRanges != null) {
      for (int x = 0; x < timeRanges.length; x += 2) {
        if (!containsEnd && !containsStart) {
          if (BeforeNode.intersect(start, end, timeRanges[x], timeRanges[x + 1])) {
            return true;
          }
        }
        else if (containsStart && containsEnd) {
          if (start >= timeRanges[x] && start <= timeRanges[x + 1] && end >= timeRanges[x] && end <= timeRanges[x + 1]) {
            return true;
          }
        }
        else if (containsStart) {
          if (start >= timeRanges[x] && start <= timeRanges[x + 1]) {
            return true;
          }
        }
        else if (containsEnd) {
          if (end >= timeRanges[x] && end <= timeRanges[x + 1]) {
            return true;
          }
        }
      }
      return false;
    }
    else {
      return true;
    }
  }

  private HashMap<String, ArrayList<String>> extractIcd9(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final IntIterator icd9Codes = model.getUniqueIcd9Codes().iterator();
    while (icd9Codes.hasNext()) {
      final int codeId = icd9Codes.next();
      String code = indices.getIcd9(codeId);
      if (code == null || code.isEmpty()) {
        code = "*MISSING CODE*" + codeId;
      }
      final IntArrayList list = model.getIcd9(codeId);
      final ArrayList<String> data = new ArrayList<>();
      for (int x = 0; x < list.size(); x++) {
        final IntArrayList l = model.getPayload(list.get(x));
        if (!PayloadPointers.containsInvalidEvents(l)) {
          for (int y = 0; y < l.size(); y += 3) {
            if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
              data.add(l.get(y) + "");
              data.add(l.get(y + 1) + "");
              data.add(PatientBuilder.isPrimaryIcdCode(l.get(y + 2)) ? ICD9_PRIMARY_STRING : ICD9_OHER_STRING);
            }
          }
        }
        else {
          data.add(Integer.MIN_VALUE + "");
          data.add(Integer.MIN_VALUE + "");
          data.add("N/A");
        }
      }
      if (data.size() > 0) {
        result.put(code, data);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<String>> extractIcd10(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final IntIterator icd10Codes = model.getUniqueIcd10Codes().iterator();
    while (icd10Codes.hasNext()) {
      final int codeId = icd10Codes.next();
      String code = indices.getIcd10(codeId);
      if (code == null || code.isEmpty()) {
        code = "*MISSING CODE*" + codeId;
      }
      final IntArrayList list = model.getIcd10(codeId);
      final ArrayList<String> data = new ArrayList<>();
      for (int x = 0; x < list.size(); x++) {
        final IntArrayList l = model.getPayload(list.get(x));
        if (!PayloadPointers.containsInvalidEvents(l)) {
          for (int y = 0; y < l.size(); y += 3) {
            if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
              data.add(l.get(y) + "");
              data.add(l.get(y + 1) + "");
              data.add(PatientBuilder.isPrimaryIcdCode(l.get(y + 2)) ? ICD9_PRIMARY_STRING : ICD9_OHER_STRING);
            }
          }
        }
        else {
          data.add(Integer.MIN_VALUE + "");
          data.add(Integer.MIN_VALUE + "");
          data.add("N/A");
        }
      }
      if (data.size() > 0) {
        result.put(code, data);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private void extractVitals(final CsvWriter writer, final Patient model, final int[] timeRanges, final DumpRequest req) {
    model.getVitals().extract(writer, indices, timeRanges, req.isContainsStart(), req.isContainsEnd());
  }

  private void extractLabValues(final CsvWriter writer, final Patient model, final int[] timeRanges, final DumpRequest req) {
    model.getLabValues().extract(writer, indices, timeRanges, req.isContainsStart(), req.isContainsEnd());
  }

  private HashMap<String, ArrayList<String>> extractLabs(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final int[] labIds = model.getUniqueLabIds();
    for (final int labId : labIds) {
      final ArrayList<String> map = new ArrayList<>();
      final IntArrayList rawData = model.getLabs().getRawData(labId);
      int pos = 0;
      if (rawData.size() != 0) {
        while (true) {
          final String labType = indices.getLabValueText(rawData.get(pos++));
          final int length = rawData.get(pos++);
          for (int x = 0; x < length; x++) {
            final int time = rawData.get(pos++);
            if (time == Integer.MIN_VALUE) {
              map.add(time + "");
              map.add("N/A");
            }
            else {
              if (isInTimeRange(time, time, timeRanges, req)) {
                map.add(time + "");
                map.add(labType);
              }
            }
          }
          if (pos >= rawData.size() - 1) {
            break;
          }
        }
      }
      if (map.size() != 0) {
        result.put(indices.getLabName(labId), map);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<Integer>> extractAtc(final Patient model) {
    final HashMap<String, ArrayList<Integer>> result = new HashMap<>();
    final String[] atcNames = indices.getAtcNames();
    for (final String atcName : atcNames) {
      final int atcId = indices.getAtcCode(atcName);
      final IntArrayList list = model.getRxNormCodesFromAtc(atcId);
      if (list != null && list.size() != 0) {
        final ArrayList<Integer> l = new ArrayList<>();
        for (int x = 0; x < list.size(); x++) {
          l.add(list.get(x));
        }
        result.put(atcName, l);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<Integer>> extractVisitTypes(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<Integer>> result = new HashMap<>();
    final String[] types = indices.getVisitTypes();
    for (final String type : types) {
      final int codeId = indices.getVisitType(type);
      final IntArrayList list = model.getVisitTypes(codeId);
      if (list != null) {
        final ArrayList<Integer> data = new ArrayList<>();
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList l = model.getPayload(list.get(x));
          if (PayloadPointers.containsInvalidEvents(l)) {
            data.add(Integer.MIN_VALUE);
            data.add(Integer.MIN_VALUE);
          }
          else {
            for (int y = 0; y < l.size(); y += 2) {
              if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
                data.add(l.get(y));
                data.add(l.get(y + 1));
              }
            }
          }
        }
        if (data.size() > 0) {
          result.put(type, data);
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<Integer>> extractDepartments(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<Integer>> result = new HashMap<>();
    final IntArrayList departmentCodes = model.getUniqueDepartmentCodes();
    if (departmentCodes != null) {
      for (int x = 0; x < departmentCodes.size(); x++) {
        final int departmentCode = departmentCodes.get(x);
        final String departmentName = indices.getDepartment(departmentCode);
        final IntArrayList list = model.getDepartment(departmentCode);
        if (list != null) {
          final ArrayList<Integer> data = new ArrayList<>();
          for (int z = 0; z < list.size(); z++) {
            final IntArrayList l = model.getPayload(list.get(z));
            if (!PayloadPointers.containsInvalidEvents(l)) {
              if (l.size() == 1) {
                if (isInTimeRange(l.get(0), l.get(0), timeRanges, req)) {
                  data.add(l.get(0));
                  data.add(l.get(0));
                }
              }
              else {
                for (int y = 0; y < l.size(); y += 2) {
                  if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
                    data.add(l.get(y));
                    data.add(l.get(y + 1));
                  }
                }
              }
            }
            else {
              data.add(Integer.MIN_VALUE);
              data.add(Integer.MIN_VALUE);
            }
            if (data.size() > 0) {
              result.put(departmentName, data);
            }
          }
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

  private ArrayList<Integer> extractEncounterDays(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final ArrayList<Integer> result = new ArrayList<>();
    final IntArrayList list = model.getEncounterDays();
    if (list != null) {
      for (int x = 0; x < list.size(); x++) {
        if (isInTimeRange(list.get(x), list.get(x) + Common.daysToTime(1), timeRanges, req)) {
          result.add(list.get(x));
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

  private ArrayList<Integer> extractAgeRanges(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final ArrayList<Integer> result = new ArrayList<>();
    final IntArrayList list = model.getAgeRanges();
    if (list != null) {
      for (int x = 0; x < list.size(); x++) {
        if (isInTimeRange(list.get(x), list.get(x), timeRanges, req)) {
          result.add(list.get(x));
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<Integer>> extractCpt(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<Integer>> result = new HashMap<>();
    final IntIterator cptCodes = model.getUniqueCptCodes().iterator();
    while (cptCodes.hasNext()) {
      final int codeId = cptCodes.next();
      String code = indices.getCpt(codeId);
      if (code == null || code.isEmpty()) {
        code = "*MISSING CODE*" + codeId;
      }
      final IntArrayList list = model.getCpt(codeId);
      final ArrayList<Integer> data = new ArrayList<>();
      for (int x = 0; x < list.size(); x++) {
        final IntArrayList l = model.getPayload(list.get(x));
        if (!PayloadPointers.containsInvalidEvents(l)) {
          for (int y = 0; y < l.size(); y += 2) {
            if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
              data.add(l.get(y));
              data.add(l.get(y + 1));
            }
          }
        }
        else {
          data.add(Integer.MIN_VALUE);
          data.add(Integer.MIN_VALUE);
        }
      }
      if (data.size() != 0) {
        result.put(code, data);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<String>> extractRx(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final IntIterator rxCodes = model.getUniqueRxNormCodes().iterator();
    while (rxCodes.hasNext()) {
      final int codeId = rxCodes.next();
      final IntArrayList list = model.getRxNorm(codeId);
      final ArrayList<String> data = new ArrayList<>();
      for (int x = 0; x < list.size(); x++) {
        final IntArrayList l = model.getPayload(list.get(x));
        if (PayloadPointers.containsInvalidEvents(l)) {
          data.add(Integer.MIN_VALUE + "");
          data.add(Integer.MIN_VALUE + "");
          data.add("N/A");
          data.add("N/A");
        }
        else {
          for (int y = 0; y < l.size(); y += 4) {
            if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
              data.add(l.get(y) + "");
              data.add(l.get(y + 1) + "");
              data.add(indices.getDrugRouteString(l.get(y + 2)));
              data.add(indices.getDrugStatusString(l.get(y + 3)));
            }
          }
        }
      }
      if (data.size() != 0) {
        result.put(codeId + "", data);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<Integer>> extractSnomed(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<Integer>> result = new HashMap<>();
    final IntIterator snomedCodes = model.getUniqueSnomedCodes().iterator();
    while (snomedCodes.hasNext()) {
      final int codeId = snomedCodes.next();
      String code = indices.getSnomedText(codeId);
      if (code == null) {
        code = indices.getSnomedCode(codeId);
      }
      final IntArrayList list = model.getSnomed(codeId);
      final ArrayList<Integer> data = new ArrayList<>();
      for (int x = 0; x < list.size(); x++) {
        final IntArrayList l = model.getPayload(list.get(x));
        if (PayloadPointers.containsInvalidEvents(l)) {
          data.add(Integer.MIN_VALUE);
          data.add(Integer.MIN_VALUE);
        }
        else {
          for (int y = 0; y < l.size(); y += 2) {
            if (isInTimeRange(l.get(y), l.get(y + 1), timeRanges, req)) {
              data.add(l.get(y));
              data.add(l.get(y + 1));
            }
          }
        }
      }
      if (data.size() != 0) {
        result.put(code, data);
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<String>> extractPositiveNotes(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final int[] keys = model.getUniquePositiveTids();
    for (final int tid : keys) {
      final IntArrayList list = model.getPositiveTerms(tid);
      if (list != null) {
        String text = indices.getStringFromTid(tid);
        if (text == null || text.isEmpty()) {
          text = "*TID*" + tid;
        }
        final ArrayList<String> data = new ArrayList<>();
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList l = model.getPayload(list.get(x));
          if (PayloadPointers.containsInvalidEvents(l)) {
            data.add(Integer.MIN_VALUE + "");
            data.add(Integer.MIN_VALUE + "");
            data.add("N/A");
          }
          else {
            for (int y = 0; y < l.size(); y += 3) {
              if (isInTimeRange(l.get(y), l.get(y), timeRanges, req)) {
                data.add(l.get(y) + "");
                data.add(l.get(y + 1) + "");
                data.add(indices.getNoteTypeText(l.get(y + 2)));
              }
            }
          }
        }
        if (data.size() > 0) {
          result.put(text, data);
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<Integer>> extractNoteTypes(final PatientSearchModel model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<Integer>> result = new HashMap<>();
    final NotesIterator iterator = new NotesIterator(new RawPayloadIterator(model, model.getUniqueNotePayloadIds()));
    while (iterator.hasNext()) {
      iterator.next();
      if (isInTimeRange(iterator.getStartId(), iterator.getEndId(), timeRanges, req)) {
        final String noteTypeText = indices.getNoteTypeText(iterator.getNoteTypeId());
        ArrayList<Integer> data = result.get(noteTypeText);
        if (data == null) {
          data = new ArrayList<>();
          result.put(noteTypeText, data);
        }
        data.add(iterator.getStartId());
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<String>> extractNegatedNotes(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final int[] keys = model.getUniqueNegatedTids();
    for (final int tid : keys) {
      final IntArrayList list = model.getNegatedTerms(tid);
      if (list != null) {
        String text = indices.getStringFromTid(tid);
        if (text == null || text.isEmpty()) {
          text = "*TID*" + tid;
        }
        final ArrayList<String> data = new ArrayList<>();
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList l = model.getPayload(list.get(x));
          if (PayloadPointers.containsInvalidEvents(l)) {
            data.add(Integer.MIN_VALUE + "");
            data.add(Integer.MIN_VALUE + "");
            data.add("N/A");
          }
          else {
            for (int y = 0; y < l.size(); y += 3) {
              if (isInTimeRange(l.get(y), l.get(y), timeRanges, req)) {
                data.add(l.get(y) + "");
                data.add(l.get(y + 1) + "");
                data.add(indices.getNoteTypeText(l.get(y + 2)));
              }
            }
          }
        }
        if (data.size() > 0) {
          result.put(text, data);
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

  private HashMap<String, ArrayList<String>> extractFhNotes(final Patient model, final int[] timeRanges, final DumpRequest req) {
    final HashMap<String, ArrayList<String>> result = new HashMap<>();
    final int[] keys = model.getUniqueFamilyHistoryTids();
    for (final int tid : keys) {
      final IntArrayList list = model.getFamilyHistoryTerms(tid);
      if (list != null) {
        String text = indices.getStringFromTid(tid);
        if (text == null || text.isEmpty()) {
          text = "*TID*" + tid;
        }
        final ArrayList<String> data = new ArrayList<>();
        for (int x = 0; x < list.size(); x++) {
          final IntArrayList l = model.getPayload(list.get(x));
          if (!PayloadPointers.containsInvalidEvents(l)) {
            for (int y = 0; y < l.size(); y += 3) {
              if (isInTimeRange(l.get(y), l.get(y), timeRanges, req)) {
                data.add(l.get(y) + "");
                data.add(l.get(y + 1) + "");
                data.add(indices.getNoteTypeText(l.get(y + 2)));
              }
            }
          }
          else {
            data.add(Integer.MIN_VALUE + "");
            data.add(Integer.MIN_VALUE + "");
            data.add("N/A");
          }
          if (data.size() > 0) {
            result.put(text, data);
          }
        }
      }
    }
    return result.size() == 0 ? null : result;
  }

}