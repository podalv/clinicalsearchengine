package com.podalv.search.server;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.podalv.objectdb.PersistentObjectDatabase;
import com.podalv.objectdb.datastructures.DatabaseSettings;
import com.podalv.search.datastructures.PatientBuilder;

/** Takes PersistantObjectDatabase and generates offheap shards
 *
 * @author podalv
 *
 */
public class OffHeapShardGenerator {

  public static void generate(final File folder, final int threadCnt, final boolean deleteOriginals) throws InstantiationException, IllegalAccessException, IOException,
      InterruptedException {
    System.out.println("DELETE originals = " + deleteOriginals);
    final PersistentObjectDatabase db = PersistentObjectDatabase.createWithoutLoadingShards(DatabaseSettings.create(PatientBuilder.class, folder).clearDiskCache(true));
    db.clearOffHeapdiskCache();
    db.readIndex();
    final ExecutorService executor = Executors.newFixedThreadPool(threadCnt);
    for (int x = 0; x < db.getShardCount(); x++) {
      final int fX = x;
      executor.submit(new Runnable() {

        @Override
        public void run() {
          try {
            db.readShard(fX).free();
            System.out.println("Generated shard " + fX);
            if (deleteOriginals && new File(folder, fX + "").delete()) {
              System.out.println("Deleted shard " + fX);
            }
            else if (deleteOriginals) {
              System.out.println("Failed to delete shard " + fX);
            }
          }
          catch (final Exception e) {
            e.printStackTrace();
          }
        }
      });

    }

    executor.shutdown();

    executor.awaitTermination(1000, TimeUnit.DAYS);
    db.saveObjectIdToPositionInOffHeapShard();
  }

  private OffHeapShardGenerator() {
    // static access only
  }

  public static void main(final String[] args) throws NumberFormatException, InstantiationException, IllegalAccessException, IOException, InterruptedException {
    System.out.println("Usage: folder threadCnt DELETE");
    System.out.println("If DELETE, after generating a shard, deletes the original shard file");
    boolean deleteOriginals = false;
    if (args.length > 2 && args[2].equalsIgnoreCase("DELETE")) {
      deleteOriginals = true;
    }
    generate(new File(args[0]), Integer.parseInt(args[1]), deleteOriginals);
  }
}
