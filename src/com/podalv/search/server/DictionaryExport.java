package com.podalv.search.server;

import java.util.HashMap;

import com.podalv.search.datastructures.DictionaryRequest;
import com.podalv.search.datastructures.DictionaryResponse;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.UmlsDictionary;

/** Responds to the /dictionary API call
 *
 * @author podalv
 *
 */
public class DictionaryExport {

  private final IndexCollection indices;
  private final UmlsDictionary  umls;

  public DictionaryExport(final IndexCollection indices, final UmlsDictionary umls) {
    this.indices = indices;
    this.umls = umls;
  }

  private HashMap<String, String> getIcd9(final String[] codes) {
    final HashMap<String, String> result = new HashMap<>();
    if (codes != null) {
      for (final String code : codes) {
        result.put(code, umls.getIcd9Text(code));
      }
    }
    return result;
  }

  private HashMap<String, String> getIcd10(final String[] codes) {
    final HashMap<String, String> result = new HashMap<>();
    if (codes != null) {
      for (final String code : codes) {
        result.put(code, umls.getIcd10Text(code));
      }
    }
    return result;
  }

  private HashMap<String, String> getCpt(final String[] codes) {
    final HashMap<String, String> result = new HashMap<>();
    if (codes != null) {
      for (final String code : codes) {
        result.put(code, umls.getCptText(code));
      }
    }
    return result;
  }

  private HashMap<String, String> getAtc(final String[] codes) {
    final HashMap<String, String> result = new HashMap<>();
    if (codes != null) {
      for (final String code : codes) {
        result.put(code, umls.getAtcText(code));
      }
    }
    return result;
  }

  private HashMap<String, String> getLabs(final String[] codes) {
    final HashMap<String, String> result = new HashMap<>();
    if (codes != null) {
      for (final String code : codes) {
        result.put(code, indices.getLabCommonName(indices.getLabsCode(code)));
      }
    }
    return result;
  }

  private HashMap<String, String> getRxNorm(final String[] codes) {
    final HashMap<String, String> result = new HashMap<>();
    if (codes != null) {
      for (final String code : codes) {
        try {
          result.put(code, umls.getRxNormText(Integer.parseInt(code)));
        }
        catch (final Exception e) {
          e.printStackTrace();
        }
      }
    }
    return result;
  }

  public DictionaryResponse request(final DictionaryRequest request) {
    final DictionaryResponse response = new DictionaryResponse();
    response.setIcd9(getIcd9(request.getIcd9()));
    response.setIcd10(getIcd10(request.getIcd10()));
    response.setCpt(getCpt(request.getCpt()));
    response.setAtc(getAtc(request.getAtc()));
    response.setLabs(getLabs(request.getLabs()));
    response.setRxNorm(getRxNorm(request.getRxNorm()));
    return response;
  }
}
