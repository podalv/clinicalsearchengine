package com.podalv.search.server;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.PatientSearchModel;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.language.evaluation.PayloadPointers.TYPE;
import com.podalv.search.language.evaluation.iterators.Icd9Iterator;
import com.podalv.search.language.evaluation.iterators.PayloadIterator;
import com.podalv.utils.file.FileUtils;

public class EventFlowPatientExport implements Closeable {

  public static final int      PROGRESS_NOT_STARTED = 0;
  public static final int      PROGRESS_ERROR       = -1;
  private final BufferedWriter data;
  private int                  processedPatients    = 0;
  private int                  errorCnt             = 0;
  private final String         outputData;

  public EventFlowPatientExport(final File outputData) throws IOException {
    this.outputData = PatientExport.exportFolder + File.separator + outputData.getName();
    data = new BufferedWriter(new FileWriter(outputData));
  }

  public String getOutputFile() {
    return outputData;
  }

  public int getProgress() {
    return processedPatients;
  }

  private static String getDate(final long value) {
    final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    df.setTimeZone(TimeZone.getTimeZone("UTC"));
    df.setLenient(false);
    final Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
    c.setTime(new Date(value * 60000));
    return df.format(c.getTime());
  }

  private void saveData(final IndexCollection context, final PatientSearchModel patient) {
    try {
      final IntArrayList icd9 = patient.getUniqueIcd9Codes();
      for (int x = 0; x < icd9.size(); x++) {
        final Icd9Iterator iterator = (Icd9Iterator) new PayloadPointers(TYPE.ICD9_RAW, null, patient, patient.getIcd9(icd9.get(x))).iterator(patient);
        while (iterator.hasNext()) {
          iterator.next();
          final int endDate = iterator.getEndId() == iterator.getStartId() ? iterator.getEndId() + 1 : iterator.getEndId();
          data.write(patient.getId() + "\t" + context.getIcd9(icd9.get(x)) + "\t" + getDate(iterator.getStartId()) + "\t" + getDate(endDate) + "\t\n");
        }
      }
      final IntArrayList cpt = patient.getUniqueCptCodes();
      for (int x = 0; x < cpt.size(); x++) {
        final PayloadIterator iterator = new PayloadPointers(TYPE.CPT_RAW, null, patient, patient.getCpt(cpt.get(x))).iterator(patient);
        while (iterator.hasNext()) {
          iterator.next();
          final int endDate = iterator.getEndId() == iterator.getStartId() ? iterator.getEndId() + 1 : iterator.getEndId();
          data.write(patient.getId() + "\tCPT" + context.getCpt(cpt.get(x)) + "\t" + getDate(iterator.getStartId()) + "\t" + getDate(endDate) + "\t\n");
        }
      }
      final IntArrayList rx = patient.getUniqueRxNormCodes();
      for (int x = 0; x < rx.size(); x++) {
        final PayloadIterator iterator = new PayloadPointers(TYPE.OTHER, null, patient, patient.getRxNorm(rx.get(x))).iterator(patient);
        while (iterator.hasNext()) {
          iterator.next();
          final int endDate = iterator.getEndId() == iterator.getStartId() ? iterator.getEndId() + 1 : iterator.getEndId();
          if (patient.getId() == 176540 && rx.get(x) == 161 && getDate(iterator.getStartId()).equals("2005-10-30 01:59")) {
            System.out.println();
          }
          data.write(patient.getId() + "\tRX" + rx.get(x) + "\t" + getDate(iterator.getStartId()) + "\t" + getDate(endDate) + "\t\n");
        }
      }
    }
    catch (final Exception e) {
      if (errorCnt < 10) {
        e.printStackTrace();
      }
      errorCnt++;
    }
  }

  public void export(final IndexCollection context, final PatientSearchModel patient) {
    processedPatients++;
    saveData(context, patient);
  }

  public void export(final String line) {
    processedPatients++;
    try {
      data.write(line);
    }
    catch (final Exception e) {
      if (errorCnt < 10) {
        e.printStackTrace();
      }
      errorCnt++;
    }
  }

  @Override
  public void close() {
    FileUtils.close(data);
  }

  public static void main(final String[] args) throws ParseException {
    for (int x = 0; x < 1000000; x++) {
      final String date1 = getDate(x);
      final String date2 = getDate(x + 1);
      final Date test1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date1);
      final Date test2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date2);
      if (test1.getTime() > test2.getTime()) {
        System.out.println("!!! " + x + " / " + date1 + " / " + date2);
      }
    }
  }
}
