package com.podalv.search.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.podalv.utils.file.FileUtils;

public class AtlasVersion {

  private String                    version  = "N/A";
  private static final AtlasVersion INSTANCE = new AtlasVersion();

  public static String getVersion() {
    return INSTANCE.version;
  }

  private AtlasVersion() {
    final ClassLoader classLoader = getClass().getClassLoader();
    final BufferedReader reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("version")));
    try {
      version = reader.readLine();
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
    finally {
      FileUtils.close(reader);
    }
  }

}
