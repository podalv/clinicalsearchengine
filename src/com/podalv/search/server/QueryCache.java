package com.podalv.search.server;

import static com.podalv.utils.Logging.debug;
import static com.podalv.utils.Logging.exception;
import static com.podalv.utils.Logging.info;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.map.IntKeyOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.utils.datastructures.SortPair;
import com.podalv.utils.file.FileUtils;

/** Stores cache of queries that take a long time to complete
 *
 * @author podalv
 *
 */
public class QueryCache {

  public static final String                           CACHE_FILE_NAME                = "cache.dat";
  public static final int                              QUERY_TIME_PURGE_LIMIT_MINUTES = 4 * 24 * 60;
  public static final int                              CACHE_PERSISTANCE_INTERVAL     = 30 * 60000;
  public static final int                              MAX_CACHED_QUERY_CNT           = 2000;
  public static final long                             SLOW_QUERY_TIME                = 3000;
  private final HashMap<PatientSearchRequest, Integer> queryToId                      = new HashMap<>();
  private final long                                   lastCacheSaveToDisk            = System.currentTimeMillis();
  private boolean                                      cacheChangedSinceSave          = false;
  private boolean                                      temporaryDisabled              = false;
  private final AtomicInteger                          queryIdCounter                 = new AtomicInteger(0);
  private final IntKeyOpenHashMap                      cache                          = new IntKeyOpenHashMap();
  private final IntKeyIntOpenHashMap                   queryIdToLastTime              = new IntKeyIntOpenHashMap();
  private static QueryCache                            INSTANCE                       = new QueryCache();

  public static void disable() {
    INSTANCE = new QueryCacheDisabled();
  }

  public static QueryCache getInstance() {
    return INSTANCE;
  }

  public void temporaryDisabled(final boolean disable) {
    this.temporaryDisabled = disable;
  }

  private int getQueryId(final PatientSearchRequest query) {
    return queryToId.containsKey(query) ? queryToId.get(query) : -1;
  }

  public int size() {
    return queryToId.size();
  }

  public synchronized void clear() {
    queryToId.clear();
    queryIdToLastTime.clear();
    cache.clear();
  }

  public void refreshCacheFromDisk(final PatientSearchManager searchManager) throws JsonSyntaxException {
    debug("Reloading cache results...");
    final ArrayList<String> queries = getQueriesFromDisk();
    for (int x = 0; x < queries.size(); x++) {
      debug("[" + (x + 1) + " / " + queries.size() + "] '" + queries.get(x));
      searchManager.submitSearchTask(-1, new Gson().fromJson(queries.get(x), PatientSearchRequest.class), Integer.MAX_VALUE);
    }
    debug("Done");
  }

  private ArrayList<String> getQueriesFromDisk() {
    try {
      return FileUtils.readFileArrayList(new File(CACHE_FILE_NAME), Charset.forName("UTF-8"));
    }
    catch (final Exception e) {
      exception("Cache data not found", e);
    }
    return new ArrayList<>();
  }

  private synchronized void saveCacheToDisk() {
    if (cacheChangedSinceSave && cache.size() > 0 && lastCacheSaveToDisk + CACHE_PERSISTANCE_INTERVAL >= System.currentTimeMillis()) {
      info("Saving cache to disk");
      try {
        final BufferedWriter writer = new BufferedWriter(new FileWriter(new File(CACHE_FILE_NAME)));
        final Iterator<Entry<PatientSearchRequest, Integer>> iterator = queryToId.entrySet().iterator();
        while (iterator.hasNext()) {
          final Entry<PatientSearchRequest, Integer> entry = iterator.next();
          writer.write(entry.getKey() + "\n");
        }
        writer.close();
      }
      catch (final Exception e) {
        exception(e);
      }
      cacheChangedSinceSave = false;
    }
  }

  public PatientSearchResponse getQueryResult(final PatientSearchRequest query) {
    saveCacheToDisk();
    final int queryId = getQueryId(query);
    if (queryId != -1) {
      final PatientSearchResponse result = (PatientSearchResponse) cache.get(queryId);
      queryIdToLastTime.put(queryId, getTimeInMinutes());
      info("Cache hit for '" + query.getQuery() + "'");
      return result;
    }
    return null;
  }

  private int getTimeInMinutes() {
    return (int) (System.currentTimeMillis() / 60000);
  }

  public synchronized void storeQuery(final PatientSearchRequest query, final PatientSearchResponse response) {
    if (response.getTimeTook() > SLOW_QUERY_TIME && !temporaryDisabled && !response.isEmpty() && !response.containsErrors()) {
      purgeCache();
      cacheChangedSinceSave = true;
      info("Added query to cache");
      final int queryId = queryIdCounter.incrementAndGet();
      queryToId.put(query, queryId);
      cache.put(queryId, response);
      queryIdToLastTime.put(queryId, getTimeInMinutes());
      if (PatientExport.getInstance() != null) {
        PatientExport.getInstance().exemptFileFromPurge(response.getExportLocation());
      }
    }
  }

  private void purgeCache() {
    debug("Cache size = " + cache.size());
    if (cache.size() > MAX_CACHED_QUERY_CNT) {
      final ArrayList<SortPair<Integer, Integer>> pastTime = new ArrayList<>();
      final IntKeyIntMapIterator iterator = queryIdToLastTime.entries();
      final IntOpenHashSet purgedIds = new IntOpenHashSet();
      while (iterator.hasNext()) {
        iterator.next();
        if (iterator.getValue() < getTimeInMinutes() - QUERY_TIME_PURGE_LIMIT_MINUTES) {
          debug("Purged cache " + iterator.getKey());
          PatientExport.getInstance().deleteFile(((PatientSearchResponse) cache.get(iterator.getKey())).getExportLocation());
          purgedIds.add(iterator.getKey());
          cache.remove(iterator.getKey());
          cacheChangedSinceSave = true;
          iterator.remove();
        }
        else {
          pastTime.add(new SortPair<>(iterator.getValue(), iterator.getKey()));
        }
      }
      if (cache.size() > MAX_CACHED_QUERY_CNT) {
        Collections.sort(pastTime);
        for (int x = 0; x < Math.min(cache.size(), MAX_CACHED_QUERY_CNT - (MAX_CACHED_QUERY_CNT * 0.1)); x++) {
          debug("Purged cache " + pastTime.get(x).getObject());
          PatientExport.getInstance().deleteFile(((PatientSearchResponse) cache.get(pastTime.get(x).getObject())).getExportLocation());
          purgedIds.add(pastTime.get(x).getObject());
          cache.remove(pastTime.get(x).getObject());
          queryIdToLastTime.remove(pastTime.get(x).getObject());
          cacheChangedSinceSave = true;
        }
      }
      for (int x = 0; x < purgedIds.size(); x++) {
        final Iterator<Entry<PatientSearchRequest, Integer>> i = queryToId.entrySet().iterator();
        while (i.hasNext()) {
          final Entry<PatientSearchRequest, Integer> entry = i.next();
          if (purgedIds.contains(entry.getValue())) {
            i.remove();
          }
        }
      }
    }
  }
}
