package com.podalv.search.server;

import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;

/** Ignores all requests
 *
 * @author podalv
 *
 */
public class QueryCacheDisabled extends QueryCache {

  @Override
  public PatientSearchResponse getQueryResult(final PatientSearchRequest query) {
    return null;
  }

  @Override
  public void refreshCacheFromDisk(final PatientSearchManager searchManager) {
    // do nothing
  }

  @Override
  public synchronized void storeQuery(final PatientSearchRequest query, final PatientSearchResponse response) {
    // do nothing
  }

}
