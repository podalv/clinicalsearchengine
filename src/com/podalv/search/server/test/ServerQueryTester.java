package com.podalv.search.server.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

/** Keeps hitting the server with requests measuring mean response time
 *
 * @author podalv
 *
 */
public class ServerQueryTester {

  private static void query(final String urlAddress, final String queryString, final String fileName) throws IOException {
    final PatientSearchRequest r = new PatientSearchRequest();
    r.setBinary(false);
    r.setQuery(queryString.toUpperCase());
    r.setStatisticsLimit(0);
    r.setPidCntLimit(0);
    System.out.println(new Gson().toJson(r));
    final URL url = new URL(urlAddress + "/query");
    //make connection
    final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
    //use post mode
    urlc.setDoOutput(true);
    urlc.setRequestMethod("POST");
    urlc.setAllowUserInteraction(false);
    //send query
    final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(urlc.getOutputStream(), "UTF-8"));
    bw.write(new Gson().toJson(r));
    bw.flush();
    bw.close();

    //get result
    if (fileName != null) {
      final PatientSearchResponse response = new Gson().fromJson(new InputStreamReader(urlc.getInputStream()), PatientSearchResponse.class);
      if (response.containsErrors()) {
        System.out.println("\nERROR = '" + response.getErrorMessage() + "\n");
        return;
      }
      final String exportLocation = response.getExportLocation();
      final URL website = new URL(urlAddress + "/" + exportLocation);
      final ReadableByteChannel rbc = Channels.newChannel(website.openStream());
      final FileOutputStream fos = new FileOutputStream(fileName);
      fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
      fos.close();
    }
  }

  public static void main(final String[] args) throws IOException, InterruptedException {
    System.out.println("Usage: URL variable_file command_file output_folder");
    final String url = args[0];
    final String variableFile = FileUtils.readFile(new File(args[1]), Charset.forName("UTF-8"));
    final ArrayList<String> commandFile = FileUtils.readFileArrayList(new File(args[2]), Charset.forName("UTF-8"));
    final File outputFolder = new File(args[3]);

    for (final String line : commandFile) {
      final String[] data = TextUtils.split(line, '\t');
      if (data.length == 2) {
        final String outputFile = new File(outputFolder, data[0]).getAbsolutePath();
        final String query = variableFile + "\n" + data[1];
        query(url, query, outputFile);
        System.out.println("Saved " + outputFile);
      }
    }
  }
}
