package com.podalv.search.server.test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;

public class PerformanceTest {

  public static void main(final String[] args) throws IOException {
    final String urlAddress = "http://localhost:8080";
    final ArrayList<String> queries = new ArrayList<>();
    queries.add("LIMIT(CACHE, ICD9=250.50)");
    queries.add("LIMIT(CACHE, INTERSECT(AGE(10 YEARS, 50 YEARS), CPT=22000, ICD9=250.50))");
    queries.add(
        "var inpatient = DURATION(VISIT TYPE=\"inpatient”, SINGLE, 24 HOURS, MAX)\nvar inpatient_adult = INTERSECT($inpatient, AGE(18 YEARS, MAX))\nvar dialysis = UNION(CPT=90935, CPT=90945, CPT=90937, CPT=90947, CPT=90993, ICD9=39.95)\n\nvar hep_enc1_inp = FIRST MENTION(RETURN $inpatient_adult INTERSECTING UNION(ICD9=572.2, ICD10=K72))\nvar hep_enc1 = RETURN $hep_enc1_inp INTERSECTING RX=6218\nvar hep_enc2_inp = FIRST MENTION(RETURN $inpatient_adult INTERSECTING LABS(\"16362-6\", 30, MAX))\nvar hep_enc2 = RETURN $hep_enc2_inp INTERSECTING RX=6218\nvar hep_enc = FIRST MENTION(UNION($hep_enc1, $hep_enc2))\nvar hep_enc_rif = SEQUENCE(START($hep_enc)*, FIRST MENTION(RX=35619)) + (-48 HOURS, 0)\nvar hep_enc_rif_rif = SEQUENCE(START($hep_enc), FIRST MENTION(RX=35619)*) + (-48 HOURS, 0)\nvar hep_enc_no_rif = RETURN $hep_enc INTERSECTING NO HISTORY OF(RX=35619)\nvar hep_enc_no_rif_filtered = RETURN $hep_enc_no_rif NOT INTERSECTING RX=35619\nvar rif_re = INTERVAL($hep_enc_rif, RECORD END)\nvar rif_re_30d_min = DURATION($rif_re, SINGLE, 30 days, MAX)\nvar rif_30d_fu = EXTEND BY(INTERSECT($hep_enc_rif, $rif_re_30d_min), START, START + 30 DAYS)\nvar no_rif_re = INTERVAL($hep_enc_no_rif_filtered, RECORD END)\nvar no_rif_re_30d_min = DURATION($no_rif_re, SINGLE, 30 days, MAX)\nvar no_rif_30d_fu = EXTEND BY(INTERSECT($hep_enc_no_rif_filtered, $no_rif_re_30d_min), START, START + 30 days)\nvar rif_30d_fu_exclusive = INTERSECT($rif_30d_fu, DIFF($rif_30d_fu, $no_rif_30d_fu))\nvar rif_30d_fu_exclusive_inpatient = INTERSECT($hep_enc_rif, $rif_30d_fu_exclusive)\nvar no_rif_30d_fu_exclusive = INTERSECT($no_rif_30d_fu, DIFF($no_rif_30d_fu, $rif_30d_fu))\nvar no_rif_30d_fu_exclusive_inpatient = INTERSECT($hep_enc_no_rif_filtered, $no_rif_30d_fu_exclusive)\nvar bilirubin_rif = SEQUENCE(START($rif_30d_fu_exclusive), LABS(“42719-5”)*) + (-24 hours, 0)\nvar inr_rif = SEQUENCE(START($rif_30d_fu_exclusive), LABS(“6301-6”)*) + (-24 hours, 0)\nvar creatinine_rif = SEQUENCE(START($rif_30d_fu_exclusive), LABS(“2160-0”)*) + (-24 hours, 0)\nvar rif_bili_inr_same = SAME($bilirubin_rif, $inr_rif)\nvar rif_bili_inr_creat_same = SAME($rif_bili_inr_same, $creatinine_rif)\nvar rif_30d_fu_exclusive_meld = INTERSECT($rif_30d_fu_exclusive, $rif_bili_inr_creat_same)\nvar rif_30d_fu_exclusive_meld_dialysis = COUNT($dialysis, EXTEND BY($rif_30d_fu_exclusive_meld, START - 7 DAYS, START), SINGLE, 1, MAX)\nvar rif_30d_fu_exclusive_meld_inpatient = INTERSECT($hep_enc_rif, $rif_30d_fu_exclusive_meld)\nvar rif_bili = INTERSECT(FIRST MENTION($bilirubin_rif), $rif_bili_inr_creat_same)\nvar rif_inr = INTERSECT(FIRST MENTION($inr_rif), $rif_bili_inr_creat_same)\nvar rif_creat = INTERSECT(FIRST MENTION($creatinine_rif), $rif_bili_inr_creat_same)\nvar bilirubin_no_rif = SEQUENCE(START($no_rif_30d_fu_exclusive_inpatient), LABS(“42719-5”)*) + (-24 hours, 0)\nvar inr_no_rif = SEQUENCE(START($no_rif_30d_fu_exclusive_inpatient), LABS(“6301-6”)*) + (-24 hours, 0)\nvar creatinine_no_rif = SEQUENCE(START($no_rif_30d_fu_exclusive_inpatient), LABS(“2160-0”)*) + (-24 hours, 0)\nvar no_rif_bili_inr_same = SAME($bilirubin_no_rif, $inr_no_rif)\nvar no_rif_bili_inr_creat_same = SAME($no_rif_bili_inr_same, $creatinine_no_rif)\nvar no_rif_30d_fu_exclusive_meld = INTERSECT($no_rif_30d_fu_exclusive, $no_rif_bili_inr_creat_same)\nvar no_rif_30d_fu_exclusive_meld_dialysis = COUNT($dialysis, EXTEND BY($no_rif_30d_fu_exclusive, START - 7 DAYS, START), SINGLE, 1, MAX)\nLIMIT(CACHE, $rif_30d_fu_exclusive_meld_dialysis)");
    long sum = 0;
    long cnt = 0;
    for (final String queryString : queries) {
      for (int x = 0; x < 10; x++) {
        final PatientSearchRequest r = new PatientSearchRequest();
        r.setBinary(false);
        r.setQuery(queryString.toUpperCase());
        r.setStatisticsLimit(0);
        r.setPidCntLimit(0);
        final URL url = new URL(urlAddress + "/query");
        //make connection
        final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
        //use post mode
        urlc.setDoOutput(true);
        urlc.setRequestMethod("POST");
        urlc.setAllowUserInteraction(false);
        //send query
        final long time = System.currentTimeMillis();
        final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(urlc.getOutputStream(), "UTF-8"));
        bw.write(new Gson().toJson(r));
        bw.flush();
        bw.close();

        //get result
        final PatientSearchResponse response = new Gson().fromJson(new InputStreamReader(urlc.getInputStream()), PatientSearchResponse.class);
        if (response.containsErrors()) {
          System.out.println("\nERROR = '" + response.getErrorMessage() + "\n");
          continue;
        }
        final String exportLocation = response.getExportLocation();
        final URL website = new URL(urlAddress + "/" + exportLocation);
        final ReadableByteChannel rbc = Channels.newChannel(website.openStream());

        final java.nio.ByteBuffer dst = ByteBuffer.allocate(100000);
        rbc.read(dst);
        rbc.close();
        cnt++;
        sum += System.currentTimeMillis() - time;
      }
      System.out.println(sum / (double) cnt);
    }
  }
}

/*
19.0
57.65
3591.366666666667
*/

/*
19.5
65.35
230.6
*/