package com.podalv.search.server;

import static com.podalv.utils.Logging.debug;
import static com.podalv.utils.Logging.exception;
import static com.podalv.utils.Logging.info;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.podalv.search.datastructures.PatientSearchRequest;
import com.podalv.search.datastructures.PatientSearchResponse;
import com.podalv.search.datastructures.PidRequest;
import com.podalv.search.datastructures.ServerStatusResponse;
import com.podalv.search.datastructures.SlaveListResponse;
import com.podalv.search.datastructures.StatisticsResponse;
import com.podalv.utils.arrays.ArrayUtils;
import com.podalv.utils.datastructures.SortPairSingle;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;
import com.podalv.workshop.BooleanResponse;
import com.podalv.workshop.ContainsPatientRequest;
import com.podalv.workshop.datastructures.DumpRequest;
import com.podalv.workshop.datastructures.DumpResponse;

/** Contains information about currently running slaves
 *  Slaves file contents:
 *  SLAVE_NAME;URL
 *  SLAVE_NAME;URL
 *  SHARD;URL => shard slave
 *
 * @author podalv
 *
 */
public class SearchEngineSlaveManager {

  public static final char        SPLITTER           = ';';
  public static final String      SHARD_SERVER       = "SHARD";
  private final File              slaveFile;
  private final ArrayList<String> slaveFileLines;
  private final HashSet<String>   shardUrls          = new HashSet<>();
  private final HashSet<String>   shardIps           = new HashSet<>();
  private final HashSet<String>   slaveBroadcastUrls = new HashSet<>();
  private final HashSet<String>   slaveBroadcastIps  = new HashSet<>();
  private final SlaveListResponse response           = new SlaveListResponse();

  public static SearchEngineSlaveManager createEmpty() {
    return new SearchEngineSlaveManager((File) null);
  }

  public boolean isImmutable() {
    return slaveFileLines != null;
  }

  public boolean containsSlaveIp(final String ip) {
    debug("Searching for IP '" + ip + "'");
    final Iterator<String> i = shardIps.iterator();
    while (i.hasNext()) {
      debug("Available IPs '" + i.next() + "'");
    }
    return shardIps.contains(ip);
  }

  public boolean containsSlave(final String url) {
    return shardUrls.contains(url) || slaveBroadcastUrls.contains(url);
  }

  public void addSlave(final String url) {
    if (!containsSlave(url)) {
      slaveBroadcastUrls.add(url);
      slaveBroadcastIps.add(url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf(":")));
      info("Adding '" + url + " = " + url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf(":")));
    }
    update();
  }

  public SearchEngineSlaveManager(final ArrayList<String> slaveFileLines) {
    this.slaveFile = null;
    this.slaveFileLines = slaveFileLines;
    update();
  }

  public SearchEngineSlaveManager(final File slaveFile) {
    this.slaveFile = slaveFile;
    this.slaveFileLines = null;
    update();
  }

  public boolean containsShardSlaves() {
    return !shardUrls.isEmpty();
  }

  public Iterator<String> getShard() {
    return shardUrls.iterator();
  }

  private void appendLinkedFileToResults(final String path, final BufferedWriter output) {
    try {
      final BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(path).openStream()));
      reader.readLine(); // ignore the header
      String line = null;
      while ((line = reader.readLine()) != null) {
        output.write(line + "\n");
      }
    }
    catch (final Exception e) {
      exception(e);
    }
  }

  public DumpResponse querySlavesDump(final DumpRequest request) {
    final Iterator<String> shards = shardUrls.iterator();
    try {
      final String queryStr = new Gson().toJson(new ContainsPatientRequest(request.getPatientId()));
      while (shards.hasNext()) {
        final String url = shards.next();
        final BooleanResponse response = new Gson().fromJson(QueryUtils.query(url + AtlasRequestHandler.CONTAINS_PID_QUERY, queryStr, 1), BooleanResponse.class);
        if (response.getResponse()) {
          return new Gson().fromJson(QueryUtils.query(url + AtlasRequestHandler.DUMP_QUERY, new Gson().toJson(request), 1), DumpResponse.class);
        }
      }
    }
    catch (final Exception e) {
      return DumpResponse.createError(e.getMessage());
    }
    return DumpResponse.createError("Patient not found");
  }

  public boolean queryContainsPid(final ContainsPatientRequest req) {
    final Iterator<String> shards = shardUrls.iterator();
    try {
      while (shards.hasNext()) {
        final String url = shards.next();
        if (new Gson().fromJson(QueryUtils.query(url + AtlasRequestHandler.CONTAINS_PID_QUERY, new Gson().toJson(req), 1), BooleanResponse.class).getResponse()) {
          return true;
        }
      }
    }
    catch (final Exception e) {
      exception(e);
    }
    return false;
  }

  public StatisticsResponse[] querySlavesStatistics() {
    final Iterator<String> shards = shardUrls.iterator();
    final ArrayList<StatisticsResponse> responses = new ArrayList<>();
    try {
      while (shards.hasNext()) {
        final String url = shards.next();
        responses.add(new Gson().fromJson(QueryUtils.query(url + AtlasRequestHandler.STATISTICS_QUERY, "", 1), StatisticsResponse.class));
      }
    }
    catch (final Exception e) {
      exception(e);
    }
    System.out.println("Returning " + responses.size() + " slave statistics responses");
    return responses.toArray(new StatisticsResponse[responses.size()]);
  }

  public PatientSearchResponse querySlaves(final ClinicalSearchEngine engine, final PatientSearchRequest query, final long timeout) throws InterruptedException {
    info("querying slaves");
    final long mainTime = System.currentTimeMillis();
    final ArrayList<PatientSearchResponse> masterResponse = new ArrayList<>();
    final ExecutorService executor = Executors.newCachedThreadPool();
    boolean interrupted = false;
    final LinkedList<Future<SortPairSingle<String, PatientSearchResponse>>> results = new LinkedList<>();
    final PatientSearchRequest binaryRequest = PatientSearchRequest.copy(query, query.getQuery());
    binaryRequest.setBinary(true);
    try {
      final Iterator<String> shards = shardUrls.iterator();
      final String queryStr = new Gson().toJson(binaryRequest);
      while (shards.hasNext()) {
        final String url = shards.next();
        info(url);
        results.add(executor.submit(new Callable<SortPairSingle<String, PatientSearchResponse>>() {

          @Override
          public SortPairSingle<String, PatientSearchResponse> call() {
            final long time = System.currentTimeMillis();
            try {
              info("Processing " + url);
              return new SortPairSingle<>(url, (PatientSearchResponse) QueryUtils.queryBinaryPatientSearchResponse(url + AtlasRequestHandler.SEARCH_QUERY, queryStr, -1, 5000));
            }
            catch (final Exception e) {
              exception(e);
              return new SortPairSingle<>(url, PatientSearchResponse.createError("Communication error at " + url, -1));
            }
            finally {
              info(url + " took " + (System.currentTimeMillis() - time) + " ms");
            }
          }
        }));
      }
      results.add(executor.submit(new Callable<SortPairSingle<String, PatientSearchResponse>>() {

        @Override
        public SortPairSingle<String, PatientSearchResponse> call() {
          try {
            long minTime = System.currentTimeMillis();
            debug("Starting main search task...");
            final PatientSearchResponse result = engine.search(-1, query);
            debug("Main time took " + (System.currentTimeMillis() - minTime));
            minTime = System.currentTimeMillis();
            masterResponse.add(result);
            debug("Added to master in " + (System.currentTimeMillis() - minTime));
            return null;
          }
          catch (final Exception e) {
            exception(e);
            return new SortPairSingle<>("MASTER", PatientSearchResponse.createError("Error collating responses at MASTER", -1));
          }
        }
      }));
    }
    finally {
      executor.shutdown();
      debug("Awaiting termination...");
      interrupted = !executor.awaitTermination(timeout, TimeUnit.MILLISECONDS);
      if (!executor.isTerminated()) {
        debug("Terminating...");
        debug(executor.shutdownNow().size() + " jobs did not complete");
      }
      debug("Shutdown");
    }
    final long finalTime = System.currentTimeMillis();
    final ArrayList<SortPairSingle<String, PatientSearchResponse>> responses = new ArrayList<>();
    if (results.size() != 0) {
      for (final Future<SortPairSingle<String, PatientSearchResponse>> resp : results) {
        try {
          if (resp.isDone() && resp.get() != null) {
            final SortPairSingle<String, PatientSearchResponse> response = resp.get();
            if (masterResponse.size() != 0) {
              responses.add(response);
            }
          }
        }
        catch (final Exception e) {
          exception(e);
          interrupted = true;
        }
      }
    }
    debug("Added final in " + (System.currentTimeMillis() - finalTime));
    debug("Adding responses...");
    final long lastFinal = System.currentTimeMillis();
    if (masterResponse.size() != 0) {
      final PatientSearchResponse[] r = new PatientSearchResponse[responses.size()];
      for (int x = 0; x < responses.size(); x++) {
        r[x] = responses.get(x).getObject();
      }
      masterResponse.get(0).add(r);
      if (masterResponse.get(0).getExportLocation() != null && !masterResponse.get(0).getExportLocation().isEmpty()) {
        try {
          final BufferedWriter fos = new BufferedWriter(new FileWriter(new File(masterResponse.get(0).getExportLocation()), true));
          for (final SortPairSingle<String, PatientSearchResponse> response : responses) {
            appendLinkedFileToResults(response.getComparable() + "/" + response.getObject().getExportLocation(), fos);
          }
          fos.close();
        }
        catch (final Exception e) {
          masterResponse.get(0).setError("Error collecting responses from slaves");
          exception(e);
        }
      }
      debug("Added..." + " in " + (System.currentTimeMillis() - lastFinal));
    }

    if (masterResponse.size() != 0 && interrupted) {
      masterResponse.get(0).addWarnings("Timed out execution of slaves");
      masterResponse.get(0).setTimeTook(System.currentTimeMillis() - mainTime);
    }

    if (masterResponse.size() != 0) {
      if (masterResponse.get(0).getPatientIds().size() <= PatientSearch.MIN_PATIENT_CNT) {
        masterResponse.get(0).clearData();
      }
    }

    return masterResponse.size() == 0 ? PatientSearchResponse.createError("No queries arrived...", -1) : masterResponse.get(0);
  }

  public void queryPids(final OutputStream out, final ClinicalSearchEngine engine, final PidRequest query, final long timeout) throws InterruptedException {
    query.setBinary(true);
    final ExecutorService executor = Executors.newCachedThreadPool();
    try {
      final Iterator<String> shards = shardUrls.iterator();
      final String queryStr = new Gson().toJson(query);
      final PrintWriter writer = new PrintWriter(out);
      while (shards.hasNext()) {
        final String url = shards.next();
        executor.submit(new Runnable() {

          @Override
          public void run() {
            try {
              QueryUtils.queryBinaryPidList(url + AtlasRequestHandler.PID_LIST, queryStr, -1, writer);
            }
            catch (final Exception e) {
              exception(e);
            }
          }
        });
      }
    }
    finally {
      executor.shutdown();
      executor.awaitTermination(timeout, TimeUnit.MILLISECONDS);
      try {
        engine.search(query.getClone(false), out);
      }
      catch (final IOException e) {
        e.printStackTrace();
      }
    }
  }

  public SlaveListResponse getInitResponseString() {
    return response;
  }

  public synchronized void update() {
    response.clear();
    if (slaveFile != null || slaveFileLines != null) {
      try {
        final ArrayList<String> slaves = (slaveFile != null && slaveFile.exists()) ? FileUtils.readFileArrayList(slaveFile, Charset.forName("UTF-8"))
            : ArrayUtils.deepCopy(slaveFileLines);
        for (int x = 0; x < slaves.size(); x++) {
          if (slaves.get(x).trim().isEmpty()) {
            continue;
          }
          if (slaves.get(x).startsWith("//")) {
            continue;
          }
          final String[] data = TextUtils.split(slaves.get(x), SPLITTER);
          if (data[0].equalsIgnoreCase(SHARD_SERVER)) {
            shardUrls.add(data[1].trim());
            shardIps.add(data[1].trim().substring(data[1].trim().lastIndexOf("/") + 1, data[1].trim().lastIndexOf(":")));
          }
          else {
            shardUrls.add(slaves.get(x).trim());
            shardIps.add(data[1].trim().substring(data[1].trim().lastIndexOf("/") + 1, data[1].trim().lastIndexOf(":")));
          }
        }
        shardUrls.addAll(slaveBroadcastUrls);
        shardIps.addAll(slaveBroadcastIps);
        info("Slave list updated");
        final Iterator<String> i = shardUrls.iterator();
        while (i.hasNext()) {
          final String url = i.next();
          ServerStatusResponse resp = null;
          try {
            resp = new Gson().fromJson(QueryUtils.query(url + AtlasRequestHandler.STATUS_QUERY, "", 0, 5000), ServerStatusResponse.class);
          }
          catch (final Exception e) {
            resp = new ServerStatusResponse(e.getMessage(), false);
          }
          response.addUrl(resp);
        }
      }
      catch (final Exception e) {
        exception(e);
        response.setError("Error while reading the slave file '" + e.getMessage() + "'");
      }
    }
    info(new Gson().toJson(response));
    info("Total number of slaves = " + shardUrls.size());
  }

}
