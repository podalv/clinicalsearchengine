package com.podalv.search.datastructures;

public class RegisterSlaveRequest {

  private final int    port;
  private final String groupId;
  private String       url;

  public RegisterSlaveRequest(final String url, final int port, final String groupId) {
    this.port = port;
    this.groupId = groupId;
    this.url = url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public int getPort() {
    return port;
  }

  public String getGroupId() {
    return groupId;
  }

  public String getUrl() {
    return "http://" + url + ":" + port;
  }
}
