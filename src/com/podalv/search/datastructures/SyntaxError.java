package com.podalv.search.datastructures;

public class SyntaxError {

  private String error;
  private int    startPosX;
  private int    endPosX;
  private int    posY;

  public int getEndPosX() {
    return endPosX;
  }

  public String getError() {
    return error;
  }

  public int getPosY() {
    return posY;
  }

  public void setEndPosX(final int endPosX) {
    this.endPosX = endPosX;
  }

  public void setError(final String error) {
    this.error = error;
  }

  public void setPosY(final int posY) {
    this.posY = posY;
  }

  public void setStartPosX(final int startPosX) {
    this.startPosX = startPosX;
  }

  public int getStartPosX() {
    return startPosX;
  }
}
