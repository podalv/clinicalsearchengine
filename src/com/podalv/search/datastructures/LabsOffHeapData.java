package com.podalv.search.datastructures;

import org.junit.Assert;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.map.IntKeyOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.primitive.set.IntSet;
import com.podalv.objectdb.datastructures.OffHeapIntArrayList;
import com.podalv.objectdb.datastructures.OffHeapIntArrayList.COMPRESSION_TYPE;
import com.podalv.objectdb.datastructures.PersistentIntKeyObjectMap;

/** Allows storing and searching in the labs data structure stored off heap
 *
 * @author podalv
 *
 */
public class LabsOffHeapData extends LabsData {

  private OffHeapMemory    memory;
  private long             position;
  private COMPRESSION_TYPE payloadDataType;
  private COMPRESSION_TYPE typeIdSizeDataType = null;
  private COMPRESSION_TYPE typeIdDataType     = null;

  public LabsOffHeapData(final OffHeapMemory memory, final long position, final ObjectWithPayload patient, final COMPRESSION_TYPE payloadDataType) {
    super(patient, null);
    this.memory = memory;
    this.position = position;
    this.payloadDataType = payloadDataType;
    if (!(patient instanceof PatientBuilder)) {
      readTypeIds();
    }
  }

  public void setMemory(final OffHeapMemory mem, final COMPRESSION_TYPE payloadDataType) {
    this.memory = mem;
    this.payloadDataType = payloadDataType;
  }

  private void readTypeIds() {
    if (position >= 0) {
      final short header = memory.getUnsignedByte(position);
      convertHeader(header);
    }
  }

  private void convertHeader(short header) {
    if (position >= 0) {
      if (header < 10) {
        typeIdSizeDataType = COMPRESSION_TYPE.BYTE;
      }
      else if (header < 20) {
        typeIdSizeDataType = COMPRESSION_TYPE.SHORT;
        header -= 10;
      }
      else if (header < 30) {
        typeIdSizeDataType = COMPRESSION_TYPE.INT;
        header -= 20;
      }
      else {
        throw new UnsupportedOperationException("Value1 " + header + " is not supported");
      }
      if (header == 0) {
        typeIdDataType = COMPRESSION_TYPE.BYTE;
      }
      else if (header == 1) {
        typeIdDataType = COMPRESSION_TYPE.SHORT;
      }
      else if (header == 2) {
        typeIdDataType = COMPRESSION_TYPE.INT;
      }
      else {
        throw new UnsupportedOperationException("Value2 " + header + " is not supported");
      }
    }
  }

  @Override
  public IntSet getUniqueLabIds() {
    final IntOpenHashSet result = new IntOpenHashSet();
    if (position >= 0) {
      long pos = position + 3;
      for (int x = 0; x < getMapSize(); x++) {
        result.add(memory.getUnsignedShort(pos));
        pos += 2;
        int typeIdSize;
        if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
          typeIdSize = memory.getInt(pos);
          pos += 4;
        }
        else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
          typeIdSize = memory.getUnsignedShort(pos);
          pos += 2;
        }
        else {
          typeIdSize = memory.getUnsignedByte(pos);
          pos++;
        }
        for (int y = 0; y < typeIdSize; y += 2) {
          if (typeIdDataType == COMPRESSION_TYPE.INT) {
            pos += 4;
          }
          else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
            pos += 2;
          }
          else {
            pos++;
          }
          pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
        }
      }
    }
    return result;
  }

  @Override
  public IntArrayList getRawData(final int labId) {
    final IntArrayList result = new IntArrayList();
    if (position >= 0) {
      long pos = position + 3;
      for (int x = 0; x < getMapSize(); x++) {
        if (labId == memory.getUnsignedShort(pos)) {
          pos += 2;
          int typeIdSize;
          if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
            typeIdSize = memory.getInt(pos);
            pos += 4;
          }
          else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
            typeIdSize = memory.getUnsignedShort(pos);
            pos += 2;
          }
          else {
            typeIdSize = memory.getUnsignedByte(pos);
            pos++;
          }
          for (int y = 0; y < typeIdSize; y += 2) {
            int typeId;
            if (typeIdDataType == COMPRESSION_TYPE.INT) {
              typeId = memory.getInt(pos);
              pos += 4;
            }
            else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
              typeId = memory.getUnsignedShort(pos);
              pos += 2;
            }
            else {
              typeId = memory.getUnsignedByte(pos);
              pos++;
            }
            result.add(typeId);
            final OffHeapIntArrayList list = new OffHeapIntArrayList(memory, pos, COMPRESSION_TYPE.SHORT, payloadDataType, 0);
            result.add(list.size());
            for (int z = 0; z < list.size(); z++) {
              if (list.get(z) == PatientBuilder.INVALID_PAYLOAD) {
                result.add(Integer.MIN_VALUE);
              }
              else {
                result.add(patient.getPayload(list.get(z)).get(0));
              }
            }
            pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
          }
          break;
        }
        else {
          pos += 2;
          int typeIdSize;
          if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
            typeIdSize = memory.getInt(pos);
            pos += 4;
          }
          else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
            typeIdSize = memory.getUnsignedShort(pos);
            pos += 2;
          }
          else {
            typeIdSize = memory.getUnsignedByte(pos);
            pos++;
          }
          for (int y = 0; y < typeIdSize; y += 2) {
            if (typeIdDataType == COMPRESSION_TYPE.INT) {
              pos += 4;
            }
            else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
              pos += 2;
            }
            else {
              pos++;
            }
            pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
          }
        }
      }
    }
    return result;
  }

  @Override
  public PersistentIntKeyObjectMap getData() {
    throw new UnsupportedOperationException();
  }

  public void setPosition(final long position, final COMPRESSION_TYPE payloadDataType) {
    this.position = position;
    this.payloadDataType = payloadDataType;
    readTypeIds();
  }

  /** Records unique labs into the provided structure where labId = index
  *
  * @param transientData
  * @return number of unique labs
  */
  @Override
  public int recordLabs(final int[] transientData) {
    if (position >= 0) {
      long pos = position + 3;
      for (int x = 0; x < getMapSize(); x++) {
        transientData[memory.getUnsignedShort(pos)]++;
        pos += 2;
        int typeIdSize;
        if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
          typeIdSize = memory.getInt(pos);
          pos += 4;
        }
        else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
          typeIdSize = memory.getUnsignedShort(pos);
          pos += 2;
        }
        else {
          typeIdSize = memory.getUnsignedByte(pos);
          pos++;
        }
        for (int y = 0; y < typeIdSize; y += 2) {
          if (typeIdDataType == COMPRESSION_TYPE.INT) {
            pos += 4;
          }
          else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
            pos += 2;
          }
          else {
            pos++;
          }
          pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
        }
      }
    }
    return getMapSize();
  }

  @Override
  public boolean isEmpty() {
    return getMapSize() == 0;
  }

  private void assertMapCanBeSaved(final IntKeyOpenHashMap map) {
    Assert.assertTrue(map.size() < 65000);
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final IntArrayList list = (IntArrayList) iterator.getValue();
      Assert.assertTrue(list.size() < 130000);
      final IntOpenHashSet processedType = new IntOpenHashSet();
      int prevType = -1;
      for (int x = 0; x < list.size(); x += 2) {
        if (list.get(x) > 255 * 255) {
          Assert.fail("The lab type value is too high (" + list.get(x) + ")");
        }
        if (x == 0) {
          prevType = list.get(x);
          processedType.add(list.get(x));
        }
        else {
          if (list.get(x) != prevType) {
            if (processedType.contains(list.get(x))) {
              Assert.fail("The lab types (NORMAL, HIGH, LOW ...) are not sorted");
            }
            prevType = list.get(x);
          }
        }
      }
      if (processedType.size() > 255) {
        Assert.fail("There are too many LAB types (" + processedType.size() + ")");
      }
    }
  }

  @Override
  public IntArrayList getAllTimes() {
    final IntArrayList result = new IntArrayList();
    if (position >= 0) {
      long pos = position + 3;
      for (int x = 0; x < getMapSize(); x++) {
        pos += 2;
        int typeIdSize;
        if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
          typeIdSize = memory.getInt(pos);
          pos += 4;
        }
        else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
          typeIdSize = memory.getUnsignedShort(pos);
          pos += 2;
        }
        else {
          typeIdSize = memory.getUnsignedByte(pos);
          pos++;
        }
        for (int y = 0; y < typeIdSize; y += 2) {
          if (typeIdDataType == COMPRESSION_TYPE.INT) {
            pos += 4;
          }
          else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
            pos += 2;
          }
          else {
            pos++;
          }
          final OffHeapIntArrayList l = new OffHeapIntArrayList(memory, pos, COMPRESSION_TYPE.SHORT, payloadDataType, 0);
          for (int z = 0; z < l.size(); z++) {
            final int time = patient.getPayload(l.get(z)).get(0);
            result.add(time);
            result.add(time);
          }
          pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
        }
      }
    }
    result.sort();
    return result;
  }

  /** Given the initial map, calculates the starting positions of each typeIds
   *
   * @return [TypeId, position, TypeId, position...]
   */
  private int[] calculateTypeIdPositionTuplets(final IntArrayList list) {
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < list.size(); x += 2) {
      if (x == 0 || list.get(x) != result.get(result.size() - 2)) {
        result.add(list.get(x));
        result.add(x);
      }
    }
    return result.toArray();
  }

  private short calculateHeader(final IntKeyOpenHashMap map) {
    int maxTypeIdSize = 0;
    int maxTypeId = 0;
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final IntArrayList list = (IntArrayList) iterator.getValue();
      final int[] positions = calculateTypeIdPositionTuplets(list);
      maxTypeIdSize = Math.max(positions.length, maxTypeIdSize);
      for (int x = 0; x < list.size(); x += 2) {
        maxTypeId = Math.max(maxTypeId, list.get(x));
      }
    }
    return calculateHeader(maxTypeIdSize, maxTypeId);
  }

  protected static short calculateHeader(final int maxTypeIdSize, final int maxTypeId) {
    short result = 0;
    if (maxTypeIdSize < 255) {
      result = 0;
    }
    else if (maxTypeIdSize < (255 * 255)) {
      result = 10;
    }
    else {
      result = 20;
    }
    if (maxTypeId < 255) {
      result += 0;
    }
    else if (maxTypeId < (255 * 255)) {
      result += 1;
    }
    else {
      result += 2;
    }
    return result;
  }

  /** Format for saving offheap is:
   *  [SHORT] map key size
   *
   *  [SHORT] labId
   *  [BYTE] actual number of typeIds
   *  [SHORT] typeId
   *  [SHORT] number of times
   *  [INT] [INT] -> time instances
   *
   * @param map
   * @return
   */
  long save(final IntKeyOpenHashMap map) {
    assertMapCanBeSaved(map);
    long savingPosition = position;
    // header
    final short header = calculateHeader(map);
    memory.putUnsignedByte(savingPosition++, header);
    convertHeader(header);
    // map key size
    memory.putUnsignedShort(savingPosition, map.size());
    savingPosition += 2;
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final IntArrayList list = (IntArrayList) iterator.getValue();
      final int[] positions = calculateTypeIdPositionTuplets(list);
      //labId
      memory.putUnsignedShort(savingPosition, iterator.getKey());
      savingPosition += 2;
      // actual number of typeIds
      if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
        memory.putInt(savingPosition, positions.length);
        savingPosition += 4;
      }
      else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
        memory.putUnsignedShort(savingPosition, positions.length);
        savingPosition += 2;
      }
      else {
        memory.putUnsignedByte(savingPosition, (short) positions.length);
        savingPosition++;
      }
      for (int x = 0; x < positions.length; x += 2) {
        // typeId
        if (typeIdDataType == COMPRESSION_TYPE.INT) {
          memory.putInt(savingPosition, positions[x]);
          savingPosition += 4;
        }
        else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
          memory.putUnsignedShort(savingPosition, positions[x]);
          savingPosition += 2;
        }
        else {
          memory.putUnsignedByte(savingPosition, (short) positions[x]);
          savingPosition++;
        }
        final long cntPos = savingPosition;
        savingPosition += 2;
        int cnt = 0;
        for (int y = positions[x + 1]; y < list.size(); y += 2) {
          if (list.get(y) == positions[x]) {
            cnt++;
            // time
            if (payloadDataType == COMPRESSION_TYPE.BYTE) {
              memory.putUnsignedByte(savingPosition, (short) list.get(y + 1));
            }
            else if (payloadDataType == COMPRESSION_TYPE.SHORT) {
              memory.putUnsignedShort(savingPosition, (short) list.get(y + 1));
            }
            else {
              memory.putUnsignedInt(savingPosition, (short) list.get(y + 1));
            }
            savingPosition += Common.compressionTypeToSizeInBytes(payloadDataType);
          }
          else {
            break;
          }
        }
        // number of times
        memory.putUnsignedShort(cntPos, cnt);
      }
    }
    return savingPosition;
  }

  private int getMapSize() {
    if (position >= 0) {
      return memory.getUnsignedShort(position + 1);
    }
    return 0;
  }

  @Override
  public IntArrayList get(final int labId, final int valueType) {
    if (position >= 0) {
      long pos = position + 3;
      for (int x = 0; x < getMapSize(); x++) {
        if (labId == memory.getUnsignedShort(pos)) {
          pos += 2;
          int typeIdSize;
          if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
            typeIdSize = memory.getInt(pos);
            pos += 4;
          }
          else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
            typeIdSize = memory.getUnsignedShort(pos);
            pos += 2;
          }
          else {
            typeIdSize = memory.getUnsignedByte(pos);
            pos++;
          }
          for (int y = 0; y < typeIdSize; y += 2) {
            int typeId;
            if (typeIdDataType == COMPRESSION_TYPE.INT) {
              typeId = memory.getInt(pos);
              pos += 4;
            }
            else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
              typeId = memory.getUnsignedShort(pos);
              pos += 2;
            }
            else {
              typeId = memory.getUnsignedByte(pos);
              pos++;
            }
            if (typeId == valueType) {
              return new OffHeapIntArrayList(memory, pos, COMPRESSION_TYPE.SHORT, payloadDataType, 0);
            }
            pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
          }
        }
        else {
          pos += 2;
          int typeIdSize;
          if (typeIdSizeDataType == COMPRESSION_TYPE.INT) {
            typeIdSize = memory.getInt(pos);
            pos += 4;
          }
          else if (typeIdSizeDataType == COMPRESSION_TYPE.SHORT) {
            typeIdSize = memory.getUnsignedShort(pos);
            pos += 2;
          }
          else {
            typeIdSize = memory.getUnsignedByte(pos);
            pos++;
          }
          for (int y = 0; y < typeIdSize; y += 2) {
            if (typeIdDataType == COMPRESSION_TYPE.INT) {
              pos += 4;
            }
            else if (typeIdDataType == COMPRESSION_TYPE.SHORT) {
              pos += 2;
            }
            else {
              pos++;
            }
            pos += 2 + (memory.getUnsignedShort(pos) * Common.compressionTypeToSizeInBytes(payloadDataType));
          }
        }
      }
    }
    return null;
  }
}