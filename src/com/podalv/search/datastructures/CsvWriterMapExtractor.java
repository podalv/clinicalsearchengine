package com.podalv.search.datastructures;

import java.util.ArrayList;
import java.util.HashMap;

public class CsvWriterMapExtractor implements CsvWriter {

  private HashMap<String, ArrayList<String>> map = new HashMap<>();

  @Override
  public void write(String name, int time, double value) {
    if (!MeasurementOffHeapData.isEventInvalid(time)) {
      ArrayList<String> list = map.get(name);
      if (list == null) {
        list = new ArrayList<>();
        map.put(name, list);
      }
      list.add(time + "");
      list.add(value + "");
    }
  }

  public HashMap<String, ArrayList<String>> result() {
    return map;
  }

}
