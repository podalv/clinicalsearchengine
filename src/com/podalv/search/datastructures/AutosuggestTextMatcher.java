package com.podalv.search.datastructures;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.map.IntKeyOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.utils.text.TextUtils;
import com.podalv.utils.text.search.TextMatcherBuilder;
import com.podalv.utils.text.search.datastructures.ExactTextMatcher;

/** A text matcher that can match a code / any portion of the text to an ID used in the search engine
 *
 * @author podalv
 *
 */
public class AutosuggestTextMatcher {

  transient ExactTextMatcher            matcher;
  transient final TextMatcherBuilder    builder           = new TextMatcherBuilder();
  int                                   id                = 0;
  int                                   codeId            = 0;
  final IntKeyObjectMap<IntOpenHashSet> idToCodeIds       = new IntKeyObjectMap<>();
  final IntKeyOpenHashMap               codeIdsToCodes    = new IntKeyOpenHashMap();
  final HashMap<String, IntOpenHashSet> fragmentToCodeIds = new HashMap<>();

  public static AutosuggestTextMatcher load(final DataInputStream stream) throws IOException {
    final AutosuggestTextMatcher result = new AutosuggestTextMatcher();
    result.matcher = ExactTextMatcher.load(stream);
    result.id = stream.readInt();
    result.codeId = stream.readInt();
    int len = stream.readInt();
    for (int x = 0; x < len; x++) {
      final int key = stream.readInt();
      final int l = stream.readInt();
      final IntOpenHashSet set = new IntOpenHashSet();
      for (int y = 0; y < l; y++) {
        set.add(stream.readInt());
      }
      result.idToCodeIds.put(key, set);
    }
    len = stream.readInt();
    for (int x = 0; x < len; x++) {
      result.codeIdsToCodes.put(stream.readInt(), stream.readUTF());
    }
    len = stream.readInt();
    for (int x = 0; x < len; x++) {
      final String key = stream.readUTF();
      final int l = stream.readInt();
      final IntOpenHashSet set = new IntOpenHashSet();
      for (int y = 0; y < l; y++) {
        set.add(stream.readInt());
      }
      result.fragmentToCodeIds.put(key, set);
    }
    return result;
  }

  public void save(final DataOutputStream stream) throws IOException {
    matcher.save(stream);
    stream.writeInt(id);
    stream.writeInt(codeId);
    final IntKeyObjectIterator<IntOpenHashSet> iterator = idToCodeIds.entries();
    stream.writeInt(idToCodeIds.size());
    while (iterator.hasNext()) {
      iterator.next();
      stream.writeInt(iterator.getKey());
      stream.writeInt(iterator.getValue().size());
      final IntIterator values = iterator.getValue().iterator();
      while (values.hasNext()) {
        stream.writeInt(values.next());
      }
    }

    stream.writeInt(codeIdsToCodes.size());
    final IntKeyMapIterator i = codeIdsToCodes.entries();
    while (i.hasNext()) {
      i.next();
      stream.writeInt(i.getKey());
      stream.writeUTF((String) i.getValue());
    }

    stream.writeInt(fragmentToCodeIds.size());
    final Iterator<Entry<String, IntOpenHashSet>> ii = fragmentToCodeIds.entrySet().iterator();
    while (ii.hasNext()) {
      final Entry<String, IntOpenHashSet> entry = ii.next();
      stream.writeUTF(entry.getKey());
      stream.writeInt(entry.getValue().size());
    }
  }

  public int getId() {
    return id;
  }

  public void add(String text, final String code) {
    text = text.toLowerCase();
    final String[] words = TextUtils.splitWords(text);
    for (int x = 0; x < words.length; x++) {
      for (int y = words[x].length(); y >= 0; y--) {
        final String substring = words[x].substring(0, y);
        if (!substring.isEmpty()) {
          IntOpenHashSet set = fragmentToCodeIds.get(substring);
          if (set == null) {
            set = new IntOpenHashSet();
            fragmentToCodeIds.put(substring, set);
          }
          set.add(codeId);
        }
      }
    }
    codeIdsToCodes.put(codeId++, code);
  }

  public void add(String text, final IntOpenHashSet codes) {
    text = text.toLowerCase();
    final String[] words = TextUtils.splitWords(text);
    for (int x = 0; x < words.length; x++) {
      for (int y = words[x].length(); y >= 0; y--) {
        final String substring = words[x].substring(0, y);
        if (!substring.isEmpty()) {
          IntOpenHashSet set = fragmentToCodeIds.get(substring);
          if (set == null) {
            set = new IntOpenHashSet();
            fragmentToCodeIds.put(substring, set);
          }
          set.add(codeId);
        }
      }
    }
    codeIdsToCodes.put(codeId++, codes);
  }

  IntArrayList getSpaces(final String text) {
    final IntArrayList spaces = new IntArrayList();
    boolean addNext = true;
    for (int x = 0; x < text.length(); x++) {
      if (text.charAt(x) == ' ' || text.charAt(x) == ',' || text.charAt(x) == '-' || text.charAt(x) == '_' || text.charAt(x) == '.' || text.charAt(x) == '(' || text.charAt(
          x) == ')' || text.charAt(x) == ']' || text.charAt(x) == '[' || text.charAt(x) == '|' || text.charAt(x) == '/' || text.charAt(x) == '\\' || text.charAt(x) == '{'
          || text.charAt(x) == '}' || text.charAt(x) == '\'' || text.charAt(x) == '\"') {
        addNext = true;
      }
      else if (addNext) {
        spaces.add(x);
        addNext = false;
      }
    }
    return spaces;
  }

  public void addWholeString(String text, final String code) {
    text = text.toLowerCase();
    final IntArrayList spaces = getSpaces(text);
    for (int x = 0; x < spaces.size(); x++) {
      for (int y = text.length(); y >= spaces.get(x); y--) {
        final String substring = text.substring(spaces.get(x), y);
        if (!substring.isEmpty()) {
          IntOpenHashSet set = fragmentToCodeIds.get(substring);
          if (set == null) {
            set = new IntOpenHashSet();
            fragmentToCodeIds.put(substring, set);
          }
          set.add(codeId);
        }
      }
    }
    codeIdsToCodes.put(codeId++, code);
  }

  public void addWholeString(String text, final IntOpenHashSet code) {
    text = text.toLowerCase();
    for (int y = text.length(); y >= 0; y--) {
      final String substring = text.substring(0, y);
      if (!substring.isEmpty()) {
        IntOpenHashSet set = fragmentToCodeIds.get(substring);
        if (set == null) {
          set = new IntOpenHashSet();
          fragmentToCodeIds.put(substring, set);
        }
        set.add(codeId);
      }
    }
    codeIdsToCodes.put(codeId++, code);
  }

  public HashSet<String> find(final String word) {
    final HashSet<String> result = new HashSet<>();
    if (matcher != null) {
      final IntOpenHashSet idCodes = idToCodeIds.get(matcher.findExactMatch(word));
      if (idCodes != null) {
        final IntIterator i = idCodes.iterator();
        while (i.hasNext()) {
          final int codeId = i.next();
          if (codeIdsToCodes.get(codeId) instanceof String) {
            result.add((String) codeIdsToCodes.get(codeId));
          }
          else if (codeIdsToCodes.get(codeId) instanceof IntOpenHashSet) {
            final IntIterator ii = ((IntOpenHashSet) codeIdsToCodes.get(codeId)).iterator();
            while (ii.hasNext()) {
              result.add(ii.next() + "");
            }
          }
        }
      }
    }
    return result;
  }

  public void build() {
    final Iterator<Entry<String, IntOpenHashSet>> iterator = fragmentToCodeIds.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, IntOpenHashSet> entry = iterator.next();
      IntOpenHashSet set = idToCodeIds.get(id);
      if (set == null) {
        set = new IntOpenHashSet();
        idToCodeIds.put(id, set);
      }
      set.addAll(entry.getValue());
      builder.add(entry.getKey(), id++);
    }
    matcher = builder.buildExactMatching();
    fragmentToCodeIds.clear();
  }

}