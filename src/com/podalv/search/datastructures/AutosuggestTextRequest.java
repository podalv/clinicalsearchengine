package com.podalv.search.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;

/** Request for replacement of the autosuggested text with a code
 *
 * @author podalv
 *
 */
public class AutosuggestTextRequest {

  @JsonProperty("selectedCode") private String selectedCode;
  @JsonProperty("text") private String         text;
  @JsonProperty("cursor") private int          cursor;

  public int getCursor() {
    return cursor;
  }

  public String getSelectedCode() {
    return selectedCode;
  }

  public String getText() {
    return text;
  }

  public void setCursor(final int cursor) {
    this.cursor = cursor;
  }

  public void setSelectedCode(final String selectedCode) {
    this.selectedCode = selectedCode;
  }

  public void setText(final String text) {
    this.text = text;
  }
}
