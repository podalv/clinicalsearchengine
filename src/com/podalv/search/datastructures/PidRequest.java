package com.podalv.search.datastructures;

public class PidRequest extends PatientSearchRequest {

  public PidRequest getClone(final boolean binary) {
    final PidRequest result = new PidRequest(this.query);
    result.setPidCntLimit(Integer.MAX_VALUE);
    result.setReturnPids(true);
    result.setStatisticsLimit(0);
    result.setReturnSurvivalData(false);
    result.setReturnTimeIntervals(false);
    result.setBinary(binary);
    return result;
  }

  public PidRequest(final String query) {
    this.query = query;
    this.setPidCntLimit(Integer.MAX_VALUE);
    this.setReturnPids(true);
    this.setStatisticsLimit(0);
    this.setReturnSurvivalData(false);
    this.setReturnTimeIntervals(false);
  }

  @Override
  public boolean isPidRequest() {
    return true;
  }

  @Override
  public boolean isReturnPids() {
    return true;
  }

  @Override
  public boolean isReturnSurvivalData() {
    return false;
  }

  @Override
  public boolean isReturnTimeIntervals() {
    return false;
  }

}
