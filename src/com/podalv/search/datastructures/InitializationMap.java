package com.podalv.search.datastructures;

/** Indicates whether certain portions of the PatientSearchModel object were initialized
 *
 * @author podalv
 *
 */
public class InitializationMap {

  private boolean icd9                  = false;
  private boolean icd10                 = false;
  private boolean cpt                   = false;
  private boolean snomed                = false;
  private boolean department            = false;
  private boolean rx                    = false;
  private boolean positiveTerms         = false;
  private boolean negatedTerms          = false;
  private boolean familyHistoryTerms    = false;
  private boolean labs                  = false;
  private boolean atcToRxNorms          = false;
  private boolean icd9ToChildren        = false;
  private boolean icd10ToChildren       = false;
  private boolean visitTypes            = false;
  private boolean noteTypes             = false;
  private boolean uniqueIcd9Codes       = false;
  private boolean uniqueIcd10Codes      = false;
  private boolean uniqueCptCodes        = false;
  private boolean uniqueSnomedCodes     = false;
  private boolean uniqueDepartmentCodes = false;
  private boolean uniqueRxNormCodes     = false;
  private boolean ageRanges             = false;
  private boolean encounterDays         = false;
  private boolean uniqueNotePayloadIds  = false;
  private boolean years                 = false;
  private boolean vitals                = false;
  private boolean labsWithType          = false;
  private boolean labValues             = false;

  public boolean getIcd9() {
    return icd9;
  }

  public boolean getUniqueDepartmentCodes() {
    return uniqueDepartmentCodes;
  }

  public boolean getIcd10() {
    return icd10;
  }

  public boolean getDepartment() {
    return department;
  }

  public boolean getCpt() {
    return cpt;
  }

  public boolean getSnomed() {
    return snomed;
  }

  public boolean getRx() {
    return rx;
  }

  public boolean getLabValues() {
    return labValues;
  }

  public boolean getPositiveTerms() {
    return positiveTerms;
  }

  public boolean getNegatedTerms() {
    return negatedTerms;
  }

  public boolean getFamilyHistoryTerms() {
    return familyHistoryTerms;
  }

  public boolean getLabs() {
    return labs;
  }

  public boolean getAtcToRxNorms() {
    return atcToRxNorms;
  }

  public boolean getIcd9ToChildren() {
    return icd9ToChildren;
  }

  public boolean getIcd10ToChildren() {
    return icd10ToChildren;
  }

  public boolean getVisitTypes() {
    return visitTypes;
  }

  public boolean getNoteTypes() {
    return noteTypes;
  }

  public boolean getUniqueIcd9Codes() {
    return uniqueIcd9Codes;
  }

  public boolean getUniqueIcd10Codes() {
    return uniqueIcd10Codes;
  }

  public boolean getUniqueCptCodes() {
    return uniqueCptCodes;
  }

  public boolean getUniqueSnomedCodes() {
    return uniqueSnomedCodes;
  }

  public boolean getUniqueRxNormCodes() {
    return uniqueRxNormCodes;
  }

  public boolean getAgeRanges() {
    return ageRanges;
  }

  public boolean getEncounterDays() {
    return encounterDays;
  }

  public boolean getUniqueNotePayloadIds() {
    return uniqueNotePayloadIds;
  }

  public boolean getYears() {
    return years;
  }

  public boolean getVitals() {
    return vitals;
  }

  public boolean getLabsWithType() {
    return labsWithType;
  }

  public void initIcd9() {
    icd9 = true;
  }

  public void initIcd10() {
    icd10 = true;
  }

  public void initCpt() {
    cpt = true;
  }

  public void initSnomed() {
    snomed = true;
  }

  public void initRx() {
    rx = true;
  }

  public void initDepartment() {
    department = true;
  }

  public void initPositiveTerms() {
    positiveTerms = true;
  }

  public void initNegatedTerms() {
    negatedTerms = true;
  }

  public void initFamilyHistoryTerms() {
    familyHistoryTerms = true;
  }

  public void initLabs() {
    labs = true;
  }

  public void initAtcToRxNorms() {
    atcToRxNorms = true;
  }

  public void initIcd9ToChildren() {
    icd9ToChildren = true;
  }

  public void initIcd10ToChildren() {
    icd10ToChildren = true;
  }

  public void initVisitTypes() {
    visitTypes = true;
  }

  public void initLabValues() {
    labValues = true;
  }

  public void initNoteTypes() {
    noteTypes = true;
  }

  public void initUniqueIcd9Codes() {
    uniqueIcd9Codes = true;
  }

  public void initUniqueIcd10Codes() {
    uniqueIcd10Codes = true;
  }

  public void initUniqueCptCodes() {
    uniqueCptCodes = true;
  }

  public void initUniqueSnomedCodes() {
    uniqueSnomedCodes = true;
  }

  public void initUniqueDepartmentCodes() {
    uniqueDepartmentCodes = true;
  }

  public void initUniqueRxNormCodes() {
    uniqueRxNormCodes = true;
  }

  public void initAgeRanges() {
    ageRanges = true;
  }

  public void initEncounterDays() {
    encounterDays = true;
  }

  public void initUniqueNotePayloadIds() {
    uniqueNotePayloadIds = true;
  }

  public void initYears() {
    years = true;
  }

  public void initVitals() {
    vitals = true;
  }

  public void initLabsWithType() {
    labsWithType = true;
  }

  public void clearAll() {
    icd9 = false;
    icd10 = false;
    cpt = false;
    snomed = false;
    rx = false;
    positiveTerms = false;
    negatedTerms = false;
    familyHistoryTerms = false;
    labs = false;
    atcToRxNorms = false;
    icd9ToChildren = false;
    icd10ToChildren = false;
    visitTypes = false;
    labValues = false;
    noteTypes = false;
    uniqueIcd9Codes = false;
    uniqueIcd10Codes = false;
    uniqueCptCodes = false;
    uniqueSnomedCodes = false;
    uniqueRxNormCodes = false;
    ageRanges = false;
    encounterDays = false;
    uniqueNotePayloadIds = false;
    years = false;
    vitals = false;
    department = false;
    uniqueDepartmentCodes = false;
    labsWithType = false;
  }
}