package com.podalv.search.datastructures;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;
import com.podalv.extractor.stride6.Common;
import com.podalv.input.Input;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.objectdb.SearchModel;
import com.podalv.objectdb.datastructures.CompressedOffHeapIntKeyIntArrayMap;
import com.podalv.objectdb.datastructures.OffHeapIntArrayList;
import com.podalv.objectdb.datastructures.OffHeapIntArrayListAutomated;
import com.podalv.output.Output;
import com.podalv.search.index.IndexCollection;

/** A super fast memory efficient patient model
 *
 * @author podalv
 *
 */
public class PatientSearchModel extends SearchModel implements Patient, ObjectWithPayload {

  public static final int                    OFFSET_ENCODING_OFFSET_SIZE_IN_BYTES = Common.BYTE_SIZE_IN_BYTES;
  public static final int                    START_TIME_SIZE_IN_BYTES             = Common.INT_SIZE_IN_BYTES;
  public static final int                    END_TIME_SIZE_IN_BYTES               = Common.INT_SIZE_IN_BYTES;

  private static final int                   OFFSET_ENCODING_OFFSET               = 0;

  private static final int                   ICD9_OFFSET                          = 0;
  private static final int                   ICD10_OFFSET                         = 1;
  private static final int                   CPT_OFFSET                           = 2;
  private static final int                   RXNORM_OFFSET                        = 3;
  private static final int                   POSITIVE_OFFSET                      = 4;
  private static final int                   NEGATED_OFFSET                       = 5;
  private static final int                   FAMILY_HISTORY_OFFSET                = 6;
  private static final int                   UNIQUE_ICD9_CODES_OFFSET             = 7;
  private static final int                   UNIQUE_ICD10_CODES_OFFSET            = 8;
  private static final int                   UNIQUE_CPT_CODES_OFFSET              = 9;
  private static final int                   UNIQUE_RXNORM_CODES_OFFSET           = 10;
  private static final int                   AGES_OFFSET                          = 11;
  private static final int                   ENCOUNTER_DAYS_OFFSET                = 12;
  private static final int                   YEAR_STARTS_OFFSET                   = 13;
  private static final int                   YEARS_OFFSET                         = 14;
  private static final int                   VITALS_OFFSET                        = 15;
  private static final int                   LAB_VALUES_OFFSET                    = 16;
  private static final int                   LABS_WITH_TYPE_OFFSET                = 17;
  private static final int                   LABS_OFFSET                          = 18;
  private static final int                   ATC_OFFSET                           = 19;
  private static final int                   UNIQUE_NOTES_OFFSET                  = 20;
  private static final int                   VISIT_TYPES_OFFSET                   = 21;
  private static final int                   NOTE_TYPES_OFFSET                    = 22;
  private static final int                   SNOMED_OFFSET                        = 23;
  private static final int                   SNOMED_CODES_OFFSET                  = 24;
  private static final int                   DEPARTMENT_OFFSET                    = 25;
  private static final int                   DEPARTMENT_CODES_OFFSET              = 26;

  // Additional map goes here

  private static final int                   PAYLOAD_OFFSET                       = DEPARTMENT_CODES_OFFSET + 1;
  private byte                               offsetEncodingType                   = 0;
  private long                               position;
  private int                                startTime                            = -1;
  private int                                endTime                              = -1;
  private CompressedOffHeapIntKeyIntArrayMap icd9;
  private CompressedOffHeapIntKeyIntArrayMap icd10;
  private CompressedOffHeapIntKeyIntArrayMap cpt;
  private CompressedOffHeapIntKeyIntArrayMap snomed;
  private CompressedOffHeapIntKeyIntArrayMap department;
  private CompressedOffHeapIntKeyIntArrayMap rx;
  private CompressedOffHeapIntKeyIntArrayMap payload;
  private CompressedOffHeapIntKeyIntArrayMap positiveTerms;
  private CompressedOffHeapIntKeyIntArrayMap negatedTerms;
  private CompressedOffHeapIntKeyIntArrayMap familyHistoryTerms;
  private CompressedOffHeapIntKeyIntArrayMap labs;
  private CompressedOffHeapIntKeyIntArrayMap atcToRxNorms;
  private CompressedOffHeapIntKeyIntArrayMap visitTypes;
  private CompressedOffHeapIntKeyIntArrayMap noteTypes;
  private OffHeapIntArrayListAutomated       uniqueIcd9Codes;
  private OffHeapIntArrayListAutomated       uniqueIcd10Codes;
  private OffHeapIntArrayListAutomated       uniqueCptCodes;
  private OffHeapIntArrayListAutomated       uniqueSnomedCodes;
  private OffHeapIntArrayListAutomated       uniqueDepartmentCodes;
  private OffHeapIntArrayListAutomated       uniqueRxNormCodes;
  private OffHeapIntArrayListAutomated       ageRanges;
  private OffHeapIntArrayListAutomated       encounterDays;
  private OffHeapIntArrayListAutomated       uniqueNotePayloadIds;
  private CompressedOffHeapIntKeyIntArrayMap years;
  private MeasurementOffHeapData             vitals;
  private MeasurementOffHeapData             labValues;
  private LabsOffHeapData                    labsWithType;
  private int                                patientId;
  private final IndexCollection              indices;
  private final InitializationMap            initMap                              = new InitializationMap();

  public PatientSearchModel(final IndexCollection indices, final OffHeapMemory memory, final int patientId) {
    setMemory(memory);
    this.patientId = patientId;
    this.indices = indices;
  }

  @Override
  public int getId() {
    return patientId;
  }

  private CompressedOffHeapIntKeyIntArrayMap readMap(CompressedOffHeapIntKeyIntArrayMap map, final long position, final int offset) {
    if (map == null) {
      if (offset < 0) {
        map = new CompressedOffHeapIntKeyIntArrayMap(memory, offset);
      }
      else {
        map = new CompressedOffHeapIntKeyIntArrayMap(memory, position + offset);
      }
    }
    else {
      map.setMemory(memory);
      if (offset < 0) {
        map.setPosition(offset);
      }
      else {
        map.setPosition(position + offset);
      }
    }
    return map;
  }

  @Override
  public boolean containsLabs() {
    initLabs();
    return !labs.isEmpty();
  }

  @Override
  public int[] getUniqueVisitTypes() {
    initVisitTypes();
    final IntOpenHashSet result = new IntOpenHashSet();
    for (int x = 0; x < visitTypes.getKeySize(); x++) {
      if (visitTypes.readState(x) == IntKeyIntOpenHashMap.OCCUPIED) {
        result.add(visitTypes.readKey(x));
      }
    }
    return result.toArray();
  }

  private OffHeapIntArrayListAutomated readList(OffHeapIntArrayListAutomated list, final long position, final int offset) {
    if (list == null) {
      if (offset < 0) {
        list = new OffHeapIntArrayListAutomated(memory, offset);
      }
      else {
        list = new OffHeapIntArrayListAutomated(memory, position + offset);
      }
    }
    else {
      list.setMemory(memory);
      if (offset < 0) {
        list.setPosition(offset);
      }
      else {
        list.setPosition(position + offset);
      }
    }
    return list;
  }

  public static int headerSizeInBytes() {
    return OFFSET_ENCODING_OFFSET_SIZE_IN_BYTES + START_TIME_SIZE_IN_BYTES + END_TIME_SIZE_IN_BYTES;
  }

  private int calculateOffset(final int offsetPos) {
    if (offsetEncodingType == PatientBuilder.OFFSET_ENCODING_TYPE_BYTE) {
      return OFFSET_ENCODING_OFFSET + headerSizeInBytes() + (offsetPos);
    }
    else if (offsetEncodingType == PatientBuilder.OFFSET_ENCODING_TYPE_SHORT) {
      return OFFSET_ENCODING_OFFSET + headerSizeInBytes() + (offsetPos * 2);
    }
    else {
      return OFFSET_ENCODING_OFFSET + headerSizeInBytes() + (offsetPos * 4);
    }
  }

  private int getOffset(final long position, final int offsetPos) {
    if (offsetEncodingType == PatientBuilder.OFFSET_ENCODING_TYPE_BYTE) {
      final short result = memory.getUnsignedByte(position + calculateOffset(offsetPos));
      if (result == Byte.MAX_VALUE * 2) {
        return -1;
      }
      return result;
    }
    else if (offsetEncodingType == PatientBuilder.OFFSET_ENCODING_TYPE_SHORT) {
      final int result = memory.getUnsignedShort(position + calculateOffset(offsetPos));
      if (result == Short.MAX_VALUE * 2) {
        return -1;
      }
      return result;
    }
    else {
      return memory.getInt(position + calculateOffset(offsetPos));
    }
  }

  private void initIcd9() {
    if (!initMap.getIcd9()) {
      icd9 = readMap(icd9, position, getOffset(position, ICD9_OFFSET));
      initMap.initIcd9();
    }
  }

  private void initDepartment() {
    if (!initMap.getDepartment()) {
      department = readMap(department, position, getOffset(position, DEPARTMENT_OFFSET));
      initMap.initDepartment();
    }
  }

  private void initIcd10() {
    if (!initMap.getIcd10()) {
      icd10 = readMap(icd10, position, getOffset(position, ICD10_OFFSET));
      initMap.initIcd10();
    }
  }

  private void initCpt() {
    if (!initMap.getCpt()) {
      cpt = readMap(cpt, position, getOffset(position, CPT_OFFSET));
      initMap.initCpt();
    }
  }

  private void initSnomed() {
    if (!initMap.getSnomed()) {
      snomed = readMap(snomed, position, getOffset(position, SNOMED_OFFSET));
      initMap.initSnomed();
    }
  }

  private void initRx() {
    if (!initMap.getRx()) {
      rx = readMap(rx, position, getOffset(position, RXNORM_OFFSET));
      initMap.initRx();
    }
  }

  private void initPositiveTerms() {
    if (!initMap.getPositiveTerms()) {
      positiveTerms = readMap(positiveTerms, position, getOffset(position, POSITIVE_OFFSET));
      initMap.initPositiveTerms();
    }
  }

  private void initNegatedTerms() {
    if (!initMap.getNegatedTerms()) {
      negatedTerms = readMap(negatedTerms, position, getOffset(position, NEGATED_OFFSET));
      initMap.initNegatedTerms();
    }
  }

  private void initFamilyHistoryTerms() {
    if (!initMap.getFamilyHistoryTerms()) {
      familyHistoryTerms = readMap(familyHistoryTerms, position, getOffset(position, FAMILY_HISTORY_OFFSET));
      initMap.initFamilyHistoryTerms();
    }
  }

  private void initUniqueIcd9Codes() {
    if (!initMap.getUniqueIcd9Codes()) {
      uniqueIcd9Codes = readList(uniqueIcd9Codes, position, getOffset(position, UNIQUE_ICD9_CODES_OFFSET));
      initMap.initUniqueIcd9Codes();
    }
  }

  private void initUniqueIcd10Codes() {
    if (!initMap.getUniqueIcd10Codes()) {
      uniqueIcd10Codes = readList(uniqueIcd10Codes, position, getOffset(position, UNIQUE_ICD10_CODES_OFFSET));
      initMap.initUniqueIcd10Codes();
    }
  }

  private void initUniqueSnomedCodes() {
    if (!initMap.getUniqueSnomedCodes()) {
      uniqueSnomedCodes = readList(uniqueSnomedCodes, position, getOffset(position, SNOMED_CODES_OFFSET));
      initMap.initUniqueSnomedCodes();
    }
  }

  private void initUniqueDepartmentCodes() {
    if (!initMap.getUniqueDepartmentCodes()) {
      uniqueDepartmentCodes = readList(uniqueDepartmentCodes, position, getOffset(position, DEPARTMENT_CODES_OFFSET));
      initMap.initUniqueDepartmentCodes();
    }
  }

  private void initUniqueCptCodes() {
    if (!initMap.getUniqueCptCodes()) {
      uniqueCptCodes = readList(uniqueCptCodes, position, getOffset(position, UNIQUE_CPT_CODES_OFFSET));
      initMap.initUniqueCptCodes();
    }
  }

  private void initUniqueRxNormCodes() {
    if (!initMap.getUniqueRxNormCodes()) {
      uniqueRxNormCodes = readList(uniqueRxNormCodes, position, getOffset(position, UNIQUE_RXNORM_CODES_OFFSET));
      initMap.initUniqueRxNormCodes();
    }
  }

  private void initAgeRanges() {
    if (!initMap.getAgeRanges()) {
      ageRanges = readList(ageRanges, position, getOffset(position, AGES_OFFSET));
      initMap.initAgeRanges();
    }
  }

  private void initEncounterDays() {
    if (!initMap.getEncounterDays()) {
      encounterDays = readList(encounterDays, position, getOffset(position, ENCOUNTER_DAYS_OFFSET));
      initMap.initEncounterDays();
    }
  }

  private void initYears() {
    if (!initMap.getYears()) {
      years = readMap(years, position, getOffset(position, YEAR_STARTS_OFFSET));
      initMap.initYears();
    }
  }

  private void initVitals() {
    if (!initMap.getVitals()) {
      final int pos = getOffset(position, VITALS_OFFSET);
      if (vitals == null) {
        if (pos < 0) {
          vitals = new MeasurementOffHeapData(null, memory, new MeasurementNameProvider() {

            @Override
            public String getName(final int id) {
              return indices.getVitalsName(id);
            }
          }, DATA_TYPE.BYTE, pos);
        }
        else {
          vitals = new MeasurementOffHeapData(null, memory, new MeasurementNameProvider() {

            @Override
            public String getName(final int id) {
              return indices.getVitalsName(id);
            }
          }, DATA_TYPE.BYTE, position + pos);
        }
      }
      else {
        vitals.setMemory(memory, null);
        if (pos < 0) {
          vitals.setPosition(pos, null);
        }
        else {
          vitals.setPosition(position + pos, null);
        }
      }
      initMap.initVitals();
    }
  }

  private void initLabValues() {
    if (!initMap.getLabValues()) {
      final int pos = getOffset(position, LAB_VALUES_OFFSET);
      if (labValues == null) {
        if (pos < 0) {
          labValues = new MeasurementOffHeapData(this, memory, new MeasurementNameProvider() {

            @Override
            public String getName(final int id) {
              return indices.getLabName(id);
            }
          }, DATA_TYPE.SHORT, pos);
        }
        else {
          labValues = new MeasurementOffHeapData(this, memory, new MeasurementNameProvider() {

            @Override
            public String getName(final int id) {
              return indices.getLabName(id);
            }
          }, DATA_TYPE.SHORT, position + pos);
        }
      }
      else {
        labValues.setMemory(memory, this);
        if (pos < 0) {
          labValues.setPosition(pos, this);
        }
        else {
          labValues.setPosition(position + pos, this);
        }
      }
      initMap.initLabValues();
    }
  }

  private void initLabsWithType() {
    if (!initMap.getLabsWithType()) {
      final int pos = getOffset(position, LABS_WITH_TYPE_OFFSET);
      if (labsWithType == null) {
        if (pos < 0) {
          labsWithType = new LabsOffHeapData(memory, pos, this, Common.calculateCompressionType(payload.size()));
        }
        else {
          labsWithType = new LabsOffHeapData(memory, position + pos, this, Common.calculateCompressionType(payload.size()));
        }
      }
      else {
        labsWithType.setMemory(memory, Common.calculateCompressionType(payload.size()));
        if (pos < 0) {
          labsWithType.setPosition(pos, Common.calculateCompressionType(payload.size()));
        }
        else {
          labsWithType.setPosition(position + pos, Common.calculateCompressionType(payload.size()));
        }
      }
      initMap.initLabsWithType();
    }
  }

  private void initLabs() {
    if (!initMap.getLabs()) {
      labs = readMap(labs, position, getOffset(position, LABS_OFFSET));
      initMap.initLabs();
    }
  }

  private void initAtcToRxNorms() {
    if (!initMap.getAtcToRxNorms()) {
      atcToRxNorms = readMap(atcToRxNorms, position, getOffset(position, ATC_OFFSET));
      initMap.initAtcToRxNorms();
    }
  }

  private void initUniqueNotePayloadIds() {
    if (!initMap.getUniqueNotePayloadIds()) {
      uniqueNotePayloadIds = readList(uniqueNotePayloadIds, position, getOffset(position, UNIQUE_NOTES_OFFSET));
      initMap.initUniqueNotePayloadIds();
    }
  }

  private void initVisitTypes() {
    if (!initMap.getVisitTypes()) {
      visitTypes = readMap(visitTypes, position, getOffset(position, VISIT_TYPES_OFFSET));
      initMap.initVisitTypes();
    }
  }

  private void initNoteTypes() {
    if (!initMap.getNoteTypes()) {
      noteTypes = readMap(noteTypes, position, getOffset(position, NOTE_TYPES_OFFSET));
      initMap.initNoteTypes();
    }
  }

  @Override
  public void setPosition(final long position) {
    this.position = position;
    final byte offsetEncodingTypeLocal = memory.getByte(position + OFFSET_ENCODING_OFFSET);
    startTime = memory.getInt(position + OFFSET_ENCODING_OFFSET + 1);
    endTime = memory.getInt(position + OFFSET_ENCODING_OFFSET + 1 + 4);
    if (offsetEncodingTypeLocal >= PatientBuilder.PAYLOAD_IS_EMPTY) {
      this.offsetEncodingType = (byte) (offsetEncodingTypeLocal - PatientBuilder.PAYLOAD_IS_EMPTY);
      payload = readMap(payload, position, -1);
    }
    else {
      this.offsetEncodingType = offsetEncodingTypeLocal;
      payload = readMap(payload, position, calculateOffset(PAYLOAD_OFFSET));
    }
    initMap.clearAll();
  }

  @Override
  public MeasurementData getVitals() {
    initVitals();
    return vitals;
  }

  @Override
  public MeasurementData getLabValues() {
    initLabValues();
    return labValues;
  }

  @Override
  public IntArrayList getPayload(final int id) {
    return payload.get(id);
  }

  @Override
  public IntArrayList getIcd9(final int icd9Code) {
    initIcd9();
    return icd9.get((icd9Code));
  }

  @Override
  public IntArrayList getIcd10(final int icd10Code) {
    initIcd10();
    return icd10.get((icd10Code));
  }

  @Override
  public boolean containsIcd9(final int icd9Code) {
    initIcd9();
    return icd9.containsKey(icd9Code);
  }

  @Override
  public boolean containsIcd10(final int icd10Code) {
    initIcd10();
    return icd10.containsKey(icd10Code);
  }

  @Override
  public IntArrayList getPositiveTerms(final int termId) {
    initPositiveTerms();
    return positiveTerms.get((termId));
  }

  @Override
  public IntArrayList getNegatedTerms(final int termId) {
    initNegatedTerms();
    return negatedTerms.get((termId));
  }

  @Override
  public IntArrayList getFamilyHistoryTerms(final int termId) {
    initFamilyHistoryTerms();
    return familyHistoryTerms.get((termId));
  }

  @Override
  public void setId(final int id) {
    patientId = id;
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public void save(final Output output) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isEmpty() {
    throw new UnsupportedOperationException();
  }

  @Override
  public IntArrayList getUniqueIcd9Codes() {
    initUniqueIcd9Codes();
    return uniqueIcd9Codes;
  }

  @Override
  public IntArrayList getUniqueIcd10Codes() {
    initUniqueIcd10Codes();
    return uniqueIcd10Codes;
  }

  @Override
  public short getMinYear() {
    final int year = memory.getUnsignedByte(getYearsPos() + 1);
    return year == 0 ? 0 : (short) (year + 1900);
  }

  @Override
  public short getMaxYear() {
    final int year = memory.getUnsignedByte(getYearsPos() + memory.getUnsignedByte(getYearsPos()));
    return year == 0 ? 0 : (short) (year + 1900);
  }

  private long getYearsPos() {
    return position + getOffset(position, YEARS_OFFSET);
  }

  @Override
  public int getYear(final int time) {
    initYears();
    for (int x = getMinYear(); x <= getMaxYear(); x++) {
      final OffHeapIntArrayList startEnd = (OffHeapIntArrayList) years.get(x);
      if (startEnd != null) {
        for (int y = 0; y < startEnd.size(); y += 2) {
          if (startEnd.get(y) <= time && startEnd.get(y + 1) >= time) {
            return x;
          }
        }
      }
    }
    return -1;
  }

  public HashMap<Integer, ArrayList<Integer>> getAllYearData() {
    initYears();
    final HashMap<Integer, ArrayList<Integer>> result = new HashMap<>();
    for (int x = getMinYear(); x <= getMaxYear(); x++) {
      final OffHeapIntArrayList startEnd = (OffHeapIntArrayList) years.get(x);
      if (startEnd != null) {
        final ArrayList<Integer> list = new ArrayList<>();
        for (int y = 0; y < startEnd.size(); y += 2) {
          list.add(startEnd.get(y));
          list.add(startEnd.get(y + 1));
        }
        result.put(x, list);
      }
    }
    return result;
  }

  @Override
  public int[] getYear(final int minYear, final int maxYear) {
    initYears();
    final int[] result = new int[] {-1, -1};
    if (getMinYear() == 0 || getMaxYear() == 0) {
      return null;
    }
    if (maxYear < getMinYear() || minYear > getMaxYear()) {
      return null;
    }
    if (minYear <= 0 || minYear <= (getMinYear())) {
      result[0] = startTime;
    }
    else {
      OffHeapIntArrayList startEnd = (OffHeapIntArrayList) years.get(minYear);
      if (startEnd == null) {
        final short len = memory.getUnsignedByte(getYearsPos());
        for (int x = 0; x < len; x++) {
          final int curYear = 1900 + memory.getUnsignedByte(getYearsPos() + 1 + x);
          if (curYear >= minYear && curYear <= maxYear) {
            startEnd = (OffHeapIntArrayList) years.get(1900 + memory.getUnsignedByte(getYearsPos() + 1 + x));
            if (startEnd != null) {
              result[0] = startEnd.get(0) + 1;
              break;
            }
          }
        }
      }
      else {
        result[0] = startEnd.get(0);
      }
    }
    if (result[0] == -1) {
      return null;
    }
    if (maxYear == Integer.MAX_VALUE || maxYear >= (getMaxYear())) {
      result[1] = endTime;
    }
    else {
      OffHeapIntArrayList startEnd = (OffHeapIntArrayList) years.get(maxYear);
      if (startEnd == null) {
        final short len = memory.getUnsignedByte(getYearsPos());
        for (int x = len - 1; x >= 0; x--) {
          final int curYear = 1900 + memory.getUnsignedByte(getYearsPos() + 1 + x);
          if (curYear >= minYear && curYear <= maxYear) {
            startEnd = (OffHeapIntArrayList) years.get(1900 + memory.getUnsignedByte(getYearsPos() + 1 + x));
            if (startEnd != null) {
              result[1] = startEnd.get(1);
              break;
            }
          }
        }
      }
      else {
        result[1] = startEnd.get(1);
      }
    }
    return result[0] == -1 || result[1] == -1 ? null : result;
  }

  @Override
  public IntArrayList getCpt(final int cptCode) {
    initCpt();
    return cpt.get((cptCode));
  }

  @Override
  public IntArrayList getUniqueCptCodes() {
    initUniqueCptCodes();
    return uniqueCptCodes;
  }

  @Override
  public boolean containsCpt(final int cptCode) {
    initCpt();
    return cpt.containsKey(cptCode);
  }

  @Override
  public boolean containsPositiveTid(final int tid) {
    initPositiveTerms();
    return positiveTerms.containsKey(tid);
  }

  @Override
  public boolean containsNegatedTid(final int tid) {
    initNegatedTerms();
    return negatedTerms.containsKey(tid);
  }

  @Override
  public boolean containsFamilyHistoryTid(final int tid) {
    initFamilyHistoryTerms();
    return familyHistoryTerms.containsKey(tid);
  }

  @Override
  public IntArrayList getAgeRanges() {
    initAgeRanges();
    return ageRanges;
  }

  @Override
  public IntArrayList getRxNorm(final int rxNormCode) {
    initRx();
    return rx.get(rxNormCode);
  }

  @Override
  public boolean containsRxNorm(final int rxNormCode) {
    initRx();
    return rx.containsKey(rxNormCode);
  }

  @Override
  public IntArrayList getUniqueRxNormCodes() {
    initUniqueRxNormCodes();
    return uniqueRxNormCodes;
  }

  @Override
  public IntArrayList getEncounterDays() {
    initEncounterDays();
    return encounterDays;
  }

  @Override
  public LabsOffHeapData getLabs() {
    initLabsWithType();
    return labsWithType;
  }

  @Override
  public int[] getUniqueLabIds() {
    initLabs();
    final int keySize = labs.getKeySize();
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < keySize; x++) {
      if (labs.isKeyIndexOccupied(x)) {
        result.add(labs.readKey(x));
      }
    }
    return result.toArray();
  }

  @Override
  public IntArrayList getLabTimes(final int labId) {
    initLabs();
    return labs.get((labId));
  }

  @Override
  public IntArrayList getRxNormCodesFromAtc(final int atc) {
    initAtcToRxNorms();
    final Object result = atcToRxNorms.get(atc);
    return result == null ? null : (IntArrayList) result;
  }

  @Override
  public IntArrayList getUniqueNotePayloadIds() {
    initUniqueNotePayloadIds();
    return uniqueNotePayloadIds;
  }

  @Override
  public boolean containsVisitTypes() {
    return visitTypes.size() != 0;
  }

  @Override
  public IntArrayList getVisitTypes(final int type) {
    initVisitTypes();
    return visitTypes.get(type);
  }

  @Override
  public boolean containsVisitType(final int visitTypeId) {
    initVisitTypes();
    return visitTypes.containsKey(visitTypeId);
  }

  @Override
  public IntArrayList getNoteTimePointsFromNoteType(final int noteTypeId) {
    initNoteTypes();
    final Object result = noteTypes.get(noteTypeId);
    return result == null ? null : (IntArrayList) result;
  }

  @Override
  public IntArrayList getSnomed(final int snomedCode) {
    initSnomed();
    return snomed.get((snomedCode));
  }

  @Override
  public IntArrayList getUniqueSnomedCodes() {
    initUniqueSnomedCodes();
    return uniqueSnomedCodes;
  }

  @Override
  public boolean containsSnomed(final int snomedCode) {
    initSnomed();
    return snomed.containsKey(snomedCode);
  }

  @Override
  public int[] getUniquePositiveTids() {
    initPositiveTerms();
    final IntArrayList result = new IntArrayList();
    if (positiveTerms.size() != 0) {
      final int keySize = positiveTerms.getKeySize();
      for (int x = 0; x < keySize; x++) {
        if (positiveTerms.isKeyIndexOccupied(x)) {
          result.add(positiveTerms.readKey(x));
        }
      }
    }
    return result.toArray();
  }

  @Override
  public int[] getUniqueNegatedTids() {
    initNegatedTerms();
    final IntArrayList result = new IntArrayList();
    if (negatedTerms.size() != 0) {
      final int keySize = negatedTerms.getKeySize();
      for (int x = 0; x < keySize; x++) {
        if (negatedTerms.isKeyIndexOccupied(x)) {
          result.add(negatedTerms.readKey(x));
        }
      }
    }
    return result.toArray();
  }

  @Override
  public int[] getUniqueFamilyHistoryTids() {
    initFamilyHistoryTerms();
    final IntArrayList result = new IntArrayList();
    if (familyHistoryTerms.size() != 0) {
      final int keySize = familyHistoryTerms.getKeySize();
      for (int x = 0; x < keySize; x++) {
        if (familyHistoryTerms.isKeyIndexOccupied(x)) {
          result.add(familyHistoryTerms.readKey(x));
        }
      }
    }
    return result.toArray();
  }

  @Override
  public String toString() {
    if (memory != null) {
      final StringBuilder result = new StringBuilder("PID=" + patientId);
      return result.toString();
    }
    return "???";
  }

  @Override
  public boolean isClosed() {
    return true;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }

  @Override
  public int getStartTime() {
    return startTime;
  }

  @Override
  public int getEndTime() {
    return endTime;
  }

  @Override
  public boolean containsDepartment(final int departmentCode) {
    initDepartment();
    return department.containsKey(departmentCode);
  }

  @Override
  public IntArrayList getDepartment(final int departmentId) {
    initDepartment();
    return department.get((departmentId));
  }

  @Override
  public IntArrayList getUniqueDepartmentCodes() {
    initUniqueDepartmentCodes();
    return uniqueDepartmentCodes;
  }

  @Override
  public boolean containsFamilyHistoryTerms() {
    initFamilyHistoryTerms();
    return familyHistoryTerms.size() != 0;
  }

  @Override
  public boolean containsNegatedTerms() {
    initNegatedTerms();
    return negatedTerms.size() != 0;
  }

  @Override
  public boolean containsPositiveTerms() {
    initPositiveTerms();
    return positiveTerms.size() != 0;
  }
}