package com.podalv.search.datastructures;

/** Allows retrieving names for measurements
 *
 * @author podalv
 *
 */
public abstract class MeasurementNameProvider {

  public abstract String getName(int id);

}
