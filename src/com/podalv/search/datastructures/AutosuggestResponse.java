package com.podalv.search.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AutosuggestResponse {

  public static AutosuggestResponse                EMPTY     = new AutosuggestResponse();

  @JsonProperty("response") private final String[] response;
  @JsonProperty("remaining") private int           remaining = 0;

  public AutosuggestResponse(final String ... response) {
    this.response = response;
  }

  public AutosuggestResponse setRemainig(final int remainig) {
    this.remaining = remainig;
    return this;
  }

  public int getRemainig() {
    return remaining;
  }

  public String[] getResponse() {
    return this.response;
  }

}
