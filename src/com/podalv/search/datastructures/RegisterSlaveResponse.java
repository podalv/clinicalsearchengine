package com.podalv.search.datastructures;

public class RegisterSlaveResponse {

  private final boolean      success;
  private final String       error;
  public static final String ERROR_SLAVE_ALREADY_ATTACHED = "Slave already attached";
  public static final String ERROR_INVALID_GROUP_ID       = "Invalid slave group id";
  public static final String ERROR_NOT_MASTER             = "This server is not MASTER and does not support slaves";

  public RegisterSlaveResponse(final boolean success, final String error) {
    this.success = success;
    this.error = error;
  }

  public boolean isSuccess() {
    return success;
  }

  public String getError() {
    return error;
  }
}
