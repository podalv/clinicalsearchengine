package com.podalv.search.datastructures;

import static com.podalv.utils.Logging.exception;
import static com.podalv.utils.Logging.info;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.podalv.input.Input;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.output.StreamOutput;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.ErrorMessages;
import com.podalv.search.server.PatientSearchManager;
import com.podalv.search.server.QueryUtils;
import com.podalv.utils.arrays.ArrayUtils;
import com.podalv.utils.datastructures.LongMutable;

/** Encapsulates the search result
 *
 * @author podalv
 *
 */
public class PatientSearchResponse extends SerializableResponse implements PersistentObject {

  @JsonProperty("patientIds") private LinkedList<double[]>                  patientIds         = new LinkedList<>();
  @JsonProperty("originalUnparsedQuery") private String                     originalUnparsedQuery;
  @JsonProperty("timeTook") private long                                    timeTook;
  @JsonProperty("parsedQuery") private String                               parsedQuery;
  @JsonProperty("exportLocation") private String                            exportLocation;
  private transient final int                                               queryId;
  private transient int[]                                                   genderStatistics;
  private transient int[]                                                   raceStatistics;
  @JsonProperty("cohortPatientCnt") private int                             cohortPatientCnt   = 0;
  @JsonProperty("generalPatientCnt") private int                            generalPatientCnt  = 0;
  @JsonProperty("skippedPatientCnt") private int                            skippedPatientCnt  = 0;
  @JsonProperty("processedPatients") private int                            processedPatients  = 0;
  @JsonProperty("deaths") private ArrayList<long[]>                         deaths;
  @JsonProperty("censored") private ArrayList<long[]>                       censored;
  @JsonProperty("genderHistogram") private ArrayList<DemographicsHistogram> genderHistogram    = new ArrayList<>();
  @JsonProperty("raceHistogram") private ArrayList<DemographicsHistogram>   raceHistogram      = new ArrayList<>();
  private transient int[]                                                   icd9StatisticsTransientMap;
  private transient int[]                                                   icd10StatisticsTransientMap;
  @JsonProperty("icd9Statistics") private ArrayList<Histogram>              icd9Statistics     = new ArrayList<>();
  private transient int[]                                                   cptStatisticsTransientMap;
  @JsonProperty("cptStatistics") private ArrayList<Histogram>               cptStatistics      = new ArrayList<>();
  private transient final int[]                                             rxStatisticsTransientMap;
  @JsonProperty("rxStatistics") private ArrayList<Histogram>                rxStatistics       = new ArrayList<>();
  private transient final int[]                                             labsStatisticsTransientMap;
  @JsonProperty("labsStatistics") private ArrayList<Histogram>              labsStatistics     = new ArrayList<>();

  @JsonProperty("encounters") private int[]                                 encounters;
  @JsonProperty("durations") private int[]                                  durations;

  private transient int[]                                                   encountersBuckets;
  private transient int[]                                                   durationsBuckets;

  private transient final IndexCollection                                   indices;
  private transient final UmlsDictionary                                    umls;
  private transient final Statistics                                        statistics;
  @JsonProperty("errorMessage") String                                      errorMessage       = null;
  @JsonProperty("warningMessage") private String[]                          warningMessage     = null;
  private transient int                                                     MAX_PATIENT_ID_CNT = Integer.MAX_VALUE;
  @JsonProperty("ages") private ArrayList<Histogram>                        ages               = new ArrayList<>();
  private transient int                                                     patientsWithLabs   = 0;
  private transient int                                                     patientsWithDrugs  = 0;
  private transient int                                                     patientsWithIcd9   = 0;
  private transient int                                                     patientsWithCpt    = 0;
  private transient int                                                     patientsWithNotes  = 0;
  @JsonProperty("patientStatistics") private final ArrayList<Histogram>     patientStatistics  = new ArrayList<>();
  @JsonProperty("consoleOutput") private final ArrayList<String>            consoleOutput      = new ArrayList<>();

  public void setOriginalUnparsedQuery(final String originalUnparsedQuery) {
    this.originalUnparsedQuery = originalUnparsedQuery;
  }

  public void addConsoleOutput(final String consoleOutputLine) {
    this.consoleOutput.add(consoleOutputLine);
  }

  public void setSkippedPatientCnt(final int skippedPatientCnt) {
    this.skippedPatientCnt = skippedPatientCnt;
  }

  public int getSkippedPatientCnt() {
    return skippedPatientCnt;
  }

  public ArrayList<String> getConsoleOutput() {
    return consoleOutput;
  }

  public String getOriginalUnparsedQuery() {
    return originalUnparsedQuery;
  }

  public static PatientSearchResponse createError(final String errorMessage, final int queryId) {
    final PatientSearchResponse response = new PatientSearchResponse(queryId, null, null, null);
    response.setError(errorMessage);
    return response;
  }

  public void addPidLine(final int pid, final double timeStart, final double timeEnd) {
    throw new UnsupportedOperationException();
  }

  public int getQueryId() {
    return queryId;
  }

  public void addWarnings(final String ... warnings) {
    if (warnings != null && warnings.length > 0) {
      final int existingLen = this.warningMessage == null ? 0 : this.warningMessage.length;
      if (this.warningMessage == null) {
        this.warningMessage = new String[0];
      }
      this.warningMessage = Arrays.copyOf(this.warningMessage, warnings.length + existingLen);
      for (int x = 0; x < warnings.length; x++) {
        this.warningMessage[existingLen + x] = warnings[x];
      }
    }
  }

  public String[] getWarningMessage() {
    return warningMessage;
  }

  public void clearData() {
    if (encounters != null) {
      Arrays.fill(encounters, 0);
    }
    if (durations != null) {
      Arrays.fill(durations, 0);
    }
    rxStatistics.clear();
    cptStatistics.clear();
    icd9Statistics.clear();
    ages.clear();
    processedPatients = 0;
    if (cohortPatientCnt != 0) {
      addWarnings(ErrorMessages.NOT_ENOUGH_PATIENTS);
    }
    cohortPatientCnt = 0;
    if (raceStatistics != null) {
      Arrays.fill(raceStatistics, 0);
    }
    if (genderStatistics != null) {
      Arrays.fill(genderStatistics, 0);
    }
  }

  public void setEncountersBuckets(final int[] encountersBuckets) {
    if (encountersBuckets != null) {
      this.encountersBuckets = encountersBuckets;
      this.encounters = new int[encountersBuckets.length];
    }
  }

  public void setDurationsBuckets(final int[] durationsBuckets) {
    if (durationsBuckets != null) {
      this.durationsBuckets = durationsBuckets;
      this.durations = new int[durationsBuckets.length];
    }
  }

  public PatientSearchResponse(final int queryId, final IndexCollection indices, final UmlsDictionary umls, final Statistics statistics) {
    this.queryId = queryId;
    this.indices = indices;
    this.umls = umls;
    this.statistics = statistics;
    if (statistics != null) {
      this.generalPatientCnt = statistics.getPatientCnt();
      icd9StatisticsTransientMap = new int[statistics.getIcd9KeyCnt()];
      icd10StatisticsTransientMap = new int[statistics.getIcd10KeyCnt()];
      cptStatisticsTransientMap = new int[statistics.getCptKeyCnt()];
      rxStatisticsTransientMap = new int[statistics.getRxNormKeyCnt()];
      labsStatisticsTransientMap = new int[statistics.getLabNormMaxKeyNr()];
      genderStatistics = new int[indices.getGenderKeySize()];
      raceStatistics = new int[indices.getRaceKeySize()];
    }
    else {
      this.generalPatientCnt = 0;
      icd9StatisticsTransientMap = null;
      icd10StatisticsTransientMap = null;
      cptStatisticsTransientMap = null;
      rxStatisticsTransientMap = null;
      labsStatisticsTransientMap = null;
    }
  }

  public void setExportLocation(final String exportLocation) {
    this.exportLocation = exportLocation;
  }

  public String getExportLocation() {
    return exportLocation;
  }

  public void setParsedQuery(final String parsedQuery) {
    this.parsedQuery = parsedQuery;
  }

  public String getParsedQuery() {
    return parsedQuery;
  }

  public void setError(final String error) {
    this.errorMessage = error;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setAges(final ArrayList<Histogram> ages) {
    this.ages = ages;
  }

  public ArrayList<Histogram> getAges() {
    return ages;
  }

  public void withPatientIds(final int howManyToRecord) {
    MAX_PATIENT_ID_CNT = howManyToRecord;
  }

  private void generatePatientStatistics() {
    patientStatistics.add(new Histogram("", "DRUGS", patientsWithDrugs, statistics.getPatientsWithDrugs(), patientsWithDrugs / (double) cohortPatientCnt,
        statistics.getPatientsWithDrugs() / (double) generalPatientCnt, statistics.getPatientCnt()));
    patientStatistics.add(new Histogram("", "ICD9", patientsWithIcd9, statistics.getPatientsWithIcd9(), patientsWithIcd9 / (double) cohortPatientCnt,
        statistics.getPatientsWithIcd9() / (double) generalPatientCnt, statistics.getPatientCnt()));
    patientStatistics.add(new Histogram("", "CPT", patientsWithCpt, statistics.getPatientsWithCpt(), patientsWithCpt / (double) cohortPatientCnt, statistics.getPatientsWithCpt()
        / (double) generalPatientCnt, statistics.getPatientCnt()));
    patientStatistics.add(new Histogram("", "LABS", patientsWithLabs, statistics.getPatientsWithLabs(), patientsWithLabs / (double) cohortPatientCnt,
        statistics.getPatientsWithLabs() / (double) generalPatientCnt, statistics.getPatientCnt()));
    patientStatistics.add(new Histogram("", "NOTES", patientsWithNotes, statistics.getPatientsWithNotes(), patientsWithNotes / (double) cohortPatientCnt,
        statistics.getPatientsWithNotes() / (double) generalPatientCnt, statistics.getPatientCnt()));
  }

  @SuppressWarnings("unchecked")
  @Override
  public void close() {
    if (cohortPatientCnt != 0 && generalPatientCnt != 0) {
      generateGenderHistograms();
      generateRaceHistograms();
      generatePatientStatistics();
      final HashSet<String> topLevelParentsIcd9 = umls.getIcd9TopLevelParents();
      final HashSet<String> topLevelParentsIcd10 = umls.getIcd10TopLevelParents();
      for (int x = 0; x < icd9StatisticsTransientMap.length; x++) {
        if (icd9StatisticsTransientMap[x] != 0) {
          final String icd9Code = indices.getIcd9(x);
          if (!icd9Code.endsWith("?") && !topLevelParentsIcd9.contains(icd9Code)) {
            String icd9Description = umls.getIcd9Text(icd9Code);
            if (icd9Description == null) {
              icd9Description = "";
            }
            icd9Statistics.add(new Histogram(icd9Code, icd9Description, icd9StatisticsTransientMap[x], statistics.getIcd9(x), icd9StatisticsTransientMap[x]
                / (double) cohortPatientCnt, statistics.getIcd9(x) / (double) generalPatientCnt, statistics.getPatientCnt()));
          }
        }
      }
      for (int x = 0; x < icd10StatisticsTransientMap.length; x++) {
        if (icd10StatisticsTransientMap[x] != 0) {
          final String icd10Code = indices.getIcd10(x);
          if (!icd10Code.endsWith("?") && !topLevelParentsIcd10.contains(icd10Code)) {
            String icd10Description = umls.getIcd10Text(icd10Code);
            if (icd10Description == null) {
              icd10Description = "";
            }
            icd9Statistics.add(new Histogram(icd10Code, icd10Description, icd10StatisticsTransientMap[x], statistics.getIcd10(x), icd10StatisticsTransientMap[x]
                / (double) cohortPatientCnt, statistics.getIcd10(x) / (double) generalPatientCnt, statistics.getPatientCnt()));
          }
        }
      }
      Collections.sort(icd9Statistics);
      icd9Statistics = ArrayUtils.removeFromIndex(icd9Statistics, 20);
      for (int x = 0; x < cptStatisticsTransientMap.length; x++) {
        if (cptStatisticsTransientMap[x] != 0) {
          final String cptCode = indices.getCpt(x);
          String cptDescription = umls.getCptText(cptCode);
          if (cptDescription == null) {
            cptDescription = "";
          }
          cptStatistics.add(new Histogram(cptCode, cptDescription, cptStatisticsTransientMap[x], statistics.getCpt(x), cptStatisticsTransientMap[x] / (double) cohortPatientCnt,
              statistics.getCpt(x) / (double) generalPatientCnt, statistics.getPatientCnt()));
        }
      }
      Collections.sort(cptStatistics);
      cptStatistics = ArrayUtils.removeFromIndex(cptStatistics, 20);
      for (int x = 0; x < rxStatisticsTransientMap.length; x++) {
        if (rxStatisticsTransientMap[x] != 0) {
          final String rxCode = String.valueOf(x);
          String rxDescription = umls.getRxNormText(x);
          if (rxDescription == null) {
            rxDescription = "";
          }
          rxStatistics.add(new Histogram(rxCode, rxDescription, rxStatisticsTransientMap[x], statistics.getRxNorm(x), rxStatisticsTransientMap[x] / (double) cohortPatientCnt,
              statistics.getRxNorm(x) / (double) generalPatientCnt, statistics.getPatientCnt()));
        }
      }
      Collections.sort(rxStatistics);
      rxStatistics = ArrayUtils.removeFromIndex(rxStatistics, 20);

      for (int x = 0; x < labsStatisticsTransientMap.length; x++) {
        if (labsStatisticsTransientMap[x] != 0) {
          String description = indices.getLabCommonName(x);
          if (description == null) {
            description = indices.getLabName(x);
            if (description == null) {
              description = "";
            }
          }
          labsStatistics.add(new Histogram("", description, labsStatisticsTransientMap[x], statistics.getLabs(x), labsStatisticsTransientMap[x] / (double) cohortPatientCnt,
              statistics.getLabs(x) / (double) generalPatientCnt, statistics.getPatientCnt()));
        }
      }
      Collections.sort(labsStatistics);
      labsStatistics = ArrayUtils.removeFromIndex(labsStatistics, 20);

    }
    if (binaryResponse) {
      try {
        final ByteArrayOutputStream s = new ByteArrayOutputStream();
        final StreamOutput persistentStream = new StreamOutput(s);
        save(persistentStream);
        s.flush();
        dos.writeInt(s.size());
        dos.write(s.toByteArray());
        dos.flush();
      }
      catch (final IOException e) {
        exception(e);
      }
    }
  }

  public ArrayList<DemographicsHistogram> getGenderHistogram() {
    return genderHistogram;
  }

  private void generateGenderHistograms() {
    genderHistogram.clear();
    for (int x = 1; x < genderStatistics.length; x++) {
      final String genderString = indices.getDemographics().getGender().getString(x);
      final int globalCnt = indices.getDemographics().getGender().getCount(genderString);
      if (globalCnt != 0) {
        genderHistogram.add(new DemographicsHistogram(genderString, genderStatistics[x], globalCnt, genderStatistics[x] / (double) cohortPatientCnt, globalCnt
            / (double) statistics.getPatientCnt(), cohortPatientCnt, statistics.getPatientCnt()));
      }
    }
    Collections.sort(genderHistogram);
    for (int x = genderHistogram.size() - 1; x > 3; x--) {
      genderHistogram.remove(x);
    }
  }

  public ArrayList<DemographicsHistogram> getRaceHistogram() {
    return raceHistogram;
  }

  private void generateRaceHistograms() {
    raceHistogram.clear();
    for (int x = 1; x < raceStatistics.length; x++) {
      final String raceString = indices.getDemographics().getRace().getString(x);
      final int globalCnt = indices.getDemographics().getRace().getCount(raceString);
      if (globalCnt != 0) {
        raceHistogram.add(new DemographicsHistogram(raceString, raceStatistics[x], globalCnt, raceStatistics[x] / (double) cohortPatientCnt, globalCnt
            / (double) statistics.getPatientCnt(), cohortPatientCnt, statistics.getPatientCnt()));
      }
    }
    Collections.sort(raceHistogram);
    for (int x = raceHistogram.size() - 1; x > 4; x--) {
      raceHistogram.remove(x);
    }
  }

  public void estimatePatientStatistics(final int estimatedTotalPatientCnt) {
    if (cohortPatientCnt != 0) {
      final int newCohortCnt = (int) Math.ceil((cohortPatientCnt / (double) processedPatients) * estimatedTotalPatientCnt);
      patientsWithCpt = (int) Math.ceil((patientsWithCpt / (double) cohortPatientCnt) * newCohortCnt);
      patientsWithIcd9 = (int) Math.ceil((patientsWithIcd9 / (double) cohortPatientCnt) * newCohortCnt);
      patientsWithDrugs = (int) Math.ceil((patientsWithDrugs / (double) cohortPatientCnt) * newCohortCnt);
      patientsWithLabs = (int) Math.ceil((patientsWithLabs / (double) cohortPatientCnt) * newCohortCnt);
      patientsWithNotes = (int) Math.ceil((patientsWithNotes / (double) cohortPatientCnt) * newCohortCnt);

      if (genderStatistics != null) {
        for (int x = 0; x < genderStatistics.length; x++) {
          genderStatistics[x] = (int) Math.ceil((genderStatistics[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (raceStatistics != null) {
        for (int x = 0; x < raceStatistics.length; x++) {
          raceStatistics[x] = (int) Math.ceil((raceStatistics[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (icd9StatisticsTransientMap != null) {
        for (int x = 0; x < icd9StatisticsTransientMap.length; x++) {
          icd9StatisticsTransientMap[x] = (int) Math.ceil((icd9StatisticsTransientMap[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (icd10StatisticsTransientMap != null) {
        for (int x = 0; x < icd10StatisticsTransientMap.length; x++) {
          icd10StatisticsTransientMap[x] = (int) Math.ceil((icd10StatisticsTransientMap[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (cptStatisticsTransientMap != null) {
        for (int x = 0; x < cptStatisticsTransientMap.length; x++) {
          cptStatisticsTransientMap[x] = (int) Math.ceil((cptStatisticsTransientMap[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (rxStatisticsTransientMap != null) {
        for (int x = 0; x < rxStatisticsTransientMap.length; x++) {
          rxStatisticsTransientMap[x] = (int) Math.ceil((rxStatisticsTransientMap[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (labsStatisticsTransientMap != null) {
        for (int x = 0; x < labsStatisticsTransientMap.length; x++) {
          labsStatisticsTransientMap[x] = (int) Math.ceil((labsStatisticsTransientMap[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (encounters != null) {
        for (int x = 0; x < encounters.length; x++) {
          encounters[x] = (int) Math.ceil((encounters[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      if (durations != null) {
        for (int x = 0; x < durations.length; x++) {
          durations[x] = (int) Math.ceil((durations[x] / (double) cohortPatientCnt) * newCohortCnt);
        }
      }

      cohortPatientCnt = (int) Math.ceil((cohortPatientCnt / (double) processedPatients) * estimatedTotalPatientCnt);
      processedPatients = estimatedTotalPatientCnt;
    }

  }

  public void recordStatistics(final Patient patient) {
    IntArrayList list = patient.getUniqueIcd9Codes();
    if (list.size() != 0) {
      patientsWithIcd9++;
    }
    for (int x = 0; x < list.size(); x++) {
      icd9StatisticsTransientMap[list.get(x)]++;
    }

    list = patient.getUniqueIcd10Codes();
    for (int x = 0; x < list.size(); x++) {
      icd10StatisticsTransientMap[list.get(x)]++;
    }

    list = patient.getUniqueCptCodes();
    if (list.size() != 0) {
      patientsWithCpt++;
    }
    for (int x = 0; x < list.size(); x++) {
      cptStatisticsTransientMap[list.get(x)]++;
    }
    list = patient.getUniqueRxNormCodes();
    if (list.size() != 0) {
      patientsWithDrugs++;
    }
    for (int x = 0; x < list.size(); x++) {
      rxStatisticsTransientMap[list.get(x)]++;
    }

    if (patient.getLabs().recordLabs(labsStatisticsTransientMap) != 0) {
      patientsWithLabs++;
    }

    if (patient.getUniqueNotePayloadIds().size() != 0) {
      patientsWithNotes++;
    }

    ArrayUtils.bucketizeValue(patient.getEncounterDays().size() / 2, encountersBuckets, encounters);
    ArrayUtils.bucketizeValue((int) ((patient.getEndTime() - patient.getStartTime()) / (double) (60 * 24 * 30)), durationsBuckets, durations);
  }

  public void recordDemographics(final int patientId) {
    genderStatistics[indices.getGenderId(patientId)]++;
    raceStatistics[indices.getRaceId(patientId)]++;
    cohortPatientCnt++;
  }

  public int getCohortPatientCnt() {
    return cohortPatientCnt;
  }

  public void setCohortPatientCnt(final int cohortPatientCnt) {
    this.cohortPatientCnt = cohortPatientCnt;
  }

  public void setGeneralPatientCnt(final int generalPatientCnt) {
    this.generalPatientCnt = generalPatientCnt;
  }

  public int getGeneralPatientCnt() {
    return generalPatientCnt;
  }

  public void setTimeTook(final long time) {
    this.timeTook = time;
  }

  public void setProcessedPatients(final int patientCnt) {
    this.processedPatients = patientCnt;
  }

  public void addPatient(final int id) {
    if (patientIds.size() <= MAX_PATIENT_ID_CNT) {
      patientIds.add(new double[] {id});
    }
  }

  public void addPatientTime(final double[] idStartEnd) {
    if (patientIds.size() <= MAX_PATIENT_ID_CNT) {
      patientIds.add(idStartEnd);
    }
  }

  public LinkedList<double[]> getPatientIds() {
    return patientIds;
  }

  public long getTimeTook() {
    return timeTook;
  }

  public void setCensored(final ArrayList<long[]> censored) {
    this.censored = censored;
  }

  public void setDeaths(final ArrayList<long[]> deaths) {
    this.deaths = deaths;
  }

  public ArrayList<long[]> getDeaths() {
    return deaths;
  }

  public ArrayList<long[]> getCensored() {
    return censored;
  }

  public int getProcessedPatients() {
    return processedPatients;
  }

  public void add(final PatientSearchResponse ... responses) {
    final HashMap<Long, Long> deathTimeToCnt = new HashMap<>();
    final HashMap<Long, Long> censoredTimeToCnt = new HashMap<>();
    ArrayList<DemographicsHistogram> genderData = genderHistogram;
    ArrayList<DemographicsHistogram> raceData = raceHistogram;
    ArrayList<Histogram> ageData = ages;
    for (final PatientSearchResponse resp : responses) {
      patientsWithCpt += resp.patientsWithCpt;
      patientsWithIcd9 += resp.patientsWithIcd9;
      patientsWithLabs += resp.patientsWithLabs;
      patientsWithDrugs += resp.patientsWithDrugs;
      patientsWithNotes += resp.patientsWithNotes;
      if (patientIds != null && resp.patientIds != null) {
        for (final double[] pid : resp.patientIds) {
          patientIds.add(pid);
        }
      }
      cohortPatientCnt += resp.cohortPatientCnt;
      processedPatients += resp.processedPatients;
      if (resp.errorMessage != null) {
        if (errorMessage == null) {
          errorMessage = resp.errorMessage;
        }
        else {
          if (!errorMessage.equals(resp.errorMessage)) {
            errorMessage += resp.errorMessage;
          }
        }
      }
      addMap(deathTimeToCnt, resp.deaths);
      addMap(censoredTimeToCnt, resp.censored);
      genderData = addHistogram(genderData, resp.genderHistogram, statistics.getPatientCnt());
      raceData = addHistogram(raceData, resp.raceHistogram, statistics.getPatientCnt());
      ageData = addHistogramNormal(ageData, resp.ages, statistics.getPatientCnt(), cohortPatientCnt);
      if (encounters != null && resp.encounters != null) {
        for (int x = 0; x < encounters.length; x++) {
          encounters[x] += resp.encounters[x];
        }
      }
      if (durations != null && resp.durations != null) {
        for (int x = 0; x < durations.length; x++) {
          durations[x] += resp.durations[x];
        }
      }
    }

    this.genderHistogram = genderData;
    this.raceHistogram = raceData;
    this.ages = ageData;
    addMap(deathTimeToCnt, this.deaths);
    addMap(censoredTimeToCnt, this.censored);
    setDeaths(PatientSearchManager.sortMap(deathTimeToCnt));
    setCensored(PatientSearchManager.sortMap(censoredTimeToCnt));
    if (isStatisticEmpty(icd9StatisticsTransientMap) && isStatisticEmpty(icd10StatisticsTransientMap)) {
      for (int x = 0; x < responses.length; x++) {
        if (responses[x].icd9Statistics.size() != 0) {
          icd9Statistics = responses[0].icd9Statistics;
          break;
        }
      }
    }
    if (isStatisticEmpty(cptStatisticsTransientMap)) {
      for (int x = 0; x < responses.length; x++) {
        if (responses[x].cptStatistics.size() != 0) {
          cptStatistics = responses[0].cptStatistics;
          break;
        }
      }
    }
    if (isStatisticEmpty(rxStatisticsTransientMap)) {
      for (int x = 0; x < responses.length; x++) {
        if (responses[x].rxStatistics.size() != 0) {
          rxStatistics = responses[0].rxStatistics;
          break;
        }
      }
    }
    if (isStatisticEmpty(labsStatisticsTransientMap)) {
      for (int x = 0; x < responses.length; x++) {
        if (responses[x].labsStatistics.size() != 0) {
          labsStatistics = responses[0].labsStatistics;
          break;
        }
      }
    }
  }

  private boolean isStatisticEmpty(final int[] stat) {
    if (stat != null) {
      for (int x = 0; x < stat.length; x++) {
        if (stat[x] != 0) {
          return false;
        }
      }
    }
    return true;
  }

  public static ArrayList<DemographicsHistogram> addHistogram(final ArrayList<DemographicsHistogram> tgt, final ArrayList<DemographicsHistogram> src, final int totalPatientCnt) {
    final ArrayList<DemographicsHistogram> result = new ArrayList<>();
    for (int x = 0; x < tgt.size(); x++) {
      boolean exists = false;
      for (int y = 0; y < src.size(); y++) {
        if (tgt.get(x).getLabel().equals(src.get(y).getLabel())) {
          result.add(DemographicsHistogram.add(tgt.get(x), src.get(y)));
          exists = true;
          break;
        }
      }
      if (!exists) {
        result.add(tgt.get(x));
      }
    }
    for (int x = 0; x < src.size(); x++) {
      boolean exists = false;
      for (int y = 0; y < tgt.size(); y++) {
        if (tgt.get(y).getLabel().equals(src.get(x).getLabel())) {
          exists = true;
          break;
        }
      }
      if (!exists) {
        result.add(src.get(x));
        result.get(result.size() - 1).setTotalPatientCnt(totalPatientCnt);
        result.get(result.size() - 1).recalculatePercentages();
      }
    }
    return result;
  }

  private static ArrayList<Histogram> addHistogramNormal(final ArrayList<Histogram> tgt, final ArrayList<Histogram> src, final int totalPatientCnt, final int cohortPatientCnt) {
    final ArrayList<Histogram> result = new ArrayList<>();
    for (int x = 0; x < tgt.size(); x++) {
      boolean exists = false;
      for (int y = 0; y < src.size(); y++) {
        if (tgt.get(x).getLabel().equals(src.get(y).getLabel())) {
          result.add(Histogram.add(tgt.get(x), src.get(y), cohortPatientCnt));
          exists = true;
          break;
        }
      }
      if (!exists) {
        result.add(tgt.get(x));
      }
    }
    for (int x = 0; x < src.size(); x++) {
      boolean exists = false;
      for (int y = 0; y < tgt.size(); y++) {
        if (tgt.get(y).getLabel().equals(src.get(x).getLabel())) {
          exists = true;
          break;
        }
      }
      if (!exists) {
        result.add(src.get(x));
        result.get(result.size() - 1).setTotalPatientCnt(totalPatientCnt);
      }
    }
    return result;
  }

  private void addMap(final HashMap<Long, Long> timeToCnt, final ArrayList<long[]> values) {
    if (values != null && timeToCnt != null) {
      for (int x = 0; x < values.size(); x++) {
        final Long cnt = timeToCnt.get(values.get(x)[0]);
        if (cnt == null) {
          timeToCnt.put(values.get(x)[0], values.get(x)[1]);
        }
        else {
          timeToCnt.put(values.get(x)[0], values.get(x)[1] + cnt);
        }
      }
    }
  }

  public static void main(final String[] args) {
    final PatientSearchResponse resp = new PatientSearchResponse(-1, null, null, null);
    resp.patientIds.add(new double[] {1, 1.1111112, 0.3000000});
    resp.patientIds.add(new double[] {2, 2.666666, 2.88888888});
    final Gson gson = new GsonBuilder().registerTypeAdapter(double.class, new JsonSerializer<Double>() {

      @Override
      public JsonElement serialize(final Double value, final Type arg1, final JsonSerializationContext arg2) {
        final double ceil = Math.ceil(value);
        if (Math.abs(value - ceil) < 0.000001) {
          return new JsonPrimitive((new BigDecimal(value)).setScale(0, BigDecimal.ROUND_HALF_UP));
        }
        return new JsonPrimitive(value);
      }
    }).create();
    System.out.println(gson.toJson(resp));
  }

  @Override
  public void write(final OutputStream out) throws IOException {
    info("Writing response in progress...");
    if (binaryResponse) {
      System.out.println("... as binary ...");
      out.write(bos.toByteArray());
      System.out.println("DONE");
    }
    else {
      System.out.println("... as JSON ...");
      final PrintWriter writer = new PrintWriter(new BufferedOutputStream(out));
      QueryUtils.writeJson(this, writer);
      writer.flush();
      System.out.println("DONE");
    }
  }

  public boolean containsErrors() {
    return errorMessage != null && !errorMessage.isEmpty();
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    timeTook = input.readLong(position);
    parsedQuery = input.readString(position);
    exportLocation = input.readString(position);
    cohortPatientCnt = input.readInt(position);
    generalPatientCnt = input.readInt(position);
    processedPatients = input.readInt(position);
    patientsWithCpt = input.readInt(position);
    patientsWithIcd9 = input.readInt(position);
    patientsWithDrugs = input.readInt(position);
    patientsWithLabs = input.readInt(position);
    patientsWithNotes = input.readInt(position);

    loadDemoHistogram(input, position, genderHistogram);
    loadDemoHistogram(input, position, raceHistogram);
    loadHistogram(input, position, icd9Statistics);
    loadHistogram(input, position, cptStatistics);
    loadHistogram(input, position, rxStatistics);
    loadHistogram(input, position, labsStatistics);
    loadHistogram(input, position, patientStatistics);
    loadHistogram(input, position, ages);
    encounters = loadArray(input, position);
    durations = loadArray(input, position);
    errorMessage = input.readString(position);
    warningMessage = new String[input.readUnsignedByte(position)];
    for (int x = 0; x < warningMessage.length; x++) {
      warningMessage[x] = input.readString(position);
    }
    deaths = loadArrayLong(input, position);
    censored = loadArrayLong(input, position);
    patientIds = loadArrayDouble(input, position);
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeLong(timeTook);
    output.writeString(parsedQuery);
    output.writeString(exportLocation);
    output.writeInt(cohortPatientCnt);
    output.writeInt(generalPatientCnt);
    output.writeInt(processedPatients);
    output.writeInt(patientsWithCpt);
    output.writeInt(patientsWithIcd9);
    output.writeInt(patientsWithDrugs);
    output.writeInt(patientsWithLabs);
    output.writeInt(patientsWithNotes);
    saveDemoHistogram(output, genderHistogram);
    saveDemoHistogram(output, raceHistogram);
    saveHistogram(output, icd9Statistics);
    saveHistogram(output, cptStatistics);
    saveHistogram(output, rxStatistics);
    saveHistogram(output, labsStatistics);
    saveHistogram(output, patientStatistics);
    saveHistogram(output, ages);
    saveArray(output, encounters);
    saveArray(output, durations);
    output.writeString(errorMessage);
    if (warningMessage == null) {
      output.writeUnsignedByte(0);
    }
    else {
      output.writeUnsignedByte(Math.min(warningMessage.length, 255));
      for (int x = 0; x < Math.min(warningMessage.length, 255); x++) {
        output.writeString(warningMessage[x]);
      }
    }
    saveArrayLong(output, deaths);
    saveArrayLong(output, censored);
    saveArrayDouble(output, patientIds);
  }

  private void saveArray(final Output output, final int[] arr) throws IOException {
    if (arr == null) {
      output.writeInt(0);
    }
    else {
      output.writeInt(arr.length);
      for (int x = 0; x < arr.length; x++) {
        output.writeInt(arr[x]);
      }
    }
  }

  public int[] getEncounters() {
    return encounters;
  }

  public int[] getDurations() {
    return durations;
  }

  private int[] loadArray(final Input input, final LongMutable position) throws IOException {
    final int size = input.readInt(position);
    final int[] result = new int[size];
    for (int x = 0; x < size; x++) {
      result[x] = input.readInt(position);
    }
    return result;
  }

  private void saveArrayLong(final Output output, final ArrayList<long[]> arr) throws IOException {
    if (arr == null) {
      output.writeInt(0);
    }
    else {
      output.writeInt(arr.size());
      int size = -1;
      for (int x = 0; x < arr.size(); x++) {
        if (size == -1) {
          size = arr.get(x).length;
        }
        else {
          if (size != arr.get(x).length) {
            throw new IOException("Inconsistent array sizes");
          }
        }
      }
      if (size > 255) {
        throw new IOException("ArrayLong has too large children");
      }
      if (size == -1) {
        output.writeUnsignedByte(0);
      }
      else {
        output.writeUnsignedByte(size);
        for (int x = 0; x < arr.size(); x++) {
          for (int y = 0; y < size; y++) {
            output.writeLong(arr.get(x)[y]);
          }
        }
      }
    }
  }

  private ArrayList<long[]> loadArrayLong(final Input input, final LongMutable position) throws IOException {
    final int size = input.readInt(position);
    final ArrayList<long[]> result = new ArrayList<>();
    if (size > 0) {
      final int arrayLengh = input.readUnsignedByte(position);
      for (int x = 0; x < size; x++) {
        final long[] val = new long[arrayLengh];
        for (int y = 0; y < arrayLengh; y++) {
          val[y] = input.readLong(position);
        }
        result.add(val);
      }
    }
    return result;
  }

  private LinkedList<double[]> loadArrayDouble(final Input input, final LongMutable position) throws IOException {
    final int size = input.readInt(position);
    final LinkedList<double[]> result = new LinkedList<>();
    if (size > 0) {
      final int arrayLengh = input.readUnsignedByte(position);
      for (int x = 0; x < size; x++) {
        final double[] val = new double[arrayLengh];
        for (int y = 0; y < arrayLengh; y++) {
          val[y] = Double.longBitsToDouble(input.readLong(position));
        }
        result.add(val);
      }
    }
    return result;
  }

  private void saveArrayDouble(final Output output, final LinkedList<double[]> arr) throws IOException {
    output.writeInt(arr.size());
    int size = -1;
    for (final double[] a : arr) {
      if (size == -1) {
        size = a.length;
      }
      else {
        if (!binaryResponse && size != a.length) {
          throw new IOException("Inconsistent array sizes");
        }
      }
    }
    if (size > 255) {
      throw new IOException("ArrayLong has too large children");
    }
    if (size == -1) {
      output.writeUnsignedByte(0);
    }
    else {
      output.writeUnsignedByte(size);
      for (final double[] a : arr) {
        for (int y = 0; y < size; y++) {
          output.writeLong(Double.doubleToRawLongBits(a[y]));
        }
      }
    }
  }

  private void saveDemoHistogram(final Output output, final ArrayList<DemographicsHistogram> histo) throws IOException {
    if (histo.size() > 255) {
      throw new IOException("Number of histograms is too big");
    }
    output.writeUnsignedByte(histo.size());
    for (int x = 0; x < histo.size(); x++) {
      histo.get(x).save(output);
    }
  }

  private void loadDemoHistogram(final Input input, final LongMutable position, final ArrayList<DemographicsHistogram> histo) throws IOException {
    final int size = input.readUnsignedByte(position);
    for (int x = 0; x < size; x++) {
      final DemographicsHistogram histogram = new DemographicsHistogram();
      position.set(histogram.load(input, position.get()));
      histo.add(histogram);
    }
  }

  private void loadHistogram(final Input input, final LongMutable position, final ArrayList<Histogram> histo) throws IOException {
    final int size = input.readUnsignedByte(position);
    for (int x = 0; x < size; x++) {
      final Histogram histogram = new Histogram();
      position.set(histogram.load(input, position.get()));
      histo.add(histogram);
    }
  }

  private void saveHistogram(final Output output, final ArrayList<Histogram> histo) throws IOException {
    if (histo.size() > 255) {
      throw new IOException("Number of histograms is too big");
    }
    output.writeUnsignedByte(histo.size());
    for (int x = 0; x < histo.size(); x++) {
      histo.get(x).save(output);
    }
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }
}