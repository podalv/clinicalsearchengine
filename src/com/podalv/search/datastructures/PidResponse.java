package com.podalv.search.datastructures;

import static com.podalv.utils.Logging.exception;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import com.podalv.extractor.stride6.Common;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;

public class PidResponse extends PatientSearchResponse {

  private final StringBuilder output        = new StringBuilder();
  private int                 lastPid       = -2;
  private double              lastStartTime = -2;
  private double              lastEndTime   = -2;

  public static PidResponse createBinaryResponse(final int queryId, final IndexCollection indices, final UmlsDictionary umls, final Statistics statistics) {
    return new PidResponse(queryId, indices, umls, statistics, true);
  }

  public static PidResponse createStringResponse(final int queryId, final IndexCollection indices, final UmlsDictionary umls, final Statistics statistics) {
    return new PidResponse(queryId, indices, umls, statistics, false);
  }

  private PidResponse(final int queryId, final IndexCollection indices, final UmlsDictionary umls, final Statistics statistics, final boolean binary) {
    super(queryId, indices, umls, statistics);
    binaryResponse = binary;
  }

  @Override
  public void addPidLine(final int pid, final double timeStart, final double timeEnd) {
    if (lastPid != pid || (Math.abs(lastStartTime - timeStart) > 0.00001 || Math.abs(lastEndTime - timeEnd) > 0.00001)) {
      record(pid, timeStart, timeEnd);
    }
    lastPid = pid;
    lastStartTime = timeStart;
    lastEndTime = timeEnd;
  }

  private void recordBinary(final int pid, final double timeStart, final double timeEnd) {
    try {
      if (timeStart < 0 || timeEnd < 0) {
        dos.writeInt(pid);
      }
      else {
        if (Double.compare(timeStart, timeEnd) == 0) {
          dos.writeInt(-1 * pid);
          dos.writeDouble(timeStart);
        }
        else {
          dos.writeInt(pid);
          dos.writeDouble(timeStart);
          dos.writeDouble(timeEnd);
        }
      }
    }
    catch (final Exception e) {
      errorMessage = "Error writing response";
      exception(e);
    }
  }

  private void recordString(final int pid, final double timeStart, final double timeEnd) {
    if (output.length() != 0) {
      output.append("\n");
    }
    if (timeStart < 0 || timeEnd < 0) {
      output.append(pid);
    }
    else {
      output.append(pid + "\t" + Common.timeInDaysToString(timeStart) + "\t" + Common.timeInDaysToString(timeEnd));
    }
  }

  private void record(final int pid, final double timeStart, final double timeEnd) {
    if (binaryResponse) {
      recordBinary(pid, timeStart, timeEnd);
    }
    else {
      recordString(pid, timeStart, timeEnd);
    }
  }

  public String getOutput() {
    return output.toString();
  }

  @Override
  public void write(final OutputStream out) throws IOException {
    if (binaryResponse) {
      writeBinary(out);
    }
    else {
      final PrintWriter pw = new PrintWriter(out);
      pw.println(output.toString());
      pw.flush();
    }
  }

  @Override
  public void close() {
    // Action not needed. Needs to overwrite the default close method
  }
}
