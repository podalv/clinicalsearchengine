package com.podalv.search.datastructures;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

/** Responses that can produce either a binary or a standard JSON response
 *
 * @author podalv
 *
 */
public abstract class SerializableResponse implements Closeable {

  final transient ByteArrayOutputStream             bos            = new ByteArrayOutputStream();
  final transient DataOutputStream                  dos            = new DataOutputStream(bos);
  @JsonProperty("binaryResponse") protected boolean binaryResponse = false;

  protected void writeBinary(final OutputStream out) throws IOException {
    bos.writeTo(out);
    out.flush();
  }

  public void setBinaryResponse(final boolean binaryResponse) {
    this.binaryResponse = binaryResponse;
  }

  public void write(final OutputStream out) throws IOException {
    if (binaryResponse) {
      writeBinary(out);
    }
    else {
      final PrintWriter pw = new PrintWriter(out);
      pw.println(new Gson().toJson(this));
      pw.flush();
    }
  }

}
