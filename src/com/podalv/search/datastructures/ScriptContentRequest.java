package com.podalv.search.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScriptContentRequest {

  @JsonProperty("scriptName") private String scriptName;

  public void setScriptName(final String scriptName) {
    this.scriptName = scriptName;
  }

  public String getScriptName() {
    return scriptName;
  }
}
