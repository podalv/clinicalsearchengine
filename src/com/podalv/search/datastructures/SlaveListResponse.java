package com.podalv.search.datastructures;

import java.util.ArrayList;

/** JSON wrapper that contains all the information about connected slaves
 *
 * @author podalv
 *
 */
public class SlaveListResponse {

  private int                                   count;
  private final ArrayList<ServerStatusResponse> urls = new ArrayList<>();
  private String                                error;

  public void setError(final String error) {
    this.error = error;
  }

  public String getError() {
    return error;
  }

  public int getCount() {
    return count;
  }

  public void clear() {
    urls.clear();
    count = 0;
  }

  public ArrayList<ServerStatusResponse> getUrls() {
    return urls;
  }

  public void addUrl(final ServerStatusResponse response) {
    urls.add(response);
    count = urls.size();
  }

}
