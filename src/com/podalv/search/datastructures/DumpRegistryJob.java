package com.podalv.search.datastructures;

/** Encapsulates a single job in the DumpRegistry
 *
 * @author podalv
 *
 */
public class DumpRegistryJob {

  public static enum REQUEST_STATUS {
    PENDING, PROCESSING, DONE, UNKNOWN
  };
  private REQUEST_STATUS status;
  private final String   id;

  public DumpRegistryJob(final String id) {
    this.id = id;
    status = REQUEST_STATUS.UNKNOWN;
  }

  public REQUEST_STATUS getStatus() {
    return status;
  }

  public String getId() {
    return id;
  }

  public DumpRegistryJob setStatus(final REQUEST_STATUS status) {
    this.status = status;
    return this;
  }
}
