package com.podalv.search.datastructures.index;

import java.io.IOException;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.googlecode.javaewah.datastructure.BitSet;
import com.podalv.input.Input;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.Iterators;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.list.IntIteratorCloneableInterface;
import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.OffHeapPersistentObject;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.arrays.integer.IntArrayCompression;
import com.podalv.utils.datastructures.LongMutable;

/** Stores PatientId Lists in a compressed format
 *
 * @author podalv
 *
 */
public class PatientIdLists implements PersistentObject, OffHeapPersistentObject {

  public static final int               CURRENT_VERSION = 0;
  public static final int               EMPTY_ARRAY     = -1;
  private final IntKeyIntOpenHashMap    idToValue       = new IntKeyIntOpenHashMap();
  private final IntKeyObjectMap<int[]>  idToArray       = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<long[]> idToBitset      = new IntKeyObjectMap<>();
  private final IntOpenHashSet          allPids         = new IntOpenHashSet();
  private int                           currentId       = 1;

  /**
   *
   * @param pids
   * @return all ids deleted because they had only patients to remove
   */
  public IntOpenHashSet keepPatients(final IntIterator pids) {
    final IntOpenHashSet keep = new IntOpenHashSet(pids);
    final IntOpenHashSet removedIds = new IntOpenHashSet();
    allPids.retainAll(keep);
    final IntKeyIntMapIterator i = idToValue.entries();
    while (i.hasNext()) {
      i.next();
      if (!keep.contains(i.getValue())) {
        removedIds.add(i.getKey());
        i.remove();
      }
    }
    final IntKeyObjectIterator<int[]> iterator = idToArray.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final int[] decompressed = IntArrayCompression.decompressSorted(iterator.getValue());
      final IntArrayList kept = new IntArrayList();
      for (int x = 0; x < decompressed.length; x++) {
        if (keep.contains(decompressed[x])) {
          kept.add(decompressed[x]);
        }
      }
      if (kept.size() == 0) {
        removedIds.add(iterator.getKey());
        iterator.remove();
      }
      else {
        idToArray.put(iterator.getKey(), IntArrayCompression.compressSorted(kept.toArray()));
      }
    }
    return removedIds;
  }

  public int add(final int[] value) {
    final int result = currentId++;
    if (value == null || value.length == 0) {
      return EMPTY_ARRAY;
    }
    else if (value.length == 1) {
      synchronized (idToValue) {
        idToValue.put(result, value[0]);
      }
    }
    else {
      final int[] compressed = IntArrayCompression.compressSorted(value);
      synchronized (idToArray) {
        idToArray.put(result, compressed);
      }
    }
    return result;
  }

  public int add(final BitSet value) {
    final int result = currentId++;
    if (value == null) {
      return EMPTY_ARRAY;
    }
    else {
      synchronized (idToValue) {
        value.trim();
        idToBitset.put(result, value.toArray());
      }
    }
    return result;
  }

  public int getSize(final int id) {
    try {
      if (idToArray.containsKey(id)) {
        synchronized (idToArray) {
          return IntArrayCompression.getDecompressedSize(idToArray.get(id));
        }
      }
      else if (idToValue.containsKey(id)) {
        return 1;
      }
      else if (idToBitset.containsKey(id)) {
        return BitSet.fromArray(idToBitset.get(id)).getSetBitCnt() * StatisticsBuilderCompressed.BITSET_BUCKET_SIZE;
      }
      else if (allPids.contains(id)) {
        return Integer.MAX_VALUE;
      }
      return 0;
    }
    catch (final Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  public IntIteratorCloneableInterface get(final int id) {
    if (id == EMPTY_ARRAY) {
      return Iterators.getEmptyCloneable();
    }
    if (idToArray.containsKey(id)) {
      return Iterators.createCloneable(IntArrayCompression.decompressSorted(idToArray.get(id)));
    }
    else if (idToValue.containsKey(id)) {
      return Iterators.createCloneable(new int[] {idToValue.get(id)});
    }
    else if (idToBitset.containsKey(id)) {
      return Iterators.createCloneable(java.util.BitSet.valueOf(idToBitset.get(id)), StatisticsBuilderCompressed.BITSET_BUCKET_SIZE);
    }
    return null;
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    idToArray.clear();
    idToValue.clear();
    int maxId = 0;
    final LongMutable position = new LongMutable(startPos);
    final int version = input.readInt(position);
    if (version != getCurrentVersion()) {
      throw new IOException("Unsupported version. Current version = " + version + ", supported version = " + getCurrentVersion());
    }
    int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int id = input.readInt(position);
      maxId = Math.max(id, maxId);
      final int val = input.readInt(position);
      idToValue.put(id, val);
      currentId = Math.max(currentId, id);
    }
    size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final int len = input.readInt(position);
      final int[] val = new int[len];
      for (int y = 0; y < len; y++) {
        val[y] = input.readInt(position);
      }
      idToArray.put(key, val);
      currentId = Math.max(currentId, key);
    }
    size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final int len = input.readInt(position);
      final long[] val = new long[len];
      for (int y = 0; y < len; y++) {
        val[y] = input.readLong(position);
      }
      idToBitset.put(key, val);
      currentId = Math.max(currentId, key);
    }
    size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      allPids.add(input.readInt(position));
    }
    currentId++;
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(getCurrentVersion());
    output.writeInt(idToValue.size());
    final IntKeyIntMapIterator iterator = idToValue.entries();
    while (iterator.hasNext()) {
      iterator.next();
      output.writeInt(iterator.getKey());
      output.writeInt(iterator.getValue());
    }
    output.writeInt(idToArray.size());
    final IntKeyObjectIterator<int[]> i = idToArray.entries();
    while (i.hasNext()) {
      i.next();
      output.writeInt(i.getKey());
      final int[] data = i.getValue();
      output.writeInt(data.length);
      for (int x = 0; x < data.length; x++) {
        output.writeInt(data[x]);
      }
    }
    output.writeInt(idToBitset.size());
    final IntKeyObjectIterator<long[]> it = idToBitset.entries();
    while (it.hasNext()) {
      it.next();
      output.writeInt(it.getKey());
      final long[] data = it.getValue();
      output.writeInt(data.length);
      for (int x = 0; x < data.length; x++) {
        output.writeLong(data[x]);
      }
    }
    output.writeInt(allPids.size());
    final com.podalv.maps.primitive.IntIterator ii = allPids.iterator();
    while (ii.hasNext()) {
      output.writeInt(ii.next());
    }
  }

  @Override
  public boolean isEmpty() {
    return idToArray.size() + idToValue.size() == 0;
  }

  @Override
  public long calculateOffHeapSize() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public long saveOffHeap(final OffHeapMemory mem, final long position) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getCurrentVersion() {
    return CURRENT_VERSION;
  }

}
