package com.podalv.search.datastructures.index;

import java.util.Arrays;
import java.util.BitSet;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.search.index.StatisticsAgeGenerator;

public class StatisticsBuilderBitSet extends StatisticsBuilder {

  private final IntKeyObjectMap<BitSet> icd9ToPatientIds             = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> departmentToPatientIds       = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> icd10ToPatientIds            = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> visitTypeToPatientIds        = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> noteTypeToPatientIds         = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> cptToPatientIds              = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> snomedToPatientIds           = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> vitalsToPatientIds           = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> labsToPatientIds             = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> rxToPatientIds               = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> atcToPatientIds              = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> tidToPatientIds              = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> tidNegatedToPatientIds       = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<BitSet> tidFamilyHistoryToPatientIds = new IntKeyObjectMap<>();
  private final StatisticsAgeGenerator  ageGenerator                 = new StatisticsAgeGenerator();

  public StatisticsBuilderBitSet(final int maxPatId) {
    super(maxPatId);
  }

  private BitSet createNew() {
    return MAX_PAT_ID == -1 ? new BitSet() : new BitSet(MAX_PAT_ID);
  }

  private void record(final IntKeyObjectMap<BitSet> map, final int key, final int value) {
    synchronized (map) {
      BitSet set = map.get(key);
      if (set == null) {
        set = createNew();
        map.put(key, set);
      }
      set.set(value);
    }
  }

  @Override
  public void recordIcd9(final int icd9, final int pid) {
    record(icd9ToPatientIds, icd9, pid);
  }

  @Override
  public void recordNegatedTid(final int negatedTid, final int pid) {
    record(tidNegatedToPatientIds, negatedTid, pid);
  }

  @Override
  public void recordFhTid(final int fhTid, final int pid) {
    record(tidFamilyHistoryToPatientIds, fhTid, pid);
  }

  @Override
  public void recordTid(final int tid, final int pid) {
    record(tidToPatientIds, tid, pid);
  }

  @Override
  public void recordAtc(final int atc, final int pid) {
    record(atcToPatientIds, atc, pid);
  }

  @Override
  public void recordRx(final int rx, final int pid) {
    record(rxToPatientIds, rx, pid);
  }

  @Override
  public void recordLabs(final int labs, final int pid) {
    record(labsToPatientIds, labs, pid);
  }

  @Override
  public void recordVitals(final int vitals, final int pid) {
    record(vitalsToPatientIds, vitals, pid);
  }

  @Override
  public void recordSnomed(final int snomed, final int pid) {
    record(snomedToPatientIds, snomed, pid);
  }

  @Override
  public void recordCpt(final int cpt, final int pid) {
    record(cptToPatientIds, cpt, pid);
  }

  @Override
  public void recordVisitType(final int visitType, final int pid) {
    record(visitTypeToPatientIds, visitType, pid);
  }

  @Override
  public void recordNoteType(final int noteType, final int pid) {
    record(noteTypeToPatientIds, noteType, pid);
  }

  private IntArrayList setToList(final BitSet set) {
    final IntArrayList result = new IntArrayList();
    int pos = -1;
    while (true) {
      pos = set.nextSetBit(pos + 1);
      if (pos == -1) {
        break;
      }
      result.add(pos);
    }
    return result;
  }

  @Override
  public void finalizeIcd9(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, icd9ToPatientIds, tgtMap);
  }

  @Override
  public void finalizeCpt(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, cptToPatientIds, tgtMap);
  }

  @Override
  public void finalizeVisitType(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, visitTypeToPatientIds, tgtMap);
  }

  @Override
  public void finalizeNoteType(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, noteTypeToPatientIds, tgtMap);
  }

  @Override
  public void finalizeSnomed(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, snomedToPatientIds, tgtMap);
  }

  @Override
  public void finalizeVitals(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, vitalsToPatientIds, tgtMap);
  }

  @Override
  public void finalizeLabs(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, labsToPatientIds, tgtMap);
  }

  @Override
  public void finalizeAtc(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, atcToPatientIds, tgtMap);
  }

  @Override
  public void finalizeTid(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, tidToPatientIds, tgtMap);
  }

  @Override
  public void finalizeTidNegated(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, tidNegatedToPatientIds, tgtMap);
  }

  @Override
  public void finalizeTidFh(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, tidFamilyHistoryToPatientIds, tgtMap);
  }

  @Override
  public void finalizeRx(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, rxToPatientIds, tgtMap);
  }

  private void finalizeMap(final PatientIdLists patientIdLists, final IntKeyObjectMap<BitSet> srcMap, final IntKeyIntOpenHashMap tgtMap) {
    try {
      final IntKeyObjectIterator<BitSet> iterator = srcMap.entries();
      while (iterator.hasNext()) {
        iterator.next();
        final IntOpenHashSet resultSet = IntOpenHashSet.create(patientIdLists.get(tgtMap.get(iterator.getKey())));
        resultSet.addAll(setToList(srcMap.get(iterator.getKey())));
        if (resultSet.size() != 0) {
          final int[] resultData = resultSet.toArray();
          Arrays.sort(resultData);
          tgtMap.put(iterator.getKey(), patientIdLists.add(resultData));
        }
      }
    }
    catch (final Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  @Override
  public void recordIcd10(final int icd10, final int pid) {
    record(icd10ToPatientIds, icd10, pid);
  }

  @Override
  public void finalizeIcd10(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, icd10ToPatientIds, tgtMap);
  }

  @Override
  public void recordDepartment(final int department, final int pid) {
    record(departmentToPatientIds, department, pid);
  }

  @Override
  public void finalizeDepartment(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, departmentToPatientIds, tgtMap);
  }

  @Override
  public void recordStartEnd(final int pid, final int start, final int end) {
    ageGenerator.addRecord(pid, start, end);
  }

  @Override
  public IntArrayList getPidStartEndOrderedByEnd() {
    return ageGenerator.getSortedByEnd();
  }

  @Override
  public IntArrayList getPidStartEndOrderedByStart() {
    return ageGenerator.getSortedByStart();
  }

}