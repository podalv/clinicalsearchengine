package com.podalv.search.datastructures.index;

import java.io.IOException;
import java.util.Arrays;

import com.podalv.input.Input;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.datastructures.LongMutable;

/** A storage of unsignedChar index
 *
 * @author podalv
 *
 */
public class UnsignedShortIndex implements PersistentObject {

  private char[] values;

  public UnsignedShortIndex(final int numberOfElements, final int defaultValue) {
    values = new char[numberOfElements + 1];
    Arrays.fill(values, (char) defaultValue);
  }

  public int get(final int pos) {
    return values[pos];
  }

  public UnsignedShortIndexIterator iterate(final int value) {
    return new UnsignedShortIndexIterator(values, value);
  }

  public UnsignedShortIndexIterator iterateExcluding(final int value) {
    return UnsignedShortIndexIterator.getAllNumbersExcept(values, value);
  }

  public void add(final int patientId, final int value) {
    values[patientId] = (char) value;
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    final int size = input.readInt(position);
    final char[] values = new char[size];
    for (int x = 0; x < values.length; x++) {
      values[x] = (char) input.readUnsignedShort(position);
    }
    this.values = values;
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(values.length);
    for (int x = 0; x < values.length; x++) {
      output.writeUnsignedShort(values[x]);
    }
  }

  @Override
  public boolean isEmpty() {
    return values.length == 0;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }
}