package com.podalv.search.datastructures.index;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;

public abstract class StatisticsBuilder {

  final int MAX_PAT_ID;

  public StatisticsBuilder(final int maxPatId) {
    MAX_PAT_ID = maxPatId;
  }

  public abstract void recordIcd9(final int icd9, final int pid);

  public abstract void recordStartEnd(final int pid, final int start, final int end);

  public abstract IntArrayList getPidStartEndOrderedByStart();

  public abstract IntArrayList getPidStartEndOrderedByEnd();

  public abstract void recordIcd10(final int icd10, final int pid);

  public abstract void recordNegatedTid(final int negatedTid, final int pid);

  public abstract void recordFhTid(final int fhTid, final int pid);

  public abstract void recordTid(final int tid, final int pid);

  public abstract void recordAtc(final int atc, final int pid);

  public abstract void recordRx(final int rx, final int pid);

  public abstract void recordLabs(final int labs, final int pid);

  public abstract void recordVitals(final int vitals, final int pid);

  public abstract void recordSnomed(final int snomed, final int pid);

  public abstract void recordDepartment(final int department, final int pid);

  public abstract void recordCpt(final int cpt, final int pid);

  public abstract void recordVisitType(final int visitType, final int pid);

  public abstract void recordNoteType(final int noteType, final int pid);

  public abstract void finalizeIcd9(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeIcd10(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeCpt(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeVisitType(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeNoteType(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeSnomed(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeDepartment(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeVitals(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeLabs(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeAtc(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeTid(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeTidNegated(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeTidFh(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

  public abstract void finalizeRx(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap);

}
