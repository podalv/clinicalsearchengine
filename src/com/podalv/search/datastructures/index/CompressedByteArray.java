package com.podalv.search.datastructures.index;

import java.io.IOException;

import com.podalv.input.Input;
import com.podalv.utils.arrays.ArrayUtils;

public class CompressedByteArray extends CompressedArray {

  private final byte[] array;

  protected CompressedByteArray(final int[] array) {
    this.array = new byte[array.length];
    for (int x = 0; x < array.length; x++) {
      this.array[x] = ArrayUtils.convertUnsignedByteToByteArray(array[x]);
    }
  }

  @Override
  public int get(final int position) {
    return ArrayUtils.convertByteToUnsignedByte(array[position]);
  }

  @Override
  public int size() {
    return array.length;
  }

  @Override
  public CompressedArrayIterator iterate() {
    return new CompressedArrayIterator(this);
  }

  @Override
  public CompressedArrayIterator iterateExcluding(final int value) {
    return new CompressedArrayIterator(this, value);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    throw new UnsupportedOperationException("Can only load the CompressedIntArray, after loading that should be compressed using the builder into appropriate classes");
  }

  @Override
  public boolean isEmpty() {
    return array.length == 0;
  }

}
