package com.podalv.search.datastructures.index;

import java.util.ArrayList;
import java.util.Arrays;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.ObjectKeyIntOpenHashMap;

/** An index of enums that does not use enums but actual Strings. Allows creating iterators that
 *  iterate over a certain type or all types
 *
 * @author podalv
 *
 */
public class StringEnumIndexBuilder {

  public static final String            NULL            = "NULL";
  public static final String            EMPTY           = "EMPTY";
  public static final String            UNMAPPED        = "UNMAPPED CONCEPT";
  private static final int              ARRAY_INCREMENT = 100;
  private final ArrayList<String>       idToString      = new ArrayList<String>();
  private final ObjectKeyIntOpenHashMap stringToIdMap   = new ObjectKeyIntOpenHashMap();
  private final IntArrayList            index           = new IntArrayList();
  private int                           size            = 0;

  public StringEnumIndexBuilder() {
    idToString.add(NULL);
    idToString.add(UNMAPPED);
    stringToIdMap.put(NULL, 0);
    stringToIdMap.put(UNMAPPED, 1);
  }

  private void ensureCapacity(final int id) {
    if (index.size() - 1 < id) {
      for (int x = index.size(); x < id + ARRAY_INCREMENT; x++) {
        index.add(0);
      }
    }
  }

  private int getStringId(final String text) {
    int result = -1;
    if (stringToIdMap.containsKey(text)) {
      result = stringToIdMap.get(text);
    }
    else {
      synchronized (stringToIdMap) {
        idToString.add(text);
        stringToIdMap.put(text, idToString.size() - 1);
        result = idToString.size() - 1;
      }
    }
    return result;
  }

  public void add(final int id, final String value) {
    size = Math.max(id, size);
    ensureCapacity(id);
    index.set(id, getStringId(value.toUpperCase()));
  }

  public StringEnumIndex build() throws UnsupportedDataTypeException {
    return new StringEnumIndex(idToString, new IntArrayList(Arrays.copyOf(index.toArray(), size + 1)));
  }

}