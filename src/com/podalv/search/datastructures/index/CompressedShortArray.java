package com.podalv.search.datastructures.index;

import java.io.IOException;

import com.podalv.input.Input;
import com.podalv.utils.arrays.ArrayUtils;

public class CompressedShortArray extends CompressedArray {

  private final short[] array;

  protected CompressedShortArray(final int[] array) {
    this.array = new short[array.length];
    for (int x = 0; x < array.length; x++) {
      this.array[x] = (short) ArrayUtils.convertIntToShorts(array[x])[1];
    }
  }

  @Override
  public int get(final int position) {
    return ArrayUtils.convertShortsToInt((short) 0, this.array[position]);
  }

  @Override
  public int size() {
    return array.length;
  }

  @Override
  public boolean isEmpty() {
    return array.length == 0;
  }

  @Override
  public CompressedArrayIterator iterate() {
    return new CompressedArrayIterator(this);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    throw new UnsupportedOperationException("Can only load the CompressedIntArray, after loading that should be compressed using the builder into appropriate classes");
  }

  @Override
  public CompressedArrayIterator iterateExcluding(final int value) {
    return new CompressedArrayIterator(this, value);
  }

}
