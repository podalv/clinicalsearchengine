package com.podalv.search.datastructures.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.input.Input;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.datastructures.LongMutable;

public class StringIntegerFastIndex implements PersistentObject {

  private final ArrayList<String>        idToString = new ArrayList<>();
  private final HashMap<String, Integer> stringToId = new HashMap<>();

  public String[] keySet() {
    return idToString.toArray(new String[idToString.size()]);
  }

  public int size() {
    return idToString.size();
  }

  public void set(final int id, final String str) {
    synchronized (stringToId) {
      stringToId.put(str, id);
      for (int x = idToString.size(); x <= id; x++) {
        idToString.add(null);
      }
      idToString.set(id, str);
      stringToId.put(str, id);
    }
  }

  public int add(final String str) {
    Integer result = stringToId.get(str);
    if (result != null) {
      return result;
    }
    synchronized (stringToId) {
      result = stringToId.get(str);
      if (result != null) {
        return result;
      }
      idToString.add(str);
      stringToId.put(str, idToString.size() - 1);
      return idToString.size() - 1;
    }
  }

  public void clear() {
    idToString.clear();
    stringToId.clear();
  }

  public int add(final String str, final String searchString) {
    Integer result = stringToId.get(searchString);
    if (result != null) {
      return result;
    }
    synchronized (stringToId) {
      result = stringToId.get(searchString);
      if (result != null) {
        return result;
      }
      idToString.add(str);
      stringToId.put(searchString, idToString.size() - 1);
      return idToString.size() - 1;
    }
  }

  public boolean contains(final String str) {
    return stringToId.containsKey(str);
  }

  public int get(final String str) {
    return stringToId.containsKey(str) ? stringToId.get(str) : Integer.MIN_VALUE;
  }

  public String get(final int id) {
    return idToString.get(id);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    idToString.clear();
    stringToId.clear();
    final LongMutable position = new LongMutable(startPos);
    final int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      add(input.readString(position));
    }
    for (int x = 0; x < size; x++) {
      stringToId.put(input.readString(position), input.readInt(position));
    }
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(idToString.size());
    for (int x = 0; x < idToString.size(); x++) {
      output.writeString(idToString.get(x));
    }
    final Iterator<Entry<String, Integer>> i = stringToId.entrySet().iterator();
    while (i.hasNext()) {
      final Entry<String, Integer> entry = i.next();
      output.writeString(entry.getKey());
      output.writeInt(entry.getValue());
    }
  }

  @Override
  public boolean isEmpty() {
    return idToString.size() == 0;
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder();
    for (int x = 0; x < idToString.size(); x++) {
      result.append(x + ": '" + idToString.get(x) + "'\n");
    }
    return result.toString();
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }
}