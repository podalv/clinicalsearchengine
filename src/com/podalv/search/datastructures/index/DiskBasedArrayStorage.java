package com.podalv.search.datastructures.index;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.podalv.utils.file.FileUtils;

/** Used for storing huge arrays that would not fit in memory
 *  Not thread safe
 *
 * @author podalv
 *
 */
public class DiskBasedArrayStorage {

  private final File          file;
  private final AtomicInteger currentMapId = new AtomicInteger(-1);

  private void clearStorage() {
    if (file.exists()) {
      FileUtils.deleteWithMessage(file);
    }
  }

  public DiskBasedArrayStorage(final File file) {
    this.file = file;
    clearStorage();
  }

  public int add(final int[] array) throws IOException {
    final DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file, true)));
    final int id = currentMapId.incrementAndGet();
    dos.writeInt(id);
    dos.writeInt(array.length);
    for (int x = 0; x < array.length; x++) {
      dos.writeInt(array[x]);
    }
    dos.close();
    return id;
  }

  public int[] get(final int id) throws IOException {
    final DataInputStream dos = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
    try {
      while (true) {
        final int curId = dos.readInt();
        if (curId == id) {
          final int len = dos.readInt();
          final int[] result = new int[len];
          for (int x = 0; x < len; x++) {
            result[x] = dos.readInt();
          }
          return result;
        }
        long toSkip = (dos.readInt() * 4);
        while (toSkip > 0) {
          toSkip = toSkip - dos.skip(toSkip);
        }
      }
    }
    finally {
      dos.close();
    }
  }

}
