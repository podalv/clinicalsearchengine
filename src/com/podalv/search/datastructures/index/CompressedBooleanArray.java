package com.podalv.search.datastructures.index;

import java.io.IOException;
import java.util.BitSet;

import com.podalv.input.Input;

/** If the values are between 0-1, they are stored as booleans
 *
 * @author podalv
 *
 */
public class CompressedBooleanArray extends CompressedArray {

  private final BitSet set;
  private final int    size;

  protected CompressedBooleanArray(final int[] array) {
    this.size = array.length;
    set = new BitSet(array.length);
    for (int x = 0; x < array.length; x++) {
      if (array[x] == 1) {
        set.set(x);
      }
    }
  }

  @Override
  public int get(final int position) {
    return set.get(position) ? 1 : 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public CompressedArrayIterator iterate() {
    return new CompressedArrayIterator(this);
  }

  @Override
  public CompressedArrayIterator iterateExcluding(final int value) {
    return new CompressedArrayIterator(this, value);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    throw new UnsupportedOperationException("Can only load the CompressedIntArray, after loading that should be compressed using the builder into appropriate classes");
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }
}
