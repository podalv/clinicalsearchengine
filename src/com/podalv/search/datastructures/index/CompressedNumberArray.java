package com.podalv.search.datastructures.index;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.set.IntOpenHashSet;

/** Stores numbers in a variaty of different indices based on maximum values in the array
 *  If there are only 2 values, boolean is used to back the values, if 256, unsigned byte....
 *
 * @author podalv
 *
 */
public class CompressedNumberArray {

  private CompressedNumberArray() {
    // static access only
  }

  private static IntOpenHashSet getUniqueValues(final int[] array) {
    final IntOpenHashSet uniqueValues = new IntOpenHashSet();
    for (int x = 0; x < array.length; x++) {
      uniqueValues.add(array[x]);
    }
    return uniqueValues;
  }

  private static int getMin(final IntOpenHashSet set) {
    int min = Integer.MAX_VALUE;
    final IntIterator iterator = set.iterator();
    while (iterator.hasNext()) {
      min = Math.min(min, iterator.next());
    }
    return min;
  }

  private static int getMax(final IntOpenHashSet set) {
    int max = Integer.MIN_VALUE;
    final IntIterator iterator = set.iterator();
    while (iterator.hasNext()) {
      max = Math.max(max, iterator.next());
    }
    return max;
  }

  public static CompressedArray buildEmpty() {
    return new CompressedIntArray(new int[0]);
  }

  public static CompressedArray build(final int[] array) throws UnsupportedDataTypeException {
    final IntOpenHashSet uniqueValues = getUniqueValues(array);
    final int min = getMin(uniqueValues);
    final int max = getMax(uniqueValues);
    if (min < 0) {
      throw new UnsupportedDataTypeException("Negative values are not supported by the CompressedNumberArray");
    }
    if (max < 2) {
      return new CompressedBooleanArray(array);
    }
    else if (max < 256) {
      return new CompressedByteArray(array);
    }
    else if (max < 65536) {
      return new CompressedShortArray(array);
    }
    return new CompressedIntArray(array);
  }
}
