package com.podalv.search.datastructures.index;

import com.podalv.maps.primitive.list.IntIteratorCloneableInterface;

/** Iterates over a
 *
 * @author podalv
 *
 */
public class StringEnumIndexIterator implements IntIteratorCloneableInterface {

  private final int             value;
  private int                   currentPos = -1;
  private final CompressedArray index;

  protected StringEnumIndexIterator(final CompressedArray index, final int value) {
    this.value = value;
    this.index = index;
    readNext();
  }

  private void readNext() {
    if (currentPos != Integer.MAX_VALUE) {
      for (int x = currentPos + 1; x < index.size(); x++) {
        if (index.get(x) == value) {
          currentPos = x;
          return;
        }
      }
      currentPos = Integer.MAX_VALUE;
    }
  }

  @Override
  public boolean hasNext() {
    return currentPos != Integer.MAX_VALUE;
  }

  @Override
  public int next() {
    final int result = currentPos;
    readNext();
    return result;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }

  @Override
  public IntIteratorCloneableInterface getClone() {
    return new StringEnumIndexIterator(index, value);
  }
}
