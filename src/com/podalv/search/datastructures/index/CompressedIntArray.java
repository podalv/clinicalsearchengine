package com.podalv.search.datastructures.index;

import java.io.IOException;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.input.Input;
import com.podalv.utils.datastructures.LongMutable;

public class CompressedIntArray extends CompressedArray {

  private int[] array;

  public static final CompressedIntArray createEmpty() {
    return new CompressedIntArray(new int[0]);
  }

  protected CompressedIntArray(final int[] array) {
    this.array = new int[array.length];
    for (int x = 0; x < array.length; x++) {
      this.array[x] = array[x];
    }
  }

  public CompressedArray compress() throws UnsupportedDataTypeException {
    return CompressedNumberArray.build(array);
  }

  @Override
  public int get(final int position) {
    return array[position];
  }

  @Override
  public int size() {
    return array.length;
  }

  @Override
  public CompressedArrayIterator iterate() {
    return new CompressedArrayIterator(this);
  }

  @Override
  public CompressedArrayIterator iterateExcluding(final int value) {
    return new CompressedArrayIterator(this, value);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    final int len = input.readInt(position);
    array = new int[len];
    for (int x = 0; x < len; x++) {
      array[x] = input.readInt(position);
    }
    return position.get();
  }

  @Override
  public boolean isEmpty() {
    return array.length == 0;
  }

}
