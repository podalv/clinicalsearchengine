package com.podalv.search.datastructures.index;

import com.podalv.maps.primitive.IntIterator;

public class UnsignedShortIndexIterator implements IntIterator {

  private final char[] array;
  private int          currentPos    = 0;
  private int          currentResult = -1;
  private final char   valueToFind;
  private boolean      exclude       = false;

  public static UnsignedShortIndexIterator getAllNumbersExcept(final char[] array, final int excludeValue) {
    final UnsignedShortIndexIterator result = new UnsignedShortIndexIterator(array, excludeValue, true);
    return result;
  }

  UnsignedShortIndexIterator(final char[] array, final int valueToFind, final boolean exclude) {
    this.array = array;
    this.exclude = exclude;
    this.valueToFind = (char) valueToFind;
    currentResult = findNext();
  }

  UnsignedShortIndexIterator(final char[] array, final int valueToFind) {
    this.array = array;
    this.valueToFind = (char) valueToFind;
    currentResult = findNext();
  }

  private int findNext() {
    for (int x = currentPos; x < array.length; x++) {
      if ((exclude && array[x] != valueToFind) || (!exclude && array[x] == valueToFind)) {
        currentResult = x;
        currentPos = x + 1;
        return x;
      }
    }
    return -1;
  }

  @Override
  public boolean hasNext() {
    return currentResult != -1;
  }

  @Override
  public int next() {
    final int oldResult = currentResult;
    currentResult = findNext();
    return oldResult;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }

}
