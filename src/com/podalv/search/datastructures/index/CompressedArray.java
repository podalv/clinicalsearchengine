package com.podalv.search.datastructures.index;

import java.io.IOException;

import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;

public abstract class CompressedArray implements PersistentObject {

  public abstract int get(int position);

  public abstract int size();

  public abstract CompressedArrayIterator iterate();

  public abstract CompressedArrayIterator iterateExcluding(final int value);

  @Override
  public int getCurrentVersion() {
    return 0;
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(size());
    for (int x = 0; x < size(); x++) {
      output.writeInt(get(x));
    }
  }

}