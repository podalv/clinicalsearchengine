package com.podalv.search.datastructures.index;

import java.io.IOException;
import java.util.ArrayList;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.input.Input;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.ObjectKeyIntOpenHashMap;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.datastructures.LongMutable;

/** An index of enums that does not use enums but actual Strings. Allows creating iterators that
 *  iterate over a certain type or all types
 *
 * @author podalv
 *
 */
public class StringEnumIndex implements PersistentObject {

  public static final int               NULL_ID   = 0;
  public static final int               UNDEFINED = -1;
  private String[]                      keys;
  private CompressedArray               index;
  private int[]                         counts;
  private final ObjectKeyIntOpenHashMap map       = new ObjectKeyIntOpenHashMap();

  public StringEnumIndex() {
    this.keys = new String[0];
    this.counts = new int[0];
    this.index = CompressedNumberArray.buildEmpty();
  }

  public String[] keys() {
    return keys;
  }

  protected StringEnumIndex(final ArrayList<String> keys, final IntArrayList index) throws UnsupportedDataTypeException {
    this.keys = new String[keys.size()];
    this.counts = new int[keys.size()];
    for (int x = 0; x < keys.size(); x++) {
      this.keys[x] = keys.get(x);
      map.put(keys.get(x), x);
    }
    for (int x = 0; x < index.size(); x++) {
      if (index.get(x) >= 0) {
        counts[index.get(x)]++;
      }
    }
    this.index = CompressedNumberArray.build(index.toArray());
  }

  public int size() {
    return index.size();
  }

  public int getCount(final String value) {
    final int id = getId(value);
    return (id < 0 || id > counts.length) ? 0 : counts[id];
  }

  public int getCount(final String value, final int[] patientIds) {
    int cnt = 0;
    final StringEnumIndexIterator iterator = iterator(getId(value));
    int currentPos = 0;
    while (iterator.hasNext()) {
      final int patientId = iterator.next();
      if (patientIds == null || patientId == patientIds[currentPos]) {
        cnt++;
        currentPos++;
        if (patientIds != null && currentPos > patientIds.length - 1) {
          break;
        }
        continue;
      }
      if (patientId > patientIds[currentPos]) {
        if (patientId > patientIds[patientIds.length - 1]) {
          break;
        }
        for (int x = currentPos; x < patientIds.length; x++) {
          if (patientId <= patientIds[x]) {
            currentPos = x;
            break;
          }
        }
      }
    }
    return cnt;
  }

  /**
   *
   * @param id
   * @return null if out of bounds
   */
  public String getString(final int id) {
    return id >= 0 && id <= keys.length - 1 ? keys[id] : null;
  }

  public int keySize() {
    return keys.length;
  }

  /**
   *
   * @param text
   * @return UNDEFINED (-1) if not exists
   */
  public int getId(String text) {
    text = text.toUpperCase();
    return map.containsKey(text) ? map.get(text) : UNDEFINED;
  }

  /**
   *
   * @param position
   * @return UNDEFINED (-1) if not exists
   */
  public int getValue(final int position) {
    return position >= 0 && position <= index.size() - 1 ? index.get(position) : UNDEFINED;
  }

  public StringEnumIndexIterator iterator(final int id) {
    return new StringEnumIndexIterator(index, id);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    final int size = input.readInt(position);
    keys = new String[size];
    counts = new int[size];
    map.clear();
    for (int x = 0; x < size; x++) {
      keys[x] = input.readString(position);
      map.put(keys[x], x);
    }
    final CompressedIntArray temp = CompressedIntArray.createEmpty();
    position.set(temp.load(input, position.get()));
    for (int x = 0; x < temp.size(); x++) {
      counts[temp.get(x)]++;
    }
    index = temp.compress();
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(keys.length);
    for (int x = 0; x < keys.length; x++) {
      output.writeString(keys[x]);
    }
    index.save(output);
  }

  @Override
  public boolean isEmpty() {
    return index.size() == 0;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }
}