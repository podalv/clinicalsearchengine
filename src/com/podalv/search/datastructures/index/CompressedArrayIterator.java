package com.podalv.search.datastructures.index;

import com.podalv.maps.primitive.IntIterator;

/** Iterates over data
 *
 * @author podalv
 *
 */
public class CompressedArrayIterator implements IntIterator {

  private final int             excluding;
  private final boolean         excludeValues;
  private int                   currentPosition = -1;
  private final CompressedArray array;

  protected CompressedArrayIterator(final CompressedArray array) {
    this.excludeValues = false;
    this.excluding = -1;
    this.array = array;
    readNext();
  }

  protected CompressedArrayIterator(final CompressedArray array, final int excludeValue) {
    this.array = array;
    this.excludeValues = true;
    this.excluding = excludeValue;
    readNext();
  }

  private void readNext() {
    for (int x = currentPosition + 1; x < array.size(); x++) {
      if (!excludeValues || array.get(x) != excluding) {
        currentPosition = x;
        return;
      }
    }
    currentPosition = Integer.MAX_VALUE;
  }

  @Override
  public boolean hasNext() {
    return currentPosition != Integer.MAX_VALUE;
  }

  @Override
  public int next() {
    final int result = currentPosition;
    readNext();
    return result;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
