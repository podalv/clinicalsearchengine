package com.podalv.search.datastructures.index;

import static com.podalv.utils.Logging.debug;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import com.googlecode.javaewah.datastructure.BitSet;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.search.index.StatisticsAgeGenerator;
import com.podalv.utils.memory.MemoryStatistics;

@SuppressWarnings("rawtypes")
public class StatisticsBuilderCompressed extends StatisticsBuilder {

  public static final int              BITSET_BUCKET_SIZE           = 5;
  private final IntKeyObjectMap        icd9ToPatientIds             = new IntKeyObjectMap();
  private final IntKeyObjectMap        icd10ToPatientIds            = new IntKeyObjectMap();
  private final IntKeyObjectMap        visitTypeToPatientIds        = new IntKeyObjectMap();
  private final IntKeyObjectMap        noteTypeToPatientIds         = new IntKeyObjectMap();
  private final IntKeyObjectMap        cptToPatientIds              = new IntKeyObjectMap();
  private final IntKeyObjectMap        snomedToPatientIds           = new IntKeyObjectMap();
  private final IntKeyObjectMap        departmentToPatientIds       = new IntKeyObjectMap();
  private final IntKeyObjectMap        vitalsToPatientIds           = new IntKeyObjectMap();
  private final IntKeyObjectMap        labsToPatientIds             = new IntKeyObjectMap();
  private final IntKeyObjectMap        rxToPatientIds               = new IntKeyObjectMap();
  private final IntKeyObjectMap        atcToPatientIds              = new IntKeyObjectMap();
  private final IntKeyObjectMap        tidToPatientIds              = new IntKeyObjectMap();
  private final IntKeyObjectMap        tidNegatedToPatientIds       = new IntKeyObjectMap();
  private final IntKeyObjectMap        tidFamilyHistoryToPatientIds = new IntKeyObjectMap();
  private final AtomicInteger          hashSetCnt                   = new AtomicInteger();
  private final AtomicInteger          bitSetCnt                    = new AtomicInteger();
  private final StatisticsAgeGenerator ageGenerator                 = new StatisticsAgeGenerator();

  public StatisticsBuilderCompressed(final int maxPatId) {
    super(maxPatId);
  }

  @Override
  public void recordIcd9(final int icd9, final int pid) {
    recordMap(icd9ToPatientIds, icd9, pid);
  }

  @SuppressWarnings({"unchecked"})
  private void recordMap(final IntKeyObjectMap map, final int key, final int pid) {
    synchronized (map) {
      final Object v = map.get(key);
      if (v == null) {
        hashSetCnt.incrementAndGet();
        map.put(key, new IntOpenHashSet(new int[] {pid}));
      }
      else {
        if (v instanceof IntOpenHashSet) {
          final IntOpenHashSet values = (IntOpenHashSet) v;
          if (!values.contains(pid)) {
            if (values.size() > MAX_PAT_ID) {
              hashSetCnt.decrementAndGet();
              bitSetCnt.incrementAndGet();
              final BitSet b = new BitSet();
              final int newVal = pid / BITSET_BUCKET_SIZE;
              b.set(newVal);
              final com.podalv.maps.primitive.IntIterator i = values.iterator();
              while (i.hasNext()) {
                b.set(i.next() / BITSET_BUCKET_SIZE);
              }
              values.clear();
              b.trim();
              map.put(key, b);
            }
            else {
              values.add(pid);
            }
          }
        }
        else if (v instanceof BitSet) {
          final BitSet b = (BitSet) v;
          final int newVal = pid / BITSET_BUCKET_SIZE;
          b.set(newVal);
        }
      }
    }
  }

  @Override
  public void recordNegatedTid(final int negatedTid, final int pid) {
    recordMap(tidNegatedToPatientIds, negatedTid, pid);
  }

  @Override
  public void recordFhTid(final int fhTid, final int pid) {
    recordMap(tidFamilyHistoryToPatientIds, fhTid, pid);
  }

  @Override
  public void recordTid(final int tid, final int pid) {
    recordMap(tidToPatientIds, tid, pid);
  }

  @Override
  public void recordAtc(final int atc, final int pid) {
    recordMap(atcToPatientIds, atc, pid);
  }

  @Override
  public void recordRx(final int rx, final int pid) {
    recordMap(rxToPatientIds, rx, pid);
  }

  @Override
  public void recordLabs(final int labs, final int pid) {
    recordMap(labsToPatientIds, labs, pid);
  }

  @Override
  public void recordVitals(final int vitals, final int pid) {
    recordMap(vitalsToPatientIds, vitals, pid);
  }

  @Override
  public void recordSnomed(final int snomed, final int pid) {
    recordMap(snomedToPatientIds, snomed, pid);
  }

  @Override
  public void recordCpt(final int cpt, final int pid) {
    recordMap(cptToPatientIds, cpt, pid);
  }

  @Override
  public void recordVisitType(final int visitType, final int pid) {
    recordMap(visitTypeToPatientIds, visitType, pid);
  }

  @Override
  public void recordNoteType(final int noteType, final int pid) {
    recordMap(noteTypeToPatientIds, noteType, pid);
  }

  @Override
  public void finalizeIcd9(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, icd9ToPatientIds, tgtMap);
  }

  private void finalizeMap(final PatientIdLists patientIdLists, final IntKeyObjectMap srcMap, final IntKeyIntOpenHashMap tgtMap) {
    debug("Finalizing statistics maps");
    debug("AVAILABLE MEMORY = " + MemoryStatistics.getAvailableMemory());
    try {
      final IntKeyObjectIterator iterator = srcMap.entries();
      int cnt = 0;
      while (iterator.hasNext()) {
        cnt++;
        if (cnt != 0 && cnt % 10 == 0) {
          debug(cnt + " / " + srcMap.size());
          debug("AVAILABLE MEMORY = " + MemoryStatistics.getAvailableMemory());
        }
        iterator.next();
        final IntOpenHashSet resultSet = IntOpenHashSet.create(patientIdLists.get(tgtMap.get(iterator.getKey())));
        if (iterator.getValue() instanceof BitSet) {
          final BitSet set = (BitSet) iterator.getValue();
          tgtMap.put(iterator.getKey(), patientIdLists.add(set));
          set.clear();
          set.trim();
          iterator.remove();
        }
        else if (iterator.getValue() instanceof IntOpenHashSet) {
          final IntOpenHashSet set = (IntOpenHashSet) iterator.getValue();
          resultSet.addAll((set).toArray());
          final int[] resultData = resultSet.toArray();
          Arrays.sort(resultData);
          tgtMap.put(iterator.getKey(), patientIdLists.add(resultData));
          set.clear();
          iterator.remove();
        }
      }
    }
    catch (final Exception e) {
      throw new RuntimeException(e.getMessage());
    }
    debug("Done");
    debug("AVAILABLE MEMORY = " + MemoryStatistics.getAvailableMemory());
  }

  @Override
  public void finalizeCpt(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, cptToPatientIds, tgtMap);
  }

  @Override
  public void finalizeVisitType(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, visitTypeToPatientIds, tgtMap);
  }

  @Override
  public void finalizeNoteType(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, noteTypeToPatientIds, tgtMap);
  }

  @Override
  public void finalizeSnomed(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, snomedToPatientIds, tgtMap);
  }

  @Override
  public void finalizeVitals(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, vitalsToPatientIds, tgtMap);
  }

  @Override
  public void finalizeLabs(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, labsToPatientIds, tgtMap);
  }

  @Override
  public void finalizeAtc(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, atcToPatientIds, tgtMap);
  }

  @Override
  public void finalizeTid(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, tidToPatientIds, tgtMap);
  }

  @Override
  public void finalizeTidNegated(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, tidNegatedToPatientIds, tgtMap);
  }

  @Override
  public void finalizeTidFh(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, tidFamilyHistoryToPatientIds, tgtMap);
  }

  @Override
  public void finalizeRx(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, rxToPatientIds, tgtMap);
  }

  @Override
  public String toString() {
    return "HSC = " + hashSetCnt.get() + ", BSC = " + bitSetCnt;
  }

  @Override
  public void recordIcd10(final int icd10, final int pid) {
    recordMap(icd10ToPatientIds, icd10, pid);
  }

  @Override
  public void finalizeIcd10(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, icd10ToPatientIds, tgtMap);
  }

  @Override
  public void recordDepartment(final int department, final int pid) {
    recordMap(departmentToPatientIds, department, pid);
  }

  @Override
  public void finalizeDepartment(final PatientIdLists patientIdLists, final IntKeyIntOpenHashMap tgtMap) {
    finalizeMap(patientIdLists, departmentToPatientIds, tgtMap);
  }

  @Override
  public void recordStartEnd(final int pid, final int start, final int end) {
    synchronized (ageGenerator) {
      ageGenerator.addRecord(pid, start, end);
    }
  }

  @Override
  public IntArrayList getPidStartEndOrderedByEnd() {
    return ageGenerator.getSortedByEnd();
  }

  @Override
  public IntArrayList getPidStartEndOrderedByStart() {
    return ageGenerator.getSortedByStart();
  }

}