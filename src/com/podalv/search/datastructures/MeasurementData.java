package com.podalv.search.datastructures;

import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.primitive.set.IntSet;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.objectdb.datastructures.PersistentIntKeyObjectMap;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.PayloadPointers;
import com.podalv.search.server.PatientDataDump;

public class MeasurementData {

  private final PersistentIntKeyObjectMap map;
  final MeasurementNameProvider           nameProvider;

  public MeasurementData(final PersistentIntKeyObjectMap map, final MeasurementNameProvider nameProvider) {
    this.map = map;
    this.nameProvider = nameProvider;
  }

  public PersistentIntKeyObjectMap getMap() {
    return map;
  }

  public boolean isEmpty() {
    return map.size() == 0;
  }

  public void extract(CsvWriter extractor, final IndexCollection indices) {
    extract(extractor, indices, null, false, false);
  }

  public void extract(final CsvWriter extractor, final IndexCollection indices, final int[] timeRanges, final boolean containsStart, final boolean containsEnd) {
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final IntArrayList list = (IntArrayList) iterator.getValue();

      for (int x = 0; x < list.size(); x += 2) {
        if (PatientDataDump.isInTimeRange(list.get(x + 1), list.get(x + 1), timeRanges, containsStart, containsEnd)) {
          extractor.write(nameProvider.getName(iterator.getKey()), list.get(x + 1), indices.getDoubleValue(iterator.getKey(), list.get(x)));
        }
      }
    }
  }

  public IntSet getUniqueKeyIds() {
    return map == null ? new IntOpenHashSet() : map.keySet();
  }

  /** Returns data in format: doubleValue, time
  *
  *
  * @param labId
  * @return
  */
  public IntArrayList getRawData(final int labId) {
    return (PersistentIntArrayList) map.get(labId);
  }

  public IntArrayList getAllTimes() {
    final IntArrayList result = new IntArrayList();
    final IntKeyMapIterator i = map.entries();
    while (i.hasNext()) {
      i.next();
      final PersistentIntArrayList list = (PersistentIntArrayList) i.getValue();
      if (PayloadPointers.containsInvalidEvents(list)) {
        continue;
      }
      for (int x = 0; x < list.size(); x += 2) {
        result.add(list.get(x + 1));
        result.add(list.get(x + 1));
      }
    }
    result.sort();
    return result;
  }

  public IntArrayList get(final IndexCollection indices, final int code, final double minValue, final double maxValue) {
    if (map != null && map.size() != 0) {
      final PersistentIntArrayList list = (PersistentIntArrayList) map.get(code);
      if (list != null) {
        final IntArrayList result = new IntArrayList();
        for (int x = 0; x < list.size(); x += 2) {
          final double val = indices.getDoubleValue(code, list.get(x));
          if (val >= minValue && val <= maxValue) {
            result.add(list.get(x + 1));
          }
        }
        return result.size() == 0 ? null : result;
      }
    }
    return null;
  }
}