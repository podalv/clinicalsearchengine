package com.podalv.search.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScriptContentResponse {

  @JsonProperty("script") private final String script;

  public ScriptContentResponse(final String script) {
    this.script = script;
  }

  public String getScript() {
    return script;
  }
}
