package com.podalv.search.datastructures;

public class CheckSyntaxRequest {

  private String command;
  private int    posX;
  private int    posY;

  public void setCommand(final String command) {
    this.command = command;
  }

  public void setPosX(final int posX) {
    this.posX = posX;
  }

  public void setPosY(final int posY) {
    this.posY = posY;
  }

  public String getCommand() {
    return command;
  }

  public int getPosX() {
    return posX;
  }

  public int getPosY() {
    return posY;
  }
}
