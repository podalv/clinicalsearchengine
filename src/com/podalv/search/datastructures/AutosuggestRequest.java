package com.podalv.search.datastructures;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AutosuggestRequest {

  @JsonProperty("query") private String      query;
  @JsonProperty("position") private int      position;
  @JsonProperty("converted") private boolean converted = false;
  @JsonProperty("patientIds") private int[]  patientIds;
  @JsonProperty("sorted") private boolean    sorted    = false;

  public AutosuggestRequest(final String query, final int position) {
    this.query = query;
    this.position = position;
  }

  public int getPosition() {
    return position;
  }

  public void setPatientIds(final int[] patientIds) {
    this.patientIds = patientIds;
  }

  public int[] getPatientIds() {
    if (!sorted && patientIds != null) {
      Arrays.sort(patientIds);
      sorted = true;
    }
    return patientIds;
  }

  private void convertQuery() {
    if (!converted) {
      query = query.toLowerCase();
      converted = true;
    }
  }

  public String getQuery() {
    convertQuery();
    return query;
  }

  public void setPosition(final int position) {
    this.position = position;
  }

  public void setQuery(final String query) {
    this.query = query;
  }

}