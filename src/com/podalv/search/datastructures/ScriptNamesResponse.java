package com.podalv.search.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScriptNamesResponse {

  @JsonProperty("scriptNames") private final String[] scriptNames;

  public ScriptNamesResponse(final String[] scriptNames) {
    this.scriptNames = scriptNames;
  }

  public String[] getScriptNames() {
    return scriptNames;
  }
}
