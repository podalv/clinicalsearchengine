package com.podalv.search.datastructures;

import com.podalv.maps.primitive.list.IntArrayList;

/** Deserialized patient object optimized for high search speed
 *
 * @author podalv
 *
 */
public interface Patient {

  public int getId();

  public int getStartTime();

  public int getEndTime();

  public IntArrayList getIcd9(final int icd9Code);

  public IntArrayList getIcd10(final int icd10Code);

  public IntArrayList getSnomed(final int snomedCode);

  public IntArrayList getCpt(final int cptCode);

  public IntArrayList getRxNorm(final int rxNormCode);

  public IntArrayList getUniqueIcd9Codes();

  public IntArrayList getUniqueDepartmentCodes();

  public IntArrayList getUniqueIcd10Codes();

  public IntArrayList getUniqueCptCodes();

  public IntArrayList getUniqueSnomedCodes();

  public IntArrayList getUniqueRxNormCodes();

  public boolean containsIcd9(final int icd9Code);

  public boolean containsIcd10(final int icd10Code);

  public boolean containsCpt(final int cptCode);

  public boolean containsSnomed(final int snomedCode);

  public boolean containsDepartment(final int departmentCode);

  public boolean containsLabs();

  public boolean containsVisitTypes();

  public boolean containsFamilyHistoryTerms();

  public boolean containsNegatedTerms();

  public boolean containsPositiveTerms();

  public boolean containsVisitType(final int visitTypeId);

  public boolean containsRxNorm(final int rxNormCode);

  public boolean containsPositiveTid(final int tid);

  public boolean containsNegatedTid(final int tid);

  public boolean containsFamilyHistoryTid(final int tid);

  public IntArrayList getPositiveTerms(final int termId);

  public IntArrayList getNegatedTerms(final int termId);

  public IntArrayList getFamilyHistoryTerms(final int termId);

  public IntArrayList getPayload(final int id);

  public IntArrayList getDepartment(final int departmentId);

  public IntArrayList getAgeRanges();

  public IntArrayList getEncounterDays();

  public int[] getUniqueVisitTypes();

  public short getMinYear();

  public short getMaxYear();

  public IntArrayList getVisitTypes(int type);

  public int[] getYear(int minYear, int maxYear);

  public MeasurementData getVitals();

  public MeasurementData getLabValues();

  public LabsData getLabs();

  public IntArrayList getLabTimes(final int labId);

  public IntArrayList getRxNormCodesFromAtc(final int atc);

  public IntArrayList getUniqueNotePayloadIds();

  public IntArrayList getNoteTimePointsFromNoteType(final int noteTypeId);

  public int[] getUniquePositiveTids();

  public int[] getUniqueNegatedTids();

  public int[] getUniqueFamilyHistoryTids();

  public int[] getUniqueLabIds();

  public boolean isClosed();

  public int getYear(int time);

}
