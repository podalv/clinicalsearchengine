package com.podalv.search.datastructures;

import static com.podalv.extractor.stride6.Common.BYTE_SIZE_IN_BYTES;
import static com.podalv.extractor.stride6.Common.INT_SIZE_IN_BYTES;
import static com.podalv.extractor.stride6.Common.SHORT_SIZE_IN_BYTES;
import static com.podalv.extractor.stride6.Common.UNSIGNED_BYTE_MAX_VALUE;
import static com.podalv.extractor.stride6.Common.UNSIGNED_SHORT_MAX_VALUE;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.activation.UnsupportedDataTypeException;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;
import com.podalv.extractor.datastructures.records.DepartmentRecord;
import com.podalv.extractor.datastructures.records.LabsRecord;
import com.podalv.extractor.datastructures.records.MedRecord;
import com.podalv.extractor.datastructures.records.ObservationRecord;
import com.podalv.extractor.datastructures.records.PatientRecord;
import com.podalv.extractor.datastructures.records.TermRecord;
import com.podalv.extractor.datastructures.records.VisitRecord;
import com.podalv.extractor.datastructures.records.VitalsRecord;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.Stride6DatabaseDownload;
import com.podalv.input.Input;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.primitive.set.IntSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.OffHeapObject;
import com.podalv.objectdb.SearchModel;
import com.podalv.objectdb.datastructures.CompressedOffHeapIntKeyIntArrayMapBuilder;
import com.podalv.objectdb.datastructures.OffHeapIntArrayListAutomated;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.objectdb.datastructures.PersistentIntKeyObjectMap;
import com.podalv.output.Output;
import com.podalv.search.datastructures.Enums.PATIENT_PERSISTENCE_ERROR;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.language.evaluation.iterators.CompressedPayloadIterator;
import com.podalv.utils.datastructures.LongMutable;
import com.podalv.utils.datastructures.SortPair;
import com.podalv.utils.datastructures.SortPairSingle;

/** Contains patient's timelines
 *
 * @author podalv
 *
 */
public class PatientBuilder extends SearchModel implements OffHeapObject, ObjectWithPayload, Closeable {

  private static final int                               MIN_YEAR                                = 1900;
  private static final int                               MAX_YEAR                                = 2020;
  private static final int                               CURRENT_VERSION                         = 3;
  private boolean                                        closed                                  = false;
  private long                                           startPositionInCache                    = -1;
  public static final int                                INVALID_PAYLOAD                         = 0;
  private int                                            patientId;
  private final IndexCollection                          indices;
  private transient int                                  startTime                               = Integer.MAX_VALUE;
  protected transient int                                endTime                                 = Integer.MIN_VALUE;
  private final HashSet<SortPair<Integer, Integer>>      ageRangeSet                             = new HashSet<>();
  private PersistentIntArrayList                         ageRanges                               = new PersistentIntArrayList();

  public static final byte                               OFFSET_ENCODING_TYPE_BYTE               = 1;
  public static final byte                               OFFSET_ENCODING_TYPE_INT                = 0;
  public static final byte                               OFFSET_ENCODING_TYPE_SHORT              = 2;

  public static final byte                               PAYLOAD_IS_EMPTY                        = 3;

  public static final int                                TERM_TIME_ID_POSITION_IN_PAYLOAD_DATA   = 0;
  public static final int                                NOTE_ID_POSITION_IN_PAYLOAD_DATA        = 1;
  public static final int                                NOTE_TYPE_ID_POSITION_IN_PAYLOAD_DATA   = 2;
  public static final int                                PRIMARY_ID_POSITION_IN_PAYLOAD_DATA     = 2;
  public static final int                                DRUG_ROUTE_ID_POSITION_IN_PAYLOAD_DATA  = 2;
  public static final int                                DRUG_STATUS_ID_POSITION_IN_PAYLOAD_DATA = 3;
  public static final int                                PRIMARY_TRUE_HIERARCHY_TRUE             = 1;
  public static final int                                PRIMARY_FALSE_HIERARCHY_FALSE           = 0;
  public static final int                                PRIMARY_TRUE_HIERARCHY_FALSE            = 2;
  public static final int                                PRIMARY_FALSE_HIERARCHY_TRUE            = 3;

  private int                                            payloadId                               = 1;

  //containts timeIndex => startTime, endTime, payLoad, payLoad data
  private transient final HashMap<IntArrayList, Integer> payloadToId                             = new HashMap<>();

  PersistentIntKeyObjectMap                              idToPayload                             = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  //timeVisitId = structured byte[] array containing [timeIndex]
  //payload contains [startTime, endTime, visitType]
  private final PersistentIntKeyObjectMap                icd9ToTimeVisitId                       = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
  private final PersistentIntKeyObjectMap                departmentToTimeVisitId                 = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
  private final PersistentIntKeyObjectMap                icd10ToTimeVisitId                      = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
  private final PersistentIntKeyObjectMap                cptToTimeVisitId                        = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
  private final PersistentIntKeyObjectMap                snomedToTimeVisitId                     = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
  private final PersistentIntKeyObjectMap                rxNormToTimeVisitId                     = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  //non-negated + non_family_history terms
  private final PersistentIntKeyObjectMap                positiveTermToTime                      = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  //negated terms
  private final PersistentIntKeyObjectMap                negatedTermToTime                       = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  //family history terms
  private final PersistentIntKeyObjectMap                familyHistoryTermToTime                 = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final PersistentIntArrayList                   uniqueNotePayloadIds                    = new PersistentIntArrayList();

  // list of unique times [start, end] for the patient when they had an encounter in minutes
  private final IntArrayList                             encounterPayloads                       = new IntArrayList();
  private final PersistentIntArrayList                   encounterRanges                         = new PersistentIntArrayList();

  private final PersistentIntKeyObjectMap                visitTypeToTime                         = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final PersistentIntKeyObjectMap                yearToStartEnd                          = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private short[]                                        years                                   = new short[0];

  private PersistentIntKeyObjectMap                      vitals                                  = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final PersistentIntKeyObjectMap                labValues                               = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final PersistentIntKeyObjectMap                labsWithTypeId                          = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final PersistentIntKeyObjectMap                labsIdToTime                            = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final PersistentIntKeyObjectMap                atcToRxNorm                             = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final IntArrayList                             encounterPayloadComputationData         = new IntArrayList();

  private final PersistentIntKeyObjectMap                noteTypeToTimePoints                    = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);

  private final HashSet<Object>                          recordedEvents                          = new HashSet<>();

  public static boolean isPrimaryIcdCode(final int primaryId) {
    return primaryId == PRIMARY_TRUE_HIERARCHY_FALSE || primaryId == PRIMARY_TRUE_HIERARCHY_TRUE;
  }

  public static boolean isOriginalIcdCode(final int primaryId) {
    return primaryId == PRIMARY_TRUE_HIERARCHY_FALSE || primaryId == PRIMARY_FALSE_HIERARCHY_FALSE;
  }

  public PatientBuilder() {
    this.indices = null;
    payloadToId.put(PersistentIntArrayList.create(new int[] {}), 0);
    idToPayload.put(0, PersistentIntArrayList.create(new int[] {}));
  }

  public void setStartEndTime(final int start, final int end) {
    startTime = start;
    endTime = end;
  }

  private int[] getComputedStartEnd(final IntArrayList payloads) {
    final IntArrayList result = new IntArrayList();
    final CompressedPayloadIterator i = new CompressedPayloadIterator(this, payloads);
    while (i.hasNext()) {
      i.next();
      result.add(i.getStartId());
      result.add(i.getEndId());
    }
    return result.toArray();
  }

  public PatientBuilder(final IndexCollection indices) {
    this.indices = indices;
    payloadToId.put(PersistentIntArrayList.create(new int[] {}), 0);
    idToPayload.put(0, PersistentIntArrayList.create(new int[] {}));
  }

  private void recordYear(final int year, final int start, final int end) {
    if (year > MIN_YEAR && year < MAX_YEAR) {
      PersistentIntArrayList data = (PersistentIntArrayList) yearToStartEnd.get(year);
      if (data == null) {
        data = new PersistentIntArrayList();
        data.add(start);
        data.add(end);
        yearToStartEnd.put(year, data);
      }
      else {
        data.set(0, Math.min(start, data.get(0)));
        data.set(1, Math.max(end, data.get(1)));
      }
    }
  }

  public void setVitals(final PersistentIntKeyObjectMap map) {
    this.vitals = map;
  }

  public void recordLabs(final PatientRecord<LabsRecord> labsRecords) {
    Collections.sort(labsRecords.getRecords());
    for (final LabsRecord record : labsRecords.getRecords()) {
      if (record == null || record.getCode() == null) {
        continue;
      }
      final String code = record.getCode().trim();
      if (!code.isEmpty()) {
        if (!recordedEvents.contains(record)) {
          if (record.isValid()) {
            recordedEvents.add(record);
            boolean fnd = false;
            if (record.getCalculatedValue() != null && !record.getCalculatedValue().trim().isEmpty() && !record.getCalculatedValue().equals(LabsRecord.UNDEFINED)) {
              final int labId = indices.addLabs(record.getCode());
              PersistentIntArrayList list = (PersistentIntArrayList) labsWithTypeId.get(labId);
              if (list == null) {
                list = new PersistentIntArrayList();
                labsWithTypeId.put(labId, list);
              }
              if (list.size() > 1 && list.get(0) == INVALID_PAYLOAD && list.get(1) == INVALID_PAYLOAD) {
                continue;
              }
              final int calculatedValue = indices.getLabValueId(record.getCalculatedValue());
              final int payloadId = getPayLoadId(record.getTime());
              for (int x = 0; x < list.size(); x += 2) {
                if (list.get(x) == calculatedValue && list.get(x + 1) == payloadId) {
                  fnd = true;
                  break;
                }
              }
              if (!fnd) {
                recordTime(record.getYear(), record.getTime(), record.getTime());
                list.add(calculatedValue);
                list.add(payloadId);
              }
            }
            if (record.getValue() > Stride6DatabaseDownload.UNDEFINED) {
              final int labId = indices.addLabs(record.getCode());
              PersistentIntArrayList valueList = (PersistentIntArrayList) labValues.get(labId);
              if (valueList == null) {
                valueList = new PersistentIntArrayList();
                labValues.put(labId, valueList);
              }
              if (valueList.size() > 1 && valueList.get(0) == INVALID_PAYLOAD && valueList.get(1) == INVALID_PAYLOAD) {
                continue;
              }
              final int valueIndex = indices.getDoubleIndex(labId, record.getValue());
              fnd = false;
              final int payloadId = getPayLoadId(record.getTime());
              for (int x = 0; x < valueList.size(); x += 2) {
                if (valueList.get(x) == valueIndex && valueList.get(x + 1) == payloadId) {
                  fnd = true;
                  break;
                }
              }
              if (!fnd) {
                recordTime(record.getYear(), record.getTime(), record.getTime());
                valueList.add(valueIndex);
                valueList.add(payloadId);
              }
            }
          }
        }
      }
    }
  }

  public IntIterator getVisitTypes() {
    return visitTypeToTime.keySet().iterator();
  }

  public void recordVitals(final PatientRecord<VitalsRecord> vitalsRecord) {
    Collections.sort(vitalsRecord.getRecords());
    for (final VitalsRecord record : vitalsRecord.getRecords()) {
      if (!record.getDescription().trim().isEmpty()) {
        if (!recordedEvents.contains(record)) {
          recordedEvents.add(record);
          if (record.isValid()) {
            recordTime(record.getYear(), record.getAge(), record.getAge());
            final int codeId = indices.addVitals(record.getDescription());
            PersistentIntArrayList list = (PersistentIntArrayList) vitals.get(codeId);
            if (list == null) {
              list = new PersistentIntArrayList();
              vitals.put(codeId, list);
            }
            list.add(indices.getDoubleIndex(codeId, record.getValue()));
            list.add(record.getAge());
          }
        }
      }
    }
  }

  public void recordTime(final int year, final int start, final int end) {
    this.startTime = Math.min(start, startTime);
    this.endTime = Math.max(end, endTime);
    Common.addAgeRange(ageRangeSet, start, end);
    if (year >= 0) {
      recordYear(year, start, end);
    }
  }

  private boolean isPayloadArrayEmpty(final PersistentIntArrayList list) {
    return list.size() == 0 || list.size() == 1 && list.get(0) == 0;
  }

  private boolean isEmpty(final PersistentIntKeyObjectMap map) {
    if (map.size() == 0) {
      return true;
    }
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (!isPayloadArrayEmpty((PersistentIntArrayList) iterator.getValue())) {
        return false;
      }
    }
    return true;
  }

  private boolean isLabEmpty(final PersistentIntKeyObjectMap map) {
    if (map.size() == 0) {
      return true;
    }
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final PersistentIntArrayList value = ((PersistentIntArrayList) iterator.getValue());
      if (value.size() > 2 || (value.get(0) != 0 || value.get(1) != 0)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isEmpty() {
    return ((isEmpty(rxNormToTimeVisitId) && isEmpty(cptToTimeVisitId) && isEmpty(icd9ToTimeVisitId) && isEmpty(positiveTermToTime) && isEmpty(negatedTermToTime) && isEmpty(
        familyHistoryTermToTime) && isLabEmpty(labsWithTypeId) && vitals.size() == 0 && isLabEmpty(labValues) && isEmpty(snomedToTimeVisitId) && isEmpty(departmentToTimeVisitId)
        && isEmpty(icd10ToTimeVisitId)));
  }

  private void recordSingleTerm(final TermRecord term, final PersistentIntKeyObjectMap map) {
    PersistentIntArrayList list = (PersistentIntArrayList) map.get(term.getTermId());
    if (list == null) {
      list = new PersistentIntArrayList();
      map.put(term.getTermId(), list);
    }
    if (term.isValid() && (list.size() == 0 || list.get(0) != INVALID_PAYLOAD)) {
      final PersistentIntArrayList data = new PersistentIntArrayList();
      data.add(term.getAge());
      data.add(term.getNoteId());
      data.add(term.getNoteTypeId());
      PersistentIntArrayList noteTimePoints = (PersistentIntArrayList) noteTypeToTimePoints.get(term.getNoteTypeId());
      if (noteTimePoints == null) {
        noteTimePoints = new PersistentIntArrayList();
        noteTypeToTimePoints.put(term.getNoteTypeId(), noteTimePoints);
      }
      noteTimePoints.add(term.getAge());
      if (data.get(NOTE_ID_POSITION_IN_PAYLOAD_DATA) != term.getNoteId()) {
        throw new RuntimeException("NOTE_ID_POSITION_IN_PAYLOAD_DATA is not set correctly");
      }
      list.add(getPayLoadId(data));
    }
    else {
      list.clear();
      list.add(INVALID_PAYLOAD);
    }
  }

  private int getPayLoadId(final int value) {
    final PersistentIntArrayList list = new PersistentIntArrayList();
    list.add(value);
    return getPayLoadId(list);
  }

  private int getPayLoadId(final PersistentIntArrayList data) {
    if (!payloadToId.containsKey(data)) {
      payloadToId.put(data, payloadId);
      idToPayload.put(payloadId, data);
      payloadId++;
    }
    else {
      return payloadToId.get(data);
    }
    return payloadId - 1;
  }

  private void recordUniqueNoteIds(final IntKeyObjectMap<IntOpenHashSet> ageToNoteIdMap, final PatientRecord<TermRecord> terms) {
    final IntArrayList ages = new IntArrayList();
    ages.addAll(ageToNoteIdMap.keySet().toArray());
    ages.sort();

    final IntKeyIntOpenHashMap termIdToTermType = new IntKeyIntOpenHashMap();
    for (final TermRecord record : terms.getRecords()) {
      if (record.isValid()) {
        if (termIdToTermType.containsKey(record.getNoteId()) && termIdToTermType.get(record.getNoteId()) != record.getNoteTypeId()) {
          throw new RuntimeException("There are notes with multiple note types");
        }
        termIdToTermType.put(record.getNoteId(), record.getNoteTypeId());
      }
    }

    for (int x = 0; x < ages.size(); x++) {
      final IntArrayList noteIds = new IntArrayList(ageToNoteIdMap.get(ages.get(x)).toArray());
      noteIds.sort();
      for (int y = 0; y < noteIds.size(); y++) {
        final PersistentIntArrayList list = new PersistentIntArrayList();
        list.add(ages.get(x));
        list.add(noteIds.get(y));
        list.add(termIdToTermType.get(noteIds.get(y)));
        uniqueNotePayloadIds.add(getPayLoadId(list));
      }
    }
  }

  public void recordDepartment(final PatientRecord<DepartmentRecord> department) {
    if (department != null) {
      Collections.sort(department.getRecords());
      for (final DepartmentRecord record : department.getRecords()) {
        if (record.getDepartment() != null && !recordedEvents.contains(record)) {
          recordedEvents.add(record);
          final int departmentId = indices.addDepartmentCode(record.getDepartment());
          PersistentIntArrayList list = (PersistentIntArrayList) departmentToTimeVisitId.get(departmentId);
          if (list == null) {
            list = new PersistentIntArrayList();
            departmentToTimeVisitId.put(departmentId, list);
          }
          final PersistentIntArrayList payload = new PersistentIntArrayList();
          if (record.isValid() && (list.size() == 0 || list.get(0) != INVALID_PAYLOAD)) {
            payload.add(record.getAge());
            final int payloadId = getPayLoadId(payload);
            recordTime(record.getYear(), record.getAge(), record.getAge());
            boolean containsPayload = false;
            for (int x = 0; x < list.size(); x++) {
              if (list.get(x) == payloadId) {
                containsPayload = true;
                break;
              }
            }
            if (!containsPayload) {
              list.add(payloadId);
            }
          }
          else {
            list.clear();
            list.add(INVALID_PAYLOAD);
          }
        }
      }
    }
  }

  public void recordTerms(final PatientRecord<TermRecord> terms) throws UnsupportedDataTypeException {
    if (terms != null) {
      Collections.sort(terms.getRecords());
      final IntKeyObjectMap<IntOpenHashSet> ageToNoteIdMap = new IntKeyObjectMap<>();
      TermRecord prevTerm = null;
      for (final TermRecord i : terms.getRecords()) {
        if (!recordedEvents.contains(i)) {
          recordedEvents.add(i);
          if (i.isValid()) {
            IntOpenHashSet set = ageToNoteIdMap.get(i.getAge());
            if (set == null) {
              set = new IntOpenHashSet();
              ageToNoteIdMap.put(i.getAge(), set);
            }
            set.add(i.getNoteId());
            recordTime(i.getYear(), i.getAge(), i.getAge());
          }
          if (prevTerm == null || !i.equals(prevTerm)) {
            if (i.getNegated() == 0 && i.getFamilyHistory() == 0) {
              recordSingleTerm(i, positiveTermToTime);
            }
            if (i.getNegated() == 1) {
              recordSingleTerm(i, negatedTermToTime);
            }
            if (i.getFamilyHistory() == 1) {
              recordSingleTerm(i, familyHistoryTermToTime);
            }
          }
          prevTerm = i;
        }
      }
      recordUniqueNoteIds(ageToNoteIdMap, terms);
    }
  }

  public void recordAtc(final IntKeyObjectMap<IntOpenHashSet> atcCodeToRxNormMap) {
    final IntKeyObjectIterator<IntOpenHashSet> i = atcCodeToRxNormMap.entries();
    while (i.hasNext()) {
      i.next();
      atcToRxNorm.put(i.getKey(), PersistentIntArrayList.create(i.getValue()));
    }
  }

  public void recordMeds(final PatientRecord<MedRecord> meds) {
    if (meds != null) {
      Collections.sort(meds.getRecords());
      for (final MedRecord i : meds.getRecords()) {
        if (!recordedEvents.contains(i)) {
          recordedEvents.add(i);
          final int rxNormCode = i.getRxCui();
          PersistentIntArrayList data = (PersistentIntArrayList) rxNormToTimeVisitId.get(rxNormCode);
          if (data == null) {
            data = new PersistentIntArrayList();
            rxNormToTimeVisitId.put(rxNormCode, data);
          }
          if (i.isValid() && (data.size() == 0 || data.get(0) != INVALID_PAYLOAD)) {
            recordTime(i.getYear(), i.getStart(), i.getStart() + i.getDuration());
            final PersistentIntArrayList payload = new PersistentIntArrayList();
            payload.add(i.getStart());
            payload.add(i.getStart() + i.getDuration());
            payload.add(indices.addDrugRouteId(i.getRoute()));
            payload.add(indices.addDrugStatusId(i.getStatus()));
            data.add(getPayLoadId(payload));
          }
          else {
            data.clear();
            data.add(INVALID_PAYLOAD);
          }
        }
      }
    }
  }

  private int getPrimaryIndicator(final VisitRecord record) {
    if (record.isPrimary() && record.isGeneratedByHierarchy()) {
      return PRIMARY_TRUE_HIERARCHY_TRUE;
    }
    else if (!record.isPrimary() && record.isGeneratedByHierarchy()) {
      return PRIMARY_FALSE_HIERARCHY_TRUE;
    }
    else if (record.isPrimary() && !record.isGeneratedByHierarchy()) {
      return PRIMARY_TRUE_HIERARCHY_FALSE;
    }
    return PRIMARY_FALSE_HIERARCHY_FALSE;
  }

  private void recordIcd9Code(final VisitRecord i) {
    if ((i.getCode() != null && !i.getCode().isEmpty()) || (i.getCodeId() != -1)) {
      final int icdCode = i.getCode() == null ? i.getCodeId() : indices.addIcd9Code(i.getCode());
      PersistentIntArrayList data = (PersistentIntArrayList) icd9ToTimeVisitId.get(icdCode);
      if (data == null) {
        data = new PersistentIntArrayList();
        icd9ToTimeVisitId.put(icdCode, data);
      }
      if (i.isValid() && (data.size() == 0 || data.get(0) != INVALID_PAYLOAD)) {
        recordTime(i.getYear(), i.getAge(), i.getAge() + i.getDuration());
        final PersistentIntArrayList payload = new PersistentIntArrayList();
        payload.add(i.getAge());
        payload.add(i.getAge() + i.getDuration());
        payload.add(getPrimaryIndicator(i));
        final int payloadId = getPayLoadId(payload);
        data.add(payloadId);
        encounterPayloads.add(payloadId);
      }
      else {
        data.clear();
        data.add(INVALID_PAYLOAD);
      }
    }
  }

  private void recordIcd10Code(final VisitRecord i) {
    if ((i.getCode() != null && !i.getCode().isEmpty()) || (i.getCodeId() != -1)) {
      final int icdCode = i.getCode() == null ? i.getCodeId() : indices.addIcd10Code(i.getCode());
      PersistentIntArrayList data = (PersistentIntArrayList) icd10ToTimeVisitId.get(icdCode);
      if (data == null) {
        data = new PersistentIntArrayList();
        icd10ToTimeVisitId.put(icdCode, data);
      }
      if (i.isValid() && (data.size() == 0 || data.get(0) != INVALID_PAYLOAD)) {
        recordTime(i.getYear(), i.getAge(), i.getAge() + i.getDuration());
        final PersistentIntArrayList payload = new PersistentIntArrayList();
        payload.add(i.getAge());
        payload.add(i.getAge() + i.getDuration());
        payload.add(getPrimaryIndicator(i));
        final int payloadId = getPayLoadId(payload);
        data.add(payloadId);
        encounterPayloads.add(payloadId);
      }
      else {
        data.clear();
        data.add(INVALID_PAYLOAD);
      }
    }
  }

  public void recordVisit(final PatientRecord<VisitRecord> visits) {
    Collections.sort(visits.getRecords());
    for (final VisitRecord i : visits.getRecords()) {
      if (!recordedEvents.contains(i)) {
        recordedEvents.add(i);
        recordVisitType(i);
        if ((i.getCode() != null && !i.getCode().isEmpty()) || i.getCodeId() != -1) {
          if (i.getSab() != null && i.getSab().equals("SNOMED") && (i.getCode() != null || i.getCodeId() != -1)) {
            recordSnomedCode(i);
          }
          else if (i.isCptCode()) {
            recordCptCode(i);
          }
          else if (i.isIcd9Code()) {
            recordIcd9Code(i);
          }
          else if (i.isIcd10Code()) {
            recordIcd10Code(i);
          }
        }
      }
    }
  }

  public void recordObservation(final PatientRecord<ObservationRecord> observations) {
    Collections.sort(observations.getRecords());
    for (final ObservationRecord i : observations.getRecords()) {
      if ((i.getCode() != null && !i.getCode().isEmpty()) || i.getCodeId() != -1) {
        if (!recordedEvents.contains(i)) {
          recordedEvents.add(i);
          if (i.getSab().equals("SNOMED")) {
            recordSnomedCode(i);
          }
          else if (i.isCptCode()) {
            recordCptCode(i);
          }
          else if (i.isIcd9Code()) {
            recordIcd9Code(i);
          }
          else if (i.isIcd10Code()) {
            recordIcd10Code(i);
          }
        }
      }
    }
  }

  private void recordVisitType(final VisitRecord i) {
    final int visitType = indices.addVisitTypeCode(i.getSrc_visit());
    PersistentIntArrayList list = (PersistentIntArrayList) visitTypeToTime.get(visitType);
    if (list == null) {
      list = new PersistentIntArrayList();
      visitTypeToTime.put(visitType, list);
    }
    if (i.isValid() && (list.size() == 0 || list.get(0) != INVALID_PAYLOAD)) {
      final PersistentIntArrayList payload = new PersistentIntArrayList();
      payload.add(i.getAge());
      payload.add(i.getAge() + i.getDuration());
      final int payloadId = getPayLoadId(payload);
      recordTime(i.getYear(), i.getAge(), i.getAge() + i.getDuration());
      boolean containsPayload = false;
      for (int x = 0; x < list.size(); x++) {
        if (list.get(x) == payloadId) {
          containsPayload = true;
          break;
        }
      }
      if (!containsPayload) {
        list.add(payloadId);
        encounterPayloadComputationData.add(payloadId);
      }
    }
    else {
      list.clear();
      list.add(INVALID_PAYLOAD);
    }
  }

  private void recordSnomedCode(final VisitRecord i) {
    if ((i.getCode() != null && !i.getCode().isEmpty()) || (i.getCodeId() != -1)) {
      final int snomedCode = i.getCode() == null ? i.getCodeId() : indices.addSnomedCode(i.getCode());
      PersistentIntArrayList data = (PersistentIntArrayList) snomedToTimeVisitId.get(snomedCode);
      if (data == null) {
        data = new PersistentIntArrayList();
        snomedToTimeVisitId.put(snomedCode, data);
      }
      if (i.isValid() && (data.size() == 0 || data.get(0) != INVALID_PAYLOAD)) {
        recordTime(i.getYear(), i.getAge(), i.getAge() + i.getDuration());
        final PersistentIntArrayList payload = new PersistentIntArrayList();
        payload.add(i.getAge());
        payload.add(i.getAge() + i.getDuration());
        final int payloadId = getPayLoadId(payload);
        data.add(payloadId);
        encounterPayloads.add(payloadId);
      }
      else {
        data.clear();
        data.add(INVALID_PAYLOAD);
      }
    }
  }

  private void recordCptCode(final VisitRecord i) {
    if ((i.getCode() != null && !i.getCode().isEmpty()) || (i.getCodeId() != -1)) {
      final int cptCode = i.getCode() == null ? i.getCodeId() : indices.addCptCode(i.getCode());
      PersistentIntArrayList data = (PersistentIntArrayList) cptToTimeVisitId.get(cptCode);
      if (data == null) {
        data = new PersistentIntArrayList();
        cptToTimeVisitId.put(cptCode, data);
      }
      if (i.isValid() && (data.size() == 0 || data.get(0) != INVALID_PAYLOAD)) {
        recordTime(i.getYear(), i.getAge(), i.getAge() + i.getDuration());
        final PersistentIntArrayList payload = new PersistentIntArrayList();
        payload.add(i.getAge());
        payload.add(i.getAge() + i.getDuration());
        final int payloadId = getPayLoadId(payload);
        data.add(payloadId);
        encounterPayloads.add(payloadId);
      }
      else {
        data.clear();
        data.add(INVALID_PAYLOAD);
      }
    }
  }

  @Override
  public int getId() {
    return patientId;
  }

  private PATIENT_PERSISTENCE_ERROR canBeSaved() {
    IntKeyMapIterator iterator = icd9ToTimeVisitId.entries();
    if (icd9ToTimeVisitId.size() > 255 * 255) {
      return PATIENT_PERSISTENCE_ERROR.TOO_MANY_ICD9_CODES;
    }
    while (iterator.hasNext()) {
      iterator.next();
      if (((PersistentIntArrayList) iterator.getValue()).size() > 255 * 255) {
        return PATIENT_PERSISTENCE_ERROR.TOO_MANY_EVENTS_PER_ICD9_CODE;
      }
    }

    iterator = cptToTimeVisitId.entries();
    if (cptToTimeVisitId.size() > 255 * 255) {
      return PATIENT_PERSISTENCE_ERROR.TOO_MANY_CPT_CODES;
    }
    while (iterator.hasNext()) {
      iterator.next();
      if (((PersistentIntArrayList) iterator.getValue()).size() > 255 * 255) {
        return PATIENT_PERSISTENCE_ERROR.TOO_MANY_EVENTS_PER_CPT_CODE;
      }
    }

    return PATIENT_PERSISTENCE_ERROR.OK;
  }

  private long saveMap(final OffHeapMemory memory, final long position, final long indexPosition, final PersistentIntKeyObjectMap map) {
    final CompressedOffHeapIntKeyIntArrayMapBuilder builder = new CompressedOffHeapIntKeyIntArrayMapBuilder(memory, position);
    final IntKeyMapIterator i = map.entries();
    while (i.hasNext()) {
      i.next();
      builder.put(i.getKey(), i.getValue());
    }
    if (map.size() > 0) {
      builder.save();
      if (indexPosition >= 0) {
        memory.putInt(indexPosition, (int) (position - startPositionInCache));
      }
      return builder.getEndPosition();
    }
    else {
      if (indexPosition >= 0) {
        memory.putInt(indexPosition, -1);
      }
      return position;
    }
  }

  private void savePayload(final Output output) throws IOException {
    final PersistentIntKeyObjectMap map = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
    final Iterator<Entry<IntArrayList, Integer>> iterator = payloadToId.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<IntArrayList, Integer> entry = iterator.next();
      map.put(entry.getValue(), entry.getKey());
    }
    map.save(output);
  }

  @Override
  public void save(final Output output) {
    try {
      final PATIENT_PERSISTENCE_ERROR errorResult = canBeSaved();
      if (!errorResult.equals(PATIENT_PERSISTENCE_ERROR.OK)) {
        throw new IndexOutOfBoundsException("Patient " + patientId + " too big to be saved '" + errorResult.toString());
      }
      output.writeInt(getCurrentVersion());
      output.writeInt(startTime);
      output.writeInt(endTime);
      ageRanges = Common.compressAgeRangesIntArrayList(ageRangeSet);
      savePayload(output);
      saveIntKeyIntArrayListMap(output, icd9ToTimeVisitId);
      saveIntKeyIntArrayListMap(output, icd10ToTimeVisitId);
      saveIntKeyIntArrayListMap(output, cptToTimeVisitId);
      saveIntKeyIntArrayListMap(output, snomedToTimeVisitId);
      saveIntKeyIntArrayListMap(output, departmentToTimeVisitId);
      saveIntKeyIntArrayListMap(output, rxNormToTimeVisitId);
      saveIntKeyIntArrayListMap(output, atcToRxNorm);
      saveIntKeyIntArrayListMap(output, visitTypeToTime);
      saveIntKeyIntArrayListMap(output, noteTypeToTimePoints);
      saveTerms(output, positiveTermToTime);
      saveTerms(output, negatedTermToTime);
      saveTerms(output, familyHistoryTermToTime);
      ageRanges.save(output);
      encounterRanges.save(output);
      yearToStartEnd.save(output);
      writeYears(output);
      vitals.save(output);
      labValues.save(output);
      labsWithTypeId.save(output);
      labsIdToTime.save(output);
      uniqueNotePayloadIds.save(output);
    }
    catch (final Exception e) {
      e.printStackTrace();
      throw new UnsupportedOperationException();
    }
  }

  private long saveLabsOffHeap(final OffHeapMemory mem, final long position, final long indexPosition) throws IOException {
    if (labsWithTypeId.size() == 0) {
      mem.putInt(indexPosition, -1);
      return position;
    }
    mem.putInt(indexPosition, (int) (position - startPositionInCache));
    final LabsOffHeapData labsData = new LabsOffHeapData(mem, position, this, Common.calculateCompressionType(payloadToId.size() == 1 ? idToPayload.size() : 0));
    return labsData.save(labsWithTypeId);
  }

  private long saveMeasurementsOffHeap(final OffHeapMemory mem, final PersistentIntKeyObjectMap map, final DATA_TYPE maxKeySize, long position, final long indexPosition)
      throws IOException {
    if (map == null || map.size() == 0) {
      mem.putInt(indexPosition, -1);
    }
    else {
      mem.putInt(indexPosition, (int) (position - startPositionInCache));
      if (maxKeySize == DATA_TYPE.BYTE) {
        mem.putUnsignedByte(position++, (short) map.size());
      }
      else if (maxKeySize == DATA_TYPE.SHORT) {
        mem.putUnsignedShort(position, map.size());
        position += 2;
      }
      else if (maxKeySize == DATA_TYPE.INT) {
        mem.putUnsignedInt(position, map.size());
        position += 4;
      }
      else {
        throw new UnsupportedOperationException("DATA_TYPE " + maxKeySize + " is not supported");
      }
      final IntKeyMapIterator iterator = map.entries();
      while (iterator.hasNext()) {
        iterator.next();
        if (iterator.getKey() > 255 && maxKeySize == DATA_TYPE.BYTE) {
          throw new UnsupportedOperationException("Number of vitals readings is too big. Maximum 256 supported. Found" + iterator.getKey() + " in patient " + patientId);
        }
        if (iterator.getKey() > 65535 && maxKeySize == DATA_TYPE.SHORT) {
          throw new UnsupportedOperationException("Number of vitals readings is too big. Maximum 65536 supported. Found" + iterator.getKey() + " in patient " + patientId);
        }
        if (maxKeySize == DATA_TYPE.BYTE) {
          mem.putUnsignedByte(position++, (short) iterator.getKey());
        }
        else if (maxKeySize == DATA_TYPE.SHORT) {
          mem.putUnsignedShort(position, iterator.getKey());
          position += 2;
        }
        else if (maxKeySize == DATA_TYPE.INT) {
          mem.putUnsignedInt(position, iterator.getKey());
          position += 4;
        }
        else {
          throw new UnsupportedOperationException("DATA_TYPE " + maxKeySize + " is not supported");
        }
        final PersistentIntArrayList list = (PersistentIntArrayList) iterator.getValue();
        if ((list.size() / 2) > 65535) {
          throw new UnsupportedOperationException("Number of vitals readings in the patient is too big. Maximum 65536*2 supported" + " in patient " + patientId);
        }
        mem.putUnsignedShort(position, list.size() / 2);
        position += 2;
        for (int x = 0; x < list.size(); x += 2) {
          if (list.get(x) > 65535) {
            throw new UnsupportedOperationException("There are too many distinct values in the measurement " + list.get(x) + " in PID " + patientId + " for lab "
                + iterator.getKey());
          }
          mem.putUnsignedShort(position, list.get(x));
          position += 2;
          mem.putInt(position, list.get(x + 1));
          position += 4;
        }
      }
    }
    return position;
  }

  private void writeYears(final Output output) throws IOException {
    output.writeUnsignedByte(years.length);
    for (final short year : years) {
      output.writeUnsignedByte(year - 1900);
    }
  }

  private void loadPayload(final Input input, final LongMutable pos) throws IOException {
    idToPayload = new PersistentIntKeyObjectMap(PersistentIntArrayList.class);
    pos.set(idToPayload.load(input, pos.get()));
  }

  @Override
  public long load(final Input input, final long position) {
    final LongMutable pos = new LongMutable(position);
    try {
      final int version = input.readInt(pos);
      if (version != getCurrentVersion()) {
        throw new IOException("Unsupported version. Current version = " + version + ", supported version = " + getCurrentVersion());
      }
      startTime = input.readInt(pos);
      endTime = input.readInt(pos);
      loadPayload(input, pos);
      loadIntKeyIntArrayListMap(input, pos, icd9ToTimeVisitId);
      loadIntKeyIntArrayListMap(input, pos, icd10ToTimeVisitId);
      loadIntKeyIntArrayListMap(input, pos, cptToTimeVisitId);
      loadIntKeyIntArrayListMap(input, pos, snomedToTimeVisitId);
      loadIntKeyIntArrayListMap(input, pos, departmentToTimeVisitId);
      loadIntKeyIntArrayListMap(input, pos, rxNormToTimeVisitId);
      loadIntKeyIntArrayListMap(input, pos, atcToRxNorm);
      loadIntKeyIntArrayListMap(input, pos, visitTypeToTime);
      loadIntKeyIntArrayListMap(input, pos, noteTypeToTimePoints);
      loadTerms(input, pos, positiveTermToTime);
      loadTerms(input, pos, negatedTermToTime);
      loadTerms(input, pos, familyHistoryTermToTime);
      pos.set(ageRanges.load(input, pos.get()));
      pos.set(encounterRanges.load(input, pos.get()));
      pos.set(yearToStartEnd.load(input, pos.get()));
      final short len = (short) input.readUnsignedByte(pos);
      years = new short[len];
      for (int x = 0; x < len; x++) {
        years[x] = (short) (input.readUnsignedByte(pos) + 1900);
      }
      vitals.clear();
      labValues.clear();
      pos.set(vitals.load(input, pos.get()));
      pos.set(labValues.load(input, pos.get()));
      labsWithTypeId.clear();
      pos.set(labsWithTypeId.load(input, pos.get()));
      labsIdToTime.clear();
      pos.set(labsIdToTime.load(input, pos.get()));
      pos.set(uniqueNotePayloadIds.load(input, pos.get()));
    }
    catch (final Exception e) {
      e.printStackTrace();
      return -1;
    }
    return pos.get();
  }

  private void loadIntKeyIntArrayListMap(final Input input, final LongMutable pos, final PersistentIntKeyObjectMap map) throws Exception {
    final int size = input.readInt(pos);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(pos);
      final int arraySize = input.readInt(pos);
      final PersistentIntArrayList array = new PersistentIntArrayList();
      for (int y = 0; y < arraySize; y++) {
        array.add(input.readInt(pos));
      }
      map.put(key, array);
    }
  }

  private void loadTerms(final Input input, final LongMutable pos, final PersistentIntKeyObjectMap map) throws Exception {
    final int size = input.readInt(pos);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(pos);
      final int arraySize = input.readUnsignedShort(pos);
      final PersistentIntArrayList array = new PersistentIntArrayList();
      for (int y = 0; y < arraySize; y++) {
        array.add(input.readInt(pos));
      }
      map.put(key, array);
    }
  }

  private void saveIntKeyIntArrayListMap(final Output output, final PersistentIntKeyObjectMap map) throws Exception {
    output.writeInt(map.size());
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      output.writeInt(iterator.getKey());
      final PersistentIntArrayList list = (PersistentIntArrayList) iterator.getValue();
      output.writeInt(list.size());
      for (int y = 0; y < list.size(); y++) {
        output.writeInt(list.get(y));
      }
    }
  }

  private void saveTerms(final Output output, final PersistentIntKeyObjectMap map) throws Exception {
    output.writeInt(map.size());
    final IntKeyMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      output.writeInt(iterator.getKey());
      final PersistentIntArrayList list = (PersistentIntArrayList) iterator.getValue();
      if (list.size() > 65535) {
        throw new Exception("Array size too large for TERMS");
      }
      output.writeUnsignedShort(list.size());
      for (int y = 0; y < list.size(); y++) {
        output.writeInt(list.get(y));
      }
    }
  }

  @Override
  public void setId(final int arg0) {
    this.patientId = arg0;
  }

  @Override
  public IntArrayList getPayload(final int id) {
    return (PersistentIntArrayList) idToPayload.get(id);
  }

  public int[] getUniqueLabIds() {
    final IntSet keys = labsIdToTime.keySet();
    final int[] result = new int[keys.size()];
    final IntIterator i = keys.iterator();
    int pos = 0;
    while (i.hasNext()) {
      result[pos++] = i.next();
    }
    return result;
  }

  private void addSynonym(final PersistentIntKeyObjectMap map, final int synonymId, final IntIterator tids) {
    final ArrayList<SortPairSingle<Integer, Integer>> result = new ArrayList<>();
    while (tids.hasNext()) {
      final int tid = tids.next();
      final IntArrayList payloadIds = (PersistentIntArrayList) map.get(tid);
      for (int x = 0; x < payloadIds.size(); x++) {
        result.add(new SortPairSingle<>(((PersistentIntArrayList) idToPayload.get(payloadIds.get(x))).get(TERM_TIME_ID_POSITION_IN_PAYLOAD_DATA), payloadIds.get(x)));
      }
    }
    Collections.sort(result);
    final PersistentIntArrayList listPayloadIds = new PersistentIntArrayList();
    for (int x = 0; x < result.size(); x++) {
      listPayloadIds.add(result.get(x).getObject());
    }
    map.put(synonymId, listPayloadIds);
  }

  public void addFamilyHistoryTermSynonym(final IntKeyObjectIterator<IntOpenHashSet> queryTextIdToTids) {
    while (queryTextIdToTids.hasNext()) {
      queryTextIdToTids.next();
      addSynonym(familyHistoryTermToTime, queryTextIdToTids.getKey(), queryTextIdToTids.getValue().iterator());
    }
  }

  public void addPositiveTermSynonym(final IntKeyObjectIterator<IntOpenHashSet> queryTextIdToTids) {
    while (queryTextIdToTids.hasNext()) {
      queryTextIdToTids.next();
      addSynonym(positiveTermToTime, queryTextIdToTids.getKey(), queryTextIdToTids.getValue().iterator());
    }
  }

  public void addNegatedTermSynonym(final IntKeyObjectIterator<IntOpenHashSet> queryTextIdToTids) {
    while (queryTextIdToTids.hasNext()) {
      queryTextIdToTids.next();
      addSynonym(negatedTermToTime, queryTextIdToTids.getKey(), queryTextIdToTids.getValue().iterator());
    }
  }

  public IntIterator getFamilyHistoryTermSet() {
    return familyHistoryTermToTime.keySet().iterator();
  }

  public IntIterator getPositiveTermSet() {
    return positiveTermToTime.keySet().iterator();
  }

  public IntIterator getNegatedTermSet() {
    return negatedTermToTime.keySet().iterator();
  }

  private long saveArray(final OffHeapMemory memory, final long position, final long indexPosition, final IntArrayList list) {
    if (list.size() == 0) {
      memory.putInt(indexPosition, -1);
      return position;
    }
    memory.putInt(indexPosition, (int) (position - startPositionInCache));
    final OffHeapIntArrayListAutomated l = OffHeapIntArrayListAutomated.create(list.toArray(), memory, position);
    return position + l.getSizeInMemory();
  }

  /**
   *
   * @param year
   * @return TRUE if the maximum year recorded if lesser or equal than year indicated
   */
  public boolean yearLowerThanCurrentYear(final int year) {
    final IntIterator set = yearToStartEnd.keySet().iterator();
    while (set.hasNext()) {
      final int val = set.next();
      if (val > year) {
        return false;
      }
    }
    return true;
  }

  public IntArrayList getYears() {
    final IntArrayList result = new IntArrayList();
    final IntIterator ii = yearToStartEnd.keySet().iterator();
    while (ii.hasNext()) {
      result.add(ii.next());
    }
    return result;
  }

  public boolean containsYear(final int year) {
    return yearToStartEnd.containsKey(year);
  }

  private byte calculateOffsetEncoding(final byte originalOffsetEncoding) {
    return (byte) (originalOffsetEncoding + (idToPayload.size() == 0 ? PAYLOAD_IS_EMPTY : 0));
  }

  @Override
  public long saveOffHeap(final OffHeapMemory memory, long position) throws IOException {
    this.startPositionInCache = position;
    //offset encoding
    memory.putByte(position, calculateOffsetEncoding(OFFSET_ENCODING_TYPE_INT));
    position += BYTE_SIZE_IN_BYTES;
    memory.putInt(position, startTime);
    position += INT_SIZE_IN_BYTES;
    memory.putInt(position, endTime);
    position += INT_SIZE_IN_BYTES;
    final long icd9MapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long icd10MapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long cptMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long rxNormMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long positiveTermsMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long negatedTermsMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long familyHistoryTermsMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueIcd9CodesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueIcd10CodesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueCptCodesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueRxNormCodesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long agesMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long encounterDaysMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long yearsMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long yearsListPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long vitalsPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long labValuesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long labsPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long labsWithoutTypeIdPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long atcToRxNormMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueNotesWithPayloadPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long visitTypePosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long noteTypePosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long snomedMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueSnomedCodesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long departmentMapPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;
    final long uniqueDepartmentCodesPosition = position;
    memory.putInt(position, -1);
    position += INT_SIZE_IN_BYTES;

    final long endOfHeaderPosition = position;

    position = saveMap(memory, position, -1, idToPayload);
    position = saveMap(memory, position, icd9MapPosition, icd9ToTimeVisitId);
    position = saveMap(memory, position, icd10MapPosition, icd10ToTimeVisitId);
    position = saveMap(memory, position, cptMapPosition, cptToTimeVisitId);
    position = saveMap(memory, position, snomedMapPosition, snomedToTimeVisitId);
    position = saveMap(memory, position, rxNormMapPosition, rxNormToTimeVisitId);
    position = saveMap(memory, position, positiveTermsMapPosition, positiveTermToTime);
    position = saveMap(memory, position, negatedTermsMapPosition, negatedTermToTime);
    position = saveMap(memory, position, familyHistoryTermsMapPosition, familyHistoryTermToTime);
    position = saveMap(memory, position, departmentMapPosition, departmentToTimeVisitId);
    position = saveArray(memory, position, uniqueIcd9CodesPosition, getUniqueIcd9Codes().sort());
    position = saveArray(memory, position, uniqueIcd10CodesPosition, getUniqueIcd10Codes().sort());
    position = saveArray(memory, position, uniqueCptCodesPosition, getUniqueCptCodes().sort());
    position = saveArray(memory, position, uniqueSnomedCodesPosition, getUniqueSnomedCodes().sort());
    position = saveArray(memory, position, uniqueDepartmentCodesPosition, getUniqueDepartmentCodes().sort());
    position = saveArray(memory, position, uniqueRxNormCodesPosition, getUniqueRxNormCodes().sort());
    position = saveArray(memory, position, agesMapPosition, ageRanges);
    position = saveArray(memory, position, encounterDaysMapPosition, encounterRanges);
    position = saveMap(memory, position, yearsMapPosition, yearToStartEnd);
    position = saveYearsOffHeap(memory, position, yearsListPosition);
    position = saveMeasurementsOffHeap(memory, vitals, DATA_TYPE.BYTE, position, vitalsPosition);
    position = saveMeasurementsOffHeap(memory, labValues, DATA_TYPE.SHORT, position, labValuesPosition);
    position = saveLabsOffHeap(memory, position, labsPosition);
    position = saveMap(memory, position, labsWithoutTypeIdPosition, labsIdToTime);
    position = saveMap(memory, position, atcToRxNormMapPosition, atcToRxNorm);
    position = saveMap(memory, position, visitTypePosition, visitTypeToTime);
    position = saveArray(memory, position, uniqueNotesWithPayloadPosition, uniqueNotePayloadIds);
    position = saveMap(memory, position, noteTypePosition, noteTypeToTimePoints);

    if (position - startPositionInCache < (UNSIGNED_BYTE_MAX_VALUE)) {
      position = compressPatientByteOffsets(memory, startPositionInCache + PatientSearchModel.headerSizeInBytes(), endOfHeaderPosition, position);
    }
    else if (position - startPositionInCache < (UNSIGNED_BYTE_MAX_VALUE)) {
      position = compressPatientShortOffsets(memory, startPositionInCache + PatientSearchModel.headerSizeInBytes(), endOfHeaderPosition, position);
    }
    return position;
  }

  private int headerItemCnt(final long headerStartPos, final long headerEndPos) {
    return (int) ((headerEndPos - headerStartPos) / INT_SIZE_IN_BYTES);
  }

  private long compressPatientByteOffsets(final OffHeapMemory mem, final long headerStartPos, final long headerEndPos, final long patientEndPosition) {
    final int headerItemCnt = headerItemCnt(headerStartPos, headerEndPos);
    final int reductionInBytes = headerItemCnt * (INT_SIZE_IN_BYTES - BYTE_SIZE_IN_BYTES);
    for (int x = 0; x < headerItemCnt; x++) {
      final int offset = mem.getInt(headerStartPos + (x * INT_SIZE_IN_BYTES));
      if (offset > (UNSIGNED_BYTE_MAX_VALUE) - 1) {
        throw new RuntimeException("Compression of patient offsets from INT to BYTE could not be performed");
      }
      if (offset < 0) {
        mem.putUnsignedByte(headerStartPos + (x), (short) (UNSIGNED_BYTE_MAX_VALUE));
      }
      else {
        mem.putUnsignedByte(headerStartPos + (x), (short) (offset - reductionInBytes));
      }
    }
    final byte[] buffer = new byte[(int) (patientEndPosition - headerEndPos)];
    mem.get(headerEndPos, buffer);
    mem.put(headerStartPos + headerItemCnt, buffer);
    mem.putByte(headerStartPos - PatientSearchModel.headerSizeInBytes(), calculateOffsetEncoding(OFFSET_ENCODING_TYPE_BYTE));
    return patientEndPosition - reductionInBytes;
  }

  private long compressPatientShortOffsets(final OffHeapMemory mem, final long headerStartPos, final long headerEndPos, final long patientEndPosition) {
    final int headerItemCnt = headerItemCnt(headerStartPos, headerEndPos);
    final int reductionInBytes = headerItemCnt * (INT_SIZE_IN_BYTES - SHORT_SIZE_IN_BYTES);
    for (int x = 0; x < headerItemCnt; x++) {
      final int offset = mem.getInt(headerStartPos + (x * INT_SIZE_IN_BYTES));
      if (offset > (UNSIGNED_SHORT_MAX_VALUE) - 1) {
        throw new RuntimeException("Compression of patient offsets from INT to SHORT could not be performed");
      }
      if (offset < 0) {
        mem.putUnsignedShort(headerStartPos + (x * SHORT_SIZE_IN_BYTES), (UNSIGNED_SHORT_MAX_VALUE));
      }
      else {
        mem.putUnsignedShort(headerStartPos + (x * SHORT_SIZE_IN_BYTES), (offset - reductionInBytes));
      }
    }
    final byte[] buffer = new byte[(int) (patientEndPosition - headerEndPos)];
    mem.get(headerEndPos, buffer);
    mem.put(headerStartPos + (headerItemCnt * SHORT_SIZE_IN_BYTES), buffer);
    mem.putByte(headerStartPos - PatientSearchModel.headerSizeInBytes(), calculateOffsetEncoding(OFFSET_ENCODING_TYPE_SHORT));
    return patientEndPosition - reductionInBytes;
  }

  private long saveYearsOffHeap(final OffHeapMemory mem, long position, final long indexPosition) {
    mem.putInt(indexPosition, (int) (position - startPositionInCache));
    mem.putUnsignedByte(position++, (short) years.length);
    for (final short year : years) {
      mem.putUnsignedByte(position++, (short) (year - 1900));
    }
    return position;
  }

  @Override
  public void setPosition(final long position) {
    throw new UnsupportedOperationException();
  }

  public IntArrayList getUniqueIcd9Codes() {
    return new IntArrayList(icd9ToTimeVisitId.keySet());
  }

  public IntArrayList getUniqueCptCodes() {
    return new IntArrayList(cptToTimeVisitId.keySet());
  }

  public IntArrayList getUniqueRxNormCodes() {
    return new IntArrayList(rxNormToTimeVisitId.keySet());
  }

  /*  1. get start
   *  2. keep adding 1 day, until the x + 1 day > current end
   *  3. scroll encounters:
   *     a. if the next start is less than last end + 1 day, add one more day
   *     b. if the next start is greater than last end + 1 day, continue to 1
   *
   *
   *
   *
   */

  private static int getNextEncounter(final IntArrayList encounterDays, final int currentTime, final int currentPos) {
    for (int x = currentPos; x < encounterDays.size(); x += 2) {
      if (encounterDays.get(x + 1) > currentTime) {
        return x;
      }
    }
    return encounterDays.size();
  }

  public static IntArrayList convertEncountersToEncounterDays(final int maxTime, final IntArrayList encounterDays) {
    final IntArrayList result = new IntArrayList();
    if (encounterDays.size() != 0) {
      int pos = 0;
      while (pos < encounterDays.size()) {
        int currentStart = encounterDays.get(pos);
        if (currentStart > maxTime) {
          break;
        }
        if (result.size() != 0) {
          if (currentStart < result.get(result.size() - 1) + (60 * 24)) {
            currentStart = result.get(result.size() - 1) + 1;
          }
        }
        int maxCurrentEnd = Math.min(currentStart + (60 * 24), maxTime);
        if (result.size() > 100000) {
          System.out.println("!!! too many encounterdays " + " / " + maxTime);
          return result;
        }
        result.add(currentStart);
        result.add(maxCurrentEnd);
        while (maxCurrentEnd <= encounterDays.get(pos + 1)) {
          currentStart = maxCurrentEnd + 1;
          maxCurrentEnd = Math.min(currentStart + (60 * 24), maxTime);
          if (currentStart > maxTime || currentStart > encounterDays.get(pos + 1)) {
            break;
          }
          result.add(currentStart);
          result.add(maxCurrentEnd);
        }
        pos = getNextEncounter(encounterDays, result.get(result.size() - 1), pos);
      }
    }
    return result;
  }

  public static IntArrayList sortPayloadsByTime(final PatientBuilder patient, final IntArrayList payloadIds) {
    final IntArrayList result = new IntArrayList();
    final IntOpenHashSet processedPayloads = new IntOpenHashSet();
    for (int x = 0; x < payloadIds.size(); x++) {
      final PersistentIntArrayList payload = (PersistentIntArrayList) patient.idToPayload.get(payloadIds.get(x));
      if (!processedPayloads.contains(payloadIds.get(x))) {
        processedPayloads.add(payloadIds.get(x));
        boolean stored = false;
        for (int y = 0; y < result.size(); y++) {
          final PersistentIntArrayList storedPayload = (PersistentIntArrayList) patient.idToPayload.get(result.get(y));
          if (storedPayload.get(0) > payload.get(0) || (storedPayload.get(0) == payload.get(0) && storedPayload.get(1) >= payload.get(1))) {
            result.add(y, payloadIds.get(x));
            stored = true;
            break;
          }
        }
        if (!stored) {
          result.add(payloadIds.get(x));
        }
      }
    }
    return result;
  }

  protected static void sortLabsWithTypeId(final IntKeyMapIterator iterator) {
    while (iterator.hasNext()) {
      iterator.next();
      final ArrayList<SortPair<Integer, Integer>> values = new ArrayList<>();
      final PersistentIntArrayList list = (PersistentIntArrayList) iterator.getValue();
      for (int x = 0; x < list.size(); x += 2) {
        values.add(new SortPair<>(list.get(x), list.get(x + 1)));
      }
      Collections.sort(values);
      list.clear();
      for (int x = 0; x < values.size(); x++) {
        list.add(values.get(x).getComparable().intValue());
        list.add(values.get(x).getObject().intValue());
      }
    }
  }

  @Override
  public void close() throws IOException {
    encounterRanges.addAll(convertEncountersToEncounterDays(endTime, new IntArrayList(getComputedStartEnd(sortPayloadsByTime(this, encounterPayloadComputationData)))));
    final IntArrayList years = new IntArrayList();
    final IntIterator i = yearToStartEnd.keySet().iterator();
    while (i.hasNext()) {
      final int year = i.next();
      years.add(year);
    }
    years.sort();
    this.years = new short[years.size()];
    for (int x = 0; x < years.size(); x++) {
      this.years[x] = (short) years.get(x);
    }
    sortLabsWithTypeId(labsWithTypeId.entries());
    IntKeyMapIterator iterator = labsWithTypeId.entries();
    final IntKeyObjectMap<IntOpenHashSet> recordedLabs = new IntKeyObjectMap<>();
    while (iterator.hasNext()) {
      iterator.next();
      PersistentIntArrayList list = (PersistentIntArrayList) labsIdToTime.get(iterator.getKey());
      IntOpenHashSet recordedTimes = recordedLabs.get(iterator.getKey());
      if (list == null) {
        list = new PersistentIntArrayList();
        labsIdToTime.put(iterator.getKey(), list);
        recordedTimes = new IntOpenHashSet();
        recordedLabs.put(iterator.getKey(), recordedTimes);
      }
      final PersistentIntArrayList listToAdd = (PersistentIntArrayList) iterator.getValue();
      for (int x = 0; x < listToAdd.size(); x += 2) {
        if (!recordedTimes.contains(listToAdd.get(x + 1))) {
          recordedTimes.add(listToAdd.get(x + 1));
          list.add(listToAdd.get(x + 1));
        }
      }
    }
    iterator = labValues.entries();
    while (iterator.hasNext()) {
      iterator.next();
      PersistentIntArrayList list = (PersistentIntArrayList) labsIdToTime.get(iterator.getKey());
      IntOpenHashSet recordedTimes = recordedLabs.get(iterator.getKey());
      if (list == null) {
        list = new PersistentIntArrayList();
        labsIdToTime.put(iterator.getKey(), list);
        recordedTimes = new IntOpenHashSet();
        recordedLabs.put(iterator.getKey(), recordedTimes);
      }
      final PersistentIntArrayList listToAdd = (PersistentIntArrayList) iterator.getValue();
      for (int x = 0; x < listToAdd.size(); x += 2) {
        if (!recordedTimes.contains(listToAdd.get(x + 1))) {
          recordedTimes.add(listToAdd.get(x + 1));
          list.add(listToAdd.get(x + 1));
        }
      }
    }

    iterator = labsIdToTime.entries();
    while (iterator.hasNext()) {
      iterator.next();
      ((PersistentIntArrayList) iterator.getValue()).sort();
    }
    iterator = noteTypeToTimePoints.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final IntOpenHashSet unique = new IntOpenHashSet();
      final PersistentIntArrayList values = (PersistentIntArrayList) iterator.getValue();
      unique.addAll((values).toArray());
      final int[] uniqueTimePoints = unique.toArray();
      Arrays.sort(uniqueTimePoints);
      values.clear();
      for (final int uniqueValue : uniqueTimePoints) {
        values.add(uniqueValue);
      }
    }
    closed = true;
  }

  public IntArrayList getUniqueNotePayloadIds() {
    return uniqueNotePayloadIds;
  }

  @Override
  public String toString() {
    final StringBuilder patientData = new StringBuilder();
    patientData.append("CPT [" + cptToTimeVisitId.keySet().toString() + "]");
    patientData.append("ICD9 [" + icd9ToTimeVisitId.keySet().toString() + "]");
    patientData.append("RX [" + rxNormToTimeVisitId.keySet().toString() + "]");
    patientData.append("LABS [" + labsIdToTime.keySet().toString() + "]");
    patientData.append("VITALS [" + vitals.size() + "]");
    patientData.append("TERMS [" + positiveTermToTime.keySet().toString() + "]");
    patientData.append("!TERMS [" + negatedTermToTime.keySet().toString() + "]");
    patientData.append("~TERMS [" + familyHistoryTermToTime.keySet().toString() + "]");
    patientData.append("AGES [" + ageRanges.toString() + "]");
    return patientData.toString();
  }

  public IntArrayList getUniqueSnomedCodes() {
    return new IntArrayList(snomedToTimeVisitId.keySet());
  }

  public boolean isClosed() {
    return closed;
  }

  @Override
  public int getCurrentVersion() {
    return CURRENT_VERSION;
  }

  public int getStartTime() {
    return startTime;
  }

  public int getEndTime() {
    return endTime;
  }

  public IntArrayList getUniqueIcd10Codes() {
    return new IntArrayList(icd10ToTimeVisitId.keySet());
  }

  public IntArrayList getUniqueDepartmentCodes() {
    return new IntArrayList(departmentToTimeVisitId.keySet());
  }

}