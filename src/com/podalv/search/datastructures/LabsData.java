package com.podalv.search.datastructures;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.set.IntSet;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.objectdb.datastructures.PersistentIntKeyObjectMap;
import com.podalv.search.language.evaluation.PayloadPointers;

public class LabsData {

  private final PersistentIntKeyObjectMap data;
  final ObjectWithPayload                 patient;

  public LabsData(final ObjectWithPayload patient, final PersistentIntKeyObjectMap data) {
    this.data = data;
    this.patient = patient;
  }

  public PersistentIntKeyObjectMap getData() {
    return data;
  }

  public boolean isEmpty() {
    return data.size() == 0;
  }

  public IntSet getUniqueLabIds() {
    return data.keySet();
  }

  public IntArrayList getAllTimes() {
    final IntArrayList result = new IntArrayList();
    final IntKeyMapIterator i = data.entries();
    while (i.hasNext()) {
      i.next();
      final PersistentIntArrayList list = (PersistentIntArrayList) i.getValue();
      for (int x = 0; x < list.size(); x += 2) {
        final IntArrayList l = patient.getPayload(list.get(x + 1));
        if (!PayloadPointers.containsInvalidEvents(l)) {
          final int val = patient.getPayload(list.get(x + 1)).get(0);
          result.add(val);
          result.add(val);
        }
      }
    }
    result.sort();
    return result;
  }

  /** Records unique labs into the provided structure where labId = index
   *
   * @param transientData
   * @return number of unique labs
   */
  public int recordLabs(final int[] transientData) {
    final IntIterator i = data.keySet().iterator();
    int cnt = 0;
    while (i.hasNext()) {
      cnt++;
      transientData[i.next()]++;
    }
    return cnt;
  }

  /** Returns data in format: ValueTypeId, time
   *
   *
   * @param labId
   * @return
   */
  public IntArrayList getRawData(final int labId) {
    return (PersistentIntArrayList) data.get(labId);
  }

  public IntArrayList get(final int labId, final int valueType) {
    final IntArrayList result = new IntArrayList();
    final PersistentIntArrayList list = (PersistentIntArrayList) data.get(labId);
    if (list != null) {
      for (int x = 0; x < list.size(); x += 2) {
        if (list.get(x) == valueType) {
          result.add(list.get(x + 1));
        }
      }
    }
    return result;
  }

}