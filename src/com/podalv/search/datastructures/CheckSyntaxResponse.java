package com.podalv.search.datastructures;

import java.util.ArrayList;

public class CheckSyntaxResponse {

  private final ArrayList<SyntaxError> errors = new ArrayList<>();

  public ArrayList<SyntaxError> getErrors() {
    return errors;
  }

  public void add(final SyntaxError error) {
    errors.add(error);
  }
}
