package com.podalv.search.datastructures;

/** An interface to write result of a measurement into results
 * 
 * @author podalv
 *
 */
public interface CsvWriter {

  public void write(String name, int time, double value);
}