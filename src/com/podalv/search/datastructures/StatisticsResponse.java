package com.podalv.search.datastructures;

import java.util.ArrayList;
import java.util.Collections;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.podalv.maps.primitive.map.LongKeyLongMapIterator;
import com.podalv.maps.primitive.map.LongKeyLongOpenHashMap;
import com.podalv.utils.datastructures.SortPairSingle;

/** Basic statistics about general population
 *
 * @author podalv
 *
 */
public class StatisticsResponse {

  @JsonProperty("patientCnt") private int                 patientCnt;
  @JsonProperty("encounterCnt") private long              encounterCnt;
  @JsonProperty("gender") private DemographicsHistogram[] gender     = new DemographicsHistogram[0];
  @JsonProperty("race") private DemographicsHistogram[]   race       = new DemographicsHistogram[0];
  @JsonProperty("ages") private DemographicsHistogram[]   ages       = new DemographicsHistogram[0];
  @JsonProperty("encounters") private ArrayList<long[]>   encounters = new ArrayList<>();
  @JsonProperty("durations") private ArrayList<long[]>    durations  = new ArrayList<>();
  @JsonProperty("deaths") private ArrayList<long[]>       deaths     = new ArrayList<>();
  @JsonProperty("censored") private ArrayList<long[]>     censored   = new ArrayList<>();

  public void setDurations(final ArrayList<long[]> durations) {
    this.durations = durations;
  }

  public void setEncounters(final ArrayList<long[]> encounters) {
    this.encounters = encounters;
  }

  public ArrayList<long[]> getDurations() {
    return durations;
  }

  private ArrayList<DemographicsHistogram> getArray(final DemographicsHistogram[] histo) {
    final ArrayList<DemographicsHistogram> result = new ArrayList<>();
    for (int x = 0; x < histo.length; x++) {
      result.add(histo[x]);
    }
    return result;
  }

  private LongKeyLongOpenHashMap arrayToMap(final ArrayList<long[]> tgt) {
    final LongKeyLongOpenHashMap result = new LongKeyLongOpenHashMap();
    for (int x = 0; x < tgt.size(); x++) {
      result.put(tgt.get(x)[0], tgt.get(x)[1]);
    }
    return result;
  }

  private void addArrayToMap(final LongKeyLongOpenHashMap tgt, final ArrayList<long[]> src) {
    for (int x = 0; x < src.size(); x++) {
      tgt.put(src.get(x)[0], tgt.get(src.get(x)[0]) + src.get(x)[1]);
    }
  }

  private ArrayList<long[]> mapToArray(final LongKeyLongOpenHashMap map) {
    final ArrayList<SortPairSingle<Long, Long>> result = new ArrayList<>();
    final LongKeyLongMapIterator i = map.entries();
    while (i.hasNext()) {
      i.next();
      result.add(new SortPairSingle<>(i.getKey(), i.getValue()));
    }
    Collections.sort(result);
    final ArrayList<long[]> out = new ArrayList<>();
    for (int x = 0; x < result.size(); x++) {
      out.add(new long[] {result.get(x).getComparable(), result.get(x).getObject()});
    }
    return out;
  }

  public StatisticsResponse add(final StatisticsResponse[] responses) {
    final ArrayList<DemographicsHistogram> genders = getArray(gender);
    final ArrayList<DemographicsHistogram> masterAges = getArray(ages);
    final ArrayList<DemographicsHistogram> races = getArray(race);
    final LongKeyLongOpenHashMap deathMap = arrayToMap(deaths);
    final LongKeyLongOpenHashMap censoredMap = arrayToMap(censored);
    final LongKeyLongOpenHashMap durationMap = arrayToMap(durations);
    final LongKeyLongOpenHashMap encounterMap = arrayToMap(encounters);
    for (int x = 0; x < responses.length; x++) {
      encounterCnt += responses[x].encounterCnt;
      PatientSearchResponse.addHistogram(masterAges, getArray(responses[x].ages), patientCnt);
      PatientSearchResponse.addHistogram(genders, getArray(responses[x].gender), patientCnt);
      PatientSearchResponse.addHistogram(races, getArray(responses[x].race), patientCnt);
      addArrayToMap(deathMap, responses[x].deaths);
      addArrayToMap(censoredMap, responses[x].censored);
      addArrayToMap(durationMap, responses[x].durations);
      addArrayToMap(encounterMap, responses[x].encounters);
    }
    ages = masterAges.toArray(new DemographicsHistogram[masterAges.size()]);
    gender = genders.toArray(new DemographicsHistogram[genders.size()]);
    race = races.toArray(new DemographicsHistogram[races.size()]);
    deaths = mapToArray(deathMap);
    censored = mapToArray(censoredMap);
    durations = mapToArray(durationMap);
    encounters = mapToArray(encounterMap);
    return this;
  }

  public ArrayList<long[]> getEncounters() {
    return encounters;
  }

  public ArrayList<long[]> getCensored() {
    return censored;
  }

  public ArrayList<long[]> getDeaths() {
    return deaths;
  }

  public void setCensored(final ArrayList<long[]> censored) {
    this.censored = censored;
  }

  public void setDeaths(final ArrayList<long[]> deaths) {
    this.deaths = deaths;
  }

  public void setAges(final DemographicsHistogram[] ages) {
    this.ages = ages;
  }

  public void setEncounterCnt(final long encounterCnt) {
    this.encounterCnt = encounterCnt;
  }

  public void setGender(final DemographicsHistogram[] gender) {
    this.gender = gender;
  }

  public void setPatientCnt(final int patientCnt) {
    this.patientCnt = patientCnt;
  }

  public void setRace(final DemographicsHistogram[] race) {
    this.race = race;
  }

  public DemographicsHistogram[] getAges() {
    return ages;
  }

  public long getEncounterCnt() {
    return encounterCnt;
  }

  public DemographicsHistogram[] getGender() {
    return gender;
  }

  public int getPatientCnt() {
    return patientCnt;
  }

  public DemographicsHistogram[] getRace() {
    return race;
  }

}
