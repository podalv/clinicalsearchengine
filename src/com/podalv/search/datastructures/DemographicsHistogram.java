package com.podalv.search.datastructures;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.podalv.input.Input;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.datastructures.LongMutable;
import com.podalv.utils.text.TextUtils;

/** Single histogram value for demographics (for example MALE portion of gender graph)
 *
 * @author podalv
 *
 */
@JsonIgnoreProperties({"currentVersion", "empty"})
public class DemographicsHistogram implements Comparable<DemographicsHistogram>, PersistentObject {

  @JsonProperty("label") private String              label;
  @JsonProperty("totalPatientCnt") private int       totalPatientCnt;
  @JsonProperty("totalCohortPatientCnt") private int totalCohortPatientCnt;
  @JsonProperty("cohortCnt") private int             cohortCnt;
  @JsonProperty("generalCnt") private int            generalCnt;
  @JsonProperty("cohortPercentage") private double   cohortPercentage;
  @JsonProperty("generalPercentage") private double  generalPercentage;

  public DemographicsHistogram() {
    // empty constructor for the PersistentObject serialization / deserialization
  }

  public DemographicsHistogram(final String label, final int cohort, final int general, final double cohortPercent, final double generalPercent, final int totalCohortPatientCnt,
      final int totalPatientCnt) {
    this.label = label;
    this.cohortCnt = cohort;
    this.cohortPercentage = cohortPercent;
    this.totalCohortPatientCnt = totalCohortPatientCnt;
    this.generalCnt = general;
    this.generalPercentage = generalPercent;
    this.totalPatientCnt = totalPatientCnt;
  }

  public int getCohortCnt() {
    return cohortCnt;
  }

  public double getCohortPercentage() {
    return cohortPercentage;
  }

  public int getGeneralCnt() {
    return generalCnt;
  }

  public void setTotalPatientCnt(final int totalPatientCnt) {
    this.totalPatientCnt = totalPatientCnt;
  }

  public void recalculatePercentages() {
    cohortPercentage = cohortCnt / (double) totalCohortPatientCnt;
    generalPercentage = generalCnt / (double) totalPatientCnt;
  }

  public void setTotalCohortPatientCnt(final int totalCohortPatientCnt) {
    this.totalCohortPatientCnt = totalCohortPatientCnt;
  }

  public void setCohortPercentage(final double cohortPercentage) {
    this.cohortPercentage = cohortPercentage;
  }

  public void setGeneralPercentage(final double generalPercentage) {
    this.generalPercentage = generalPercentage;
  }

  public double getGeneralPercentage() {
    return generalPercentage;
  }

  public String getLabel() {
    return label;
  }

  @Override
  public int hashCode() {
    return generalCnt;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof DemographicsHistogram) {
      final DemographicsHistogram o = (DemographicsHistogram) obj;
      return Integer.compare(cohortCnt, o.cohortCnt) == 0 && //
          Integer.compare(generalCnt, o.generalCnt) == 0 && //
          TextUtils.compareStrings(label, o.label);
    }
    return false;
  }

  @Override
  public int compareTo(final DemographicsHistogram o) {
    return Integer.compare(o.generalCnt, generalCnt);
  }

  public int getTotalCohortPatientCnt() {
    return totalCohortPatientCnt;
  }

  public static DemographicsHistogram add(final DemographicsHistogram masterHistogram, final DemographicsHistogram histogram2) {
    return new DemographicsHistogram(masterHistogram.label, histogram2.cohortCnt + masterHistogram.cohortCnt, histogram2.generalCnt + masterHistogram.generalCnt,
        (histogram2.cohortCnt + masterHistogram.cohortCnt) / (double) (masterHistogram.totalCohortPatientCnt + histogram2.totalCohortPatientCnt), (masterHistogram.generalCnt
            + histogram2.generalCnt) / (double) (masterHistogram.totalPatientCnt), masterHistogram.totalCohortPatientCnt + histogram2.totalCohortPatientCnt,
        masterHistogram.totalPatientCnt);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    label = input.readString(position);
    cohortCnt = input.readInt(position);
    generalCnt = input.readInt(position);
    totalPatientCnt = input.readInt(position);
    totalCohortPatientCnt = input.readInt(position);
    cohortPercentage = Double.longBitsToDouble(input.readLong(position));
    generalPercentage = Double.longBitsToDouble(input.readLong(position));
    return position.get();
  }

  public int getTotalPatientCnt() {
    return totalPatientCnt;
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeString(label);
    output.writeInt(cohortCnt);
    output.writeInt(generalCnt);
    output.writeInt(totalPatientCnt);
    output.writeInt(totalCohortPatientCnt);
    output.writeLong(Double.doubleToRawLongBits(cohortPercentage));
    output.writeLong(Double.doubleToRawLongBits(generalPercentage));
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }
}
