package com.podalv.search.datastructures;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.podalv.input.Input;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.datastructures.LongMutable;
import com.podalv.utils.text.TextUtils;

/** Encapsulates a single histogram with Label + values
 *
 * @author podalv
 *
 */
@JsonIgnoreProperties({"currentVersion", "empty", "toString"})
public class Histogram implements Comparable<Histogram>, PersistentObject {

  @JsonProperty("label") private String              label;
  @JsonProperty("code") private String               code;
  @JsonProperty("cohortCnt") private int             cohortCnt;
  @JsonProperty("generalCnt") private int            generalCnt;
  @JsonProperty("totalPatientCnt") private int       totalPatientCnt;
  @JsonProperty("cohortPercentage") private double   cohortPercentage;
  @JsonProperty("generalPercentage") private double  generalPercentage;
  @JsonProperty("residual") private transient double residual;
  @JsonProperty("labelValue") private transient int  labelValue;

  public Histogram() {
    // empty constructor for the PersistentObject serialization / deserialization
  }

  public Histogram(final String code, final String label, final int cohortCnt, final int generalCnt, final double cohortPercentage, final double generalPercentage,
      final int totalPatientCnt) {
    this.code = code;
    this.label = label;
    this.cohortCnt = cohortCnt;
    this.generalCnt = generalCnt;
    this.cohortPercentage = cohortPercentage;
    this.generalPercentage = generalPercentage;
    this.totalPatientCnt = totalPatientCnt;
    residual = calculateResidualCnt();
  }

  public void setLabelValue(final int labelValue) {
    this.labelValue = labelValue;
  }

  public void setTotalPatientCnt(final int totalPatientCnt) {
    this.totalPatientCnt = totalPatientCnt;
  }

  public int getLabelValue() {
    return labelValue;
  }

  private double calculateResidualCnt() {
    final double a = cohortCnt;
    final double b = (cohortCnt / (cohortPercentage / 100)) - cohortCnt;
    final double c = generalCnt - cohortCnt;
    final double t = (a + b) * (a + c) / (generalCnt / (generalPercentage / 100));
    return (a - t) / Math.sqrt(t);
  }

  public double getResidual() {
    return residual;
  }

  public String getCode() {
    return code;
  }

  public int getCohortCnt() {
    return cohortCnt;
  }

  public double getCohortPercentage() {
    return cohortPercentage;
  }

  public int getGeneralCnt() {
    return generalCnt;
  }

  public double getGeneralPercentage() {
    return generalPercentage;
  }

  public String getLabel() {
    return label;
  }

  @Override
  public int hashCode() {
    return cohortCnt;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && obj instanceof Histogram) {
      final Histogram o = (Histogram) obj;
      return (cohortCnt == o.cohortCnt && generalCnt == o.generalCnt && TextUtils.compareStrings(o.label, label) && TextUtils.compareStrings(code, o.code));
    }
    return false;
  }

  @Override
  public int compareTo(final Histogram o) {
    final int val = Double.compare(o.residual, residual);
    if (val == 0) {
      return Integer.compare(o.cohortCnt, cohortCnt);
    }
    return val;
  }

  public static Histogram add(final Histogram masterHistogram, final Histogram histogram2, final int totalCohortCnt) {
    return new Histogram(masterHistogram.code, masterHistogram.label, histogram2.cohortCnt + masterHistogram.cohortCnt, histogram2.generalCnt + masterHistogram.generalCnt,
        (histogram2.cohortCnt + masterHistogram.cohortCnt) / (double) (totalCohortCnt), (masterHistogram.generalCnt + histogram2.generalCnt)
            / (double) (masterHistogram.totalPatientCnt), masterHistogram.totalPatientCnt);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    label = input.readString(position);
    code = input.readString(position);
    cohortCnt = input.readInt(position);
    generalCnt = input.readInt(position);
    totalPatientCnt = input.readInt(position);
    cohortPercentage = Double.longBitsToDouble(input.readLong(position));
    generalPercentage = Double.longBitsToDouble(input.readLong(position));
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeString(label);
    output.writeString(code);
    output.writeInt(cohortCnt);
    output.writeInt(generalCnt);
    output.writeInt(totalPatientCnt);
    output.writeLong(Double.doubleToRawLongBits(cohortPercentage));
    output.writeLong(Double.doubleToRawLongBits(generalPercentage));
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }

  @Override
  public String toString() {
    return label + " " + cohortCnt + " / " + generalCnt;
  }
}