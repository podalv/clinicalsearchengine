package com.podalv.search.datastructures;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyMapIterator;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.search.index.IndexCollection;
import com.podalv.utils.text.TextUtils;
import com.podalv.utils.text.search.datastructures.ExactTextMatcher;

/** Decomposes phrases into words and then autosuggests phrases that have the same words
 *  Example: "patient with hypertension" = TID 1
 *           "patient with insomnia" = TID 2
 *           breaks down to:
 *           patient = TID 1, TID 2
 *           with = TID 1, TID 2
 *           hypertension = TID 1
 *           insomnia = TID 2
 *
 * @author
 podalv
 *
 */
public class StringTidAutosuggestTextMatcher extends AutosuggestTextMatcher {

  private final HashMap<String, IntOpenHashSet> strToTid        = new HashMap<>();
  private final IntKeyObjectMap<int[]>          trieIdToTidList = new IntKeyObjectMap<>();
  private final IndexCollection                 indices;

  public StringTidAutosuggestTextMatcher(final IndexCollection indices) {
    this.indices = indices;
  }

  public static StringTidAutosuggestTextMatcher load(final DataInputStream stream, final IndexCollection indices) throws IOException {
    final StringTidAutosuggestTextMatcher result = new StringTidAutosuggestTextMatcher(indices);
    result.matcher = ExactTextMatcher.load(stream);
    result.id = stream.readInt();
    result.codeId = stream.readInt();
    int len = stream.readInt();
    for (int x = 0; x < len; x++) {
      final int key = stream.readInt();
      final int l = stream.readInt();
      final IntOpenHashSet set = new IntOpenHashSet();
      for (int y = 0; y < l; y++) {
        set.add(stream.readInt());
      }
      result.idToCodeIds.put(key, set);
    }
    len = stream.readInt();
    for (int x = 0; x < len; x++) {
      result.codeIdsToCodes.put(stream.readInt(), stream.readUTF());
    }
    len = stream.readInt();
    for (int x = 0; x < len; x++) {
      final String key = stream.readUTF();
      final int l = stream.readInt();
      final IntOpenHashSet set = new IntOpenHashSet();
      for (int y = 0; y < l; y++) {
        set.add(stream.readInt());
      }
      result.fragmentToCodeIds.put(key, set);
    }

    len = stream.readInt();
    for (int x = 0; x < len; x++) {
      final String key = stream.readUTF();
      final int l = stream.readInt();
      final IntOpenHashSet set = new IntOpenHashSet();
      for (int y = 0; y < l; y++) {
        set.add(stream.readInt());
      }
      result.strToTid.put(key, set);
    }

    len = stream.readInt();
    for (int x = 0; x < len; x++) {
      final int key = stream.readInt();
      final int l = stream.readInt();
      final int[] arr = new int[l];
      for (int y = 0; y < l; y++) {
        arr[y] = stream.readInt();
      }
      result.trieIdToTidList.put(key, arr);
    }

    return result;
  }

  @Override
  public void save(final DataOutputStream stream) throws IOException {
    matcher.save(stream);
    stream.writeInt(id);
    stream.writeInt(codeId);
    final IntKeyObjectIterator<IntOpenHashSet> iterator = idToCodeIds.entries();
    stream.writeInt(idToCodeIds.size());
    while (iterator.hasNext()) {
      iterator.next();
      stream.writeInt(iterator.getKey());
      stream.writeInt(iterator.getValue().size());
      final IntIterator values = iterator.getValue().iterator();
      while (values.hasNext()) {
        stream.writeInt(values.next());
      }
    }

    stream.writeInt(codeIdsToCodes.size());
    final IntKeyMapIterator i = codeIdsToCodes.entries();
    while (i.hasNext()) {
      stream.writeInt(i.getKey());
      stream.writeUTF((String) i.getValue());
    }

    stream.writeInt(fragmentToCodeIds.size());
    Iterator<Entry<String, IntOpenHashSet>> ii = fragmentToCodeIds.entrySet().iterator();
    while (ii.hasNext()) {
      final Entry<String, IntOpenHashSet> entry = ii.next();
      stream.writeUTF(entry.getKey());
      stream.writeInt(entry.getValue().size());
    }

    stream.writeInt(strToTid.size());
    ii = strToTid.entrySet().iterator();
    while (ii.hasNext()) {
      final Entry<String, IntOpenHashSet> entry = ii.next();
      stream.writeUTF(entry.getKey());
      stream.writeInt(entry.getValue().size());
    }

    stream.writeInt(trieIdToTidList.size());
    final IntKeyObjectIterator<int[]> iii = trieIdToTidList.entries();
    while (iii.hasNext()) {
      iii.next();
      stream.writeInt(iii.getKey());
      stream.writeInt(iii.getValue().length);
      for (int y = 0; y < iii.getValue().length; y++) {
        stream.writeInt(iii.getValue()[y]);
      }
    }
  }

  @Override
  public void add(final String text, final IntOpenHashSet codes) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void add(final String text, final String code) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addWholeString(final String text, final IntOpenHashSet code) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addWholeString(final String text, final String code) {
    throw new UnsupportedOperationException();
  }

  private void addWord(final String word, final int tid) {
    if (word != null && !word.isEmpty()) {
      IntOpenHashSet set = strToTid.get(word);
      if (set == null) {
        set = new IntOpenHashSet();
        strToTid.put(word, set);
      }
      set.add(tid);
    }
  }

  public void add(final String text, final int tid) {
    addWord(text.toLowerCase(), tid);
    final String[] texts = TextUtils.splitWords(text.toLowerCase());
    for (final String t : texts) {
      for (int x = 1; x <= t.length(); x++) {
        addWord(t.substring(0, x), tid);
      }
    }
  }

  @Override
  public void build() {
    final Iterator<Entry<String, IntOpenHashSet>> iterator = strToTid.entrySet().iterator();
    int currentId = 0;
    while (iterator.hasNext()) {
      final Entry<String, IntOpenHashSet> entry = iterator.next();
      final int[] sorted = entry.getValue().toArray();
      Arrays.sort(sorted);
      trieIdToTidList.put(currentId, sorted);
      builder.add(entry.getKey(), currentId);
      currentId++;
    }
    strToTid.clear();
    matcher = builder.buildExactMatching();
  }

  @Override
  public HashSet<String> find(final String word) {
    final HashSet<String> result = new HashSet<>();
    if (matcher != null) {
      final int[] idCodes = trieIdToTidList.get(matcher.findExactMatch(word));
      if (idCodes != null) {
        for (int x = 0; x < idCodes.length; x++) {
          result.add(indices.getStringFromTid(idCodes[x]));
        }
      }
    }
    return result;
  }

}
