package com.podalv.search.datastructures;

import java.security.InvalidParameterException;

import com.alexkasko.unsafe.offheap.OffHeapMemory;
import com.podalv.extractor.datastructures.BinaryFileExtractionSource.DATA_TYPE;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.server.PatientDataDump;

/** Loads and searches in the OffHeap version of the vitals for a single patient
 *
 * @author podalv
 *
 */
public class MeasurementOffHeapData extends MeasurementData {

  public static final int INVALID_EVENT = Integer.MIN_VALUE;

  private OffHeapMemory   memory;
  private long            position;
  private final DATA_TYPE sizeEncoding;
  private final byte      sizeEncodingLengthInBytes;
  private Patient         patient;
  private CONTENT         contentType;

  private enum CONTENT {
    LABS, VITALS
  };

  public static boolean isEventInvalid(int time) {
    return time == INVALID_EVENT;
  }

  public MeasurementOffHeapData(final Patient patient, final OffHeapMemory mem, final MeasurementNameProvider nameProvider, final DATA_TYPE sizeEncoding, final long position) {
    super(null, nameProvider);
    this.memory = mem;
    this.patient = patient;
    if (patient == null) {
      contentType = CONTENT.VITALS;
    }
    else {
      contentType = CONTENT.LABS;
    }
    this.position = position;
    if (sizeEncoding != DATA_TYPE.BYTE && sizeEncoding != DATA_TYPE.SHORT && sizeEncoding != DATA_TYPE.INT) {
      throw new InvalidParameterException("DATA_TYPE " + sizeEncoding + " is not supported");
    }
    if (sizeEncoding == DATA_TYPE.BYTE) {
      sizeEncodingLengthInBytes = 1;
    }
    else if (sizeEncoding == DATA_TYPE.SHORT) {
      sizeEncodingLengthInBytes = 2;
    }
    else if (sizeEncoding == DATA_TYPE.INT) {
      sizeEncodingLengthInBytes = 4;
    }
    else {
      throw new InvalidParameterException("DATA_TYPE " + sizeEncoding + " is not supported");
    }

    this.sizeEncoding = sizeEncoding;
  }

  public void setMemory(final OffHeapMemory mem, final Patient patient) {
    this.memory = mem;
    this.patient = patient;
  }

  public void setPosition(final long position, final Patient patient) {
    this.position = position;
    this.patient = patient;
  }

  public int size() {
    if (position >= 0) {
      if (sizeEncoding == DATA_TYPE.BYTE) {
        return memory.getUnsignedByte(position);
      }
      else if (sizeEncoding == DATA_TYPE.SHORT) {
        return memory.getUnsignedShort(position);
      }
      else if (sizeEncoding == DATA_TYPE.INT) {
        return (int) memory.getUnsignedInt(position);
      }
    }
    return 0;
  }

  @Override
  public boolean isEmpty() {
    return size() == 0;
  }

  private long codeStartPosition(final int code) {
    long result = position + sizeEncodingLengthInBytes;
    final int size = size();
    for (int x = 0; x < size; x++) {
      final int codeFound = sizeEncoding == DATA_TYPE.BYTE ? memory.getUnsignedByte(result)
          : sizeEncoding == DATA_TYPE.SHORT ? memory.getUnsignedShort(result) : sizeEncoding == DATA_TYPE.INT ? (int) memory.getUnsignedInt(result) : Integer.MIN_VALUE;
      if (codeFound == Integer.MIN_VALUE) {
        break;
      }
      result += sizeEncodingLengthInBytes;
      if (code == codeFound) {
        return result;
      }
      final int listSize = memory.getUnsignedShort(result);
      result += 2;
      result += listSize * (long) (4 + 2);
    }
    return -1;
  }

  private Double readDoubleValue(final IndexCollection indices, final int labCode, final long position) {
    return indices.getDoubleValue(labCode, memory.getUnsignedShort(position));
  }

  private int search(final IndexCollection indices, final int labCode, final double key, final long startPosition, final int size) {
    int lo = 0;
    int hi = size - 1;
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      final long midPos = (startPosition + 2) + (mid * (long) (4 + 2));
      final double val = readDoubleValue(indices, labCode, midPos);
      if (key < val) {
        hi = mid - 1;
      }
      else if (key > val) {
        lo = mid + 1;
      }
      else {
        for (int x = mid - 1; x >= 0; x--) {
          final double v = readDoubleValue(indices, labCode, startPosition, x);
          if (Math.abs(v - key) < 0.0001) {
            mid = x;
          }
          else {
            break;
          }
        }
        return mid;
      }
    }
    return lo;
  }

  private double readDoubleValue(final IndexCollection indices, final int labCode, final long startPosition, final int posIndex) {
    return indices.getDoubleValue(labCode, memory.getUnsignedShort(startPosition + 2 + (posIndex * (long) (4 + 2))));
  }

  private int readTime(final int labCode, final long startPosition, final int posIndex) {
    return memory.getInt(startPosition + 2 + 2 + (posIndex * (long) (4 + 2)));
  }

  @Override
  public IntArrayList getAllTimes() {
    final IntArrayList result = new IntArrayList();
    if (position >= 0) {
      long curPos = position;
      final int len = sizeEncoding == DATA_TYPE.BYTE ? memory.getUnsignedByte(curPos)
          : sizeEncoding == DATA_TYPE.SHORT ? memory.getUnsignedShort(curPos) : sizeEncoding == DATA_TYPE.INT ? (int) memory.getUnsignedInt(curPos) : Integer.MIN_VALUE;
      if (len == Integer.MIN_VALUE) {
        return result;
      }
      curPos += sizeEncodingLengthInBytes;
      for (int x = 0; x < len; x++) {
        curPos++;
        final int listSize = memory.getUnsignedShort(curPos);
        curPos += 2;
        for (int y = 0; y < listSize; y++) {
          curPos += 2;
          final int time = memory.getInt(curPos);
          result.add(time);
          result.add(time);
          curPos += 4;
        }
      }
    }
    result.sort();
    return result;
  }

  @Override
  public void extract(CsvWriter writer, final IndexCollection indices, final int[] timeRanges, final boolean containsStart, final boolean containsEnd) {
    if (position >= 0) {
      long curPos = position;
      final int len = sizeEncoding == DATA_TYPE.BYTE ? memory.getUnsignedByte(curPos)
          : sizeEncoding == DATA_TYPE.SHORT ? memory.getUnsignedShort(curPos) : sizeEncoding == DATA_TYPE.INT ? (int) memory.getUnsignedInt(curPos) : Integer.MIN_VALUE;
      if (len == Integer.MIN_VALUE) {
        return;
      }
      curPos += sizeEncodingLengthInBytes;

      for (int x = 0; x < len; x++) {
        final int vitalId = sizeEncoding == DATA_TYPE.BYTE ? memory.getUnsignedByte(curPos)
            : sizeEncoding == DATA_TYPE.SHORT ? memory.getUnsignedShort(curPos) : sizeEncoding == DATA_TYPE.INT ? (int) memory.getUnsignedInt(curPos) : Integer.MIN_VALUE;
        curPos += sizeEncodingLengthInBytes;
        final int listSize = memory.getUnsignedShort(curPos);
        final String vitalName = nameProvider.getName(vitalId);
        curPos += 2;
        for (int y = 0; y < listSize; y++) {
          int valueId = memory.getUnsignedShort(curPos);
          curPos += 2;
          int time = memory.getInt(curPos);
          curPos += 4;
          if (valueId == 0 && time == 0) {
            writer.write(vitalName, INVALID_EVENT, -1);
            continue;
          }
          Double val = indices.getDoubleValue(vitalId, valueId);
          if (contentType == CONTENT.LABS) {
            time = patient.getPayload(time).get(0);
          }
          if (PatientDataDump.isInTimeRange(time, time, timeRanges, containsStart, containsEnd)) {
            writer.write(vitalName, time, val);
          }
        }
      }
    }
  }

  @Override
  public IntArrayList get(final IndexCollection indices, final int code, final double minValue, final double maxValue) {
    if (position >= 0) {
      final long codeStartPosition = codeStartPosition(code);
      if (codeStartPosition != -1) {
        final IntArrayList result = new IntArrayList();
        final int size = memory.getUnsignedShort(codeStartPosition);
        final int posMin = search(indices, code, minValue, codeStartPosition, size);
        for (int x = posMin; x < size; x++) {
          final double readValue = readDoubleValue(indices, code, codeStartPosition, x);
          if (readValue >= minValue && readValue <= maxValue) {
            final int time = readTime(code, codeStartPosition, x);
            result.add(time);
            result.add(time);
          }
          if (readValue > maxValue) {
            break;
          }
        }
        result.sort();
        return result.size() == 0 ? null : result;
      }
    }
    return null;
  }

}