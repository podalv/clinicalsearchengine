package com.podalv.search.datastructures;

import com.podalv.maps.primitive.list.IntArrayList;

/** Interface for an object that contains payload information (Patient, PatientBuilder)
 * 
 * @author podalv
 *
 */
public interface ObjectWithPayload {

  public IntArrayList getPayload(final int id);

}
