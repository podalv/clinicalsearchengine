package com.podalv.search.datastructures;

import com.fasterxml.jackson.annotation.JsonProperty;

/** Performs text replacement in the query text area and returns the new text + new position of the cursor
 *
 * @author podalv
 *
 */
public class AutosuggestTextResponse {

  @JsonProperty("text") private String text;
  @JsonProperty("cursor") private int  cursor;

  public int getCursor() {
    return cursor;
  }

  public String getText() {
    return text;
  }

  public void setCursor(final int cursor) {
    this.cursor = cursor;
  }

  public void setText(final String text) {
    this.text = text;
  }
}
