package com.podalv.search.index;

import javax.activation.UnsupportedDataTypeException;

import com.podalv.extractor.stride6.Common;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.search.datastructures.index.StringEnumIndex;
import com.podalv.search.datastructures.index.StringEnumIndexBuilder;

/** Contains builders for all the demographics elements (gender, race, ethnicity, death time)
 *
 * @author podalv
 *
 */
public class DemographicsBuilder {

  private final StringEnumIndexBuilder gender    = new StringEnumIndexBuilder();
  private final StringEnumIndexBuilder race      = new StringEnumIndexBuilder();
  private final StringEnumIndexBuilder ethnicity = new StringEnumIndexBuilder();
  private final IntArrayList           deathTime = new IntArrayList();

  public void addGender(final int patientId, final String gender) {
    this.gender.add(patientId, gender == null ? "EMPTY" : gender.toUpperCase());
  }

  public void addRace(final int patientId, final String race) {
    this.race.add(patientId, race == null ? "EMPTY" : race.toUpperCase());
  }

  public void addEthnicity(final int patientId, final String ethnicity) {
    this.ethnicity.add(patientId, ethnicity == null ? "EMPTY" : ethnicity.toUpperCase());
  }

  public void addDeathTime(final int patientId, final double deathTime) {
    for (int x = this.deathTime.size() - 1; x <= patientId; x++) {
      this.deathTime.add(0);
    }
    this.deathTime.set(patientId, Common.daysToTime(deathTime));
  }

  public StringEnumIndex buildGender() throws UnsupportedDataTypeException {
    return gender.build();
  }

  public StringEnumIndex buildRace() throws UnsupportedDataTypeException {
    return race.build();
  }

  public StringEnumIndex buildEthnicity() throws UnsupportedDataTypeException {
    return ethnicity.build();
  }

  public IntArrayList buildDeathTime() throws UnsupportedDataTypeException {
    return deathTime;
  }
}
