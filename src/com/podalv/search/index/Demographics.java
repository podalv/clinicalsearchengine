package com.podalv.search.index;

import java.io.IOException;

import com.podalv.input.Input;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.search.datastructures.index.StringEnumIndex;
import com.podalv.utils.datastructures.LongMutable;

/** Contains a matrix of demographic data for each patient
 *
 * @author podalv
 *
 */
public class Demographics implements PersistentObject {

  public static final int       CURRENT_VERSION    = 1;
  public static final int       DEATH_NOT_RECORDED = 0;
  private final StringEnumIndex gender;
  private final StringEnumIndex race;
  private final StringEnumIndex ethnicity;
  private IntArrayList          deathTime;
  private final IntArrayList    deadPatientIds     = new IntArrayList();

  public Demographics(final StringEnumIndex gender, final StringEnumIndex race, final StringEnumIndex ethnicity, final IntArrayList deathTime) {
    this.gender = gender;
    this.race = race;
    this.ethnicity = ethnicity;
    this.deathTime = deathTime;
  }

  public Demographics() {
    this.gender = new StringEnumIndex();
    this.race = new StringEnumIndex();
    this.ethnicity = new StringEnumIndex();
    this.deathTime = new IntArrayList();
  }

  public IntArrayCloneableIterator getDeathTimeIterator() {
    return new IntArrayCloneableIterator(deadPatientIds);
  }

  protected void addDeadPatientId(final int patientId) {
    deadPatientIds.add(patientId);
  }

  public StringEnumIndex getGender() {
    return gender;
  }

  public StringEnumIndex getEthnicity() {
    return ethnicity;
  }

  public int getDeathTime(final int patientId) {
    return patientId >= 0 && deathTime.size() > patientId ? deathTime.get(patientId) : 0;
  }

  public StringEnumIndex getRace() {
    return race;
  }

  public boolean containsGender(final int patientId, final int genderId) {
    return gender.getValue(patientId) == genderId;
  }

  public boolean containsRace(final int patientId, final int raceId) {
    return race.getValue(patientId) == raceId;
  }

  public boolean containsEthnicity(final int patientId, final int ethnicityId) {
    return ethnicity.getValue(patientId) == ethnicityId;
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    final int version = input.readInt(position);
    if (version != getCurrentVersion()) {
      throw new IOException("Unsupported version. Current version = " + version + ", supported version = " + getCurrentVersion());
    }
    position.set(gender.load(input, position.get()));
    position.set(race.load(input, position.get()));
    position.set(ethnicity.load(input, position.get()));
    deathTime = new IntArrayList();
    final int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int time = input.readInt(position);
      deathTime.add(time);
      if (time > DEATH_NOT_RECORDED) {
        deadPatientIds.add(x);
      }
    }
    deathTime.trimToSize();
    return position.get();
  }

  public int size() {
    return gender == null ? 0 : gender.size();
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(getCurrentVersion());
    gender.save(output);
    race.save(output);
    ethnicity.save(output);
    output.writeInt(deathTime.size());
    for (int x = 0; x < deathTime.size(); x++) {
      output.writeInt(deathTime.get(x));
    }
  }

  @Override
  public boolean isEmpty() {
    return gender.isEmpty() && race.isEmpty() && ethnicity.isEmpty() && deathTime.isEmpty();
  }

  @Override
  public int getCurrentVersion() {
    return CURRENT_VERSION;
  }

}