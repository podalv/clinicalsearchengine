package com.podalv.search.index;

import static com.podalv.utils.Logging.debug;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import com.podalv.input.Input;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.MutableIntWrapper;
import com.podalv.maps.primitive.list.IntArrayCloneableIterator;
import com.podalv.maps.primitive.list.IntArrayList;
import com.podalv.maps.primitive.list.IntIteratorCloneableInterface;
import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.search.datastructures.PatientBuilder;
import com.podalv.search.datastructures.index.PatientIdLists;
import com.podalv.search.datastructures.index.StatisticsBuilder;
import com.podalv.search.datastructures.index.StatisticsBuilderBitSet;
import com.podalv.utils.arrays.ComparableIntArray;
import com.podalv.utils.datastructures.LongMutable;

/** Contains statistics about the general population
 *
 * @author podalv
 *
 */
public class Statistics implements PersistentObject {

  public static final int                             CURRENT_VERSION              = 9;
  private final PatientIdLists                        patientIdLists;
  private final IntArrayList                          ages                         = new IntArrayList();
  private final IntArrayList                          patientIds                   = new IntArrayList();
  private final AtomicInteger                         patientsWithLabs             = new AtomicInteger(0);
  private final AtomicInteger                         patientsWithCpt              = new AtomicInteger(0);
  private final AtomicInteger                         patientsWithIcd9             = new AtomicInteger(0);
  private final AtomicInteger                         patientsWithIcd10            = new AtomicInteger(0);
  private final AtomicInteger                         patientsWithNotes            = new AtomicInteger(0);
  private final AtomicInteger                         patientsWithDrugs            = new AtomicInteger(0);
  private final AtomicInteger                         patientsWithDepartments      = new AtomicInteger(0);
  private StatisticsBuilder                           statisticsBuilder;
  final IntKeyIntOpenHashMap                          icd9ToPatientIds             = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          departmentToPatientIds       = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          icd10ToPatientIds            = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          snomedToPatientIds           = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          visitTypeToPatientIds        = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          noteTypeToPatientIds         = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          cptToPatientIds              = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          vitalsToPatientIds           = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          labsToPatientIds             = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          rxToPatientIds               = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          atcToPatientIds              = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          tidToPatientIds              = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          tidNegatedToPatientIds       = new IntKeyIntOpenHashMap();
  final IntKeyIntOpenHashMap                          tidFamilyHistoryToPatientIds = new IntKeyIntOpenHashMap();
  IntArrayList                                        pidToStartEndSortedByStart   = new IntArrayList();
  IntArrayList                                        pidToStartEndSortedByEnd     = new IntArrayList();
  private final IntKeyObjectMap<IntKeyIntOpenHashMap> labIdToLabValueToPatientCnt  = new IntKeyObjectMap<>();
  private final IntArrayKeyIntMap                     rxToStatusPatientCounts      = new IntArrayKeyIntMap();
  private final IntArrayKeyIntMap                     rxToRoutePatientCounts       = new IntArrayKeyIntMap();
  private final IntArrayKeyIntMap                     rxToStatusRoutePatientCounts = new IntArrayKeyIntMap();
  private final IntArrayKeyIntMap                     rxToRouteStatusPatientCounts = new IntArrayKeyIntMap();
  private final MutableIntWrapper                     icd9MaxKeyNr                 = new MutableIntWrapper(0);
  private final MutableIntWrapper                     departmentMaxKeyNr           = new MutableIntWrapper(0);
  private final MutableIntWrapper                     icd10MaxKeyNr                = new MutableIntWrapper(0);
  private final MutableIntWrapper                     cptMaxKeyNr                  = new MutableIntWrapper(0);
  private final MutableIntWrapper                     snomedMaxKeyNr               = new MutableIntWrapper(0);
  private final MutableIntWrapper                     rxNormMaxKeyNr               = new MutableIntWrapper(0);
  private final MutableIntWrapper                     labMaxKeyNr                  = new MutableIntWrapper(0);
  private final MutableIntWrapper                     visitTypeMaxKeyNr            = new MutableIntWrapper(0);
  private final MutableIntWrapper                     noteTypeMaxKeyNr             = new MutableIntWrapper(0);
  private StatisticsAgeGenerator                      ageGenerator                 = null;
  private int                                         patientCnt                   = 0;
  private int                                         maxPatientId                 = 0;

  public void setAgeGenerator(final StatisticsAgeGenerator ageGenerator) {
    this.ageGenerator = ageGenerator;
  }

  public Statistics() {
    // default constructor
    patientIdLists = new PatientIdLists();
    statisticsBuilder = new StatisticsBuilderBitSet(-1);
  }

  public int getPatientsWithVisitTypeCnt() {
    return visitTypeToPatientIds.size();
  }

  public int getPatientsWithVitalsCnt() {
    return vitalsToPatientIds.size();
  }

  public int getPatientsWithAtcCnt() {
    return atcToPatientIds.size();
  }

  public int getPatientsWithLabsCnt() {
    return labsToPatientIds.size();
  }

  public Statistics(final int maxPatId) {
    patientIdLists = new PatientIdLists();
    statisticsBuilder = new StatisticsBuilderBitSet(-1);
  }

  public Statistics(final StatisticsBuilder builder) {
    patientIdLists = new PatientIdLists();
    statisticsBuilder = builder;
  }

  public int getPatientsWithCptCnt() {
    return cptToPatientIds.size();
  }

  public int getPatientsWithRxNormCnt() {
    return rxToPatientIds.size();
  }

  public int getPatientsWithSnomedCnt() {
    return snomedToPatientIds.size();
  }

  public int getPatientsWithIcd10Cnt() {
    return icd10ToPatientIds.size();
  }

  public int getPatientCnt() {
    return patientCnt;
  }

  public void setPatientCnt(final int patientCnt) {
    this.patientCnt = patientCnt;
  }

  public int getPatientsWithIcd9Cnt() {
    return icd9ToPatientIds.size();
  }

  public int getPatientsWithNoteTypeCnt() {
    return noteTypeToPatientIds.size();
  }

  protected static int findAge(final IntArrayList a, final int key, final int index) {
    int low = 0;
    int high = (a.size() - 1) / 3;

    while (low <= high) {
      final int mid = (low + high) >>> 1;
      final int midVal = a.get((mid * 3) + index);

      if (midVal < key) {
        low = mid + 1;
      }
      else if (midVal > key) {
        high = mid - 1;
      }
      else {
        int tempMid = mid;
        int tempMidVal = midVal;
        while (tempMidVal == key) {
          tempMid--;
          if (tempMid < 0) {
            return tempMid + 1;
          }
          tempMidVal = a.get((tempMid * 3) + index);
        }
        return tempMid + 1;
      }
    }
    return low <= 0 ? 0 : low - 1;
  }

  private int getInitialId(final int start, final int end) {
    if (start == Integer.MIN_VALUE) {
      return 0;
    }
    if (end == Integer.MAX_VALUE) {
      return findAge(pidToStartEndSortedByEnd, start, 2);
    }
    return findAge(pidToStartEndSortedByEnd, start, 2);
  }

  private boolean canBreak(final IntArrayList list, final int position, final int startAge, final int endAge) {
    if (startAge == Integer.MIN_VALUE && list.get(position + 1) > endAge) {
      return true;
    }
    return false;
  }

  public IntArrayCloneableIterator getPidsForAges(final int startAge, final int endAge) {
    if (startAge == Integer.MIN_VALUE && endAge == Integer.MAX_VALUE) {
      return new IntArrayCloneableIterator(patientIds);
    }

    final int initialId = getInitialId(startAge, endAge) * 3;

    final IntArrayList list = startAge == Integer.MIN_VALUE ? pidToStartEndSortedByStart : pidToStartEndSortedByEnd;

    final IntArrayList result = new IntArrayList();
    for (int x = initialId; x < list.size(); x += 3) {
      if (x + 2 > list.size() - 1 || canBreak(list, x, startAge, endAge)) {
        break;
      }
      if ((list.get(x + 1) >= startAge && list.get(x + 1) <= endAge) || (list.get(x + 2) >= startAge && list.get(x + 2) <= endAge) || (list.get(x + 1) <= startAge && list.get(x
          + 2) >= startAge)) {
        result.add(list.get(x));
      }
    }
    result.sort();
    return new IntArrayCloneableIterator(result);
  }

  public int getMaxPatientId() {
    return maxPatientId;
  }

  public IntKeyIntMapIterator getRxRoute(final int rx) {
    return rxToRoutePatientCounts.get(rx);
  }

  public IntKeyIntMapIterator getRxRouteStatus(final int rx, final int statusId) {
    return rxToRouteStatusPatientCounts.get(rx, statusId);
  }

  public IntKeyIntMapIterator getRxStatusRoute(final int rx, final int routeId) {
    return rxToStatusRoutePatientCounts.get(rx, routeId);
  }

  public IntKeyIntMapIterator getRxStatus(final int rx) {
    return rxToStatusPatientCounts.get(rx);
  }

  public int getRxRoutePatientCnt(final int rx, final int routeId) {
    return rxToRoutePatientCounts.getCount(rx, routeId);
  }

  public int getRxStatusPatientCnt(final int rx, final int statusId) {
    return rxToStatusPatientCounts.getCount(rx, statusId);
  }

  public int getRxRouteStatusPatientCnt(final int rx, final int routeId, final int statusId) {
    return rxToRouteStatusPatientCounts.getCount(rx, routeId, statusId);
  }

  public int getRxStatusRoutePatientCnt(final int rx, final int statusId, final int routeId) {
    return rxToStatusRoutePatientCounts.getCount(rx, statusId, routeId);
  }

  public int getIcd9KeyCnt() {
    return icd9MaxKeyNr.getValue() + 1;
  }

  public int getDepartmentKeyCnt() {
    return departmentMaxKeyNr.getValue() + 1;
  }

  public int getIcd10KeyCnt() {
    return icd10MaxKeyNr.getValue() + 1;
  }

  public int getCptKeyCnt() {
    return cptMaxKeyNr.getValue() + 1;
  }

  public int getRxNormKeyCnt() {
    return rxNormMaxKeyNr.getValue() + 1;
  }

  public int getLabNormMaxKeyNr() {
    return labMaxKeyNr.getValue() + 1;
  }

  public IntArrayList getPatientIds() {
    return patientIds;
  }

  public IntArrayCloneableIterator getPatientIdIterator() {
    return new IntArrayCloneableIterator(patientIds);
  }

  public void recordPatientId(final PatientBuilder patient) {
    if (!patient.isClosed()) {
      throw new InvalidParameterException("Patient object needs to be closed before generating statistics");
    }
    synchronized (statisticsBuilder) {
      statisticsBuilder.recordStartEnd(patient.getId(), patient.getStartTime(), patient.getEndTime());
    }
    synchronized (patientIds) {
      patientIds.add(patient.getId());
      if (patient.getUniqueCptCodes().size() != 0) {
        patientsWithCpt.incrementAndGet();
      }
      if (patient.getUniqueIcd9Codes().size() != 0) {
        patientsWithIcd9.incrementAndGet();
      }
      if (patient.getUniqueIcd10Codes().size() != 0) {
        patientsWithIcd10.incrementAndGet();
      }
      if (patient.getUniqueRxNormCodes().size() != 0) {
        patientsWithDrugs.incrementAndGet();
      }
      if (patient.getUniqueDepartmentCodes().size() != 0) {
        patientsWithDepartments.incrementAndGet();
      }
      if (patient.getUniqueNotePayloadIds().size() != 0) {
        patientsWithNotes.incrementAndGet();
      }
      if (patient.getUniqueLabIds().length != 0) {
        patientsWithLabs.incrementAndGet();
      }
    }
  }

  private void removeKeys(final IntKeyIntOpenHashMap map, final IntOpenHashSet removedIds) {
    int cnt = 0;
    final IntKeyIntMapIterator iterator = map.entries();
    while (iterator.hasNext()) {
      iterator.next();
      if (removedIds.contains(iterator.getValue())) {
        iterator.remove();
        cnt++;
      }
    }
    debug("Removed " + cnt + " features completely");
  }

  public void keepPatients(final IntIterator pids, final boolean removeKeys) {
    final long time = System.currentTimeMillis();
    final IntOpenHashSet s = new IntOpenHashSet(pids);
    debug("Reducing statistics to " + s.size() + " patients");
    patientIds.clear();
    patientIds.addAll(s.iterator());
    patientIds.sort();

    if (removeKeys) {
      final IntOpenHashSet removed = patientIdLists.keepPatients(s.iterator());

      removeKeys(icd9ToPatientIds, removed);
      removeKeys(departmentToPatientIds, removed);
      removeKeys(icd10ToPatientIds, removed);
      removeKeys(snomedToPatientIds, removed);
      removeKeys(visitTypeToPatientIds, removed);
      removeKeys(noteTypeToPatientIds, removed);
      removeKeys(cptToPatientIds, removed);
      removeKeys(vitalsToPatientIds, removed);
      removeKeys(labsToPatientIds, removed);
      removeKeys(rxToPatientIds, removed);
      removeKeys(atcToPatientIds, removed);
      removeKeys(tidToPatientIds, removed);
      removeKeys(tidNegatedToPatientIds, removed);
      removeKeys(tidFamilyHistoryToPatientIds, removed);
    }
    removeAgesStart(s);
    removeAgesEnd(s);
    debug("TOOK " + (System.currentTimeMillis() - time));
  }

  private void removeAgesStart(final IntOpenHashSet keep) {
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < pidToStartEndSortedByStart.size(); x += 3) {
      if (keep.contains(pidToStartEndSortedByStart.get(x))) {
        result.add(pidToStartEndSortedByStart.get(x));
        result.add(pidToStartEndSortedByStart.get(x + 1));
        result.add(pidToStartEndSortedByStart.get(x + 2));
      }
    }
    result.trimToSize();
    pidToStartEndSortedByStart = result;
  }

  private void removeAgesEnd(final IntOpenHashSet keep) {
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < pidToStartEndSortedByEnd.size(); x += 3) {
      if (keep.contains(pidToStartEndSortedByEnd.get(x))) {
        result.add(pidToStartEndSortedByEnd.get(x));
        result.add(pidToStartEndSortedByEnd.get(x + 1));
        result.add(pidToStartEndSortedByEnd.get(x + 2));
      }
    }
    result.trimToSize();
    pidToStartEndSortedByEnd = result;
  }

  public int getLabValuesToPatientCounts(final int labId, final int labValue) {
    int result = 0;
    final IntKeyIntOpenHashMap map = labIdToLabValueToPatientCnt.get(labId);
    if (map != null) {
      result = map.get(labValue);
    }
    return result;
  }

  public IntKeyIntMapIterator getLabValuesToPatientCounts(final int labId) {
    final IntKeyIntOpenHashMap result = labIdToLabValueToPatientCnt.get(labId);
    return result == null ? null : result.entries();
  }

  public void addIcd9(final int icd9UniqueId, final int patientId) {
    statisticsBuilder.recordIcd9(icd9UniqueId, patientId);
    synchronized (icd9MaxKeyNr) {
      icd9MaxKeyNr.setValue(Math.max(icd9MaxKeyNr.getValue(), icd9UniqueId));
    }
  }

  public void addDepartment(final int departmentUniqueId, final int patientId) {
    statisticsBuilder.recordDepartment(departmentUniqueId, patientId);
    synchronized (departmentMaxKeyNr) {
      departmentMaxKeyNr.setValue(Math.max(departmentMaxKeyNr.getValue(), departmentUniqueId));
    }
  }

  public void addIcd10(final int icd10UniqueId, final int patientId) {
    statisticsBuilder.recordIcd10(icd10UniqueId, patientId);
    synchronized (icd10MaxKeyNr) {
      icd10MaxKeyNr.setValue(Math.max(icd10MaxKeyNr.getValue(), icd10UniqueId));
    }
  }

  public void addVisitType(final int visitTypeUniqueId, final int patientId) {
    statisticsBuilder.recordVisitType(visitTypeUniqueId, patientId);
    synchronized (visitTypeMaxKeyNr) {
      visitTypeMaxKeyNr.setValue(Math.max(visitTypeMaxKeyNr.getValue(), visitTypeUniqueId));
    }
  }

  public void addNoteType(final int noteTypeUniqueId, final int patientId) {
    statisticsBuilder.recordNoteType(noteTypeUniqueId, patientId);
    synchronized (noteTypeMaxKeyNr) {
      noteTypeMaxKeyNr.setValue(Math.max(noteTypeMaxKeyNr.getValue(), noteTypeUniqueId));
    }
  }

  public void addTid(final int tid, final int patientId) {
    statisticsBuilder.recordTid(tid, patientId);
  }

  public void addTidNegated(final int tid, final int patientId) {
    statisticsBuilder.recordNegatedTid(tid, patientId);
  }

  public void addTidFamilyHistory(final int tid, final int patientId) {
    statisticsBuilder.recordFhTid(tid, patientId);
  }

  public void addVitals(final int vitalsId, final int patientId) {
    statisticsBuilder.recordVitals(vitalsId, patientId);
  }

  public void addLabs(final int labsId, final int patientId) {
    statisticsBuilder.recordLabs(labsId, patientId);
  }

  public void addLabValue(final int labsId, final int labValueId) {
    synchronized (labIdToLabValueToPatientCnt) {
      IntKeyIntOpenHashMap labIdToMap = labIdToLabValueToPatientCnt.get(labsId);
      if (labIdToMap == null) {
        labIdToMap = new IntKeyIntOpenHashMap();
        labIdToLabValueToPatientCnt.put(labsId, labIdToMap);
      }
      labIdToMap.put(labValueId, labIdToMap.get(labValueId) + 1);
    }
  }

  public void addRxNormStats(final Iterator<ComparableIntArray> rxNormRouteStatusArrayIterator) {
    final HashSet<ComparableIntArray> uniqueRxNormRouteId = new HashSet<>();
    final HashSet<ComparableIntArray> uniqueRxNormStatusId = new HashSet<>();
    while (rxNormRouteStatusArrayIterator.hasNext()) {
      final ComparableIntArray rxNormRouteStatus = rxNormRouteStatusArrayIterator.next();
      final ComparableIntArray rxNormRouteId = new ComparableIntArray(new int[] {rxNormRouteStatus.get(0), rxNormRouteStatus.get(1)});
      final ComparableIntArray rxNormStatusId = new ComparableIntArray(new int[] {rxNormRouteStatus.get(0), rxNormRouteStatus.get(2)});
      if (!uniqueRxNormRouteId.contains(rxNormRouteId)) {
        synchronized (rxToRoutePatientCounts) {
          rxToRoutePatientCounts.add(rxNormRouteId.get(0), rxNormRouteId.get(1));
        }
        uniqueRxNormRouteId.add(rxNormRouteId);
      }
      if (!uniqueRxNormStatusId.contains(rxNormStatusId)) {
        synchronized (rxToStatusPatientCounts) {
          rxToStatusPatientCounts.add(rxNormStatusId.get(0), rxNormStatusId.get(1));
        }
        uniqueRxNormStatusId.add(rxNormStatusId);
      }
      synchronized (rxToRouteStatusPatientCounts) {
        rxToRouteStatusPatientCounts.add(rxNormRouteStatus.get(0), rxNormRouteStatus.get(1), rxNormRouteStatus.get(2));
      }
      synchronized (rxToStatusRoutePatientCounts) {
        rxToStatusRoutePatientCounts.add(rxNormRouteStatus.get(0), rxNormRouteStatus.get(2), rxNormRouteStatus.get(1));
      }
    }
  }

  public void addRxNorm(final int rxNorm, final int patientId) {
    statisticsBuilder.recordRx(rxNorm, patientId);
    synchronized (rxNormMaxKeyNr) {
      rxNormMaxKeyNr.setValue(Math.max(rxNormMaxKeyNr.getValue(), rxNorm));
    }
  }

  public void addAtc(final int atc, final int patientId) {
    statisticsBuilder.recordAtc(atc, patientId);
  }

  public void addAge(final int age) {
    synchronized (ages) {
      if (ages.size() <= age) {
        for (int x = ages.size(); x <= age; x++) {
          ages.add(0);
        }
      }
      ages.set(age, ages.get(age) + 1);
    }
  }

  public IntArrayList getAges() {
    return ages;
  }

  public int getUniqueIcd9Counts() {
    return icd9ToPatientIds.size();
  }

  public int getUniqueDepartmentCounts() {
    return icd9ToPatientIds.size();
  }

  public int getUniqueIcd10Counts() {
    return icd10ToPatientIds.size();
  }

  public IntIterator getUniquerxNormCodes() {
    return rxToPatientIds.keySet().iterator();
  }

  public int getUniqueRxNormCounts() {
    return rxToPatientIds.size();
  }

  public int getUniqueCptCounts() {
    return cptToPatientIds.size();
  }

  public int getUniqueSnomedCounts() {
    return snomedToPatientIds.size();
  }

  public int getIcd9(final int icd9UniqueId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getIcd9(icd9UniqueId);
    }
    return getSameSorted(patientIds, getIcd9Patients(icd9UniqueId));
  }

  public int getIcd10(final int icd10UniqueId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getIcd10(icd10UniqueId);
    }
    return getSameSorted(patientIds, getIcd10Patients(icd10UniqueId));
  }

  public int getCpt(final int cptUniqueId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getCpt(cptUniqueId);
    }
    return getSameSorted(patientIds, getCptPatients(cptUniqueId));
  }

  public int getSnomed(final int snomedUniqueId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getSnomed(snomedUniqueId);
    }
    return getSameSorted(patientIds, getSnomedPatients(snomedUniqueId));
  }

  public int getRxNorm(final int rxNorm, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getRxNorm(rxNorm);
    }
    return getSameSorted(patientIds, getRxNormPatients(rxNorm));
  }

  public int getIcd9(final int icd9UniqueId) {
    if (icd9ToPatientIds.containsKey(icd9UniqueId)) {
      return patientIdLists.getSize(icd9ToPatientIds.get(icd9UniqueId));
    }
    return 0;
  }

  public int getDepartment(final int departmentUniqueId) {
    if (departmentToPatientIds.containsKey(departmentUniqueId)) {
      return patientIdLists.getSize(departmentToPatientIds.get(departmentUniqueId));
    }
    return 0;
  }

  public int getIcd10(final int icd10UniqueId) {
    if (icd10ToPatientIds.containsKey(icd10UniqueId)) {
      return patientIdLists.getSize(icd10ToPatientIds.get(icd10UniqueId));
    }
    return 0;
  }

  public int getVisitType(final int visitTypeUniqueId) {
    if (visitTypeToPatientIds.containsKey(visitTypeUniqueId)) {
      return patientIdLists.getSize(visitTypeToPatientIds.get(visitTypeUniqueId));
    }
    return 0;
  }

  public int getNoteType(final int noteTypeUniqueId) {
    if (noteTypeToPatientIds.containsKey(noteTypeUniqueId)) {
      return patientIdLists.getSize(noteTypeToPatientIds.get(noteTypeUniqueId));
    }
    return 0;
  }

  public static int getSameSorted(final int[] patientIds, final IntIterator list) {
    if (list == null || patientIds == null || !list.hasNext()) {
      return 0;
    }
    final IntArrayList result = new IntArrayList(patientIds.length);
    int listVal = list.next();
    int patientPos = 0;
    while (true) {
      if (listVal == patientIds[patientPos]) {
        result.add(patientIds[patientPos]);
        if (!list.hasNext() || patientPos >= patientIds.length - 1) {
          break;
        }
        listVal = list.next();
        patientPos++;
        continue;
      }
      else if (listVal < patientIds[patientPos]) {
        /*        if (list.get(list.size() - 1) < patientIds[patientPos]) {
                  break;
                }*/
        while (list.hasNext()) {
          listVal = list.next();
          if (listVal >= patientIds[patientPos]) {
            break;
          }
        }
        if (!list.hasNext()) {
          for (int x = patientPos; x < patientIds.length; x++) {
            if (listVal == patientIds[x]) {
              result.add(patientIds[patientPos]);
              break;
            }
          }
          break;
        }
      }
      else if (listVal > patientIds[patientPos]) {
        if (listVal > patientIds[patientIds.length - 1]) {
          break;
        }
        for (int x = patientPos; x < patientIds.length; x++) {
          if (listVal <= patientIds[x]) {
            patientPos = x;
            break;
          }
        }
        if (!list.hasNext()) {
          if (listVal == patientIds[patientPos]) {
            result.add(listVal);
          }
          break;
        }
      }
    }
    return result.size();
  }

  public int getTid(final int tid, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getTid(tid);
    }
    return getSameSorted(patientIds, getTidPatients(tid));
  }

  public int getTidNegated(final int tid, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getTidNegated(tid);
    }
    return getSameSorted(patientIds, getTidNegatedPatients(tid));
  }

  public int getDepartment(final int deptId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getDepartment(deptId);
    }
    return getSameSorted(patientIds, getDepartmentPatients(deptId));
  }

  public int getTidFamilyHistory(final int tid, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getTidFamilyHistory(tid);
    }
    return getSameSorted(patientIds, getTidFamilyHistoryPatients(tid));
  }

  public int getTid(final int tid) {
    if (tidToPatientIds.containsKey(tid)) {
      return patientIdLists.getSize(tidToPatientIds.get(tid));
    }
    return 0;
  }

  public int getTidNegated(final int tid) {
    if (tidNegatedToPatientIds.containsKey(tid)) {
      return patientIdLists.getSize(tidNegatedToPatientIds.get(tid));
    }
    return 0;
  }

  public int getPatientsWithTidFamilyHistoryCnt() {
    return tidFamilyHistoryToPatientIds.size();
  }

  public int getPatientsWithTidCnt() {
    return tidToPatientIds.size();
  }

  public int getPatientsWithDepartmentCnt() {
    return departmentToPatientIds.size();
  }

  public int getPatientsWithNegatedTidCnt() {
    return tidNegatedToPatientIds.size();
  }

  public int getTidFamilyHistory(final int tid) {
    if (tidFamilyHistoryToPatientIds.containsKey(tid)) {
      return patientIdLists.getSize(tidFamilyHistoryToPatientIds.get(tid));
    }
    return 0;
  }

  public int getVitals(final int vitalsId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getVitals(vitalsId);
    }
    return getSameSorted(patientIds, getVitalsPatients(vitalsId));
  }

  public int getLabs(final int labsId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getLabs(labsId);
    }
    return getSameSorted(patientIds, getLabsPatients(labsId));
  }

  public int getVitals(final int codeId) {
    if (vitalsToPatientIds.containsKey(codeId)) {
      return patientIdLists.getSize(vitalsToPatientIds.get(codeId));
    }
    return 0;
  }

  public int getLabs(final int codeId) {
    if (labsToPatientIds.containsKey(codeId)) {
      return patientIdLists.getSize(labsToPatientIds.get(codeId));
    }
    return 0;
  }

  public IntIteratorCloneableInterface getLabsPatients(final int codeId) {
    if (labsToPatientIds.containsKey(codeId)) {
      return patientIdLists.get(labsToPatientIds.get(codeId));
    }
    return null;
  }

  public void addCpt(final int cptUniqueId, final int patientId) {
    statisticsBuilder.recordCpt(cptUniqueId, patientId);
    synchronized (cptMaxKeyNr) {
      cptMaxKeyNr.setValue(Math.max(cptMaxKeyNr.getValue(), cptUniqueId));
    }
  }

  public void addSnomed(final int snomedUniqueId, final int patientId) {
    statisticsBuilder.recordSnomed(snomedUniqueId, patientId);
    synchronized (snomedMaxKeyNr) {
      snomedMaxKeyNr.setValue(Math.max(snomedMaxKeyNr.getValue(), snomedUniqueId));
    }
  }

  public int getCpt(final int cptUniqueCode) {
    if (cptToPatientIds.containsKey(cptUniqueCode)) {
      return patientIdLists.getSize(cptToPatientIds.get(cptUniqueCode));
    }
    return 0;
  }

  public int getSnomed(final int snomedUniqueCode) {
    if (snomedToPatientIds.containsKey(snomedUniqueCode)) {
      return patientIdLists.getSize(snomedToPatientIds.get(snomedUniqueCode));
    }
    return 0;
  }

  public IntIteratorCloneableInterface getTidPatients(final int tid) {
    if (tidToPatientIds.containsKey(tid)) {
      return patientIdLists.get(tidToPatientIds.get(tid));
    }
    return null;
  }

  public IntIteratorCloneableInterface getTidNegatedPatients(final int tid) {
    if (tidNegatedToPatientIds.containsKey(tid)) {
      return patientIdLists.get(tidNegatedToPatientIds.get(tid));
    }
    return null;
  }

  public IntIteratorCloneableInterface getTidFamilyHistoryPatients(final int tid) {
    if (tidFamilyHistoryToPatientIds.containsKey(tid)) {
      return patientIdLists.get(tidFamilyHistoryToPatientIds.get(tid));
    }
    return null;
  }

  public IntIteratorCloneableInterface getVisitTypesPatients(final int visitTypeId) {
    if (visitTypeToPatientIds.containsKey(visitTypeId)) {
      return patientIdLists.get(visitTypeToPatientIds.get(visitTypeId));
    }
    return null;
  }

  public IntIteratorCloneableInterface getNoteTypesPatients(final int noteTypeId) {
    if (noteTypeToPatientIds.containsKey(noteTypeId)) {
      return patientIdLists.get(noteTypeToPatientIds.get(noteTypeId));
    }
    return null;
  }

  public IntIteratorCloneableInterface getRxNormPatients(final int rx) {
    if (rxToPatientIds.containsKey(rx)) {
      return patientIdLists.get(rxToPatientIds.get(rx));
    }
    return null;
  }

  public IntIteratorCloneableInterface getAtcPatients(final int atc) {
    if (atcToPatientIds.containsKey(atc)) {
      return patientIdLists.get(atcToPatientIds.get(atc));
    }
    return null;
  }

  public IntIteratorCloneableInterface getIcd9Patients(final int icd9) {
    if (icd9ToPatientIds.containsKey(icd9)) {
      return patientIdLists.get(icd9ToPatientIds.get(icd9));
    }
    return null;
  }

  public IntIteratorCloneableInterface getDepartmentPatients(final int department) {
    if (departmentToPatientIds.containsKey(department)) {
      return patientIdLists.get(departmentToPatientIds.get(department));
    }
    return null;
  }

  public IntIteratorCloneableInterface getIcd10Patients(final int icd10) {
    if (icd10ToPatientIds.containsKey(icd10)) {
      return patientIdLists.get(icd10ToPatientIds.get(icd10));
    }
    return null;
  }

  public IntIteratorCloneableInterface getVitalsPatients(final int vitalsId) {
    if (vitalsToPatientIds.containsKey(vitalsId)) {
      return patientIdLists.get(vitalsToPatientIds.get(vitalsId));
    }
    return null;
  }

  public IntIteratorCloneableInterface getCptPatients(final int cpt) {
    if (cptToPatientIds.containsKey(cpt)) {
      return patientIdLists.get(cptToPatientIds.get(cpt));
    }
    return null;
  }

  public IntIteratorCloneableInterface getSnomedPatients(final int snomed) {
    if (snomedToPatientIds.containsKey(snomed)) {
      return patientIdLists.get(snomedToPatientIds.get(snomed));
    }
    return null;
  }

  public int getRxNorm(final int rxNorm) {
    if (rxToPatientIds.containsKey(rxNorm)) {
      return patientIdLists.getSize(rxToPatientIds.get(rxNorm));
    }
    return 0;
  }

  public int getAtc(final int atc, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getAtc(atc);
    }
    return getSameSorted(patientIds, getAtcPatients(atc));
  }

  public int getVisitType(final int visitTypeId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getVisitType(visitTypeId);
    }
    return getSameSorted(patientIds, getVisitTypesPatients(visitTypeId));
  }

  public int getNoteType(final int noteTypeId, final int[] patientIds) {
    if (patientIds == null || patientIds.length == 0) {
      return getNoteType(noteTypeId);
    }
    return getSameSorted(patientIds, getNoteTypesPatients(noteTypeId));
  }

  public int getAtc(final int atc) {
    if (atcToPatientIds.containsKey(atc)) {
      return patientIdLists.getSize(atcToPatientIds.get(atc));
    }
    return 0;
  }

  public int getPatientsWithCpt() {
    return patientsWithCpt.get();
  }

  public int getPatientsWithIcd9() {
    return patientsWithIcd9.get();
  }

  public int getPatientsWithIcd10() {
    return patientsWithIcd10.get();
  }

  public int getPatientsWithLabs() {
    return patientsWithLabs.get();
  }

  public int getPatientsWithDrugs() {
    return patientsWithDrugs.get();
  }

  public int getPatientsWithDepartments() {
    return patientsWithDepartments.get();
  }

  public int getPatientsWithNotes() {
    return patientsWithNotes.get();
  }

  private int loadMap(final Input input, final LongMutable position, final IntKeyIntOpenHashMap map, int initialKeyCnt) throws IOException {
    final int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final int value = input.readInt(position);
      map.put(key, value);
      initialKeyCnt = Math.max(initialKeyCnt, key);
    }
    return initialKeyCnt;
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    icd9ToPatientIds.clear();
    departmentToPatientIds.clear();
    icd10ToPatientIds.clear();
    cptToPatientIds.clear();
    snomedToPatientIds.clear();
    rxToPatientIds.clear();
    vitalsToPatientIds.clear();
    atcToPatientIds.clear();
    labsToPatientIds.clear();
    visitTypeToPatientIds.clear();
    noteTypeToPatientIds.clear();
    ages.clear();
    patientIds.clear();
    final int version = input.readInt(position);
    if (version != getCurrentVersion()) {
      throw new IOException("Unsupported version. Current version = " + version + ", supported version = " + CURRENT_VERSION);
    }
    int size = input.readUnsignedByte(position);
    System.out.println("AGES size = " + size);
    for (int x = 0; x < size; x++) {
      ages.add(input.readInt(position));
    }
    ages.trimToSize();
    icd9MaxKeyNr.setValue(loadMap(input, position, icd9ToPatientIds, icd9MaxKeyNr.getValue()));
    icd10MaxKeyNr.setValue(loadMap(input, position, icd10ToPatientIds, icd10MaxKeyNr.getValue()));
    cptMaxKeyNr.setValue(loadMap(input, position, cptToPatientIds, cptMaxKeyNr.getValue()));
    snomedMaxKeyNr.setValue(loadMap(input, position, snomedToPatientIds, snomedMaxKeyNr.getValue()));
    departmentMaxKeyNr.setValue(loadMap(input, position, departmentToPatientIds, departmentMaxKeyNr.getValue()));
    rxNormMaxKeyNr.setValue(loadMap(input, position, rxToPatientIds, rxNormMaxKeyNr.getValue()));
    loadMap(input, position, atcToPatientIds, 0);
    loadMap(input, position, tidToPatientIds, 0);
    loadMap(input, position, tidNegatedToPatientIds, 0);
    loadMap(input, position, tidFamilyHistoryToPatientIds, 0);
    loadMap(input, position, vitalsToPatientIds, 0);
    labMaxKeyNr.setValue(loadMap(input, position, labsToPatientIds, 0));
    loadMap(input, position, visitTypeToPatientIds, 0);
    loadMap(input, position, noteTypeToPatientIds, 0);
    position.set(rxToRoutePatientCounts.load(input, position.get()));
    position.set(rxToRouteStatusPatientCounts.load(input, position.get()));
    position.set(rxToStatusPatientCounts.load(input, position.get()));
    position.set(rxToStatusRoutePatientCounts.load(input, position.get()));
    loadLabIdToValuesMap(input, position);
    position.set(patientIdLists.load(input, position.get()));

    size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int val = input.readInt(position);
      patientIds.add(val);
      maxPatientId = Math.max(maxPatientId, val);
    }
    patientIds.trimToSize();

    patientCnt = patientIds.size();

    patientsWithCpt.set(input.readInt(position));
    patientsWithIcd9.set(input.readInt(position));
    patientsWithIcd10.set(input.readInt(position));
    patientsWithDrugs.set(input.readInt(position));
    patientsWithDepartments.set(input.readInt(position));
    patientsWithLabs.set(input.readInt(position));
    patientsWithNotes.set(input.readInt(position));

    size = input.readInt(position);
    System.out.println("PIDTOSTARTEND size = " + size);
    for (int x = 0; x < size; x++) {
      pidToStartEndSortedByStart.add(input.readInt(position));
    }
    pidToStartEndSortedByStart.trimToSize();
    for (int x = 0; x < size; x++) {
      pidToStartEndSortedByEnd.add(input.readInt(position));
    }
    pidToStartEndSortedByEnd.trimToSize();
    System.out.println("PIDTOSTARTEND size2 = " + pidToStartEndSortedByStart.size());
    System.out.println("PIDTOSTARTEND size3 = " + pidToStartEndSortedByEnd.size());
    return position.get();
  }

  public void recordCompleteIcd9Code(final int icd9Code, final int[] pids) {
    statisticsBuilder = null;
    final int listId = patientIdLists.add(pids);
    icd9ToPatientIds.put(icd9Code, listId);
  }

  public void recordCompleteIcd10Code(final int icd10Code, final int[] pids) {
    statisticsBuilder = null;
    final int listId = patientIdLists.add(pids);
    icd10ToPatientIds.put(icd10Code, listId);
  }

  public void recordCompleteCptCode(final int cptCode, final int[] pids) {
    statisticsBuilder = null;
    final int listId = patientIdLists.add(pids);
    cptToPatientIds.put(cptCode, listId);
  }

  public void recordCompleteAtcCode(final int atcCode, final int[] pids) {
    statisticsBuilder = null;
    final int listId = patientIdLists.add(pids);
    atcToPatientIds.put(atcCode, listId);
  }

  public void recordCompleteRxCode(final int rxCode, final int[] pids) {
    statisticsBuilder = null;
    final int listId = patientIdLists.add(pids);
    rxToPatientIds.put(rxCode, listId);
  }

  public void recordCompleteVisitType(final int visitType, final int[] pids) {
    statisticsBuilder = null;
    final int listId = patientIdLists.add(pids);
    visitTypeToPatientIds.put(visitType, listId);
  }

  public void finalizePatientIdLists(final Output output) throws IOException {
    if (statisticsBuilder != null) {
      debug("Statistics ICD9");
      statisticsBuilder.finalizeIcd9(patientIdLists, icd9ToPatientIds);
      saveMap(output, icd9ToPatientIds);
      debug("Statistics ICD10");
      statisticsBuilder.finalizeIcd10(patientIdLists, icd10ToPatientIds);
      saveMap(output, icd10ToPatientIds);
      debug("Statistics CPT");
      statisticsBuilder.finalizeCpt(patientIdLists, cptToPatientIds);
      saveMap(output, cptToPatientIds);
      debug("Statistics SNOMED");
      statisticsBuilder.finalizeSnomed(patientIdLists, snomedToPatientIds);
      saveMap(output, snomedToPatientIds);
      debug("Statistics DEPARTMENT");
      statisticsBuilder.finalizeDepartment(patientIdLists, departmentToPatientIds);
      saveMap(output, departmentToPatientIds);
      debug("Statistics RX");
      statisticsBuilder.finalizeRx(patientIdLists, rxToPatientIds);
      saveMap(output, rxToPatientIds);
      debug("Statistics ATC");
      statisticsBuilder.finalizeAtc(patientIdLists, atcToPatientIds);
      saveMap(output, atcToPatientIds);
      debug("Statistics TID");
      statisticsBuilder.finalizeTid(patientIdLists, tidToPatientIds);
      saveMap(output, tidToPatientIds);
      debug("Statistics nTID");
      statisticsBuilder.finalizeTidNegated(patientIdLists, tidNegatedToPatientIds);
      saveMap(output, tidNegatedToPatientIds);
      debug("Statistics fhTID");
      statisticsBuilder.finalizeTidFh(patientIdLists, tidFamilyHistoryToPatientIds);
      saveMap(output, tidFamilyHistoryToPatientIds);
      debug("Statistics VITALS");
      statisticsBuilder.finalizeVitals(patientIdLists, vitalsToPatientIds);
      saveMap(output, vitalsToPatientIds);
      debug("Statistics LABS");
      statisticsBuilder.finalizeLabs(patientIdLists, labsToPatientIds);
      saveMap(output, labsToPatientIds);
      debug("Statistics VTYPE");
      statisticsBuilder.finalizeVisitType(patientIdLists, visitTypeToPatientIds);
      saveMap(output, visitTypeToPatientIds);
      debug("Statistics NTYPE");
      statisticsBuilder.finalizeNoteType(patientIdLists, noteTypeToPatientIds);
      saveMap(output, noteTypeToPatientIds);
    }
    else {
      debug("Statistics ICD9");
      saveMap(output, icd9ToPatientIds);
      debug("Statistics ICD10");
      saveMap(output, icd10ToPatientIds);
      debug("Statistics CPT");
      saveMap(output, cptToPatientIds);
      debug("Statistics SNOMED");
      saveMap(output, snomedToPatientIds);
      debug("Statistics DEPARTMENT");
      saveMap(output, departmentToPatientIds);
      debug("Statistics RX");
      saveMap(output, rxToPatientIds);
      debug("Statistics ATC");
      saveMap(output, atcToPatientIds);
      debug("Statistics TID");
      saveMap(output, tidToPatientIds);
      debug("Statistics nTID");
      saveMap(output, tidNegatedToPatientIds);
      debug("Statistics fhTID");
      saveMap(output, tidFamilyHistoryToPatientIds);
      debug("Statistics VITALS");
      saveMap(output, vitalsToPatientIds);
      debug("Statistics LABS");
      saveMap(output, labsToPatientIds);
      debug("Statistics VTYPE");
      saveMap(output, visitTypeToPatientIds);
      debug("Statistics NTYPE");
      saveMap(output, noteTypeToPatientIds);
    }
  }

  @Override
  public void save(final Output output) throws IOException {
    output.writeInt(CURRENT_VERSION);
    output.writeUnsignedByte(ages.size());
    for (int x = 0; x < ages.size(); x++) {
      output.writeInt(ages.get(x));
    }
    finalizePatientIdLists(output);
    rxToRoutePatientCounts.save(output);
    rxToRouteStatusPatientCounts.save(output);
    rxToStatusPatientCounts.save(output);
    rxToStatusRoutePatientCounts.save(output);
    saveLabIdToValuesMap(output);
    patientIdLists.save(output);
    patientIds.sort();
    output.writeInt(patientIds.size());
    for (int x = 0; x < patientIds.size(); x++) {
      output.writeInt(patientIds.get(x));
    }
    output.writeInt(patientsWithCpt.get());
    output.writeInt(patientsWithIcd9.get());
    output.writeInt(patientsWithIcd10.get());
    output.writeInt(patientsWithDrugs.get());
    output.writeInt(patientsWithDepartments.get());
    output.writeInt(patientsWithLabs.get());
    output.writeInt(patientsWithNotes.get());
    System.out.println("Statistics builder = " + statisticsBuilder);
    if (pidToStartEndSortedByStart == null || pidToStartEndSortedByStart.size() == 0) {
      if (ageGenerator != null) {
        System.out.println("AGE generator " + ageGenerator);
        pidToStartEndSortedByStart = ageGenerator.getSortedByStart();
        System.out.println("PIDTOSTARTEND size = " + pidToStartEndSortedByStart.size());
      }
      else {
        if (statisticsBuilder != null) {
          pidToStartEndSortedByStart = statisticsBuilder.getPidStartEndOrderedByStart();
        }
      }
    }
    System.out.println("Statistics save pidToStartEndSortedByStart size = " + pidToStartEndSortedByStart.size());
    output.writeInt(pidToStartEndSortedByStart.size());
    for (int x = 0; x < pidToStartEndSortedByStart.size(); x++) {
      output.writeInt(pidToStartEndSortedByStart.get(x));
    }
    if (pidToStartEndSortedByEnd == null || pidToStartEndSortedByEnd.size() == 0) {
      if (ageGenerator != null) {
        pidToStartEndSortedByStart.clear();
        pidToStartEndSortedByEnd = ageGenerator.getSortedByEnd();
      }
      else {
        if (statisticsBuilder != null) {
          pidToStartEndSortedByStart.clear();
          pidToStartEndSortedByEnd = statisticsBuilder.getPidStartEndOrderedByEnd();
        }
      }
    }
    for (int x = 0; x < pidToStartEndSortedByEnd.size(); x++) {
      output.writeInt(pidToStartEndSortedByEnd.get(x));
    }
  }

  private void loadLabIdToValuesMap(final Input input, final LongMutable position) throws IOException {
    labIdToLabValueToPatientCnt.clear();
    final int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final int mapSize = input.readInt(position);
      final IntKeyIntOpenHashMap map = new IntKeyIntOpenHashMap();
      for (int y = 0; y < mapSize; y++) {
        map.put(input.readInt(position), input.readInt(position));
      }
      labIdToLabValueToPatientCnt.put(key, map);
    }
  }

  private void saveLabIdToValuesMap(final Output output) throws IOException {
    output.writeInt(labIdToLabValueToPatientCnt.size());
    final IntKeyObjectIterator<IntKeyIntOpenHashMap> iterator = labIdToLabValueToPatientCnt.entries();
    while (iterator.hasNext()) {
      iterator.next();
      output.writeInt(iterator.getKey());
      output.writeInt(iterator.getValue().size());
      final IntKeyIntMapIterator i = iterator.getValue().entries();
      while (i.hasNext()) {
        i.next();
        output.writeInt(i.getKey());
        output.writeInt(i.getValue());
      }
    }
  }

  private void saveMap(final Output output, final IntKeyIntOpenHashMap map) throws IOException {
    if (output != null) {
      output.writeInt(map.size());
      final IntKeyIntMapIterator iterator = map.entries();
      while (iterator.hasNext()) {
        iterator.next();
        output.writeInt(iterator.getKey());
        output.writeInt(iterator.getValue());
      }
    }
  }

  @Override
  public boolean isEmpty() {
    return icd9ToPatientIds.size() == 0 && icd10ToPatientIds.size() == 0 && cptToPatientIds.size() == 0 && rxToPatientIds.size() == 0 && tidToPatientIds.size() == 0;
  }

  @Override
  public String toString() {
    return statisticsBuilder.toString();
  }

  @Override
  public int getCurrentVersion() {
    return CURRENT_VERSION;
  }
}