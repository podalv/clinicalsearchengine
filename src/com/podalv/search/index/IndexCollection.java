package com.podalv.search.index;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.podalv.extractor.datastructures.records.DemographicsRecord;
import com.podalv.extractor.stride6.Common;
import com.podalv.input.Input;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.list.IntIteratorCloneableInterface;
import com.podalv.maps.primitive.map.ObjectKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.IntKeyStringIterator;
import com.podalv.maps.string.IntKeyStringMap;
import com.podalv.objectdb.PersistentObject;
import com.podalv.objectdb.datastructures.PersistentIntArrayList;
import com.podalv.output.Output;
import com.podalv.search.datastructures.index.StringEnumIndexBuilder;
import com.podalv.search.datastructures.index.StringIntegerFastIndex;
import com.podalv.utils.datastructures.LongMutable;

/** Encapsulates the collection of all original indices (text => id)...
 *
 * @author podalv
 *
 */
public class IndexCollection implements PersistentObject {

  public static final int                                           CURRENT_VERSION                   = 4;
  private final StringIntegerFastIndex                              labValues                         = new StringIntegerFastIndex();
  private final DemographicsBuilder                                 demographicsBuilder               = new DemographicsBuilder();
  private Demographics                                              demographics                      = new Demographics();
  private final StringIntegerFastIndex                              icd9                              = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              department                        = new StringIntegerFastIndex();
  private final IntOpenHashSet                                      icd9HierarchyParentIds            = new IntOpenHashSet();
  private final StringIntegerFastIndex                              icd10                             = new StringIntegerFastIndex();
  private final IntOpenHashSet                                      icd10HierarchyParentIds           = new IntOpenHashSet();
  private final StringIntegerFastIndex                              atc                               = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              snomed                            = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              drugStatus                        = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              drugRoute                         = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              docDescription                    = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              cpt                               = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              vitals                            = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              labs                              = new StringIntegerFastIndex();
  private final StringIntegerFastIndex                              visitTypes                        = new StringIntegerFastIndex();
  private final IntKeyStringMap                                     labsIdToCommonName                = new IntKeyStringMap();
  private final IntKeyObjectMap<double[]>                           codeToDoubleIndexToDoubleValueMap = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<String>                             tidToString                       = new IntKeyObjectMap<>();
  private final ObjectKeyIntOpenHashMap                             stringToTid                       = new ObjectKeyIntOpenHashMap();
  private final HashMap<String, String>                             snomedToText                      = new HashMap<>();
  private transient final IntKeyObjectMap<HashMap<Double, Integer>> codeToDoubleValueToDoubleIndexMap = new IntKeyObjectMap<>();
  private UmlsDictionary                                            umls;

  public void clearTidToStringMap() {
    tidToString.clear();
    stringToTid.clear();
  }

  public IntIterator getUniqueTids() {
    return tidToString.keySet().iterator();
  }

  public void setUmls(final UmlsDictionary dict) {
    this.umls = dict;
  }

  public void addAllIcd9HierarchyParents() {
    if (umls != null) {
      final HashSet<String> parents = umls.getIcd9HierarchyParents();
      final Iterator<String> i = parents.iterator();
      while (i.hasNext()) {
        final String v = i.next();
        icd9HierarchyParentIds.add(addIcd9Code(v));
      }
    }
  }

  public void addAllIcd10HierarchyParents() {
    if (umls != null) {
      final HashSet<String> parents = umls.getIcd10HierarchyParents();
      final Iterator<String> i = parents.iterator();
      while (i.hasNext()) {
        final String v = i.next();
        icd10HierarchyParentIds.add(addIcd10Code(v));
      }
    }
  }

  public String[] getVisitTypes() {
    return visitTypes.keySet();
  }

  public String getLabName(final int id) {
    return labs.get(id);
  }

  public void addSnomedText(final String code, final String text) {
    snomedToText.put(code, text);
  }

  public String getText(final String code) {
    return snomedToText.get(code);
  }

  public String[] getDrugStatus() {
    return drugStatus.keySet();
  }

  public String[] getDrugRoute() {
    return drugRoute.keySet();
  }

  public String[] getLabValues() {
    return labValues.keySet();
  }

  public int getGenderKeySize() {
    return demographics.getGender().keySize();
  }

  public int getDeath(final int pid) {
    return demographics.getDeathTime(pid);
  }

  public int getRaceKeySize() {
    return demographics.getRace().keySize();
  }

  public int getEthnicityKeySize() {
    return demographics.getEthnicity().keySize();
  }

  public void addTid(final int tid, final String term) {
    tidToString.put(tid, Common.filterString(term.toLowerCase()));
    stringToTid.put(Common.filterString(term.toLowerCase()), tid);
  }

  public int getTid(final String str) {
    return stringToTid.containsKey(str) ? stringToTid.get(str) : -1;
  }

  public String getStringFromTid(final int tid) {
    return tidToString.get(tid);
  }

  public int getMaxTid() {
    int maxTid = Integer.MIN_VALUE;
    final IntIterator i = tidToString.keySet().iterator();
    while (i.hasNext()) {
      maxTid = Math.max(i.next(), maxTid);
    }
    return maxTid;
  }

  public boolean containsLabValueId(final String value) {
    return labValues.contains(Common.filterString(value.toUpperCase()));
  }

  public int getLabValueId(final String value) {
    return labValues.add(Common.filterString(value.toUpperCase()));
  }

  public String getLabValueText(final int id) {
    return labValues.get(id);
  }

  public void addDeathTime(final int patientId, final int time) {
    synchronized (demographics) {
      if (time != Demographics.DEATH_NOT_RECORDED) {
        demographics.addDeadPatientId(patientId);
      }
      demographicsBuilder.addDeathTime(patientId, time);
    }
  }

  public void setDocDescription(final StringIntegerFastIndex index) {
    docDescription.clear();
    final String[] keys = index.keySet();
    for (final String key : keys) {
      docDescription.add(key);
    }
  }

  public String[] getNoteDescriptionNames() {
    return docDescription.keySet();
  }

  public int getNoteTypeId(final String str) {
    return docDescription.get(str.toLowerCase());
  }

  public String getNoteTypeText(final int id) {
    return docDescription.get(id);
  }

  public boolean isIcd9Parent(final int icd9Code) {
    return icd9HierarchyParentIds.contains(icd9Code);
  }

  public boolean isIcd10Parent(final int icd10Code) {
    return icd10HierarchyParentIds.contains(icd10Code);
  }

  public int getVisitType(final String name) {
    return visitTypes.get(name.toLowerCase());
  }

  public String[] getVisitTypeNames() {
    return visitTypes.keySet();
  }

  public String[] getLabsNames() {
    return labs.keySet();
  }

  public String[] getVitalsNames() {
    return vitals.keySet();
  }

  public String[] getAtcNames() {
    return atc.keySet();
  }

  public int getAtcCode(final String atcString) {
    return atc.get(atcString.toLowerCase());
  }

  public int getDoubleIndexSize(final int code) {
    return codeToDoubleIndexToDoubleValueMap.get(code).length;
  }

  public String getVitalsName(final int code) {
    return vitals.get(code);
  }

  public String getVisitTypeName(final int code) {
    return visitTypes.get(code);
  }

  public int getVitalsCode(final String vitalsString) {
    return vitals.get(vitalsString);
  }

  public boolean containsTidString(final String text) {
    return stringToTid.containsKey(Common.filterString(text));
  }

  public Iterator<Object> getTidStrings() {
    return stringToTid.keySet().iterator();
  }

  public int addDrugRouteId(final String route) {
    return drugRoute.add(Common.filterString(route).toLowerCase());
  }

  public int addDrugStatusId(final String status) {
    return drugStatus.add(Common.filterString(status).toLowerCase());
  }

  public String getDrugRouteString(final int routeId) {
    return drugRoute.get(routeId);
  }

  public String getDrugStatusString(final int statusId) {
    return drugStatus.get(statusId);
  }

  public int getDrugRouteId(final String route) {
    return route == null ? drugRoute.get("null") : drugRoute.get(route.toLowerCase());
  }

  public int getDrugStatusId(final String status) {
    return status == null ? drugStatus.get("null") : drugStatus.get(status.toLowerCase());
  }

  public int getLabsCode(final String labsString) {
    return labs.get(labsString.toLowerCase());
  }

  public static IndexCollection createWithoutIcd9Hierarchy() {
    final IndexCollection result = new IndexCollection();
    result.icd9.clear();
    result.icd9HierarchyParentIds.clear();
    return result;
  }

  public IndexCollection(final UmlsDictionary umls) {
    this.umls = UmlsDictionary.create();
    addAllIcd9HierarchyParents();
    addAllIcd10HierarchyParents();
  }

  public IndexCollection() {
    this.umls = UmlsDictionary.create();
    addAllIcd9HierarchyParents();
    addAllIcd10HierarchyParents();
  }

  public IndexCollection(final UmlsDictionary umls, final DemographicsBuilder demographicsBuilder) {
    this.umls = UmlsDictionary.create();
    addAllIcd9HierarchyParents();
    addAllIcd10HierarchyParents();
  }

  public UmlsDictionary getUmls() {
    return umls;
  }

  public IntIteratorCloneableInterface getGenderIterator(final int genderId) {
    return demographics.getGender().iterator(genderId);
  }

  public IntIteratorCloneableInterface getEthnicityIterator(final int ethnicityId) {
    return demographics.getEthnicity().iterator(ethnicityId);
  }

  public IntIteratorCloneableInterface getRaceIterator(final int raceId) {
    return demographics.getRace().iterator(raceId);
  }

  public void addDemographicRecord(final DemographicsRecord record) {
    synchronized (demographicsBuilder) {
      demographicsBuilder.addDeathTime(record.getPatientId(), record.getDeathInDays());
      demographicsBuilder.addEthnicity(record.getPatientId(), (record.getEthnicity() == null || record.getEthnicity().isEmpty()) ? StringEnumIndexBuilder.EMPTY
          : record.getEthnicity());
      demographicsBuilder.addGender(record.getPatientId(), (record.getGender() == null || record.getGender().isEmpty()) ? StringEnumIndexBuilder.EMPTY : record.getGender());
      demographicsBuilder.addRace(record.getPatientId(), (record.getRace() == null || record.getRace().isEmpty()) ? StringEnumIndexBuilder.EMPTY : record.getRace());
    }
  }

  public void addEmptyDemographicRecord(final int patientId) {
    synchronized (demographicsBuilder) {
      demographicsBuilder.addDeathTime(patientId, 0);
      demographicsBuilder.addEthnicity(patientId, StringEnumIndexBuilder.NULL);
      demographicsBuilder.addGender(patientId, StringEnumIndexBuilder.NULL);
      demographicsBuilder.addRace(patientId, StringEnumIndexBuilder.NULL);
    }
  }

  public String getGender(final int patientId) {
    return demographics.getGender().getString(demographics.getGender().getValue(patientId));
  }

  public int getGenderId(final int patientId) {
    return demographics.getGender().getValue(patientId);
  }

  public int getRaceId(final int patientId) {
    return demographics.getRace().getValue(patientId);
  }

  public int getEthnicityId(final int patientId) {
    return demographics.getEthnicity().getValue(patientId);
  }

  public String getRace(final int patientId) {
    return demographics.getRace().getString(demographics.getRace().getValue(patientId));
  }

  public String getEthnicity(final int patientId) {
    return demographics.getEthnicity().getString(demographics.getEthnicity().getValue(patientId));
  }

  public boolean containsGender(final int patientId, final int expectedGenderId) {
    return demographics.getGender().getValue(patientId) == expectedGenderId;
  }

  public boolean containsRace(final int patientId, final int expectedRaceId) {
    return demographics.getRace().getValue(patientId) == expectedRaceId;
  }

  public boolean containsEthnicity(final int patientId, final int expectedEthnicityId) {
    return demographics.getEthnicity().getValue(patientId) == expectedEthnicityId;
  }

  public String[] getGenders() {
    return demographics.getGender().keys();
  }

  public String[] getRaces() {
    return demographics.getRace().keys();
  }

  public String[] getEthnicities() {
    return demographics.getEthnicity().keys();
  }

  public Demographics getDemographics() {
    return demographics;
  }

  public double[] getDoubleValues(final int labIndex) {
    return codeToDoubleIndexToDoubleValueMap.get(labIndex);
  }

  public Double getDoubleValue(final int labIndex, final int doubleIndex) {
    final double[] result = codeToDoubleIndexToDoubleValueMap.get(labIndex);
    return result == null ? null : result[doubleIndex];
  }

  private void saveDoubleIndices(final Output output) throws IOException {
    output.writeInt(codeToDoubleIndexToDoubleValueMap.size());
    final IntKeyObjectIterator<double[]> i = codeToDoubleIndexToDoubleValueMap.entries();
    while (i.hasNext()) {
      i.next();
      output.writeInt(i.getKey());
      output.writeInt(i.getValue().length);
      for (int x = 0; x < i.getValue().length; x++) {
        output.writeLong(Double.doubleToLongBits(i.getValue()[x]));
      }
    }
    output.writeInt(codeToDoubleValueToDoubleIndexMap.size());
    final IntKeyObjectIterator<HashMap<Double, Integer>> iterator = codeToDoubleValueToDoubleIndexMap.entries();
    while (iterator.hasNext()) {
      iterator.next();
      output.writeInt(iterator.getKey());
      output.writeInt(iterator.getValue().size());
      final Iterator<Entry<Double, Integer>> valueIterator = iterator.getValue().entrySet().iterator();
      while (valueIterator.hasNext()) {
        final Entry<Double, Integer> entry = valueIterator.next();
        output.writeLong(Double.doubleToLongBits(entry.getKey()));
        output.writeInt(entry.getValue());
      }
    }
  }

  private void loadDoubleIndices(final Input input, final LongMutable position) throws IOException {
    int size = input.readInt(position);
    codeToDoubleIndexToDoubleValueMap.clear();
    codeToDoubleValueToDoubleIndexMap.clear();
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final int length = input.readInt(position);
      final double[] values = new double[length];
      for (int y = 0; y < length; y++) {
        values[y] = Double.longBitsToDouble(input.readLong(position));
      }
      if (values.length > 65535) {
        System.out.println("Too many values (" + values.length + ") in the lab " + labs.get(key));
      }
      codeToDoubleIndexToDoubleValueMap.put(key, values);
    }

    size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final int length = input.readInt(position);
      final HashMap<Double, Integer> map = new HashMap<>();
      for (int y = 0; y < length; y++) {
        map.put(Double.longBitsToDouble(input.readLong(position)), input.readInt(position));
      }
      codeToDoubleValueToDoubleIndexMap.put(key, map);
    }
  }

  public int getDoubleIndex(final int labIndex, final Double doubleValue) {
    synchronized (codeToDoubleValueToDoubleIndexMap) {
      HashMap<Double, Integer> map = codeToDoubleValueToDoubleIndexMap.get(labIndex);
      double[] reversedMap = codeToDoubleIndexToDoubleValueMap.get(labIndex);
      if (map == null) {
        map = new HashMap<>();
        reversedMap = new double[0];
        codeToDoubleValueToDoubleIndexMap.put(labIndex, map);
        codeToDoubleIndexToDoubleValueMap.put(labIndex, reversedMap);
      }
      Integer result = map.get(doubleValue);
      if (result == null) {
        result = Integer.valueOf(reversedMap.length);
        reversedMap = Arrays.copyOf(reversedMap, reversedMap.length + 1);
        reversedMap[reversedMap.length - 1] = doubleValue;
        map.put(doubleValue, reversedMap.length - 1);
        codeToDoubleIndexToDoubleValueMap.put(labIndex, reversedMap);
      }
      return result;
    }
  }

  public int addVitals(final String vitalsDescription) {
    return vitals.add(Common.filterString(vitalsDescription), Common.filterString(vitalsDescription.toLowerCase()));
  }

  public void addLabsCommonName(final String baseName, final String commonName) {
    labsIdToCommonName.put(labs.get(Common.filterString(baseName.toLowerCase())), Common.filterString(commonName));
  }

  public int addLabs(final String labsCode) {
    final int result = labs.add(Common.filterString(labsCode), Common.filterString(labsCode.toLowerCase()));
    return result;
  }

  public String getLabCommonName(final int labId) {
    return labsIdToCommonName.get(labId);
  }

  public int addVisitTypeCode(final String code) {
    return (code == null || code.isEmpty()) ? visitTypes.add("undefined") : visitTypes.add(Common.filterString(code.toLowerCase()));
  }

  public int setIcd9Code(final int id, final String icdCode) {
    icd9.set(id, Common.filterString(icdCode));
    return id;
  }

  public int setDepartmentCode(final int id, final String departmentCode) {
    department.set(id, Common.filterString(departmentCode).toLowerCase());
    return id;
  }

  public int addIcd9Code(final String icdCode) {
    return icd9.add(Common.filterString(icdCode));
  }

  public int addDepartmentCode(final String departmentCode) {
    return department.add(Common.filterString(departmentCode).toLowerCase());
  }

  public boolean containsIcd9(final String icd9String) {
    return icd9.contains(icd9String);
  }

  public int getIcd9(final String icd9String) {
    return (containsIcd9(icd9String)) ? icd9.get(icd9String) : -1;
  }

  public String getIcd9(final int icd9Code) {
    return icd9Code <= icd9.size() - 1 ? icd9.get(icd9Code) : null;
  }

  public boolean containsDepartment(final String departmentString) {
    return department.contains(departmentString.toLowerCase());
  }

  public int getDepartment(final String departmentString) {
    final String dept = departmentString.toLowerCase();
    return (containsDepartment(dept)) ? department.get(dept) : -1;
  }

  public String getDepartment(final int departmentCode) {
    return departmentCode <= department.size() - 1 ? department.get(departmentCode) : null;
  }

  public int setIcd10Code(final int id, final String icdCode) {
    icd10.set(id, Common.filterString(icdCode));
    return id;
  }

  public int addIcd10Code(final String icdCode) {
    return icd10.add(Common.filterString(icdCode));
  }

  public boolean containsIcd10(final String icd10String) {
    return icd10.contains(icd10String);
  }

  public int getIcd10(final String icd10String) {
    return (containsIcd10(icd10String)) ? icd10.get(icd10String) : -1;
  }

  public String getIcd10(final int icd10Code) {
    return icd10Code <= icd10.size() - 1 ? icd10.get(icd10Code) : null;
  }

  public int setCptCode(final int id, final String cptCode) {
    cpt.set(id, Common.filterString(cptCode));
    return id;
  }

  public int addCptCode(final String cptCode) {
    return cpt.add(Common.filterString(cptCode));
  }

  public int addSnomedCode(final String snomedCode) {
    return snomed.add(Common.filterString(snomedCode));
  }

  public int addAtcCode(final String atcCode) {
    return atc.add(Common.filterString(atcCode.toLowerCase()));
  }

  public boolean containsCpt(final String cptString) {
    return cpt.contains(cptString);
  }

  public boolean containsSnomed(final String snomedString) {
    return snomed.contains(snomedString);
  }

  public boolean containsTid(final int tid) {
    return tidToString.containsKey(tid);
  }

  public int getCpt(final String cptString) {
    return containsCpt(cptString) ? cpt.get(cptString) : -1;
  }

  public String[] getSnomedKeys() {
    return snomed.keySet();
  }

  private void saveSnomedTextMap(final Output output) throws IOException {
    output.writeInt(snomedToText.size());
    final Iterator<Entry<String, String>> iterator = snomedToText.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, String> entry = iterator.next();
      output.writeString(entry.getKey());
      output.writeString(entry.getValue());
    }
  }

  private void loadSnomedTextMap(final Input output, final LongMutable position) throws IOException {
    snomedToText.clear();
    final int size = output.readInt(position);
    for (int x = 0; x < size; x++) {
      snomedToText.put(output.readString(position), output.readString(position));
    }
  }

  public int getSnomed(final String snomedString) {
    return containsSnomed(snomedString) ? snomed.get(snomedString) : -1;
  }

  public String getSnomedCode(final int snomedId) {
    return snomed.get(snomedId);
  }

  public String getSnomedText(final int snomedId) {
    return snomedToText.get(snomed.get(snomedId));
  }

  public String getSnomedText(final String snomedCode) {
    return snomedToText.get(snomedCode);
  }

  public String getCpt(final int cptCode) {
    return cptCode <= cpt.size() - 1 ? cpt.get(cptCode) : null;
  }

  private void saveLabsCommonNames(final Output output) throws IOException {
    output.writeInt(labsIdToCommonName.size());
    final IntKeyStringIterator iterator = labsIdToCommonName.entries();
    while (iterator.hasNext()) {
      iterator.next();
      output.writeInt(iterator.getKey());
      output.writeString(iterator.getValue());
    }
  }

  private void loadLabsCommonNames(final Input input, final LongMutable position) throws IOException {
    labsIdToCommonName.clear();
    final int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      labsIdToCommonName.put(input.readInt(position), input.readString(position));
    }
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    demographics = new Demographics();
    final int version = input.readInt(position);
    if (version != getCurrentVersion()) {
      throw new IOException("Unsupported version. Current version = " + version + ", supported version = " + getCurrentVersion());
    }
    position.set(demographics.load(input, position.get()));
    position.set(icd9.load(input, position.get()));
    position.set(icd10.load(input, position.get()));
    position.set(cpt.load(input, position.get()));
    position.set(snomed.load(input, position.get()));
    position.set(department.load(input, position.get()));
    position.set(atc.load(input, position.get()));
    position.set(vitals.load(input, position.get()));
    position.set(labs.load(input, position.get()));
    position.set(visitTypes.load(input, position.get()));
    position.set(docDescription.load(input, position.get()));
    position.set(labValues.load(input, position.get()));
    position.set(drugRoute.load(input, position.get()));
    position.set(drugStatus.load(input, position.get()));
    PersistentIntArrayList list = new PersistentIntArrayList();
    position.set(list.load(input, position.get()));
    icd9HierarchyParentIds.addAll(list.toArray());
    list = new PersistentIntArrayList();
    position.set(list.load(input, position.get()));
    icd10HierarchyParentIds.addAll(list.toArray());
    loadDoubleIndices(input, position);
    loadLabsCommonNames(input, position);
    loadTidStringMap(input, position);
    loadSnomedTextMap(input, position);
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    demographics = new Demographics(demographicsBuilder.buildGender(), demographicsBuilder.buildRace(), demographicsBuilder.buildEthnicity(), demographicsBuilder.buildDeathTime());
    output.writeInt(getCurrentVersion());
    demographics.save(output);
    icd9.save(output);
    icd10.save(output);
    cpt.save(output);
    snomed.save(output);
    department.save(output);
    atc.save(output);
    vitals.save(output);
    labs.save(output);
    visitTypes.save(output);
    docDescription.save(output);
    labValues.save(output);
    drugRoute.save(output);
    drugStatus.save(output);
    PersistentIntArrayList list = new PersistentIntArrayList();
    list.addAll(icd9HierarchyParentIds.toArray());
    list.save(output);
    list = new PersistentIntArrayList();
    list.addAll(icd10HierarchyParentIds.toArray());
    list.save(output);
    saveDoubleIndices(output);
    saveLabsCommonNames(output);
    saveTidStringMap(output);
    saveSnomedTextMap(output);
  }

  private void saveTidStringMap(final Output output) throws IOException {
    output.writeInt(tidToString.size());
    final IntKeyObjectIterator<String> keys = tidToString.entries();
    while (keys.hasNext()) {
      keys.next();
      output.writeInt(keys.getKey());
      output.writeString(keys.getValue());
    }
  }

  public Set<Object> getUniqueTidStrings() {
    return stringToTid.keySet();
  }

  public void loadTidStringMap(final Input input, final LongMutable position) throws IOException {
    tidToString.clear();
    stringToTid.clear();
    final int size = input.readInt(position);
    for (int x = 0; x < size; x++) {
      final int key = input.readInt(position);
      final String value = input.readString(position);
      tidToString.put(key, value);
      stringToTid.put(value, key);
    }
  }

  @Override
  public boolean isEmpty() {
    return demographics.isEmpty() && icd9.isEmpty();
  }

  @Override
  public int getCurrentVersion() {
    return CURRENT_VERSION;
  }
}