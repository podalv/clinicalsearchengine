package com.podalv.search.index;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.input.Input;
import com.podalv.maps.primitive.map.EmptyIntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.objectdb.PersistentObject;
import com.podalv.output.Output;
import com.podalv.utils.arrays.ComparableIntArray;
import com.podalv.utils.datastructures.LongMutable;

/** Contains a map linking RX=>ROUTE=>STATUS=>patient cnt
 *
 * @author podalv
 *
 */
public class IntArrayKeyIntMap implements PersistentObject {

  private final HashMap<ComparableIntArray, IntKeyIntOpenHashMap> map     = new HashMap<>();
  private int                                                     keySize = -1;

  public void add(final int ... key) {
    if (key.length < 2) {
      throw new UnsupportedOperationException("Key size must be at least 2 int long. For smaller ones use IntKeyIntMap");
    }
    final ComparableIntArray k = new ComparableIntArray(Arrays.copyOf(key, key.length - 1));
    if (keySize == -1) {
      keySize = key.length;
    }
    else {
      if (keySize != key.length) {
        throw new UnsupportedOperationException("Keys of variable lengths are not supported");
      }
    }
    if (map.containsKey(k)) {
      IntKeyIntOpenHashMap m = map.get(k);
      if (m == null) {
        m = new IntKeyIntOpenHashMap();
        map.put(k, m);
      }
      m.put(key[key.length - 1], m.get(key[key.length - 1]) + 1);
    }
    else {
      final IntKeyIntOpenHashMap m = new IntKeyIntOpenHashMap();
      map.put(k, m);
      m.put(key[key.length - 1], 1);
    }
  }

  public IntKeyIntMapIterator get(final int ... key) {
    final ComparableIntArray k = new ComparableIntArray(key);
    final IntKeyIntOpenHashMap m = map.get(k);
    return m == null ? EmptyIntKeyIntMapIterator.getEmpty() : m.entries();
  }

  public int getCount(final int ... key) {
    final ComparableIntArray k = new ComparableIntArray(Arrays.copyOf(key, key.length - 1));
    final IntKeyIntOpenHashMap m = map.get(k);
    return m == null ? 0 : m.get(key[key.length - 1]);
  }

  @Override
  public long load(final Input input, final long startPos) throws IOException {
    final LongMutable position = new LongMutable(startPos);
    final int size = input.readInt(position);
    keySize = input.readInt(position);
    map.clear();
    if (keySize > 0) {
      for (int x = 0; x < size; x++) {
        final int[] array = new int[keySize - 1];
        for (int y = 0; y < keySize - 1; y++) {
          array[y] = input.readInt(position);
        }
        final int mapSize = input.readInt(position);
        final IntKeyIntOpenHashMap m = new IntKeyIntOpenHashMap();
        for (int y = 0; y < mapSize; y++) {
          m.put(input.readInt(position), input.readInt(position));
        }
        map.put(new ComparableIntArray(array), m);
      }
    }
    return position.get();
  }

  @Override
  public void save(final Output output) throws IOException {
    final Iterator<Entry<ComparableIntArray, IntKeyIntOpenHashMap>> iterator = map.entrySet().iterator();
    output.writeInt(map.size());
    output.writeInt(keySize);
    while (iterator.hasNext()) {
      final Entry<ComparableIntArray, IntKeyIntOpenHashMap> entry = iterator.next();
      final ComparableIntArray key = entry.getKey();
      for (int x = 0; x < key.length(); x++) {
        output.writeInt(key.get(x));
      }
      output.writeInt(entry.getValue().size());
      final IntKeyIntMapIterator m = entry.getValue().entries();
      while (m.hasNext()) {
        m.next();
        output.writeInt(m.getKey());
        output.writeInt(m.getValue());
      }
    }
  }

  @Override
  public boolean isEmpty() {
    return map.size() == 0;
  }

  @Override
  public int getCurrentVersion() {
    return 0;
  }

}