package com.podalv.search.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.podalv.maps.primitive.list.IntArrayList;

/** Generates list of start/end/pid sorted by start and sorted by end
 *
 * @author podalv
 *
 */
public class StatisticsAgeGenerator {

  private final ArrayList<int[]> startEndPid = new ArrayList<>();

  public void addRecord(final int pid, final int start, final int end) {
    startEndPid.add(new int[] {pid, start, end});
  }

  public IntArrayList getSortedByStart() {
    final Comparator<int[]> comparator = new Comparator<int[]>() {

      @Override
      public int compare(final int[] o1, final int[] o2) {
        final int result = Integer.compare(o1[1], o2[1]);
        if (result == 0) {
          return Integer.compare(o2[2], o1[2]);
        }
        return result;
      }
    };
    Collections.sort(startEndPid, comparator);
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < startEndPid.size(); x++) {
      result.add(startEndPid.get(x)[0]);
      result.add(startEndPid.get(x)[1]);
      result.add(startEndPid.get(x)[2]);
    }
    result.trimToSize();
    return result;
  }

  public IntArrayList getSortedByEnd() {
    final Comparator<int[]> comparator = new Comparator<int[]>() {

      @Override
      public int compare(final int[] o1, final int[] o2) {
        final int result = Integer.compare(o1[2], o2[2]);
        if (result == 0) {
          return Integer.compare(o2[1], o1[1]);
        }
        return result;
      }
    };
    Collections.sort(startEndPid, comparator);
    final IntArrayList result = new IntArrayList();
    for (int x = 0; x < startEndPid.size(); x++) {
      result.add(startEndPid.get(x)[0]);
      result.add(startEndPid.get(x)[1]);
      result.add(startEndPid.get(x)[2]);
    }
    result.trimToSize();
    return result;
  }

  public int size() {
    return startEndPid.size();
  }

  public void clear() {
    startEndPid.clear();
  }

  public static void main(final String[] args) {
    final StatisticsAgeGenerator gen = new StatisticsAgeGenerator();
    long time = System.currentTimeMillis();
    for (int x = 0; x < 7000000; x++) {
      gen.addRecord(x, x, x);
    }
    System.out.println("Added in " + (time - System.currentTimeMillis()));
    time = System.currentTimeMillis();
    final IntArrayList result = gen.getSortedByStart();
    System.out.println(result.size());
    System.out.println("Sorted in " + (time - System.currentTimeMillis()));
  }

}
