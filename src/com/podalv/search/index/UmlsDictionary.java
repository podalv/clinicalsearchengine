package com.podalv.search.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.podalv.extractor.hierarchies.atc.AtcHierarchy;
import com.podalv.extractor.hierarchies.icd10.Icd10Hierarchy;
import com.podalv.extractor.hierarchies.icd9.Icd9Hierarchy;
import com.podalv.extractor.stride6.AtlasDatabaseDownload;
import com.podalv.extractor.stride6.Common;
import com.podalv.extractor.stride6.RxNormToAtcLeafMap;
import com.podalv.extractor.umls.CodeTextPair;
import com.podalv.extractor.umls.UmlsExtractor;
import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyStringMap;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.utils.file.FileUtils;
import com.podalv.utils.text.TextUtils;

/** Contains all the maps in the UMLS dictionary
 *
 * @author podalv
 *
 */
public class UmlsDictionary {

  private static final Icd9Hierarchy               icd9            = Icd9Hierarchy.create();
  private static final Icd10Hierarchy              icd10           = Icd10Hierarchy.create();
  private final AtcHierarchy                       atc             = AtcHierarchy.create();
  private final RxNormToAtcLeafMap                 rxNormToAtcLeaf = new RxNormToAtcLeafMap();
  private final HashMap<String, String>            cptCodeToText   = new HashMap<>();
  private final IntKeyStringMap                    rxNormToText    = new IntKeyStringMap();
  private final StringKeyObjectMap<IntOpenHashSet> textToRxNorm    = new StringKeyObjectMap<>();
  private static UmlsDictionary                    INSTANCE        = null;

  public void extractCustomCodeDictionary(final File file) {
    if (!file.exists()) {
      System.out.println("There is no code dictionary to extract...");
      return;
    }
    BufferedReader reader = null;
    try {
      reader = new BufferedReader(new FileReader(file));
      String line;
      while ((line = reader.readLine()) != null) {
        try {
          final String[] data = TextUtils.split(line, '\t');
          if (data.length == 3) {
            if (data[0].equalsIgnoreCase(AtlasDatabaseDownload.CODE_TYPE_CPT)) {
              putCpt(data[1], data[2]);
            }
            else if (data[0].equalsIgnoreCase(AtlasDatabaseDownload.CODE_TYPE_ICD9)) {
              putIcd9(data[1], data[2]);
            }
            else if (data[0].equalsIgnoreCase(AtlasDatabaseDownload.CODE_TYPE_RX)) {
              putRx(Integer.parseInt(data[1]), data[2]);
            }
          }
        }
        catch (final Exception e) {
          System.out.println("Error processing code dictionary line '" + line + "'");
          e.printStackTrace();
        }
      }
      reader.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    finally {
      FileUtils.close(reader);
    }
  }

  public void calculateIcd9Hierarchy(final IndexCollection indices) {
    icd9.calculateIds(indices);
  }

  public static void clearCache() {
    INSTANCE = null;
  }

  public synchronized static UmlsDictionary create() {
    if (INSTANCE == null) {
      INSTANCE = new UmlsDictionary();
    }
    return INSTANCE;
  }

  private UmlsDictionary() {
    // Access only via static methods
    final ArrayList<CodeTextPair> cpt = UmlsExtractor.get(UmlsExtractor.CPT_ONTOLOGY);
    for (final CodeTextPair pair : cpt) {
      cptCodeToText.put(Common.filterString(pair.getCode().toUpperCase()), Common.filterString(pair.getText()));
    }
    final ArrayList<CodeTextPair> rxNorm = UmlsExtractor.get(UmlsExtractor.RXNORM_ONTOLOGY);
    for (final CodeTextPair pair : rxNorm) {
      rxNormToText.put(Integer.parseInt(pair.getCode()), Common.filterString(pair.getText()));
      IntOpenHashSet s = textToRxNorm.get(Common.filterString(pair.getText()));
      if (s == null) {
        s = new IntOpenHashSet();
        textToRxNorm.put(Common.filterString(pair.getText()), s);
      }
      s.add(Integer.parseInt(pair.getCode()));
    }

  }

  public void clearRxNorm() {
    textToRxNorm.clear();
    rxNormToText.clear();
  }

  public String[] getAtcHierarchy(final String atc) {
    return this.atc.getHierarchy(atc);
  }

  public String[] getIcd9Parents(final String icd9) {
    return UmlsDictionary.icd9.getHierarchy(icd9);
  }

  public int[] getIcd9Parents(final int icd9, final IndexCollection indices) {
    return UmlsDictionary.icd9.getHierarchy(icd9, indices);
  }

  public String[] getIcd10Parents(final String icd10) {
    return UmlsDictionary.icd10.getHierarchy(icd10);
  }

  public int[] getIcd10Parents(final int icd10, final IndexCollection indices) {
    return UmlsDictionary.icd10.getHierarchy(icd10, indices);
  }

  public HashSet<String> getIcd9TopLevelParents() {
    return UmlsDictionary.icd9.getTopLevelParents();
  }

  public HashSet<String> getIcd10TopLevelParents() {
    return UmlsDictionary.icd10.getTopLevelParents();
  }

  public HashSet<String> getIcd9HierarchyParents() {
    return UmlsDictionary.icd9.getParents();
  }

  public HashSet<String> getIcd10HierarchyParents() {
    return UmlsDictionary.icd10.getParents();
  }

  public HashSet<String> getAtcHierarchy(final int rxNorm) {
    final HashSet<String> leaves = rxNormToAtcLeaf.getAtcLeaves(rxNorm);
    final HashSet<String> result = new HashSet<>();
    final Iterator<String> i = leaves.iterator();
    while (i.hasNext()) {
      final String leaf = i.next();
      final String[] hierarchy = atc.getHierarchy(leaf);
      if (hierarchy != null) {
        for (int x = 0; x < hierarchy.length; x++) {
          result.add(hierarchy[x]);
        }
      }
      result.add(leaf);
    }
    return result;
  }

  public IntOpenHashSet getAtcHierarchy(final IndexCollection indices, final int rxNorm) {
    final IntOpenHashSet leaves = rxNormToAtcLeaf.getAtcLeaves(indices, rxNorm);
    final IntOpenHashSet result = new IntOpenHashSet();
    final IntIterator i = leaves.iterator();
    while (i.hasNext()) {
      final int leaf = i.next();
      final IntIterator hierarchy = atc.getHierarchy(indices, leaf).iterator();
      if (hierarchy != null) {
        while (hierarchy.hasNext()) {
          result.add(hierarchy.next());
        }
      }
      result.add(leaf);
    }
    return result;
  }

  public Iterator<String> getIcd9Codes() {
    return icd9.getCodes();
  }

  public Iterator<String> getAtcCodes() {
    return atc.getCodes();
  }

  public void putCpt(final String cptCode, final String cptString) {
    cptCodeToText.put(cptCode, Common.filterString(cptString));
  }

  public void putIcd9(final String icd9Code, final String icd9String) {
    icd9.set(icd9Code, Common.filterString(icd9String));
  }

  public void putRx(final int rxNorm, String text) {
    text = Common.filterString(text);
    rxNormToText.put(rxNorm, text);
    IntOpenHashSet set = textToRxNorm.get(text);
    if (set == null) {
      set = new IntOpenHashSet();
      textToRxNorm.put(text, set);
    }
    set.add(rxNorm);
  }

  public Set<String> getCptCodes() {
    return cptCodeToText.keySet();
  }

  public StringKeyObjectMap<IntOpenHashSet> getRxNorms() {
    return textToRxNorm;
  }

  public IntOpenHashSet getCodeFromRxNormText(final String text) {
    return textToRxNorm.get(text);
  }

  public String getRxNormText(final int rxNorm) {
    return rxNormToText.get(rxNorm);
  }

  public String getIcd9Text(final String icd9Code) {
    if (icd9Code.endsWith("?")) {
      return icd9Code;
    }
    return icd9.getText(icd9Code);
  }

  public String getIcd10Text(final String icd10Code) {
    if (icd10Code.endsWith("?")) {
      return icd10Code;
    }
    return icd10.getText(icd10Code);
  }

  public String getCptText(final String cptCode) {
    return cptCodeToText.get(cptCode);
  }

  public String getAtcText(final String atcCode) {
    return atc.getText(atcCode);
  }

}