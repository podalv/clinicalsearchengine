package com.podalv.search.autosuggest;

import static com.podalv.utils.Logging.debug;
import static com.podalv.utils.Logging.error;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

import com.podalv.maps.primitive.IntIterator;
import com.podalv.maps.primitive.map.IntKeyIntMapIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.maps.string.StringArrayList;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.search.datastructures.AutosuggestResponse;
import com.podalv.search.datastructures.AutosuggestTextMatcher;
import com.podalv.search.datastructures.AutosuggestTextRequest;
import com.podalv.search.datastructures.AutosuggestTextResponse;
import com.podalv.search.datastructures.StringTidAutosuggestTextMatcher;
import com.podalv.search.datastructures.index.StringEnumIndex;
import com.podalv.search.index.IndexCollection;
import com.podalv.search.index.Statistics;
import com.podalv.search.index.UmlsDictionary;
import com.podalv.search.language.script.ScriptParser;
import com.podalv.utils.datastructures.SortPair;
import com.podalv.utils.file.FileUtils;

/** Controls the autosuggest behavior. Given the AutosuggestRequest, produces the correct AutosuggestResponse
 *
 * @author podalv
 *
 */
public class AutosuggestSearch {

  public static final String                            UNKNOWN_NAME               = "UNKNOWN NAME";
  public static final String                            VALUE_SEPARATOR            = "=";
  public static final char                              QUOTE_CHAR                 = '"';
  public static final String                            QUOTE                      = "\"";
  public static final int                               MAX_LIST_SIZE              = 10;
  public static final char                              ANY_TEXT                   = '*';
  public static final char                              OPTIONAL_CHAR              = '?';
  static final int                                      ICD9_ID                    = 0;
  private static final int                              CPT_ID                     = 1;
  static final int                                      RX_ID                      = 2;
  private static final int                              ATC_ID                     = 4;
  private static final int                              VITALS_ID                  = 5;
  public static final int                               LABS_CODE_ID               = 6;
  public static final int                               LABS_VALUE_ID              = 7;
  private static final int                              VISIT_TYPE_ID              = 8;
  static final int                                      SCRIPT_ID                  = 9;
  private static final int                              NOTE_DESCRIPTION_ID        = 10;
  private static final int                              GENDER_ID                  = 11;
  private static final int                              RACE_ID                    = 12;
  private static final int                              ETHNICITY_ID               = 13;
  private static final int                              DRUG_ROUTE_ID              = 14;
  private static final int                              DRUG_STATUS_ID             = 15;
  private static final int                              DRUG_ROUTE_STATUS_ID       = 16;
  private static final int                              DRUG_STATUS_ROUTE_ID       = 17;
  private static final int                              TID_ID                     = 18;
  private static final int                              TID_NEGATED_ID             = 19;
  private static final int                              TID_FH_ID                  = 20;
  private static final int                              SNOMED_ID                  = 21;
  static final int                                      ICD10_ID                   = 22;
  static final int                                      DEPARTMENT_ID              = 23;
  private static final String[]                         ICD9_TOKEN                 = new String[] {"icd9", VALUE_SEPARATOR};
  private static final String[]                         ICD10_TOKEN                = new String[] {"icd10", VALUE_SEPARATOR};
  private static final String[]                         DEPARTMENT_TOKEN           = new String[] {"department", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         DEPT_TOKEN                 = new String[] {"dept", VALUE_SEPARATOR, QUOTE};
  private static final String[]                         CPT_TOKEN                  = new String[] {"cpt", VALUE_SEPARATOR};
  private static final String[]                         RX_TOKEN                   = new String[] {"rx", VALUE_SEPARATOR};
  private static final String[]                         SNOMED_TOKEN               = new String[] {"snomed", VALUE_SEPARATOR};
  private static final String[]                         DRUGS_ROUTE_TOKEN          = new String[] {"drug", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",", "route",
      VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         MEDS_ROUTE_TOKEN           = new String[] {"med", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",", "route",
      VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         DRUGS_STATUS_TOKEN         = new String[] {"drug", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",",
      "status", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         MEDS_STATUS_TOKEN          = new String[] {"med", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",", "status",
      VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         DRUGS_ROUTE_STATUS_TOKEN   = new String[] {"drug", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",", "route",
      VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE, ANY_TEXT + "", ",", "status", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         MEDS_ROUTE_STATUS_TOKEN    = new String[] {"med", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",", "route",
      VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE, ANY_TEXT + "", ",", "status", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         DRUGS_STATUS_ROUTE_TOKEN   = new String[] {"drug", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",",
      "status", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE, ANY_TEXT + "", ",", "route", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         MEDS_STATUS_ROUTE_TOKEN    = new String[] {"med", OPTIONAL_CHAR + "s", "(", "rx", VALUE_SEPARATOR, ANY_TEXT + ",", "status",
      VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE, ANY_TEXT + "", ",", "route", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         TID_TOKEN                  = new String[] {"text", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         TID_NEGATED_TOKEN          = new String[] {"!text", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         TID_FH_TOKEN               = new String[] {"~text", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         ATC_TOKEN                  = new String[] {"atc", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         VITALS_TOKEN               = new String[] {"vitals", "(", OPTIONAL_CHAR + QUOTE};
  private static final String[]                         LABS_TOKEN                 = new String[] {"lab", OPTIONAL_CHAR + "s", "(", OPTIONAL_CHAR + QUOTE};
  private static final String[]                         LABS_VALUE_TOKEN           = new String[] {"lab", OPTIONAL_CHAR + "s", "(", OPTIONAL_CHAR + QUOTE, ANY_TEXT + "", ",",
      OPTIONAL_CHAR + QUOTE};
  private static final String[]                         VISIT_TYPE_TOKEN           = new String[] {"visit", "type", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         SCRIPT_TOKEN               = new String[] {"$"};
  private static final String[]                         NOTE_DESCRIPTION_TOKEN     = new String[] {"note", "type", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         GENDER_TOKEN               = new String[] {"gender", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         RACE_TOKEN                 = new String[] {"race", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private static final String[]                         ETHNICITY_TOKEN            = new String[] {"ethnicity", VALUE_SEPARATOR, OPTIONAL_CHAR + QUOTE};
  private AutosuggestTextMatcher                        icd9Autosuggest            = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        icd10Autosuggest           = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        cptAutosuggest             = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        rxAutosuggest              = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        snomedAutosuggest          = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        vitalsAutosuggest          = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        labsAutosuggest            = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        drugRouteAutosuggest       = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        drugStatusAutosuggest      = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        departmentAutosuggest      = new AutosuggestTextMatcher();
  private StringTidAutosuggestTextMatcher               tidStringsAutosuggest      = null;
  private AutosuggestTextMatcher                        labValuesAutosuggest       = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        genderAutosuggest          = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        raceAutosuggest            = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        ethnicityAutosuggest       = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        atcAutosuggest             = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        visitTypeAutosuggest       = new AutosuggestTextMatcher();
  private AutosuggestTextMatcher                        scriptAutosuggest          = null;
  private AutosuggestTextMatcher                        noteDescriptionAutosuggest = new AutosuggestTextMatcher();
  private final UmlsDictionary                          umls;
  private final Statistics                              statistics;
  private final IndexCollection                         indices;
  private final IntKeyObjectMap<AutosuggestTextMatcher> tokenIdToTextMatcher       = new IntKeyObjectMap<>();
  private final IntKeyObjectMap<AutosuggestResponse>    idToResponseCached         = new IntKeyObjectMap<>();

  public AutosuggestSearch(final UmlsDictionary umls, final Statistics statistics, final IndexCollection indices) {
    debug("Generating autosuggest...");
    this.umls = umls;
    this.statistics = statistics;
    this.indices = indices;
    init();
    debug("Done with autosuggest...");
  }

  public AutosuggestSearch(final File file, final long version, final UmlsDictionary umls, final Statistics statistics, final IndexCollection indices) throws IOException {
    debug("Loading autosuggest...");
    this.umls = umls;
    this.statistics = statistics;
    this.indices = indices;
    DataInputStream stream = null;
    try {
      stream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
      final long ver = stream.readLong();
      if (version != ver) {
        throw new UnsupportedClassVersionError();
      }
      icd9Autosuggest = AutosuggestTextMatcher.load(stream);
      icd10Autosuggest = AutosuggestTextMatcher.load(stream);
      cptAutosuggest = AutosuggestTextMatcher.load(stream);
      rxAutosuggest = AutosuggestTextMatcher.load(stream);
      snomedAutosuggest = AutosuggestTextMatcher.load(stream);
      vitalsAutosuggest = AutosuggestTextMatcher.load(stream);
      labsAutosuggest = AutosuggestTextMatcher.load(stream);
      drugRouteAutosuggest = AutosuggestTextMatcher.load(stream);
      drugStatusAutosuggest = AutosuggestTextMatcher.load(stream);
      departmentAutosuggest = AutosuggestTextMatcher.load(stream);
      labValuesAutosuggest = AutosuggestTextMatcher.load(stream);
      genderAutosuggest = AutosuggestTextMatcher.load(stream);
      raceAutosuggest = AutosuggestTextMatcher.load(stream);
      ethnicityAutosuggest = AutosuggestTextMatcher.load(stream);
      atcAutosuggest = AutosuggestTextMatcher.load(stream);
      visitTypeAutosuggest = AutosuggestTextMatcher.load(stream);
      noteDescriptionAutosuggest = AutosuggestTextMatcher.load(stream);
      buildScriptAutosuggest();
      tidStringsAutosuggest = StringTidAutosuggestTextMatcher.load(stream, indices);
      generateTokenIdToMatcherMap();
    }
    finally {
      FileUtils.close(stream);
    }
  }

  private void init() {
    buildGenericMatcher(labValuesAutosuggest, indices.getLabValues());
    buildGenericMatcher(drugStatusAutosuggest, indices.getDrugStatus());
    buildGenericMatcher(drugRouteAutosuggest, indices.getDrugRoute());
    buildGenericMatcher(noteDescriptionAutosuggest, indices.getNoteDescriptionNames());
    buildGenericMatcher(vitalsAutosuggest, indices.getVitalsNames());
    buildCptAutosuggest();
    buildIcd9Autosuggest();
    buildIcd10Autosuggest();
    buildRxAutosuggest();
    buildDepartmentAutosuggest();
    buildLabsAutosuggest();
    buildAtcAutosuggest();
    buildVisitTypeAutosuggest();
    buildScriptAutosuggest();
    buildGenderAutosuggest();
    buildRaceAutosuggest();
    buildEthnicityAutosuggest();
    buildTidStringsAutosuggest();
    buildSnomedAutosuggest();
    generateTokenIdToMatcherMap();
  }

  public void saveToDisk(final long version, final File file) throws IOException {
    debug("Saving autosuggest to disk...");
    final DataOutputStream stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
    stream.writeLong(version);
    icd9Autosuggest.save(stream);
    icd10Autosuggest.save(stream);
    cptAutosuggest.save(stream);
    rxAutosuggest.save(stream);
    snomedAutosuggest.save(stream);
    vitalsAutosuggest.save(stream);
    labsAutosuggest.save(stream);
    drugRouteAutosuggest.save(stream);
    drugStatusAutosuggest.save(stream);
    departmentAutosuggest.save(stream);
    labValuesAutosuggest.save(stream);
    genderAutosuggest.save(stream);
    raceAutosuggest.save(stream);
    ethnicityAutosuggest.save(stream);
    atcAutosuggest.save(stream);
    visitTypeAutosuggest.save(stream);
    noteDescriptionAutosuggest.save(stream);
    tidStringsAutosuggest.save(stream);
    stream.close();
    debug("Done...");
  }

  private void buildTidStringsAutosuggest() {
    tidStringsAutosuggest = new StringTidAutosuggestTextMatcher(indices);
    final Iterator<Object> strings = indices.getTidStrings();
    if (strings != null) {
      while (strings.hasNext()) {
        final String str = (String) strings.next();
        final int tid = indices.getTid(str);
        if (statistics.getTid(tid) > 0 || statistics.getTidFamilyHistory(tid) > 0 || statistics.getTidNegated(tid) > 0) {
          tidStringsAutosuggest.add(str, tid);
        }
      }
    }
    tidStringsAutosuggest.build();
  }

  private void buildGenericMatcher(final AutosuggestTextMatcher matcher, final String[] values) {
    if (values != null && values.length > 100000) {
      error("!!! ERROR. Too many values for autosuggest.");
    }
    else {
      if (values != null) {
        for (final String value : values) {
          matcher.addWholeString(value, value);
          matcher.add(value, value);
        }
      }
      matcher.build();
    }
  }

  private void buildEthnicityAutosuggest() {
    final String[] ethnicityNames = indices.getRaces();
    if (ethnicityNames != null) {
      for (final String ethnicityName : ethnicityNames) {
        final int patientCnt = indices.getDemographics().getEthnicity().getCount(ethnicityName);
        if (indices.getDemographics().getEthnicity().getId(ethnicityName) != StringEnumIndex.NULL_ID && patientCnt > 0) {
          ethnicityAutosuggest.addWholeString(ethnicityName, ethnicityName);
          ethnicityAutosuggest.add(ethnicityName, ethnicityName);
        }
      }
    }
    ethnicityAutosuggest.build();
  }

  private void buildRaceAutosuggest() {
    final String[] raceNames = indices.getRaces();
    if (raceNames != null) {
      for (final String raceName : raceNames) {
        final int patientCnt = indices.getDemographics().getRace().getCount(raceName);
        if (indices.getDemographics().getRace().getId(raceName) != StringEnumIndex.NULL_ID && patientCnt > 0) {
          raceAutosuggest.addWholeString(raceName, raceName);
          raceAutosuggest.add(raceName, raceName);
        }
      }
    }
    raceAutosuggest.build();
  }

  private void buildGenderAutosuggest() {
    final String[] genderNames = indices.getGenders();
    if (genderNames != null) {
      for (final String genderName : genderNames) {
        if (indices.getDemographics().getGender().getId(genderName) != StringEnumIndex.NULL_ID) {
          final int patientCnt = indices.getDemographics().getGender().getCount(genderName);
          if (patientCnt > 0) {
            genderAutosuggest.addWholeString(genderName, genderName);
            genderAutosuggest.add(genderName, genderName);
          }
        }
      }
    }
    genderAutosuggest.build();
  }

  private void buildLabsAutosuggest() {
    final String[] labsNames = indices.getLabsNames();
    if (labsNames != null) {
      for (final String labsName : labsNames) {
        final int patientCnt = statistics.getLabs(indices.getLabsCode(labsName));
        if (patientCnt > 0) {
          String commonName = indices.getLabCommonName(indices.getLabsCode(labsName));
          if (commonName == null) {
            commonName = labsName;
          }
          labsAutosuggest.addWholeString(commonName, labsName);
          labsAutosuggest.add(labsName, labsName);
        }
      }
    }
    labsAutosuggest.build();
  }

  public void buildScriptAutosuggest() {
    if (ScriptParser.getInstance() != null) {
      scriptAutosuggest = new AutosuggestTextMatcher();
      buildGenericMatcher(scriptAutosuggest, ScriptParser.getInstance().getScriptNames());
    }
    tokenIdToTextMatcher.put(SCRIPT_ID, scriptAutosuggest);
  }

  private void buildVisitTypeAutosuggest() {
    final String[] visitTypeNames = indices.getVisitTypeNames();
    if (visitTypeNames != null) {
      for (final String visitTypeName : visitTypeNames) {
        final int patientCnt = statistics.getVisitType(indices.getVisitType(visitTypeName));
        if (patientCnt > 0) {
          visitTypeAutosuggest.add(visitTypeName, visitTypeName);
        }
      }
    }
    visitTypeAutosuggest.build();
  }

  private void buildIcd9Autosuggest() {
    for (int x = 0; x < statistics.getIcd9KeyCnt(); x++) {
      String code = indices.getIcd9(x);
      if (code == null) {
        code = UNKNOWN_NAME;
      }
      final int patientCnt = statistics.getIcd9(x);
      if (patientCnt != 0) {
        if (umls.getIcd9Text(code) != null) {
          icd9Autosuggest.add(umls.getIcd9Text(code), code);
        }
        icd9Autosuggest.addWholeString(code, code);
      }
    }
    icd9Autosuggest.build();
  }

  private void buildIcd10Autosuggest() {
    for (int x = 0; x < statistics.getIcd10KeyCnt(); x++) {
      String code = indices.getIcd10(x);
      if (code == null) {
        code = UNKNOWN_NAME;
      }
      final int patientCnt = statistics.getIcd10(x);
      if (patientCnt != 0) {
        if (umls.getIcd10Text(code) != null) {
          icd10Autosuggest.add(umls.getIcd10Text(code), code);
        }
        icd10Autosuggest.addWholeString(code, code);
      }
    }
    icd10Autosuggest.build();
  }

  private void buildSnomedAutosuggest() {
    final String[] codes = indices.getSnomedKeys();
    if (codes != null) {
      for (final String code : codes) {
        final int id = indices.getSnomed(code);
        final int patientCnt = statistics.getSnomed(id);
        if (patientCnt != 0) {
          snomedAutosuggest.add(code, code);
          snomedAutosuggest.addWholeString(indices.getSnomedText(id) == null ? code : indices.getSnomedText(id), code);
        }
      }
    }
    snomedAutosuggest.build();
  }

  private void buildAtcAutosuggest() {
    final Iterator<String> codes = umls.getAtcCodes();
    while (codes.hasNext()) {
      final String code = codes.next();
      final int patientCnt = statistics.getAtc(indices.getAtcCode(code));
      if (patientCnt != 0) {
        atcAutosuggest.add(code, code);
        atcAutosuggest.addWholeString(umls.getAtcText(code), code);
      }
    }
    atcAutosuggest.build();
  }

  private void buildRxAutosuggest() {
    for (int x = 0; x < statistics.getRxNormKeyCnt(); x++) {
      final int patientCnt = statistics.getRxNorm(x);
      if (patientCnt != 0) {
        String text = umls.getRxNormText(x);
        if (text == null) {
          text = UNKNOWN_NAME;
        }
        rxAutosuggest.add(String.valueOf(x), String.valueOf(x));
        rxAutosuggest.addWholeString(text, String.valueOf(x));
      }
    }
    rxAutosuggest.build();
  }

  private void buildDepartmentAutosuggest() {
    for (int x = 0; x < statistics.getDepartmentKeyCnt(); x++) {
      final int patientCnt = statistics.getDepartment(x);
      if (patientCnt != 0) {
        String text = indices.getDepartment(x);
        if (text == null) {
          text = UNKNOWN_NAME;
        }
        departmentAutosuggest.add(String.valueOf(x), String.valueOf(x));
        departmentAutosuggest.addWholeString(text, String.valueOf(x));
      }
    }
    departmentAutosuggest.build();
  }

  private void buildCptAutosuggest() {
    for (int x = 0; x < statistics.getCptKeyCnt(); x++) {
      String code = indices.getCpt(x);
      if (code == null) {
        code = UNKNOWN_NAME;
      }
      final int patientCnt = statistics.getCpt(x);
      if (patientCnt != 0) {
        if (umls.getCptText(code) != null) {
          cptAutosuggest.add(umls.getCptText(code), code);
        }
        cptAutosuggest.addWholeString(code, code);
      }
    }
    cptAutosuggest.build();
  }

  private HashSet<String> find(final AutosuggestTextMatcher matcher, HashSet<String> result, final String currentWord) {
    if (result == null) {
      result = matcher.find(currentWord);
    }
    else if (result.size() != 0) {
      final HashSet<String> found = matcher.find(currentWord);
      final Iterator<String> i = result.iterator();
      final HashSet<String> out = new HashSet<>();
      while (i.hasNext()) {
        final String word = i.next();
        if (found.contains(word)) {
          out.add(word);
        }
      }
      result = out;
    }
    return result;
  }

  private HashSet<String> search(final String query, final int firstPos, final int lastPos, final AutosuggestTextMatcher matcher) {
    HashSet<String> prevCodes = null;
    final StringBuilder currentWord = new StringBuilder();
    for (int x = Math.min(lastPos - 1, query.length() - 1); x >= firstPos; x--) {
      if (!(AutosuggestSearchCommon.isWhiteSpace(query.charAt(x)) || x == firstPos - 1)) {
        currentWord.insert(0, query.charAt(x));
      }
      if (AutosuggestSearchCommon.isWhiteSpace(query.charAt(x)) || x == firstPos) {
        if (!currentWord.toString().isEmpty()) {
          final HashSet<String> newCodes = find(matcher, prevCodes, currentWord.toString());
          prevCodes = newCodes;
          if (newCodes.size() == 0) {
            break;
          }
          currentWord.setLength(0);
        }
      }
    }
    return prevCodes == null ? new HashSet<>() : prevCodes;
  }

  public AutosuggestTextResponse replace(final AutosuggestTextRequest request) {
    final String lowerCasedText = request.getText().toLowerCase();
    final SortPair<Integer, Integer> startPos = AutosuggestSearchCommon.getClosest(generatePositionTokenList(lowerCasedText, request.getCursor()));
    String newText = request.getText();
    String sub = "";
    if (startPos.getComparable().intValue() >= 0) {
      String spaces = "";
      for (int x = startPos.getComparable().intValue(); x < request.getText().length(); x++) {
        if (request.getText().charAt(x) != ' ') {
          spaces = request.getText().substring(startPos.getComparable().intValue(), x);
          break;
        }
      }
      newText = request.getText().substring(0, startPos.getComparable().intValue()) + spaces + request.getSelectedCode().trim();
      if (startPos.getObject() == ATC_ID || //
          startPos.getObject() == VITALS_ID || //
          startPos.getObject() == LABS_CODE_ID || //
          startPos.getObject() == VISIT_TYPE_ID || //
          startPos.getObject() == GENDER_ID || //
          startPos.getObject() == RACE_ID || //
          startPos.getObject() == ETHNICITY_ID || //
          startPos.getObject() == NOTE_DESCRIPTION_ID || //
          startPos.getObject() == DRUG_ROUTE_ID || //
          startPos.getObject() == TID_ID || //
          startPos.getObject() == DEPARTMENT_ID || //
          startPos.getObject() == TID_NEGATED_ID || //
          startPos.getObject() == TID_FH_ID || //
          startPos.getObject() == DRUG_STATUS_ID) {
        String firstPart = request.getText().substring(0, startPos.getComparable().intValue());
        if (!firstPart.endsWith(QUOTE)) {
          firstPart += QUOTE;
        }
        firstPart += request.getSelectedCode().trim();
        newText = firstPart + QUOTE;
      }
      if (startPos.getObject() == LABS_VALUE_ID || //
          startPos.getObject() == DRUG_ROUTE_STATUS_ID || //
          startPos.getObject() == DRUG_STATUS_ROUTE_ID) {
        String firstPart = request.getText().substring(0, startPos.getComparable().intValue());
        if (!firstPart.endsWith(QUOTE)) {
          firstPart += QUOTE;
        }
        firstPart += request.getSelectedCode().trim();
        newText = firstPart + QUOTE;
        newText += ")";
      }
      sub = request.getText().substring(request.getCursor());
      if (newText.endsWith(QUOTE) && sub.startsWith(QUOTE)) {
        sub = sub.substring(1);
      }
      if (newText.endsWith(")") && sub.startsWith(")")) {
        sub = sub.substring(1);
      }
      if (newText.endsWith(QUOTE + ")") && sub.startsWith(QUOTE + ")")) {
        sub = sub.substring(2);
      }
    }
    final AutosuggestTextResponse response = new AutosuggestTextResponse();
    response.setCursor(newText.length());
    response.setText(newText + sub);
    return response;
  }

  public AutosuggestResponse search(final AutosuggestRequest request) {
    final String req = request.getQuery();
    final ArrayList<SortPair<Integer, Integer>> positionTokenList = generatePositionTokenList(req, request.getPosition());

    Integer codeSearchPerformed = null;
    HashSet<String> result = new HashSet<>();
    int remaining = 0;
    int startPos = -1;
    boolean storeToCache = false;
    final StringArrayList out = new StringArrayList();
    if (AutosuggestSearchCommon.searchSucceeded(positionTokenList)) {
      final SortPair<Integer, Integer> closestToken = AutosuggestSearchCommon.getClosest(positionTokenList);
      if (closestToken != null) {
        codeSearchPerformed = closestToken.getObject();
        startPos = closestToken.getComparable();
        if (startPos > request.getPosition()) {
          return AutosuggestResponse.EMPTY;
        }
        if (AutosuggestSearchCommon.isCaretInDifferentLine(request, startPos)) {
          return AutosuggestResponse.EMPTY;
        }
        if (AutosuggestSearchCommon.isCaretAfterCode(request, startPos, closestToken)) {
          return AutosuggestResponse.EMPTY;
        }
        if (AutosuggestSearchCommon.isScriptDefinedInCurrentContext(request, closestToken)) {
          return AutosuggestResponse.EMPTY;
        }
        result = search(req, startPos, request.getPosition(), tokenIdToTextMatcher.get(closestToken.getObject()));
      }

      final String actualData = request.getQuery().substring(startPos, request.getPosition()).trim();

      if (codeSearchPerformed != null) {

        if (result.size() == 1) {
          if (req.length() >= startPos + result.iterator().next().length() && result.iterator().next().equalsIgnoreCase(req.substring(startPos, startPos
              + result.iterator().next().length()))) {
            result.clear();
          }
        }
        final Iterator<String> i = result.iterator();
        int labCode = -1;
        int rxCode = -1;
        int routeId = -1;
        int statusId = -1;
        if (codeSearchPerformed == LABS_VALUE_ID) {
          labCode = indices.getLabsCode(AutosuggestSearchCommon.extractLabCode(req, req.lastIndexOf('(', startPos)));
        }
        if (codeSearchPerformed == DRUG_ROUTE_ID || codeSearchPerformed == DRUG_STATUS_ID) {
          rxCode = AutosuggestSearchCommon.extractRxNorm(req, startPos);
        }
        if (codeSearchPerformed == DRUG_ROUTE_STATUS_ID) {
          final Object[] previousCodes = AutosuggestSearchCommon.extractRxCodeBeforeString(req, startPos);
          rxCode = ((Integer) previousCodes[0]).intValue();
          routeId = indices.getDrugRouteId((String) previousCodes[1]);
        }
        if (codeSearchPerformed == DRUG_STATUS_ROUTE_ID) {
          final Object[] previousCodes = AutosuggestSearchCommon.extractRxCodeBeforeString(req, startPos);
          rxCode = ((Integer) previousCodes[0]).intValue();
          statusId = indices.getDrugStatusId((String) previousCodes[1]);
        }
        if (i.hasNext()) {
          final ArrayList<SortPair<Integer, String>> sortableList = new ArrayList<>();
          while (i.hasNext()) {
            final String code = i.next();
            int cnt = 0;
            if (codeSearchPerformed == TID_ID) {
              cnt = statistics.getTid(indices.getTid(code), request.getPatientIds());
            }
            if (codeSearchPerformed == DEPARTMENT_ID) {
              String text = indices.getDepartment(Integer.parseInt(code));
              if (text == null) {
                text = UNKNOWN_NAME;
              }
              sortableList.add(new SortPair<>(statistics.getDepartment(Integer.parseInt(code), request.getPatientIds()), text));
            }
            else if (codeSearchPerformed == TID_NEGATED_ID) {
              cnt = statistics.getTidNegated(indices.getTid(code), request.getPatientIds());
            }
            else if (codeSearchPerformed == TID_FH_ID) {
              cnt = statistics.getTidFamilyHistory(indices.getTid(code), request.getPatientIds());
            }
            else if (codeSearchPerformed == DRUG_STATUS_ROUTE_ID) {
              cnt = statistics.getRxStatusRoutePatientCnt(rxCode, statusId, indices.getDrugRouteId(code));
            }
            else if (codeSearchPerformed == DRUG_ROUTE_STATUS_ID) {
              cnt = statistics.getRxRouteStatusPatientCnt(rxCode, routeId, indices.getDrugStatusId(code));
            }
            else if (codeSearchPerformed == DRUG_ROUTE_ID) {
              cnt = statistics.getRxRoutePatientCnt(rxCode, indices.getDrugRouteId(code));
            }
            else if (codeSearchPerformed == DRUG_STATUS_ID) {
              cnt = statistics.getRxStatusPatientCnt(rxCode, indices.getDrugStatusId(code));
            }
            else if (codeSearchPerformed == ICD9_ID) {
              String icd9Code = umls.getIcd9Text(code);
              if (icd9Code == null) {
                icd9Code = UNKNOWN_NAME;
              }
              sortableList.add(new SortPair<>(statistics.getIcd9(indices.getIcd9(code), request.getPatientIds()), code + VALUE_SEPARATOR + icd9Code));
            }
            else if (codeSearchPerformed == ICD10_ID) {
              String icd10Code = umls.getIcd10Text(code);
              if (icd10Code == null) {
                icd10Code = UNKNOWN_NAME;
              }
              sortableList.add(new SortPair<>(statistics.getIcd10(indices.getIcd10(code), request.getPatientIds()), code + VALUE_SEPARATOR + icd10Code));
            }
            else if (codeSearchPerformed == CPT_ID) {
              String cptCode = umls.getCptText(code);
              if (cptCode == null) {
                cptCode = UNKNOWN_NAME;
              }
              sortableList.add(new SortPair<>(statistics.getCpt(indices.getCpt(code), request.getPatientIds()), code + VALUE_SEPARATOR + cptCode));
            }
            else if (codeSearchPerformed == RX_ID) {
              String text = umls.getRxNormText(Integer.parseInt(code));
              if (text == null) {
                text = UNKNOWN_NAME;
              }
              sortableList.add(new SortPair<>(statistics.getRxNorm(Integer.parseInt(code), request.getPatientIds()), code + VALUE_SEPARATOR + text));
            }
            else if (codeSearchPerformed == SNOMED_ID) {
              sortableList.add(new SortPair<>(statistics.getSnomed(indices.getSnomed(code), request.getPatientIds()), code + VALUE_SEPARATOR + indices.getSnomedText(code)));
            }
            else if (codeSearchPerformed == VITALS_ID) {
              sortableList.add(new SortPair<>(statistics.getVitals(indices.getVitalsCode(code), request.getPatientIds()), indices.getVitalsName(indices.getVitalsCode(code))));
            }
            else if (codeSearchPerformed == LABS_VALUE_ID) {
              cnt = statistics.getLabValuesToPatientCounts(labCode, indices.getLabValueId(code));
            }
            else if (codeSearchPerformed == LABS_CODE_ID) {
              String labsCommonName = indices.getLabCommonName(indices.getLabsCode(code));
              labsCommonName = labsCommonName == null ? "" : VALUE_SEPARATOR + labsCommonName;
              sortableList.add(new SortPair<>(statistics.getLabs(indices.getLabsCode(code), request.getPatientIds()), code + labsCommonName));
            }
            else if (codeSearchPerformed == ATC_ID) {
              sortableList.add(new SortPair<>(statistics.getAtc(indices.getAtcCode(code), request.getPatientIds()), code + VALUE_SEPARATOR + indices.getUmls().getAtcText(code)));
            }
            else if (codeSearchPerformed == VISIT_TYPE_ID) {
              sortableList.add(new SortPair<>(statistics.getVisitType(indices.getVisitType(code), request.getPatientIds()), code));
            }
            else if (codeSearchPerformed == SCRIPT_ID) {
              sortableList.add(new SortPair<>(0, code));
            }
            else if (codeSearchPerformed == NOTE_DESCRIPTION_ID) {
              sortableList.add(new SortPair<>(statistics.getNoteType(indices.getNoteTypeId(code), request.getPatientIds()), code));
            }
            else if (codeSearchPerformed == GENDER_ID) {
              if (indices.getDemographics().getGender().getCount(code) != 0) {
                sortableList.add(new SortPair<>(indices.getDemographics().getGender().getCount(code, request.getPatientIds()), code.toUpperCase()));
              }
            }
            else if (codeSearchPerformed == RACE_ID) {
              if (indices.getDemographics().getRace().getCount(code) != 0) {
                sortableList.add(new SortPair<>(indices.getDemographics().getRace().getCount(code, request.getPatientIds()), code.toUpperCase()));
              }
            }
            else if (codeSearchPerformed == ETHNICITY_ID) {
              if (indices.getDemographics().getEthnicity().getCount(code) != 0) {
                sortableList.add(new SortPair<>(indices.getDemographics().getEthnicity().getCount(code, request.getPatientIds()), code.toUpperCase()));
              }
            }
            if (cnt != 0) {
              sortableList.add(new SortPair<>(cnt, code));
            }
          }
          Collections.sort(sortableList);
          final int minSize = Math.max(sortableList.size() - MAX_LIST_SIZE, 0);
          for (int x = sortableList.size() - 1; x >= minSize; x--) {
            out.add(sortableList.get(x).getObject() + (sortableList.get(x).getComparable().intValue() == 0 ? "" : " [" + sortableList.get(x).getComparable() + "]"));
          }
          if (sortableList.size() > MAX_LIST_SIZE) {
            remaining = sortableList.size() - MAX_LIST_SIZE;
          }
        }
        else if (actualData.length() == 0 || actualData.equalsIgnoreCase("\"")) {
          if (idToResponseCached.containsKey(codeSearchPerformed)) {
            return idToResponseCached.get(codeSearchPerformed);
          }
          final ArrayList<SortPair<Integer, String>> sorted = new ArrayList<>();
          if (codeSearchPerformed == VISIT_TYPE_ID) {
            final String[] visitTypeNames = indices.getVisitTypeNames();
            for (final String visitTypeName : visitTypeNames) {
              if (statistics.getVisitType(indices.getVisitType(visitTypeName)) > 0) {
                sorted.add(new SortPair<>(statistics.getVisitType(indices.getVisitType(visitTypeName), request.getPatientIds()), visitTypeName));
              }
            }
          }
          if (codeSearchPerformed == DEPARTMENT_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final int keyCnt = statistics.getDepartmentKeyCnt();
              for (int x = 0; x < keyCnt; x++) {
                final int patCnt = statistics.getDepartment(x);
                if (patCnt > 0) {
                  sorted.add(new SortPair<>(patCnt, indices.getDepartment(x)));
                }
              }
            }
          }
          if (codeSearchPerformed == ICD9_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final int keyCnt = statistics.getIcd9KeyCnt();
              for (int x = 0; x < keyCnt; x++) {
                final int patCnt = statistics.getIcd9(x);
                if (patCnt > 0) {
                  final String icd9Code = indices.getIcd9(x);
                  sorted.add(new SortPair<>(patCnt, icd9Code + VALUE_SEPARATOR + umls.getIcd9Text(icd9Code)));
                }
              }
            }
          }
          if (codeSearchPerformed == CPT_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final int keyCnt = statistics.getCptKeyCnt();
              for (int x = 0; x < keyCnt; x++) {
                final int patCnt = statistics.getCpt(x);
                if (patCnt > 0) {
                  final String cptCode = indices.getCpt(x);
                  sorted.add(new SortPair<>(patCnt, cptCode + VALUE_SEPARATOR + umls.getCptText(cptCode)));
                }
              }
            }
          }
          if (codeSearchPerformed == SNOMED_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final int keyCnt = statistics.getUniqueSnomedCounts();
              for (int x = 0; x < keyCnt; x++) {
                final int patCnt = statistics.getSnomed(x);
                if (patCnt > 0) {
                  final String snomedCode = indices.getSnomedCode(x);
                  sorted.add(new SortPair<>(patCnt, snomedCode + VALUE_SEPARATOR + indices.getSnomedText(snomedCode)));
                }
              }
            }
          }
          if (codeSearchPerformed == TID_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntIterator iterator = indices.getUniqueTids();
              while (iterator.hasNext()) {
                final int tid = iterator.next();
                final int patCnt = statistics.getTid(tid);
                if (patCnt > 0) {
                  sorted.add(new SortPair<>(patCnt, indices.getStringFromTid(tid)));
                }
              }
            }
          }
          if (codeSearchPerformed == TID_FH_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntIterator iterator = indices.getUniqueTids();
              while (iterator.hasNext()) {
                final int tid = iterator.next();
                final int patCnt = statistics.getTidFamilyHistory(tid);
                if (patCnt > 0) {
                  sorted.add(new SortPair<>(patCnt, indices.getStringFromTid(tid)));
                }
              }
            }
          }
          if (codeSearchPerformed == TID_NEGATED_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntIterator iterator = indices.getUniqueTids();
              while (iterator.hasNext()) {
                final int tid = iterator.next();
                final int patCnt = statistics.getTidNegated(tid);
                if (patCnt > 0) {
                  sorted.add(new SortPair<>(patCnt, indices.getStringFromTid(tid)));
                }
              }
            }
          }
          if (codeSearchPerformed == RX_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final int keyCnt = statistics.getRxNormKeyCnt();
              for (int x = 0; x < keyCnt; x++) {
                final int patCnt = statistics.getRxNorm(x);
                if (patCnt > 0) {
                  sorted.add(new SortPair<>(patCnt, x + VALUE_SEPARATOR + umls.getRxNormText(x)));
                }
              }
            }
          }
          if (codeSearchPerformed == ICD10_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final int keyCnt = statistics.getIcd10KeyCnt();
              for (int x = 0; x < keyCnt; x++) {
                final int patCnt = statistics.getIcd10(x);
                if (patCnt > 0) {
                  final String icd10Code = indices.getIcd10(x);
                  sorted.add(new SortPair<>(patCnt, icd10Code + VALUE_SEPARATOR + umls.getIcd10Text(icd10Code)));
                }
              }
            }
          }
          if (codeSearchPerformed == DRUG_STATUS_ROUTE_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntKeyIntMapIterator iterator = statistics.getRxStatusRoute(rxCode, statusId);
              if (iterator != null) {
                while (iterator.hasNext()) {
                  iterator.next();
                  if (iterator.getValue() != 0) {
                    sorted.add(new SortPair<>(iterator.getValue(), indices.getDrugRouteString(iterator.getKey())));
                  }
                }
              }
            }
          }
          if (codeSearchPerformed == DRUG_ROUTE_STATUS_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntKeyIntMapIterator iterator = statistics.getRxRouteStatus(rxCode, routeId);
              if (iterator != null) {
                while (iterator.hasNext()) {
                  iterator.next();
                  if (iterator.getValue() != 0) {
                    sorted.add(new SortPair<>(iterator.getValue(), indices.getDrugStatusString(iterator.getKey())));
                  }
                }
              }
            }
          }
          if (codeSearchPerformed == DRUG_ROUTE_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntKeyIntMapIterator iterator = statistics.getRxRoute(rxCode);
              if (iterator != null) {
                while (iterator.hasNext()) {
                  iterator.next();
                  if (iterator.getValue() != 0) {
                    sorted.add(new SortPair<>(iterator.getValue(), indices.getDrugRouteString(iterator.getKey())));
                  }
                }
              }
            }
          }
          if (codeSearchPerformed == DRUG_STATUS_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final IntKeyIntMapIterator iterator = statistics.getRxStatus(rxCode);
              if (iterator != null) {
                while (iterator.hasNext()) {
                  iterator.next();
                  if (iterator.getValue() != 0) {
                    sorted.add(new SortPair<>(iterator.getValue(), indices.getDrugStatusString(iterator.getKey())));
                  }
                }
              }
            }
          }
          if (codeSearchPerformed == LABS_VALUE_ID) {
            final IntKeyIntMapIterator iterator = statistics.getLabValuesToPatientCounts(labCode);
            if (iterator != null) {
              while (iterator.hasNext()) {
                iterator.next();
                System.out.println(iterator.getKey() + " / " + iterator.getValue() + " / " + indices.getLabValueText(iterator.getKey()));
                if (iterator.getValue() != 0) {
                  sorted.add(new SortPair<>(iterator.getValue(), indices.getLabValueText(iterator.getKey())));
                }
              }
            }
          }
          if (codeSearchPerformed == VITALS_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final String[] vitalsNames = indices.getVitalsNames();
              for (final String vitalsName : vitalsNames) {
                if (statistics.getVitals(indices.getVitalsCode(vitalsName)) > 0) {
                  sorted.add(new SortPair<>(statistics.getVitals(indices.getVitalsCode(vitalsName), request.getPatientIds()), vitalsName));
                }
              }
            }
          }
          if (codeSearchPerformed == LABS_CODE_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final String[] labsNames = indices.getLabsNames();
              for (final String labsName : labsNames) {
                if (statistics.getLabs(indices.getLabsCode(labsName)) > 0) {
                  sorted.add(new SortPair<>(statistics.getLabs(indices.getLabsCode(labsName), request.getPatientIds()), labsName));
                }
              }
            }
          }
          if (codeSearchPerformed == ATC_ID) {
            if (!AutosuggestSearchCommon.isPastEnd(req, startPos)) {
              final String[] atcNames = indices.getAtcNames();
              for (final String atcName : atcNames) {
                if (atcName.length() == 1 && statistics.getAtc(indices.getAtcCode(atcName), request.getPatientIds()) > 0) {
                  sorted.add(new SortPair<>(statistics.getAtc(indices.getAtcCode(atcName)), atcName + VALUE_SEPARATOR + indices.getUmls().getAtcText(atcName)));
                }
              }
            }
          }
          if (codeSearchPerformed == SCRIPT_ID) {
            if (startPos >= req.length() - 1 || ((req.length() == startPos || req.charAt(startPos - 1) == ' ' || req.charAt(startPos - 1) == '\n'))) {
              final String[] domainNames = ScriptParser.getInstance().getScriptDomains();
              int cnt = 0;
              for (final String domainName : domainNames) {
                out.add(domainName.toLowerCase() + ".");
                if (cnt++ > 15) {
                  break;
                }
              }
            }
          }
          if (codeSearchPerformed == NOTE_DESCRIPTION_ID) {
            if (startPos >= req.length() - 1 || ((req.length() == startPos || req.charAt(startPos - 1) == ' ' || req.charAt(startPos - 1) == '\n'))) {
              final String[] noteDescriptionNames = indices.getNoteDescriptionNames();
              for (final String noteDescriptionName : noteDescriptionNames) {
                sorted.add(new SortPair<>(statistics.getNoteType(indices.getNoteTypeId(noteDescriptionName), request.getPatientIds()), noteDescriptionName.toUpperCase()));
              }
            }
          }
          if (codeSearchPerformed == GENDER_ID) {
            if (startPos >= req.length() - 1 || ((req.length() == startPos || req.charAt(startPos - 1) == ' ' || req.charAt(startPos - 1) == '\n'))) {
              final String[] genderNames = indices.getGenders();
              for (final String genderName : genderNames) {
                if (indices.getDemographics().getGender().getId(genderName) != StringEnumIndex.NULL_ID && indices.getDemographics().getGender().getCount(genderName,
                    request.getPatientIds()) != 0) {
                  sorted.add(new SortPair<>(indices.getDemographics().getGender().getCount(genderName, request.getPatientIds()), genderName));
                }
              }
            }
          }
          if (codeSearchPerformed == RACE_ID) {
            if (startPos >= req.length() - 1 || ((req.length() == startPos || req.charAt(startPos - 1) == ' ' || req.charAt(startPos - 1) == '\n'))) {
              final String[] raceNames = indices.getRaces();
              for (final String raceName : raceNames) {
                if (indices.getDemographics().getGender().getId(raceName) != StringEnumIndex.NULL_ID && indices.getDemographics().getRace().getCount(raceName,
                    request.getPatientIds()) != 0) {
                  sorted.add(new SortPair<>(indices.getDemographics().getRace().getCount(raceName, request.getPatientIds()), raceName));
                }
              }
            }
          }
          if (codeSearchPerformed == ETHNICITY_ID) {
            if (startPos >= req.length() - 1 || ((req.length() == startPos || req.charAt(startPos - 1) == ' ' || req.charAt(startPos - 1) == '\n'))) {
              final String[] ethnicityNames = indices.getEthnicities();
              for (final String ethnicityName : ethnicityNames) {
                if (indices.getDemographics().getGender().getId(ethnicityName) != StringEnumIndex.NULL_ID && indices.getDemographics().getEthnicity().getCount(ethnicityName,
                    request.getPatientIds()) != 0) {
                  sorted.add(new SortPair<>(indices.getDemographics().getEthnicity().getCount(ethnicityName, request.getPatientIds()), ethnicityName));
                }
              }
            }
          }
          storeToCache = sorted.size() > 10000;
          AutosuggestSearchCommon.sortList(out, sorted);
          remaining = downsizeList(out);
        }
      }
    }

    if (storeToCache) {
      idToResponseCached.put(codeSearchPerformed, new AutosuggestResponse(out.toArray()).setRemainig(remaining));
    }

    return new AutosuggestResponse(out.toArray()).setRemainig(remaining);
  }

  private int downsizeList(final StringArrayList list) {
    if (list.size() > MAX_LIST_SIZE) {
      final int remaining = list.size() - MAX_LIST_SIZE;
      final String[] top = new String[10];
      for (int x = 0; x < top.length; x++) {
        top[x] = list.get(x);
      }
      list.clear();
      for (final String element : top) {
        list.add(element);
      }
      return remaining;
    }
    return 0;
  }

  private void generateTokenIdToMatcherMap() {
    tokenIdToTextMatcher.put(ICD9_ID, icd9Autosuggest);
    tokenIdToTextMatcher.put(ICD10_ID, icd10Autosuggest);
    tokenIdToTextMatcher.put(CPT_ID, cptAutosuggest);
    tokenIdToTextMatcher.put(RX_ID, rxAutosuggest);
    tokenIdToTextMatcher.put(SNOMED_ID, snomedAutosuggest);
    tokenIdToTextMatcher.put(VITALS_ID, vitalsAutosuggest);
    tokenIdToTextMatcher.put(LABS_CODE_ID, labsAutosuggest);
    tokenIdToTextMatcher.put(LABS_VALUE_ID, labValuesAutosuggest);
    tokenIdToTextMatcher.put(ATC_ID, atcAutosuggest);
    tokenIdToTextMatcher.put(VISIT_TYPE_ID, visitTypeAutosuggest);
    tokenIdToTextMatcher.put(SCRIPT_ID, scriptAutosuggest);
    tokenIdToTextMatcher.put(NOTE_DESCRIPTION_ID, noteDescriptionAutosuggest);
    tokenIdToTextMatcher.put(GENDER_ID, genderAutosuggest);
    tokenIdToTextMatcher.put(RACE_ID, raceAutosuggest);
    tokenIdToTextMatcher.put(ETHNICITY_ID, ethnicityAutosuggest);
    tokenIdToTextMatcher.put(DRUG_ROUTE_ID, drugRouteAutosuggest);
    tokenIdToTextMatcher.put(DRUG_STATUS_ID, drugStatusAutosuggest);
    tokenIdToTextMatcher.put(DRUG_ROUTE_STATUS_ID, drugStatusAutosuggest);
    tokenIdToTextMatcher.put(DRUG_STATUS_ROUTE_ID, drugRouteAutosuggest);
    tokenIdToTextMatcher.put(TID_ID, tidStringsAutosuggest);
    tokenIdToTextMatcher.put(TID_NEGATED_ID, tidStringsAutosuggest);
    tokenIdToTextMatcher.put(TID_FH_ID, tidStringsAutosuggest);
    tokenIdToTextMatcher.put(DEPARTMENT_ID, departmentAutosuggest);
  }

  private ArrayList<SortPair<Integer, Integer>> generatePositionTokenList(final String req, final int pos) {
    final ArrayList<SortPair<Integer, Integer>> positionTokenList = new ArrayList<>();
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, ICD9_TOKEN, pos), ICD9_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, DEPARTMENT_TOKEN, pos), DEPARTMENT_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, DEPT_TOKEN, pos), DEPARTMENT_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, ICD10_TOKEN, pos), ICD10_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, CPT_TOKEN, pos), CPT_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, RX_TOKEN, pos), RX_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, SNOMED_TOKEN, pos), SNOMED_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, VITALS_TOKEN, pos), VITALS_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, LABS_TOKEN, pos), LABS_CODE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, LABS_VALUE_TOKEN, pos), LABS_VALUE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, ATC_TOKEN, pos), ATC_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, VISIT_TYPE_TOKEN, pos), VISIT_TYPE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, SCRIPT_TOKEN, pos), SCRIPT_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, NOTE_DESCRIPTION_TOKEN, pos), NOTE_DESCRIPTION_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, GENDER_TOKEN, pos), GENDER_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, RACE_TOKEN, pos), RACE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, ETHNICITY_TOKEN, pos), ETHNICITY_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, DRUGS_ROUTE_TOKEN, pos), DRUG_ROUTE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, MEDS_ROUTE_TOKEN, pos), DRUG_ROUTE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, DRUGS_STATUS_TOKEN, pos), DRUG_STATUS_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, MEDS_STATUS_TOKEN, pos), DRUG_STATUS_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, DRUGS_ROUTE_STATUS_TOKEN, pos), DRUG_ROUTE_STATUS_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, MEDS_ROUTE_STATUS_TOKEN, pos), DRUG_ROUTE_STATUS_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, DRUGS_STATUS_ROUTE_TOKEN, pos), DRUG_STATUS_ROUTE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, MEDS_STATUS_ROUTE_TOKEN, pos), DRUG_STATUS_ROUTE_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, TID_TOKEN, pos), TID_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, TID_NEGATED_TOKEN, pos), TID_NEGATED_ID));
    positionTokenList.add(new SortPair<>(AutosuggestSearchCommon.find(req, TID_FH_TOKEN, pos), TID_FH_ID));
    return positionTokenList;
  }

}