package com.podalv.search.autosuggest;

import java.util.ArrayList;
import java.util.Collections;

import com.podalv.maps.string.StringArrayList;
import com.podalv.search.datastructures.AutosuggestRequest;
import com.podalv.utils.datastructures.SortPair;

public class AutosuggestSearchCommon {

  static boolean skipOptionalChar(final String text, final String[] values, final int index, final int valueIndex, final int valuePos, final int x) {
    boolean fnd = true;
    for (int y = index; y < text.length(); y++) {
      if (text.charAt(x) == ' ') {
        continue;
      }
      if (text.charAt(x) != values[valueIndex].charAt(valuePos)) {
        fnd = false;
        break;
      }
    }
    return fnd;
  }

  static int find(final String text, final String[] values, final int lastIndex) {
    int index = text.lastIndexOf(values[0], lastIndex);
    if (index != -1 && values.length != 1) {
      index += values[0].length();
      int valueIndex = 1;
      int valuePos = 0;
      for (int x = index; x < text.length(); x++) {
        if (text.charAt(x) == ' ') {
          continue;
        }
        if (values[valueIndex].charAt(0) == AutosuggestSearch.ANY_TEXT) {
          char terminatorChar = AutosuggestSearch.QUOTE_CHAR;
          if (values[valueIndex].length() > 1) {
            terminatorChar = values[valueIndex].charAt(1);
          }
          for (int y = x; y < text.length(); y++) {
            if (text.charAt(y) == terminatorChar) {
              x = y;
              break;
            }
          }
          valueIndex++;
          valuePos = 0;
          continue;
        }
        if (values[valueIndex].charAt(0) == AutosuggestSearch.OPTIONAL_CHAR) {
          valuePos++;
          if (skipOptionalChar(text, values, index, valueIndex, valuePos, x)) {
            x += values[valueIndex].length() - 1;
          }
          valueIndex++;
          valuePos = 0;
          if (valueIndex >= values.length) {
            return Math.min(x, text.length() - 1);
          }
        }
        if (x <= text.length() - 1 && text.charAt(x) != ' ' && text.charAt(x) != '_' && text.charAt(x) == values[valueIndex].charAt(valuePos)) {
          if (values[valueIndex].length() - 1 <= valuePos) {
            if (valueIndex >= values.length - 1) {
              return x + 1;
            }
            else {
              valuePos = -1;
              valueIndex++;
            }
          }
          valuePos++;
        }
      }
      if (valueIndex >= values.length - 1 && values[valueIndex].charAt(0) == AutosuggestSearch.OPTIONAL_CHAR) {
        return lastIndex;
      }
    }
    else if (index != -1) {
      return index + values[0].length();
    }
    return Integer.MIN_VALUE;
  }

  public static boolean isWhiteSpace(final char ch) {
    return (Character.isWhitespace(ch) || ch == '(' || ch == ')' || ch == '\n' || ch == '\r');
  }

  public static String extractRxNumber(final String req, final int startPos) {
    final int comma = req.lastIndexOf(',', startPos);
    return req.substring(req.lastIndexOf('=', comma) + 1, comma);
  }

  public static int extractRxNorm(final String req, final int startPos) {
    final String rxNorm = extractRxNumber(req, startPos);
    try {
      return Integer.parseInt(rxNorm);
    }
    catch (final Exception e) {
      // Ignore
    }
    return -1;
  }

  public static String extractLabCode(final String req, final int startPos) {
    String result = "";
    try {
      final int[] parenthesisPositions = new int[3];
      int parenthesisPos = 0;
      for (int x = 0; x < req.length(); x++) {
        if (x <= req.length() - 1) {
          if (req.charAt(x) == '"') {
            parenthesisPositions[parenthesisPos++] = x;
          }
          if (parenthesisPos > 2) {
            break;
          }
        }
      }
      result = req.substring(parenthesisPositions[0] + 1, parenthesisPositions[1]);
    }
    catch (final Exception e) {
      // Ignore just return empty string
    }
    return result;
  }

  public static Object[] extractRxCodeBeforeString(final String req, final int startPos) {
    String str = "";
    int rxNorm = -1;
    try {
      final int[] parenthesisPositions = new int[3];
      int parenthesisPos = 0;
      for (int x = startPos; x >= 0; x--) {
        if (x <= req.length() - 1) {
          if (req.charAt(x) == '"') {
            parenthesisPositions[parenthesisPos++] = x;
          }
          if (parenthesisPos > 2) {
            break;
          }
        }
      }
      if (parenthesisPositions[2] == 0) {
        str = req.substring(parenthesisPositions[1] + 1, parenthesisPositions[0]);
        rxNorm = extractRxNorm(req, parenthesisPositions[1]);
      }
      else {
        str = req.substring(parenthesisPositions[2] + 1, parenthesisPositions[1]);
        rxNorm = extractRxNorm(req, parenthesisPositions[2]);
      }
    }
    catch (final Exception e) {
      // Ignore just return empty string
    }
    return new Object[] {rxNorm, str};
  }

  public static void sortList(final StringArrayList out, final ArrayList<SortPair<Integer, String>> sorted) {
    Collections.sort(sorted);
    for (int x = sorted.size() - 1; x >= 0; x--) {
      out.add(sorted.get(x).getObject() + " [" + sorted.get(x).getComparable() + "]");
    }
  }

  public static boolean searchSucceeded(final ArrayList<SortPair<Integer, Integer>> list) {
    for (final SortPair<Integer, Integer> item : list) {
      if (item.getComparable().intValue() >= 0) {
        return true;
      }
    }
    return false;
  }

  public static SortPair<Integer, Integer> getClosest(final ArrayList<SortPair<Integer, Integer>> positionTokenList) {
    int result = -1;
    SortPair<Integer, Integer> closest = null;
    for (final SortPair<Integer, Integer> token : positionTokenList) {
      if (result <= token.getComparable().intValue()) {
        result = token.getComparable().intValue();
        closest = token;
      }
    }
    return closest;
  }

  public static boolean isCaretInDifferentLine(final AutosuggestRequest req, final int searchPos) {
    final String query = req.getQuery();
    for (int x = req.getPosition() - 1; x >= searchPos; x--) {
      if (query.charAt(x) == '\r' || query.charAt(x) == '\n') {
        return true;
      }
    }
    return false;
  }

  public static boolean isCaretAfterCode(final AutosuggestRequest req, final int searchPos, final SortPair<Integer, Integer> closestToken) {
    final String query = req.getQuery();
    if (closestToken.getObject().intValue() == AutosuggestSearch.RX_ID || closestToken.getObject().intValue() == AutosuggestSearch.ICD9_ID) {
      for (int x = req.getPosition() - 1; x >= searchPos; x--) {
        if (query.charAt(x) == ',' || query.charAt(x) == '(' || query.charAt(x) == ')') {
          return true;
        }
      }
    }
    return false;
  }

  public static boolean isScriptDefinedInCurrentContext(final AutosuggestRequest request, final SortPair<Integer, Integer> closestToken) {
    if (closestToken.getObject().intValue() == AutosuggestSearch.SCRIPT_ID) {
      final String script = request.getQuery().substring(closestToken.getComparable().intValue(), request.getPosition());
      for (int x = 0; x < script.length(); x++) {
        if (script.charAt(x) == ',' || script.charAt(x) == ')' || script.charAt(x) == '(') {
          return true;
        }
      }
      int varDefinitionPos = -1;
      while (true) {
        varDefinitionPos = request.getQuery().indexOf("var ", varDefinitionPos + 1);
        if (varDefinitionPos < 0 || varDefinitionPos > closestToken.getComparable().intValue()) {
          return false;
        }
        final int valueSeparatorPos = request.getQuery().indexOf(AutosuggestSearch.VALUE_SEPARATOR, varDefinitionPos);
        if (valueSeparatorPos < 0) {
          return false;
        }
        final int scriptFnd = request.getQuery().lastIndexOf(script, valueSeparatorPos);
        if (scriptFnd != -1) {
          int varStartPos = varDefinitionPos + 3;
          for (int x = varDefinitionPos + 3; x < valueSeparatorPos; x++) {
            if (request.getQuery().charAt(x) != ' ') {
              varStartPos = x;
              break;
            }
          }
          if (varStartPos == scriptFnd) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public static boolean isPastEnd(final String req, final int startPos) {
    if (startPos >= req.length() - 1 || (req.charAt(startPos - 1) == AutosuggestSearch.QUOTE_CHAR && (req.length() == startPos || req.charAt(startPos - 1) == ' ' || req.charAt(
        startPos - 1) == '\n'))) {
      boolean foundEnd = false;
      for (int x = startPos; x < req.length(); x++) {
        if (req.charAt(x) == AutosuggestSearch.QUOTE_CHAR) {
          if (req.lastIndexOf('"', x - 1) != -1) {
            foundEnd = true;
            break;
          }
        }
      }
      return foundEnd;
    }
    return false;
  }

}
